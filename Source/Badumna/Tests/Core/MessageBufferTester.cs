﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using NUnit.Framework;
using System.Runtime.Serialization;

namespace BadumnaTests.Core
{
    /// <summary>
    /// A test serialization class.
    /// </summary>
    [Serializable]
    [DontPerformCoverage]
    public class TestSerializableClass
    {
        public int mIntMember = 12;
        public String mStringMember = "TestString";

        public override bool Equals(System.Object obj)
        {
            if (obj == null)
            {
                return false;
            }

            TestSerializableClass a = obj as TestSerializableClass;
            if ((Object)a == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (a.mIntMember == this.mIntMember) && (a.mStringMember == this.mStringMember);
        }

        public override int GetHashCode()
        {
            return this.mIntMember;
        }
    }

    /// <summary>
    /// A non serializable test class.
    /// </summary>
    [DontPerformCoverage]
    public class TestNonSerializableClass
    {
        public int mIntMember = 99;
    }


    /// <summary>
    /// Unit tests.
    /// </summary>
    [TestFixture]
    [DontPerformCoverage]
    public class MessageBufferTester : ParseableTestHelper
    {
        class ParseableTester : ParseableTester<MessageBuffer>
        {
            public override ICollection<MessageBuffer> CreateExpectedValues()
            {
                ICollection<MessageBuffer> expectedValues = new List<MessageBuffer>();

                MessageBuffer messageBuffer = new MessageBuffer();
                messageBuffer.Write("the expected message");
                expectedValues.Add(messageBuffer);

                return expectedValues;
            }

            public override void AssertAreEqual(MessageBuffer expected, MessageBuffer actual)
            {
                //System.Console.WriteLine("lens = {0}, {1}", expected.Length, actual.Length);

                String expectedString = expected.ReadString();
                String actualString = actual.ReadString();

                //System.Console.WriteLine("actual = {0}", actualString);

                Assert.AreEqual(expectedString, actualString);
            }
        }

        private MessageBuffer mMessage;

        public MessageBufferTester()
            : base(new ParseableTester())
        { }

        [SetUp]
        public new void Initialize()
        {
            this.mMessage = new MessageBuffer();
            base.Initialize();
        }

        private void WriteAsciiString(MessageBuffer message, string str, int length)
        {
            byte[] stringBytes = Encoding.ASCII.GetBytes(str);
            Assert.GreaterOrEqual(stringBytes.Length, length);
            message.Write(stringBytes, length);
        }

        private string ReadAsciiString(MessageBuffer message, int length)
        {
            return Encoding.ASCII.GetString(message.ReadBytes(length));
        }

        [Test]
        public void BasicTest()
        {
            // Test that we can create an empty message.
            MessageBuffer envelope = new MessageBuffer();

            // Test that the read and write pointers are intially 0.
            Assert.IsTrue(envelope.ReadOffset == 0, "Invalid initial ReadOffset");
            Assert.IsTrue(envelope.WriteOffset == 0, "Invalid intial WriteOffset");

            // Test that a Read() call returns an empty string.
            String resultString = envelope.ReadString();

            Assert.IsTrue(resultString == "", resultString);//"Incorrect expected result from Read()");
            Assert.IsTrue(envelope.ReadOffset == 0, "Invalid ReadOffset");

            // Test that a ReadBytes() call throws
            bool gotException = false;
            try
            {
                envelope.ReadBytes(20);
            }
            catch (ArgumentOutOfRangeException)
            {
                gotException = true;
            }
            Assert.IsTrue(gotException, "ReadBytes(int) failed to throw ArgumentOutOfRange");

            // Test that the ReadOffset is still 0.
            Assert.IsTrue(envelope.ReadOffset == 0, "Invalid ReadOffset");

            // Test that N chars can be written to the message.
            String testString = "A test string.";

            this.WriteAsciiString(envelope, testString, testString.Length);

            // Test that the message length has increased by N.
            Assert.AreEqual(testString.Length, envelope.Length);

            // Test that the write pointer has been increased by N.
            Assert.IsTrue(envelope.WriteOffset == testString.Length, "Invalid WriteOffset");

            // Test that the read pointer is unchanged.
            Assert.IsTrue(envelope.ReadOffset == 0, "Invalid ReadOffset");

            // Test that the Read() method returns the same string as the previous Write().
            resultString = this.ReadAsciiString(envelope, testString.Length);

            Assert.AreEqual(testString, resultString);

            // Test that the ReadOffset has increased to the end of the message.
            Assert.IsTrue(envelope.ReadOffset == testString.Length, "Incorrect ReadOffset");

            // Test that a subsequent read throws, and has not changed read offset
            gotException = false;
            try
            {
                envelope.ReadBytes(20);
            }
            catch (ArgumentOutOfRangeException)
            {
                gotException = true;
            }

            Assert.AreEqual(testString.Length, envelope.ReadOffset);
            Assert.IsTrue(gotException, "ReadBytes(int) failed to throw ArgumentOutOfRange");

            // Test that a further Write and Read is possible
            envelope.Write(testString);
            resultString = envelope.ReadString();

            Assert.AreEqual(testString, resultString);

            // Test that a subsequent read returns an empty string.
            resultString = envelope.ReadString();

            Assert.AreEqual("", resultString);
        }

        [Test]
        public void WriteEmptyByteArrayTest()
        {
            MessageBuffer message = new MessageBuffer();
            byte[] emptyArray = new byte[0];

            message.Write(emptyArray, 0);
            Assert.AreEqual(0, message.Length);

            message.Write(null, 0, 0);
            Assert.AreEqual(0, message.Length);
        }

        [Test, ExpectedException(typeof(ArgumentException))]
        public void WriteByteArrayOverflowTest()
        {
            MessageBuffer message = new MessageBuffer();
            byte[] bytes = new byte[10];

            message.Write(bytes, bytes.Length + 1);
        }

        [Test]
        public void SerializableReadWriteTest()
        {
            TestSerializableClass testSerializable = new TestSerializableClass();
            TestSerializableClass resultSerializable = new TestSerializableClass();

            resultSerializable.mIntMember = -1;
            resultSerializable.mStringMember = "UniqueAndNonDefault";

            // Test that the Write(Type) method works for serializable types.
            this.mMessage.PutObject(testSerializable, typeof(object));

            // Test that a Read(Type) works for deserializable types.
            resultSerializable = this.mMessage.GetObject(typeof(object), f => { return Activator.CreateInstance(typeof(object), true); }) as TestSerializableClass;

            // Test that an object written to the message is the same as the object read from the message.
            Assert.AreEqual(testSerializable, resultSerializable);

            // Test that multiple reads works.

            //Console.WriteLine(this.mMessage.WriteOffset + ", " + this.mMessage.Length);
            this.mMessage.PutObject(testSerializable, typeof(object));

            //Console.WriteLine(this.mMessage.WriteOffset + ", " + this.mMessage.Length);
            this.mMessage.Write("test string");

            //Console.WriteLine(this.mMessage.WriteOffset + ", " + this.mMessage.Length);

            //Console.WriteLine(this.mMessage.ReadOffset + ", " + this.mMessage.Length);
            Assert.AreEqual(testSerializable, this.mMessage.GetObject(typeof(object), f => { return Activator.CreateInstance(typeof(object), true); }) as TestSerializableClass);
            //Console.WriteLine(this.mMessage.ReadOffset + ", " + this.mMessage.Length);
            Assert.AreEqual("test string", this.mMessage.ReadString());
        }

        [Test]
        public void NonSerializableReadWriteTest()
        {
            TestNonSerializableClass testNonSerializable = new TestNonSerializableClass();
            TestNonSerializableClass resultNonSerializable = new TestNonSerializableClass();
            bool caughtException = false;

            // Test that the Write(Type) method fails with exception for non serializable types.                        
            try
            {
                this.mMessage.PutObject(testNonSerializable, typeof(object));
            }
            catch (SerializationException)
            {
                caughtException = true;
            }

            Assert.IsTrue(caughtException);

            // Test that a Read(Type) fails for non-deserializable types.
            try
            {
                resultNonSerializable = this.mMessage.GetObject(typeof(object), f => { return Activator.CreateInstance(typeof(object), true); }) as TestNonSerializableClass;
            }
            catch (SerializationException)
            {
                caughtException = true;
            }

            Assert.IsTrue(caughtException);

            // Test that the ReadOffset and WriteOffset are unchanghed after the previous 2 operations.
            Assert.IsTrue(this.mMessage.ReadOffset == 0);
            Assert.IsTrue(this.mMessage.WriteOffset == 0);
        }

        [Test]
        public void ByteReadWriteTest()
        {
            byte[] testBytes = { 0, 1, 2, 3, 4, 5, 6 };

            // Test that an array can written to the message.
            this.mMessage.Write(testBytes, testBytes.Length);

            // Test that an array of bytes can be read from the message.
            Assert.AreEqual(testBytes, this.mMessage.ReadBytes(testBytes.Length));
        }

        [Test]
        public void ByteReadWriteTest2()
        {
            byte[] testBytes = { 0, 1, 2, 3, 4, 5, 6 };
            byte[] result = new byte[testBytes.Length];

            // Test that an array can written to the message.
            this.mMessage.Write(testBytes, testBytes.Length);

            // Test that an array of bytes can be read from the message.
            Assert.AreEqual(testBytes.Length, this.mMessage.ReadBytes(result, testBytes.Length));
            Assert.AreEqual(testBytes, result);

            // Test that subsequent reads read from the correct position.
            // Test that the return length is correct for a partial read.
            byte[] result2 = new byte[testBytes.Length + 1];
            this.mMessage.Write(testBytes, testBytes.Length);
            Assert.AreEqual(testBytes.Length, this.mMessage.ReadBytes(result2, testBytes.Length + 1));
        }

        [Test]
        public void ByteReadWriteTest3()
        {
            // Test that an empty array arrives intact
            byte[] testBytes = { };

            this.mMessage.PutObject(testBytes, testBytes.GetType());

            Assert.AreEqual(testBytes, this.mMessage.GetObject(testBytes.GetType(), f => { return Activator.CreateInstance(testBytes.GetType(), true); }));
        }

        [Test]
        public void TypedReadWriteTest()
        {
            String testData = "Some random stuff: 42097202lk20- %$%@#$K%246 9826 ^Vl;gvliws83gvp54@Cc5ouv06vb$^fc67";
            TestSerializableClass resultSerializable;
            bool caughtException = false;

            this.mMessage.Write(testData);

            // Test that a Read(Type) method fails for a random string with an exception.
            try
            {
                resultSerializable = this.mMessage.GetObject(typeof(object), f => { return Activator.CreateInstance(typeof(object), true); }) as TestSerializableClass;
            }
            catch (SerializationException)
            {
                caughtException = true;
            }
            Assert.IsTrue(caughtException);

            // Test that the ReadOffset is unchanghed after the previous operation.
            Assert.IsTrue(this.mMessage.ReadOffset == 0);
        }

        [Test]
        public void OffsetTest()
        {
            String testString = "A test string.";
            this.WriteAsciiString(this.mMessage, testString, testString.Length);

            // Test that the ReadOffset can be moved within the valid buffer and that corresponding reads give the correct answer.
            this.mMessage.ReadOffset = 2;
            String result = this.ReadAsciiString(this.mMessage, 4);

            Assert.AreEqual("test", result);
            Assert.AreEqual(6, this.mMessage.ReadOffset);

            // Test that setting the ReadOffset fails with exception if the offset is out of bounds.
            bool caughtException = false;

            try
            {
                this.mMessage.ReadOffset = 99;
            }
            catch (System.ArgumentOutOfRangeException) // e)
            {
                //Console.WriteLine("Test exception was : " + e.Message);
                caughtException = true;
            }

            Assert.IsTrue(caughtException, "Failed to throw out of range exception.");

            // Test that the WriteOffset can be moved within the valid buffer and that the corresponding write gives the correct answer.
            this.mMessage.WriteOffset = 2;

            this.WriteAsciiString(this.mMessage, "pass", 4);
            this.mMessage.ReadOffset = 0;
            result = this.ReadAsciiString(this.mMessage, 14);


            Assert.AreEqual("A pass string.", result);
            Assert.AreEqual(6, this.mMessage.WriteOffset);
            Assert.AreEqual(testString.Length, this.mMessage.Length);

            // Test that an offseted read returns the correct result.
            this.mMessage.ReadOffset = 2;
            result = this.ReadAsciiString(this.mMessage, 4);

            Assert.AreEqual("pass", result);
            Assert.AreEqual(6, this.mMessage.ReadOffset);

            // Test that setting the WriteOffset fails with exception if the offset is out of bounds.
            caughtException = false;

            try
            {
                this.mMessage.WriteOffset = -1;
            }
            catch (System.ArgumentOutOfRangeException) // e)
            {
                //Console.WriteLine("Test exception was : " + e.Message);
                caughtException = true;
            }

            Assert.IsTrue(caughtException, "Failed to throw exception.");

        }
        /*
                [Test]
                public void ReadLineTest()
                {
                    String testString = "Some text with a \n newline character.";
                    String expectedString = "Some text with a ";
                    String resultString;

                    // Test that a string with an end-of-line character can be written.
                    this.mMessage.WriteChars(testString, testString.Length);

                    this.mMessage.ReadOffset += 2;  // Skip over length bytes written by WriteChars

                    // Test that the ReadLine method will return the string up until the end-of-line character.
                    resultString = this.mMessage.ReadLine();

                    Assert.AreEqual(expectedString, resultString);

                    // Test that the ReadLine(L) method returns a string of L chars, where L is less than the remaining chars in the message.
                    resultString = this.mMessage.ReadLine(8);

                    Assert.AreEqual(" newline", resultString);

                    // Test that the ReadLine() method will return the remaining chars in the message.
                    resultString = this.mMessage.ReadLine();

                    Assert.AreEqual(" character.", resultString);
                }
                */

        [Test]
        public void OffsetTypeTest()
        {
            int a = 0;
            int b = 1;
            int c = 2;
            int offset;
            int anotherNumber = 99;

            this.mMessage.Write(a);
            offset = this.mMessage.WriteOffset;
            this.mMessage.Write(b);
            this.mMessage.Write(c);

            Assert.AreEqual(a, this.mMessage.ReadInt());
            Assert.AreEqual(b, this.mMessage.ReadInt());
            Assert.AreEqual(c, this.mMessage.ReadInt());

            // Test that sequence of ints can be overwritten.
            this.mMessage.WriteOffset = offset;
            this.mMessage.Write(anotherNumber);

            // Test that the correct read sequence is given.
            this.mMessage.ReadOffset = 0;

            Assert.AreEqual(a, this.mMessage.ReadInt());
            Assert.AreEqual(anotherNumber, this.mMessage.ReadInt());
            Assert.AreEqual(c, this.mMessage.ReadInt());
        }

        [Test]
        public void ListTest()
        {
            List<String> expectedList = new List<string>();

            expectedList.Add("one");
            expectedList.Add("two");
            expectedList.Add("three");

            this.mMessage.PutObject(expectedList, expectedList.GetType());

            Assert.Less(this.mMessage.Length, 20);

            // Test with type of List<...>
            IList<String> actualList = (IList<String>)this.mMessage.GetObject(expectedList.GetType(), f => { return Activator.CreateInstance(expectedList.GetType(), true); });

            Assert.AreEqual(expectedList.Count, actualList.Count);
            for (int i = 0; i < actualList.Count; i++)
            {
                Assert.AreEqual(expectedList[i], actualList[i]);
            }


            // Test with type of IList<...>
            this.mMessage.ReadOffset = 0;
            actualList = (IList<String>)this.mMessage.GetObject(typeof(IList<String>), f => { return Activator.CreateInstance(typeof(IList<String>), true); });
            Assert.AreEqual(expectedList.Count, actualList.Count);
            for (int i = 0; i < actualList.Count; i++)
            {
                Assert.AreEqual(expectedList[i], actualList[i]);
            }
        }

        [Test]
        public void DictionaryTest()
        {
            Dictionary<string, int> expectedDict = new Dictionary<string, int>();

            expectedDict.Add("one", 1);
            expectedDict.Add("two", 2);
            expectedDict.Add("three", 3);

            this.mMessage.PutObject(expectedDict, expectedDict.GetType());

            Assert.Less(this.mMessage.Length, 32);

            Dictionary<string, int> actualDict = (Dictionary<string, int>)this.mMessage.GetObject(expectedDict.GetType(), f => { return Activator.CreateInstance(expectedDict.GetType(), true); });

            Assert.AreEqual(expectedDict.Count, actualDict.Count);
            foreach (KeyValuePair<string, int> pair in actualDict)
            {
                Assert.IsTrue(expectedDict.ContainsKey(pair.Key));
                Assert.AreEqual(expectedDict[pair.Key], pair.Value);
            }
        }

        /*
        [Test]
        public void GetTest()
        {
            char[] testChars = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h' };
            char[] expectedChars = testChars;

            this.mMessage.Write(testChars);

            // Test that Buffer can be got and compared with an expected char array.
            for (int i = 0; i < this.mMessage.Length; ++i)
            {
                Assert.IsTrue(this.mMessage.Buffer[i] == (byte)expectedChars[i]);
            }

            // Test that Length can be got and is correct.
            Assert.IsTrue(this.mMessage.Length == 8);
        }
        */
        /*
        [Test]
        public void PeekTest()
        {
            this.WriteAsciiString(this.mMessage, "abcdefgh", 8);
            this.mMessage.ReadOffset = 3;

            // Test that Peek returns the next correct char without incrementing the read offset.
            Assert.AreEqual('d', this.mMessage.Peek());
            Assert.AreEqual(3, this.mMessage.ReadOffset);

            // Test that Peek at the end of the the stream returns -1.
            this.mMessage.ReadOffset = 8;
            Assert.AreEqual(-1, this.mMessage.Peek());
        }
         */
    }

}
