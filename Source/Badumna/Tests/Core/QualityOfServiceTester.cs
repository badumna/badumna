﻿using System.Collections.Generic;
using Badumna.Core;
using NUnit.Framework;

namespace BadumnaTests.Core
{
    [TestFixture]
    [DontPerformCoverage]
    public class QualityOfServiceTester : ParseableTestHelper
    {
        class ParseableTester : ParseableTester<QualityOfService>
        {
            public override ICollection<QualityOfService> CreateExpectedValues()
            {
                ICollection<QualityOfService> expectedValues = new List<QualityOfService>();

                expectedValues.Add(QualityOfService.Reliable);
                expectedValues.Add(QualityOfService.Unreliable);

                QualityOfService priorityQos = new QualityOfService(true);
                priorityQos.Priority = QosPriority.Low;

                expectedValues.Add(priorityQos);

                return expectedValues;
            }

            public override void AssertAreEqual(QualityOfService expected, QualityOfService actual)
            {
                // AllowableDelayMilliseconds and IsDelayable are not serialized
                //Assert.AreEqual(expected.AllowableDelayMilliseconds, actual.AllowableDelayMilliseconds);
                //Assert.AreEqual(expected.IsDelayable, actual.IsDelayable);
                Assert.AreEqual(expected.HandleAlongRoutePath, actual.HandleAlongRoutePath);
                Assert.AreEqual(expected.IsReliable, actual.IsReliable);
                Assert.AreEqual(expected.MustBeInOrder, actual.MustBeInOrder);
                Assert.AreEqual(expected.PriorityProbability, actual.PriorityProbability);
            }
        }

        class FailureQOS : QualityOfService
        {
            public bool mConnectionFailureCalled = false;
            public bool mHandleFailureCalled = false;

            public FailureQOS(bool reliable)
                : base(reliable)
            {
            }

            internal override void HandleConnectionFailure(PeerAddress destination)
            {
                this.mConnectionFailureCalled = true;
            }

            internal override bool HandleFailure(PeerAddress destination, int nthAttempt)
            {
                this.mHandleFailureCalled = true;
                return true;
            }
        }

        public QualityOfServiceTester()
            : base(new ParseableTester())
        {
        }

        [Test]
        public void MultipleQOSTest()
        {
            FailureQOS qos1 = new FailureQOS(false);
            FailureQOS qos2 = new FailureQOS(false);
            MultipleQOS mult = new MultipleQOS(qos1);
            mult.Combine(qos2);
            Assert.IsFalse(mult.IsReliable, "Combined QOS incorrectly marked as reliable");

            mult.HandleConnectionFailure(new PeerAddress());
            Assert.IsTrue(qos1.mConnectionFailureCalled, "HandleConnectionFailure in first QOS was not called");
            Assert.IsTrue(qos2.mConnectionFailureCalled, "HandleConnectionFailure in second QOS was not called");


            qos1 = new FailureQOS(true);
            qos2 = new FailureQOS(true);
            mult = new MultipleQOS(qos1);
            mult.Combine(qos2);
            Assert.IsTrue(mult.IsReliable, "Combined QOS incorrectly marked as unreliable");

            mult.HandleFailure(new PeerAddress(), 0);
            Assert.IsTrue(qos1.mHandleFailureCalled, "HandleFailure in first QOS was not called");
            Assert.IsTrue(qos2.mHandleFailureCalled, "HandleFailure in second QOS was not called");
        }
    }

}
