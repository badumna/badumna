﻿using System;
using System.Collections.Generic;
using Badumna.Core;
using Badumna.Utilities;
using NUnit.Framework;


namespace BadumnaTests.Core
{
    [TestFixture]
    [DontPerformCoverage]
    public class NetworkEventTester
    {
        private bool mDelegate0WasCalled = false;
        private bool mDelegate1WasCalled = false;
        private String mExpectedEventArgument = "A test argument.";

        public void HandleEvent()
        {
            this.mDelegate0WasCalled = true;
            //Console.WriteLine("Delegate 0 ");
        }

        public void HandleEvent(String envelope)
        {
            this.mDelegate1WasCalled = true;
            //Console.WriteLine("Delegate 0 : {0} ", envelope);
            Assert.AreEqual(this.mExpectedEventArgument, envelope);
        }

        [Test]
        public void NetworkEventConstructerTest()
        {
            // Test that we can create an event with a time and handler associated with it.
            new NetworkEvent(TimeSpan.Zero, Apply.Func(this.HandleEvent));
        }

        [Test]
        public void PerformActionTest()
        {
            NetworkEvent ev1 = new NetworkEvent(TimeSpan.Zero, Apply.Func(this.HandleEvent));
            NetworkEvent ev2 = new NetworkEvent(TimeSpan.Zero, Apply.Func(this.HandleEvent, this.mExpectedEventArgument));

            // Test that the handler is be called via the PerformAction() method.
            ev1.PerformAction();
            Assert.IsTrue(this.mDelegate0WasCalled);

            // Test that a handler with arguments is called via the PerformAction() method.
            ev2.PerformAction();
            Assert.IsTrue(this.mDelegate1WasCalled);
        }

        [Test]
        public void IterationTest()
        {
            List<NetworkEvent> eventList = new List<NetworkEvent>();
            this.mDelegate0WasCalled = false;

            for (int i = 0; i < 20; i++)
                eventList.Add(new NetworkEvent(TimeSpan.Zero, Apply.Func(this.HandleEvent)));

            // Test that a collection of Events can be iterated through.
            foreach (NetworkEvent ev in eventList)
            {
                this.mDelegate0WasCalled = false;
                ev.PerformAction();
                Assert.IsTrue(this.mDelegate0WasCalled);
            }

            Assert.IsTrue(this.mDelegate0WasCalled);
            this.mDelegate0WasCalled = false;
            eventList[7].PerformAction();
            Assert.IsTrue(this.mDelegate0WasCalled);
        }

        [Test]
        public void SortTest()
        {
            List<NetworkEvent> eventList = new List<NetworkEvent>();
            this.mDelegate0WasCalled = false;

            TimeSpan delay = new TimeSpan(0, 0, 0, 0, 25); // 25ms
            TimeSpan start = TimeSpan.Zero;
            TimeSpan nextEventTime = start;

            for (int i = 0; i < 20; i++)
            {
                eventList.Add(new NetworkEvent(nextEventTime, Apply.Func(this.HandleEvent)));
                nextEventTime += delay;
            }

            eventList.Reverse();

            // Test that a collection of Events can be sorted.
            eventList.Sort();

            TimeSpan lastEventTime = start - delay;
            foreach (NetworkEvent ev in eventList)
            {
                Assert.Less(lastEventTime, ev.Time);
                lastEventTime = ev.Time;
            }
        }
    }
}
