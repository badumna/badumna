﻿using System.Collections.Generic;
using System.Net;
using Badumna.Core;
using NUnit.Framework;

namespace BadumnaTests.Core
{
    [TestFixture]
    public class PeerAddressTester : ParseableTestHelper
    {
        class ParseableTester : ParseableTester<PeerAddress>
        {
            public override ICollection<PeerAddress> CreateExpectedValues()
            {
                ICollection<PeerAddress> expectedValues = new List<PeerAddress>(); ;

                byte[] bytes = new byte[] { 99, 21, 193, 2 };

                expectedValues.Add(new PeerAddress(new IPAddress(bytes), 66, NatType.Open));
                expectedValues.Add(PeerAddress.GetLoopback(222));

                return expectedValues;
            }
        }

        public PeerAddressTester()
            : base(new ParseableTester())
        { }

        [Test]
        public void AsKeyTest()
        {
            foreach (PeerAddress expected in this.CreateExpectedValues<PeerAddress>())
            {
                MessageBuffer message = new MessageBuffer();
                Dictionary<PeerAddress, string> store = new Dictionary<PeerAddress, string>();

                store.Add(expected, "some string");

                expected.ToMessage(message, typeof(PeerAddress));
                PeerAddress actual = new PeerAddress();
                actual.FromMessage(message);

                //System.Console.WriteLine("{0} -- {1}", expected.GetHashCode(), actual.GetHashCode());
                //System.Console.WriteLine("{0} - {1}", expected, actual);
                Assert.AreEqual(expected, actual);
                Assert.IsTrue(store.ContainsKey(actual));
            }
        }

        [Test]
        public void PortComparisonTest()
        {
            List<PeerAddress> addressList = new List<PeerAddress>();

            PeerAddress addressOne = PeerAddress.GetAddress("2.3.0.0", 9, NatType.SymmetricNat);
            PeerAddress addressTwo = PeerAddress.GetAddress("2.3.0.0", 11, NatType.SymmetricNat);
            PeerAddress addressThree = PeerAddress.GetAddress("2.3.0.0", 12, NatType.SymmetricNat);

            addressList.Add(addressOne);
            addressList.Add(addressTwo);
            Assert.IsFalse(addressList.Contains(addressThree), "Failed to distinguish between addresses with the same port.");

            addressList.Add(addressThree);
            Assert.IsTrue(addressList.Contains(addressThree));
        }

        [Test]
        public void GetAddressTest()
        {
            Assert.AreEqual(PeerAddress.Nowhere, PeerAddress.GetAddress("too:many:colons"));
            Assert.AreEqual(PeerAddress.Nowhere, PeerAddress.GetAddress("no colons"));
            Assert.AreEqual(PeerAddress.Nowhere, PeerAddress.GetAddress("localhost:65536"));
            Assert.AreEqual(PeerAddress.Nowhere, PeerAddress.GetAddress(":"));
            Assert.AreEqual(PeerAddress.Nowhere, PeerAddress.GetAddress("localhost:asdf"));
            Assert.AreEqual(PeerAddress.Nowhere, PeerAddress.GetAddress("localhost:-42"));
            Assert.AreEqual(PeerAddress.GetLoopback(42), PeerAddress.GetAddress("localhost:42"));
            Assert.AreEqual(PeerAddress.GetLoopback(42), PeerAddress.GetAddress("127.0.0.1:42"));
        }
    }
}
