﻿using System;
using System.Threading;
using Badumna.Core;
using Badumna.Utilities;
using NUnit.Framework;

namespace BadumnaTests.Core
{
    [TestFixture]
    [DontPerformCoverage]
    public class NetworkEventQueueTester
    {
        private int mEventOrderNum = 0;

        public delegate void TestEventHandler();
        public delegate void TestEventHandler<T>(T expected);
        private TestEventHandler<int> mTestEventHandlerDelegate;

        private TimeSpan mTotalEventDelay;
        private TimeSpan mLastEventTime;

        private NetworkEventQueue eventQueue;

        public void HandleEvent(int expected)
        {
            Assert.AreEqual(expected, this.mEventOrderNum++);
            this.mTotalEventDelay += (this.eventQueue.Now - this.mLastEventTime);
            this.mLastEventTime = this.eventQueue.Now;
        }

        public void HandleNoArgEvent()
        {
        }

        private NetworkEvent PushNetworkEvent(int delayMilliseconds, int arg)
        {
            return this.eventQueue.Schedule(delayMilliseconds, delegate { this.mTestEventHandlerDelegate(arg); });
        }

        public TimeSpan GetRandomDelay()
        {
            int delayMs = (int)(100.0 * RandomSource.Generator.NextDouble());
            return new TimeSpan(0, 0, 0, delayMs, 0);
        }

        public void StopQueueOnEmpty()
        {
            this.eventQueue.Stop();
        }

        [SetUp]
        public void Initialize()
        {
            this.eventQueue = new NetworkEventQueue();

            this.mTotalEventDelay = new TimeSpan();
            this.mLastEventTime = this.eventQueue.Now;

            this.mEventOrderNum = 0;
            
            this.mTestEventHandlerDelegate = new TestEventHandler<int>(this.HandleEvent);

            this.eventQueue.Reset();

            // Set the empty queue handler to the StopQueueOnEmpty method.
            this.eventQueue.EmptyQueueHandler += new NetworkEventQueue.QueueEventHandler(this.StopQueueOnEmpty);
        }

        [TearDown]
        public void Terminate()
        {
        }

        [Test]
        public void PushTest()
        {
            // Test that an event can be pushed.
            NetworkEvent ev = this.eventQueue.Push(this.HandleNoArgEvent);

            Assert.IsTrue(this.eventQueue.Contains(ev), "Failed to insert event.");
            Assert.AreEqual(1, this.eventQueue.Count);
        }

        [Test]
        public void PushTest2()
        {
            // Test that an event can be pushed.
            NetworkEvent ev = this.eventQueue.Schedule(0, delegate { this.mTestEventHandlerDelegate(0); });

            Assert.IsTrue(this.eventQueue.Contains(ev), "Failed to insert event.");
            Assert.AreEqual(1, this.eventQueue.Count);
            this.eventQueue.Pop().PerformAction();

            Assert.AreEqual(1, this.mEventOrderNum);
        }

        [Test]
        public void PopTest()
        {
            // Test that an event can be popped.
            NetworkEvent ev = this.eventQueue.Pop();

            Assert.AreEqual(0, this.eventQueue.Count);
        }

        [Test]
        public void OrderTest()
        {
            TimeSpan now = this.eventQueue.Now;

            for (int i = 0; i < 10; i++)
            {
                this.PushNetworkEvent((int)GetRandomDelay().TotalMilliseconds, i);
            }

            // Test that popped events are in order.
            TimeSpan lastTime = this.eventQueue.Pop().Time;
            for (int i = 0; i < 9; i++)
            {
                NetworkEvent ev = this.eventQueue.Pop();
                Assert.IsTrue(lastTime <= ev.Time, "Incorrect order of popped events.");
                lastTime = ev.Time;
            }
        }

        [Test]
        public void RemoveTest()
        {
            TimeSpan now = this.eventQueue.Now;

            for (int i = 0; i < 100; i++)
            {
                this.PushNetworkEvent((int)this.GetRandomDelay().TotalMilliseconds, i);
            }
            Assert.AreEqual(100, this.eventQueue.Count);

            NetworkEvent ev = this.eventQueue.Schedule((int)this.GetRandomDelay().TotalMilliseconds, delegate { this.mTestEventHandlerDelegate(100); });

            Assert.IsTrue(this.eventQueue.Contains(ev), "Failed to insert event.");

            // Test that events can be removed.
            this.eventQueue.Remove(ev);
            Assert.IsFalse(this.eventQueue.Contains(ev), "Failed to remove event.");

            // Test that the first event can be removed.
            ev = this.eventQueue.Peek();
            this.eventQueue.Remove(ev);
            Assert.IsFalse(this.eventQueue.Contains(ev), "Failed to remove event.");

            // Test that attempting to remove a null event is caught.
            this.eventQueue.Remove(null);

            while (this.eventQueue.Count > 0) this.eventQueue.Pop();
            Assert.AreEqual(0, this.eventQueue.Count);

            // Test that removing on an empty list works.
            this.eventQueue.Schedule((int)this.GetRandomDelay().TotalMilliseconds, this.HandleNoArgEvent);
            this.eventQueue.Remove(ev);
            this.eventQueue.Remove(null);
        }

        // Test that two events with the same time can be removed correctly.

        // Test that we can specify a start time offset.

        [Test]
        public void NextEventTimeTest()
        {
            NetworkEvent ev = this.PushNetworkEvent(0, 0);

            // Test that we can get the time of the next event.
            Assert.AreEqual(ev.Time, this.eventQueue.TimeOfNextEvent());
        }

        [Test]
        public void RunTest()
        {
            TimeSpan time = this.eventQueue.Now;
            int delayMs = 0;

            for (int i = 0; i < 100; i++)
            {
                this.PushNetworkEvent(delayMs, i);
                delayMs += 10;
            }

            // Test that we can specify the time of the last popped event.
            // Test that the Run() method will pop events at the correct time and correct order.
            this.eventQueue.RunUntil(this.eventQueue.Now + TimeSpan.FromMilliseconds(delayMs + 1));

            Assert.AreEqual(100, this.mEventOrderNum);
            Assert.AreEqual(0, this.eventQueue.Count);
        }

        [Test]
        public void RunTest2()
        {
            TimeSpan time = this.eventQueue.Now;
            int delayMs = 0;

            for (int i = 0; i < 100; i++)
            {
                this.PushNetworkEvent(delayMs, i);
                delayMs += 10;
            }

            // Test that we can call Run() without specifying the time of the last popped event.
            // Test that the Run() method will pop events at the correct time and correct order.
            this.eventQueue.Run(null);

            Assert.AreEqual(100, this.mEventOrderNum);
            Assert.AreEqual(0, this.eventQueue.Count);
        }


        [Test]
        public void TimingTest()
        {
            TimeSpan runTime = new TimeSpan(0, 0, 0, 1, 100);

            TimeSpan eventTime = this.eventQueue.Now;
            int eventDelayMs = 0;

            this.mLastEventTime = eventTime;

            for (int i = 0; i < 100; i++)
            {
                this.PushNetworkEvent(eventDelayMs - (int)((this.eventQueue.Now - eventTime).TotalMilliseconds), i);
                eventDelayMs += 10;
            }

            // Test that events are called at the correct time in Run();
            this.eventQueue.RunUntil(this.eventQueue.Now + runTime);

            Assert.AreEqual(100, this.mEventOrderNum);
            //Console.WriteLine("Total event delay was {0}ms.", this.mTotalEventDelay.TotalMilliseconds);
            //Console.WriteLine("Average event delay was {0}ms.", this.mTotalEventDelay.TotalMilliseconds / 99);

            // Allow an average deviation of 1ms per event.
            TimeSpan minDelay = new TimeSpan(0, 0, 0, 0, 900);
            TimeSpan maxDelay = new TimeSpan(0, 0, 0, 0, 1100);

            Assert.IsTrue(this.mTotalEventDelay.TotalMilliseconds > minDelay.TotalMilliseconds, "Events were too fast.");
            Assert.IsTrue(this.mTotalEventDelay.TotalMilliseconds < maxDelay.TotalMilliseconds, "Events were too slow.");
        }

        // Test that duplicate events are not created.


        [Test]
        [Category("Slow")]
        public void ThreadTest()
        {
            this.eventQueue.Schedule(10000, this.eventQueue.Stop);

            // Test that we can call Run() from within a thread with events being pushed.
            this.eventQueue.IsRunning = true;
            Fiber fiber = new Fiber(this.eventQueue.Run);
            ThreadManager threadManager = new ThreadManager();
            threadManager.Start(fiber);

            for (int i = 0; i < 100; i++)
            {
                this.PushNetworkEvent(0, i);
                Thread.Sleep(15);
            }

            this.eventQueue.Schedule(3, () => this.eventQueue.Stop());

            fiber.Join();

            Assert.AreEqual(100, this.mEventOrderNum);
            Assert.AreEqual(1, this.eventQueue.Count);
        }
    }
}
