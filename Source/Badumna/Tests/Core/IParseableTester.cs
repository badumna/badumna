﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using NUnit.Framework;

namespace BadumnaTests.Core
{
    [DontPerformCoverage]
    public interface IParseableTester
    {
        void Initialize();
        void ToFromMessageTest();
        void AsArgumentTest();
    }

    [DontPerformCoverage]
    public abstract class ParseableTestHelper
    {
        protected IParseableTester mParseableTester;

        public ParseableTestHelper(IParseableTester baseTester)
        {
            this.mParseableTester = baseTester;
        }

        [SetUp]
        public virtual void Initialize()
        {
            this.mParseableTester.Initialize();
        }

        [Test]
        public void ToFromMessageTest()
        {
            this.mParseableTester.ToFromMessageTest();
        }

        [Test]
        public void AsArgumentTest()
        {
            this.mParseableTester.AsArgumentTest();
        }

        internal ICollection<T> CreateExpectedValues<T>() where T : IParseable
        {
            return (this.mParseableTester as ParseableTester<T>).CreateExpectedValues();
        }

        internal void AssertAreEqual<T>(T expected, T actual) where T : IParseable
        {
            (this.mParseableTester as ParseableTester<T>).AssertAreEqual(expected, actual);
        }
    }

    [DontPerformCoverage]
    abstract class ParseableTester<T> : IParseableTester where T : IParseable
    {
        enum Protocol
        {
            MethodCallBack
        }

        private int mNumCalls = 0;
        private bool mPassedAssertion = false;
        private T mExpectedValue;

        [SetUp]
        public void Initialize()
        {
            this.mNumCalls = 0;
            this.mPassedAssertion = false;
            this.mExpectedValue = default(T);
        }

        [TearDown]
        public void Terminate() { }

        [UnitTestProtocolMethod(0)]
        internal void MethodCallBack(String leftPadding, T arg, String rightPadding)
        {
            this.mNumCalls++;
            this.AssertAreEqual(this.mExpectedValue, arg);
            this.mPassedAssertion = true;
            Assert.AreEqual("left padding", leftPadding);
            Assert.AreEqual("right padding", rightPadding);
        }

        public abstract ICollection<T> CreateExpectedValues();

        public ICollection<IParseable> ExampleList
        {
            get
            {
                ICollection<IParseable> exampleList = new List<IParseable>();

                foreach (T item in this.CreateExpectedValues())
                {
                    exampleList.Add(item as IParseable);
                }

                return exampleList;
            }
        }

        public virtual void AssertAreEqual(T expected, T actual)
        {
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ToFromMessageTest()
        {
            foreach (T expected in this.CreateExpectedValues())
            {
                MessageBuffer message = new MessageBuffer();

                expected.ToMessage(message, typeof(T));
                //System.Diagnostics.Debug.WriteLine("Message length = " + message.Length);
                T actual = (T)this.ConstructForDeserialization(typeof(T));
                actual.FromMessage(message);

                this.AssertAreEqual(expected, actual);
                Assert.AreEqual(0, message.Available, "Message has available data after deserialization.");
            }
        }

        protected virtual object ConstructForDeserialization(Type type)
        {
            return Activator.CreateInstance(type, true);  // Not using new so that the constructors for IParseables can be private if desired
        }

        [Test]
        public void AsArgumentTest()
        {
            ICollection<T> arguments = this.CreateExpectedValues();

            foreach (T argument in arguments)
            {
                this.mNumCalls = 0;
                this.mPassedAssertion = false;
                BaseEnvelope message = new BaseEnvelope(QualityOfService.Reliable);
                MessageParser messageParser = new MessageParser("ParseableTester", typeof(UnitTestProtocolMethodAttribute), this.ConstructForDeserialization);

                messageParser.RegisterMethodsIn(this);
                messageParser.ForgeMethodList();

                messageParser.RemoteCall(message, this.MethodCallBack, "left padding", argument, "right padding");
                //System.Diagnostics.Debug.WriteLine("Message length = " + message.Length + " (including 30 bytes padding)");
                this.mExpectedValue = argument;
                messageParser.Parse(message);

                Assert.AreEqual(1, this.mNumCalls, "More than one call made.");
                Assert.IsTrue(this.mPassedAssertion, "Failed to pass assertions.");
            }
        }
    }
}
