﻿//---------------------------------------------------------------------------------
// <copyright file="TransportLimiterTester.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace BadumnaTests.Transport
{
    using System;
    using Badumna.Core;
    using Badumna.Transport;
    using Badumna.Utilities;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    [DontPerformCoverage]
    public class TransportLimiterTester
    {
        private Simulator eventQueue;
        private TransportLimiter limiter;

        [SetUp]
        public void Initialize()
        {
        }

        [TearDown]
        public void TearDown()
        {
        }

        [Test]
        public void ValidConstructorParameterThrowsNoException()
        {
            this.limiter = this.CreateLimiter(100000, 2);
            this.limiter = this.CreateLimiter(10000, 5);
            this.limiter = this.CreateLimiter(10000, 99);
        }

        [Test]
        [ExpectedException("System.ArgumentException")]
        public void InvalidDropRateConstructorParameterThrowsException()
        {
            this.limiter = this.CreateLimiter(100000, 100);
        }

        [Test]
        [ExpectedException("System.ArgumentException")]
        public void InvalidDropRateConstructorParameterThrowsException2()
        {
            this.limiter = this.CreateLimiter(100000, -1);
        }

        [Test]
        [ExpectedException("System.ArgumentException")]
        public void InvalidBandwidthLimitConstructorParameterThrowsException()
        {
            this.limiter = this.CreateLimiter(-1, 0);
        }

        /*
        [Test]
        public void DropRateIsEnforced()
        {
            int dropped = 0;

            // 250 are expected to be dropped on average
            this.limiter = this.CreateLimiter(100000000, 5);
            for (int i = 0; i < 5000; i++)
            {
                bool pass = this.limiter.ProcessPacket(100);
                if (!pass)
                {
                    dropped++;
                }

                this.eventQueue.RunFor(TimeSpan.FromMilliseconds(10));
            }

            Assert.IsTrue(dropped >= 200 && dropped <= 300);
        }*/

        [Test]
        public void PacketsNeverGetDroppedForVeryLargeLimitAndSmallTrafficAmount()
        {
            this.limiter = this.CreateLimiter(100000000, 0);
            for (int i = 0; i < 1000; i++)
            {
                bool pass = this.limiter.ProcessPacket(100);
                Assert.IsTrue(pass);
                this.eventQueue.RunFor(TimeSpan.FromMilliseconds(10));
            }
        }

        [Test]
        public void ShortTermBrustIsAllowed()
        {
            // 20kbytes per second limit, 25ms sampling period, 40 samples per second, 500 bytes allowed per period
            this.limiter = this.CreateLimiter(20000, 0);

            for (int t = 0; t < 4; t++)
            {
                for (int i = 0; i < 10; i++)
                {
                    bool pass = this.limiter.ProcessPacket(100);
                    Assert.IsTrue(pass);
                }

                this.eventQueue.RunFor(TimeSpan.FromMilliseconds(4));
            }
        }

        [Test]
        public void AvgBandwidthConsumptionIsEnforced()
        {
            int totalPassed = 0;
            int totalSend = 0;
            int totalMs = 10000;
            this.limiter = this.CreateLimiter(20000, 0);

            for (int t = 0; t < totalMs; t = t + 10)
            {
                for (int r = 0; r < 5; r++)
                {
                    totalSend += 200;
                    bool pass = this.limiter.ProcessPacket(200);
                    if (pass)
                    {
                        totalPassed += 200;
                    }
                }

                this.eventQueue.RunFor(TimeSpan.FromMilliseconds(10));
            }

            // total send is at least twice an many as the limit
            int totalLimit = 20000 * totalMs / 1000;
            Assert.IsTrue(totalSend > 2 * totalLimit);
            Assert.IsTrue(totalPassed <= totalLimit * 1.1);
        }

        [Test]
        public void PerSamplingPeriodLimitIsEnforced()
        {
            this.limiter = this.CreateLimiter(20000, 0);

            for (int i = 0; i < 20; i++)
            {
                bool pass = this.limiter.ProcessPacket(1000);
                Assert.IsTrue(pass);
                this.eventQueue.RunFor(TimeSpan.FromMilliseconds(25));
            }

            for (int i = 0; i < 10; i++)
            {
                bool pass = this.limiter.ProcessPacket(50);
                Assert.IsTrue(pass);
            }

            for (int i = 0; i < 10; i++)
            {
                bool pass = this.limiter.ProcessPacket(50);
                Assert.IsFalse(pass);
            }
        }

        [Test]
        public void MeasuredBandwidthIsResetEverySamplingPeriod()
        {
            this.limiter = this.CreateLimiter(20000, 0);

            bool pass = this.limiter.ProcessPacket(20000);
            Assert.IsTrue(pass);
            pass = this.limiter.ProcessPacket(501);
            Assert.IsFalse(pass);
            
            this.eventQueue.RunFor(TimeSpan.FromMilliseconds(1025));

            pass = this.limiter.ProcessPacket(20000);
            Assert.IsTrue(pass);
        }

        private TransportLimiter CreateLimiter(int bandwidthLimit, int dropRate)
        {
            this.eventQueue = new Simulator();
            INetworkConnectivityReporter connectivityReporter = MockRepository.GenerateStub<INetworkConnectivityReporter>();
            connectivityReporter.Stub(x => x.Status).Return(ConnectivityStatus.Online);
            return new TransportLimiter(this.eventQueue, connectivityReporter, bandwidthLimit, dropRate);
        }
    }
}
