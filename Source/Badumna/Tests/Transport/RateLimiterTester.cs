﻿using System;
using System.Collections.Generic;
using Badumna.Core;
using Badumna.Transport;
using Badumna.Utilities;
using NUnit.Framework;

namespace BadumnaTests.Transport
{
    public class SimpleChannelTester : ChannelTester<Block>
    {
        protected override Block MakeTea()
        {
            return new Block(10);
        }

        internal override Channel<Block> MakeChannel()
        {
            ITime timeKeeper = new NetworkEventQueue();
            return new SimpleChannel<Block>(timeKeeper);
        }
    }

    class SimpleChannel<T> : Channel<T> where T : ISized
    {
        private Queue<T> mQueue = new Queue<T>();

        public SimpleChannel(ITime timeKeeper)
            : base(timeKeeper)
        {
        }

        public override bool IsEmpty { get { return this.mQueue.Count == 0; } }

        protected override void DoPut(T item)
        {
            this.mQueue.Enqueue(item);
        }

        protected override T DoGet()
        {
            if (this.IsEmpty)
            {
                return default(T);
            }

            return this.mQueue.Dequeue();
        }

        public override T Peek()
        {
            if (this.IsEmpty)
            {
                return default(T);
            }

            return this.mQueue.Peek();
        }

        public override void Clear()
        {
            this.mQueue.Clear();
        }
    }

    /// <summary>
    /// A channel that always has exactly one element on it; Get() returns that element, also making the
    /// list empty, but a new one arrives immediately so Notify gets called.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    class IncessantChannel<T> : Channel<T> where T : ISized
    {
        public override bool IsEmpty
        {
            get { return false; }
        }

        private T mItem;

        public IncessantChannel(T item, ITime timeKeeper)
            : base(timeKeeper)
        {
            this.mItem = item;
        }

        protected override void DoPut(T item)
        {
        }

        protected override T DoGet()
        {
            this.InvokeUnemptiedCallback();
            return this.mItem;
        }

        public override T Peek()
        {
            return this.mItem;
        }

        public override void Clear()
        {
        }
    }

    public class Block : ISized
    {
        private int mLength;
        public int Length
        {
            get { return this.mLength; }
        }

        public Block(int length)
        {
            this.mLength = length;
        }
    }

    [TestFixture]
    public class RateLimiterTester
    {
        internal Simulator eventQueue;
        private ITime timeKeeper;

        [SetUp]
        public virtual void SetUp()
        {
            this.eventQueue = new Simulator();
            this.timeKeeper = this.eventQueue;
        }

        protected virtual void CheckExpected(int runTimeSeconds, int bitsPerSecond, int blockSize, int count)
        {
            int expected = (int)Math.Floor((double)runTimeSeconds * bitsPerSecond / (blockSize * 8));
            Assert.AreEqual(expected, count);
        }

        [Test]
        public void BasicRateTest()
        {
            int blockSize = 13;
            int bitsPerSecond = 1024;
            int blockCount = 1000;
            int runTimeSeconds = 10;

            SimpleChannel<Block> channel = new SimpleChannel<Block>(this.timeKeeper);

            int count = 0;
            RateLimiter<Block> rateLimiter = new RateLimiter<Block>(
                delegate { return bitsPerSecond / 8; },
                channel,
                delegate(Block block) { count++; return block.Length; },
                this.eventQueue,
                this.timeKeeper);

            // Fill up the channel instantaneously
            for (int i = 0; i < blockCount; i++)
            {
                channel.Put(new Block(blockSize));
            }

            // Assert.AreEqual(1, count);  // First will get processed as soon as it's on the queue, but no others should be

            this.eventQueue.RunFor(TimeSpan.FromSeconds(runTimeSeconds));
            this.CheckExpected(runTimeSeconds, bitsPerSecond, blockSize, count);

            this.eventQueue.RunFor(TimeSpan.FromSeconds(runTimeSeconds));
            this.CheckExpected(2 * runTimeSeconds, bitsPerSecond, blockSize, count);
        }

        // RateLimiter should still send packets when the rate is extreme.
        [Test]
        public void ExtremeRateTest()
        {
            int blockSize = 13;
            double rateBytesPerSecond = 1e30;
            int blockCount = 1000;
            int runTimeSeconds = 10;

            SimpleChannel<Block> channel = new SimpleChannel<Block>(this.timeKeeper);

            int count = 0;
            RateLimiter<Block> rateLimiter = new RateLimiter<Block>(
                delegate { return rateBytesPerSecond; },
                channel,
                delegate(Block block) { count++; return block.Length; },
                this.eventQueue,
                this.timeKeeper);

            // Fill up the channel instantaneously
            for (int i = 0; i < blockCount; i++)
            {
                channel.Put(new Block(blockSize));
            }

            this.eventQueue.RunFor(TimeSpan.FromSeconds(runTimeSeconds));

            // Big limit, we should have sent everything
            Assert.AreEqual(blockCount, count);
        }

        // Make sure the RateLimiter starts up if there are items in the channel before the rate limiter is created
        [Test]
        public void PreFilledTest()
        {
            int blockSize = 13;
            int bitsPerSecond = 1024;
            int blockCount = 1000;
            int runTimeSeconds = 10;

            SimpleChannel<Block> channel = new SimpleChannel<Block>(this.timeKeeper);

            for (int i = 0; i < blockCount; i++)
            {
                channel.Put(new Block(blockSize));
            }

            int count = 0;
            RateLimiter<Block> rateLimiter = new RateLimiter<Block>(
                delegate { return bitsPerSecond / 8; },
                channel,
                delegate(Block block) { count++; return block.Length; },
                this.eventQueue,
                this.timeKeeper);
            //this.eventQueue.RunFor(TimeSpan.FromMilliseconds(100.0));
            //Assert.AreEqual(1, count);  // First will get processed as soon as it's on the queue, but no others should be
            this.eventQueue.RunFor(TimeSpan.FromSeconds(runTimeSeconds));

            this.CheckExpected(runTimeSeconds, bitsPerSecond, blockSize, count);
        }

        // RateLimiter should work with Channels that call ChannelUnemptied from within a call to Get
        [Test]
        public void GetCausesNotifyTest()
        {
            int blockSize = 13;
            int bitsPerSecond = 1024;
            int runTimeSeconds = 10;

            IncessantChannel<Block> channel = new IncessantChannel<Block>(new Block(blockSize), this.timeKeeper);

            int count = 0;
            RateLimiter<Block> rateLimiter = new RateLimiter<Block>(
                delegate { return bitsPerSecond / 8; },
                channel,
                delegate(Block block) { count++; return block.Length; },
                this.eventQueue,
                this.timeKeeper);
            //this.eventQueue.RunFor(TimeSpan.FromMilliseconds(10.0));
            //Assert.AreEqual(1, count);  // First will get processed as soon as it's on the queue, but no others should be

            this.eventQueue.RunFor(TimeSpan.FromSeconds(runTimeSeconds));
            this.CheckExpected(runTimeSeconds, bitsPerSecond, blockSize, count);

            this.eventQueue.RunFor(TimeSpan.FromSeconds(runTimeSeconds));
            this.CheckExpected(2 * runTimeSeconds, bitsPerSecond, blockSize, count);
        }
    }

    [TestFixture]
    public class DelayedRateLimiterTester : RateLimiterTester
    {
        [SetUp]
        public override void SetUp()
        {
            base.SetUp();
            this.eventQueue.ExecutionDelay = TimeSpan.FromMilliseconds(400);
        }

        protected override void CheckExpected(int runTimeSeconds, int bitsPerSecond, int blockSize, int count)
        {
            int expected = (int)Math.Floor((double)runTimeSeconds * bitsPerSecond / (blockSize * 8));
            Assert.LessOrEqual(count, expected);
            Assert.GreaterOrEqual(count, (int)(0.94 * expected));
        }
    }
}
