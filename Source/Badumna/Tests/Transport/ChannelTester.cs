﻿using Badumna.Transport;
using NUnit.Framework;

namespace BadumnaTests.Transport
{
    public abstract class ChannelTester<T> where T : ISized
    {
        internal abstract Channel<T> MakeChannel();

        protected abstract T MakeTea();

        [Test]
        public void NotifyTest()
        {
            int count = 0;

            Channel<T> channel = this.MakeChannel();
            channel.SetUnemptiedCallback(delegate { count++; });

            Assert.IsTrue(channel.IsEmpty);

            Assert.AreEqual(0, count);
            channel.Put(this.MakeTea());
            Assert.AreEqual(1, count);
        }

        [Test]
        public void PreFilledNotifyTest()
        {
            int count = 0;

            Channel<T> channel = this.MakeChannel();
            channel.Put(this.MakeTea());
            Assert.AreEqual(0, count);
            channel.SetUnemptiedCallback(delegate { count++; });
            Assert.AreEqual(1, count);
        }
    }
}
