﻿//---------------------------------------------------------------------------------
// <copyright file="IGDDescParserTester.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace BadumnaTests.UPnP
{
    using System.IO;
    using Badumna.Core;
    using Badumna.Transport.Upnp;
    using NUnit.Framework;

    [TestFixture]
    [DontPerformCoverage]
    public class IGDDescParserTester
    {
        /// <summary>
        /// The IGD description parser.
        /// </summary>
        private IGDDescParser parser;

        [Test]
        public void SimpleConstructorThrowsNoException()
        {
            this.parser = new IGDDescParser();
        }

        [Test]
        public void Billion7401DescCanBeParsed()
        {
            this.parser = new IGDDescParser();
            this.parser.Parse(Billion7401IGDDesc.Content);
            Assert.AreEqual("http://192.168.1.254:2800/upnp/", this.parser.Result.URLBase);
            Assert.AreEqual("/EmWeb/UPnP/Control/12", this.parser.Result.Second.ControlURL);
            Assert.AreEqual("/EmWeb/UPnP/Eventing/12", this.parser.Result.Second.EventSubURL);
            Assert.AreEqual("WANIPConnection.xml", this.parser.Result.Second.SCPDURL);
            Assert.AreEqual("urn:schemas-upnp-org:service:WANIPConnection:1", this.parser.Result.Second.ServiceType);
        }

        [Test]
        public void TomatoDescCanBeParsed()
        {
            this.parser = new IGDDescParser();
            this.parser.Parse(TomatoIGDDesc.Content);
            
            Assert.AreEqual(string.Empty, this.parser.Result.URLBase);
            Assert.AreEqual("/ctl/IPConn", this.parser.Result.Second.ControlURL);
            Assert.AreEqual("/evt/IPConn", this.parser.Result.Second.EventSubURL);
            Assert.AreEqual("/WANIPCn.xml", this.parser.Result.Second.SCPDURL);
            Assert.AreEqual("urn:schemas-upnp-org:service:WANIPConnection:1", this.parser.Result.Second.ServiceType);
        }
    }
}