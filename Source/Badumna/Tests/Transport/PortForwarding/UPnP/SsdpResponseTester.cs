﻿//-----------------------------------------------------------------------
// <copyright file="SsdpResponseTester.cs" company="NICTA">
//     Copyright (c) 2011 NICTA. All right reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.UPnP
{
    using System.IO;
    using Badumna.Core;
    using Badumna.Transport.Upnp;
    using NUnit.Framework;

    [TestFixture]
    [DontPerformCoverage]
    public class SsdpResponseTester
    {
        /// <summary>
        /// SSDP response.
        /// </summary>
        private SsdpResponse response;

        [Test]
        public void Billion7401ResposeCanBeParsedCorrectly()
        {
            using (TextReader reader = new StringReader(Billion7401SSDPResponse.Content))
            {
                this.response = new SsdpResponse(reader);
                Assert.AreEqual("http://192.168.1.254:2800/upnp/InternetGatewayDevice.xml", this.response.GetLocation());
                Assert.AreEqual("urn:schemas-upnp-org:device:InternetGatewayDevice:1", this.response.GetSearchTarget());
            }
        }

        [Test]
        public void InvalidInputCanBeSilentlyIgnored()
        {
            using (TextReader reader = new StringReader("OK this is just some junk."))
            {
                this.response = new SsdpResponse(reader);
                Assert.IsNull(this.response.GetSearchTarget());
                Assert.IsNull(this.response.GetLocation());
            }
        }
    }
}