﻿//-----------------------------------------------------------------------
// <copyright file="UPnPPortMappingEntryTester.cs" company="NICTA">
//     Copyright (c) 2011 NICTA. All right reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.UPnP
{
    using Badumna.Core;
    using Badumna.Transport.Upnp;
    using NUnit.Framework;

    [TestFixture]
    [DontPerformCoverage]
    public class UPnPPortMappingEntryTester
    {
        /// <summary>
        /// The port mapping entry.
        /// </summary>
        private UPnPPortMappingEntry entry;

        [Test]
        public void SimpleConstructorThrowsNoException()
        {
            this.entry = new UPnPPortMappingEntry();
        }

        [Test]
        [ExpectedException("Badumna.Transport.Upnp.SimpleXMLException")]
        public void MissingTagCausesSimleXMLException()
        {
            this.entry = new UPnPPortMappingEntry(string.Empty);
        }

        [Test]
        public void ValidXMLCanBeDeserializedCorrectly()
        {
            string content = "<NewRemoteHost></NewRemoteHost>"
                                    + "<NewExternalPort>1234</NewExternalPort>"
                                    + "<NewProtocol>UDP</NewProtocol>"
                                    + "<NewInternalPort>1234</NewInternalPort>"
                                    + "<NewInternalClient>192.168.1.1</NewInternalClient>"
                                    + "<NewEnabled>1</NewEnabled>"
                                    + "<NewPortMappingDescription>random</NewPortMappingDescription>"
                                    + "<NewLeaseDuration>1233</NewLeaseDuration>";

            this.entry = new UPnPPortMappingEntry(content);
            Assert.AreEqual(string.Empty, this.entry.RemoteHost);
            Assert.AreEqual(1234, this.entry.ExternalPort);
            Assert.AreEqual(1234, this.entry.InternalPort);
            Assert.AreEqual("UDP", this.entry.Protocol.ToString().ToUpper());
            Assert.AreEqual("192.168.1.1", this.entry.InternalClient);
            Assert.AreEqual(true, this.entry.IsEnabled);
            Assert.AreEqual("random", this.entry.Description);
            Assert.AreEqual(1233, this.entry.LeaseDuration);
        }

        [Test]
        [ExpectedException("System.ArgumentException")]
        public void UnsupportedProtocolCausesException()
        {
            this.entry = new UPnPPortMappingEntry();
            this.entry.Protocol = System.Net.Sockets.ProtocolType.Raw;
        }

        [Test]
        public void SerializeProducesExpectedXML()
        {
            string content = "<NewRemoteHost></NewRemoteHost>"
                                    + "<NewExternalPort>1234</NewExternalPort>"
                                    + "<NewProtocol>UDP</NewProtocol>"
                                    + "<NewInternalPort>1234</NewInternalPort>"
                                    + "<NewInternalClient>192.168.1.1</NewInternalClient>"
                                    + "<NewEnabled>1</NewEnabled>"
                                    + "<NewPortMappingDescription>random</NewPortMappingDescription>"
                                    + "<NewLeaseDuration>1233</NewLeaseDuration>";

            this.entry = new UPnPPortMappingEntry(content);
            string serialized = this.entry.Serialize("name", "method");
            Assert.IsTrue(serialized.Contains("<NewRemoteHost></NewRemoteHost>"));
            Assert.IsTrue(serialized.Contains("<NewExternalPort>1234</NewExternalPort>"));
            Assert.IsTrue(serialized.Contains("<NewProtocol>UDP</NewProtocol>"));
            Assert.IsTrue(serialized.Contains("<NewInternalPort>1234</NewInternalPort>"));
            Assert.IsTrue(serialized.Contains("<NewInternalClient>192.168.1.1</NewInternalClient>"));
            Assert.IsTrue(serialized.Contains("<NewEnabled>1</NewEnabled>"));
            Assert.IsTrue(serialized.Contains("<NewPortMappingDescription>random</NewPortMappingDescription>"));
            Assert.IsTrue(serialized.Contains("<NewLeaseDuration>1233</NewLeaseDuration>"));

            Assert.IsTrue(serialized.Contains("<u:method xmlns:u=\"name\">"));
        }
    }
}