﻿//---------------------------------------------------------------------------------
// <copyright file="Billion7401IGDDesc.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace BadumnaTests.UPnP
{
    using System.IO;
    using Badumna.Core;
    using Badumna.Transport.Upnp;
    using NUnit.Framework;

    [DontPerformCoverage]
    public class Billion7401IGDDesc
    {
        /// <summary>
        /// Gets the content.
        /// </summary>
        /// <value>The content.</value>
        public static string Content
        {
            get
            {
                return @"<?xml version=""1.0""?>
<root xmlns=""urn:schemas-upnp-org:device-1-0"">
  <specVersion>
    <major>1</major>
    <minor>0</minor>
  </specVersion>
  <URLBase>http://192.168.1.254:2800/upnp/</URLBase>

  
  <device>
    <deviceType>urn:schemas-upnp-org:device:InternetGatewayDevice:1</deviceType>
    <friendlyName>home.gateway</friendlyName>
<manufacturer>Copyright @ Billion Electric Co., Ltd. All rights reserved.</manufacturer>
<manufacturerURL>http://www.billion.com.au</manufacturerURL>
<modelDescription>Copyright @ Billion Electric Co., Ltd. All rights reserved. UPnP IGD in ISOS 6.02b</modelDescription>
<modelName>BiPAC 7401VGPR3</modelName>
<modelNumber>BiPAC 7401VGPR3</modelNumber>
<modelURL>http://www.billion.com.au</modelURL>
<serialNumber>Prototype</serialNumber>
<UDN>uuid:58cddb90-80cd-a77f-361f-a371c8eee89b</UDN>
<UPC>Universal Product Code</UPC>

        <iconList>
      <icon>
        <mimetype>image/png</mimetype>
        <width>16</width>
        <height>16</height>
        <depth>1</depth>
        <url>16X16X1.png</url>
      </icon>
      <icon>
        <mimetype>image/png</mimetype>
        <width>16</width>
        <height>16</height>
        <depth>8</depth>
        <url>16X16X8.png</url>
      </icon>
      <icon>
        <mimetype>image/png</mimetype>
        <width>32</width>
        <height>32</height>
        <depth>1</depth>
<url>32X32X1.png</url>
</icon>
      <icon>
        <mimetype>image/png</mimetype>
        <width>32</width>
        <height>32</height>
        <depth>8</depth>
        <url>32X32X8.png</url>
      </icon>
      <icon>
        <mimetype>image/png</mimetype>
        <width>48</width>
        <height>48</height>
        <depth>1</depth>
        <url>48X48X1.png</url>
      </icon>
      <icon>
        <mimetype>image/png</mimetype>
        <width>48</width>
        <height>48</height>
        <depth>8</depth>
        <url>48X48X8.png</url>
      </icon>
    </iconList>

    <serviceList>
      <service>
	<serviceType>urn:dslforum-org:service:DeviceInfo:1</serviceType>
	<serviceId>urn:dslforum-org:serviceId:DeviceInfo1</serviceId>
	<SCPDURL>DeviceInfo.xml</SCPDURL>
	
	<controlURL>/EmWeb/UPnP/Control/2</controlURL>
	<eventSubURL></eventSubURL>
      </service>
      <service>
	<serviceType>urn:dslforum-org:service:LANConfigSecurity:1</serviceType>
	<serviceId>urn:dslforum-org:serviceId:LANConfigSecurity1</serviceId>
	<SCPDURL>LANConfigSecurity.xml</SCPDURL>
	
	<controlURL>/EmWeb/UPnP/Control/4</controlURL>
	<eventSubURL></eventSubURL>
      </service>
      <service>
	<serviceType>urn:schemas-upnp-org:service:Layer3Forwarding:1</serviceType>
	<serviceId>urn:upnp-org:serviceId:L3Forwarding1</serviceId>
	<SCPDURL>Layer3Forwarding.xml</SCPDURL>
	
	<controlURL>/EmWeb/UPnP/Control/6</controlURL>
	<eventSubURL>/EmWeb/UPnP/Eventing/6</eventSubURL>
      </service>
    </serviceList>
    <deviceList> 
     
      
      
      


      <device> 
        <deviceType>urn:schemas-upnp-org:device:WANDevice:1</deviceType>
        <friendlyName>home.gateway</friendlyName>
<manufacturer>Copyright @ Billion Electric Co., Ltd. All rights reserved.</manufacturer>
<manufacturerURL>http://www.billion.com.au</manufacturerURL>
<modelDescription>Copyright @ Billion Electric Co., Ltd. All rights reserved. UPnP IGD in ISOS 6.02b</modelDescription>
<modelName>BiPAC 7401VGPR3</modelName>
<modelNumber>BiPAC 7401VGPR3</modelNumber>
<modelURL>http://www.billion.com.au</modelURL>
<serialNumber>Prototype</serialNumber>
<UDN>uuid:64423774-667a-b126-36a2-ad228d6274f5</UDN>
<UPC>Universal Product Code</UPC>

	    <iconList>
      <icon>
        <mimetype>image/png</mimetype>
        <width>16</width>
        <height>16</height>
        <depth>1</depth>
        <url>16X16X1.png</url>
      </icon>
      <icon>
        <mimetype>image/png</mimetype>
        <width>16</width>
        <height>16</height>
        <depth>8</depth>
        <url>16X16X8.png</url>
      </icon>
      <icon>
        <mimetype>image/png</mimetype>
        <width>32</width>
        <height>32</height>
        <depth>1</depth>
        <url>32X32X1.png</url>
      </icon>
      <icon>
        <mimetype>image/png</mimetype>
        <width>32</width>
        <height>32</height>
        <depth>8</depth>
        <url>32X32X8.png</url>
      </icon>
      <icon>
        <mimetype>image/png</mimetype>
        <width>48</width>
        <height>48</height>
        <depth>1</depth>
        <url>48X48X1.png</url>
      </icon>
      <icon>
        <mimetype>image/png</mimetype>
        <width>48</width>
        <height>48</height>
        <depth>8</depth>
        <url>48X48X8.png</url>
      </icon>
    </iconList>

        <serviceList>
          <service>
            <serviceType>urn:schemas-upnp-org:service:WANCommonInterfaceConfig:1</serviceType>
            <serviceId>urn:upnp-org:serviceId:WANCommonIFC1</serviceId>
            <SCPDURL>WANCommonInterfaceConfig.xml</SCPDURL>
            
            <controlURL>/EmWeb/UPnP/Control/8</controlURL>
            <eventSubURL>/EmWeb/UPnP/Eventing/8</eventSubURL>
          </service>
        </serviceList>
        <deviceList> 
         
         
          


          <device> 
            <deviceType>urn:schemas-upnp-org:device:WANConnectionDevice:1</deviceType>
            <friendlyName>home.gateway</friendlyName>
<manufacturer>Copyright @ Billion Electric Co., Ltd. All rights reserved.</manufacturer>
<manufacturerURL>http://www.billion.com.au</manufacturerURL>
<modelDescription>Copyright @ Billion Electric Co., Ltd. All rights reserved. UPnP IGD in ISOS 6.02b</modelDescription>
<modelName>BiPAC 7401VGPR3</modelName>
<modelNumber>BiPAC 7401VGPR3</modelNumber>
<modelURL>http://www.billion.com.au</modelURL>
<serialNumber>Prototype</serialNumber>
<UDN>uuid:ab969b8a-b957-9340-3aca-ad4cffcae6bd</UDN>
<UPC>Universal Product Code</UPC>

	        <iconList>
      <icon>
        <mimetype>image/png</mimetype>
        <width>16</width>
        <height>16</height>
        <depth>1</depth>
        <url>16X16X1.png</url>
      </icon>
      <icon>
        <mimetype>image/png</mimetype>
        <width>16</width>
        <height>16</height>
        <depth>8</depth>
        <url>16X16X8.png</url>
      </icon>
      <icon>
        <mimetype>image/png</mimetype>
        <width>32</width>
        <height>32</height>
        <depth>1</depth>
        <url>32X32X1.png</url>
      </icon>
      <icon>
        <mimetype>image/png</mimetype>
        <width>32</width>
        <height>32</height>
        <depth>8</depth>
        <url>32X32X8.png</url>
      </icon>
      <icon>
        <mimetype>image/png</mimetype>
        <width>48</width>
        <height>48</height>
        <depth>1</depth>
        <url>48X48X1.png</url>
      </icon>
      <icon>
        <mimetype>image/png</mimetype>
        <width>48</width>
        <height>48</height>
        <depth>8</depth>
        <url>48X48X8.png</url>
      </icon>
    </iconList>

            <serviceList>
              <service>
                <serviceType>urn:schemas-upnp-org:service:WANDSLLinkConfig:1</serviceType>
                <serviceId>urn:upnp-org:serviceId:WANDSLLinkC1</serviceId>
                <SCPDURL>WANDSLLinkConfig.xml</SCPDURL>
                
                <controlURL>/EmWeb/UPnP/Control/10</controlURL>
                <eventSubURL>/EmWeb/UPnP/Eventing/10</eventSubURL>
              </service>

              

              

              

              
              <service>
                <serviceType>urn:schemas-upnp-org:service:WANIPConnection:1</serviceType>
                <serviceId>urn:upnp-org:serviceId:WANIPConn1</serviceId>
                <SCPDURL>WANIPConnection.xml</SCPDURL>
                
                <controlURL>/EmWeb/UPnP/Control/12</controlURL>
                <eventSubURL>/EmWeb/UPnP/Eventing/12</eventSubURL>
              </service>
              

              
            </serviceList>
          </device>

          
        </deviceList> 
      </device>

      
    </deviceList> 
   <presentationURL>http://192.168.1.254:2800/index.html</presentationURL>
  </device>
</root>";
            }
        }
    }
}