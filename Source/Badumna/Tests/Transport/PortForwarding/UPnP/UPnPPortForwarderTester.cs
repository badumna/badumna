﻿//-----------------------------------------------------------------------
// <copyright file="UPnPPortForwarderTester.cs" company="NICTA">
//     Copyright (c) 2008 NICTA. All right reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.UPnP
{
    using System;
    using System.Net;
    using System.Net.Sockets;
    using Badumna.Core;
    using Badumna.Transport.Upnp;
    using NUnit.Framework;

    [TestFixture]
    [DontPerformCoverage]
    public class UPnPPortForwarderTester
    {
        /// <summary>
        /// The port forwarder.
        /// </summary>
        private UpnpPortForwarder forwarder;

        [SetUp]
        public void SetUp()
        {
            this.forwarder = new UpnpPortForwarder();
        }

        [TearDown]
        public void TearDown()
        {
            this.forwarder = null;
        }

        [Test]
        public void SimpleConstructorThrowsNoException()
        {
            this.forwarder = new UpnpPortForwarder();
        }

        [Test]
        public void DisposeOnUnusedForwarderThrowsNoException()
        {
            this.forwarder.Dispose();
        }

        [Test]
        [ExpectedException("System.InvalidOperationException")]
        public void UninitializedForwarderThrowsExceptionWhenTryingToMapPort()
        {
            IPEndPoint ep = new IPEndPoint(new IPAddress(new byte[] { 1, 1, 1, 1 }), 1);
            this.forwarder.TryMapPort(ProtocolType.Udp, ep, 1, TimeSpan.FromSeconds(100));
        }

        [Test]
        [ExpectedException("System.ArgumentNullException")]
        public void ForwarderThrowsExceptionWhenTryingToMapPortOnNullEndPoint()
        {
            this.forwarder.TryMapPort(ProtocolType.Udp, null, 1, TimeSpan.FromSeconds(100));
        }

        [Test]
        [ExpectedException("System.ArgumentException")]
        public void ForwarderThrowsExceptionWhenTryingToMapPortForUnsupportedProtocolType()
        {
            IPEndPoint ep = new IPEndPoint(new IPAddress(new byte[] { 1, 1, 1, 1 }), 1);
            this.forwarder.TryMapPort(ProtocolType.Raw, ep, 1, TimeSpan.FromSeconds(100));
        }

        [Test]
        [ExpectedException("System.InvalidOperationException")]
        public void GetExternalAddressThrowsExceptionWhenNotInitialized()
        {
            this.forwarder.GetExternalAddress();
        }
    }
}