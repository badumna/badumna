﻿//---------------------------------------------------------------------------------
// <copyright file="Billion7401SSDPResponse.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace BadumnaTests.UPnP
{
    using Badumna.Core;
    using NUnit.Framework;

    [DontPerformCoverage]
    public class Billion7401SSDPResponse
    {
        /// <summary>
        /// Gets the content.
        /// </summary>
        /// <value>The content.</value>
        public static string Content
        {
            get
            {
                string content = "HTTP/1.1 200 OK\r\n" +
                    "Server: Unknown/0.0 UPnP/1.0 Conexant-EmWeb/R6_1_0\r\n" +
                    "Connection: close\r\n" +
                    "EXT:\r\n" +
                    "LOCATION: http://192.168.1.254:2800/upnp/InternetGatewayDevice.xml\r\n" +
                    "CACHE-CONTROL: max-age = 1830\r\n" +
                    "ST: urn:schemas-upnp-org:device:InternetGatewayDevice:1\r\n" +
                    "USN: uuid:58cddb90-80cd-a77f-361f-a371c8eee89b::urn:schemas-upnp-org:device:InternetGatewayDevice:1\r\n";

                return content;
            }
        }
    }
}