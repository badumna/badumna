﻿//-----------------------------------------------------------------------
// <copyright file="UPnPExternalIPAddressTester.cs" company="NICTA">
//     Copyright (c) 2011 NICTA. All right reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.UPnP
{
    using Badumna.Core;
    using Badumna.Transport.Upnp;
    using NUnit.Framework;

    [TestFixture]
    [DontPerformCoverage]
    public class UPnPExternalIPAddressTester
    {
        /// <summary>
        /// The address.
        /// </summary>
        private UPnPExternalIPAddress address;

        [Test]
        public void SimpleConstructorThrowsNoException()
        {
            this.address = new UPnPExternalIPAddress();
        }

        [Test]
        public void DeserializerWorksAsExpected()
        {
            this.address = new UPnPExternalIPAddress("<NewExternalIPAddress>1.2.3.4</NewExternalIPAddress>");
            Assert.AreEqual("1.2.3.4", this.address.ExternalIPAddress);
        }

        [Test]
        [ExpectedException("Badumna.Transport.Upnp.SimpleXMLException")]
        public void IllegalTagNameCausesSimpleXMLException()
        {
            this.address = new UPnPExternalIPAddress("<ExternalIPAddress>1.2.3.4</ExternalIPAddress>");
        }

        [Test]
        public void SerializedObjectDoesNotContainActuallyAddress()
        {
            this.address = new UPnPExternalIPAddress("<NewExternalIPAddress>1.2.3.4</NewExternalIPAddress>");
            string serialized = this.address.Serialize("random", "methodName");
            Assert.IsFalse(serialized.Contains("1.2.3.4"));
            Assert.IsTrue(serialized.Contains("<NewExternalIPAddress></NewExternalIPAddress>"));
        }

        [Test]
        public void NamespaceAndMethodNameAreCorrectlySerialized()
        {
            this.address = new UPnPExternalIPAddress("<NewExternalIPAddress>1.2.3.4</NewExternalIPAddress>");
            string serialized = this.address.Serialize("random", "methodName");
            Assert.IsTrue(serialized.Contains("<u:methodName xmlns:u=\"random\">"));
            Assert.IsTrue(serialized.Contains("</u:methodName>"));
        }
    }
}