﻿//---------------------------------------------------------------------------------
// <copyright file="UPnPPortMappingIdTester.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace BadumnaTests.UPnP
{
    using Badumna.Core;
    using Badumna.Transport.Upnp;
    using NUnit.Framework;

    [TestFixture]
    [DontPerformCoverage]
    public class UPnPPortMappingIdTester
    {
        /// <summary>
        /// The mapping id.
        /// </summary>
        private UPnPPortMappingId id;

        [Test]
        public void SimpleConstructorThrowsNoException()
        {
            this.id = new UPnPPortMappingId();
        }

        [Test]
        public void SerializeProducesExpectedXML()
        {
            this.id = new UPnPPortMappingId();
            this.id.RemoteHost = "192.168.1.1";
            this.id.Protocol = "UDP";
            this.id.ExternalPort = 12345;

            string serialized = this.id.Serialize("name", "method");
            Assert.IsTrue(serialized.Contains("<NewRemoteHost>192.168.1.1</NewRemoteHost>"));
            Assert.IsTrue(serialized.Contains("<NewExternalPort>12345</NewExternalPort>"));
            Assert.IsTrue(serialized.Contains("<NewProtocol>UDP</NewProtocol>"));

            Assert.IsTrue(serialized.Contains("<u:method xmlns:u=\"name\">"));
            Assert.IsTrue(serialized.Contains("</u:method>"));
        }
    }
}