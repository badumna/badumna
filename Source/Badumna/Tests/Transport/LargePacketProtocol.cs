﻿using System;
using Badumna;
using Badumna.Core;
using Badumna.Transport;
using Badumna.Utilities;
using NUnit.Framework;

namespace BadumnaTests.Transport
{
    [DontPerformCoverage]
    class LargePacketProcotol : TransportProtocol
    {
        public int NumBytesReceived { get; private set; }

        public LargePacketProcotol(TransportProtocol parent)
            : base(parent)
        {
            this.ForgeMethodList();
        }

        public void ClearBytesReceived()
        {
            this.NumBytesReceived = 0;
        }

        [ConnectionfulProtocolMethod(0)]
        public void ReceiveBytes(byte[] bytes)
        {
            Logger.TraceInformation(LogTag.Tests, "LargePacketPrototocol receiving {0} bytes", this.CurrentEnvelope.Length);
            this.NumBytesReceived = this.CurrentEnvelope.Length;
        }

        public void SendBytes(int numBytesToSend, PeerAddress destination)
        {
            TransportEnvelope envelope = this.GetMessageFor(destination, QualityOfService.Reliable);

            int numBytesLeft = numBytesToSend - envelope.Length;
            while (numBytesLeft > 0)
            {
                int numBytes = Math.Min(numBytesLeft, ushort.MaxValue);
                byte[] bytes = new byte[numBytes - 3];

                this.RemoteCall(envelope, this.ReceiveBytes, bytes);
                numBytesLeft -= numBytes;
            }

            Assert.AreEqual(numBytesToSend, envelope.Length);
            this.SendMessage(envelope);
        }
    }
}
