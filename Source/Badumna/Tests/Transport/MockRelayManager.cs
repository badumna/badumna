﻿using System;
using System.Collections.Generic;
using System.Text;
using BadumnaTests.Core;
using Badumna.Transport;
using Badumna.Core;

namespace BadumnaTests.Transport
{
    [DontPerformCoverage]
    class MockRelayManager : IRelayManager
    {
#pragma warning disable 67  // The event 'xxx' is never used
        public event EventHandler<RelayMessageArrivalEventArgs> MessageArrivalEvent;
#pragma warning restore 67

        public void RelayMessage(TransportEnvelope payloadEnvelope, bool useReliableTransport)
        {
        }

        public void RelayMessage(TransportEnvelope payloadEnvelope, bool useReliableTransport, int redundancy)
        {
        }

        public void BuildRoutePath(PeerAddress destination)
        {
        }

        public void TearDownRoutePath(PeerAddress destination)
        {
        }
    }
}
