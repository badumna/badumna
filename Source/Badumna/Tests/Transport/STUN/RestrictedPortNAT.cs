﻿//-----------------------------------------------------------------------
// <copyright file="RestrictedPortNAT.cs" company="NICTA">
//     Copyright (c) 2011 NICTA. All right reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using Badumna.Core;

namespace BadumnaTests.STUN
{
    /// <summary>
    /// The RestrictedPort NAT type.
    /// </summary>
    [DontPerformCoverage]
    internal class RestrictedPortNAT : NATDevice
    {
        /// <summary>
        /// A list of allowed addresses that can receive from.
        /// </summary>
        private List<IPEndPoint> allowedAddress;

        /// <summary>
        /// Initializes a new instance of the <see cref="RestrictedPortNAT"/> class.
        /// </summary>
        public RestrictedPortNAT()
        {
            this.allowedAddress = new List<IPEndPoint>();
        }

        /// <summary>
        /// Whether inbound is allowed for specified source and destination condition..
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="destination">The destination.</param>
        /// <returns>Whether inbound allowed.</returns>
        protected override bool InboundAllowed(IPEndPoint source, IPEndPoint destination)
        {
            return this.allowedAddress.Contains(source);
        }

        /// <summary>
        /// Rewrites the port.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="destination">The destination.</param>
        /// <returns>The rewriteen port.</returns>
        protected override int RewritePort(IPEndPoint source, IPEndPoint destination)
        {
            return source.Port;
        }

        /// <summary>
        /// Receives the outbound packet.
        /// </summary>
        /// <param name="destination">The destination.</param>
        protected override void ReceiveOutboundPacket(IPEndPoint destination)
        {
            if (!this.allowedAddress.Contains(destination))
            {
                this.allowedAddress.Add(destination);
            }
        }
    }
}