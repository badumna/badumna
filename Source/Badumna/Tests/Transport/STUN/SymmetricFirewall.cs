﻿// <copyright file="SymmetricFirewall.cs" company="NICTA">
//     Copyright (c) 2011 NICTA. All right reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using Badumna.Core;

namespace BadumnaTests.STUN
{
    /// <summary>
    /// The SymmetricFirewall type, behind a SymmetricFirewall device, not behind any actual NAT.
    /// </summary>
    [DontPerformCoverage]
    internal class SymmetricFirewall : NATDevice
    {
        /// <summary>
        /// allowed list 1
        /// </summary>
        private List<IPEndPoint> allowedEndPoints;

        /// <summary>
        /// allowed list 2
        /// </summary>
        private List<IPEndPoint> allowedRemoteEndPoint;

        /// <summary>
        /// Initializes a new instance of the <see cref="SymmetricFirewall"/> class.
        /// </summary>
        public SymmetricFirewall()
            : this(NATDevice.DefaultPublicAddress)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SymmetricFirewall"/> class.
        /// </summary>
        /// <param name="publicAddress">The public address.</param>
        public SymmetricFirewall(IPEndPoint publicAddress)
            : base(publicAddress)
        {
            this.allowedEndPoints = new List<IPEndPoint>();
            this.allowedRemoteEndPoint = new List<IPEndPoint>();
        }

        /// <summary>
        /// Whether inbound is allowed for specified source and destination condition..
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="destination">The destination.</param>
        /// <returns>Whether inbound allowed.</returns>
        protected override bool InboundAllowed(IPEndPoint source, IPEndPoint destination)
        {
            if (this.allowedRemoteEndPoint.Contains(source) && this.allowedEndPoints.Contains(destination))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Rewrites the port.
        /// </summary>
        /// <param name="destination">The destination.</param>
        /// <returns>The rewriteen port.</returns>
        protected override int RewritePort(IPEndPoint source, IPEndPoint destination)
        {
            if (!this.allowedEndPoints.Contains(source))
            {
                this.allowedEndPoints.Add(source);
            }

            return source.Port;
        }

        /// <summary>
        /// Receives the outbound packet.
        /// </summary>
        /// <param name="destination">The destination.</param>
        protected override void ReceiveOutboundPacket(IPEndPoint destination)
        {
            if (!this.allowedRemoteEndPoint.Contains(destination))
            {
                this.allowedRemoteEndPoint.Add(destination);
            }
        }
    }
}