﻿//-----------------------------------------------------------------------
// <copyright file="MockedNAT.cs" company="NICTA">
//     Copyright (c) 2011 NICTA. All right reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Net;
using Badumna.Core;
using Badumna.Transport;

namespace BadumnaTests.STUN
{
    /// <summary>
    /// The mocked NAT device.
    /// </summary>
    [DontPerformCoverage]
    internal class MockedNAT : ISocketWrapper
    {
        /// <summary>
        /// The NAT device.
        /// </summary>
        private NATDevice nat;

        /// <summary>
        /// A STUN server.
        /// </summary>
        private STUNServerProxy server;

        /// <summary>
        /// Initializes a new instance of the <see cref="MockedNAT"/> class.
        /// </summary>
        /// <param name="device">The NAT device to use.</param>
        public MockedNAT(NATDevice device)
        {
            this.nat = device;
            this.server = new STUNServerProxy();
        }

        #region ISocketWrapper Members

        /// <summary>
        /// Send packet to.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="destination">The destination.</param>
        void ISocketWrapper.SendTo(byte[] message, IPEndPoint destination)
        {
            PacketDetails request = this.nat.SendTo(message, destination);
            if (request != null)
            {
                // not dropped.
                try
                {
                    PacketDetails response = this.server.ProcessRequest(request);
                    this.nat.AddReceivedPacket(response.Message, response.Source, response.Destination);
                }
                catch
                {
                    Console.Error.WriteLine("Exception caught when processing request.");
                }
            }
        }

        /// <summary>
        /// Receive a packet from.
        /// </summary>
        /// <param name="buffer">The buffer.</param>
        /// <param name="endpoint">The endpoint.</param>
        /// <param name="timeoutMilliseconds">The timeout milliseconds.</param>
        /// <returns>The number of bytes received.</returns>
        int ISocketWrapper.ReceiveFrom(byte[] buffer, ref EndPoint endpoint, int timeoutMilliseconds)
        {
            return this.nat.ReceiveFrom(buffer, ref endpoint, timeoutMilliseconds);
        }

        #endregion
    }
}