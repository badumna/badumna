﻿//-----------------------------------------------------------------------
// <copyright file="MockedNATFactory.cs" company="NICTA">
//     Copyright (c) 2011 NICTA. All right reserved.
// </copyright>
//-----------------------------------------------------------------------

using Badumna.Core;
using Badumna.Transport;

namespace BadumnaTests.STUN
{
    /// <summary>
    /// The mocked NAT factory class.
    /// </summary>
    internal class MockedNATFactory
    {
        /// <summary>
        /// Creates the specified mocked NAT.
        /// </summary>
        /// <param name="type">The nat type.</param>
        /// <returns>The mocked NAT</returns>
        public static ISocketWrapper Create(NatType type)
        {
            if (type == NatType.Blocked)
            {
                return new MockedNAT(new BlockedNAT());
            }
            else if (type == NatType.RestrictedCone)
            {
                return new MockedNAT(new RestrictedConeNAT());
            }
            else if (type == NatType.RestrictedPort)
            {
                return new MockedNAT(new RestrictedPortNAT());
            }
            else if (type == NatType.FullCone)
            {
                return new MockedNAT(new FullConeNAT());
            }
            else if (type == NatType.SymmetricNat)
            {
                return new MockedNAT(new SymmetricNAT());
            }
            else if (type == NatType.Open)
            {
                return new MockedNAT(new Open());
            }
            else if (type == NatType.SymmetricFirewall)
            {
                return new MockedNAT(new SymmetricFirewall());
            }
            else
            {
                throw new System.NotImplementedException(type + " not implemented");
            }
        }
    }
}