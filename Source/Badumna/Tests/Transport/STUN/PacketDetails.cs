﻿//-----------------------------------------------------------------------
// <copyright file="PacketDetails.cs" company="NICTA">
//     Copyright (c) 2011 NICTA. All right reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.Net;
using Badumna.Core;

namespace BadumnaTests.STUN
{
    /// <summary>
    /// The packet details.
    /// </summary>
    [DontPerformCoverage]
    internal class PacketDetails
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PacketDetails"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="source">The source.</param>
        /// <param name="destination">The destination.</param>
        public PacketDetails(byte[] message, IPEndPoint source, IPEndPoint destination)
        {
            this.Message = message;
            this.Source = source;
            this.Destination = destination;
        }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>The message.</value>
        public byte[] Message
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the source.
        /// </summary>
        /// <value>The source.</value>
        public IPEndPoint Source
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the destination.
        /// </summary>
        /// <value>The destination.</value>
        public IPEndPoint Destination
        {
            get;
            private set;
        }
    }
}