﻿//-----------------------------------------------------------------------
// <copyright file="FullConeNAT.cs" company="NICTA">
//     Copyright (c) 2011 NICTA. All right reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.Net;
using Badumna.Core;

namespace BadumnaTests.STUN
{
    /// <summary>
    /// The FullCone NAT type.
    /// </summary>
    [DontPerformCoverage]
    internal class FullConeNAT : NATDevice
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FullConeNAT"/> class.
        /// </summary>
        public FullConeNAT()
        {
        }

        /// <summary>
        /// Whether inbound is allowed for specified source and destination condition..
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="destination">The destination.</param>
        /// <returns>Whether inbound allowed.</returns>
        protected override bool InboundAllowed(IPEndPoint source, IPEndPoint destination)
        {
            return true;
        }

        /// <summary>
        /// Rewrites the port.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="destination">The destination.</param>
        /// <returns>The rewriteen port.</returns>
        protected override int RewritePort(IPEndPoint source, IPEndPoint destination)
        {
            return source.Port;
        }

        /// <summary>
        /// Receives the outbound packet.
        /// </summary>
        /// <param name="destination">The destination.</param>
        protected override void ReceiveOutboundPacket(IPEndPoint destination)
        {
            return;    
        }
    }
}