﻿//-----------------------------------------------------------------------
// <copyright file="STUNServerProxy.cs" company="NICTA">
//     Copyright (c) 2011 NICTA. All right reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Net;

using Badumna.Core;

namespace BadumnaTests.STUN
{
    /// <summary>
    /// A stun server class.
    /// </summary>
    [DontPerformCoverage]
    internal class STUNServerProxy
    {
        private const Int16 mBindAddressRequest = 0x0001;
        private const Int16 mBindAddressResponse = 0x0101;

        private const Int16 mMappedAddress = 0x0001;
        private const Int16 mResponseAddress = 0x0002;
        private const Int16 mChangeRequest = 0x0003;
        private const Int16 mSourceAddress = 0x0004;
        private const Int16 mChangedAddress = 0x0005;

        private const Int32 mNoRequest = 0x0000000;
        private const Int32 mChangeAddress = 0x0000005; 
        private const Int32 mChangePort = 0x0000002;

        /// <summary>
        /// Initializes a new instance of the <see cref="STUNServerProxy"/> class.
        /// </summary>
        public STUNServerProxy()
        {
        }

        /// <summary>
        /// Gets the server address.
        /// </summary>
        /// <value>The server address.</value>
        public static IPEndPoint ServerAddress
        {
            get
            {
                return new IPEndPoint(new IPAddress(new byte[] { 202, 96, 1, 1}), 1234);
            }
        }

        /// <summary>
        /// Gets the second server address.
        /// </summary>
        /// <value>The second server address.</value>
        public static IPEndPoint SecondServerAddress
        {
            get
            {
                return new IPEndPoint(new IPAddress(new byte[] { 202, 96, 1, 2 }), 1234);
            }
        }

        /// <summary>
        /// Processes the request.
        /// </summary>
        /// <param name="request">The request packet.</param>
        /// <returns>The response packet.</returns>
        public PacketDetails ProcessRequest(PacketDetails packet)
        {
            MessageBuffer message = new MessageBuffer(packet.Message);
            bool changeAddress = false;
            bool changePort = false;
            IPEndPoint responseAddress = null;

            Int16 type = this.ReadInt16FromMessage(message);
            Int16 len = this.ReadInt16FromMessage(message);
            byte[] id = message.ReadBytes(16);

            while (message.ReadOffset < message.Length)
            {
                Int16 request = this.ReadInt16FromMessage(message);
                if (request == STUNServerProxy.mChangeRequest)
                {
                    Int16 length = this.ReadInt16FromMessage(message);
                    Int32 value = this.ReadInt32FromMessage(message);

                    if ((value & (Int32)STUNServerProxy.mChangeAddress) == STUNServerProxy.mChangeAddress)
                    {
                        changeAddress = true;
                    }

                    if ((value & (Int32)STUNServerProxy.mChangePort) == STUNServerProxy.mChangePort)
                    {
                        changePort = true;
                    }
                }
                else if (request == STUNServerProxy.mResponseAddress)
                {
                    Int16 length = this.ReadInt16FromMessage(message);

                    Int16 addressFamily = this.ReadInt16FromMessage(message);
                    UInt16 port = this.ReadUInt16FromMessage(message);
                    byte[] address = this.ReadBytesFromMessage(message, 4);

                    Array.Reverse(address);

                    responseAddress = new IPEndPoint(new IPAddress(address), port);
                }
            }

            if (responseAddress == null)
            {
                responseAddress = packet.Source;
            }

            IPAddress sourceAddress = null;
            if (changeAddress)
            {
                if (packet.Destination.Address.Equals(STUNServerProxy.ServerAddress.Address))
                {
                    sourceAddress = STUNServerProxy.SecondServerAddress.Address;
                }
                else
                {
                    sourceAddress = STUNServerProxy.ServerAddress.Address;
                }
            }
            else
            {
                sourceAddress = packet.Destination.Address;
            }

            int sourcePort = STUNServerProxy.ServerAddress.Port;
            if (changePort)
            {
                sourcePort++;
            }

            return this.GetResponse(id, new IPEndPoint(sourceAddress, sourcePort), responseAddress, packet.Source);
        }

        /// <summary>
        /// Gets the response.
        /// </summary>
        /// <param name="id">The random id.</param>
        /// <param name="source">The source address.</param>
        /// <param name="destination">The destination address.</param>
        /// <param name="senderSource">The sender source.</param>
        /// <returns>The response.</returns>
        private PacketDetails GetResponse(byte[] id, IPEndPoint source, IPEndPoint destination, IPEndPoint senderSource)
        {
            MessageBuffer message = new MessageBuffer();
            this.WriteTypeToMessage(message, STUNServerProxy.mBindAddressResponse);
            this.WriteLengthToMessage(message, 0x0000);
            this.WriteIDToMessage(message, id);

            this.WriteAddressToMessage(message, STUNServerProxy.SecondServerAddress, STUNServerProxy.mChangedAddress);
            this.WriteAddressToMessage(message, senderSource, STUNServerProxy.mMappedAddress);

            return new PacketDetails(message.GetBuffer(), source, destination);
        }

        /// <summary>
        /// Reads the int16 from message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        private Int16 ReadInt16FromMessage(MessageBuffer message)
        {
            this.CheckLength(message, 2);
            byte[] bytes = message.ReadBytes(2);

            Array.Reverse(bytes); // to host order.

            return BitConverter.ToInt16(bytes, 0);
        }

        /// <summary>
        /// Reads the int32 from message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        private Int32 ReadInt32FromMessage(MessageBuffer message)
        {
            this.CheckLength(message, 4);
            byte[] bytes = message.ReadBytes(4);

            Array.Reverse(bytes); // to host order.

            return BitConverter.ToInt32(bytes, 0);
        }

        /// <summary>
        /// Reads the U int16 from message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        private UInt16 ReadUInt16FromMessage(MessageBuffer message)
        {
            this.CheckLength(message, 2);
            byte[] bytes = message.ReadBytes(2);

            Array.Reverse(bytes); // to host order.

            return BitConverter.ToUInt16(bytes, 0);
        }

        /// <summary>
        /// Reads the bytes from message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="length">The length.</param>
        /// <returns></returns>
        private byte[] ReadBytesFromMessage(MessageBuffer message, int length)
        {
            this.CheckLength(message, length);
            byte[] bytes = message.ReadBytes(length);

            Array.Reverse(bytes); // to host order.

            return bytes;
        }

        /// <summary>
        /// Writes the type to message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="value">The value.</param>
        private void WriteTypeToMessage(MessageBuffer message, Int16 value)
        {
            byte[] bytes = BitConverter.GetBytes(value);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            message.WriteOffset = 0;
            message.Write(bytes, bytes.Length);
        }

        /// <summary>
        /// Writes the length to message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="value">The value.</param>
        private void WriteLengthToMessage(MessageBuffer message, Int16 value)
        {
            byte[] bytes = BitConverter.GetBytes(value);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            message.WriteOffset = 2;
            message.Write(bytes, bytes.Length);
        }

        /// <summary>
        /// Writes the ID to message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="id">The id.</param>
        private void WriteIDToMessage(MessageBuffer message, byte[] id)
        {
            message.WriteOffset = 4;
            message.Write(id, id.Length);
        }

        /// <summary>
        /// Writes the address to message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="address">The address.</param>
        /// <param name="type">The address type.</param>
        private void WriteAddressToMessage(MessageBuffer message, IPEndPoint address, short type)
        {
            byte[] familyBytes = BitConverter.GetBytes((UInt16)1);
            byte[] portBytes = BitConverter.GetBytes((UInt16)address.Port);
            byte[] addressBytes = address.Address.GetAddressBytes();
            byte[] t = BitConverter.GetBytes(type);
            byte[] len = BitConverter.GetBytes((Int16)(familyBytes.Length + portBytes.Length + addressBytes.Length));

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(t);
                Array.Reverse(len);
                Array.Reverse(familyBytes);
                Array.Reverse(portBytes);
            }

            message.WriteOffset = message.Length;
            message.Write(t, t.Length);
            message.Write(len, len.Length);
            message.Write(familyBytes, familyBytes.Length);
            message.Write(portBytes, portBytes.Length);
            message.Write(addressBytes, addressBytes.Length);   
        }

        /// <summary>
        /// Checks the length.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="length">The length.</param>
        private void CheckLength(MessageBuffer message, int length)
        {
            if ((message.Length - message.ReadOffset) < length)
            {
                throw new Exception("Not enough bytes.");
            }
        }
    }
}