﻿//-----------------------------------------------------------------------
// <copyright file="Open.cs" company="NICTA">
//     Copyright (c) 2011 NICTA. All right reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using Badumna.Core;

namespace BadumnaTests.STUN
{
    /// <summary>
    /// The open type, not behind any nat.
    /// </summary>
    [DontPerformCoverage]
    internal class Open : NATDevice
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Open"/> class.
        /// </summary>
        public Open()
            : this(NATDevice.DefaultPublicAddress)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Open"/> class.
        /// </summary>
        /// <param name="publicAddress">The public address.</param>
        public Open(IPEndPoint publicAddress)
            : base(publicAddress)
        {
        }

        /// <summary>
        /// Whether inbound is allowed for specified source and destination condition..
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="destination">The destination.</param>
        /// <returns>Whether inbound allowed.</returns>
        protected override bool InboundAllowed(IPEndPoint source, IPEndPoint destination)
        {
            return true;
        }

        /// <summary>
        /// Rewrites the port.
        /// </summary>
        /// <param name="destination">The destination.</param>
        /// <returns>The rewriteen port.</returns>
        protected override int RewritePort(IPEndPoint source, IPEndPoint destination)
        {
            return source.Port;
        }

        /// <summary>
        /// Receives the outbound packet.
        /// </summary>
        /// <param name="destination">The destination.</param>
        protected override void ReceiveOutboundPacket(IPEndPoint destination)
        {
        }
    }
}