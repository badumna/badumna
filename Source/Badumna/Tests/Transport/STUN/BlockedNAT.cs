﻿//-----------------------------------------------------------------------
// <copyright file="BlockedNAT.cs" company="NICTA">
//     Copyright (c) 2011 NICTA. All right reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using Badumna.Core;

namespace BadumnaTests.STUN
{
    /// <summary>
    /// The blocked NAT type.
    /// </summary>
    [DontPerformCoverage]
    internal class BlockedNAT : NATDevice
    {
        /// <summary>
        /// Whether inbound is allowed for specified source and destination condition..
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="destination">The destination.</param>
        /// <returns>Whether inbound allowed.</returns>
        protected override bool InboundAllowed(IPEndPoint source, IPEndPoint destination)
        {
            return false;
        }

        /// <summary>
        /// Rewrites the port.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="destination">The destination.</param>
        /// <returns>The rewriteen port.</returns>
        protected override int RewritePort(IPEndPoint source, IPEndPoint destination)
        {
            return 0;
        }

        /// <summary>
        /// Whether outbound message is allowed.
        /// </summary>
        /// <param name="destination"></param>
        /// <returns>Whether the outbound is allowed.</returns>
        protected override bool OutboundAllowed(IPEndPoint destination)
        {
            return false;
        }

        /// <summary>
        /// Receives the outbound packet.
        /// </summary>
        /// <param name="destination">The destination.</param>
        protected override void ReceiveOutboundPacket(IPEndPoint destination)
        {
            return;
        }
    }
}