﻿//-----------------------------------------------------------------------
// <copyright file="SymmetricNAT.cs" company="NICTA">
//     Copyright (c) 2011 NICTA. All right reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using Badumna.Core;

namespace BadumnaTests.STUN
{
    /// <summary>
    /// The Symmetric NAT type.
    /// </summary>
    [DontPerformCoverage]
    internal class SymmetricNAT : NATDevice
    {
        private Dictionary<int, IPEndPoint> portMapping;

        /// <summary>
        /// current max mapping port.
        /// </summary>
        private int maxMapping;

        /// <summary>
        /// Initializes a new instance of the <see cref="SymmetricNAT"/> class.
        /// </summary>
        public SymmetricNAT()
        {
            this.maxMapping = 1025;
            this.portMapping = new Dictionary<int, IPEndPoint>();
        }

        /// <summary>
        /// Whether inbound is allowed for specified source and destination condition..
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="destination">The destination.</param>
        /// <returns>Whether inbound allowed.</returns>
        protected override bool InboundAllowed(IPEndPoint source, IPEndPoint destination)
        {
            foreach (KeyValuePair<int, IPEndPoint> kvp in this.portMapping)
            {
                if (kvp.Value.Equals(source))
                {
                    return destination.Port == kvp.Key;
                }
            }

            return false;
        }

        /// <summary>
        /// Rewrites the port.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="destination">The destination.</param>
        /// <returns>The rewriteen port.</returns>
        protected override int RewritePort(IPEndPoint source, IPEndPoint destination)
        {
            foreach (KeyValuePair<int, IPEndPoint> kvp in this.portMapping)
            {
                if (kvp.Value.Equals(destination))
                {
                    return kvp.Key;
                }
            }

            this.maxMapping++;
            this.portMapping.Add(this.maxMapping, destination);
            return this.maxMapping;
        }

        /// <summary>
        /// Receives the outbound packet.
        /// </summary>
        /// <param name="destination">The destination.</param>
        protected override void ReceiveOutboundPacket(IPEndPoint destination)
        {
        }
    }
}