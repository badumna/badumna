﻿//-----------------------------------------------------------------------
// <copyright file="NATDevice.cs" company="NICTA">
//     Copyright (c) 2011 NICTA. All right reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using Badumna.Core;

namespace BadumnaTests.STUN
{
    /// <summary>
    /// The NAT device base class.
    /// A NAT device is a mocked PC attached to a NAT box. The NAT will be assigned a public IP and the PC will be
    /// assigned a private IP and a binded port. 
    /// </summary>
    [DontPerformCoverage]
    internal abstract class NATDevice
    {
        /// <summary>
        /// The public address.
        /// </summary>
        private IPAddress publicAddress;

        /// <summary>
        /// The private address.
        /// </summary>
        private IPEndPoint privateEndPoint;

        /// <summary>
        /// The message queue.
        /// </summary>
        private Queue<PacketDetails> messageQueue;

        /// <summary>
        /// Initializes a new instance of the <see cref="NATDevice"/> class.
        /// </summary>
        public NATDevice()
            : this(NATDevice.DefaultPublicAddress.Address, NATDevice.DefaultPrivateAddress)
        {         
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NATDevice"/> class.
        /// </summary>
        /// <param name="publicAddress">The public address.</param>
        public NATDevice(IPEndPoint publicAddress)
            : this(publicAddress.Address, publicAddress)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NATDevice"/> class.
        /// </summary>
        /// <param name="publicAddress">The public address.</param>
        public NATDevice(IPAddress publicAddress, IPEndPoint privateEndPoint)
        {
            this.publicAddress = publicAddress;
            this.privateEndPoint = privateEndPoint;
            this.messageQueue = new Queue<PacketDetails>();
        }

        /// <summary>
        /// Gets the default address.
        /// </summary>
        /// <value>The default address.</value>
        public static IPEndPoint DefaultPublicAddress
        {
            get { return new IPEndPoint(new IPAddress(new byte[] { 202, 96, 100, 1 }), 2345); }
        }

        /// <summary>
        /// Gets the default private address.
        /// </summary>
        /// <value>The default private address.</value>
        public static IPEndPoint DefaultPrivateAddress
        {
            get { return new IPEndPoint(new IPAddress(new byte[] { 192, 168, 1, 1 }), 2345); }
        }

        /// <summary>
        /// Sends a message to.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="destination">The destination.</param>
        public PacketDetails SendTo(byte[] message, IPEndPoint destination)
        {
            // this can potentically update the port mapping. 
            this.ReceiveOutboundPacket(destination);

            if (this.OutboundAllowed(destination))
            {
                int port = this.RewritePort(this.privateEndPoint, destination);
                IPEndPoint updatedSource = new IPEndPoint(this.publicAddress, port);
                return new PacketDetails(message, updatedSource, destination);
            }

            return null;
        }

        /// <summary>
        /// Tries to get a message.
        /// </summary>
        /// <param name="buffer">The buffer.</param>
        /// <param name="endpoint">The endpoint.</param>
        /// <param name="timeoutMilliseconds">The timeout milliseconds.</param>
        /// <returns>The number of bytes read.</returns>
        public int ReceiveFrom(byte[] buffer, ref EndPoint endpoint, int timeoutMilliseconds)
        {
            if (this.messageQueue.Count > 0)
            {
                PacketDetails details = this.messageQueue.Dequeue();
                Array.Copy(details.Message, buffer, details.Message.Length);
                endpoint = details.Source;

                return details.Message.Length;
            }
            else
            {
                throw new SocketException();
            }
        }

        /// <summary>
        /// Adds the received packet.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="source">The source.</param>
        /// <param name="destination">The destination.</param>
        public void AddReceivedPacket(byte[] message, IPEndPoint source, IPEndPoint destination)
        {
            if (this.InboundAllowed(source, destination))
            {
                this.messageQueue.Enqueue(new PacketDetails(message, source, destination));
            }
            else
            {
                // message silently dropped by the NAT.
            }
        }

        /// <summary>
        /// Whether outbound message is allowed.
        /// </summary>
        /// <returns>Whether the outbound is allowed.</returns>
        protected virtual bool OutboundAllowed(IPEndPoint destination)
        {
            return true;
        }

        /// <summary>
        /// Whether inbound is allowed for specified source and destination condition..
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="destination">The destination.</param>
        /// <returns>Whether inbound allowed.</returns>
        protected abstract bool InboundAllowed(IPEndPoint source, IPEndPoint destination);

        /// <summary>
        /// Rewrites the port.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="destination">The destination.</param>
        /// <returns>The rewriteen port.</returns>
        protected abstract int RewritePort(IPEndPoint source, IPEndPoint destination);

        /// <summary>
        /// Receives the outbound packet.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="destination">The destination.</param>
        protected abstract void ReceiveOutboundPacket(IPEndPoint destination);
    }
}