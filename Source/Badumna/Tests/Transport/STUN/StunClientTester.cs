﻿//---------------------------------------------------------------------------------
// <copyright file="StunClientTester.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace BadumnaTests.STUN
{
    using System.Collections.Generic;
    using System.Net;
    using Badumna.Core;
    using Badumna.Transport;
    using NUnit.Framework;

    [TestFixture]
    [DontPerformCoverage]
    public class StunClientTester
    {
        /// <summary>
        /// The stun client.
        /// </summary>
        private StunClient client;

        /// <summary>
        /// The open peer address.
        /// </summary>
        private List<PeerAddress> openPeerAddresses;

        /// <summary>
        /// The non open peer address.
        /// </summary>
        private List<PeerAddress> nonOpenPeerAddresses;

        [SetUp]
        public void SetUp()
        {
            PeerAddress a1 = new PeerAddress(NATDevice.DefaultPublicAddress, NatType.Open);
            this.openPeerAddresses = new List<PeerAddress>();
            this.openPeerAddresses.Add(a1);

            PeerAddress a2 = new PeerAddress(NATDevice.DefaultPrivateAddress, NatType.Internal);
            this.nonOpenPeerAddresses = new List<PeerAddress>();
            this.nonOpenPeerAddresses.Add(a2);
        }

        // The internal/public accessibility issue stopped me from using TestCases to test on different NatTypes, thus
        // had to put them in separate methods. Test cases are used below to test on different supplied expectedPorts. 
        // Port -1 means expected public port is unknown, 2345 is the right port, 65000 is just a random wrong port 
        // number.
        [Test]
        [TestCase(-1)] 
        [TestCase(2345)]
        [TestCase(65000)]
        public void BlockedNATCanBeIdentified(int port)
        {
            ISocketWrapper wrapper = MockedNATFactory.Create(NatType.Blocked);
            this.client = new StunClient(wrapper, this.nonOpenPeerAddresses, port);
            this.client.InitiateRequest(STUNServerProxy.ServerAddress);

            Assert.AreEqual(NatType.Blocked, this.client.NatType);
        }

        [Test]
        [TestCase(-1)]
        [TestCase(2345)]
        [TestCase(65000)]
        public void RestrictConeNATCanBeIdentified(int port)
        {
            ISocketWrapper wrapper = MockedNATFactory.Create(NatType.RestrictedCone);
            this.client = new StunClient(wrapper, this.nonOpenPeerAddresses, port);
            this.client.InitiateRequest(STUNServerProxy.ServerAddress);

            Assert.AreEqual(NatType.RestrictedCone, this.client.NatType);
        }

        [Test]
        [TestCase(-1)]
        [TestCase(2345)]
        [TestCase(65000)]
        public void RestrictPortNATCanBeIdentified(int port)
        {
            ISocketWrapper wrapper = MockedNATFactory.Create(NatType.RestrictedPort);
            this.client = new StunClient(wrapper, this.nonOpenPeerAddresses, port);
            this.client.InitiateRequest(STUNServerProxy.ServerAddress);

            Assert.AreEqual(NatType.RestrictedPort, this.client.NatType);
        }

        [Test]
        [TestCase(-1)]
        [TestCase(2345)]
        [TestCase(65000)]
        public void FullConeNATCanBeIdentified(int port)
        {
            ISocketWrapper wrapper = MockedNATFactory.Create(NatType.FullCone);
            this.client = new StunClient(wrapper, this.nonOpenPeerAddresses, port);
            this.client.InitiateRequest(STUNServerProxy.ServerAddress);

            Assert.AreEqual(NatType.FullCone, this.client.NatType);
        }

        [Test]
        [TestCase(-1)]
        [TestCase(2345)]
        [TestCase(65000)]
        public void SymmetricNATCanBeIdentified(int port)
        {
            ISocketWrapper wrapper = MockedNATFactory.Create(NatType.SymmetricNat);
            this.client = new StunClient(wrapper, this.nonOpenPeerAddresses, port);
            this.client.InitiateRequest(STUNServerProxy.ServerAddress);

            Assert.AreEqual(NatType.SymmetricNat, this.client.NatType);
        }

        [Test]
        [TestCase(-1)]
        [TestCase(2345)]
        [TestCase(65000)]
        public void OpenTypeCanBeIdentified(int port)
        {
            ISocketWrapper wrapper = MockedNATFactory.Create(NatType.Open);
            this.client = new StunClient(wrapper, this.openPeerAddresses, port);
            this.client.InitiateRequest(STUNServerProxy.ServerAddress);

            Assert.AreEqual(NatType.Open, this.client.NatType);
        }

        [Test]
        [TestCase(-1)]
        [TestCase(2345)]
        [TestCase(65000)]
        public void SymmetricFirewallCanBeIdentified(int port)
        {
            ISocketWrapper wrapper = MockedNATFactory.Create(NatType.SymmetricFirewall);
            this.client = new StunClient(wrapper, this.openPeerAddresses, port);
            this.client.InitiateRequest(STUNServerProxy.ServerAddress);

            Assert.AreEqual(NatType.SymmetricFirewall, this.client.NatType);
        }
    }
}