﻿//-----------------------------------------------------------------------
// <copyright file="RestrictedConeNAT.cs" company="NICTA">
//     Copyright (c) 2011 NICTA. All right reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using Badumna.Core;

namespace BadumnaTests.STUN
{
    /// <summary>
    /// The RestrictedCone NAT type.
    /// </summary>
    [DontPerformCoverage]
    internal class RestrictedConeNAT : NATDevice
    {
        /// <summary>
        /// A list of allowed addresses that can receive from.
        /// </summary>
        private List<IPAddress> allowedAddress;

        /// <summary>
        /// Initializes a new instance of the <see cref="RestrictedConeNAT"/> class.
        /// </summary>
        public RestrictedConeNAT()
        {
            this.allowedAddress = new List<IPAddress>();
        }

        /// <summary>
        /// Whether inbound is allowed for specified source and destination condition..
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="destination">The destination.</param>
        /// <returns>Whether inbound allowed.</returns>
        protected override bool InboundAllowed(IPEndPoint source, IPEndPoint destination)
        {
            return this.allowedAddress.Contains(source.Address);
        }

        /// <summary>
        /// Rewrites the port.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="destination">The destination.</param>
        /// <returns>The rewriteen port.</returns>
        protected override int RewritePort(IPEndPoint source, IPEndPoint destination)
        {
            return source.Port;
        }

        /// <summary>
        /// Receives the outbound packet.
        /// </summary>
        /// <param name="destination">The destination.</param>
        protected override void ReceiveOutboundPacket(IPEndPoint destination)
        {
            if (!this.allowedAddress.Contains(destination.Address))
            {
                this.allowedAddress.Add(destination.Address);
            }
        }
    }
}