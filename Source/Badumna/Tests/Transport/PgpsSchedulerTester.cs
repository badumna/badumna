﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Badumna.Utilities;
using Badumna.Core;
using Badumna.Transport;

namespace BadumnaTests.Transport
{
    class NamedBlock : Block
    {
        private int mId;
        public int Id { get { return this.mId; } }

        public NamedBlock(int id, int length)
            : base(length)
        {
            this.mId = id;
        }
    }

    [TestFixture]
    public class PgpsSchedulerTester
    {
        private struct SourceDescription
        {
            public int BlockLengthBytes;
            public double Weight;

            public SourceDescription(int blockLengthBytes, double weight)
            {
                this.BlockLengthBytes = blockLengthBytes;
                this.Weight = weight;
            }
        }

        private Simulator eventQueue;

        [SetUp]
        public void SetUp()
        {
            this.eventQueue = new Simulator();
        }

        private void RunFullActivityTest(double runTimeSeconds, double rateBytesPerSecond, params SourceDescription[] sourceDescriptions)
        {
            int[] blockCounts = new int[sourceDescriptions.Length];

            ITime timeKeeper = this.eventQueue;

            PgpsScheduler<NamedBlock> pgps = new PgpsScheduler<NamedBlock>(timeKeeper);
            RateLimiter<NamedBlock> limiter = new RateLimiter<NamedBlock>(
                delegate { return rateBytesPerSecond; },
                pgps,
                delegate(NamedBlock block)
                {
                    blockCounts[block.Id]++;
                    return block.Length;
                },
                this.eventQueue,
                timeKeeper);


            List<SimpleChannel<NamedBlock>> channels = new List<SimpleChannel<NamedBlock>>();
            int channelId = 0;
            double totalWeight = 0;
            foreach (SourceDescription sourceDescription in sourceDescriptions)
            {
                SimpleChannel<NamedBlock> channel = new SimpleChannel<NamedBlock>(timeKeeper);
                pgps.AddInputSource(channel, sourceDescription.Weight);
                totalWeight += sourceDescription.Weight;
                for (int i = 0; i < 1000; i++)
                {
                    channel.Put(new NamedBlock(channelId, sourceDescription.BlockLengthBytes));
                }
                channelId++;
            }

            for (int i = 0; i < channels.Count; i++)
            {
                Assert.AreEqual(i == 0 ? 1 : 0, blockCounts[i]);
            }

            this.eventQueue.RunFor(TimeSpan.FromSeconds(runTimeSeconds));

            for (int i = 0; i < sourceDescriptions.Length; i++)
            {
                int expected = (int)Math.Floor(runTimeSeconds * rateBytesPerSecond * sourceDescriptions[i].Weight /
                    (sourceDescriptions[i].BlockLengthBytes * totalWeight));

                //Console.WriteLine("Channel {0}: expected {1}, got {2}", i, expected, blockCounts[i]);

                Assert.GreaterOrEqual(blockCounts[i], 0.97 * expected, "Channel " + i.ToString() + " count");
                Assert.LessOrEqual(blockCounts[i], 1.03 * expected, "Channel " + i.ToString() + " count");
            }
        }


        // Test that single channel goes at full rate
        [Test]
        public void SingleChannelTest()
        {
            this.RunFullActivityTest(10, 512, new SourceDescription(13, 1.0));
        }

        [Test, Ignore]
        public void SporadicSingleChannelTest()
        {
        }

        [Test, Ignore]
        public void ShortPacketTest()
        {
            // Send lots of tiny packets to make sure the clock resolution is OK
        }

        [Test, Ignore]
        public void SlowRateTest()
        {
            // Less than 1 byte / second
            this.RunFullActivityTest(500, 0.5, new SourceDescription(13, 1.0), new SourceDescription(19, 1.0));
        }

        [Test]
        public void TwoChannelEvenWeightsTest()
        {
            this.RunFullActivityTest(10, 512, new SourceDescription(13, 1.0), new SourceDescription(19, 1.0));
        }

        [Test, Ignore]
        public void TwoChannelUnevenWeightsTest()
        {
        }

        [Test, Ignore]
        public void MultiChannelSingleActiveTest()
        {
        }

        [Test, Ignore]
        public void BoundedDelayTest()
        {
        }
    }
}
