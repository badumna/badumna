﻿//-----------------------------------------------------------------------
// <copyright file="ConnectionTester.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Badumna;
using Badumna.Core;
using Badumna.Security;
using Badumna.Transport;
using Badumna.Utilities;
using NUnit.Framework;

namespace BadumnaTests.Transport.Connections
{
    [TestFixture]
    [DontPerformCoverage]
    public class ConnectionTester
    {
        public ConnectionTester()
        {
        }

        /// <summary>
        /// Test that a connection can be formed between two peers.
        /// </summary>
        [Test]
        public void ConnectionTest()
        {
            var eventQueue = new Simulator();
            var encryptionQueue = eventQueue;
            IList<TestTransportPeer> peers = this.CreateAndForgeTestTransportPeerPair(eventQueue, encryptionQueue);
            this.EstablishConnection(peers[0], peers[1], eventQueue, encryptionQueue);
        }

        /// <summary>
        /// Test that a connection can be formed between two peers when both peers start connection
        /// establishment at the same time.
        /// </summary>
        [Test]
        public void SimultaneousConnectionTest()
        {
            var eventQueue = new Simulator();
            var encryptionQueue = eventQueue;
            IList<TestTransportPeer> peers = this.CreateAndForgeTestTransportPeerPair(eventQueue, encryptionQueue);
            this.EstablishConnectionSimultaneously(peers[0], peers[1], eventQueue, encryptionQueue);
        }

        /// <summary>
        /// Test that the NAT type of a peer becomes known after connecting to the peer if it was
        /// initially unknown.
        /// </summary>
        [Test]
        public void NatTypeSharedOnConnectionTest()
        {
            var eventQueue = new Simulator();
            var encryptionQueue = eventQueue;

            IList<TestTransportPeer> peers = new List<TestTransportPeer>
            {
                new TestTransportPeer(1, NatType.Open, eventQueue, encryptionQueue),
                new TestTransportPeer(2, NatType.FullCone, eventQueue, encryptionQueue)
            };

            // Initialize the Connections with unknown destination NAT types
            PeerAddress peer0Address = new PeerAddress(peers[0].AddressProvider.PublicAddress.EndPoint, NatType.Unknown);
            PeerAddress peer1Address = new PeerAddress(peers[1].AddressProvider.PublicAddress.EndPoint, NatType.Unknown);

            peers[0].CreateConnection(peer1Address, peers[1].Id);
            peers[1].CreateConnection(peer0Address, peers[0].Id);

            foreach (TestPeer peer in peers)
            {
                peer.Forge();
            }

            this.JoinWithRealiability(peers[0], peers[1], 1.0);

            peers[0].ConnectionTable.Connection.RoundTripTimeEstimate = 100;
            peers[1].ConnectionTable.Connection.RoundTripTimeEstimate = 100;

            Assert.AreEqual(NatType.Unknown, peers[0].ConnectionTable.Connection.PeersPublicAddress.NatType);
            Assert.AreEqual(NatType.Unknown, peers[1].ConnectionTable.Connection.PeersPublicAddress.NatType);

            peers[0].ConnectionTable.Connection.BeginInitialization();

            eventQueue.RunUntil(
                () => peers[0].ConnectionTable.Connection.IsConnected && peers[1].ConnectionTable.Connection.IsConnected,
                TimeSpan.FromSeconds(10),
                () => Assert.Fail("Connection failed"));

            // Check that the NAT type of the neighbour is shared.
            Assert.AreEqual(NatType.FullCone, peers[0].ConnectionTable.Connection.PeersPublicAddress.NatType);
            Assert.AreEqual(NatType.Open, peers[1].ConnectionTable.Connection.PeersPublicAddress.NatType);
        }

        /// <summary>
        /// Test that a connection is still formed successfully if some of the InitialMessage
        /// packets are dropped.
        /// </summary>
        [Test]
        public void DroppedInitialMessageTest()
        {
            var eventQueue = new Simulator();
            var encryptionQueue = eventQueue;

            IList<TestTransportPeer> peers = this.CreateAndForgeTestTransportPeerPair(eventQueue, encryptionQueue);

            peers[0].Transport.ModifyLink(peers[1].Transport, 100, 0.0, 64);
            peers[1].Transport.ModifyLink(peers[0].Transport, 100, 1.0, 64);

            peers[0].ConnectionTable.Connection.BeginInitialization();

            // Send some initial messages.
            eventQueue.RunUntil(
                () => peers[0].Transport.NumSentMessages >= 3,
                TimeSpan.FromSeconds(5),
                () => Assert.Fail("Inital messages not sent"));

            // Check that it fails to receive.
            Assert.AreEqual(0, peers[1].Transport.NumReceivedMessages);

            Assert.IsTrue(
                !peers[0].ConnectionTable.Connection.IsConnected && !peers[1].ConnectionTable.Connection.IsConnected,
                "Peers connected without receiving messages");

            // Change reliablity of transport.
            peers[0].Transport.ModifyLink(peers[1].Transport, 0, 1.0, 64);

            eventQueue.RunUntil(
                () => peers[0].ConnectionTable.Connection.IsConnected &&
                      peers[1].ConnectionTable.Connection.IsConnected,
                TimeSpan.FromSeconds(10),
                () => Assert.Fail("Failed to connect"));
        }

        /// <summary>
        /// Test that trying to instanstiate a connection with a null connection table fails with exception.
        /// </summary>
        [Test, ExpectedException(typeof(ArgumentNullException))]
        public void ConnectionTableNullTest()
        {
            var eventQueue = new Simulator();
            var encryptionQueue = eventQueue; 
            ITime timeKeeper = eventQueue;
            IList<TestTransportPeer> peers = this.CreateAndForgeTestTransportPeerPair(eventQueue, encryptionQueue);
            new Connection(peers[1].AddressProvider.PublicAddress, peers[0].Transport, null, eventQueue, encryptionQueue, timeKeeper, peers[0].AddressProvider, peers[0].ConnectivityReporter, new TokenCache(eventQueue, timeKeeper, peers[0].ConnectivityReporter));
        }

        /// <summary>
        /// Test that trying to initialize with invalid PeerAddress fails with exception.
        /// </summary>
        [Test, ExpectedException(typeof(ArgumentException))]
        public void InvalidPeerAdddressTest()
        {
            var eventQueue = new Simulator();
            var encryptionQueue = eventQueue;
            ITime timeKeeper = eventQueue;
            IList<TestTransportPeer> peers = this.CreateAndForgeTestTransportPeerPair(eventQueue, encryptionQueue);
            PeerAddress invalidAddress = new PeerAddress();
            new Connection(invalidAddress, peers[0].Transport, peers[0].ConnectionTable, eventQueue, encryptionQueue, timeKeeper, peers[0].AddressProvider, peers[0].ConnectivityReporter, new TokenCache(eventQueue, timeKeeper, peers[0].ConnectivityReporter));
        }

        /// <summary>
        /// Test that trying to initialize with a null dispatcher fails with exception.
        /// </summary>
        [Test, ExpectedException(typeof(ArgumentNullException))]
        public void DispatcherNullTest()
        {
            var eventQueue = new Simulator();
            var encryptionQueue = eventQueue;
            ITime timeKeeper = eventQueue;
            IList<TestTransportPeer> peers = this.CreateAndForgeTestTransportPeerPair(eventQueue, encryptionQueue);
            new Connection(peers[1].AddressProvider.PublicAddress, null, peers[0].ConnectionTable, eventQueue, encryptionQueue, timeKeeper, peers[0].AddressProvider, peers[0].ConnectivityReporter, new TokenCache(eventQueue, timeKeeper, peers[0].ConnectivityReporter));
        }

        /// <summary>
        /// Test that an unreliable message can be sent across a lossless connection.
        /// </summary>
        [Test]
        public void UnreliableMessageTest()
        {
            var eventQueue = new Simulator();
            var encryptionQueue = eventQueue;
            IList<TestTransportPeer> peers = this.CreateAndForgeTestTransportPeerPair(eventQueue, encryptionQueue);
            this.EstablishConnection(peers[0], peers[1], eventQueue, encryptionQueue);

            TransportEnvelope envelope = peers[0].Protocol.GetMessageFor(peers[1].AddressProvider.PublicAddress, QualityOfService.Unreliable);
            peers[0].Protocol.SendMessage(envelope);

            Assert.IsFalse(peers[1].Protocol.mHasCallTestCB, "Peer 1 apparently received message before any were sent.");

            eventQueue.RunUntil(
                () => peers[1].Protocol.mHasCallTestCB,
                TimeSpan.FromMilliseconds(100),
                () => Assert.Fail("Peer 1 failed to receive message."));
        }

        /// <summary>
        /// Test that reliable messages sent over an unreliable link are re-sent if dropped.
        /// </summary>
        [Test]
        public void ReliableMessageTest()
        {
            var eventQueue = new Simulator();
            var encryptionQueue = eventQueue;
            IList<TestTransportPeer> peers = this.CreateAndForgeTestTransportPeerPair(eventQueue, encryptionQueue);
            this.EstablishConnection(peers[0], peers[1], eventQueue, encryptionQueue);

            // Temporarily disconnect the link from peer 0 to peer 1
            peers[0].Transport.ModifyLink(peers[1].Transport, 10, 0.0, 64);

            int lastAttempt = 0;

            FailHandlerQos qos = new FailHandlerQos(QualityOfService.Reliable, new SendFailureEventArgs());
            qos.FailureEvent +=
                delegate(object sender, SendFailureEventArgs args)
                {
                    lastAttempt = args.NthAttempt;
                };

            TransportEnvelope envelope = peers[0].Protocol.GetMessageFor(peers[1].AddressProvider.PublicAddress, qos);
            peers[0].ConnectionTable.Connection.RoundTripTimeEstimate = 5;
            peers[0].ConnectionTable.Connection.SendMessage(envelope);

            // Check that first few sends fail.
            eventQueue.RunUntil(
                () => lastAttempt >= 3,
                TimeSpan.FromSeconds(10),
                () => Assert.Fail("Failure handler never set attempt >= 3"));

            Assert.IsFalse(peers[1].Protocol.mHasCallTestCB, "Received message when it was meant to fail.");

            // Reconnect the link from peer 0 to peer 1
            peers[0].Transport.ModifyLink(peers[1].Transport, 10, 1.0, 64);

            // Check that the message now gets resent and arrives
            eventQueue.RunUntil(
                () => peers[1].Protocol.mHasCallTestCB,
                TimeSpan.FromSeconds(10),
                () => Assert.Fail("Failed to receive message."));
        }

        /// <summary>
        /// Test that the MaximumRetriesReachedEvent is invoked when a reliable message has been
        /// resent Parameters.MaximumNumberOfRetries times.
        /// </summary>
        [Test]
        public void MaxRetryTest()
        {
            var eventQueue = new Simulator();
            var encryptionQueue = eventQueue;
            IList<TestTransportPeer> peers = this.CreateAndForgeTestTransportPeerPair(eventQueue, encryptionQueue);
            this.EstablishConnection(peers[0], peers[1], eventQueue, encryptionQueue);

            peers[0].ConnectionTable.Connection.RoundTripTimeEstimate = 5;

            // Take the link down.
            peers[0].Transport.ModifyLink(peers[1].Transport, 0, 0.0, 64);

            peers[0].Transport.ResetCounters();

            PeerAddress peer1Address = peers[1].AddressProvider.PublicAddress;
            TransportEnvelope envelope;

            envelope = peers[0].Protocol.GetMessageFor(peer1Address, QualityOfService.Reliable);
            peers[0].ConnectionTable.Connection.SendMessage(envelope);

            eventQueue.RunUntil(
                () => peers[0].LastFailedMessage != null,
                TimeSpan.FromSeconds(10));

            Assert.IsFalse(peers[1].Protocol.mHasCallTestCB, "Received message when it was meant to fail.");

            // Test that the maximum number of retries is reached if the message is not delivered.
            Assert.AreEqual(Parameters.MaximumNumberOfRetries, peers[0].Transport.NumSentMessages);

            // Test that when maximum retries is reached the MaxRetries event is signalled.
            Assert.AreEqual(envelope, peers[0].LastFailedMessage);
            Assert.AreEqual(peer1Address, peers[0].LastFailedDestination);
        }

        /// <summary>
        /// Tests that no extraneous messages are sent when an unreliable message is sent.
        /// </summary>
        [Test]
        public void UnreliableSendsOnlyOnePacketTest()
        {
            var eventQueue = new Simulator();
            var encryptionQueue = eventQueue;
            IList<TestTransportPeer> peers = this.CreateAndForgeTestTransportPeerPair(eventQueue, encryptionQueue);
            this.EstablishConnection(peers[0], peers[1], eventQueue, encryptionQueue);

            foreach (TestTransportPeer peer in peers)
            {
                peer.Transport.ResetCounters();
                peer.ConnectionTable.Connection.ClearEmptyMessageCounter();
            }

            TransportEnvelope envelope = peers[0].Protocol.GetMessageFor(peers[1].AddressProvider.PublicAddress, QualityOfService.Unreliable);
            peers[0].ConnectionTable.Connection.SendMessage(envelope);

            TimeSpan elapsed = eventQueue.RunUntil(
                () => peers[1].Protocol.mHasCallTestCB,
                TimeSpan.FromMilliseconds(65),
                () => Assert.Fail("Failed to recieve message."));

            // Run for a few more periods to see if any spurious packets are generated
            eventQueue.RunFor(TimeSpan.FromTicks(3 * elapsed.Ticks));

            // Test that exactly one non-feedback message is sent for an unreliable message.
            Assert.AreEqual(peers[1].Transport.NumReceivedMessages, peers[0].ConnectionTable.Connection.EmptyMessageCounter + 1);
            Assert.AreEqual(peers[1].Transport.NumSentMessages, peers[1].ConnectionTable.Connection.EmptyMessageCounter);
        }

        /// <summary>
        /// Tests that reliable messages are resent if an ack is not received, and that duplicates
        /// are correctly ignored.
        /// </summary>
        [Test]
        public void ResendTest()
        {
            var eventQueue = new Simulator();
            var encryptionQueue = eventQueue;
            IList<TestTransportPeer> peers = this.CreateAndForgeTestTransportPeerPair(eventQueue, encryptionQueue);
            this.EstablishConnection(peers[0], peers[1], eventQueue, encryptionQueue);

            // Disconnect the link from peer 1 to peer 0 so that acks can't get through.
            peers[1].Transport.ModifyLink(peers[0].Transport, 0, 0.0, 64);

            TimeSpan rttEstimate = TimeSpan.FromMilliseconds(10);
            peers[0].ConnectionTable.Connection.RoundTripTimeEstimate = rttEstimate.TotalMilliseconds;

            peers[1].Transport.ResetCounters();

            int messageCount = 3;

            // Send some messages
            for (int i = 0; i < messageCount; i++)
            {
                TransportEnvelope envelope = peers[0].Protocol.GetMessageFor(peers[1].Transport.Address, QualityOfService.Reliable);
                peers[0].ConnectionTable.Connection.SendMessage(envelope);
            }

            // Test that dropping an acknowledgment does not stop the messages being delivered.
            eventQueue.RunUntil(
                () => peers[1].Protocol.MessagesArrivedCount == messageCount,
                TimeSpan.FromMilliseconds(100),
                () => Assert.Fail("Failed to receive all messages."));

            Assert.AreEqual(messageCount, peers[1].Transport.NumReceivedMessages, "Resends sent too early");

            var extraDelay = TimeSpan.FromMilliseconds(10);  // Extra delay associated with rate limiting.
            eventQueue.RunFor(rttEstimate + extraDelay + Parameters.AckDelay + TimeSpan.FromMilliseconds(RttRateEstimator.ExtraTimeoutDelay));

            // Check that resends came through
            Assert.AreEqual(2 * messageCount, peers[1].Transport.NumReceivedMessages, "Missing resends");

            // Test that duplicates are ignored
            Assert.AreEqual(messageCount, peers[1].Protocol.MessagesArrivedCount, "Failed to filter duplicate messages.");
        }

        /// <summary>
        /// Tests that a sequence of reliable messages are sent correctly, that ACKs are packet into response
        /// packets, and that average time to send a message is within reasonable bounds.
        /// </summary>
        /// <param name="latency">The latency of the connection under test.</param>
        /// <param name="reliability">The reliability of the connection under test.</param>
        ///
        //// [TestCase(8)] TODO: For some reason an ack is being sent separately in this case
        [TestCase(10, 1.0)]
        [TestCase(10, 0.8)]
        [TestCase(20, 1.0)]
        [TestCase(50, 1.0)]
        [TestCase(100, 1.0)]
        [TestCase(200, 1.0)]
        [TestCase(1000, 1.0)]
        public void PingPongTest(int latency, double reliability)
        {
            int numMessages = 25;
            var eventQueue = new Simulator();
            var encryptionQueue = eventQueue;
            IList<PingPongPeer> peers = this.CreateAndForgePingPongPeerPair(numMessages, eventQueue, encryptionQueue);
            this.EstablishConnection(peers[0], peers[1], eventQueue, encryptionQueue);

            peers[0].ConnectionTable.Connection.RoundTripTimeEstimate = latency * 2;
            peers[1].ConnectionTable.Connection.RoundTripTimeEstimate = latency * 2;

            peers[0].Transport.ModifyLink(peers[1].Transport, latency, reliability, 64);
            peers[1].Transport.ModifyLink(peers[0].Transport, latency, reliability, 64);
            
            foreach (PingPongPeer peer in peers)
            {
                peer.ConnectionTable.Connection.ClearEmptyMessageCounter();
                peer.Transport.ResetCounters();
            }

            peers[0].Protocol.Run(); // Send the initial message (this function call also counts as a "message arrived" on peer 0).

            TimeSpan elapsed = eventQueue.RunUntil(
                () => peers[0].Protocol.NumMessagesArrived == numMessages,
                TimeSpan.FromMinutes(10));

            // Allow time for the final ack to go through, and run a bit longer to to check that we don't send
            // any spurious messages.
            eventQueue.RunFor(TimeSpan.FromSeconds(10));

            Assert.AreEqual(numMessages - 1, peers[1].Protocol.NumMessagesArrived, "Wrong number of pings");
            Assert.AreEqual(numMessages, peers[0].Protocol.NumMessagesArrived, "Wrong number of pongs");

            int totalMessages = 2 * (numMessages - 1);  // Number of ping pong packets actually sent on the wire
            double avgMsPerMessage = elapsed.TotalMilliseconds / totalMessages;

            ////System.Console.WriteLine("Took " + elapsed.TotalMilliseconds + "ms (simulated time) for ping pong.");
            ////System.Console.WriteLine("Avg per envelope = " + (elapsed.TotalMilliseconds / (2 * (numMessages - 1))) + "ms.");

            // Assertions below are only made for lossless connections.
            // TODO: Add an assertion on time per message for lossy connections based on the
            //       loss rate and RTT.
            if (reliability < 1.0)
            {
                return;
            }

            // DGC: The allowable time per message formula was chosen to just admit the test results
            //      seen at the time this assertion was added.  The values seem reasonable, and given
            //      we can acheive this performance at the moment we should make sure we don't make
            //      changes that give worse performance.
            Assert.LessOrEqual(avgMsPerMessage, (1.01 * latency) + 17, "Average message time significantly greater than latency");

            // Test that multiple reliable sends doesn't send unneccessary messages, and that ACKs are packed
            // into the messages successfully
            int pingOverheadMessageCount = peers[0].ConnectionTable.Connection.EmptyMessageCounter;
            int pongOverheadMessageCount = peers[1].ConnectionTable.Connection.EmptyMessageCounter;

            // Peer 0 sends (numMessages - 1) pings with acks packed in all but the first, and a final separate ack
            // (although the final ack may be packed into a CongestionFeedback message).
            int expectedSentPeer0 = numMessages - 1 + pingOverheadMessageCount;
            Assert.That(
                peers[0].Transport.NumSentMessages,
                Is.EqualTo(expectedSentPeer0).Or.EqualTo(expectedSentPeer0 + 1),
                "Incorrect number of sent messages (source).");

            // Peer 1 sends (numMessages - 1) pongs with an ack packed in each one.
            Assert.AreEqual(numMessages - 1 + pongOverheadMessageCount, peers[1].Transport.NumSentMessages, "Incorrect number of sent messages (dest).");

            ////System.Console.WriteLine("num sent = " + peers[0].Transport.NumSentMessages);
        }

        /// <summary>
        /// Test that the RTT estimate converges to twice the link latency.
        /// </summary>
        /// <param name="latency">Latency of the link in each direction.</param>
        [TestCase(50)]
        [TestCase(150)]
        [TestCase(500)]
        [TestCase(1000)]
        public void RttConvergenceTest(int latency)
        {
            var eventQueue = new Simulator();
            var encryptionQueue = eventQueue;
            ITime timeKeeper = eventQueue;
            IList<PingPongPeer> peers = this.CreateAndForgePingPongPeerPair(10000, eventQueue, encryptionQueue);
            this.EstablishConnection(peers[0], peers[1], eventQueue, encryptionQueue);

            peers[0].Transport.ModifyLink(peers[1].Transport, latency, 1.0, 64);
            peers[1].Transport.ModifyLink(peers[0].Transport, latency, 1.0, 64);

            peers[0].Protocol.Run(); // Send the initial message.

            // Run for a bit to get the system stable.  For some reason it takes some time
            // to start sending ping-pong packets.  This time is roughly [6 * RTT + 800]ms,
            // and probably related to the rate limit during the start of the connection.
            eventQueue.RunFor(TimeSpan.FromMilliseconds((12 * latency) + 800));

            // Set the intial estimate to something way off
            double initialRttEstimate = 10 * latency;
            peers[0].ConnectionTable.Connection.RoundTripTimeEstimate = initialRttEstimate;
            peers[1].ConnectionTable.Connection.RoundTripTimeEstimate = initialRttEstimate;

            double lastRttEstimate = 0;
            TimeSpan lastCheck = TimeSpan.Zero;
            GenericCallBackReturn<bool> rttHasSettled =
                delegate
                {
                    if (lastCheck == TimeSpan.Zero)
                    {
                        lastCheck = timeKeeper.Now;
                        lastRttEstimate = peers[0].ConnectionTable.Connection.RoundTripTimeEstimate;
                        return false;
                    }

                    TimeSpan elapsed = timeKeeper.Now - lastCheck;

                    // Only check for settling once every 6 RTT.
                    // TODO: The estimate should be being updated every RTT, but the test fails if
                    //       we check for settling that frequently.  Investigate why this is.
                    if (elapsed.TotalMilliseconds < 12 * latency)
                    {
                        return false;
                    }

                    double rttEstimate = peers[0].ConnectionTable.Connection.RoundTripTimeEstimate;
                    bool settled = Math.Abs(rttEstimate - lastRttEstimate) < 0.1;
                    lastCheck = timeKeeper.Now;
                    lastRttEstimate = rttEstimate;
                    return settled;
                };

            TimeSpan settlingTime = eventQueue.RunUntil(
                rttHasSettled,
                TimeSpan.FromMinutes(10));

            Logger.TraceInformation(
                LogTag.Tests,
                "RTT estimate has converged to {0}ms in {1}ms.",
                peers[0].ConnectionTable.Connection.RoundTripTimeEstimate,
                settlingTime.TotalMilliseconds);

            /* TODO: Add an assertion on the maximum time allowed for convergence.  Because of the design
             *       of the estimator this should be a test for convergence to within (say) 5% rather than
             *       absolute convergence which will take a very long time.
             */

            Assert.Greater(peers[0].ConnectionTable.Connection.RoundTripTimeEstimate, (2 * latency) - 5);
            Assert.Less(peers[0].ConnectionTable.Connection.RoundTripTimeEstimate, (2 * latency) + 22);
        }

        /// <summary>
        /// Test that an acknowledgement comes through even if there is no other subsequent packet on that link.
        /// </summary>
        [Test]
        public void DelayedAckTest()
        {
            var eventQueue = new Simulator();
            var encryptionQueue = eventQueue;
            IList<TestTransportPeer> peers = this.CreateAndForgeTestTransportPeerPair(eventQueue, encryptionQueue);
            this.EstablishConnection(peers[0], peers[1], eventQueue, encryptionQueue);

            TransportEnvelope envelope = peers[0].Protocol.GetMessageFor(peers[1].AddressProvider.PublicAddress, QualityOfService.Reliable);
            peers[0].ConnectionTable.Connection.SendMessage(envelope);

            eventQueue.RunFor(TimeSpan.FromMilliseconds(100));

            Assert.IsTrue(peers[1].Protocol.mHasCallTestCB, "Failed to receive message.");
            Assert.IsNull(peers[0].LastFailedMessage, "Ack was never received");
        }

        /// <summary>
        /// Test that a message sent before a connection begins the establishment process causes
        /// establishment to begin, and that the message is sent once the connection is established.
        /// </summary>
        [Test]
        public void QueueMessageBeforeEstablishmentBeginsTest()
        {
            var eventQueue = new Simulator();
            var encryptionQueue = eventQueue;
            IList<TestTransportPeer> peers = this.CreateAndForgeTestTransportPeerPair(eventQueue, encryptionQueue);

            this.JoinWithRealiability(peers[0], peers[1], 1.0);

            TransportEnvelope envelope = peers[0].Protocol.GetMessageFor(peers[1].Transport.Address, QualityOfService.Unreliable);
            peers[0].Protocol.SendMessage(envelope);

            eventQueue.RunFor(TimeSpan.FromMilliseconds(20));

            // Test that messages passed to SendMessage(...) are queued if not yet connected.
            Assert.AreEqual(1, peers[0].ConnectionTable.Connection.NumberOfQueuedMessages);
            Assert.IsFalse(peers[1].Protocol.mHasCallTestCB);

            eventQueue.RunFor(TimeSpan.FromSeconds(120));

            // Test that queued messages are sent once the connection is up.
            Assert.AreEqual(0, peers[0].ConnectionTable.Connection.NumberOfQueuedMessages);
            Assert.IsTrue(peers[1].Protocol.mHasCallTestCB, "Failed to receive message.");
        }

        /// <summary>
        /// Test that a message sent during the establishment process is sent once the
        /// connection is established.
        /// </summary>
        [Test]
        public void QueueMessageDuringEstablishmentTest()
        {
            var eventQueue = new Simulator();
            var encryptionQueue = eventQueue;
            IList<TestTransportPeer> peers = this.CreateAndForgeTestTransportPeerPair(eventQueue, encryptionQueue);

            this.JoinWithRealiability(peers[0], peers[1], 1.0);

            peers[0].ConnectionTable.Connection.BeginInitialization();

            peers[1].ConnectionTable.Connection.BeginInitialization();

            TransportEnvelope envelope = peers[0].Protocol.GetMessageFor(peers[1].Transport.Address, QualityOfService.Unreliable);
            peers[0].Protocol.SendMessage(envelope);

            eventQueue.RunFor(TimeSpan.FromMilliseconds(20));

            // Test that messages passed to SendMessage(...) are queued if not yet connected.
            Assert.AreEqual(1, peers[0].ConnectionTable.Connection.NumberOfQueuedMessages);
            Assert.IsFalse(peers[1].Protocol.mHasCallTestCB);

            eventQueue.RunFor(TimeSpan.FromSeconds(120));

            // Test that queued messages are sent once the connection is up.
            Assert.AreEqual(0, peers[0].ConnectionTable.Connection.NumberOfQueuedMessages);
            Assert.IsTrue(peers[1].Protocol.mHasCallTestCB, "Failed to receive message.");
        }

        /// <summary>
        /// Test that a connection can be re-established if one side loses it.
        /// </summary>
        [Test]
        public void ReInitializeTest()
        {
            var eventQueue = new Simulator();
            var encryptionQueue = eventQueue;
            IList<TestTransportPeer> peers = this.CreateAndForgeTestTransportPeerPair(eventQueue, encryptionQueue);
            this.EstablishConnection(peers[0], peers[1], eventQueue, encryptionQueue);

            TransportEnvelope envelope;

            // Test a call to SendMessage() will pass the message to the transport layer.
            envelope = peers[0].Protocol.GetMessageFor(peers[1].AddressProvider.PublicAddress, QualityOfService.Unreliable);
            peers[0].Protocol.SendMessage(envelope);

            eventQueue.RunUntil(
                () => peers[1].Protocol.mHasCallTestCB,
                TimeSpan.FromMilliseconds(100),
                () => Assert.Fail("Failed to recieve first message (1)."));

            Assert.AreEqual(1, peers[1].Protocol.MessagesArrivedCount, "Failed to recieve first message (2).");

            // Simulate a restart of the source peer
            peers[0] = new TestTransportPeer(1, NatType.Open, eventQueue, encryptionQueue);
            peers[0].CreateConnection(peers[1]);
            peers[0].Forge();
            this.JoinWithRealiability(peers[0], peers[1], 1.0);

            peers[0].ConnectionTable.Connection.BeginInitialization();

            // Test a call to SendMessage() will pass the message to the transport layer.
            envelope = peers[0].Protocol.GetMessageFor(peers[1].AddressProvider.PublicAddress, QualityOfService.Unreliable);
            peers[0].Protocol.SendMessage(envelope);

            eventQueue.RunFor(TimeSpan.FromSeconds(5));

            Assert.AreEqual(2, peers[1].Protocol.MessagesArrivedCount, "Failed to recieve second message.");
        }

        [TestCase(1.0)]
        [TestCase(0.95)]
        public void FragmentTest(double reliability)
        {
            // Initialize the connection
            var eventQueue = new Simulator();
            var encryptionQueue = eventQueue;
            IList<LargePacketPeer> peers = this.CreateAndForgeLargePacketPeerPair(eventQueue, encryptionQueue);
            this.EstablishConnection(peers[0], peers[1], eventQueue, encryptionQueue);

            this.JoinWithRealiability(peers[0], peers[1], reliability);

            for (int length = 500; length <= 2000; length += 500)
            {
                for (int i = 0; i < 10; i++)
                {
                    peers[1].Protocol.ClearBytesReceived();

                    peers[0].Protocol.SendBytes(length, peers[1].Transport.Address);

                    TimeSpan elapsed = eventQueue.RunUntil(
                        () => peers[1].Protocol.NumBytesReceived == length,
                        TimeSpan.FromMinutes(10),
                        () => Assert.Fail("Failed to defragment message for length {0}, attempt {1}", length, i));

                    // TODO: Add assertion for the time taken to send message.  Need to take into account
                    //       reliability and low initial rate limit (need to send a bunch of packets first
                    //       to get the rate limit up).
                    
                    ////Console.WriteLine("{0} -> {1}", length, elapsed.TotalMilliseconds);
                }
            }
        }

        /* TODO:
         *   - Test that sequencing still works after overflow.  (including out of order message arrival around overflow:
         *     EG. with sequence max-2, max-1, 0, max; the last message (max) should be considered as ordered between max-1 and 0
         */

        private IList<T> CreateAndForgePeerPair<T>(Func<int, NatType, T> createPeer) where T : TestPeer
        {
            List<T> peers = new List<T>
            {
                createPeer(1, NatType.Open),
                createPeer(2, NatType.FullCone)
            };

            peers[0].CreateConnection(peers[1]);
            peers[1].CreateConnection(peers[0]);

            this.JoinWithRealiability(peers[0], peers[1], 1.0);

            foreach (T peer in peers)
            {
                peer.Forge();
            }

            return peers;
        }

        private IList<TestTransportPeer> CreateAndForgeTestTransportPeerPair(NetworkEventQueue eventQueue, NetworkEventQueue encryptionQueue)
        {
            return this.CreateAndForgePeerPair((id, natType) => new TestTransportPeer(id, natType, eventQueue, encryptionQueue));
        }

        private IList<PingPongPeer> CreateAndForgePingPongPeerPair(int expectedCount, NetworkEventQueue eventQueue, NetworkEventQueue encryptionQueue)
        {
            return this.CreateAndForgePeerPair((id, natType) => new PingPongPeer(id, natType, expectedCount, eventQueue, encryptionQueue));
        }

        private IList<LargePacketPeer> CreateAndForgeLargePacketPeerPair(NetworkEventQueue eventQueue, NetworkEventQueue encryptionQueue)
        {
            return this.CreateAndForgePeerPair((id, natType) => new LargePacketPeer(id, natType, eventQueue, encryptionQueue));
        }

        private void JoinWithRealiability(TestPeer peerA, TestPeer peerB, double reliability)
        {
            peerA.Transport.ModifyLink(peerB.Transport, 10, reliability, 64);
            peerB.Transport.ModifyLink(peerA.Transport, 10, reliability, 64);
        }

        private void EstablishConnection(TestPeer peerA, TestPeer peerB, Simulator eventQueue, Simulator encryptionQueue)
        {
            peerA.ConnectionTable.Connection.BeginInitialization();

            Assert.IsFalse(peerA.ConnectionTable.Connection.IsConnected, "Peer A claims to be connection prior to initializing");
            Assert.IsFalse(peerB.ConnectionTable.Connection.IsConnected, "Peer B claims to be connection prior to initializing");
            
            TimeSpan elapsed = eventQueue.RunUntil(
                () => peerA.ConnectionTable.Connection.IsConnected && peerB.ConnectionTable.Connection.IsConnected,
                TimeSpan.FromSeconds(10),
                () => Assert.Fail("Connection not established"));

            // TODO: Reduce the allowed connection time
            Assert.LessOrEqual(elapsed, TimeSpan.FromMilliseconds(1500), "Took too long to connect");

            Logger.TraceInformation(LogTag.Tests, "Connection established in {0}ms", (int)elapsed.TotalMilliseconds);

            // TODO: Eliminate the RunFor line below.
            //       An extra 100ms are simulated here.  This was added when EstablishedConnection was changed
            //       to use Simulator.RunUntil(...) instead of Simulator.RunFor(10 seconds).  Without it
            //       some of the existing test cases will fail.  I think that the time is required so that
            //       the rate limiter can get back to a point where it's allowed to send the next packet
            //       immediately.  If the test cases are changed to use RunUntil(...) instead of RunFor(...)
            //       then this extra simulated time should not be required.
            eventQueue.RunFor(TimeSpan.FromMilliseconds(100));
        }

        private void EstablishConnectionSimultaneously(TestPeer peerA, TestPeer peerB, Simulator eventQueue, Simulator encryptionQueue)
        {
            foreach (TestPeer peer in new TestPeer[] { peerA, peerB })
            {
                peer.ConnectionTable.Connection.BeginInitialization();
            }

            Assert.IsFalse(peerA.ConnectionTable.Connection.IsConnected, "Peer A claims to be connection prior to initializing");
            Assert.IsFalse(peerB.ConnectionTable.Connection.IsConnected, "Peer B claims to be connection prior to initializing");

            TimeSpan elapsed = eventQueue.RunUntil(
                () => peerA.ConnectionTable.Connection.IsConnected && peerB.ConnectionTable.Connection.IsConnected,
                TimeSpan.FromSeconds(10),
                () => Assert.Fail("Connection not established"));

            Logger.TraceInformation(LogTag.Tests, "Connection established in {0}ms", (int)elapsed.TotalMilliseconds);
        }

        private class TestTransportPeer : TestPeer<TestTransportProtocol>
        {
            public TestTransportPeer(int id, NatType natType, NetworkEventQueue eventQueue, NetworkEventQueue encryptionQueue)
                : base(id, natType, eventQueue, encryptionQueue)
            {
            }

            protected override ProtocolComponent<TransportEnvelope> CreateProtocol(PeerAddress otherPeerAddress, int otherPeerId)
            {
                TestTransportProtocol protocol = new TestTransportProtocol(this.ConnectionTable.ProtocolRoot, this.timeKeeper);
                protocol.SendText = String.Format("Test data (source = {0})", this.Id);
                protocol.ExpectedText = String.Format("Test data (source = {0})", otherPeerId);
                return protocol;
            }
        }

        private class PingPongPeer : TestPeer<TestPingPongProtocol>
        {
            private int expectedCount;

            public PingPongPeer(int id, NatType natType, int expectedCount, NetworkEventQueue eventQueue, NetworkEventQueue encryptionQueue)
                : base(id, natType, eventQueue, encryptionQueue)
            {
                this.expectedCount = expectedCount;
            }

            protected override ProtocolComponent<TransportEnvelope> CreateProtocol(PeerAddress otherPeerAddress, int otherPeerId)
            {
                return new TestPingPongProtocol(this.ConnectionTable.ProtocolRoot, this.expectedCount, otherPeerAddress, this.timeKeeper);
            }
        }

        private class LargePacketPeer : TestPeer<LargePacketProcotol>
        {
            public LargePacketPeer(int id, NatType natType, NetworkEventQueue eventQueue, NetworkEventQueue encryptionQueue)
                : base(id, natType, eventQueue, encryptionQueue)
            {
            }

            protected override ProtocolComponent<TransportEnvelope> CreateProtocol(PeerAddress otherPeerAddress, int otherPeerId)
            {
                return new LargePacketProcotol(this.ConnectionTable.ProtocolRoot);
            }
        }
    }
}
