﻿//---------------------------------------------------------------------------------
// <copyright file="OpenPeerListGeneratorTester.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Net;
using Badumna.Core;
using Badumna.Transport;
using Badumna.Utilities;
using NUnit.Framework;
using Rhino.Mocks;

namespace BadumnaTests.Transport.Connections
{
    /// <summary>
    /// Open PeerList Generator Tester
    /// </summary>
    [TestFixture]
    [DontPerformCoverage]
    public class OpenPeerListGeneratorTester
    {
        private IConnectionTable mockedConnectionTable;
        private List<IConnection> mockedConnections;

        [SetUp]
        public void Initialize()
        {
        }

        [TearDown]
        public void Terminate()
        {
        }

        [Test]
        public void SimpleConstructorThrowsNoException()
        {
            this.SetupConnections(100, 50);
            OpenPeerListGenerator generator = new OpenPeerListGenerator(this.mockedConnectionTable);
        }

        [Test]
        public void GetRecentActiveOpenPeersReturnsEnoughPeers()
        {
            this.SetupConnections(100, 50);
            OpenPeerListGenerator generator = new OpenPeerListGenerator(this.mockedConnectionTable);
            List<IPEndPoint> list = generator.GetRecentActiveOpenPeers();
            Assert.AreEqual(Parameters.NumberOfOpenPeerAddressesToSave, list.Count);
        }

        [Test]
        public void GetRecentActiveOpenPeersHandlesZeroOpenPeerCondition()
        {
            this.SetupConnections(100, 0);
            OpenPeerListGenerator generator = new OpenPeerListGenerator(this.mockedConnectionTable);
            List<IPEndPoint> list = generator.GetRecentActiveOpenPeers();
            Assert.AreEqual(0, list.Count);

            this.SetupConnections(0, 0);
            generator = new OpenPeerListGenerator(this.mockedConnectionTable);
            list = generator.GetRecentActiveOpenPeers();
            Assert.AreEqual(0, list.Count);
        }

        [Test]
        public void ReturnedPeersAreOpenPeers()
        {
            this.SetupConnections(100, 50);
            OpenPeerListGenerator generator = new OpenPeerListGenerator(this.mockedConnectionTable);
            List<IPEndPoint> list = generator.GetRecentActiveOpenPeers();
            foreach (IPEndPoint ep in list)
            {
                foreach (IConnection con in this.mockedConnections)
                {
                    IPEndPoint endpoint = con.PeersPublicAddress.EndPoint;
                    if (endpoint.Equals(ep))
                    {
                        Assert.IsTrue(con.PeersPublicAddress.NatType == NatType.FullCone || 
                                      con.PeersPublicAddress.NatType == NatType.Open);
                    }
                }
            }
        }

        [Test]
        public void ReturnedPeersAreRecentlyActivePeers()
        {
            this.SetupConnections(100, 50);
            OpenPeerListGenerator generator = new OpenPeerListGenerator(this.mockedConnectionTable);
            List<IPEndPoint> list = generator.GetRecentActiveOpenPeers();

            foreach (IPEndPoint ep in list)
            {
                foreach (IConnection con in this.mockedConnections)
                {
                    IPEndPoint endpoint = con.PeersPublicAddress.EndPoint;
                    if (endpoint.Equals(ep))
                    {
                        int count = 0;
                        foreach (IConnection curCon in this.mockedConnections)
                        {
                            if (!curCon.PeersPublicAddress.Equals(con.PeersPublicAddress))
                            {
                                if (curCon.LastReceiveTime > con.LastReceiveTime)
                                {
                                    count++;
                                }
                            }
                        }

                        Assert.IsTrue(count < Parameters.NumberOfOpenPeerAddressesToSave);
                    }
                }
            }
        }

        [Test]
        public void ReturnedPeersAreNotDuplicated()
        {
            this.SetupConnections(100, 50);
            OpenPeerListGenerator generator = new OpenPeerListGenerator(this.mockedConnectionTable);
            List<IPEndPoint> list = generator.GetRecentActiveOpenPeers();

            foreach (IPEndPoint ep in list)
            {
                int count = 0;
                foreach (IPEndPoint curEP in list)
                {
                    if (ep.Equals(curEP))
                    {
                        count++;
                    }
                }

                Assert.AreEqual(1, count);
            }
        }

        private void SetupConnections(int total, int open)
        {
            this.mockedConnectionTable = MockRepository.GenerateMock<IConnectionTable>();
            this.mockedConnections = new List<IConnection>();

            // other types
            int otherTyps = total - open;
            for (int i = 0; i < otherTyps; i++)
            {
                PeerAddress address = PeerAddress.GetAddress("127.0.0.1", i, NatType.RestrictedCone);
                TimeSpan lastReceive = TimeSpan.FromSeconds(i);
                IConnection con = MockRepository.GenerateMock<IConnection>();
                con.Stub(f => f.PeersPublicAddress).Return(address).Repeat.Any();
                con.Stub(f => f.LastReceiveTime).Return(lastReceive).Repeat.Any();
                this.mockedConnections.Add(con);
            }

            // open
            for (int i = 0; i < open; i++)
            {
                PeerAddress address;
                if (i % 2 == 0)
                {
                    address = PeerAddress.GetAddress("127.0.0.2", i, NatType.Open);
                }
                else
                {
                    address = PeerAddress.GetAddress("127.0.0.2", i, NatType.FullCone);
                }

                TimeSpan lastReceive = TimeSpan.FromSeconds(RandomSource.Generator.Next(1000));
                IConnection con = MockRepository.GenerateMock<IConnection>();
                con.Stub(f => f.PeersPublicAddress).Return(address).Repeat.Any();
                con.Stub(f => f.LastReceiveTime).Return(lastReceive).Repeat.Any();
                this.mockedConnections.Add(con);
            }

            this.mockedConnectionTable.Stub(f => f.AllConnections).Return(this.mockedConnections).Repeat.Any();
        }
    }
}
