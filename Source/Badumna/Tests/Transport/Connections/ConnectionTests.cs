﻿//-----------------------------------------------------------------------
// <copyright file="ConnectionTests.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using Badumna.Core;
using Badumna.Security;
using Badumna.Transport;
using Badumna.Utilities;
using BadumnaTests.Core;
using NUnit.Framework;
using Rhino.Mocks;

namespace BadumnaTests.Transport.Connections
{
    [TestFixture]
    [DontPerformCoverage]
    public class ConnectionTests
    {
        /// <summary>
        /// Test that a relay connection can be created.
        /// </summary>
        [Test]
        public void ConnectionTest()
        {
            var relayManager = MockRepository.GenerateMock<IRelayManager>();
            var relayTransport = new RelayTransport(relayManager);
            var connectionProtocol = MockRepository.GenerateMock<ConnectionProtocol>(
                "foo",
                typeof(ConnectionlessProtocolMethodAttribute),
                new GenericCallBackReturn<object, Type>(MessageParserTester.DefaultFactory));
            var eventQueue = new Simulator();
            var encryptionQueue = new Simulator();
            var timeKeeper = eventQueue;
            var addressProvider = MockRepository.GenerateStub<INetworkAddressProvider>();
            var connectivityReporter = new NetworkConnectivityReporter(eventQueue);
            var tokens = new TokenCache(eventQueue, timeKeeper, connectivityReporter);

            new Connection(PeerAddress.GetRandomAddress(), relayTransport, connectionProtocol, eventQueue, encryptionQueue, timeKeeper, addressProvider, connectivityReporter, tokens);
        }
    }
}
