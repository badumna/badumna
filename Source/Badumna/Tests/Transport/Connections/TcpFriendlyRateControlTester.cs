﻿using System;
using Badumna.Core;
using Badumna.Transport;
using Badumna.Utilities;
using NUnit.Framework;
using SequenceNumber = Badumna.Utilities.CyclicalID.UShortID;
using Rhino.Mocks;

namespace BadumnaTests.Transport.Connections
{
    class MockTcpFriendlyRateControlDelegate : ITcpFriendlyRateControlDelegate
    {
        public double RoundTripTimeEstimateMs { get; set; }
        public double SentBytesPerSecond { get; set; }

        public bool PiggybackFeedback { get; set; }

        public void ScheduleFeedback(TimeSpan maximumDelay)
        {
            this.EventQueue.Schedule(
                this.PiggybackFeedback ? maximumDelay.TotalMilliseconds : 0,
                delegate
                {
                    var feedback = this.CongestionControl.GetFeedback();
                    if (this.Other != null && feedback != null)
                    {
                        this.EventQueue.Schedule(
                            this.RoundTripTimeEstimateMs / 2.0,
                            delegate
                            {
                                this.Other.Received(this.GetNextSequenceNumber(), 10, true);  // Simulate the feedback packet
                                this.Other.CongestionFeedback(feedback, this.Now);
                            });
                    }
                });
        }

        private SequenceNumber mSequenceNumber = new SequenceNumber();

        public NetworkEventQueue EventQueue { get; private set; }

        public ICongestionControl CongestionControl { get; set; }

        public MockTcpFriendlyRateControlDelegate(Simulator eventQueue)
        {
            this.EventQueue = eventQueue;
            this.PiggybackFeedback = true;
        }

        public TimeSpan Now
        {
            get { return this.EventQueue.Now; }
        }

        public TcpFriendlyRateControl Other { get; set; }

        public SequenceNumber GetNextSequenceNumber()
        {
            this.mSequenceNumber.Increment();
            return this.mSequenceNumber;
        }
    }

    class UnbufferedLink
    {
        public double InputRate { get { return this.mInputRate.EstimatedRate; } }
        public double OutputRate { get { return this.mOutputRate.EstimatedRate; } }


        private GenericCallBack<SequenceNumber, int> mReceive;
        private GenericCallBackReturn<double> mGetBandwidth;
        private TimeSpan mNextSendTime = TimeSpan.Zero;

        private RateSmoother mInputRate;
        private RateSmoother mOutputRate;

        private ITime timeKeeper;


        public UnbufferedLink(GenericCallBack<SequenceNumber, int> receive, GenericCallBackReturn<double> getBandwidth, double rttSeconds, ITime timeKeeper)
        {
            this.timeKeeper = timeKeeper;

            this.mReceive = receive;
            this.mGetBandwidth = getBandwidth;

            this.mInputRate = new RateSmoother(TimeSpan.FromSeconds(5 * rttSeconds), TimeSpan.FromSeconds(100 * rttSeconds), timeKeeper);
            this.mOutputRate = new RateSmoother(TimeSpan.FromSeconds(5 * rttSeconds), TimeSpan.FromSeconds(100 * rttSeconds), timeKeeper);
        }

        public virtual void Send(SequenceNumber sequenceNumber, int packetSize)
        {
            this.mInputRate.ValueEvent(packetSize);

            if (!this.AllowSend())
            {
                return;
            }

            double bandwidth = this.mGetBandwidth();
            this.mNextSendTime = this.timeKeeper.Now + TimeSpan.FromSeconds(packetSize / bandwidth);
            this.mOutputRate.ValueEvent(packetSize);
            this.mReceive(sequenceNumber, packetSize);
        }

        protected virtual bool AllowSend()
        {
            return this.timeKeeper.Now >= this.mNextSendTime;
        }
    }

    class LossyLink : UnbufferedLink
    {
        public double LossRate { get; set; }
        private Random mRandom;

        public LossyLink(GenericCallBack<SequenceNumber, int> receive, double rttSeconds, int seed, ITime timeKeeper)
            : base(receive, delegate { return 0; }, rttSeconds, timeKeeper)
        {
            this.mRandom = new Random(seed);
        }

        protected override bool AllowSend()
        {
            return this.mRandom.NextDouble() > this.LossRate;
        }
    }

    [TestFixture]
    public class TcpFriendlyRateControlTester
    {
        [Test]
        public void RemoteLossRateTest()
        {
            double threshold = 0.05;  // Test that estimate is within 5%

            // We only test up to 80% generated packet loss.  The TFRC system
            // overestimates loss rates above this because its loss event 
            // history is limited to 8 loss intervals.  The cut off occurs
            // when 8 out of 9 packets are dropped, or at a rate of 0.888.
            // In this case, most of the time the loss history is overwhelmed
            // by the loss intervals of 1.

            for (int i = 0; i <= 8; i++)
            {
                double generatedLossRate = (double)i / 10;
                double estimatedLossRate = this.RegularLossRateCalcuationTest(generatedLossRate, 100);
                //Console.WriteLine("{0:F3} => {1:F3}", generatedLossRate, estimatedLossRate);
                Assert.GreaterOrEqual(estimatedLossRate, (1 - threshold) * generatedLossRate);
                Assert.LessOrEqual(estimatedLossRate, (1 + threshold) * generatedLossRate);
            }
        }

        private double RegularLossRateCalcuationTest(double lossRate, double packetInterval)
        {
            Simulator eventQueue = new Simulator();

            MockTcpFriendlyRateControlDelegate mockDelegate = new MockTcpFriendlyRateControlDelegate(eventQueue);
            // We leave the RTT set to 0 because the TFRC algorithm treats multiple losses in a
            // within one RTT as a single loss event.  In this simulated scenario we want all loss
            // events to be recognized so that the calculated rate matches our generated loss rate.

            TcpFriendlyRateControl tfrc = new TcpFriendlyRateControl(mockDelegate, new RttRateEstimator(TimeSpan.FromSeconds(1.5)));
            mockDelegate.CongestionControl = tfrc;

            SequenceNumber seqNum = new SequenceNumber();
            tfrc.ReceivedInitial(seqNum, 10);

            int lost = 0;
            TimeSpan stepTime = TimeSpan.FromMilliseconds(packetInterval);
            for (int i = 1; i <= 200; i++)
            {
                eventQueue.RunFor(stepTime);

                seqNum.Increment();

                if ((double)lost / i < lossRate)
                {
                    lost++;
                    //Console.Write("0");
                    continue;
                }

                //Console.Write("1");
                tfrc.Received(seqNum, 100, false);
            }

            //Console.WriteLine();
            return tfrc.RemoteLossEventRate;
        }

        [TestCase(true), TestCase(false)]
        public void NoLossSendRateLimitTest(bool piggybackFeedback)
        {
            // So long as there's no loss TFRC should allow us to double our
            // current sending rate (after we get past the slow-start phase).

            double desiredSendRate = 150 * 1024;  // 150 kB/s
            double receiveRate = 1000 * 1024;  // Plenty of bandwidth
            double packetSize = TcpFriendlyRateControl.AverageSegmentSize;  // bytes
            double rtt = 200;  // ms
            TimeSpan runTime = TimeSpan.FromMilliseconds(20 * rtt);  // a million times the starting rate should be enough...

            this.SendTest(desiredSendRate, receiveRate, packetSize, rtt, runTime, null,
                delegate(TcpFriendlyRateControl senderTfrc, double smoothedFinalSendRate)
                {
                    Assert.GreaterOrEqual(senderTfrc.SendLimitBytesPerSecond, 0.95 * 2.0 * desiredSendRate);

                    // When congestion feedback information is sent delayed (due the wait for a message
                    // to piggyback on), the calculation of Feedback.ReceivedBytesPerSecond can overestimate
                    // the receive rate.  This results in an overestimate of the allowed send rate.
                    // This only appears to affect the non-loss case, so this isn't a major problem.  If
                    // we send to fast we'll end up seeing loss, and then will reduce our send rate appropriately.
                    var upperBound = piggybackFeedback ? 1.6 * 2.0 * desiredSendRate : 1.05 * 2.0 * desiredSendRate;
                    Assert.LessOrEqual(senderTfrc.SendLimitBytesPerSecond, upperBound);
                },
                piggybackFeedback);
        }

        [Test]
        public void BandwidthLimitedTest1()
        {
            this.BandwidthLimitedTest(200);
        }

        [Test]
        public void BandwidthLimitedTest2()
        {
            this.BandwidthLimitedTest(10);
        }

        private void BandwidthLimitedTest(double rtt)
        {
            double desiredSendRate = 150 * 1024;  // 150 kB/s
            double receiveRate = 100 * 1024; // 100 kB/s
            double packetSize = TcpFriendlyRateControl.AverageSegmentSize;  // bytes
            TimeSpan runTime = TimeSpan.FromMilliseconds(1000 * rtt);

            this.SendTest(desiredSendRate, receiveRate, packetSize, rtt, runTime, null,
                delegate(TcpFriendlyRateControl senderTfrc, double smoothedFinalSendRate)
                {
                    Assert.GreaterOrEqual(smoothedFinalSendRate, 0.85 * receiveRate);  // TODO: Try to improve this lower bound
                    Assert.LessOrEqual(smoothedFinalSendRate, 1.05 * receiveRate);
                });
        }


        /// <summary>
        /// Runs a test with one simulated TRFC peer sending to another according to the given parameters.
        /// This method logs values to the console which can be saved as a CSV file to graph the send limit and send rate over time.
        /// </summary>
        private void SendTest(double desiredSendRate, double? receiveRate, double packetSize, double rtt,
            TimeSpan runTime, double? lossRate, GenericCallBack<TcpFriendlyRateControl, double> assert,
            bool piggybackFeedback = true)
        {
            if (receiveRate.HasValue && lossRate.HasValue ||
                !receiveRate.HasValue && !lossRate.HasValue)
            {
                throw new ArgumentException("Must specify exactly one of receiveRate or lossRate");
            }

            Simulator eventQueue = new Simulator();
            ITime timeKeeper = eventQueue;

            MockTcpFriendlyRateControlDelegate senderDelegate = new MockTcpFriendlyRateControlDelegate(eventQueue);
            TcpFriendlyRateControl senderTfrc = new TcpFriendlyRateControl(senderDelegate, new RttRateEstimator(TimeSpan.FromSeconds(1.5)));
            senderDelegate.CongestionControl = senderTfrc;
            senderDelegate.PiggybackFeedback = piggybackFeedback;

            MockTcpFriendlyRateControlDelegate receiverDelegate = new MockTcpFriendlyRateControlDelegate(eventQueue);
            TcpFriendlyRateControl receiverTfrc = new TcpFriendlyRateControl(receiverDelegate, new RttRateEstimator(TimeSpan.FromSeconds(1.5)));
            receiverDelegate.CongestionControl = receiverTfrc;
            receiverDelegate.PiggybackFeedback = piggybackFeedback;

            senderDelegate.RoundTripTimeEstimateMs = rtt;
            senderDelegate.Other = receiverTfrc;

            receiverDelegate.RoundTripTimeEstimateMs = rtt;
            receiverDelegate.Other = senderTfrc;

            UnbufferedLink link;

            if (receiveRate.HasValue)
            {
                link = new UnbufferedLink(
                    delegate(SequenceNumber seq, int size)
                    {
                        receiverTfrc.Received(seq, (int)packetSize, false);
                    },
                    delegate()
                    {
                        return (double)receiveRate;
                    },
                    rtt / 1000,
                    timeKeeper);
            }
            else
            {
                link = new LossyLink(
                    delegate(SequenceNumber seq, int size)
                    {
                        receiverTfrc.Received(seq, (int)packetSize, false);
                    },
                    rtt / 1000,
                    0,
                    timeKeeper);
                ((LossyLink)link).LossRate = (double)lossRate;
            }

            senderTfrc.ReceivedInitial(receiverDelegate.GetNextSequenceNumber(), 10);
            receiverTfrc.ReceivedInitial(senderDelegate.GetNextSequenceNumber(), 10);

            TimeSpan lastLog = TimeSpan.Zero;

            //Console.WriteLine("t (s),limit (kB/s),sent (kB/s),smoothed sent estimate (kB/s)");

            while (timeKeeper.Now < runTime)
            {
                senderDelegate.SentBytesPerSecond = Math.Min(desiredSendRate, senderTfrc.SendLimitBytesPerSecond);

                senderTfrc.Received(receiverDelegate.GetNextSequenceNumber(), 1, false);  // Simulated acks going back
                senderTfrc.Sent((int)packetSize);
                link.Send(senderDelegate.GetNextSequenceNumber(), (int)packetSize);

                TimeSpan time = TimeSpan.FromTicks((long)(TimeSpan.TicksPerSecond * packetSize / senderDelegate.SentBytesPerSecond));
                eventQueue.RunFor(time);

                if (timeKeeper.Now - lastLog > TimeSpan.FromMilliseconds(rtt))
                {
                    lastLog = timeKeeper.Now;
                    //Console.WriteLine("{0},{1},{2},{3}",
                    //    timeKeeper.Now.TotalSeconds,
                    //    senderTfrc.SendLimitBytesPerSecond / 1024,
                    //    senderDelegate.SentBytesPerSecond / 1024,
                    //    link.InputRate / 1024);
                }
            }

            assert(senderTfrc, link.InputRate);  // The input rate to the link is the send rate of the sender which is limited by TFRC.
        }

        /// <summary>
        /// Tests the modulo arithemtic used to calculate RTT samples from a peer's clock times.
        /// </summary>
        [TestCase((short)150, (short)50, 100)]
        [TestCase((short)50, (short)(short.MaxValue - 50), 100)]
        [TestCase((short)16385, (short)1, 16384)]
        [TestCase((short)1, (short)16385, 16383)]
        [TestCase((short)1, (short)2, short.MaxValue - 1)]
        [TestCase((short)2, (short)1, 1)]
        public void RttSampleCalculationTest(short endTime, short startTime, int sample)
        {
            IRttRateEstimator rttEstimator = MockRepository.GenerateMock<IRttRateEstimator>();
            rttEstimator.Expect(x => x.UpdateEstimate(Arg<TimeSpan>.Is.Anything)).WhenCalled(
                call => Assert.AreEqual(TimeSpan.FromMilliseconds(sample), (TimeSpan)call.Arguments[0]));

            ITcpFriendlyRateControlDelegate mockDelegate = MockRepository.GenerateMock<ITcpFriendlyRateControlDelegate>();
            mockDelegate.Stub(m => m.EventQueue).Return(new NetworkEventQueue()).Repeat.Any();
            TcpFriendlyRateControl tfrc = new TcpFriendlyRateControl(mockDelegate, rttEstimator);

            tfrc.CongestionFeedback(new Feedback(0, 0, 0, startTime), TimeSpan.FromMilliseconds(endTime));

            rttEstimator.VerifyAllExpectations();
        }
    }
}
