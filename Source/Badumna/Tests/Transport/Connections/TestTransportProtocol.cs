﻿//-----------------------------------------------------------------------
// <copyright file="TestTransportProtocol.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using Badumna.Core;
using Badumna.Transport;
using BadumnaTests.Core;

namespace BadumnaTests.Transport.Connections
{
    /// <summary>
    /// A transport protocol that with a protocol method that simply records the fact that it
    /// was called, and asserts it was called with the correct parameters.
    /// </summary>
    internal class TestTransportProtocol : TestProtocolComponent2<TransportEnvelope>
    {
        public TestTransportProtocol(ProtocolComponent<TransportEnvelope> parent, ITime timeKeeper)
            : base(parent, timeKeeper)
        {
        }

        public TransportEnvelope GetMessageFor(PeerAddress destination, QualityOfService qos)
        {
            TransportEnvelope envelope = new TransportEnvelope(destination, qos);

            this.PrepareMessageForDeparture(ref envelope);
            this.MakeCall(envelope);
            return envelope;
        }
    }
}
