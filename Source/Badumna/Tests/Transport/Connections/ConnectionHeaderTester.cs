﻿using System.Collections.Generic;
using Badumna.Core;
using Badumna.Transport;
using NUnit.Framework;
using SequenceNumber = Badumna.Utilities.CyclicalID.UShortID;

namespace BadumnaTests.Transport.Connections
{
    using Badumna.Transport.Connections;

    [TestFixture]
    [DontPerformCoverage]
    public class ConnectionHeaderTester : BadumnaTests.Core.ParseableTestHelper
    {
        class ParseableTester : BadumnaTests.Core.ParseableTester<ConnectionHeader>
        {
            public override void AssertAreEqual(ConnectionHeader expected, ConnectionHeader actual)
            {
                Assert.AreEqual(expected.SequenceNumber, actual.SequenceNumber);
                Assert.AreEqual(expected.IsInitial, actual.IsInitial);
                Assert.AreEqual(expected.IsReliable, actual.IsReliable);
                Assert.AreEqual(expected.ShouldEnforceOrder, actual.ShouldEnforceOrder);
            }

            public override ICollection<ConnectionHeader> CreateExpectedValues()
            {
                ICollection<ConnectionHeader> expectedValues = new List<ConnectionHeader>();

                ConnectionHeader header = new ConnectionHeader();
                header.SequenceNumber = new SequenceNumber(99);
                header.IsInitial = true;
                header.IsReliable = false;

                expectedValues.Add(header);

                header = new ConnectionHeader(); 
                header.SequenceNumber= new SequenceNumber(89);
                header.ShouldEnforceOrder = true;
                header.IsReliable = true;

                expectedValues.Add(header);

                return expectedValues;
            }
        }

        public ConnectionHeaderTester()
            : base(new ParseableTester())
        {
        }
    }
}
