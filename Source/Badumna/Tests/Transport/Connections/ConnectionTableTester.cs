﻿using System;
using System.Collections.Generic;
using Badumna.Core;
using Badumna.Transport;
using Badumna.Utilities;
using BadumnaTests.Core;
using NUnit.Framework;

namespace BadumnaTests.Transport.Connections
{
    [TestFixture]
    public class ConnectionTableTester
    {
        [Test]
        public void PrepareMessageTriggersConnectionFormation()
        {
            var eventQueue = new Simulator();
            var encryptionQueue = eventQueue;
            Peer peer1 = new Peer(1, NatType.Open, eventQueue, encryptionQueue);
            Peer peer2 = new Peer(2, NatType.Open, eventQueue, encryptionQueue);

            int latency = 10;
            double reliability = 1.0;
            double bandwidthKBps = 64;
            peer1.Transport.ModifyLink(peer2.Transport, latency, reliability, bandwidthKBps);
            peer2.Transport.ModifyLink(peer1.Transport, latency, reliability, bandwidthKBps);

            // Calling PrepareMessage should trigger connection establishment.
            TransportEnvelope envelope = new TransportEnvelope(peer2.PublicAddress, QualityOfService.Unreliable);
            peer1.ConnectionTable.PrepareMessage(ref envelope);

            Assert.IsFalse(peer1.ConnectionTable.IsConnected(peer2.PublicAddress));
            Assert.IsFalse(peer2.ConnectionTable.IsConnected(peer1.PublicAddress));

            TimeSpan timeToEstablish = eventQueue.RunUntil(
                () => peer1.ConnectionTable.IsConnected(peer2.PublicAddress) && peer2.ConnectionTable.IsConnected(peer1.PublicAddress),
                TimeSpan.FromSeconds(2),  // TODO: Reduce this limit, connection initiation should be faster.  Seems like slowness here is due to initial TFRC rate limit?
                () => Assert.Fail("Connection not established within time limit"));

            //Console.WriteLine("Time to establish = {0}", timeToEstablish.TotalSeconds);
        }

        private class Peer : BarebonesTestPeer
        {
            public ConnectionTable ConnectionTable { get; private set; }

            public Peer(int id, NatType natType, NetworkEventQueue eventQueue, NetworkEventQueue encryptionQueue)
                : base(id, natType, eventQueue, encryptionQueue)
            {
                this.ConnectionTable = new ConnectionTable(this.Transport, this.Transport, new SeedPeerFinder(new List<string>()), eventQueue, encryptionQueue, eventQueue, this.AddressProvider, this.ConnectivityReporter, "testAppName", this.tokens, MessageParserTester.DefaultFactory);
                this.ConnectionTable.Initialize(new MockRelayManager(), null);
                this.ConnectionTable.ForgeMethodList();
            }
        }


        // TODO

        // Test that a connection that is unused is removed after the correct timeout.

        // Test that a new restricted neighbour causes a hole punch.

        // Test that connections that are uninitialized queue the messages for send.

        // Test that failed rendezvous requests are enveloped.
    }
}
