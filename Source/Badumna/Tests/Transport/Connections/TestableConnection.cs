﻿//-----------------------------------------------------------------------
// <copyright file="TestableConnection.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using Badumna.Core;
using Badumna.Security;
using Badumna.Transport;

namespace BadumnaTests.Transport.Connections
{
    /// <summary>
    /// Connection class augmented by some helper methods required for unit testing.
    /// </summary>
    [DontPerformCoverage]
    internal class TestableConnection : Connection
    {
        public TestableConnection(
            PeerAddress address,
            IMessageDispatcher<TransportEnvelope> dispatcher,
            ConnectionProtocol connectionTable,
            NetworkEventQueue eventQueue,
            NetworkEventQueue encryptionQueue,
            ITime timeKeeper,
            INetworkAddressProvider addressProvider,
            INetworkConnectivityReporter connectivityReporter,
            TokenCache tokens)
            : base(address, dispatcher, connectionTable, eventQueue, encryptionQueue, timeKeeper, addressProvider, connectivityReporter, tokens)
        {
        }

        public int EmptyMessageCounter { get; private set; }

        // helper functions used in unit test.
        public void ClearEmptyMessageCounter()
        {
            this.EmptyMessageCounter = 0;
        }

        protected override void OnSendEmptyMessage()
        {
            this.EmptyMessageCounter++;
        }
    }
}
