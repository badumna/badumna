﻿//-----------------------------------------------------------------------
// <copyright file="TestPingPongProtocol.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using Badumna.Core;
using Badumna.Transport;

namespace BadumnaTests.Transport.Connections
{
    /// <summary>
    /// A transport protocol that sends a series of ping messages between two peers.
    /// </summary>
    [DontPerformCoverage]
    internal class TestPingPongProtocol : TransportProtocol
    {
        private int numExpectedMessages;

        private ITime timeKeeper;

        public TestPingPongProtocol(TransportProtocol layer1, int numExpectedMessages, PeerAddress peersAddress, ITime timeKeeper)
            : base(layer1)
        {
            this.ForgeMethodList();
            this.numExpectedMessages = numExpectedMessages;
            this.PeersAddress = peersAddress;
            this.NumMessagesArrived = 0;
            this.timeKeeper = timeKeeper;
        }

        public int NumMessagesArrived { get; private set; }

        public TimeSpan MessageReceiveTime { get; private set; }

        public PeerAddress PeersAddress { get; set; }

        [ConnectionfulProtocolMethod(0)]
        public void Run()
        {
            this.MessageReceiveTime = this.timeKeeper.Now;
            this.NumMessagesArrived++;

            PeerAddress address = PeerAddress.Nowhere;
            if (null == this.CurrentEnvelope || !this.CurrentEnvelope.Source.IsValid)
            {
                address = this.PeersAddress;
            }
            else
            {
                address = this.CurrentEnvelope.Source;
            }

            if (this.NumMessagesArrived < this.numExpectedMessages)
            {
                TransportEnvelope envelope = this.GetMessageFor(address, QualityOfService.Reliable);

                this.RemoteCall(envelope, this.Run);
                this.SendMessage(envelope);
            }
        }
    }
}
