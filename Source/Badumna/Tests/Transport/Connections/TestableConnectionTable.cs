﻿//-----------------------------------------------------------------------
// <copyright file="TestableConnectionTable.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using Badumna.Core;
using Badumna.Security;
using Badumna.Transport;
using Badumna.Utilities;
using BadumnaTests.Utilities;

namespace BadumnaTests.Transport.Connections
{
    /// <summary>
    /// ConnectionTable class with a single Connection to the peer address specified in the constructor.
    /// </summary>
    [DontPerformCoverage]
    internal class TestableConnectionTable : ConnectionTable
    {
        public TestableConnectionTable(PeerAddress destination, MockTransportLayer transportLayer, IPeerFinder peerFinder, NetworkEventQueue eventQueue, NetworkEventQueue encryptionQueue, ITime timeKeeper, INetworkAddressProvider addressProvider, INetworkConnectivityReporter connectivityReporter, TokenCache tokens, GenericCallBackReturn<object, Type> factory)
            : base(transportLayer, transportLayer, peerFinder, eventQueue, encryptionQueue, timeKeeper, addressProvider, connectivityReporter, "testAppName", tokens, factory)
        {
            this.Connection = new TestableConnection(destination, transportLayer, this, eventQueue, encryptionQueue, timeKeeper, addressProvider, connectivityReporter, tokens);
            this.mConnections[destination] = this.Connection;
            this.Initialize(new MockRelayManager(), null);
        }

        public TestableConnection Connection { get; private set; }
    }
}
