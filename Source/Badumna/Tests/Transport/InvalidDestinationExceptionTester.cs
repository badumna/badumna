﻿using Badumna.Core;
using Badumna.Transport;
using BadumnaTests.Core;
using NUnit.Framework;

namespace BadumnaTests.Transport
{
    [TestFixture]
    [DontPerformCoverage]
    class InvalidDestinationExceptionTester : NetworkExceptionTester<InvalidDestinationException>
    {
    }
}
