﻿//---------------------------------------------------------------------------------
// <copyright file="CombinedPortForwarderTester.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using Badumna.Core;
using Badumna.Transport;
using NUnit.Framework;
using Rhino.Mocks;

namespace BadumnaTests.Transport
{
    [TestFixture]
    [DontPerformCoverage]
    public class CombinedPortForwarderTester
    {
        private IPortForwarder upnpForwarder;
        private IPortForwarder natpmpForwarder;
        private CombinedPortForwarder forwarder;

        [SetUp]
        public void SetUp()
        {
            this.Reset();
        }

        [TearDown]
        public void TearDown()
        {
            this.forwarder = null;
        }

        [Test]
        public void SimpleConstructorThrowsNoException()
        {
            this.forwarder = new CombinedPortForwarder(this.upnpForwarder, this.natpmpForwarder);
        }

        [Test]
        public void InitializeReturnsTrueIfUPnPForwarderCanBeInitialized()
        {
            this.upnpForwarder.Expect(f => f.Initialize(null)).IgnoreArguments().Return(true);
            Assert.IsTrue(this.forwarder.Initialize(null));
            this.upnpForwarder.VerifyAllExpectations();
        }

        [Test]
        public void InitializeReturnsTrueIfNATPMPCanBeInitialized()
        {
            this.upnpForwarder.Expect(f => f.Initialize(null)).IgnoreArguments().Return(false);
            this.natpmpForwarder.Expect(f => f.Initialize(null)).IgnoreArguments().Return(true);
            Assert.IsTrue(this.forwarder.Initialize(null));
            this.upnpForwarder.VerifyAllExpectations();
            this.natpmpForwarder.VerifyAllExpectations();
        }

        [Test]
        public void InitializeReturnsFalseWhenBothForwardersReturnFalse()
        {
            this.upnpForwarder.Expect(f => f.Initialize(null)).IgnoreArguments().Return(false);
            this.natpmpForwarder.Expect(f => f.Initialize(null)).IgnoreArguments().Return(false);
            Assert.IsFalse(this.forwarder.Initialize(null));
            this.upnpForwarder.VerifyAllExpectations();
            this.natpmpForwarder.VerifyAllExpectations();
        }

        [Test]
        public void DisposeGetBothForwardersDisposed()
        {
            this.upnpForwarder.Expect(f => f.Dispose()).Repeat.Once();
            this.natpmpForwarder.Expect(f => f.Dispose()).Repeat.Once();
            this.forwarder.Dispose();
            this.upnpForwarder.VerifyAllExpectations();
            this.natpmpForwarder.VerifyAllExpectations();
        }

        [Test]
        public void GetExternalIPCallsUPnPModuleWhenUPnPForwarderIsEnabled()
        {
            this.EnabledUPnP();
            this.upnpForwarder.Expect(f => f.GetExternalAddress()).Return(null).Repeat.Once();
            this.forwarder.GetExternalAddress();
            this.upnpForwarder.VerifyAllExpectations();
        }

        [Test]
        public void GetExternalIPCallsNATPMPModuleWhenNATPMPForwarderIsEnabled()
        {
            this.EnabledNATPMP();
            this.natpmpForwarder.Expect(f => f.GetExternalAddress()).Return(null).Repeat.Once();
            this.forwarder.GetExternalAddress();
            this.natpmpForwarder.VerifyAllExpectations();
        }

        [Test]
        public void TryMapPortCallsUPnPModuleWhenUPnPForwarderIsEnabled()
        {
            this.EnabledUPnP();
            this.upnpForwarder.Expect(f => f.TryMapPort(null, TimeSpan.MaxValue)).IgnoreArguments().Return(null).Repeat.Once();
            this.forwarder.TryMapPort(null, TimeSpan.MaxValue);
            this.upnpForwarder.VerifyAllExpectations();
        }

        [Test]
        public void TryMapPortCallsNATPMPModuleWhenNATPMPForwarderIsEnabled()
        {
            this.EnabledNATPMP();
            this.natpmpForwarder.Expect(f => f.TryMapPort(null, TimeSpan.MaxValue)).IgnoreArguments().Return(null).Repeat.Once();
            this.forwarder.TryMapPort(null, TimeSpan.MaxValue);
            this.natpmpForwarder.VerifyAllExpectations();
        }

        private void EnabledUPnP()
        {
            this.upnpForwarder.Expect(f => f.Initialize(null)).IgnoreArguments().Return(true);
            Assert.IsTrue(this.forwarder.Initialize(null));
        }

        private void EnabledNATPMP()
        {
            this.upnpForwarder.Expect(f => f.Initialize(null)).IgnoreArguments().Return(false);
            this.natpmpForwarder.Expect(f => f.Initialize(null)).IgnoreArguments().Return(true);
            Assert.IsTrue(this.forwarder.Initialize(null));
        }

        private void Reset()
        {
            this.upnpForwarder = MockRepository.GenerateMock<IPortForwarder>();
            this.natpmpForwarder = MockRepository.GenerateMock<IPortForwarder>();
            this.forwarder = new CombinedPortForwarder(this.upnpForwarder, this.natpmpForwarder);
        }
    }
}