﻿// -----------------------------------------------------------------------
// <copyright file="MatchmakingRequestTests.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace BadumnaTests.Matchmaking
{
    using System;
    using System.IO;
    using Badumna;
    using Badumna.Arbitration;
    using Badumna.Matchmaking;
    using NUnit.Framework;
    using Rhino.Mocks;

    /// <summary>
    /// MatchmakingRequest unit tests.
    /// </summary>
    [TestFixture]
    public class MatchmakingRequestTests
    {
        private IArbitrator arbitrator = null;

        private Options options = new Options();
        private ArbitrationConnectionResultHandler connectionResultHandler;
        private HandleConnectionFailure connectionFailureHandler;
        private HandleServerMessage serverEventHandler;
        private MatchmakingResult completion;
        private MatchmakingError? error;
        private MatchmakingAsyncResult result;
        private MatchmakingOptions validOptions;

        [SetUp]
        public void SetUp()
        {
            this.arbitrator = MockRepository.GenerateMock<IArbitrator>();
            this.error = null;
            this.completion = null;
            this.connectionResultHandler = null;
            this.connectionFailureHandler = null;
            this.serverEventHandler = null;
            this.result = null;
            this.validOptions = new MatchmakingOptions { MinimumPlayers = 2, MaximumPlayers = 50 };
        }

        [Test]
        public void SetMaxMinPlayersWithValidInputThrowsNoException()
        {
            this.Begin(this.validOptions);
        }

        [Test]
        [ExpectedException("System.ArgumentException", ExpectedMessage = "Number of minimum players must be between 2 and 50.")]
        public void NotSettingMinMinPlayersThrowsArgumentException()
        {
            this.Begin(new MatchmakingOptions());
        }

        [Test]
        [ExpectedException("System.ArgumentException")]
        public void SetInvalidMaxPlayersThrowsArgumentException()
        {
            this.Begin(new MatchmakingOptions { MinimumPlayers = 2, MaximumPlayers = 256 });
        }

        [Test]
        [ExpectedException("System.ArgumentException")]
        public void SetInvalidMinPlayersThrowsArgumentException()
        {
            this.Begin(new MatchmakingOptions { MinimumPlayers = 0, MaximumPlayers = 2 });
        }

        [Test]
        [ExpectedException("System.ArgumentException")]
        public void SetInvalidTimeoutThrowsArgumentException()
        {
            this.Begin(new MatchmakingOptions { Timeout = TimeSpan.FromSeconds(1200) });
        }

        [Test]
        [ExpectedException("System.ArgumentException")]
        public void ThrowExceptionsWhenMinPlayersLargerThanMaxPlayers()
        {
            this.Begin(new MatchmakingOptions { MinimumPlayers = 5, MaximumPlayers = 2 });
        }

        [Test]
        public void HandlesFirstConnectionFailure()
        {
            this.Begin();
            this.connectionFailureHandler();
            this.connectionFailureHandler();
            Assert.AreEqual(this.error, MatchmakingError.ConnectionFailed);
        }

        [Test]
        public void HandlesFirstUnsuccessfulConnection()
        {
            this.Begin();
            this.connectionResultHandler(ServiceConnectionResultType.ServiceNotAvailable);
            this.connectionResultHandler(ServiceConnectionResultType.ServiceNotAvailable);
            Assert.AreEqual(this.error, MatchmakingError.ConnectionFailed);
        }

        [Test]
        public void HandlesFirstMatchmakingTimeout()
        {
            this.Begin();
            var response = new MatchmakingReplyEvent("", 0, 0);
            
            this.serverEventHandler(this.Serialize(response));
            this.serverEventHandler(this.Serialize(response));
            Assert.AreEqual(this.error, MatchmakingError.Timeout);
        }

        [Test]
        public void HandlesFirstSuccess()
        {
            this.Begin();
            this.serverEventHandler(this.Serialize(new MatchmakingReplyEvent("room-101", 2, 1)));
            this.serverEventHandler(this.Serialize(new MatchmakingReplyEvent("room-123", 4, 0)));
            Assert.IsTrue(((IEquatable<MatchmakingResult>)this.completion).Equals(new MatchmakingResult("room-101", 2, 1)));
        }

        private MatchmakingRequest Begin()
        {
            return this.Begin(this.validOptions);
        }

        private MatchmakingRequest Begin(MatchmakingOptions options)
        {
            // trap arguments passed to arbitrator.Connect()
            this.arbitrator.Stub(a => a.Connect(
                Arg<ArbitrationConnectionResultHandler>.Is.Anything,
                Arg<HandleConnectionFailure>.Is.Anything,
                Arg<HandleServerMessage>.Is.Anything)).WhenCalled(method =>
            {
                this.connectionResultHandler = (ArbitrationConnectionResultHandler)method.Arguments[0];
                this.connectionFailureHandler = (HandleConnectionFailure)method.Arguments[1];
                this.serverEventHandler = (HandleServerMessage)method.Arguments[2];
            });

            var request = new MatchmakingRequest(this.arbitrator, this.options, this.OnCompletion, null, options);
            this.result = request.Begin();
            return request;
        }

        private void OnCompletion(MatchmakingAsyncResult result)
        {
            Assert.AreEqual(result, this.result);
            Assert.IsNull(this.completion, "Multiple success results received");
            if (result.Succeeded)
            {
                this.completion = result.Match;
            }
            else
            {
                this.error = result.Error;
            }
        }

        private byte[] Serialize(MatchmakingReplyEvent e)
        {
            return MatchmakingEventSet.Serialize(e);
        }
    }
}
