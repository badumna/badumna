﻿// -----------------------------------------------------------------------
// <copyright file="MatchmakingAsyncResultTests.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace BadumnaTests.Matchmaking
{
    using System;
    using System.Threading;
    using Badumna.Matchmaking;
    using NUnit.Framework;

    /// <summary>
    /// MatchmakingResult unit tests.
    /// </summary>
    [TestFixture]
    public class MatchmakingAsyncResultTests
    {
        private MatchmakingAsyncResult result;
        private MatchmakingResult successfulMatch;
        
        [SetUp]
        public void Setup()
        {
            this.result = new MatchmakingAsyncResult();
            this.successfulMatch = new MatchmakingResult("room", 3, 2);
        }

        [Test]
        public void ShouldOnlyAllowCompletionOnce()
        {
            this.result.Complete(this.successfulMatch);
            Assert.Throws<InvalidOperationException>(() => this.result.Complete(this.successfulMatch));
            Assert.Throws<InvalidOperationException>(() => this.result.Fail(MatchmakingError.ConnectionFailed));
        }

        [Test]
        public void ShouldCompleteOnSuccess()
        {
            Assert.IsFalse(this.result.IsCompleted);
            this.result.Complete(this.successfulMatch);

            Assert.IsTrue(this.result.IsCompleted);
            Assert.IsTrue(this.result.Succeeded);

            Assert.AreEqual(this.successfulMatch, this.result.Match);
        }

        [Test]
        public void ShouldCompleteOnFailure()
        {
            Assert.IsFalse(this.result.IsCompleted);
            Assert.IsNull(this.result.Error);
            this.result.Fail(MatchmakingError.Timeout);

            Assert.IsTrue(this.result.IsCompleted);
            Assert.IsFalse(this.result.Succeeded);

            Assert.AreEqual(null, this.result.Match);
            Assert.AreEqual(MatchmakingError.Timeout, this.result.Error);
        }

        [Test]
        public void ShouldTriggerWaitEventIfCreated()
        {
            var handle = this.result.AsyncWaitHandle;
            Assert.AreEqual(handle, this.result.AsyncWaitHandle); // reuses single handle

            var triggered = false;
            var t = new Thread(delegate()
                {
                    handle.WaitOne();
                    triggered = true;
                });
            t.Start();

            try
            {
                Thread.Sleep(10);
                Assert.IsFalse(triggered);

                this.result.Complete(this.successfulMatch);

                Thread.Sleep(10);
                Assert.IsTrue(triggered);
            }
            finally
            {
                t.Abort();
            }
        }
    }
}
