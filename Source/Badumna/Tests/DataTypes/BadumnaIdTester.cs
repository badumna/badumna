﻿using System;
using System.Collections.Generic;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Utilities;
using NUnit.Framework;
using Rhino.Mocks;

namespace BadumnaTests.DataTypes
{
    [TestFixture]
    [DontPerformCoverage]
    public class BadumnaIdTester : BadumnaTests.Core.ParseableTestHelper
    {
        private readonly PeerAddress mLocalAddress = PeerAddress.GetLoopback(0);

        PeerAddress creatorAddress;
        PeerAddress hostAddress;
        ushort localId;
        BadumnaIdAllocator idAllocator;

        [SetUp]
        public void SetUp()
        {
            INetworkAddressProvider addressProvider = MockRepository.GenerateStub<INetworkAddressProvider>();
            addressProvider.Stub(x => x.PublicAddress).Return(this.mLocalAddress);
            INetworkConnectivityReporter connectivityReporter = MockRepository.GenerateStub<INetworkConnectivityReporter>();
            this.idAllocator = new BadumnaIdAllocator(addressProvider, connectivityReporter, new RandomNumberGenerator());

            this.creatorAddress = PeerAddress.GetRandomAddress();
            this.hostAddress = PeerAddress.GetRandomAddress();
            while (this.creatorAddress == this.hostAddress)
            {
                this.creatorAddress = PeerAddress.GetRandomAddress();
            }

            this.localId = (ushort)new Random().Next(ushort.MinValue, ushort.MaxValue);
        }

        class ParseableTester : BadumnaTests.Core.ParseableTester<BadumnaId>
        {
            public override void AssertAreEqual(BadumnaId expected, BadumnaId actual)
            {
                Assert.AreEqual(expected, actual);
            }

            public override ICollection<BadumnaId> CreateExpectedValues()
            {
                ICollection<BadumnaId> expectedValues = new List<BadumnaId>();

                expectedValues.Add(new BadumnaId(PeerAddress.GetLoopback(1), 1));
                expectedValues.Add(new BadumnaId(PeerAddress.GetLoopback(4), 256));

                return expectedValues;
            }
        }

        public BadumnaIdTester()
            : base(new ParseableTester())
        {
        }

#if DEBUG
        // failed assertion will not cause the expected AssertionException in the release build. 
        [Test, ExpectedException(typeof(AssertionException))]
        [Category("MonoFail")]
        public void SerializeNoneTest()
        {
            MessageBuffer message = new MessageBuffer();
            message.PutObject(BadumnaId.None, typeof(BadumnaId));
        }
#endif

        [Test]
        public void CopyTest()
        {
            foreach (BadumnaId expected in this.CreateExpectedValues<BadumnaId>())
            {
                BadumnaId actual = new BadumnaId(expected);

                this.AssertAreEqual<BadumnaId>(expected, actual);
            }
        }

        [Test]
        public void EqualityOperators()
        {
            BadumnaId a = this.idAllocator.GetNextUniqueId();
            BadumnaId b = this.idAllocator.GetNextUniqueId();
            BadumnaId copyA = new BadumnaId(a);
            BadumnaId copyA2 = a;

            Assert.AreEqual(a, a);
            Assert.AreEqual(a, copyA);
            Assert.AreEqual(a, copyA2);

            Assert.AreNotEqual(a, b);
            Assert.AreNotEqual(b, copyA);
            Assert.AreNotEqual(b, copyA2);

            if (a != copyA2)
            {
                Assert.Fail();
            }

            if (a != copyA)
            {
                Assert.Fail();
            }

            if (a == b)
            {
                Assert.Fail();
            }

            if (b == copyA)
            {
                Assert.Fail();
            }

            if (b == copyA2)
            {
                Assert.Fail();
            }
        }

        [Test]
        public void AddressTest()
        {
            BadumnaId a = this.idAllocator.GetNextUniqueId();
            Assert.AreEqual(this.mLocalAddress, a.Address);
        }

        [Test]
        public void OriginalAddressUnchangedByHostAddressSetter()
        {
            var id = new BadumnaId(this.creatorAddress, this.localId);
            id.Address = this.hostAddress;

            Assert.AreEqual(this.creatorAddress, id.OriginalAddress);
        }

        [Test]
        public void HostAddressReturnsHostAddressIfSet()
        {
            var id = new BadumnaId(this.creatorAddress, this.localId);
            id.Address = this.hostAddress;

            Assert.AreEqual(this.hostAddress, id.Address);
        }

        [Test]
        public void HostAddressReturnsCreatorAddressIfNotSet()
        {
            var id = new BadumnaId(this.creatorAddress, this.localId);

            Assert.AreEqual(this.creatorAddress, id.Address);
        }

        [Test]
        public void HostAddressReturnsCreatorAddressIfSetToNull()
        {
            var id = new BadumnaId(this.creatorAddress, this.localId);
            id.Address = this.hostAddress;
            id.Address = null;

            Assert.AreEqual(this.creatorAddress, id.Address);
        }

        [Test]
        public void EqualsIgnoresHostAddress()
        {
            BadumnaId id1 = new BadumnaId(PeerAddress.GetRandomAddress(), 0);
            BadumnaId id2 = new BadumnaId(id1);

            while (id2.Address == id1.Address)
            {
                id2.Address = PeerAddress.GetRandomAddress();
            }

            Assert.IsTrue(id1.Equals(id2));
        }

        [TestCase(true)]
        [TestCase(false)]
        public void IParseableImplementationPreservesHostAddress(bool hostSpecified)
        {
            // Arrange
            BadumnaId id1 = new BadumnaId(this.creatorAddress, this.localId);
            if (hostSpecified)
            {
                id1.Address = this.hostAddress;
            }

            MessageBuffer buffer = new MessageBuffer();

            // Act
            ((IParseable)id1).ToMessage(buffer, typeof(BadumnaId));
            BadumnaId id2 = new BadumnaId();
            ((IParseable)id2).FromMessage(buffer);

            // Assert
            if (hostSpecified)
            {
                Assert.AreEqual(id2.Address, this.hostAddress);
            }
            else
            {
                Assert.AreEqual(id2.Address, this.creatorAddress);
            }
        }

        [TestCase(true)]
        [TestCase(false)]
        public void ToBytesFromBytesPreservesHostAddress(bool hostSpecified)
        {
            // Arrange
            BadumnaId id1 = new BadumnaId(this.creatorAddress, this.localId);
            if (hostSpecified)
            {
                id1.Address = this.hostAddress;
            }

            MessageBuffer buffer = new MessageBuffer();

            // Act
            byte[] bytes = id1.ToBytes();
            BadumnaId id2 = new BadumnaId();
            id2.FromBytes(bytes);

            // Assert
            if (hostSpecified)
            {
                Assert.AreEqual(id2.Address, this.hostAddress);
            }
            else
            {
                Assert.AreEqual(id2.Address, this.creatorAddress);
            }
        }

        [TestCase(true)]
        [TestCase(false)]
        public void ToStringTryParsePreservesHostAddress(bool hostSpecified)
        {
            // Arrange
            BadumnaId id1 = new BadumnaId(this.creatorAddress, this.localId);
            if (hostSpecified)
            {
                id1.Address = this.hostAddress;
            }

            MessageBuffer buffer = new MessageBuffer();

            // Act
            string serializedId = id1.ToString();
            BadumnaId id2 = BadumnaId.TryParse(serializedId);

            // Assert
            if (hostSpecified)
            {
                Assert.AreEqual(id2.Address, this.hostAddress);
            }
            else
            {
                Assert.AreEqual(id2.Address, this.creatorAddress);
            }
        }
    }
}
