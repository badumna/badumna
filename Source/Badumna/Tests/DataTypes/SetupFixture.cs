﻿//-----------------------------------------------------------------------
// <copyright file="SetupFixture.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.DataTypes
{
    using System.Diagnostics;
    using Badumna.Core;
    using NUnit.Framework;

    [SetUpFixture]
    [DontPerformCoverage]
    public class NUnitSetup
    {
        // Field to hold exisitng trace listeners so they can be restored after test are run.
        private TraceListener[] originalListeners = null;

        // A trace listener to use during testing.
        private TraceListener nunitListener = new NUnitListener();

        [SetUp]
        public void SetUp()
        {
            // Replace existing listeners with listener for testing.
            this.originalListeners = new TraceListener[Trace.Listeners.Count];
            Trace.Listeners.CopyTo(this.originalListeners, 0);
            Trace.Listeners.Clear();
            Trace.Listeners.Add(this.nunitListener);
        }

        [TearDown]
        public void TearDown()
        {
            // Restore original trace listeners.
            Trace.Listeners.Remove(this.nunitListener);
            Trace.Listeners.AddRange(this.originalListeners);
        }

        public class NUnitListener : DefaultTraceListener
        {
            public override void Fail(string message)
            {
                Assert.Fail("Ignoring Debug.Fail(\"{0}\")", message);
            }

            public override void Fail(string message, string detailMessage)
            {
                Assert.Fail("Ignoring Debug.Fail(\"{0},{1}\")", message, detailMessage);
            }
        }
    }
}