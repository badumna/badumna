﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Diagnostics;
using System.IO;
using NUnit.Framework;


using Badumna.Core;
using Badumna.Utilities;
using Badumna.Chat;
using Badumna.Replication;
using Badumna.Security;
using Badumna.DataTypes;
using Badumna.Streaming;
using Badumna.DistributedHashTable;
using Badumna.Transport;
using Badumna.Controllers;
using Badumna.SpatialEntities;
using Badumna.Arbitration;
using Badumna.ServiceDiscovery;
using Badumna;

namespace BadumnaTests.Facade
{
  public class NetworkFacadeTester
  {
        private class MockNetworkObject : ISpatialOriginal, ISpatialReplica
        {
            public BadumnaId Guid { get; set; }
            public Vector3 Position { get; set; }

            public float Radius { get { return 0f; } set { } }
            public float AreaOfInterestRadius { get { return 0f; } set { } }

            public void HandleEvent(System.IO.Stream stream)
            {
            }

            public void Serialize(BooleanArray requiredParts, Stream stream)
            {
            }

            public void Deserialize(BooleanArray includedParts, Stream stream, int estimatedMillisecondsSinceDeparture)
            {
            }
        }

        private class MockNetworkScene
        {
            public ISpatialReplica ObjectFactory(String scene, BadumnaId objectId, String objectTag)
            {
                ISpatialReplica remoteEntity = new MockNetworkObject();

                remoteEntity.Guid = objectId;
                return remoteEntity;
            }

            public ISpatialReplica InstantiateRemoteEntity(NetworkScene scene, BadumnaId entityId, uint entityType)
            {
                return new MockNetworkObject();
            }

            public void RemoveObject(NetworkScene scene, IReplicableEntity remoteEntity)
            {
            }
        }

        private NetworkFacade GetInstance(Options options)
        {
            NetworkEventQueue eventQueue = new NetworkEventQueue();
            ITime timeKeeper = eventQueue;
            INetworkConnectivityReporter connectivityReporter = new NetworkConnectivityReporter(eventQueue);
            UdpTransport transportLayer = new UdpTransport(options.Connectivity, eventQueue, timeKeeper, connectivityReporter);
            transportLayer.Initialize();
            return new MainNetworkFacade(
                options, transportLayer, eventQueue, timeKeeper, connectivityReporter, new RandomNumberGenerator());
        }

        private int threadCount;

        private Options options;

        [SetUp]
        public void Setup()
        {
            this.options = new Options();
            this.options.Connectivity.StartPortRange = 21251;
            this.options.Connectivity.EndPortRange = 21261;
            this.options.Connectivity.StunServers.Clear();

            this.threadCount = System.Diagnostics.Process.GetCurrentProcess().Threads.Count;
        }

        [TearDown]
        public void TearDown()
        {
            int threadCount = 0;
            int waitCount = 0;
            while (waitCount < 30)
            {
                threadCount = System.Diagnostics.Process.GetCurrentProcess().Threads.Count;
                if (threadCount == this.threadCount)
                {
                    break;
                }

                waitCount++;
                System.Threading.Thread.Sleep(100);
            }

            int availWorkers;
            int availCompletionPorts;
            int maxWorkers;
            int maxCompletionPorts;
            System.Threading.ThreadPool.GetAvailableThreads(out availWorkers, out availCompletionPorts);
            System.Threading.ThreadPool.GetMaxThreads(out maxWorkers, out maxCompletionPorts);
            //Console.WriteLine("thread pool threads: workers {0}; completion ports {1}", maxWorkers - availWorkers, maxCompletionPorts - availCompletionPorts);
            //Console.WriteLine("original thread count: {0}; final thread count: {1}", this.threadCount, threadCount);
            //Assert.AreEqual(this.threadCount, threadCount, "lingering thread(s)");
        }

        // Check that we can start up and shutdown without throwing any exceptions
        [Test]
        [Category("Slow")]
        public void InitShutdownTest()
        {
            NetworkFacade facade = this.GetInstance(this.options);
            facade.Shutdown();
        }

        // Check that we can login and shutdown without throwing any exceptions
        [Test]
        [Category("Slow")]
        public void LoginShutdownTest()
        {
            NetworkFacade facade = this.GetInstance(this.options);
            facade.Login(new UnverifiedIdentityProvider());
            facade.Shutdown();
        }

        // Check that we go through a full process without throwing any exceptions
        [Test]
        [Category("Slow")]
        public void FullProcessTest()
        {
            this.FullProcess(this.GetInstance(this.options), true);
        }

        // Test that we can go through a whole cycle twice in a single process
        [Test]
        public void RepeatFullProcessTest()
        {
            this.FullProcess(this.GetInstance(this.options), true);
            this.FullProcess(this.GetInstance(this.options), true);
        }

        [Test]
        [Category("Slow")]
        public void LoginThrowsWhenAlreadyLoggedIn()
        {
            NetworkFacade facade = this.GetInstance(this.options);
            facade.Login(new UnverifiedIdentityProvider());
            Assert.Throws<InvalidOperationException>(
                () => facade.Login(new UnverifiedIdentityProvider()));
        }

        private void FullProcess(NetworkFacade facade, bool doUnregisterLeave)
        {
            bool succeeded = facade.Login(new UnverifiedIdentityProvider());
            Assert.IsTrue(succeeded, "Login failed");
            MockNetworkScene sceneCallbacks = new MockNetworkScene();

            NetworkScene scene = facade.JoinScene("scene", sceneCallbacks.InstantiateRemoteEntity, sceneCallbacks.RemoveObject);
            ISpatialOriginal obj = new MockNetworkObject();
            scene.RegisterEntity(obj, 0);
            facade.FlagForUpdate(obj, new BooleanArray(true));

            if (doUnregisterLeave)
            {
                scene.UnregisterEntity(obj);
                scene.Leave();
            }

            facade.Shutdown();
        }

        [Test]
        [Category("Slow")]
        public void MemoryLeakTest1()
        {
            this.MemoryLeakTest(true);
        }

        [Test]
        [Category("Slow")]
        public void MemoryLeakTest2()
        {
            this.MemoryLeakTest(false);
        }

        private void MemoryLeakTest(bool doUnregisterLeave)
        {
            // Do it once first so any reflection caches etc are built up
            this.FullProcess(this.GetInstance(this.options), doUnregisterLeave);

            long startMemory = GC.GetTotalMemory(true);
            //Debugger.Break();

            int iterations = 5;
            for (int i = 0; i < iterations; i++)
            {
                this.FullProcess(this.GetInstance(this.options), doUnregisterLeave);
            }

            long endMemory = GC.GetTotalMemory(true);
            double growthPerIteration = (double)(endMemory - startMemory) / iterations;
            //Debugger.Break();

            Assert.LessOrEqual(growthPerIteration, 3000);  // Small growth is ok, appears to be strings stored in an internal .NET hash table (System.Security.Util.Tokenizer+StringMaker).
        }
    }
}
