﻿//-----------------------------------------------------------------------
// <copyright file="IArbitrationManager.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Arbitration
{
    using System;
    using Badumna.Core;
    using Badumna.Security;
    using Badumna.ServiceDiscovery;
    using Badumna.Utilities;

    /// <summary>
    /// A delegate invoked on the arbitrator that processes messages arriving from clients.
    /// </summary>
    /// <param name="sessionId">An id that uniquely identifies the session</param>
    /// <param name="message">The message sent by the client</param>
    public delegate void HandleClientMessage(int sessionId, byte[] message);

    /// <summary>
    /// A delegate invoked on the arbitrator when a client disconnects or times out.
    /// </summary>
    /// <param name="sessionId">The id identifying the session</param>
    public delegate void HandleClientDisconnect(int sessionId);

    /// <summary>
    /// A delegate invoked on the client that processes messages arriving from the arbitrator.
    /// </summary>
    /// <param name="message">The message sent by the arbitrator</param>
    public delegate void HandleServerMessage(byte[] message);

    /// <summary>
    /// A delegate invoked on the client when the connection to the server fails for any reason.
    /// </summary>
    public delegate void HandleConnectionFailure();

    /// <summary>
    /// The Arbitration Manager ... TODO.
    /// </summary>
    internal interface IArbitrationManager
    {
        #region Properties

        /// <summary>
        /// Gets or sets the connection timeout.
        /// </summary>
        TimeSpan ConnectionTimeout { get; set; }

        /// <summary>
        /// Gets or sets the callback for when the arbitration server becomes offline.
        /// </summary>
        OnServiceBecomeOffline OnArbitrationServerBecomeOffline { get; set; }

        #endregion // Properties

        #region Methods

        /// <summary>
        /// Gets an arbitation client for the named aribtration server.
        /// </summary>
        /// <param name="name">The name of the arbitration server.</param>
        /// <returns>An arbitration client for the given server.</returns>
        IArbitrator GetArbitrator(string name);

        /// <summary>
        /// Register the arbitration handler with the network
        /// </summary>
        /// <param name="clientEvent">Handler for client messages.</param>
        /// <param name="disconnectTimeout">The disconnect timeout.</param>
        /// <param name="disconnect">Handler for client disconnection.</param>
        void RegisterArbitrationHandler(HandleClientMessage clientEvent, TimeSpan disconnectTimeout, HandleClientDisconnect disconnect);

        /// <summary>
        /// Sends a server message to a client in a given session.
        /// </summary>
        /// <param name="sessionId">The ID of the session the messag belongs to.</param>
        /// <param name="message">The message content.</param>
        void SendServerEvent(int sessionId, byte[] message);

        /// <summary>
        /// Send an notification to a peer that its message has been ignored.
        /// </summary>
        /// <param name="destinationAddress">The address of the peer to send the message to.</param>
        /// <param name="sequence">The sequence number of the rejected message.</param>
        /// <param name="message">The content of the rejected message.</param>
        void SendServerRejectionEvent(PeerAddress destinationAddress, CyclicalID.UShortID sequence, byte[] message);
        
        /// <summary>
        /// Connect the client's initial sequence number to the server.
        /// </summary>
        /// <param name="serverAddress">The address of the server.</param>
        /// <param name="initialSequence">The client's initial sequence number.</param>
        /// <param name="connectionFailure">A call back for connection failure.</param>
        void SendClientInitialSequence(
            PeerAddress serverAddress,
            CyclicalID.UShortID initialSequence,
            GenericCallBack connectionFailure);

        /// <summary>
        /// Send a client message to the server.
        /// </summary>
        /// <param name="serverAddress">The server address.</param>
        /// <param name="sequence">The event sequence number.</param>
        /// <param name="message">The message content.</param>
        /// <param name="connectionFailure">TODO: remove.</param>
        void SendClientEvent(
            PeerAddress serverAddress,
            CyclicalID.UShortID sequence,
            byte[] message,
            GenericCallBack connectionFailure);

        /// <summary>
        /// Send the server's initial sequence number to a client.
        /// </summary>
        /// <param name="clientAddress">The address of the client.</param>
        /// <param name="initialSequence">The initial sequence number.</param>
        void SendServerInitialSequence(PeerAddress clientAddress, CyclicalID.UShortID initialSequence);

        /// <summary>
        /// Send a sequenced server message to a client.
        /// </summary>
        /// <param name="clientAddress">The address of the client.</param>
        /// <param name="sequence">The sequence number of the message.</param>
        /// <param name="message">The message content.</param>
        void SendServerEvent(PeerAddress clientAddress, CyclicalID.UShortID sequence, byte[] message);

        /// <summary>
        /// Get the ID for the user associated with a given session.
        /// </summary>
        /// <param name="sessionId">The ID of the session.</param>
        /// <returns>The user ID.</returns>
        long GetUserIdForSession(int sessionId);

        /// <summary>
        /// Get the Character for the user associated with a given session.
        /// </summary>
        /// <param name="sessionId">The ID of the session.</param>
        /// <returns>The user ID.</returns>
        Character GetCharacterForSession(int sessionId);

        /// <summary>
        /// Connection lost event handler.
        /// </summary>
        /// <param name="address">The address.</param>
        void ConnectionLostEvent(PeerAddress address);

        /// <summary>
        /// Gets the server status.
        /// </summary>
        /// <returns>Arbitration server status.</returns>
        ArbitrationStatus GetServerStatus();

        #endregion // Methods
    }
}
