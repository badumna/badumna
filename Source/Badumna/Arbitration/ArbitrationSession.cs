﻿//-----------------------------------------------------------------------
// <copyright file="ArbitrationSession.cs" company="NICTA">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Arbitration
{
    using System;
    using System.Diagnostics;
    using Badumna.Core;
    using Badumna.Security;
    using Badumna.Utilities;

    /// <summary>
    /// Class to manage an arbitration session on the server for a particular client.
    /// </summary>
    internal class ArbitrationSession : IArbitrationSession
    {
        #region Fields

        /// <summary>
        /// Sequencer to order incoming messages.
        /// </summary>
        private readonly ISequencer<byte[]> sequencer;

        /// <summary>
        /// Sequence generator to label outgoing messages.
        /// </summary>
        private readonly SequenceGenerator sequenceGenerator;

        /// <summary>
        /// Generic callback for sending sequenced messages to clients.
        /// </summary>
        private readonly GenericCallBack<CyclicalID.UShortID, byte[]> sender;

        /// <summary>
        /// Arbitration session must be initialized before being used.
        /// </summary>
        private bool isInitialized = false;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        #endregion // Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the ArbitrationSession class.
        /// </summary>
        /// <param name="id">An ID to identify the session.</param>
        /// <param name="peerAddress">The address of the associated peer.</param>
        /// <param name="userCertificate">The user certificate of the associated peer.</param>
        /// <param name="sequencerFactory">The factory to create a Sequencer from.</param>
        /// <param name="handler">The callback to handle incoming messages.</param>
        /// <param name="sender">The callback to send outgoing messages.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        public ArbitrationSession(
            int id,
            PeerAddress peerAddress,
            ICertificateToken userCertificate,
            SequencerFactory<byte[]> sequencerFactory,
            GenericCallBack<byte[]> handler,
            GenericCallBack<CyclicalID.UShortID, byte[]> sender,
            ITime timeKeeper)
        {
            if (peerAddress == null)
            {
                throw new ArgumentNullException("peerAddress");
            }

            if (sequencerFactory == null)
            {
                throw new ArgumentNullException("sequencerFactory");
            }

            if (handler == null)
            {
                throw new ArgumentNullException("failureHandler");
            }

            if (sender == null)
            {
                throw new ArgumentNullException("sender");
            }

            if (timeKeeper == null)
            {
                throw new ArgumentNullException("timeKeeper");
            }

            this.timeKeeper = timeKeeper;
            this.Id = id;
            this.UserId = userCertificate.UserId;
            this.Character = userCertificate.Character;
            this.PeerAddress = peerAddress;
            this.sequencer = sequencerFactory(handler);
            this.sender = sender;
            this.sequenceGenerator = new SequenceGenerator();
            this.LastReceiveTime = this.timeKeeper.Now;
        }

        #endregion // Constructors

        #region Properties

        /// <summary>
        /// Gets the ID of the session.
        /// </summary>
        public int Id { get; private set; }

        /// <inheritdoc/>
        public long UserId { get; private set; }

        /// <inheritdoc/>
        public Character Character { get; private set; }

        /// <inheritdoc/>
        public PeerAddress PeerAddress { get; private set; }

        /// <summary>
        /// Gets the time a message was last received.
        /// </summary>
        public TimeSpan LastReceiveTime { get; private set; }
        
        /// <summary>
        /// Gets the sequence number that will be used for the next client message.
        /// </summary>
        public CyclicalID.UShortID NextOutgoingSequenceNumber { get; private set; }

        #endregion // Properties

        #region Public methods

        /// <summary>
        /// Initialize the session to expect a given sequence number.
        /// </summary>
        /// <param name="sequenceNumber">The sequence number to expect.</param>
        public void Initialize(CyclicalID.UShortID sequenceNumber)
        {
            this.isInitialized = true;
            this.sequencer.Initialize(sequenceNumber);
            this.NextOutgoingSequenceNumber = this.sequenceGenerator.NextSequenceNumber;
            this.LastReceiveTime = this.timeKeeper.Now;
        }

        /// <summary>
        /// Handle messages received from the client and passes them to the sequencer.
        /// </summary>
        /// <param name="sequenceNumber">The sequence number of the message.</param>
        /// <param name="message">The message content.</param>
        public void ReceiveMessage(CyclicalID.UShortID sequenceNumber, byte[] message)
        {
            Debug.Assert(this.isInitialized, "Attempt to use uninitialized ArbitrationSession");
            this.LastReceiveTime = this.timeKeeper.Now;
            this.sequencer.ReceiveEvent(sequenceNumber, message);
        }

        /// <summary>
        /// Send a message via the sender callback with a sequence number from the sequence generator.
        /// </summary>
        /// <param name="message">The message to send.</param>
        public void SendMessage(byte[] message)
        {
            this.sender.Invoke(this.sequenceGenerator.ConsumeSequenceNumber(), message);
        }

        #endregion // Public methods

        #region Private methods
        #endregion // Private methods
    }
}
