﻿//-----------------------------------------------------------------------
// <copyright file="ArbitrationSessionFactory.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Badumna.Arbitration
{
    using System;
    using Badumna.Core;
    using Badumna.Security;
    using Badumna.Utilities;

    /// <summary>
    /// Factory for creating ArbitrationSession%s
    /// </summary>
    internal class ArbitrationSessionFactory : IArbitrationSessionFactory
    {
        /// <summary>
        /// The sequencer factory to pass to created Arbitration Sessions.
        /// </summary>
        private readonly SequencerFactory<byte[]> sequencerFactory;

        /// <summary>
        /// Initializes a new instance of the ArbitrationSessionFactory class.
        /// </summary>
        /// <param name="sequencerFactory">An ISequencerFactory to pass to ArbitrationSessionFactories.</param>
        public ArbitrationSessionFactory(SequencerFactory<byte[]> sequencerFactory)
        {
            if (sequencerFactory == null)
            {
                throw new ArgumentNullException("sequencerFactory");
            }

            this.sequencerFactory = sequencerFactory;
        }

        /// <summary>
        /// Initializes a new instance of the ArbitrationSessionFactory class.
        /// </summary>
        public ArbitrationSessionFactory() :
            this(h => new Sequencer<byte[]>(h))
        {
        }

        /// <inheritdoc/>
        public IArbitrationSession Create(
            int id,
            PeerAddress peerAddress,
            ICertificateToken userCertificate,
            GenericCallBack<byte[]> handler,
            GenericCallBack<CyclicalID.UShortID, byte[]> sender,
            ITime timeKeeper)
        {
            return new ArbitrationSession(id, peerAddress, userCertificate, this.sequencerFactory, handler, sender, timeKeeper);
        }
    }
}
