﻿//-----------------------------------------------------------------------
// <copyright file="SequenceGenerator.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Arbitration
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using Badumna.Core;
    using Badumna.Utilities;

    /// <summary>
    /// Interface for servering up contiguously incrementing sequence numbers.
    /// </summary>
    internal interface ISequenceGenerator
    {
        /// <summary>
        /// Gets the sequence number that will be returned next by <c>ConsumeSequenceNumber</c>.
        /// </summary>
        CyclicalID.UShortID NextSequenceNumber { get; }

        /// <summary>
        /// Consume a sequence number.
        /// Atomically increments backing field so contiguous sequences
        /// can be served up in thread-safe manner.
        /// </summary>
        /// <returns>An consecutively increasing sequence number.</returns>
        CyclicalID.UShortID ConsumeSequenceNumber();

        /// <summary>
        /// Reset the sequence generator to a particular number.
        /// </summary>
        /// <param name="nextSequenceNumber">The next sequence number to generate.</param>
        void Reset(ushort nextSequenceNumber);
    }

    /// <summary>
    /// Class to servers up contiguously incrementing sequence numbers.
    /// </summary>
    /// <remarks>
    /// Thread safe.
    /// </remarks>
    internal class SequenceGenerator : ISequenceGenerator
    {
        /// <summary>
        /// Object to lock access to get NextSeqeunceNumber property to make it thread safe.
        /// </summary>
        private readonly object locker = new object();

        /// <summary>
        /// Field holding sequence number to be incremented to serve out sequences.
        /// </summary>
        private CyclicalID.UShortID nextSequenceNumber = new CyclicalID.UShortID(0);

        /// <inheritdoc/>
        public CyclicalID.UShortID NextSequenceNumber
        {
            get
            {
                return this.nextSequenceNumber;
            }
        }

        /// <inheritdoc/>
        public CyclicalID.UShortID ConsumeSequenceNumber()
        {
            lock (this.locker)
            {
                this.nextSequenceNumber.Increment();
                return this.nextSequenceNumber.Previous;
            }
        }

        /// <inheritdoc/>
        public void Reset(ushort nextSequenceNumber)
        {
            lock (this.locker)
            {
                this.nextSequenceNumber = new CyclicalID.UShortID(nextSequenceNumber);
            }
        }
    }
}
