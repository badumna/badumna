﻿//-----------------------------------------------------------------------
// <copyright file="IArbitrationSessionManager.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Badumna.Arbitration
{
    using Badumna.Core;
    using Badumna.Utilities;

    /// <summary>
    /// Interface for managing communication sessions including sequencing messages.
    /// </summary>
    /// <typeparam name="T">Type of messages to be sequenced.</typeparam>
    internal interface IArbitrationSessionManager<T>
    {
        #region Public methods

        /// <summary>
        /// Indicated whether a peer has been registered.
        /// </summary>
        /// <param name="address">The address of the peer.</param>
        /// <returns>A flag indicating whether the peer hsa been registered.</returns>
        bool PeerIsRegistered(PeerAddress address);

        /// <summary>
        /// Create and store a Sequencer and SequenceGenerator for use with a given peer.
        /// </summary>
        /// <param name="address">The address of the peer.</param>
        /// <param name="handler">The handler to pass sequenced messages to.</param>
        void RegisterPeer(PeerAddress address, GenericCallBack<T> handler);

        /// <summary>
        /// Get the sequencer for use with a given peer.
        /// </summary>
        /// <param name="address">The address of the peer.</param>
        /// <returns>The sequencer.</returns>
        Sequencer<T> GetSequencer(PeerAddress address);

        /// <summary>
        /// Get the sequence generator for use with a given peer.
        /// </summary>
        /// <param name="address">The address of the peer.</param>
        /// <returns>The sequence generator.</returns>
        SequenceGenerator GetSequenceGenerator(PeerAddress address);

        /// <summary>
        /// Remove sequencer and sequence generator used for a given peer.
        /// </summary>
        /// <param name="address">The address of the peer.</param>
        void RemovePeer(PeerAddress address);

        #endregion // Public methods
    }
}
