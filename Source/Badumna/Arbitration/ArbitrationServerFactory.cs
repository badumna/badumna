﻿//-----------------------------------------------------------------------
// <copyright file="ArbitrationServerFactory.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Badumna.Arbitration
{
    using System;
    using Badumna.Core;
    using Badumna.Utilities;

    /// <summary>
    /// Factory for creating ArbitrationServer%s
    /// </summary>
    internal class ArbitrationServerFactory : IArbitrationServerFactory
    {
        /// <summary>
        /// Create an arbitration server.
        /// </summary>
        /// <param name="manager">The arbitration manager that owns the server.</param>
        /// <param name="queueApplicationEvent">The method to use to queue events that must run in the application's thread.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        /// <returns>The arbitration server created.</returns>
        public IArbitrationServer Create(IArbitrationManager manager, QueueInvokable queueApplicationEvent, ITime timeKeeper)
        {
            return new ArbitrationServer(manager, queueApplicationEvent, timeKeeper);
        }
    }
}
