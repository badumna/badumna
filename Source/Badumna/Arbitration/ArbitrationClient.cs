﻿//-----------------------------------------------------------------------
// <copyright file="ArbitrationClient.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Arbitration
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using Badumna.Core;
    using Badumna.ServiceDiscovery;
    using Badumna.Utilities;

    /// <summary>
    /// TODO: Explain why class is used.
    /// </summary>
    internal class ArbitrationClient : ServiceConnection, IArbitrator
    {
        #region Fields

        /// <summary>
        /// The manager that owns this client.
        /// </summary>
        private readonly IArbitrationManager manager;

        /// <summary>
        /// Sequencer to order messages before processing them.
        /// </summary>
        private readonly ISequencer<byte[]> sequencer;

        /// <summary>
        /// Sequence generator to assign outgoing messages a sequence number.
        /// </summary>
        private SequenceGenerator sequenceGenerator;

        /// <summary>
        /// Flag indicating whether a connection to a server has been requested.
        /// </summary>
        /// <remarks>Used to automatically connect to servers found through discovery service if a connection has been requested.</remarks>
        private bool connectionRequested = false;

        /// <summary>
        /// Callback to report connection attempt result.
        /// </summary>
        private ArbitrationConnectionResultHandler connectionResultHandler = null;

        /// <summary>
        /// Callback to notify connection failure.
        /// </summary>
        private HandleConnectionFailure connectionFailedHandler = null;

        /// <summary>
        /// Handler for messages received from the server.
        /// </summary>
        private HandleServerMessage serverEventHandler = null;

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// The method to use to queue events that must run in the application's thread.
        /// </summary>
        private QueueInvokable queueApplicationEvent;

        /// <summary>
        /// Configuration options for the arbitration system.
        /// </summary>
        private IArbitrationModule config;

        #endregion // Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the ArbitrationClient class.
        /// </summary>
        /// <remarks>Uses passed sequencer (for testing).</remarks>
        /// <param name="manager">The arbitration manager that owns this client.</param>
        /// <param name="serverAddress">The address of the arbitration server.</param>
        /// <param name="serviceDescription">The service description.</param>
        /// <param name="sequencerFactory">A factory to create sequencers.</param>
        /// <param name="connectionTimeoutPeriod">How long to wait before timing out connection attempt.</param>
        /// <param name="config">Configuration options for the arbitration system.</param>
        /// <param name="eventQueue">The queue to use for scheduling events.</param>
        /// <param name="queueApplicationEvent">The method to use to queue events that must run in the application's thread.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        internal ArbitrationClient(
            IArbitrationManager manager,
            PeerAddress serverAddress,
            ServiceDescription serviceDescription,
            SequencerFactory<byte[]> sequencerFactory,
            TimeSpan connectionTimeoutPeriod,
            IArbitrationModule config,
            NetworkEventQueue eventQueue,
            QueueInvokable queueApplicationEvent,
            ITime timeKeeper) :
            base(connectionTimeoutPeriod, eventQueue)
        {
            // TODO: timeKeeper is not used. Remove.
            if (manager == null)
            {
                throw new ArgumentNullException("manager");
            }

            if (sequencerFactory == null)
            {
                throw new ArgumentNullException("sequencerFactory");
            }

            this.eventQueue = eventQueue;
            this.queueApplicationEvent = queueApplicationEvent;

            this.manager = manager;
            this.ServerAddress = serverAddress;
            this.sequencer = sequencerFactory(m => this.OnMessage(m));
            this.sequenceGenerator = new SequenceGenerator();
            this.ServiceDescription = serviceDescription;
            this.config = config;
        }

        /// <summary>
        /// Initializes a new instance of the ArbitrationClient class.
        /// </summary>
        /// <remarks>Uses passed sequencer (for testing).</remarks>
        /// <param name="manager">The arbitration manager that owns this client.</param>
        /// <param name="serverAddress">The address of the arbitration server.</param>
        /// <param name="serviceDescription">The service description.</param>
        /// <param name="sequencerFactory">A factory to create sequencers.</param>
        /// <param name="config">Configuration options for the arbitration system.</param>
        /// <param name="eventQueue">The queue to use for scheduling events.</param>
        /// <param name="queueApplicationEvent">The method to use to queue events that must run in the application's thread.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        internal ArbitrationClient(
            IArbitrationManager manager,
            PeerAddress serverAddress,
            ServiceDescription serviceDescription,
            SequencerFactory<byte[]> sequencerFactory,
            IArbitrationModule config,
            NetworkEventQueue eventQueue,
            QueueInvokable queueApplicationEvent,
            ITime timeKeeper) :
            this(manager, serverAddress, serviceDescription, sequencerFactory, Parameters.ArbitrationConnectionTimeoutTime, config, eventQueue, queueApplicationEvent, timeKeeper)
        {
        }

        /// <summary>
        /// Initializes a new instance of the ArbitrationClient class.
        /// </summary>
        /// <remarks>Constructs own sequencer.</remarks>
        /// <param name="manager">The arbitration manager that owns this client.</param>
        /// <param name="serverAddress">The address of the arbitration server.</param>
        /// <param name="serviceDescription">The service description.</param>
        /// <param name="config">Configuration options for the arbitration system.</param>
        /// <param name="eventQueue">The queue to use for scheduling events.</param>
        /// <param name="queueApplicationEvent">The method to use to queue events that must run in the application's thread.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        internal ArbitrationClient(
            IArbitrationManager manager,
            PeerAddress serverAddress,
            ServiceDescription serviceDescription,
            IArbitrationModule config,
            NetworkEventQueue eventQueue,
            QueueInvokable queueApplicationEvent,
            ITime timeKeeper) :
            this(
                manager,
                serverAddress,
                serviceDescription,
                h => new Sequencer<byte[]>(h),
                config,
                eventQueue,
                queueApplicationEvent,
                timeKeeper)
        {
        }

        #endregion // Constructors

        #region Properties

        /// <summary>
        /// Gets a value indicating whether the client is connected to a server.
        /// </summary>
        public bool IsServerConnected
        {
            get { return this.IsConnected; }
            private set { this.IsConnected = value; }
        }

        /// <summary>
        /// Gets and sets the server address.
        /// </summary>
        public PeerAddress ServerAddress { get; private set; }

        /// <summary>
        /// Gets the service description. Used to identify which service the client is for.
        /// </summary>
        public ServiceDescription ServiceDescription { get; private set; }

        #endregion // Properties

        #region Public Methods

        /// <summary>
        /// Connect to the arbitration server.
        /// </summary>
        /// <param name="connectionResultHandler">A callback that will be called when connection has successfully completed, or failed.</param>
        /// <param name="connectionFailedHandler">A callback that will be called if connection fails.</param>
        /// <param name="serverEventHandler">A handler to be triggered when the arbitrator sends an event to this client.</param>
        public void Connect(
            ArbitrationConnectionResultHandler connectionResultHandler,
            HandleConnectionFailure connectionFailedHandler,
            HandleServerMessage serverEventHandler)
        {
            if (connectionResultHandler == null)
            {
                throw new ArgumentNullException("connectionResultHandler");
            }

            if (connectionFailedHandler == null)
            {
                throw new ArgumentNullException("connectionFailedHandler");
            }

            if (serverEventHandler == null)
            {
                throw new ArgumentNullException("serverEventHandler");
            }

            this.connectionResultHandler = connectionResultHandler;
            this.connectionFailedHandler = connectionFailedHandler;
            this.serverEventHandler = serverEventHandler;

            // the app code can call the Connect method in any app thread, need to make sure it actually runs on the NetworkEventQueue. 
            this.eventQueue.Push(this.RequestConnection, new PeerAddress(this.ServerAddress));
        }

        /// <summary>
        /// Find out if the arbitration server is available.
        /// </summary>
        /// <remarks>
        /// Will return true if the arbitration client was recently created and so may not
        /// have connected to the server yet, otherwise returns connection status.
        /// </remarks>
        /// <returns>A flag indicating server availability.</returns>
        public bool IsArbitrationServerAvailable()
        {
            Logger.TraceInformation(LogTag.Arbitration, "Checking whether the current Arbitration client is available.");
            if (this.ServerAddress.Equals(PeerAddress.Nowhere))
            {
                Logger.TraceInformation(LogTag.Arbitration, "Server address is PeerAddress.Nowhere.");
                return false;
            }
            else
            {
                return this.IsAvailable();
            }
        }

        /// <summary>
        /// Callback for when the connection fails.
        /// </summary>
        public void OnConnectionFailure()
        {
            if (this.IsServerConnected)
            {
                this.IsServerConnected = false;
                HandleConnectionFailure handler = this.connectionFailedHandler;
                if (handler != null)
                {
                    this.queueApplicationEvent(Apply.Func(handler.Invoke));
                }
            }
        }

        /// <summary>
        /// Handle an arbitration message from the server.
        /// </summary>
        /// <param name="sequence">The sequence number of the message.</param>
        /// <param name="message">The message content.</param>
        public void ReceiveServerArbitrationEvent(CyclicalID.UShortID sequence, byte[] message)
        {
            this.sequencer.ReceiveEvent(sequence, message);
        }

        /// <summary>
        /// Handle an rejection message from the server.
        /// </summary>
        /// <param name="sequence">The sequence number of the message that was rejected.</param>
        /// <param name="message">The rejected message content.</param>
        public void ReceiveServerArbitrationRejection(CyclicalID.UShortID sequence, byte[] message)
        {
            // TODO: Rejections are sent inresponse to events, not initial sequences, so
            // I don't think the connection timeout event needs to be cancelled here.
            // connect to the server didn't timeout, so cancel the timeout event first. 
            ////this.CancelConnectionTimeoutEvent();

            // Treat as a connnection failure
            if (this.IsServerConnected)
            {
                this.manager.ConnectionLostEvent(new PeerAddress(this.ServerAddress));
            }
        }

        /// <summary>
        /// Handle the initial sequence number from the server.
        /// </summary>
        /// <remarks>Messages are passed ot he sequencer for ordeirng before processing.</remarks>
        /// <param name="initialSequence">The initial sequence number.</param>
        public void ReceiveServerInitialSequence(CyclicalID.UShortID initialSequence)
        {
            if (!this.IsConnecting)
            {
                // recieved server initial sequence when not connecting. the server initial sequence message
                // will be ignored.
                return;
            }

            // connect to the server didn't timeout, so cancel the timeout event first. 
            this.CancelConnectionTimeoutEvent();

            this.IsConnecting = false;
            this.IsServerConnected = true;
            this.sequencer.Initialize(initialSequence);
            if (this.connectionResultHandler != null)
            {
                this.queueApplicationEvent(Apply.Func(this.connectionResultHandler.Invoke, ServiceConnectionResultType.Success));
            }
        }

        /// <summary>
        /// Send a message to the server.
        /// </summary>
        /// <remarks>Will lazily connect. TODO - handshaking.</remarks>
        /// <param name="message">The message content.</param>
        public void SendEvent(byte[] message)
        {
            if (message == null)
            {
                throw new ArgumentNullException("message");
            }

            if (this.ServerAddress.Equals(PeerAddress.Nowhere))
            {
                return;
            }

            // SendEvent is called by the app code on any app thread, need to make sure the actual send is called on 
            // the network event queue thread. 
            this.eventQueue.Push(this.DoSendEvent, new PeerAddress(this.ServerAddress), message);
        }

        /// <summary>
        /// Switch to a new arbitration server.
        /// </summary>
        /// <param name="address">The address of the new server. Must not be PeerAddress.Nowhere.</param>
        public void SwitchToNewServer(PeerAddress address)
        {
            Debug.Assert(address != PeerAddress.Nowhere, "Cannot switch to server with address PeerAddress.Nowhere");

            this.IsServerConnected = false;
            this.ServerAddress = address;

            // TODO: Connect if previously requested.
            if (this.connectionRequested)
            {
                Logger.TraceInformation(LogTag.Arbitration, "Calling RequestConnection.");
                this.connectionRequested = false;
                this.RequestConnection(this.ServerAddress);
            }
            else
            {
                Logger.TraceInformation(LogTag.Arbitration, "Not calling RequestConnection.");
            }
        }

        /// <summary>
        /// Changeds the state to server unknown.
        /// </summary>
        public void ChangedToServerUnknownState()
        {
            ArbitrationServerDetails details = this.config.GetServerDetails(this.ServiceDescription.ServiceType);
            if (details != null && details.UseDistributedLookup)
            {
                this.ServerAddress = PeerAddress.Nowhere;
            }
        }

        #endregion // Public Methods

        #region Protected methods

        /// <summary>
        /// Called via base class when attempt to connect to server times out.
        /// </summary>
        protected override void ConnectionTimeoutHandler()
        {
            ArbitrationConnectionResultHandler handler = this.connectionResultHandler;
            if (handler != null)
            {
                // change to server unknown state if using service discovery.
                // this is basically saying do not connect until the service discovery module say so. 
                if (this.ConnectionResultType == ServiceConnectionResultType.ConnectionTimeout)
                {
                    this.ChangedToServerUnknownState();    
                }

                this.queueApplicationEvent(Apply.Func(handler.Invoke, this.ConnectionResultType));
            }
        }

        #endregion // Protected methods

        #region Private Methods

        /// <summary>
        /// Sends initial sequence to server (via manager).
        /// </summary>
        /// <param name="serverAddress">The server address.</param>
        private void RequestConnection(PeerAddress serverAddress)
        {
            if (serverAddress.Equals(PeerAddress.Nowhere))
            {
                this.connectionRequested = true;
                return;
            }

            this.ScheduleConnectionTimeoutEvent();
            this.IsConnecting = true;
            CyclicalID.UShortID sequence = this.sequenceGenerator.NextSequenceNumber;
            Logger.TraceInformation(LogTag.Arbitration, "Sending client initial sequence to {0}, sequence number is {1}", serverAddress, sequence.Value);

            this.manager.SendClientInitialSequence(serverAddress, sequence, this.OnConnectionFailure);
        }

        /// <summary>
        /// Sends the event. This method should only be called on the network event queue thread.
        /// </summary>
        /// <param name="serverAddress">The server address.</param>
        /// <param name="message">The message.</param>
        private void DoSendEvent(PeerAddress serverAddress, byte[] message)
        {
            if (serverAddress.Equals(PeerAddress.Nowhere))
            {
                return;
            }

            this.manager.SendClientEvent(serverAddress, this.sequenceGenerator.ConsumeSequenceNumber(), message, this.OnConnectionFailure);
        }

        /// <summary>
        /// Message handler passed to sequencer to handle messages.
        /// </summary>
        /// <param name="message">The message content.</param>
        private void OnMessage(byte[] message)
        {
            HandleServerMessage handler = this.serverEventHandler;
            if (handler != null)
            {
                this.queueApplicationEvent(Apply.Func(
                    delegate { handler(message); }));
            }
        }

        #endregion // Private Methods
    }
}
