﻿//---------------------------------------------------------------------------------
// <copyright file="ArbitrationStatus.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using Badumna.Core;

namespace Badumna.Arbitration
{
    /// <summary>
    /// The status of the arbitration server.
    /// </summary>
    internal class ArbitrationStatus
    {
        /// <summary>
        /// The number of sessions being maintained by the arbitration server.
        /// </summary>
        private int numberOfClients;

        /// <summary>
        /// The number of requests received.
        /// </summary>
        private int numberOfReceivedRequests;

        /// <summary>
        /// The number of requests handled.
        /// </summary>
        private int numberOfHandledRequests;

        /// <summary>
        /// The number of response messages sent to clients.
        /// </summary>
        private int numberOfSentMessages;

        /// <summary>
        /// The last stats collection time.
        /// </summary>
        private TimeSpan lastCollectTime;

        /// <summary>
        /// The interval.
        /// </summary>
        private TimeSpan interval;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// Initializes a new instance of the <see cref="ArbitrationStatus"/> class.
        /// </summary>
        /// <param name="timeKeeper">The time keeper object.</param>
        public ArbitrationStatus(ITime timeKeeper)
        {
            this.Clear();
            this.timeKeeper = timeKeeper;
            this.lastCollectTime = timeKeeper.Now;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ArbitrationStatus"/> class.
        /// </summary>
        /// <param name="other">The other object to copy from.</param>
        public ArbitrationStatus(ArbitrationStatus other)
        {
            this.numberOfClients = other.numberOfClients;
            this.numberOfReceivedRequests = other.numberOfReceivedRequests;
            this.numberOfHandledRequests = other.numberOfHandledRequests;
            this.numberOfSentMessages = other.numberOfSentMessages;
            this.lastCollectTime = other.lastCollectTime;
            this.interval = other.interval;
            this.timeKeeper = other.timeKeeper;
        }

        /// <summary>
        /// Gets or sets the number of clients.
        /// </summary>
        /// <value>
        /// The number of clients.
        /// </value>
        public int NumberOfClients
        {
            get { return this.numberOfClients; }
            set { this.numberOfClients = value; }
        }

        /// <summary>
        /// Gets or sets the number of received requests.
        /// </summary>
        /// <value>
        /// The number of received requests.
        /// </value>
        public int NumberOfReceivedRequests
        {
            get { return this.numberOfReceivedRequests; }
            set { this.numberOfReceivedRequests = value; }
        }

        /// <summary>
        /// Gets or sets the number of handled requests.
        /// </summary>
        /// <value>
        /// The number of handled requests.
        /// </value>
        public int NumberOfHandledRequests
        {
            get { return this.numberOfHandledRequests; }
            set { this.numberOfHandledRequests = value; }
        }

        /// <summary>
        /// Gets or sets the number of sent messages.
        /// </summary>
        /// <value>
        /// The number of sent messages.
        /// </value>
        public int NumberOfSentMessages
        {
            get { return this.numberOfSentMessages; }
            set { this.numberOfSentMessages = value; }
        }

        /// <summary>
        /// Gets the interval.
        /// </summary>
        public TimeSpan Interval
        {
            get { return this.interval; }
        }

        /// <summary>
        /// Updates the interval.
        /// </summary>
        public void UpdateInterval()
        {
            this.interval = this.timeKeeper.Now - this.lastCollectTime;
            this.lastCollectTime = this.timeKeeper.Now;
        }

        /// <summary>
        /// Clears this instance.
        /// </summary>
        private void Clear()
        {
            this.numberOfClients = 0;
            this.numberOfReceivedRequests = 0;
            this.numberOfHandledRequests = 0;
            this.numberOfSentMessages = 0;
        }
    }
}
