﻿//-----------------------------------------------------------------------
// <copyright file="IArbitrationSession.cs" company="NICTA">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Arbitration
{
    using System;
    using Badumna.Core;
    using Badumna.Security;
    using Badumna.Utilities;

    /// <summary>
    /// Class to manage an arbitration session on the server for a particular client.
    /// </summary>
    internal interface IArbitrationSession
    {
        #region Properties

        /// <summary>
        /// Gets the ID of the session.
        /// </summary>
        int Id { get; }

        /// <summary>
        /// Gets the user ID of the client connected to this session.
        /// </summary>
        long UserId { get; }

        /// <summary>
        /// Gets the Character of the client connected to this session.
        /// </summary>
        Character Character { get; }

        /// <summary>
        /// Gets the address of the client peer for this session.
        /// </summary>
        PeerAddress PeerAddress { get; }

        /// <summary>
        /// Gets the time a message was last received.
        /// </summary>
        TimeSpan LastReceiveTime { get; }

        /// <summary>
        /// Gets the sequence number that will be used for the next client message.
        /// </summary>
        CyclicalID.UShortID NextOutgoingSequenceNumber { get; }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Initialize the session to expect a given sequence number.
        /// </summary>
        /// <param name="sequenceNumber">The sequence number to expect.</param>
        void Initialize(CyclicalID.UShortID sequenceNumber);

        /// <summary>
        /// Receive message from client to be sequenced and handled in order.
        /// </summary>
        /// <param name="sequenceNumber">Sequence number of message.</param>
        /// <param name="message">Message content.</param>
        void ReceiveMessage(CyclicalID.UShortID sequenceNumber, byte[] message);

        /// <summary>
        /// Send a message to client with sequence number.
        /// </summary>
        /// <param name="message">The message to send.</param>
        void SendMessage(byte[] message);

        #endregion // Public methods
    }
}
