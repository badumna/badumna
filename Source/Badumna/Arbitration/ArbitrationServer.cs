﻿//-----------------------------------------------------------------------
// <copyright file="ArbitrationServer.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Arbitration
{
    using System;
    using System.Collections.Generic;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Security;
    using Badumna.Utilities;

    /// <summary>
    /// Handles communications with an arbitration client.
    /// Responsibilities include ensuring received messages are properly ordered,
    /// and passing messages on to appropriate message handlers.
    /// </summary>
    internal class ArbitrationServer : IArbitrationServer
    {
        #region Fields

        /// <summary>
        /// Object for synchronizing access to the sessionById dictionary.
        /// </summary>
        private readonly object sessionsByIdLock = new object();

        /// <summary>
        /// The arbitration manager that owns this server (dependency).
        /// </summary>
        private readonly IArbitrationManager arbitrationManager;

        /// <summary>
        /// A factory for creating sessions (dependecy).
        /// </summary>
        private readonly IArbitrationSessionFactory arbitrationSessionFactory;

        /// <summary>
        /// Map storing sessions by client address.
        /// </summary>
        private Dictionary<PeerAddress, IArbitrationSession> sessionsByClientAddress = new Dictionary<PeerAddress, IArbitrationSession>();

        /// <summary>
        /// Map storing sessions by session ID.
        /// </summary>
        private Dictionary<int, IArbitrationSession> sessionsById = new Dictionary<int, IArbitrationSession>();

        /// <summary>
        /// Used to set ID numbers to identify each session.
        /// </summary>
        private int nextSessionId;

        /// <summary>
        /// The method to use to queue events that must run in the application's thread.
        /// </summary>
        private QueueInvokable queueApplicationEvent;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// The number of requests received in the last reporting period.
        /// </summary>
        private int numberOfReceivedRequests;

        /// <summary>
        /// The number of requests handled in the last reporting period.
        /// </summary>
        private int numberOfHandledRequests;

        /// <summary>
        /// The number of messages sent to clients in the last reporting period.
        /// </summary>
        private int numberOfSentMessages;

        /// <summary>
        /// The arbitration stats.
        /// </summary>
        private ArbitrationStatus arbitrationStatus;

        #endregion //Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the ArbitrationServer class.
        /// </summary>
        /// <param name="arbitrationManager">The arbitration manager that owns this server.</param>
        /// <param name="arbitrationSessionFactory">The factory to create sessions.</param>
        /// <param name="queueApplicationEvent">The method to use to queue events that must run in the application's thread.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        public ArbitrationServer(
            IArbitrationManager arbitrationManager,
            IArbitrationSessionFactory arbitrationSessionFactory,
            QueueInvokable queueApplicationEvent,
            ITime timeKeeper)
        {
            if (arbitrationManager == null)
            {
                throw new ArgumentNullException("arbitrationManager");
            }

            if (arbitrationSessionFactory == null)
            {
                throw new ArgumentNullException("arbitrationSessionFactory");
            }

            if (queueApplicationEvent == null)
            {
                throw new ArgumentNullException("applicationEventQueue");
            }

            if (timeKeeper == null)
            {
                throw new ArgumentNullException("timeKeeper");
            }

            this.arbitrationManager = arbitrationManager;
            this.arbitrationSessionFactory = arbitrationSessionFactory;
            this.queueApplicationEvent = queueApplicationEvent;
            this.timeKeeper = timeKeeper;

            this.numberOfReceivedRequests = 0;
            this.numberOfHandledRequests = 0;
            this.numberOfSentMessages = 0;

            this.arbitrationStatus = new ArbitrationStatus(timeKeeper);
        }

        /// <summary>
        /// Initializes a new instance of the ArbitrationServer class.
        /// </summary>
        /// <param name="arbitrationManager">The arbitration manager that owns this server.</param>
        /// <param name="queueApplicationEvent">The method to use to queue events that must run in the application's thread.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        public ArbitrationServer(IArbitrationManager arbitrationManager, QueueInvokable queueApplicationEvent, ITime timeKeeper) :
            this(arbitrationManager, new ArbitrationSessionFactory(), queueApplicationEvent, timeKeeper)
        {
        }

        #endregion // Constructors

        #region Properties

        /// <summary>
        /// Gets or sets a handler for client events.
        /// </summary>
        public HandleClientMessage ClientEvent { get; set; }

        /// <summary>
        /// Gets or sets and handler for client disconnections.
        /// </summary>
        public HandleClientDisconnect Disconnect { get; set; }

        #endregion // Properties

        #region Public methods

        /// <summary>
        /// Checks for timed out connections and gets rid of them.
        /// </summary>
        public void CollectGarbage()
        {
            List<IArbitrationSession> expiredSessions = new List<IArbitrationSession>();
            List<PeerAddress> expiredClients = new List<PeerAddress>();
            foreach (KeyValuePair<PeerAddress, IArbitrationSession> sessionByAddress in this.sessionsByClientAddress)
            {
                if (sessionByAddress.Value.LastReceiveTime + this.arbitrationManager.ConnectionTimeout < this.timeKeeper.Now)
                {
                    expiredSessions.Add(sessionByAddress.Value);
                    expiredClients.Add(sessionByAddress.Key);
                }
            }

            foreach (PeerAddress client in expiredClients)
            {
                this.sessionsByClientAddress.Remove(client);
            }

            foreach (IArbitrationSession session in expiredSessions)
            {
                lock (this.sessionsByIdLock)
                {
                    this.sessionsById.Remove(session.Id);
                }

                HandleClientDisconnect handler = this.Disconnect;
                if (handler != null)
                {
                    var id = session.Id;
                    this.queueApplicationEvent(Apply.Func(
                            delegate { handler(id); }));
                }
            }
        }

        /// <inheritdoc/>
        public long GetUserIdForSession(int sessionId)
        {
            IArbitrationSession session = this.SessionForId(sessionId);
            return session == null ? -1 : session.UserId;
        }

        /// <inheritdoc/>
        public Character GetCharacterForSession(int sessionId)
        {
            IArbitrationSession session = this.SessionForId(sessionId);
            return session == null ? null : session.Character;
        }

        /// <inheritdoc/>
        public BadumnaId GetBadumnaIdForSession(int sessionId)
        {
            IArbitrationSession session = this.SessionForId(sessionId);
            return session == null ? BadumnaId.None : new BadumnaId(session.PeerAddress, 0);
        }

        /// <summary>
        /// Sends an event to a session.
        /// </summary>
        /// <param name="sessionId">The sesion to send the event to.</param>
        /// <param name="message">The message to send.</param>
        public void SendEvent(int sessionId, byte[] message)
        {
            if (message == null)
            {
                throw new ArgumentNullException("message");
            }

            IArbitrationSession session = null;
            bool success = false;
            lock (this.sessionsByIdLock)
            {
                success = this.sessionsById.TryGetValue(sessionId, out session);
            }

            if (success)
            {
                session.SendMessage(message);
            }
            else
            {
                throw new InvalidOperationException("No connection for given sessionId (may have timed out?)");
            }
        }

        /// <inheritdoc/>
        public void ReceiveClientInitialSequence(PeerAddress clientAddress, ICertificateToken userCertificate, CyclicalID.UShortID initialSequence)
        {
            IArbitrationSession session = null;
            if (!this.sessionsByClientAddress.TryGetValue(clientAddress, out session))
            {
                int sessionId = this.nextSessionId;
                this.nextSessionId++;
                session = this.arbitrationSessionFactory.Create(
                    sessionId,
                    clientAddress,
                    userCertificate,
                    (message) => this.InvokeHandler(sessionId, message),
                    (seq, message) => this.SendMessageToClient(clientAddress, seq, message),
                    this.timeKeeper);
                this.sessionsByClientAddress.Add(clientAddress, session);
                lock (this.sessionsByIdLock)
                {
                    this.sessionsById.Add(sessionId, session);
                }
            }

            session.Initialize(initialSequence);
            this.arbitrationManager.SendServerInitialSequence(clientAddress, session.NextOutgoingSequenceNumber);
        }

        /// <summary>
        /// Handle arbitration requests from the client.
        /// </summary>
        /// <param name="source">The address of the client.</param>
        /// <param name="sequence">The sequence number of the message.</param>
        /// <param name="message">The message.</param>
        public void ReceiveClientArbitrationEvent(PeerAddress source, CyclicalID.UShortID sequence, byte[] message)
        {
            this.numberOfReceivedRequests++;

            IArbitrationSession session = null;
            if (this.sessionsByClientAddress.TryGetValue(source, out session))
            {
                session.ReceiveMessage(sequence, message);
            }
            else
            {
                this.arbitrationManager.SendServerRejectionEvent(source, sequence, message);
            }
        }

        /// <summary>
        /// Updates the stats.
        /// </summary>
        /// <returns>
        /// The latest stats.
        /// </returns>
        public ArbitrationStatus UpdateStats()
        {
            lock (this.sessionsByIdLock)
            {
                this.arbitrationStatus.NumberOfClients = this.sessionsById.Count;
            }

            this.arbitrationStatus.NumberOfReceivedRequests = this.numberOfReceivedRequests;
            this.arbitrationStatus.NumberOfHandledRequests = this.numberOfHandledRequests;
            this.arbitrationStatus.NumberOfSentMessages = this.numberOfSentMessages;
            this.arbitrationStatus.UpdateInterval();

            this.numberOfReceivedRequests = 0;
            this.numberOfHandledRequests = 0;
            this.numberOfSentMessages = 0;

            return this.arbitrationStatus;
        }

        #endregion // Public methods

        /// <summary>
        /// Gets the session for a given sessionId in a thread-safe way.
        /// </summary>
        /// <param name="sessionId">The session id.</param>
        /// <returns>The session, or null if not found.</returns>
        internal IArbitrationSession SessionForId(int sessionId)
        {
            lock (this.sessionsByIdLock)
            {
                IArbitrationSession session = null;
                this.sessionsById.TryGetValue(sessionId, out session);
                return session;
            }
        }

        #region Private methods

        /// <summary>
        /// Invoke the message handler for received and sequenced messages.
        /// </summary>
        /// <param name="sessionId">The session the message belongs to.</param>
        /// <param name="message">The message.</param>
        private void InvokeHandler(int sessionId, byte[] message)
        {
            this.numberOfHandledRequests++;

            HandleClientMessage handler = this.ClientEvent;
            if (handler != null)
            {
                this.queueApplicationEvent(Apply.Func(
                    delegate { handler(sessionId, message); }));
            }
        }

        /// <summary>
        /// Send a message to a client.
        /// </summary>
        /// <param name="clientAddress">The address of the client.</param>
        /// <param name="sequenceNumber">The sequence number for the message.</param>
        /// <param name="message">The message content.</param>
        private void SendMessageToClient(
            PeerAddress clientAddress,
            CyclicalID.UShortID sequenceNumber,
            byte[] message)
        {
            this.numberOfSentMessages++;

            this.arbitrationManager.SendServerEvent(
                clientAddress,
                sequenceNumber,
                message);
        }
        #endregion // Private methods
    }
}
