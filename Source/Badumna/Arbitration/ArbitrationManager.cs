﻿//-----------------------------------------------------------------------
// <copyright file="ArbitrationManager.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Arbitration
{
    using System;
    using System.Collections.Generic;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Security;
    using Badumna.ServiceDiscovery;
    using Badumna.Transport;
    using Badumna.Utilities;

    /// <summary>
    /// The Arbitration Manager ... TODO.
    /// </summary>
    internal class ArbitrationManager : IArbitrationManager, IServiceConsumer
    {
        #region Fields

        /// <summary>
        /// The parent transport protocol, responsible for messaging.
        /// </summary>
        private readonly TransportProtocol parent;

        /// <summary>
        /// The configuration modele specifying arbitration services.
        /// </summary>
        private readonly IArbitrationModule arbitrationModule;

        /// <summary>
        /// TODO ... field comment.
        /// </summary>
        private readonly IArbitrationServer server;

        /// <summary>
        /// TODO ... field comment.
        /// </summary>
        private List<ArbitrationClient> clients = new List<ArbitrationClient>();

        /// <summary>
        /// TODO ... field comment.
        /// </summary>
        private RegularTask garbageTask;

        /// <summary>
        /// TODO ... field comment.
        /// </summary>
        private TimeSpan connectionTimeout;
        
        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// The method to use to queue events that must run in the application's thread.
        /// </summary>
        private QueueInvokable queueApplicationEvent;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// The regular task for collecting stats.
        /// </summary>
        private RegularTask statsCollectionTask;

        /// <summary>
        /// The arbitration stats.
        /// </summary>
        private ArbitrationStatus arbitrationStatus;

        /// <summary>
        /// Whether the local process is an arbitration server or not. 
        /// </summary>
        private bool isServer;

        /// <summary>
        /// Reports on network connectivity status.
        /// </summary>
        private INetworkConnectivityReporter connectivityReporter;

        #endregion // Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the ArbitrationManager class.
        /// </summary>
        /// <param name="parent">The parent transport protocol.</param>
        /// <param name="arbitrationModule">The arbitration module specifying arbitration services.</param>
        /// <param name="serverFactory">The factory for creating arbitration servers.</param>
        /// <param name="eventQueue">The queue to use for scheduling events.</param>
        /// <param name="queueApplicationEvent">The method to use to queue events that must run in the application's thread.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        /// <param name="connectionNotifier">Provides notification of peer connections and disconnections.</param>
        public ArbitrationManager(
            TransportProtocol parent,
            IArbitrationModule arbitrationModule,
            IArbitrationServerFactory serverFactory,
            NetworkEventQueue eventQueue,
            QueueInvokable queueApplicationEvent,
            ITime timeKeeper,
            INetworkConnectivityReporter connectivityReporter,
            IPeerConnectionNotifier connectionNotifier)
        {
            if (parent == null)
            {
                throw new ArgumentNullException("parent");
            }

            if (arbitrationModule == null)
            {
                throw new ArgumentNullException("arbitrationModule");
            }

            if (serverFactory == null)
            {
                throw new ArgumentNullException("serverFactory");
            }

            if (eventQueue == null)
            {
                throw new ArgumentNullException("eventQueue");
            }

            if (queueApplicationEvent == null)
            {
                throw new ArgumentNullException("queueApplicationEvent");
            }

            if (timeKeeper == null)
            {
                throw new ArgumentNullException("timeKeeper");
            }

            if (connectivityReporter == null)
            {
                throw new ArgumentNullException("connectivityReporter");
            }

            if (connectionNotifier == null)
            {
                throw new ArgumentNullException("connectionNotifier");
            }

            this.isServer = false;

            this.eventQueue = eventQueue;
            this.queueApplicationEvent = queueApplicationEvent;
            this.timeKeeper = timeKeeper;
            this.connectivityReporter = connectivityReporter;

            this.parent = parent;
            this.parent.Parser.RegisterMethodsIn(this);

            this.arbitrationModule = arbitrationModule;

            this.server = serverFactory.Create(this, this.queueApplicationEvent, this.timeKeeper);

            this.statsCollectionTask = new RegularTask(
                "arbitration stats collection",
                Parameters.ArbitrationServerStatsCollectionInterval,
                this.eventQueue,
                this.connectivityReporter,
                this.CollectStats);
            this.statsCollectionTask.Start();
            this.arbitrationStatus = new ArbitrationStatus(this.timeKeeper);

            connectionNotifier.ConnectionLostEvent += this.ConnectionLostEvent;
        }

        /// <summary>
        /// Initializes a new instance of the ArbitrationManager class.
        /// </summary>
        /// <param name="parent">The parent transport protocol.</param>
        /// <param name="arbitrationModule">The arbitration module specifying arbitration services.</param>
        /// <param name="eventQueue">The queue to use for scheduling events.</param>
        /// <param name="queueApplicationEvent">The method to use to queue events that must run in the application's thread.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        /// <param name="connectionNotifier">Provides notification of peer connections and disconnections.</param>
        public ArbitrationManager(
            TransportProtocol parent,
            IArbitrationModule arbitrationModule,
            NetworkEventQueue eventQueue,
            QueueInvokable queueApplicationEvent,
            ITime timeKeeper,
            INetworkConnectivityReporter connectivityReporter,
            IPeerConnectionNotifier connectionNotifier) :
            this(parent, arbitrationModule, new ArbitrationServerFactory(), eventQueue, queueApplicationEvent, timeKeeper, connectivityReporter, connectionNotifier)
        {
        }

        #endregion // Constructors

        #region Properties

        /// <summary>
        /// Gets or sets the callback for when the arbitration server becomes offline.
        /// </summary>
        public OnServiceBecomeOffline OnArbitrationServerBecomeOffline { get; set; }

        /// <summary>
        /// Gets or sets the connection timeout.
        /// </summary>
        public TimeSpan ConnectionTimeout
        {
            get
            {
                return this.connectionTimeout;
            }

            set
            {
                if (this.connectionTimeout == value)
                {
                    return;
                }

                this.connectionTimeout = value;

                TimeSpan checkPeriod = TimeSpan.FromTicks(value.Ticks / 2);

                if (this.garbageTask == null)
                {
                    this.garbageTask = new RegularTask("Arbitration garbage collector", checkPeriod, this.eventQueue, this.connectivityReporter, this.CollectGarbage);
                }
                else
                {
                    this.garbageTask.Stop();
                    this.garbageTask.PeriodMilliseconds = checkPeriod.TotalMilliseconds;
                }

                this.garbageTask.Start();
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is server.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is server; otherwise, <c>false</c>.
        /// </value>
        public bool IsServer
        {
            get { return this.isServer; }
        }

        #endregion // Properties

        #region Public methods
        
        /// <summary>
        /// Gets an arbitation client for the named arbitration server.
        /// </summary>
        /// <param name="name">The name of the arbitration server.</param>
        /// <returns>An arbitration client for the given server.</returns>
        public IArbitrator GetArbitrator(string name)
        {
            Logger.TraceInformation(LogTag.Arbitration, "GetArbitrator ({0}) is called.", name);
            if (!this.arbitrationModule.IsEnabled)
            {
                throw new InvalidOperationException("Arbitration module is not enabled");
            }

            ArbitrationServerDetails details = this.arbitrationModule.GetServerDetails(name.ToLower());
            if (null == details)
            {
                throw new InvalidOperationException("Arbitration server with the name " + name + " is not found in the configuration");
            }

            ServiceDescription serviceDescription = new ServiceDescription(ServerType.Arbitration, details.Name);

            if (details.UseDistributedLookup)
            {
                return this.GetClient(PeerAddress.Nowhere, serviceDescription);
            }
            else
            {
                return this.GetClient(details.PeerAddress, serviceDescription);
            }
        }

        /// <summary>
        /// Register the arbitration handler with the network
        /// </summary>
        /// <param name="clientEvent">Handler for client messages.</param>
        /// <param name="disconnectTimeout">The disconnect timeout.</param>
        /// <param name="disconnect">Handler for client disconnection.</param>
        public void RegisterArbitrationHandler(HandleClientMessage clientEvent, TimeSpan disconnectTimeout, HandleClientDisconnect disconnect)
        {
            this.isServer = true;
            this.server.ClientEvent = clientEvent;
            this.server.Disconnect = disconnect;
            this.ConnectionTimeout = disconnectTimeout;
        }

        /// <summary>
        /// Send an notification to a peer that its message has been ignored.
        /// </summary>
        /// <param name="destinationAddress">The address of the peer to send the rejection message to.</param>
        /// <param name="sequence">The sequence number of the rejected message.</param>
        /// <param name="message">The content of the rejected message.</param>
        public void SendServerRejectionEvent(PeerAddress destinationAddress, CyclicalID.UShortID sequence, byte[] message)
        {
            Logger.TraceInformation(LogTag.Arbitration, "Sending server rejection event to {0}, sequence is {1}", destinationAddress, sequence.Value);
            TransportEnvelope envelope = this.parent.GetMessageFor(destinationAddress, QualityOfService.Reliable);
            this.parent.RemoteCall(envelope, this.ReceiveServerArbitrationRejection, sequence, message);
            this.parent.SendMessage(envelope);
        }

        /// <summary>
        /// Sends a message to a client.
        /// </summary>
        /// <param name="sessionId">The ID of the session the message belongs to.</param>
        /// <param name="message">The message content.</param>
        public void SendServerEvent(int sessionId, byte[] message)
        {
            this.server.SendEvent(sessionId, message);
        }

        /// <summary>
        /// Connect the client's initial sequence number to the server.
        /// </summary>
        /// <param name="serverAddress">The address of the server.</param>
        /// <param name="initialSequence">The client's initial sequence number.</param>
        /// <param name="connectionFailure">A call back for connection failure.</param>
        public void SendClientInitialSequence(
            PeerAddress serverAddress,
            CyclicalID.UShortID initialSequence,
            GenericCallBack connectionFailure)
        {
            Logger.TraceInformation(LogTag.Arbitration, "Trying to send client initial sequence to server {0}, sequence is {1}", serverAddress, initialSequence.Value);
            TransportEnvelope envelope = this.parent.GetMessageFor(serverAddress, QualityOfService.Reliable);
            this.parent.RemoteCall(envelope, this.ArbitrationClientInitialSequence, initialSequence);
            this.parent.SendMessage(envelope);
        }

        /// <summary>
        /// Send a client message to the server.
        /// </summary>
        /// <param name="serverAddress">The server address.</param>
        /// <param name="sequence">The event sequence number.</param>
        /// <param name="message">The message content.</param>
        /// <param name="connectionFailure">TODO: remove.</param>
        public void SendClientEvent(
            PeerAddress serverAddress,
            CyclicalID.UShortID sequence,
            byte[] message,
            GenericCallBack connectionFailure)
        {
            // TODO: Check correct QoS being used!
            Logger.TraceInformation(LogTag.Arbitration, "Sending client event to server {0}, seq number is {1}.", serverAddress, sequence.Value);
            TransportEnvelope envelope = this.parent.GetMessageFor(serverAddress, QualityOfService.Reliable);
            this.parent.RemoteCall(envelope, this.ReceiveClientArbitrationEvent, sequence, message);
            this.parent.SendMessage(envelope);
        }

        /// <summary>
        /// Send the server's initial sequence number to a client.
        /// </summary>
        /// <param name="clientAddress">The address of the client.</param>
        /// <param name="initialSequence">The initial sequence number.</param>
        public void SendServerInitialSequence(PeerAddress clientAddress, CyclicalID.UShortID initialSequence)
        {
            Logger.TraceInformation(LogTag.Arbitration, "sending server initial sequence to client {0}, sequence is {1}", clientAddress, initialSequence.Value);
            TransportEnvelope envelope = this.parent.GetMessageFor(clientAddress, QualityOfService.Reliable);
            this.parent.RemoteCall(envelope, this.ArbitrationServerInitialSequence, initialSequence);
            this.parent.SendMessage(envelope);
        }

        /// <summary>
        /// Send a server message to a client.
        /// </summary>
        /// <param name="clientAddress">The address of the client.</param>
        /// <param name="sequence">The sequence number of the message.</param>
        /// <param name="message">The message content.</param>
        public void SendServerEvent(PeerAddress clientAddress, CyclicalID.UShortID sequence, byte[] message)
        {
            Logger.TraceInformation(LogTag.Arbitration, "Sending server event to client {0}, sequence is {1}", clientAddress, sequence.Value);
            TransportEnvelope envelope = this.parent.GetMessageFor(clientAddress, QualityOfService.Reliable);
            this.parent.RemoteCall(envelope, this.ReceiveServerArbitrationEvent, sequence, message);
            this.parent.SendMessage(envelope);
        }

        /// <summary>
        /// Get the ID for the user associated with a given session.
        /// </summary>
        /// <param name="sessionId">The ID of the session.</param>
        /// <returns>The user ID.</returns>
        public long GetUserIdForSession(int sessionId)
        {
            return this.server.GetUserIdForSession(sessionId);
        }

        /// <inheritdoc/>
        public Character GetCharacterForSession(int sessionId)
        {
            return this.server.GetCharacterForSession(sessionId);
        }

        /// <summary>
        /// Gets a <see cref="BadumnaId"/> corresponding to the peer belonging to the session.
        /// </summary>
        /// <param name="sessionId">The session id.</param>
        /// <returns>A <see cref="BadumnaId"/> identifying the peer.</returns>
        public BadumnaId GetBadumnaIdForSession(int sessionId)
        {
            return this.server.GetBadumnaIdForSession(sessionId);
        }

        /// <summary>
        /// Gets the server status.
        /// </summary>
        /// <returns>Arbitration server status.</returns>
        public ArbitrationStatus GetServerStatus()
        {
            return this.arbitrationStatus;
        }

        #region IServiceConsumer Members

        /// <summary>
        /// Gets the service description for the first client found that doesn't have
        /// an arbitration server available, and that matches the given service description.
        /// </summary>
        /// <param name="service">The service description to match.</param>
        /// <returns>The service description of the matching client.</returns>
        public ServiceDescription GetOfflineService(ServiceDescription service)
        {
            foreach (ArbitrationClient client in this.clients)
            {
                if (!client.IsArbitrationServerAvailable())
                {
                    if (client.ServiceDescription.Equals(service))
                    {
                        return client.ServiceDescription;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Tells clients with a given service description to switch to a new server.
        /// </summary>
        /// <param name="description">The service description to switch.</param>
        /// <param name="address">The address of the server to switch to.</param>
        public void SwitchToNewServiceHost(ServiceDescription description, PeerAddress address)
        {
            foreach (ArbitrationClient client in this.clients)
            {
                if (client.ServiceDescription.Equals(description))
                {
                    Logger.TraceInformation(LogTag.Arbitration, "Requesting the client to switch to a new server. address is {0}", address);
                    client.SwitchToNewServer(address);
                }
            }
        }

        /// <summary>
        /// Register arbitration service with the service manager.
        /// </summary>
        /// <param name="manager">the service manager to register with.</param>
        public void RegisterWithServiceManager(ServiceManager manager)
        {
            bool hasInterestedService = false;

            foreach (ArbitrationServerDetails rec in this.arbitrationModule.Servers)
            {
                if (rec.UseDistributedLookup)
                {
                    ServiceDescription service = new ServiceDescription(ServerType.Arbitration, rec.Name);
                    manager.RegisterServiceConsumer(service, this);
                    manager.RegisterInterestedService(service);
                    hasInterestedService = true;
                }
            }

            if (hasInterestedService)
            {
                // register the callback if there is any interested service
                this.OnArbitrationServerBecomeOffline = manager.OnServiceBecomeOffline;
            }
        }

        /// <summary>
        /// Called on connection loss events, to see if the lost connection is a server for
        /// one of the arbitration clients.
        /// </summary>
        /// <param name="address">The address of the lost connection.</param>
        public void ConnectionLostEvent(PeerAddress address)
        {
            foreach (ArbitrationClient client in this.clients)
            {
                if (client.ServerAddress.Equals(address))
                {
                    // Let the client handle its server's connection failure.

                    // change to the server unknown state if using service discovery.
                    client.ChangedToServerUnknownState();

                    // the application will be notified of the connection lost event. 
                    // the callback method specified by the application will be called in the application thread event queue. 
                    client.OnConnectionFailure();

                    // Handle arbitration server failure.
                    if (this.OnArbitrationServerBecomeOffline != null)
                    {
                        // this makes sure that the queued event is only invoked after the application has been notified for the connection lost event.  
                        this.queueApplicationEvent(Apply.Func(this.ScheduleArbitrationServerBecomeOfflineHandler, client));
                    }
                }
            }
        }

        #endregion

        #endregion // Public methods

        #region Private methods

        /// <summary>
        /// Gets the arbitration client for a given server.
        /// </summary>
        /// <param name="address">The address of the server to fetch the client for.</param>
        /// <returns>The arbitration client for the given server.</returns>
        private ArbitrationClient GetClient(PeerAddress address)
        {
            foreach (ArbitrationClient client in this.clients)
            {
                if (client.ServerAddress.Equals(address))
                {
                    return client;
                }
            }

            Logger.TraceWarning(LogTag.Arbitration, "GetClient failed to get ArbitrationClient with the address {0}.", address);
            return null;
        }

        /// <summary>
        /// Gets the arbitration client for a given server with a given service description.
        /// </summary>
        /// <param name="address">The address of the server to fetch the client for.</param>
        /// <param name="serviceDescription">The service description of the server to fetch the client for.</param>
        /// <returns>The arbitration client for the given server.</returns>
        private ArbitrationClient GetClient(PeerAddress address, ServiceDescription serviceDescription)
        {
            foreach (ArbitrationClient client in this.clients)
            {
                if (client.ServerAddress.Equals(address))
                {
                    if (client.ServiceDescription.Equals(serviceDescription))
                    {
                        return client;
                    }
                }
            }

            ArbitrationClient newClient = new ArbitrationClient(this, address, serviceDescription, this.arbitrationModule, this.eventQueue, this.queueApplicationEvent, this.timeKeeper);
            this.clients.Add(newClient);

            return newClient;
        }

        /// <summary>
        /// Make a Quality Of Service object for sending messages.
        /// </summary>
        /// <remarks>Currently unused - could be restored as chosen mechanism for
        /// handling connection atempt failures, or even message failures.</remarks>
        /// <param name="connectionFailure">A callback for connection failure.</param>
        /// <returns>The Quality Of Service object.</returns>
        private QualityOfService MakeQos(GenericCallBack connectionFailure)
        {
            QualityOfService baseQos = QualityOfService.Reliable;

            if (connectionFailure == null)
            {
                return baseQos;
            }

            return new FailNotificationQos(
                baseQos,
                delegate(int nthAttempt) // negative nthAttempt indicates connection failure
                {
                    if (nthAttempt < 0)
                    {
                        connectionFailure();
                    }

                    return true;
                });
        }

        /// <summary>
        /// Schedules the arbitration server become offline event handler to be called on the network event queue.
        /// </summary>
        /// <param name="client">The client.</param>
        private void ScheduleArbitrationServerBecomeOfflineHandler(ArbitrationClient client)
        {
            this.eventQueue.Push(this.OnArbitrationServerBecomeOffline.Invoke, client.ServiceDescription); 
        }

        /// <summary>
        /// Do periodic tidying.
        /// </summary>
        private void CollectGarbage()
        {
            this.server.CollectGarbage();
        }

        /// <summary>
        /// Protocol method for handling initial sequence numbers from clients.
        /// </summary>
        /// <param name="sequence">The initial sequence number.</param>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.ArbitrationClientInitialSequence)]
        private void ArbitrationClientInitialSequence(CyclicalID.UShortID sequence)
        {
            Logger.TraceInformation(LogTag.Arbitration, "Client initial sequence is received by the server, sequence is {0}, client is {1}", sequence.Value, this.parent.CurrentEnvelope.Source);
            this.server.ReceiveClientInitialSequence(
                this.parent.CurrentEnvelope.Source,
                this.parent.CurrentEnvelope.Certificate,
                sequence);
        }

        /// <summary>
        /// Protocol method for handling initial sequence numbers from servers.
        /// </summary>
        /// <param name="sequence">The initial sequence number received.</param>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.ArbitrationServerInitialSequence)]
        private void ArbitrationServerInitialSequence(CyclicalID.UShortID sequence)
        {
            Logger.TraceInformation(LogTag.Arbitration, "Server initial sequence is received by the client, sequence is {0}", sequence.Value);
            ArbitrationClient client = this.GetClient(this.parent.CurrentEnvelope.Source);
            client.ReceiveServerInitialSequence(sequence);
        }
        
        /// <summary>
        /// Protocol method for events sent client -> server, processed on server
        /// </summary>
        /// <param name="sequence">The message sequende number.</param>
        /// <param name="message">The message content.</param>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.ReceiveClientArbitrationEvent)]
        private void ReceiveClientArbitrationEvent(CyclicalID.UShortID sequence, byte[] message)
        {
            Logger.TraceInformation(LogTag.Arbitration, "client arbitration event is received by the server, sequence is {0}", sequence.Value);
            this.server.ReceiveClientArbitrationEvent(
                this.parent.CurrentEnvelope.Source,
                sequence,
                message);
        }

        /// <summary>
        /// Protocol method for events sent server -> client, processed on client.
        /// </summary>
        /// <param name="sequence">The message sequende number.</param>
        /// <param name="message">The message content.</param>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.ReceiveServerArbitrationEvent)]
        private void ReceiveServerArbitrationEvent(CyclicalID.UShortID sequence, byte[] message)
        {
            Logger.TraceInformation(LogTag.Arbitration, "server arbitration event is received by the client, sequence is {0}", sequence.Value);
            ArbitrationClient client = this.GetClient(this.parent.CurrentEnvelope.Source);
            client.ReceiveServerArbitrationEvent(sequence, message);
        }

        /// <summary>
        /// Protocol method for rejections sent server -> client, processed on client.
        /// </summary>
        /// <param name="sequence">The message sequends number of the rejected message.</param>
        /// <param name="message">The message content.</param>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.ReceiveServerArbitrationRejection)]
        private void ReceiveServerArbitrationRejection(CyclicalID.UShortID sequence, byte[] message)
        {
            Logger.TraceInformation(LogTag.Arbitration, "server arbitration rejection is received by the client, sequence is {0}", sequence.Value);
            ArbitrationClient client = this.GetClient(this.parent.CurrentEnvelope.Source);
            client.ReceiveServerArbitrationRejection(sequence, message);
        }

        /// <summary>
        /// Collects the stats from the arbitration server object.
        /// </summary>
        private void CollectStats()
        {
            this.arbitrationStatus = new ArbitrationStatus(this.server.UpdateStats());
        }

        #endregion // Private methods
    }
}
