﻿//-----------------------------------------------------------------------
// <copyright file="ArbitrationEventSet.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Arbitration
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
 
    /// <summary>
    /// Defines a set of arbitration event types and facilitates their serialization and deserialization to byte arrays.
    /// </summary>
    /// <remarks>
    /// <para>To use this class, first define the arbitration event classes, which must all derive from ArbitrationEvent.
    /// Then register all of the arbitration event types with the ArbitrationEventSet.  Each event type will be
    /// assigned an id based on the order that it is registered so it is important that the event types are 
    /// registered in the same order for all instances of an ArbitrationEventSet that will exchange serialized
    /// data.</para>
    /// <para>Once all types are registered, event instances can be serialized and deserialized using the corresponding
    /// methods on this class.  ArbitrationEventSets that have different registered events can be used at the
    /// same time so long as the serialized data is passed to the correct instance.  Passing data serialized
    /// by an event set that contains one set of registered types to an event set that contains different
    /// registered types will result in undefined behaviour.  Typically, one ArbitrationEventSet would be
    /// used for events sent from an arbitrator to a client and a different ArbitrationEventSet for events
    /// sent from the client to the arbitrator.</para>
    /// </remarks>
    public class ArbitrationEventSet
    {
        /// <summary>
        /// The index to use for the next registered type.
        /// </summary>
        private int nextIndex;

        /// <summary>
        /// The number of bytes required to store the registered type index.
        /// </summary>
        private int indexBytes;

        /// <summary>
        /// Map of indices to event type constructors.
        /// </summary>
        private Dictionary<int, ConstructorInfo> constructors = new Dictionary<int, ConstructorInfo>();
        
        /// <summary>
        /// Map of event type to ID.
        /// </summary>
        private Dictionary<Type, int> ids = new Dictionary<Type, int>();

        /// <summary>
        /// Gets a value indicating whether the set has been sealed, and no more types can be registered.
        /// </summary>
        private bool IsSealed
        {
            get { return this.indexBytes != 0; }
        }

        /// <summary>
        /// Register the set of types specified as parameters.  Each Type must derive from ArbirationEvent
        /// and define a contructor that takes a single BinaryReader parameter.
        /// Cannot be called after a call to Serialize or Deserialize has been made.
        /// </summary>
        /// <param name="clientEventTypes">The types to register</param>
        public void Register(params Type[] clientEventTypes)
        {
            this.Register((IEnumerable<Type>)clientEventTypes);
        }

        /// <summary>
        /// Register the set of types specified by the IEnumerable.  Each Type must derive from ArbirationEvent
        /// and define a contructor that takes a single BinaryReader parameter.
        /// Cannot be called after a call to Serialize or Deserialize has been made.
        /// </summary>
        /// <param name="clientEventTypes">The types to register</param>
        public void Register(IEnumerable<Type> clientEventTypes)
        {
            foreach (Type clientEventType in clientEventTypes)
            {
                this.Register(clientEventType);
            }
        }

        /// <summary>
        /// Register the type specified.  The type must derive from ArbirationEvent
        /// and define a contructor that takes a single BinaryReader parameter.
        /// Cannot be called after a call to Serialize or Deserialize has been made.
        /// </summary>
        /// <param name="clientEventType">The type to register</param>
        public void Register(Type clientEventType)
        {
            if (this.IsSealed)
            {
                throw new InvalidOperationException("Can't register more event types after Serialize or Deserialize have been called");
            }

            if (this.nextIndex < 0)
            {
                throw new InvalidOperationException("Too many event types");
            }

            if (!typeof(ArbitrationEvent).IsAssignableFrom(clientEventType))
            {
                throw new ArgumentException("Must derive from ArbitrationEvent", "clientEventType");
            }

            ConstructorInfo cons = clientEventType.GetConstructor(new Type[] { typeof(BinaryReader) });
            if (cons == null)
            {
                throw new ArgumentException("Must implement a constructor taking a BinaryReader", "clientEventType");
            }

            this.constructors[this.nextIndex] = cons;
            this.ids[clientEventType] = this.nextIndex;
            this.nextIndex++;
        }

        /// <summary>
        /// Serialize the clientEvent into a byte stream.  This method will
        /// invoke the SerializedLength property and Serialize method on the
        /// clientEvent.
        /// </summary>
        /// <param name="clientEvent">The event to serialize</param>
        /// <returns>A byte array containing the serialized data</returns>
        /// <exception cref="InvalidOperationException">Thrown when the serialized length of the
        /// arbitration event exceeds that specified in its <see cref="ArbitrationEvent.SerializedLength" /> property.</exception>
        public byte[] Serialize(ArbitrationEvent clientEvent)
        {
            this.Seal();

            int eventId;
            if (!this.ids.TryGetValue(clientEvent.GetType(), out eventId))
            {
                return null;
            }

            bool knowSize = clientEvent.SerializedLength >= 0;

            byte[] data = null;
            if (knowSize)
            {
                data = new byte[clientEvent.SerializedLength + this.indexBytes];
            }

            using (MemoryStream stream = knowSize ? new MemoryStream(data) : new MemoryStream())
            using (BinaryWriter writer = new BinaryWriter(stream))
            {
                for (int i = 0; i < this.indexBytes; i++)
                {
                    byte indexByte = (byte)(eventId & 0xFF);
                    writer.Write(indexByte);
                    eventId >>= 8;
                }

                try
                {
                    clientEvent.Serialize(writer);
                }
                catch (NotSupportedException exception)
                {
                    throw new InvalidOperationException(
                        string.Format(
                        "Size of serialized {0} message must not exceed {0}.SerializedLength. Only override ArbitrationEvent.SerializedLength for messages with known length.",
                        clientEvent.GetType()),
                        exception);
                }

                return knowSize ? data : stream.ToArray();
            }
        }

        /// <summary>
        /// Deserialize an event.  The returned event will be of the same type
        /// as was serialized on the remote end.  It will be constructed by
        /// calling the constructor which takes a single BinaryReader as a parameter.
        /// </summary>
        /// <param name="data">The data to deserialize</param>
        /// <returns>The event that was serialized</returns>
        public ArbitrationEvent Deserialize(byte[] data)
        {
            this.Seal();

            using (MemoryStream stream = new MemoryStream(data))
            using (BinaryReader reader = new BinaryReader(stream))
            {
                int eventId = 0;
                for (int i = 0; i < this.indexBytes; i++)
                {
                    byte indexByte = reader.ReadByte();
                    int indexIntPart = (int)indexByte;
                    indexIntPart <<= 8 * i;
                    eventId |= indexIntPart;
                }

                ConstructorInfo cons;
                if (!this.constructors.TryGetValue(eventId, out cons))
                {
                    return null;
                }

                return (ArbitrationEvent)cons.Invoke(new object[] { reader });
            }
        }

        /// <summary>
        /// Seal the set so it can be used for serializing/deserializing events.
        /// </summary>
        /// <remarks>Sets the number of bytes required to store the index of registered events.</remarks>
        private void Seal()
        {
            if (this.IsSealed)
            {
                return;
            }

            int lastIndex = this.nextIndex - 1;

            if (lastIndex <= 0xFF)
            {
                this.indexBytes = 1;
            }
            else if (lastIndex <= 0xFFFF)
            {
                this.indexBytes = 2;
            }
            else if (lastIndex <= 0xFFFFFF)
            {
                this.indexBytes = 3;
            }
            else
            {
                this.indexBytes = 4;
            }
        }
    }
}
