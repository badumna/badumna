﻿//-----------------------------------------------------------------------
// <copyright file="IArbitrator.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
 
namespace Badumna.Arbitration
{
    using System;
    using Badumna.Utilities;

    /// <summary>
    /// A delegate for handling arbitration connection results.
    /// </summary>
    /// <param name="type">The result type indicating the connection result.</param>
    public delegate void ArbitrationConnectionResultHandler(ServiceConnectionResultType type);

    /// <summary>
    /// Repesents a session with an arbitrator.
    /// </summary>
    public interface IArbitrator
    {
        /// <summary>
        /// Gets a value indicating whether the connection to the arbitrator is established.
        /// </summary>
        bool IsServerConnected { get; }

        /// <summary>
        /// Initiate the connection to the arbitration server.
        /// </summary>
        /// <param name="connectionResultHandler">A callback that will be called to report the result of the connection attempt.</param>
        /// <param name="connectionFailedHandler">A callback that will be called if an established connection fails.</param>
        /// <param name="serverEventHandler">A callback that will be called to handle messages received from the arbitration server.</param>
        /// <remarks>
        /// <paramref name="connectionResultHandler"/> is guaranteed to be called to deliver the result of the connection attempt (success or failure).
        /// <paramref name="connectionFailedHandler"/> will only be called if a connection subsequently fails after <paramref name="connectionResultHandler"/>
        /// has already reported that it was successfully established.
        /// </remarks>
        void Connect(
            ArbitrationConnectionResultHandler connectionResultHandler,
            HandleConnectionFailure connectionFailedHandler,
            HandleServerMessage serverEventHandler);

        /// <summary>
        /// Send an event to the arbitrator.  This will cause the arbitrator's
        /// HandleClientMessage handler to be called with the given message and
        /// a clientId that identifies this client.
        /// </summary>
        /// <param name="message">The byte array to send</param>
        void SendEvent(byte[] message);
    }
}
