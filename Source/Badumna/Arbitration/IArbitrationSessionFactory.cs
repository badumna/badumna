﻿//-----------------------------------------------------------------------
// <copyright file="IArbitrationSessionFactory.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Arbitration
{
    using Badumna.Core;
    using Badumna.Security;
    using Badumna.Utilities;

    /// <summary>
    /// Abstract Factory for creating ArbitrationSessions
    /// </summary>
    internal interface IArbitrationSessionFactory
    {
        /// <summary>
        /// Create an arbitration session.
        /// </summary>
        /// <param name="id">An ID to identify the session.</param>
        /// <param name="peerAddress">The address of the associated peer.</param>
        /// <param name="userCertificate">The user certificate of the associated peer.</param>
        /// <param name="handler">A callback that will handle messages received.</param>
        /// <param name="sender">A callback that will send outgoing messages.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        /// <returns>The arbitration session created.</returns>
        IArbitrationSession Create(
            int id,
            PeerAddress peerAddress,
            ICertificateToken userCertificate,
            GenericCallBack<byte[]> handler,
            GenericCallBack<CyclicalID.UShortID, byte[]> sender,
            ITime timeKeeper);
    }
}
