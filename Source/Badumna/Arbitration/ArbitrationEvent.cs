﻿//-----------------------------------------------------------------------
// <copyright file="ArbitrationEvent.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Arbitration
{
    using System.IO;

    /// <summary>
    /// Defines an arbitration event type.  All arbitration events must derive
    /// from this class.
    /// </summary>
    /// <remarks>
    /// The Serialize method must serialize all the data required to reconstruct
    /// the event upon deserialization.  The derived class must also define
    /// a constructor which accepts a single BinaryReader parameter.  Deserialization 
    /// is performed by invoking this constructor with a BinaryReader that is
    /// at the beginning of the data written by the BinaryWriter in the Serialize
    /// method.
    /// </remarks>
    public abstract class ArbitrationEvent
    {
        /// <summary>
        /// Gets the length of the serialized data in bytes.
        /// </summary>
        /// <remarks>
        /// Only override this property if the size of the serialized
        /// data is known in advance.  This property is used to allocate
        /// an array of the correct size before calling Serialize and
        /// improve performance by avoiding extra copying. If
        /// the size is unknown then do not override this method -- the
        /// stream given to the BinaryWriter will resize dynamically.
        /// </remarks>
        public virtual int SerializedLength 
        {
            get { return -1; }
        }

        /// <summary>
        /// Serializes the content of the arbitration event into the writer.
        /// </summary>
        /// <param name="writer">The BinaryWriter the data should be written to</param>
        public abstract void Serialize(BinaryWriter writer);
    }
}
