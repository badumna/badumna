﻿//-----------------------------------------------------------------------
// <copyright file="Sequencer.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Badumna.Arbitration
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using Badumna.Core;
    using Badumna.Utilities;

    /// <summary>
    /// Delegate for factory method for creating sequencers.
    /// </summary>
    /// <typeparam name="T">The type of event to be sequenced.</typeparam>
    /// <param name="handler">A handler for handling the sequenced events.</param>
    /// <returns>A new sequencer.</returns>
    internal delegate ISequencer<T> SequencerFactory<T>(GenericCallBack<T> handler);

    /// <summary>
    /// Interface for sequencers to sequence received events.
    /// </summary>
    /// <typeparam name="T">The type of the message included in the event.</typeparam>
    internal interface ISequencer<T>
    {
        /// <summary>
        /// Initalize with the sequence number to expect.
        /// </summary>
        /// <param name="initialSequence">The sequence number of the next expected event.</param>
        void Initialize(CyclicalID.UShortID initialSequence);

        /// <summary>
        /// Deal with an incoming event.
        /// </summary>
        /// <param name="sequenceNumber">The sequence number of the event.</param>
        /// <param name="message">The message in the event.</param>
        void ReceiveEvent(CyclicalID.UShortID sequenceNumber, T message);
    }

    /// <summary>
    /// Class to sequence received events and pass them in order to a handler.
    /// </summary>
    /// <remarks>
    /// Events are received with an accompanying sequence number. The sequence keeps track of
    /// the expected number in the sequence, and only forwards events to the handler in strict order.
    /// I.e. events that arrive before earlier ones in the sequence are buffered until the missing
    /// ones are received.
    /// </remarks>
    /// <typeparam name="T">The type of the message included in the event.</typeparam>
    internal class Sequencer<T> : ISequencer<T>, IDisposable
    {
        /// <summary>
        /// Field holding the handler to pass sequenced messages to.
        /// </summary>
        private GenericCallBack<T> handler;

        /// <summary>
        /// Field to track next expected sequencer number.
        /// </summary>
        private CyclicalID.UShortID? expectedSequenceNumber;

        /// <summary>
        /// Field holding sorted list of received events,
        /// so out of order events can be held until missing ones are received.
        /// </summary>
        private SortedList<CyclicalID.UShortID, T> pendingEvents = new SortedList<CyclicalID.UShortID, T>();

        /// <summary>
        /// Initializes a new instance of the Sequencer class.
        /// </summary>
        /// <param name="handler">The handler to pass sequenced messgaes to.</param>
        public Sequencer(GenericCallBack<T> handler)
        {
            this.handler = handler;
        }
        
        /// <summary>
        /// Warns of unhandled pending events when disposed.
        /// </summary>
        public void Dispose()
        {
            if (this.pendingEvents.Count > 0)
            {
                Logger.TraceWarning(LogTag.Arbitration, "Disposing Sequencer with {0} pending events", this.pendingEvents.Count);
            }
        }

        /// <summary>
        /// Initalize the sequence number to expect.
        /// </summary>
        /// <remarks>
        /// Gets rid of any pending events prior to the new expected event.
        /// </remarks>
        /// <param name="initialSequence">The sequence number of the next expected event.</param>
        public void Initialize(CyclicalID.UShortID initialSequence)
        {
            this.expectedSequenceNumber = initialSequence;
            while (this.pendingEvents.Count > 0 && this.pendingEvents.Keys[0] < this.expectedSequenceNumber.Value)
            {
                this.pendingEvents.RemoveAt(0);  // Remove stale events
            }

            this.CheckPending();
        }

        /// <summary>
        /// Deal with an incoming event.
        /// </summary>
        /// <param name="sequenceNumber">The sequence number of the event.</param>
        /// <param name="message">The message in the event.</param>
        public void ReceiveEvent(CyclicalID.UShortID sequenceNumber, T message)
        {
            if (this.expectedSequenceNumber.HasValue &&
                sequenceNumber == this.expectedSequenceNumber.Value)
            {
                this.IncrementReceiveSequenceNumber();
                this.handler(message);
                this.CheckPending();
                return;
            }

            if (this.expectedSequenceNumber.HasValue &&
                sequenceNumber < this.expectedSequenceNumber.Value)
            {
                // Ignore old out of order messages (should be result of connection failure).
                return;
            }

            // We've missed some sequence numbers; queue this event until the missing
            // ones arrive.
            this.pendingEvents[sequenceNumber] = message;
        }

        /// <summary>
        /// Increment the expected sequence number, should be called each time a
        /// sequenced event is handled.
        /// </summary>
        private void IncrementReceiveSequenceNumber()
        {
            CyclicalID.UShortID sequenceNumber = this.expectedSequenceNumber.Value;
            sequenceNumber.Increment();
            this.expectedSequenceNumber = sequenceNumber;
        }

        /// <summary>
        /// Check to see if pending events can now be handled
        /// I.e. preceding events in the dequence have all been handled.
        /// </summary>
        private void CheckPending()
        {
            while (this.pendingEvents.Count > 0 && this.pendingEvents.Keys[0] == this.expectedSequenceNumber.Value)
            {
                this.handler(this.pendingEvents.Values[0]);
                this.IncrementReceiveSequenceNumber();

                this.pendingEvents.RemoveAt(0);
            }
        }
    }
}
