﻿//-----------------------------------------------------------------------
// <copyright file="IArbitrationServer.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Arbitration
{
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Security;
    using Badumna.Utilities;

    /// <summary>
    /// Interface for arbitration servers.
    /// </summary>
    internal interface IArbitrationServer
    {
        /// <summary>
        /// Gets or sets a handler for client events.
        /// </summary>
        HandleClientMessage ClientEvent { get; set; }

        /// <summary>
        /// Gets or sets and handler for client disconnections.
        /// </summary>
        HandleClientDisconnect Disconnect { get; set; }

        /// <summary>
        /// Checks for timed out connections and gets rid of them.
        /// </summary>
        void CollectGarbage();

        /// <summary>
        /// Gets the user ID for a given session.
        /// </summary>
        /// <param name="sessionId">ID of the session.</param>
        /// <returns>The user ID, or -1 if the session was not found.</returns>
        long GetUserIdForSession(int sessionId);

        /// <summary>
        /// Gets a <see cref="BadumnaId"/> corresponding to the peer belonging to the session.
        /// </summary>
        /// <param name="sessionId">The session id.</param>
        /// <returns>A <see cref="BadumnaId"/> identifying the peer, or <see cref="BadumnaId.None"/> if the session was not found.</returns>
        BadumnaId GetBadumnaIdForSession(int sessionId);

        /// <summary>
        /// Get the Character for the user associated with a given session.
        /// </summary>
        /// <param name="sessionId">The ID of the session.</param>
        /// <returns>The user ID.</returns>
        Character GetCharacterForSession(int sessionId);

        /// <summary>
        /// Sends an event to a session.
        /// </summary>
        /// <param name="sessionId">The sesion to send the event to.</param>
        /// <param name="message">The message to send.</param>
        void SendEvent(int sessionId, byte[] message);

        /// <summary>
        /// Handle the initial connection from a client.
        /// </summary>
        /// <param name="clientAddress">The address of the client.</param>
        /// <param name="userCertificate">The user certificate of the client.</param>
        /// <param name="initialSequence">The initial sequence number to expect.</param>
        void ReceiveClientInitialSequence(PeerAddress clientAddress, ICertificateToken userCertificate, CyclicalID.UShortID initialSequence);
        
        /// <summary>
        /// Handle arbitration requests from the client.
        /// </summary>
        /// <param name="source">The address of the client.</param>
        /// <param name="sequence">The sequence number of the message.</param>
        /// <param name="message">The message.</param>
        void ReceiveClientArbitrationEvent(PeerAddress source, CyclicalID.UShortID sequence, byte[] message);

        /// <summary>
        /// Updates the stats.
        /// </summary>
        /// <returns>The latest stats.</returns>
        ArbitrationStatus UpdateStats();
    }
}
