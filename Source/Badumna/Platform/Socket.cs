﻿//------------------------------------------------------------------------------
// <copyright file="Socket.cs" company="National ICT Australia Limited">
//     Copyright (c) 2014 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Badumna.Platform
{
    using System;
    using System.Collections;
    using System.Net;
    using System.Net.Sockets;

    /// <summary>
    /// A network socket.
    /// </summary>
    internal abstract class Socket : IDisposable
    {
        /// <summary>
        /// Gets the local endpoint.
        /// </summary>
        public abstract EndPoint LocalEndPoint { get; }

        /// <summary>
        /// Gets the remote endpoint.
        /// </summary>
        public abstract EndPoint RemoteEndPoint { get; }

        /// <summary>
        /// Gets a value indicating whether a socket is connected to a remote host as of
        /// the last Send or Receive operation.
        /// </summary>
        public abstract bool Connected { get; }

        /// <summary>
        /// Gets or sets a value indicating whether the socket is in blocking mode.
        /// </summary>
        public abstract bool Blocking { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the socket can send or receive broadcast packets.
        /// </summary>
        public abstract bool EnableBroadcast { get; set; }

        /// <summary>
        /// Gets or sets a value that specifies the amount of time after which a synchronous Receive call will time out.
        /// </summary>
        public abstract int ReceiveTimeout { get; set; }

        /// <summary>
        /// Gets or sets a value that specifies the size of the send buffer of the socket.
        /// </summary>
        public abstract int SendBufferSize { get; set; }

        /// <summary>
        /// Gets or sets a value that specifies the size of the receive buffer of the socket.
        /// </summary>
        public abstract int ReceiveBufferSize { get; set; }

        /// <summary>
        /// Creates a socket with the given configuration.
        /// </summary>
        /// <param name="addressFamily">One of the System.Net.Sockets.AddressFamily values.</param>
        /// <param name="socketType">One of the System.Net.Sockets.SocketType values.</param>
        /// <param name="protocolType">One of the System.Net.Sockets.ProtocolType values.</param>
        /// <returns>The new socket</returns>
        public static Socket Create(AddressFamily addressFamily, SocketType socketType, ProtocolType protocolType)
        {
            return new DotNetSocket(addressFamily, socketType, protocolType);
        }

        /// <summary>
        /// Determines the status of one or more sockets.
        /// </summary>
        /// <param name="checkRead">An System.Collections.IList of System.Net.Sockets.Socket instances to check for readability.</param>
        /// <param name="checkWrite">An System.Collections.IList of System.Net.Sockets.Socket instances to check for writability.</param>
        /// <param name="checkError">An System.Collections.IList of System.Net.Sockets.Socket instances to check for errors.</param>
        /// <param name="microSeconds">The time-out value, in microseconds. A -1 value indicates an infinite time-out.</param>
        public static void Select(IList checkRead, IList checkWrite, IList checkError, int microSeconds)
        {
            System.Net.Sockets.Socket.Select(checkRead, checkWrite, checkError, microSeconds);
        }

        /// <summary>
        ///  Sets the specified System.Net.Sockets.Socket option to the specified System.Boolean value.
        /// </summary>
        /// <param name="optionLevel">One of the System.Net.Sockets.SocketOptionLevel values.</param>
        /// <param name="optionName">One of the System.Net.Sockets.SocketOptionName values.</param>
        /// <param name="optionValue">A value of the option.</param>
        public abstract void SetSocketOption(SocketOptionLevel optionLevel, SocketOptionName optionName, int optionValue);

        /// <summary>
        /// Sets low-level operating modes for the socket using numerical control codes.
        /// </summary>
        /// <param name="controlCode">An System.Int32 value that specifies the control code of the operation to perform.</param>
        /// <param name="optionInValue">A System.Byte array that contains the input data required by the operation.</param>
        /// <param name="optionOutValue">A System.Byte array that contains the output data returned by the operation.</param>
        /// <returns>The number of bytes in the optionOutValue parameter.</returns>
        public abstract int IOControl(int controlCode, byte[] optionInValue, byte[] optionOutValue);

        /// <summary>
        /// Associates a System.Net.Sockets.Socket with a local endpoint.
        /// </summary>
        /// <param name="localEP">The local System.Net.EndPoint to associate with the System.Net.Sockets.Socket.</param>
        public abstract void Bind(EndPoint localEP);

        /// <summary>
        /// Determines the status of the socket.
        /// </summary>
        /// <param name="microSeconds">The time to wait for a response, in microseconds.</param>
        /// <param name="mode">One of the System.Net.Sockets.SelectMode values.</param>
        /// <returns>The status of the socket.</returns>
        public abstract bool Poll(int microSeconds, SelectMode mode);

        /// <summary>
        /// Sends data to a connected System.Net.Sockets.Socket.
        /// </summary>
        /// <param name="buffer">An array of type System.Byte that contains the data to be sent.</param>
        /// <returns>The number of bytes sent to the System.Net.Sockets.Socket.</returns>
        public abstract int Send(byte[] buffer);

        /// <summary>
        /// Sends data to the specified endpoint.
        /// </summary>
        /// <param name="buffer">An array of type System.Byte that contains the data to be sent.</param>
        /// <param name="remoteEP">The System.Net.EndPoint that represents the destination for the data.</param>
        /// <returns>The number of bytes sent.</returns>
        public abstract int SendTo(byte[] buffer, EndPoint remoteEP);

        /// <summary>
        /// Receives data from a bound System.Net.Sockets.Socket into a receive buffer.
        /// </summary>
        /// <param name="buffer">An array of type System.Byte that is the storage location for the received data.</param>
        /// <returns>The number of bytes received.</returns>
        public abstract int Receive(byte[] buffer);

        /// <summary>
        /// Receives data from a bound socket into a receive buffer, using the specified System.Net.Sockets.SocketFlags.
        /// </summary>
        /// <param name="buffer">An array of type System.Byte that is the storage location for the received data.</param>
        /// <param name="offset">The position in the buffer parameter to store the received data.</param>
        /// <param name="size">The number of bytes to receive.</param>
        /// <param name="socketFlags">A bitwise combination of the System.Net.Sockets.SocketFlags values.</param>
        /// <param name="errorCode">A System.Net.Sockets.SocketError object that stores the socket error.</param>
        /// <returns>The number of bytes received.</returns>
        public abstract int Receive(byte[] buffer, int offset, int size, SocketFlags socketFlags, out SocketError errorCode);

        /// <summary>
        /// Receives the specified number of bytes of data into the specified location of the data buffer,
        /// using the specified System.Net.Sockets.SocketFlags, and stores the endpoint.
        /// </summary>
        /// <param name="buffer">An array of type System.Byte that is the storage location for received data.</param>
        /// <param name="remoteEP">An System.Net.EndPoint, passed by reference, that represents the remote server.</param>
        /// <returns>The number of bytes received.</returns>
        public abstract int ReceiveFrom(byte[] buffer, ref EndPoint remoteEP);

        /// <summary>
        /// Closes the socket connection and releases all associated resources.
        /// </summary>
        public abstract void Close();

        /// <summary>
        /// Disables sends and receives on a socket.
        /// </summary>
        /// <param name="how">One of the System.Net.Sockets.SocketShutdown values that specifies the
        /// operation that will no longer be allowed.</param>
        public abstract void Shutdown(SocketShutdown how);

        /// <inheritdoc />
        void IDisposable.Dispose()
        {
            this.Dispose();
        }

        /// <summary>
        /// Called on disposal.
        /// </summary>
        protected abstract void Dispose();
    }
}
