﻿//------------------------------------------------------------------------------
// <copyright file="Net.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Badumna.Platform
{
    using System.Collections.Generic;
    using System.Net;
    using System.Net.NetworkInformation;
    using System.Net.Sockets;
    using Badumna.Core;

    /// <summary>
    /// Cross-platform network support functions.
    /// </summary>
    internal static class Net
    {
        /// <summary>
        /// Network interface lock.
        /// </summary>
        private static readonly object NetworkInterfaceLock = new object();

        /// <summary>
        /// Get the list of addresses for this machine.
        /// </summary>
        /// <param name="port">The port being used</param>
        /// <returns>The list of addresses</returns>
        internal static List<PeerAddress> GetLocalAddresses(int port)
        {
            List<PeerAddress> addresses = new List<PeerAddress>();

#if !IOS
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());

            // Add the addresses that are resolved by the DNS query first.
            foreach (IPAddress address in ipHostInfo.AddressList)
            {
                if (address.AddressFamily == AddressFamily.InterNetwork && !IPAddress.IsLoopback(address))
                {
                    PeerAddress localAddress = new PeerAddress(address, port, NatType.Internal);
                    if (localAddress.IsValid)
                    {
                        addresses.Add(localAddress);
                    }
                }
            }
#endif

            // The NetworkInterface.GetAllNetworkInterfaces() seems to be not supported by Mono for Android
#if !ANDROID
            NetworkInterface[] networkInterfaces = null;

            lock (NetworkInterfaceLock)
            {
                networkInterfaces = NetworkInterface.GetAllNetworkInterfaces();
            }

            // Add any additional address that are for other interfaces.
            foreach (NetworkInterface networkInterface in networkInterfaces)
            {
                // on Ubuntu 9.04 + Mono 2.0, network interface's operational status is aways unknown.  
                if (networkInterface.NetworkInterfaceType != NetworkInterfaceType.Loopback &&
                    (networkInterface.OperationalStatus == OperationalStatus.Up ||
                    networkInterface.OperationalStatus == OperationalStatus.Unknown ||
                    networkInterface.OperationalStatus == OperationalStatus.Testing))
                {
                    // Accocding to the .Net API docs, IPInterfaceProperties.UnicastAddresses and
                    // UnicastAddressInformation.Address never throw any exception. However, it appears that they 
                    // throw Null Reference Exception when the machine is heavily loaded and running on Mono. 
                    // These Null Refreence Exceptions will be silently ignored here based on the assumption that the 
                    // address that should be picked as the private address will be added to the list when it is 
                    // returned from the above DNS query.  
                    try
                    {
                        foreach (UnicastIPAddressInformation addressInformation in networkInterface.GetIPProperties().UnicastAddresses)
                        {
                            if (addressInformation.Address.AddressFamily == AddressFamily.InterNetwork)
                            {
                                // only keep the IPv4 addresses
                                PeerAddress address = new PeerAddress(addressInformation.Address, port, NatType.Internal);
                                if (!addresses.Contains(address) && address.IsValid)
                                {
                                    addresses.Add(address);
                                }
                            }
                        }
                    }
                    catch
                    {
                    }
                }
            }
#endif

            return addresses;
        }
    }
}
