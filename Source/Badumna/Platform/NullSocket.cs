﻿//------------------------------------------------------------------------------
// <copyright file="NullSocket.cs" company="National ICT Australia Limited">
//     Copyright (c) 2014 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Badumna.Platform
{
    using System.Net;
    using System.Net.Sockets;

    /// <summary>
    /// A socket which does nothing.
    /// </summary>
    internal class NullSocket : Socket
    {
        /// <summary>
        /// The local endpoint.
        /// </summary>
        private EndPoint localEndPoint;

        /// <inheritdoc />
        public override EndPoint LocalEndPoint
        {
            get { return this.localEndPoint; }
        }

        /// <inheritdoc />
        public override EndPoint RemoteEndPoint
        {
            get { return null; }
        }

        /// <inheritdoc />
        public override bool Connected
        {
            get { return false; }
        }

        /// <inheritdoc />
        public override bool Blocking { get; set; }

        /// <inheritdoc />
        public override bool EnableBroadcast { get; set; }

        /// <inheritdoc />
        public override int ReceiveTimeout { get; set; }

        /// <inheritdoc />
        public override int SendBufferSize { get; set; }

        /// <inheritdoc />
        public override int ReceiveBufferSize { get; set; }

        /// <inheritdoc />
        public override void SetSocketOption(SocketOptionLevel optionLevel, SocketOptionName optionName, int optionValue)
        {
        }

        /// <inheritdoc />
        public override int IOControl(int controlCode, byte[] optionInValue, byte[] optionOutValue)
        {
            return 0;
        }

        /// <inheritdoc />
        public override void Bind(EndPoint localEP)
        {
            this.localEndPoint = localEP;
        }

        /// <inheritdoc />
        public override bool Poll(int microSeconds, SelectMode mode)
        {
            return false;
        }

        /// <inheritdoc />
        public override int Send(byte[] buffer)
        {
            return buffer.Length;
        }

        /// <inheritdoc />
        public override int SendTo(byte[] buffer, EndPoint remoteEP)
        {
            return buffer.Length;
        }

        /// <inheritdoc />
        public override int Receive(byte[] buffer)
        {
            return 0;
        }

        /// <inheritdoc />
        public override int Receive(byte[] buffer, int offset, int size, SocketFlags socketFlags, out SocketError errorCode)
        {
            errorCode = SocketError.Success;
            return 0;
        }

        /// <inheritdoc />
        public override int ReceiveFrom(byte[] buffer, ref EndPoint remoteEP)
        {
            return 0;
        }

        /// <inheritdoc />
        public override void Close()
        {
        }

        /// <inheritdoc />
        public override void Shutdown(SocketShutdown how)
        {
        }

        /// <inheritdoc />
        protected override void Dispose()
        {
        }
    }
}
