﻿//------------------------------------------------------------------------------
// <copyright file="DotNetSocket.cs" company="National ICT Australia Limited">
//     Copyright (c) 2014 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Badumna.Platform
{
    using System;
    using System.Collections;
    using System.Net;
    using System.Net.Sockets;

    /// <summary>
    /// A socket implemented using .NET's System.Net.Sockets.Socket.
    /// </summary>
    internal class DotNetSocket : Socket
    {
        /// <summary>
        /// The underlying socket.
        /// </summary>
        private System.Net.Sockets.Socket socket;

        /// <summary>
        /// Initializes a new instance of the DotNetSocket class using the given underlying socket.
        /// </summary>
        /// <param name="socket">The underlying socket.</param>
        public DotNetSocket(System.Net.Sockets.Socket socket)
        {
            this.socket = socket;
        }

        /// <summary>
        /// Initializes a new instance of the DotNetSocket class using the specified address family, socket type and protocol.
        /// </summary>
        /// <param name="addressFamily">One of the System.Net.Sockets.AddressFamily values.</param>
        /// <param name="socketType">One of the System.Net.Sockets.SocketType values.</param>
        /// <param name="protocolType">One of the System.Net.Sockets.ProtocolType values.</param>
        public DotNetSocket(AddressFamily addressFamily, SocketType socketType, ProtocolType protocolType)
        {
            this.socket = new System.Net.Sockets.Socket(addressFamily, socketType, protocolType);
        }
        
        /// <inheritdoc />
        public override EndPoint LocalEndPoint
        {
            get { return this.socket.LocalEndPoint; }
        }

        /// <inheritdoc />
        public override EndPoint RemoteEndPoint
        {
            get { return this.socket.RemoteEndPoint; }
        }

        /// <inheritdoc />
        public override bool Connected
        {
            get { return this.socket.Connected; }
        }

        /// <inheritdoc />
        public override bool Blocking
        {
            get { return this.socket.Blocking; }
            set { this.socket.Blocking = value; }
        }

        /// <inheritdoc />
        public override bool EnableBroadcast
        {
            get { return this.socket.EnableBroadcast; }
            set { this.socket.EnableBroadcast = value; }
        }

        /// <inheritdoc />
        public override int ReceiveTimeout
        {
            get { return this.socket.ReceiveTimeout; }
            set { this.socket.ReceiveTimeout = value; }
        }

        /// <inheritdoc />
        public override int SendBufferSize
        {
            get { return this.socket.SendBufferSize; }
            set { this.socket.SendBufferSize = value; }
        }

        /// <inheritdoc />
        public override int ReceiveBufferSize
        {
            get { return this.socket.ReceiveBufferSize; }
            set { this.socket.ReceiveBufferSize = value; }
        }

        /// <inheritdoc />
        public override void SetSocketOption(SocketOptionLevel optionLevel, SocketOptionName optionName, int optionValue)
        {
            this.socket.SetSocketOption(optionLevel, optionName, optionValue);
        }

        /// <inheritdoc />
        public override int IOControl(int controlCode, byte[] optionInValue, byte[] optionOutValue)
        {
            return this.socket.IOControl(controlCode, optionInValue, optionOutValue);
        }

        /// <inheritdoc />
        public override void Bind(EndPoint localEP)
        {
            this.socket.Bind(localEP);
        }

        /// <inheritdoc />
        public override bool Poll(int microSeconds, SelectMode mode)
        {
            return this.socket.Poll(microSeconds, mode);
        }

        /// <inheritdoc />
        public override int Send(byte[] buffer)
        {
            return this.socket.Send(buffer);
        }

        /// <inheritdoc />
        public override int SendTo(byte[] buffer, EndPoint remoteEP)
        {
            return this.socket.SendTo(buffer, remoteEP);
        }

        /// <inheritdoc />
        public override int Receive(byte[] buffer)
        {
            return this.socket.Receive(buffer);
        }

        /// <inheritdoc />
        public override int Receive(byte[] buffer, int offset, int size, SocketFlags socketFlags, out SocketError errorCode)
        {
            return this.socket.Receive(buffer, offset, size, socketFlags, out errorCode);
        }

        /// <inheritdoc />
        public override int ReceiveFrom(byte[] buffer, ref EndPoint remoteEP)
        {
            return this.socket.ReceiveFrom(buffer, ref remoteEP);
        }

        /// <inheritdoc />
        public override void Close()
        {
            this.socket.Close();
        }

        /// <inheritdoc />
        public override void Shutdown(SocketShutdown how)
        {
            this.socket.Shutdown(how);
        }

        /// <inheritdoc />
        protected override void Dispose()
        {
            ((IDisposable)this.socket).Dispose();
        }
    }
}
