//------------------------------------------------------------------------------
// <copyright file="ExecutionPlatform.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Reflection;

namespace Badumna.Platform
{
    /// <summary>
    /// Enumerates the different supported platforms
    /// </summary>
    internal enum PlatformFamily
    {
        /// <summary>
        /// Indicates the Mono.NET platform
        /// </summary>
        Mono,

        /// <summary>
        /// Indicates the Microsoft.NET platform
        /// </summary>
        Microsoft,
    }

    /// <summary>
    /// Provides helper methods for determining the execution environment.
    /// </summary>
    internal static class ExecutionPlatform
    {
        /// <summary>
        /// Initializes static members of the ExecutionPlatform class.
        /// </summary>
        static ExecutionPlatform()
        {
            if (Type.GetType("Mono.Runtime") != null || IsIOSDevice())
            {
                ExecutionPlatform.Current = PlatformFamily.Mono;
            }
            else
            {
                ExecutionPlatform.Current = PlatformFamily.Microsoft;
            }
        }

        /// <summary>
        /// Gets a value indicating the current .NET platform.
        /// </summary>
        public static PlatformFamily Current { get; private set; }

        /// <summary>
        /// Gets a value indicating whether we're running on Windows.
        /// </summary>
        public static bool IsPlatformWindows
        {
            get
            {
                return System.Environment.OSVersion.Platform == PlatformID.Win32NT || System.Environment.OSVersion.Platform == PlatformID.Win32S
                    || System.Environment.OSVersion.Platform == PlatformID.Win32Windows || System.Environment.OSVersion.Platform == PlatformID.WinCE;
            }
        }

        /// <summary>
        /// Returns a value indicating whether we're running on Mono 1.2.x on Mac.
        /// </summary>
        /// <returns>True if running on Mono 1.2.x on Mac, false otherwise</returns>
        public static bool IsRunningOnOldMacMono()
        {
            try
            {
                if (Environment.OSVersion.Platform != PlatformID.Unix)
                {
                    return false;
                }

                Type monoRuntimeType = typeof(object).Assembly.GetType("Mono.Runtime");
                if (monoRuntimeType == null)
                {
                    return false;
                }

                MethodInfo getDisplayNameMethod = monoRuntimeType.GetMethod("GetDisplayName", BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.DeclaredOnly | BindingFlags.ExactBinding, null, Type.EmptyTypes, null);
                if (getDisplayNameMethod == null)
                {
                    return false;
                }

                string displayName = (string)getDisplayNameMethod.Invoke(null, null);
                if (displayName.Contains("1.2."))
                {
                    return true;
                }
            }
            catch (Exception)
            {
            }

            return false;
        }
        
        /// <summary>
        /// Check whether Badumna is running on iOS device.
        /// </summary>
        /// <returns>Retuns true when running in iOS device.</returns>
        private static bool IsIOSDevice()
        {
#if MONOTOUCH
            return MonoTouch.ObjCRuntime.Runtime.Arch == MonoTouch.ObjCRuntime.Arch.DEVICE;
#else
            return false;
#endif
        }
    }
}