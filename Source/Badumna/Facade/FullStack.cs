using System;
using System.Diagnostics;
using System.Net.NetworkInformation;

using Badumna.Arbitration;
using Badumna.Chat;
using Badumna.Controllers;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.DistributedHashTable;
using Badumna.InterestManagement;
using Badumna.Matchmaking;
using Badumna.Multicast;
using Badumna.Overload;
using Badumna.Platform;
using Badumna.Replication;
using Badumna.Security;
using Badumna.ServiceDiscovery;
using Badumna.SpatialEntities;
using Badumna.Streaming;
using Badumna.Transport;
using Badumna.Utilities;
using Badumna.Utilities.UserTracker;
using Microsoft.Win32;

using SpatialAutoreplicationManager = Badumna.Autoreplication.SpatialAutoreplicationManager;
using MatchAutoreplicationManager = Badumna.Autoreplication.MatchAutoreplicationManager;

namespace Badumna
{
    using Badumna.Autoreplication;
    using Badumna.Autoreplication.Serialization;

    using Manager = Badumna.Autoreplication.Serialization.Manager;
    using System.Collections.Generic;

    class FullStack : ProtocolStack
    {
        private readonly INetworkFacade networkFacade;
        private GenericCallBack<FullStack> mCreateOverride;
        private TrustedExtensionLoader mTrustedExtensionLoader = new TrustedExtensionLoader();

        private UdpTransport UDPTransportLayer { get { return (UdpTransport)this.TransportLayer; } }

        public FullStack(
            INetworkFacade networkFacade,
            Options options,
            UdpTransport udpTransportLayer,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            INetworkConnectivityReporter connectivityReporter,
            IRandomNumberGenerator randomNumberGenerator)
            : this(networkFacade, options, udpTransportLayer, null, eventQueue, timeKeeper, connectivityReporter, randomNumberGenerator)
        {
            this.mTrustedExtensionLoader.Initialize();
        }

        /// <summary>
        /// Initializes a new instance of the FullStack class.
        /// </summary>
        /// <param name="networkFacade">The parent network facade.</param>
        /// <param name="options">The configuration options.</param>
        /// <param name="transportLayer">Transport to use.  Must be a UdpTransport and must be initialized.
        /// already (although initialization doesn't have to have completed).</param>
        /// <param name="createOverride">Extra code to execute when creating the stack.</param>
        /// <param name="eventQueue">The event queue to schedule events on.</param>
        /// <param name="timeKeeper">The time source.</param>
        /// <param name="connectivityReporter">The network connectivity reporter.</param>
        /// <param name="randomNumberGenerator">A random number generator.</param>
        public FullStack(
            INetworkFacade networkFacade,
            Options options,
            ITransport transportLayer,
            GenericCallBack<FullStack> createOverride,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            INetworkConnectivityReporter connectivityReporter,
            IRandomNumberGenerator randomNumberGenerator)
            : base(options, transportLayer, eventQueue, timeKeeper, connectivityReporter, randomNumberGenerator)
        {
            if (networkFacade == null)
            {
                throw new ArgumentNullException("networkFacade");
            }

            this.networkFacade = networkFacade;

            this.mCreateOverride = createOverride;

            // initialize the workaround tasks when running on Mono, or hookup with the event handlers directly when 
            // running on .Net. 
            this.InitializeCheckTask();

            this.mTrustedExtensionLoader.Initialize();
        }

        /// <inheritdoc/>
        protected override MatchmakingRequest CreateMatchmakingRequest(
            HandleMatchmakingResult onCompletion,
            HandleMatchmakingProgress onProgress,
            MatchmakingOptions options)
        {
            return new MatchmakingRequest(this.GetMatchmakingArbitrator(),
                this.options, onCompletion, onProgress, options);
        }

        protected override void CreateOverride()
        {
            base.CreateOverride();

            if (this.mCreateOverride != null)
            {
                this.mCreateOverride(this);
                this.mCreateOverride = null;
            }
        }

        protected override ArbitrationManager CreateArbitrationManager()
        {
            ArbitrationManager manager = new ArbitrationManager(this.ProtocolRoot, this.options.Arbitration, this.eventQueue, this.ApplicationEventQueue.Add, this.timeKeeper, this.ConnectivityReporter, this.ConnectionTable);
            if (((IArbitrationModule)this.options.Arbitration).RequiresDistributedLookup)
            {
                Debug.Assert(null != this.mServiceManager, "Service Discovery is enabled, but the service manager is null.");
                manager.RegisterWithServiceManager(this.mServiceManager);
            }

            return manager;
        }

        protected override Badumna.Validation.IFacade CreateValidationFacade(ISpatialEntityManager spatialEntityManager)
        {
            Validation.IFacade facade = new Validation.Facade(
                this.options.Validation,
                this.ProtocolRoot,
                this.DhtFacades[Parameters.ServiceDiscoveryTier],
                this.eventQueue,
                this.ConnectionTable,
                this.TransportLayer,
                spatialEntityManager,
                this.networkFacade,
                this.timeKeeper,
                this.ApplicationEventQueue.Add,
                this.badumnaIdAllocator.GetNextUniqueId);
            return facade;
        }

        protected override Match.Facade CreateMatchFacade()
        {
            return new Match.Facade(
                this.ConnectivityReporter,
                this.ConnectionTable,
                this.ProtocolRoot,
                this.TransportLayer,
                this.ConnectivityReporter,
				this.ApplicationEventQueue.Add,
                this.eventQueue,
                this.options,
                this.timeKeeper,
                this.MatchEntityManager,
                this.MatchAutoreplicationManager);
        }

        protected override IComplaintForwarder CreateComplaintForwarder()
        {
            return this.mTrustedExtensionLoader.CreateComplaintForwarder(this.mProtocolRoot, this.TransportLayer, this.TokenCache, this.ConnectivityReporter);
        }

        protected override RelayManager CreateRelayManager()
        {
            return new Badumna.Transport.RelayManager(this.mDhtFacades[Parameters.RendezvousTier], this.mConnectionTable, this.eventQueue, this.timeKeeper, this.TransportLayer, this.ConnectivityReporter, this.ConnectionTable);
        }

        protected override DhtTransport CreateDhtTransport()
        {
            return new Badumna.Transport.DhtTransport(
                this.mConnectionTable,
                this.mDhtFacades[Parameters.ControllerTier],
                this.eventQueue,
                this.timeKeeper);
        }

        protected override DhtFacade CreateDht(NamedTier tier)
        {
            DhtFacade facade = null;

            if (tier == NamedTier.Trusted)
            {
                facade = this.mTrustedExtensionLoader.CreateTrustedDhtFacade(this.mRouterMultiplexer, this.mConnectionTable);
            }
            else
            {
                facade = this.mRouterMultiplexer.CreateDhtFacade(tier, this.mConnectionTable);
            }

            return facade;
        }

        protected override IInterestManagementService CreateIms()
        {
            return new GossipWrapperService(
                this.mDhtFacades[Parameters.ImsTier], 
                this.mDhtFacades[Parameters.ControllerTier], 
                this.mProtocolRoot,
                this.eventQueue,
                this.timeKeeper,
                this.ConnectivityReporter);
        }

        protected override EntityManager CreateEntityManager()
        {
            EntityManager entityManager = new EntityManager(
                this.mProtocolRoot,
                this.mConnectionTable,
                this.mForwardingManager,
                this.options.Overload,
                this.eventQueue,
                this.ApplicationEventQueue.Add,
                this.timeKeeper,
                this.TransportLayer,
                this.ConnectivityReporter,
                this.nest.Construct);
            if (this.options.Overload.IsDistributedLookupUsed)
            {
                Debug.Assert(null != this.mServiceManager, "Service Discovery is enabled, but the service manager is null.");

                entityManager.RegisterWithServiceManager(this.mServiceManager);
            }

            return entityManager;
        }

        protected override Autoreplication.Serialization.Manager CreateSerializationManager()
        {
            return new Autoreplication.Serialization.Manager(this.timeKeeper);
        }

        protected override TypeRegistry CreateTypeRegistry()
        {
            return new TypeRegistry(this.serialization);
        }

        protected override SpatialAutoreplicationManager CreateAutoreplicationManager()
        {
            return new SpatialAutoreplicationManager(
                this.serialization,
                this.timeKeeper,
                (o, d) => new PositionalOriginalWrapper(o, d.Key, d.Value, this.SpatialEntityManager, this.serialization),
                r => new PositionalReplicaWrapper(r, this.serialization, this.timeKeeper));
        }
        
        protected override MatchAutoreplicationManager CreateMatchAutoreplicationManager()
        {
            return new MatchAutoreplicationManager(
                this.serialization,
                this.timeKeeper,
                (o, d) => new OriginalEntityWrapper(o, this.matchEntityManager, this.serialization),
                r => new ReplicaEntityWrapper(r, this.serialization, this.timeKeeper));
        }

        protected override RPCManager CreateRPCManager()
        {
            return new RPCManager(this.serialization);
        }
        
        protected override Match.EntityManager CreateMatchEntityManager()
        {
            return new Match.EntityManager(
                this.EntityManager,
                this.matchAutoreplicationManager,
                this.ConnectivityReporter,
                this.eventQueue,
                this.ApplicationEventQueue.Add,
                new BadumnaIdAllocator(this.TransportLayer, this.ConnectivityReporter, this.RandomNumberGenerator));
        }

        protected override SpatialEntityManager CreateSpatialEntityManager()
        {
            return new SpatialEntityManager(
                this.EntityManager,
                this.InterestManagementService,
                this.autoreplicationManager,
                this.eventQueue,
                this.ApplicationEventQueue.Add,
                this.timeKeeper,
                this.ConnectivityReporter,
                this.ConnectionTable,
                this.TransportLayer,
                this.badumnaIdAllocator.GetNextUniqueId,
                this.badumnaIdAllocator.GetNextUniqueId);
        }

        protected override MulticastFacade CreateMulticastFacade()
        {
            MembershipService membershipService = new MembershipService(this.mDhtFacades[Parameters.MembershipTier]);
            return new MulticastFacade(this.mProtocolRoot, membershipService, this.eventQueue, this.TransportLayer, this.ConnectivityReporter, this.ConnectionTable, this.badumnaIdAllocator.GetNextUniqueId, this.nest.Construct);
        }

        protected override PresenceService CreatePresenceService()
        {
            return new PresenceService(
                this.mDhtFacades[Parameters.PresenceTier],
                this.eventQueue,
                this.timeKeeper,
                this.ConnectivityReporter,
                this.TokenCache,
                this.badumnaIdAllocator.GetNextUniqueId);
        }

        protected override ChatService CreateChatService(PresenceService presenceService)
        {
            ChatProtocolFacadeFactory chatProtocolFactory = ChatProtocolFacade.Factory(
                presenceService,
                this.mEntityManager,
                this.mMulticastFacade,
                this.mProtocolRoot,
                this.TransportLayer);

            ChatSessionFactory chatSessionFactory = ChatSession.Factory(
                this.eventQueue,
                this.featurePolicy,
                this.ApplicationEventQueue.Add,
                this.badumnaIdAllocator.GetNextUniqueId,
                this.autoreplicationManager.GetEntityID);

            return new ChatService(
                chatProtocolFactory,
                chatSessionFactory,
                this.eventQueue,
                this.ConnectivityReporter,
                this.ConnectionTable);
        }

        protected override StreamingManager CreateStreamingManager()
        {
            return new LocalStreamingManager(
                this.mProtocolRoot,
                this.mConnectionTable,
                this.eventQueue,
                this.ApplicationEventQueue.Add,
                this.timeKeeper,
                this.ConnectivityReporter,
                this.ConnectionTable,
                this.autoreplicationManager.GetEntityID);
        }

        protected override ForwardingManager CreateForwardingManager()
        {
            return new ForwardingManager(
                this.options.Overload,
                this.ProtocolRoot, 
                this.ConnectionTable, 
                new EnvelopeForwarderFactory(), 
                new ForwardingGroupFactory(),
                this.eventQueue,
                this.timeKeeper,
                this.TransportLayer,
                this.ConnectivityReporter,
                this.ConnectionTable);
        }

        protected override ControllerInitializer CreateControllerInitializer()
        {
            return new ControllerInitializer(
                this.networkFacade,
                this.DhtFacades[Parameters.ControllerTier],
                this.eventQueue,
                this.ApplicationEventQueue.Add,
                this.ConnectivityReporter,
                this.badumnaIdAllocator.SetNewIdsAddress,
                this.options.IsInCppMode);
        }

        protected override BroadcastLayer CreateBroadcastLayer()
        {
            return new BroadcastLayer(this.options.Connectivity, this.eventQueue, this.timeKeeper, this.TokenCache.GetNetworkParticipationToken, this.ConnectivityReporter);
        }

        protected override DiscoveryLayer CreateDiscoveryService(IMessageProducer<TransportEnvelope> messageProducer)
        {
            return new DiscoveryLayer(this.mBroadcastLayer, messageProducer, this.mConnectionTable, this.options.Connectivity.BroadcastPort, this.eventQueue, this.ConnectivityReporter, this.TransportLayer, this.nest.Construct);
        }

        protected override SystemControlService CreateControlService()
        {
            return this.mTrustedExtensionLoader.CreateSystemControlService(
                this.mDhtFacades[NamedTier.AllInclusive], 
                this.mConnectionTable,
                this.TokenCache);
        }

        protected override ServiceDiscoveryService CreateServiceDiscoveryService()
        {
            // connects the service discovery service and the service manager together.
            Debug.Assert(null != this.mServiceManager, "Service Discovery is enabled, but the service manager is null.");

            ServiceDiscoveryService service = new ServiceDiscoveryService(
                this.DhtFacades[Parameters.ServiceDiscoveryTier],
                this.options.IsServiceDiscoveryEnabled,
                this.eventQueue,
                this.timeKeeper,
                this.TransportLayer,
                this.ConnectivityReporter,
                this.TokenCache);
            Logger.TraceInformation(LogTag.Facade, "Starting the Service Discovery Service.");
            service.Start();
            service.SetOnServiceBecomeAvailableCallback(this.mServiceManager.OnServiceBecomeAvailable);
            this.mServiceManager.Start(service);

            return service;
        }

        protected override UserTracker CreateUserTracker()
        {
            UserTracker tracker = new UserTracker(this.networkFacade, this.eventQueue, this.ConnectivityReporter, this.options.Connectivity.ApplicationName);
            tracker.Start();

            return tracker;
        }

        /// <summary>
        /// Networks availability changed event handler.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Net.NetworkInformation.NetworkAvailabilityEventArgs"/> instance 
        /// containing the event data.</param>
        private void NetworkAvailabilityChanged(object sender, NetworkAvailabilityEventArgs e)
        {
            if (e.IsAvailable)
            {
                // Delay for a bit because it seems that the connection does not come up right away
                this.eventQueue.Schedule(1000, this.UDPTransportLayer.CheckConnectivityStatus);
            }
            else
            {
                this.ConnectivityReporter.SetStatus(ConnectivityStatus.Offline);
            }
        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        private void InitializeCheckTask()
        {
            if (ExecutionPlatform.Current == PlatformFamily.Mono)
            {
                // if the clock change significantly on a hosted service, then just ignore it. assuming it is due to NTP
                if (!this.options.Connectivity.IsHostedService)
                {
                    // start the workaround task used to detect address change event.
                    this.addressChangedDetector = new MonoAddressChangedDetector(this.HandleAddressChanged, this.eventQueue);
                    this.addressChangedDetector.Start();

                    // start the workaround task used to detect sleep event. 
                    this.sleepDetector = new MonoSleepDetector(this.HandleSystemResume, this.eventQueue);
                    this.sleepDetector.Start();
                }
            }
            else
            {
                // Should have an option to disable? 
                NetworkChange.NetworkAddressChanged += this.NetworkAddressChanged;
                NetworkChange.NetworkAvailabilityChanged += this.NetworkAvailabilityChanged;
#if !ANDROID && !IOS
                SystemEvents.PowerModeChanged += this.SleepNotification;
#endif
            }
        }

        protected override void ShutdownOverride()
        {
            if (ExecutionPlatform.Current != PlatformFamily.Mono)
            {
                NetworkChange.NetworkAddressChanged -= this.NetworkAddressChanged;
                NetworkChange.NetworkAvailabilityChanged -= this.NetworkAvailabilityChanged;
#if !ANDROID && !IOS
                SystemEvents.PowerModeChanged -= this.SleepNotification;
#endif
            }
        }

        private void NetworkAddressChanged(object sender, EventArgs e)
        {
            // Delay for a bit because it seems that the connection does not come up right away
            this.HandleAddressChanged();
        }

        private void HandleAddressChanged()
        {
            this.eventQueue.Schedule(1000, this.UDPTransportLayer.CheckConnectivityStatus);
        }


#if !ANDROID && !IOS
        /// <summary>
        /// The .net power mode changed handler.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="Microsoft.Win32.PowerModeChangedEventArgs"/> instance containing the 
        /// event data.</param>
        private void SleepNotification(object sender, PowerModeChangedEventArgs e)
        {
            // on resume, check the connectivity status. 
            if (e.Mode == PowerModes.Resume)
            {
                this.HandleSystemResume();
            }
        }
#endif
        /// <summary>
        /// Handles the system resume (resume from sleep, stand by or hibernate) event.
        /// </summary>
        private void HandleSystemResume()
        {            
            // It should not set the network to offline in LAN test mode.
            if (this.options.Connectivity.PerformStun)
            {
                this.ConnectivityReporter.SetStatus(ConnectivityStatus.Offline);
                this.eventQueue.Schedule(3000, this.UDPTransportLayer.CheckConnectivityStatus);
            }
        }
    }
}
