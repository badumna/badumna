﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Net;

using Badumna.Core;
using Badumna.Transport;
using Badumna.Utilities;


namespace Badumna
{
    class Diagnostics : IDiagnosticsExtension
    {
        private readonly INetworkFacadeInternal networkFacadeInternal;

        private IDiagnosticsExtension mDiagnosticsExtension;

        public Diagnostics(INetworkFacadeInternal networkFacadeInternal)
        {
            if (networkFacadeInternal == null)
            {
                throw new ArgumentNullException("networkFacadeInternal");
            }

            this.networkFacadeInternal = networkFacadeInternal;
        }

        public bool IsLoaded { get { return this.mDiagnosticsExtension != null; } }

        public void Initialize(INetworkFacadeInternal facadeInternal)
        {
            if (this.mDiagnosticsExtension != null)
            {
                return;
            }

            IDiagnosticsExtension diagnosticsExtension = null;

            try
            {
                Assembly diagnostics = Assembly.Load("Diagnostics");
                foreach (Type type in diagnostics.GetTypes())
                {
                    if (typeof(IDiagnosticsExtension).IsAssignableFrom(type))
                    {
                        diagnosticsExtension = (IDiagnosticsExtension)Activator.CreateInstance(type);
                        break;
                    }
                }
            }
            catch (Exception)
            {
            }

            if (diagnosticsExtension != null)
            {
                diagnosticsExtension.Initialize(facadeInternal);
                this.mDiagnosticsExtension = diagnosticsExtension;
                Logger.TraceInformation(LogTag.Facade, "Diagnostics extension loaded");
            }
        }

        public void Shutdown()
        {
            IDiagnosticsExtension diagnosticsExtension = this.mDiagnosticsExtension;
            this.mDiagnosticsExtension = null;
            if (diagnosticsExtension != null)
            {
                diagnosticsExtension.Shutdown();
            }
        }

        private void AssertLoaded()
        {
            if (this.mDiagnosticsExtension == null)
            {
                throw new InvalidOperationException("Diagnostics must be loaded first");
            }
        }

        public void Connect(IPEndPoint serverEndPoint)
        {
            this.AssertLoaded();
            this.mDiagnosticsExtension.Connect(serverEndPoint);
        }

        [ConnectionlessProtocolMethod(ConnectionlessMethod.DiagnosticsInitializeAndConnect)]
        public void InitializeAndConnect(IPEndPoint serverEndPoint)  // TODO:  Challenge/response!
        {
            this.Initialize(this.networkFacadeInternal);
            this.Connect(serverEndPoint);
        }

        [ConnectionlessProtocolMethod(ConnectionlessMethod.DiagnosticsDisconnect)]  // TODO:  Challenge/response!
        public void Disconnect()
        {
            this.AssertLoaded();
            this.mDiagnosticsExtension.Disconnect();
        }
    }
}
