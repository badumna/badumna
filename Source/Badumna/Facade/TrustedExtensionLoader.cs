﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;
using Badumna.DistributedHashTable;
using Badumna.Security;

namespace Badumna
{
    class TrustedExtensionLoader : ITrustedExtension
    {
        private ITrustedExtension mTrustedExtension;

        #region ITrustedExtension Members

        public void Initialize()
        {
            if (this.mTrustedExtension != null)
            {
                return;
            }

            ITrustedExtension trustedExtension = null;

            try
            {
                Assembly diagnostics = Assembly.Load("TrustedComponent");
                foreach (Type type in diagnostics.GetTypes())
                {
                    if (typeof(ITrustedExtension).IsAssignableFrom(type))
                    {
                        trustedExtension = (ITrustedExtension)Activator.CreateInstance(type);
                        break;
                    }
                }
            }
            catch (Exception)
            {
                Logger.TraceInformation(LogTag.Facade, "Trusted compnent is not loaded.");
            }

            if (trustedExtension != null)
            {
                trustedExtension.Initialize();
                this.mTrustedExtension = trustedExtension;
                Logger.TraceInformation(LogTag.Facade, "Trusted component extension loaded");
            }
        }

        public void Shutdown()
        {
            ITrustedExtension trustedExtension = this.mTrustedExtension;
            this.mTrustedExtension = null;
            if (trustedExtension != null)
            {
                trustedExtension.Shutdown();
            }
        }

        public IComplaintForwarder CreateComplaintForwarder(TransportProtocol parent, INetworkAddressProvider addressProvider, TokenCache tokens, INetworkConnectivityReporter connectivityReporter)
        {
            if (this.mTrustedExtension != null)
            {
                return this.mTrustedExtension.CreateComplaintForwarder(parent, addressProvider, tokens, connectivityReporter);
            }

            return new ComplaintForwarder(parent, addressProvider, tokens);
        }

        public DhtFacade CreateTrustedDhtFacade(RouterMultiplexer multiplexer, IConnectionTable connectionTable)
        {
            if (this.mTrustedExtension != null)
            {
                this.mTrustedExtension.CreateTrustedDhtFacade(multiplexer, connectionTable);
            }

            return null;
        }

        public SystemControlService CreateSystemControlService(DhtProtocol parent, ConnectionTable connectionTable, TokenCache tokens)
        {
            return new SystemControlService(parent, connectionTable, tokens);
        }

        #endregion
    }
}
