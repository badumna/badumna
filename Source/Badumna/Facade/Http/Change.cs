﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Globalization;
using System.Threading;

using Badumna.Chat;
using Badumna.Utilities;
using Badumna.Streaming;
using Badumna.Core;
using Badumna.Replication;
using Badumna.DataTypes;
using Badumna.SpatialEntities;
using Badumna.Facade.Http.Streaming;

namespace Badumna.Facade.Http
{
    internal sealed class ChangeOld
    {


        public static Vector3? TryParseVector3(TextReader reader)
        {
            string vectorLine = reader.ReadLine() ?? "";
            string[] vectorParts = vectorLine.Split(',');
            float x, y, z;
            if (vectorParts.Length != 3 ||
                !float.TryParse(vectorParts[0], NumberStyles.Float, CultureInfo.InvariantCulture, out x) ||
                !float.TryParse(vectorParts[1], NumberStyles.Float, CultureInfo.InvariantCulture, out y) ||
                !float.TryParse(vectorParts[2], NumberStyles.Float, CultureInfo.InvariantCulture, out z))
            {
                return null;
            }

            return new Vector3(x, y, z);
        }

        public static float? TryParseFloat(TextReader reader)
        {
            float result;
            if (!float.TryParse(reader.ReadLine(), NumberStyles.Float, CultureInfo.InvariantCulture, out result))
            {
                return null;
            }
            return result;
        }

        public static int? TryParseInt(TextReader reader)
        {
            int result;
            if (!int.TryParse(reader.ReadLine(), NumberStyles.Integer, CultureInfo.InvariantCulture, out result))
            {
                return null;
            }
            return result;
        }

        public static uint? TryParseUInt(TextReader reader)
        {
            uint result;
            if (!uint.TryParse(reader.ReadLine(), NumberStyles.Integer, CultureInfo.InvariantCulture, out result))
            {
                return null;
            }
            return result;
        }

        public static byte[] TryParseBytes(TextReader reader)
        {
            try
            {
                return Convert.FromBase64String(reader.ReadLine());
            }
            catch (Exception)
            {
            }

            return null;
        }
    }
}
