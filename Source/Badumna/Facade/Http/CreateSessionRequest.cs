﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Badumna.Security;
using Badumna.Core;

namespace Badumna.Facade.Http
{
    internal class CreateSessionRequest : Request
    {
        private class CachingIdentityProvider : IIdentityProvider
        {
            private SymmetricKeyToken mNetworkParticipationToken;
            private PermissionListToken mPermissionListToken;
            private TrustedAuthorityToken mTrustedAuthorityToken;
            private CertificateToken mCertificateToken;
            private TimeToken mTimeToken;

            public CachingIdentityProvider(TrustedAuthorityToken trustedAuthority, PermissionListToken permissionList,
                SymmetricKeyToken networkParticipation, CertificateToken certificateToken, TimeToken timeToken)
            {
                this.mTrustedAuthorityToken = trustedAuthority;
                this.mPermissionListToken = permissionList;
                this.mNetworkParticipationToken = networkParticipation;
                this.mCertificateToken = certificateToken;
                this.mTimeToken = timeToken;
            }

            public SymmetricKeyToken GetNetworkParticipationToken()
            {
                return this.mNetworkParticipationToken;
            }

            public PermissionListToken GetPermissionListToken()
            {
                return this.mPermissionListToken;
            }

            public TrustedAuthorityToken GetTrustedAuthorityToken()
            {
                return this.mTrustedAuthorityToken;
            }

            public CertificateToken GetUserCertificateToken()
            {
                return this.mCertificateToken;
            }

            public TimeToken GetTimeToken()
            {
                return this.mTimeToken;
            }

            public void Activate()
            {
            }

            public void Dispose()
            {
            }
        }


        private string mSessionId;
        public string SessionId { get { return this.mSessionId; } }

        private IIdentityProvider mIdentityProvider;
        public IIdentityProvider IdentityProvider { get { return this.mIdentityProvider; } }

        public CreateSessionRequest(SessionManager sessionManager, IIdentityProvider identityProvider)
            : base(sessionManager)
        {
            this.mIdentityProvider = identityProvider;
        }

        public CreateSessionRequest(TextReader reader)
            : base(null)
        {
            string initialTokensString = reader.ReadLine() ?? "";

            byte[] initialTokens = Convert.FromBase64String(initialTokensString);

            using (MessageBuffer message = new MessageBuffer(initialTokens))
            {
                TrustedAuthorityToken trustedAuthority = message.Read<TrustedAuthorityToken>();
                PermissionListToken permissionList = message.Read <PermissionListToken>();
                SymmetricKeyToken networkParticipation = message.Read <SymmetricKeyToken>();
                
                // userCertificate is treated specially because standard serialization
                // omits the private key (wchich the tunnel needs)
                CertificateToken userCertificate = CertificateToken.FromMessageWithPrivateKey(message);

                TimeToken timeToken = message.Read <TimeToken>();

                this.mIdentityProvider = new CachingIdentityProvider(trustedAuthority, permissionList,
                    networkParticipation, userCertificate, timeToken);
            }
        }

        public override void MakeRequest()
        {
            TrustedAuthorityToken trustedAuthority = this.mIdentityProvider.GetTrustedAuthorityToken();
            PermissionListToken permissionList = this.mIdentityProvider.GetPermissionListToken();
            SymmetricKeyToken networkParticipation = this.mIdentityProvider.GetNetworkParticipationToken();
            CertificateToken userCertificate = this.mIdentityProvider.GetUserCertificateToken();
            TimeToken timeToken = this.mIdentityProvider.GetTimeToken();

            using (MessageBuffer message = new MessageBuffer())
            {
                message.Write(trustedAuthority);
                message.Write(permissionList);
                message.Write(networkParticipation);
                userCertificate.ToMessageWithPrivateKey(message);
                message.Write(timeToken);
                
                string body = Convert.ToBase64String(message.GetBuffer());
                this.mSessionId = this.mSessionManager.MakeRequest("POST", false, "/sessions", body).Trim();
            }
        }
    }
}
