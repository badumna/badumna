﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Badumna.DataTypes;

namespace Badumna.Facade.Http
{
    internal class CustomMessageRequest : Request
    {
        private BadumnaId mEntityId;

        private MemoryStream mEventData;
        public MemoryStream EventData { get { return this.mEventData; } }

        public CustomMessageRequest(SessionManager sessionManager, BadumnaId entityId, MemoryStream eventData)
            : base(sessionManager)
        {
            this.mEntityId = entityId;
            this.mEventData = eventData;
        }

        public CustomMessageRequest(TextReader reader)
            : base(null)
        {
            try
            {
                this.mEventData = new MemoryStream(Convert.FromBase64String(reader.ReadLine()));
            }
            catch (Exception)
            {
            }

            if (this.mEventData == null)
            {
                throw new ArgumentException("Invalid custom message request");
            }
        }

        public override void MakeRequest()
        {
            string body = Convert.ToBase64String(this.mEventData.ToArray());
            this.mSessionManager.SessionPost("entities/" + this.mEntityId.ToString() + "/custom", body);
        }
    }
}
