﻿//-----------------------------------------------------------------------
// <copyright file="ChangeIdentifier.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Facade.Http.Change
{
    /// <summary>
    /// Contains a set of constant strings used to identify the types of change events
    /// sent from the tunnel server back to the tunnel client.
    /// </summary>
    internal static class ChangeIdentifier
    {
        /// <summary>
        /// Sent by the tunnel server when a new replica joins a scene.
        /// </summary>
        public const string SceneJoin = "scnJoin";

        /// <summary>
        /// Sent by the tunnel server when a replica leaves a scene.
        /// </summary>
        public const string ScenePart = "scnPart";

        /// <summary>
        /// Sent by the tunnel server when a replica has been updated. 
        /// </summary>
        public const string EntityUpdate = "scnEntUpd";

        /// <summary>
        /// Sent by the tunnel server when a custom message has been received.
        /// </summary>
        public const string CustomMessage = "scnCustMsg";

        /// <summary>
        /// Sent by the tunnel server when a chat message has been received.
        /// </summary>
        public const string ChatMessage = "chatMsg";

        /// <summary>
        /// Sent by the tunnel server when a private channel invite has been received.
        /// </summary>
        public const string PrivateChannelInvite = "chatInvite";

        /// <summary>
        /// Sent by the tunnel server when a presence change notification has been received.
        /// </summary>
        public const string PresenceChange = "chatPresesnce";

        /// <summary>
        /// Sent by the tunnel server when the result of a connection attempt is known.
        /// </summary>
        public const string ArbitrationConnected = "arbConnect";

        /// <summary>
        /// Sent by the tunnel server when an arbitration connection fails.
        /// </summary>
        public const string ArbitrationConnectionFailed = "arbConnFail";

        /// <summary>
        /// Sent by the tunnel server when an arbitration server event has been received.
        /// </summary>
        public const string ArbitrationServerEvent = "arbSrvEvent";

        /// <summary>
        /// Sent by the tunnel server when a request to send a file has been received.
        /// </summary>
        public const string SendFile = "strmSend";

        /// <summary>
        /// Sent by the tunnel server when a request to receive a file has been received.
        /// </summary>
        public const string ReceiveFile = "strmRecv";
    }
}
