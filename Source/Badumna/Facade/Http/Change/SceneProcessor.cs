﻿//-----------------------------------------------------------------------
// <copyright file="SceneProcessor.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;

using Badumna.DataTypes;
using Badumna.SpatialEntities;
using Badumna.Utilities;
using Badumna.Utilities.LitJson;

namespace Badumna.Facade.Http.Change
{
    /// <summary>
    /// Processes scene-related changes received from the tunnel server.
    /// </summary>
    internal class SceneProcessor
    {
        /// <summary>
        /// Helper class for dead reckoning.
        /// </summary>
        private DeadReckoningHelper deadReckoningHelper;

        /// <summary>
        /// The set of scenes currently subscribed to.
        /// </summary>
        private Dictionary<string, NetworkScene> scenes;

        /// <summary>
        /// The set of entities currently registered.
        /// </summary>
        private Dictionary<BadumnaId, ISpatialOriginal> entities;

        /// <summary>
        /// Initializes a new instance of the SceneProcessor class.
        /// </summary>
        /// <param name="processor">The ChangelistProcessor to register handlers with.</param>
        /// <param name="deadReckoningHelper">Helper class for dead reckoning.</param>
        /// <param name="scenes">A reference to the set of scenes currently subscribed to.</param>
        /// <param name="entities">A reference to the set of entities currently registered.</param>
        public SceneProcessor(
            ChangelistProcessor processor,
            DeadReckoningHelper deadReckoningHelper,
            Dictionary<string, NetworkScene> scenes,
            Dictionary<BadumnaId, ISpatialOriginal> entities)
        {
            this.deadReckoningHelper = deadReckoningHelper;
            this.scenes = scenes;
            this.entities = entities;

            processor.Register(ChangeIdentifier.SceneJoin, this.ProcessSceneJoin);
            processor.Register(ChangeIdentifier.ScenePart, this.ProcessScenePart);
            processor.Register(ChangeIdentifier.EntityUpdate, this.ProcessEntityUpdate);
            processor.Register(ChangeIdentifier.CustomMessage, this.ProcessCustomMessage);
        }

        /// <summary>
        /// Creates an instance of an anonymous type ready for JSON serialization containing properties
        /// describing an scene join event.
        /// </summary>
        /// <param name="sceneName">The name of the relevant scene</param>
        /// <param name="entityId">The id of the entity that has joined the scene</param>
        /// <param name="type">The type of the entity</param>
        /// <returns>An object containing the parameters</returns>
        public static object MakeSceneJoin(string sceneName, BadumnaId entityId, uint type)
        {
            return new
            {
                _cid = ChangeIdentifier.SceneJoin,
                scene = sceneName,
                id = entityId.ToString(),
                type = type,
            };
        }

        /// <summary>
        /// Creates an instance of an anonymous type ready for JSON serialization containing properties
        /// describing an scene part event.
        /// </summary>
        /// <param name="sceneName">The relevant scene name</param>
        /// <param name="entityId">The id of the entity that has left the scene</param>
        /// <returns>An object containing the parameters</returns>
        public static object MakeScenePart(string sceneName, BadumnaId entityId)
        {
            return new
            {
                _cid = ChangeIdentifier.ScenePart,
                scene = sceneName,
                id = entityId.ToString(),
            };
        }

        /// <summary>
        /// Creates an instance of an anonymous type ready for JSON serialization containing properties
        /// describing an entity update.
        /// </summary>
        /// <param name="entityId">The id of the entity that the update applies to</param>
        /// <param name="position">The new position</param>
        /// <param name="velocity">The new velocity</param>
        /// <param name="radius">The new radius</param>
        /// <param name="interestRadius">The new interest radius</param>
        /// <param name="includedParts">A BooleanArray indicating which user specified parts are included in the update</param>
        /// <param name="update">The user specified portion of the update</param>
        /// <returns>An object containing the parameters</returns>
        public static object MakeEntityUpdate(BadumnaId entityId, Vector3 position, Vector3 velocity, float radius, float interestRadius, BooleanArray includedParts, byte[] update)
        {
            return new
            {
                _cid = ChangeIdentifier.EntityUpdate,
                id = entityId.ToString(),
                pos = position,
                vel = velocity,
                r = radius,
                intR = interestRadius,
                parts = includedParts.ToString(),  // TODO: ! Better serialization for this
                update = Convert.ToBase64String(update),  // TODO: Check if json has built in support for binary data
            };
        }

        /// <summary>
        /// Creates an instance of an anonymous type ready for JSON serialization containing properties
        /// describing a custom message.
        /// </summary>
        /// <param name="entityId">The id of the entity that sent the custom message</param>
        /// <param name="isToRemote">Indicates whether the message is to a remote or local entity</param>
        /// <param name="data">The body of the message</param>
        /// <returns>An object containing the parameters</returns>
        public static object MakeCustomMessage(BadumnaId entityId, bool isToRemote, byte[] data)
        {
            return new
            {
                _cid = ChangeIdentifier.CustomMessage,
                id = entityId.ToString(),
                toRem = isToRemote,
                data = Convert.ToBase64String(data),
            };
        }

        /// <summary>
        /// Processes a scene join event received from the tunnel server.
        /// </summary>
        /// <param name="json">The parameters for the event</param>
        private void ProcessSceneJoin(JsonData json)
        {
            string sceneName = (string)json["scene"];
            BadumnaId entityId = BadumnaId.TryParse((string)json["id"]);
            uint type = (uint)json["type"];

            if (entityId == null)
            {
                throw new ArgumentException("Malformed entity id in SceneJoin change");
            }

            NetworkScene scene;
            if (!this.scenes.TryGetValue(sceneName, out scene))
            {
                Logger.TraceInformation(LogTag.Facade, "Got SceneJoin change for unknown scene");
                return;
            }

            scene.InstantiateReplica(entityId, type);
        }

        /// <summary>
        /// Processes a scene part event received from the tunnel server.
        /// </summary>
        /// <param name="json">The parameters for the event</param>
        private void ProcessScenePart(JsonData json)
        {
            string sceneName = (string)json["scene"];
            BadumnaId entityId = BadumnaId.TryParse((string)json["id"]);

            if (entityId == null)
            {
                throw new ArgumentException("Malformed entity id in ScenePart change");
            }

            NetworkScene scene;
            if (!this.scenes.TryGetValue(sceneName, out scene))
            {
                Logger.TraceInformation(LogTag.Facade, "Got ScenePart change for unknown scene");
                return;
            }

            ISpatialReplica entity;
            if (!scene.Replicas.TryGetValue(entityId, out entity))
            {
                Logger.TraceInformation(LogTag.Facade, "Got ScenePart change for unknown entity");
                return;
            }

            scene.RemoveReplica(entity);
        }

        /// <summary>
        /// Processes an entity update received from the tunnel server.
        /// </summary>
        /// <param name="json">The parameters for the update</param>
        private void ProcessEntityUpdate(JsonData json)
        {
            BadumnaId entityId = BadumnaId.TryParse((string)json["id"]);
            Vector3 position = Vector3.FromJson(json["pos"]);
            Vector3 velocity = Vector3.FromJson(json["vel"]);
            float radius = (float)json["r"];
            float interestRadius = (float)json["intR"];
            BooleanArray includedParts = BooleanArray.TryParse((string)json["parts"]);
            byte[] update = Convert.FromBase64String((string)json["update"]);

            if (entityId == null)
            {
                throw new ArgumentException("Malformed entity id in EntityUpdate change");
            }

            // TODO: !!! Eliminate the scan over scenes
            ISpatialReplica remoteEntity = null;
            foreach (NetworkScene scene in this.scenes.Values)
            {
                if (scene.Replicas.TryGetValue(entityId, out remoteEntity))
                {
                    break;
                }
            }

            if (remoteEntity == null)
            {
                Logger.TraceInformation(LogTag.Facade, "Got EntityUpdate for unknown entity");
                return;
            }

            if (remoteEntity is IDeadReckonable)
            {
                this.deadReckoningHelper.UpdateApplied((IDeadReckonable)remoteEntity, position, velocity, 0);  // TODO: correct estimated delay!
            }
            else
            {
                remoteEntity.Position = position;
            }

            remoteEntity.Radius = radius;
            remoteEntity.AreaOfInterestRadius = interestRadius;

            using (MemoryStream stream = new MemoryStream(update))
            {
                remoteEntity.Deserialize(includedParts, stream, 0);  // TODO: Determine approximate delay
            }
        }

        /// <summary>
        /// Processes a custom message received from the tunnel server.
        /// </summary>
        /// <param name="json">The parameters for the message</param>
        private void ProcessCustomMessage(JsonData json)
        {
            BadumnaId entityId = BadumnaId.TryParse((string)json["id"]);
            bool isToRemote = (bool)json["toRem"];
            byte[] data = Convert.FromBase64String((string)json["data"]);

            if (entityId == null)
            {
                throw new ArgumentException("Malformed entity id in CustomMessage change");
            }

            if (isToRemote)
            {
                // TODO: !!! Eliminate the scan over scenes
                ISpatialReplica remoteEntity = null;
                foreach (NetworkScene scene in this.scenes.Values)
                {
                    if (scene.Replicas.TryGetValue(entityId, out remoteEntity))
                    {
                        break;
                    }
                }

                if (remoteEntity == null)
                {
                    Logger.TraceInformation(LogTag.Facade, "Got CustomMessage for unknown entity");
                    return;
                }

                using (MemoryStream stream = new MemoryStream(data))
                {
                    remoteEntity.HandleEvent(stream);
                }
            }
            else
            {
                ISpatialOriginal localEntity;
                if (!this.entities.TryGetValue(entityId, out localEntity))
                {
                    Logger.TraceInformation(LogTag.Facade, "Got CustomMessage for unknown entity");
                    return;
                }

                using (MemoryStream stream = new MemoryStream(data))
                {
                    localEntity.HandleEvent(stream);
                }
            }
        }
    }
}
