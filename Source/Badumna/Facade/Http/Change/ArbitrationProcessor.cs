﻿//-----------------------------------------------------------------------
// <copyright file="ArbitrationProcessor.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;

using Badumna.Utilities.LitJson;

namespace Badumna.Facade.Http.Change
{
    /// <summary>
    /// Processes arbitration-related changes received from the tunnel server.
    /// </summary>
    internal class ArbitrationProcessor
    {
        /// <summary>
        /// The arbitration clients currently in use.
        /// </summary>
        private Dictionary<string, HttpArbitrationClient> arbitrationClients;

        /// <summary>
        /// Initializes a new instance of the ArbitrationProcessor class.
        /// </summary>
        /// <param name="processor">The ChangelistProcessor to register handlers with.</param>
        /// <param name="arbitrationClients">A reference to the arbitration clients currently in use.</param>
        public ArbitrationProcessor(ChangelistProcessor processor, Dictionary<string, HttpArbitrationClient> arbitrationClients)
        {
            this.arbitrationClients = arbitrationClients;

            processor.Register(ChangeIdentifier.ArbitrationConnected, this.ProcessConnected);
            processor.Register(ChangeIdentifier.ArbitrationConnectionFailed, this.ProcessConnectionFailed);
            processor.Register(ChangeIdentifier.ArbitrationServerEvent, this.ProcessServerEvent);
        }

        /// <summary>
        /// Creates an instance of an anonymous type ready for JSON serialization containing properties
        /// describing the result of an arbitration server connection attempt.
        /// </summary>
        /// <param name="name">The name of the arbitration server</param>
        /// <param name="type">The result type.</param>
        /// <returns>An object containing the parameters</returns>
        public static object MakeConnectedEvent(string name, ServiceConnectionResultType type)
        {
            return new
            {
                _cid = ChangeIdentifier.ArbitrationConnected,
                arb = name,
                res = type
            };
        }

        /// <summary>
        /// Creates an instance of an anonymous type ready for JSON serialization containing properties
        /// describing an arbitration connection failure event.
        /// </summary>
        /// <param name="name">The name of the arbitration server</param>
        /// <returns>An object containing the parameters</returns>
        public static object MakeConnectionFailedEvent(string name)
        {
            return new
            {
                _cid = ChangeIdentifier.ArbitrationConnectionFailed,
                arb = name
            };
        }

        /// <summary>
        /// Creates an instance of an anonymous type ready for JSON serialization containing properties
        /// describing an arbitration server event.
        /// </summary>
        /// <param name="name">The name of the arbitration server</param>
        /// <param name="message">The data sent with the arbitration server event</param>
        /// <returns>An object containing the parameters</returns>
        public static object MakeServerEvent(string name, byte[] message)
        {
            return new
            {
                _cid = ChangeIdentifier.ArbitrationServerEvent,
                arb = name,
                msg = Convert.ToBase64String(message),
            };
        }

        /// <summary>
        /// Processes an arbitration connected event received from the tunnel server.
        /// </summary>
        /// <param name="json">The parameters for the event</param>
        private void ProcessConnected(JsonData json)
        {
            string name = (string)json["arb"];
            this.arbitrationClients[name].OnConnected((ServiceConnectionResultType)(int)json["res"]);
        }

        /// <summary>
        /// Processes an arbitration connection failed event received from the tunnel server.
        /// </summary>
        /// <param name="json">The parameters for the event</param>
        private void ProcessConnectionFailed(JsonData json)
        {
            string name = (string)json["arb"];
            this.arbitrationClients[name].OnConnectionFailed();
        }

        /// <summary>
        /// Processes an arbitration server event received from the tunnel server.
        /// </summary>
        /// <param name="json">The parameters for the event</param>
        private void ProcessServerEvent(JsonData json)
        {
            string name = (string)json["arb"];
            byte[] message = Convert.FromBase64String((string)json["msg"]);
            this.arbitrationClients[name].OnMessage(message);
        }
    }
}
