﻿//-----------------------------------------------------------------------
// <copyright file="ChatProcessor.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using Badumna.Chat;
using Badumna.DataTypes;
using Badumna.Security;
using Badumna.Utilities.LitJson;

namespace Badumna.Facade.Http.Change
{
    /// <summary>
    /// Processes chat-related changes received from the tunnel server.
    /// </summary>
    internal class ChatProcessor
    {
        /// <summary>
        /// The chat service to use 
        /// </summary>
        private HttpTunnelChatService chatService;

        /// <summary>
        /// Initializes a new instance of the ChatProcessor class.
        /// </summary>
        /// <param name="processor">The ChangelistProcessor to register handlers with.</param>
        /// <param name="chatService">The chat service to pass change requests to.</param>
        public ChatProcessor(ChangelistProcessor processor, HttpTunnelChatService chatService)
        {
            this.chatService = chatService;
            processor.Register(ChangeIdentifier.ChatMessage, this.ProcessChatMessage);
            processor.Register(ChangeIdentifier.PrivateChannelInvite, this.ProcessPrivateChannelInvite);
            processor.Register(ChangeIdentifier.PresenceChange, this.ProcessPresenceChange);
        }

        /// <summary>
        /// Creates an instance of an anonymous type ready for JSON serialization containing properties
        /// describing a chat message.
        /// </summary>
        /// <param name="channelId">The channel the chat message arrived on</param>
        /// <param name="userId">The user id that sent the mesage</param>
        /// <param name="message">The message body</param>
        /// <returns>An object containing the parameters</returns>
        public static object MakeChatMessage(ChatChannelId channelId, BadumnaId userId, string message)
        {
            return new
            {
                _cid = ChangeIdentifier.ChatMessage,
                chanid = channelId.ToInvariantIDString(),
                user = userId.ToString(),
                msg = message,
            };
        }

        /// <summary>
        /// Creates an instance of an anonymous type ready for JSON serialization containing properties
        /// describing a private channel invitation.
        /// </summary>
        /// <param name="channelId">The id of the private channel</param>
        /// <param name="username">The name of the user who sent the invitation</param>
        /// <returns>An object containing the parameters</returns>
        public static object MakePrivateChannelInvite(ChatChannelId channelId, string username)
        {
            return new
            {
                _cid = ChangeIdentifier.PrivateChannelInvite,
                chanid = channelId.ToInvariantIDString(),
                user = username,
            };
        }

        /// <summary>
        /// Creates an instance of an anonymous type ready for JSON serialization containing properties
        /// describing a presence change.
        /// </summary>
        /// <param name="channelId">The id of the channel</param>
        /// <param name="sender">The id of the sender</param>
        /// <param name="displayName">The display name of the sender</param>
        /// <param name="status">The new status of the sender</param>
        /// <returns>An object containing the parameters</returns>
        public static object MakePresenceChange(ChatChannelId channelId, BadumnaId sender, string displayName, ChatStatus status)
        {
            return new
            {
                _cid = ChangeIdentifier.PresenceChange,
                chanid = channelId.ToInvariantIDString(),
                user = sender.ToString(),
                dispName = displayName,
                status = status
            };
        }

        /// <summary>
        /// Processes a chat message received from the tunnel server.
        /// </summary>
        /// <param name="json">The parameters describing the message</param>
        private void ProcessChatMessage(JsonData json)
        {
            /* TODO: How to ensure the correct callback function is called on the source peer? [DGC Aug 2010: Check this todo and remove it if it's invalid] */

            ChatChannelId channelId = ChatChannelId.TryParse((string)json["chanid"]);
            BadumnaId userId = BadumnaId.TryParse((string)json["user"]);
            string message = (string)json["msg"];

            if (channelId == null)
            {
                throw new ArgumentException("Malformed channel id in ChatMessage change");
            }

            if (userId == null)
            {
                throw new ArgumentException("Malformed user id in ChatMessage change");
            }

            this.chatService.ReceiveMessage(channelId, userId, message);
        }

        /// <summary>
        /// Processes a private channel invitation received from the tunnel server.
        /// </summary>
        /// <param name="json">The parameters describing the message</param>
        private void ProcessPrivateChannelInvite(JsonData json)
        {
            ChatChannelId channelId = ChatChannelId.TryParse((string)json["chanid"]);
            if (channelId == null)
            {
                throw new ArgumentException("Malformed channel id in PrivateChannelInvite change");
            }

            string user = (string)json["user"];

            this.chatService.SessionFor(null).NewPrivateChannelInvite(channelId, user);
        }

        /// <summary>
        /// Processes a presence change notification received from the tunnel server.
        /// </summary>
        /// <param name="json">The parameters describing the message</param>
        private void ProcessPresenceChange(JsonData json)
        {
            ChatChannelId channelId = ChatChannelId.TryParse((string)json["chanid"]);
            if (channelId == null)
            {
                throw new ArgumentException("Malformed channel id in PresenceChange change");
            }
            
            BadumnaId userId = BadumnaId.TryParse((string)json["user"]);
            if (userId == null)
            {
                throw new ArgumentException("Malformed user id in PresenceChange change");
            }

            string displayName = (string)json["dispName"];

            ChatStatus status = (ChatStatus)(int)json["status"];

            this.chatService.ReceivePresence(channelId, userId, displayName, status);
        }
    }
}
