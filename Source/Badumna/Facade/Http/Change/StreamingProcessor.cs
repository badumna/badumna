﻿//-----------------------------------------------------------------------
// <copyright file="StreamingProcessor.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;

using Badumna.Facade.Http.Streaming;
using Badumna.Streaming;
using Badumna.Utilities;
using Badumna.Utilities.LitJson;

namespace Badumna.Facade.Http.Change
{
    /// <summary>
    /// Processes streaming-related changes received from the tunnel server.
    /// </summary>
    internal class StreamingProcessor
    {
        /// <summary>
        /// The streaming manager to pass requests to.
        /// </summary>
        private HttpTunnelStreamingManager streamingManager;

        /// <summary>
        /// Initializes a new instance of the StreamingProcessor class.
        /// </summary>
        /// <param name="processor">The ChangelistProcessor to register handlers with.</param>
        /// <param name="streamingManager">The streaming manager to pass requests to.</param>
        public StreamingProcessor(ChangelistProcessor processor, HttpTunnelStreamingManager streamingManager)
        {
            this.streamingManager = streamingManager;
            processor.Register(ChangeIdentifier.ReceiveFile, this.ProcessReceiveFile);
            processor.Register(ChangeIdentifier.SendFile, this.ProcessSendFile);
        }

        /// <summary>
        /// Creates an instance of an anonymous type ready for JSON serialization containing properties
        /// describing a receive file request.
        /// </summary>
        /// <param name="eventArgs">The arguments describing the request</param>
        /// <param name="requestId">The id of the request</param>
        /// <returns>An object containing the parameters</returns>
        public static object MakeReceiveFile(ReceiveStreamEventArgs eventArgs, int requestId)
        {
            return StreamingProcessor.MakeStreamRequest(ChangeIdentifier.ReceiveFile, eventArgs, requestId);
        }

        /// <summary>
        /// Creates an instance of an anonymous type ready for JSON serialization containing properties
        /// describing a send file request.
        /// </summary>
        /// <param name="eventArgs">The arguments describing the request</param>
        /// <param name="requestId">The id of the request</param>
        /// <returns>An object containing the parameters</returns>
        public static object MakeSendFile(SendStreamEventArgs eventArgs, int requestId)
        {
            return StreamingProcessor.MakeStreamRequest(ChangeIdentifier.SendFile, eventArgs, requestId);
        }

        /// <summary>
        /// Creates an instance of an anonymous type ready for JSON serialization containing properties
        /// describing a stream request.
        /// </summary>
        /// <param name="changeIdentifier">Identifies the change type</param>
        /// <param name="eventArgs">The arguments describing the request</param>
        /// <param name="requestId">The id of the request</param>
        /// <returns>An object containing the parameters</returns>
        /* TODO: Passing of request ID is dodgy++; it's also part of ReceiveFileEventArgs
                 but not accessible.  Should do an implementation of ReceiveFileEventArgs for the tunnel?? */
        private static object MakeStreamRequest(string changeIdentifier, StreamRequestEventArgs eventArgs, int requestId)
        {
            string additionalData = eventArgs.AdditionalData != null ? Convert.ToBase64String(eventArgs.AdditionalData) : null;

            return new
            {
                _cid = changeIdentifier,
                tag = eventArgs.StreamTag,
                name = eventArgs.StreamName,
                data = additionalData,
                size = eventArgs.SizeKiloBytes,
                user = eventArgs.UserName,
                id = requestId,
            };
        }

        /// <summary>
        /// Parses a StreamRequest and request id from JSON data.
        /// </summary>
        /// <param name="json">The JsonData to parse</param>
        /// <param name="requestId">The parsed request id</param>
        /// <returns>The parsed StreamRequest</returns>
        private StreamRequest ParseStreamRequest(JsonData json, out int requestId)
        {
            string streamTag = (string)json["tag"];
            string streamName = (string)json["name"];
            string dataString = (string)json["data"];
            byte[] additionalData = dataString != null ? Convert.FromBase64String(dataString) : null;
            double size = (double)json["size"];
            string username = (string)json["user"];
            requestId = (int)json["id"];

            // TODO: !!! Are stream ID, source address, or data rate required??
            return new StreamRequest(StreamingManager.TaggedName(streamTag, streamName), additionalData, (long)size, username, new CyclicalID.UShortID());
        }

        /// <summary>
        /// Processes a receive file request received from the tunnel server.
        /// </summary>
        /// <param name="json">The parameters for the event</param>
        private void ProcessReceiveFile(JsonData json)
        {
            int requestId;
            StreamRequest request = this.ParseStreamRequest(json, out requestId);
            this.streamingManager.ReceiveRequest(request, requestId);
        }

        /// <summary>
        /// Processes a send file request received from the tunnel server.
        /// </summary>
        /// <param name="json">The parameters for the event</param>
        private void ProcessSendFile(JsonData json)
        {
            int requestId;
            StreamRequest request = this.ParseStreamRequest(json, out requestId);
            this.streamingManager.SendRequest(request, requestId);
        }
    }
}
