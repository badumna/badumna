﻿//-----------------------------------------------------------------------
// <copyright file="HttpTunnelChatSession.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2012 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Badumna.Chat;
using Badumna.DataTypes;
using Badumna.SpatialEntities;
using Badumna.Utilities;

namespace Badumna.Facade.Http
{
    /// <summary>
    /// Implements ChatSession via the Http tunnel.
    /// </summary>
    internal class HttpTunnelChatSession : IChatSession
    {
        /// <summary>
        /// A collection of received private invitations.
        /// </summary>
        private Dictionary<ChatChannelId, string> receivedInvitations = new Dictionary<ChatChannelId, string>();
        
        /// <summary>
        /// A mapping of channel IDs to HttpChannels.
        /// </summary>
        private Dictionary<ChatChannelId, HttpChatChannel> channels = new Dictionary<ChatChannelId, HttpChatChannel>();
        
        /// <summary>
        /// The invitation handler to be invoked by incoming invitations.
        /// </summary>
        private ChatInvitationHandler invitationHandler;

        /// <summary>
        /// The session manager, used to send requests to the tunnel server.
        /// </summary>
        private SessionManager sessionManager;

        /// <summary>
        /// Initializes a new instance of the HttpTunnelChatSession class.
        /// </summary>
        /// <param name="sessionManager">The session manager, used to send requests to the tunnel server.</param>
        public HttpTunnelChatSession(SessionManager sessionManager)
        {
            this.sessionManager = sessionManager;
        }

        /// <inheritdoc/>
        public IChatChannel SubscribeToProximityChannel(BadumnaId entityId, ChatMessageHandler messageHandler)
        {
            ChatChannelId channelId = new ChatChannelId(ChatChannelType.Proximity, entityId);
            return this.SubscribeToChatChannel(channelId, messageHandler, null);
        }

        /// <inheritdoc/>
        public IChatChannel SubscribeToProximityChannel(IReplicableEntity entity, ChatMessageHandler messageHandler)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc/>
        public void OpenPrivateChannels(ChatInvitationHandler invitationHandler)
        {
            if (this.invitationHandler != null)
            {
                throw new InvalidOperationException("Private channels already opened.");
            }

            this.invitationHandler = invitationHandler;
        }

        /// <inheritdoc/>
        public void InviteUserToPrivateChannel(string username)
        {
            new InviteToPrivateChannelRequest(this.sessionManager, username).MakeRequest();
        }

        /// <inheritdoc/>
        public IChatChannel AcceptInvitation(ChatChannelId channelId, ChatMessageHandler messageHandler, ChatPresenceHandler presenceHandler)
        {
            string username;
            if (!this.receivedInvitations.TryGetValue(channelId, out username))
            {
                throw new InvalidOperationException("Attempt to accept unknown invitation");
            }

            return this.SubscribeToChatChannel(channelId, messageHandler, presenceHandler);
        }

        /// <inheritdoc/>
        public void ChangePresence(ChatStatus status)
        {
            new ChangePresenceRequest(this.sessionManager, status).MakeRequest();
        }

        /// <inheritdoc/>
        public void Logout()
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc/>
        internal void ReceiveMessage(ChatChannelId channelId, BadumnaId senderId, string message)
        {
            HttpChatChannel channel;
            if (this.channels.TryGetValue(channelId, out channel))
            {
                channel.HandleMessage(senderId, message);
            }
            else
            {
                Logger.TraceInformation(LogTag.Facade, "Got message for unknown channel: " + channelId);
            }
        }

        /// <summary>
        /// Processes a presence change notification.
        /// </summary>
        /// <param name="channelId">The channel id.</param>
        /// <param name="senderId">The sender's id.</param>
        /// <param name="displayName">The sender's display name.</param>
        /// <param name="status">The sender's new status.</param>
        internal void NewPresenceChange(ChatChannelId channelId, BadumnaId senderId, string displayName, ChatStatus status)
        {
            HttpChatChannel channel;
            if (this.channels.TryGetValue(channelId, out channel))
            {
                channel.HandlePresence(senderId, displayName, status);
            }
            else
            {
                Logger.TraceInformation(LogTag.Facade, "Got presence for unknown channel: " + channelId);
            }
        }

        /// <summary>
        /// Creates a new private channel invite.
        /// </summary>
        /// <param name="channel">The channel.</param>
        /// <param name="username">The username.</param>
        internal void NewPrivateChannelInvite(ChatChannelId channel, string username)
        {
            this.receivedInvitations[channel] = username;

            // TODO: Only invoke callback from ProcessNetworkState?
            ChatInvitationHandler handler = this.invitationHandler;
            if (handler != null)
            {
                handler(channel, username);
            }
        }

        /// <summary>
        /// Subscribe to a chat channel.
        /// </summary>
        /// <param name="channelId">The channel ID</param>
        /// <param name="messageHandler">A message handler</param>
        /// <param name="presenceHandler">A presence handler</param>
        /// <returns>The chat channel.</returns>
        private IChatChannel SubscribeToChatChannel(ChatChannelId channelId, ChatMessageHandler messageHandler, ChatPresenceHandler presenceHandler)
        {
            new SubscribeToChannelRequest(this.sessionManager, channelId).MakeRequest();
            HttpChatChannel channel = new HttpChatChannel(channelId, messageHandler, presenceHandler, this.sessionManager);
            this.channels[channelId] = channel;
            return channel;
        }
    }
}
