﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using System.Globalization;

using Badumna.Core;
using Badumna.Chat;
using Badumna.DataTypes;
using Badumna.Security;

namespace Badumna.Facade.Http
{
    internal abstract class Request
    {
        protected SessionManager mSessionManager;

        public Request(SessionManager sessionManager)
        {
            this.mSessionManager = sessionManager;
        }

        public abstract void MakeRequest();
    }

    internal class SubscribeToChannelRequest : Request
    {
        private ChatChannelId channelId;

        public SubscribeToChannelRequest(SessionManager sessionManager, ChatChannelId channelId)
            : base(sessionManager)
        {
            this.channelId = channelId;
        }

        public override void MakeRequest()
        {
            this.mSessionManager.SessionPost("/chat/1/channel", this.channelId.ToInvariantIDString());
        }
    }

    internal class InviteToPrivateChannelRequest : Request
    {
        private string mUsername;

        public InviteToPrivateChannelRequest(SessionManager sessionManager, string username)
            : base(sessionManager)
        {
            this.mUsername = username;
        }

        public override void MakeRequest()
        {
            this.mSessionManager.SessionPost("/chat/1/invite", this.mUsername);
        }
    }

    internal class ChangePresenceRequest : Request
    {
        private ChatStatus mChatStatus;

        public ChangePresenceRequest(SessionManager sessionManager, ChatStatus chatStatus)
            : base(sessionManager)
        {
            this.mChatStatus = chatStatus;
        }

        public override void MakeRequest()
        {
            this.mSessionManager.SessionPost("/chat/1/presence", ((int)this.mChatStatus).ToString());
        }
    }
}
