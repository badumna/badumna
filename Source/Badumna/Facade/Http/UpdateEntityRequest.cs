﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Globalization;

using Badumna.Replication;
using Badumna.DataTypes;
using Badumna.SpatialEntities;
using Badumna.Utilities;
using System.Diagnostics;
using Badumna.Utilities.LitJson;
using System.Collections;

namespace Badumna.Facade.Http
{
    internal static class UpdateEntityRequest
    {
        public delegate void PartChange(int partId, byte[] part);

        public static void UpdateEntityFromJson(JsonData json, ISpatialOriginal original, PartChange partChange)
        {
            JsonData partIds = json["ids"];
            JsonData parts = json["parts"];

            for (int i = 0; i < partIds.Count; i++)
            {
                int partId = (int)partIds[i];
                switch (partId)
                {
                    case (int)SpatialEntityStateSegment.Scene:
                        // TODO: Implement
                        break;

                    case (int)SpatialEntityStateSegment.Position:
                        original.Position = Vector3.FromJson(parts[i]);
                        break;

                    case (int)SpatialEntityStateSegment.Velocity:
                        ((IDeadReckonable)original).Velocity = Vector3.FromJson(parts[i]);
                        break;

                    case (int)SpatialEntityStateSegment.Radius:
                        original.Radius = (float)parts[i];
                        break;

                    case (int)SpatialEntityStateSegment.InterestRadius:
                        original.AreaOfInterestRadius = (float)parts[i];
                        break;

                    default:
                        byte[] part = Convert.FromBase64String((string)parts[i]);
                        partChange(partId, part);
                        break;
                }
            }
        }

        public static object UpdateToJson(ISpatialOriginal entity, BooleanArray changedParts)
        {
            int maxIndex = changedParts.HighestUsedIndex;
            if (maxIndex < 0)
            {
                // This can occur when, e.g., changedParts is new BooleanArray(true).
                // TODO: Currently in this case we have to call Serialize up to 127 times.
                //       Find a better method that is still robust.
                maxIndex = BooleanArray.MaxIndex;
            }

            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            // !!! If this code is changed then SpatialOriginalWrapper.Serialize and related code must also be reviewed. !!!
            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            // TODO: Refactor to remove duplicated logic.

            if (entity is IDeadReckonable)
            {
                // Position <=> Velocity
                changedParts[(int)SpatialEntityStateSegment.Position] |= changedParts[(int)SpatialEntityStateSegment.Velocity];
                changedParts[(int)SpatialEntityStateSegment.Velocity] |= changedParts[(int)SpatialEntityStateSegment.Position];
            }
            else
            {
                // Support for all flags true for inital update, etc without worrying about whether the entity actually has velocity or not
                changedParts[(int)SpatialEntityStateSegment.Velocity] = false;
            }


            List<int> partIds = new List<int>();
            List<object> parts = new List<object>();

            for (int i = 0; i <= maxIndex; i++)
            {
                if (!changedParts[i])
                {
                    continue;
                }

                partIds.Add(i);

                switch (i)
                {
                    case (int)SpatialEntityStateSegment.Scene:
                        partIds.Remove(i);  // TODO: Remove this line and actually add the scene name instead (is it actually required when using the tunnel?)
                        break;

                    case (int)SpatialEntityStateSegment.Position:
                        parts.Add(entity.Position);
                        break;

                    case (int)SpatialEntityStateSegment.Velocity:
                        parts.Add(((IDeadReckonable)entity).Velocity);
                        break;

                    case (int)SpatialEntityStateSegment.Radius:
                        parts.Add(entity.Radius);
                        break;

                    case (int)SpatialEntityStateSegment.InterestRadius:
                        parts.Add(entity.AreaOfInterestRadius);
                        break;

                    default:
                        if (i < (int)SpatialEntityStateSegment.FirstAvailableSegment)
                        {
                            Debug.Assert(false, "Unknown non-user spatial entity state segment");
                            partIds.Remove(i);
                            continue;
                        }

                        string updateBase64 = "";
                        using (MemoryStream updateStream = new MemoryStream())
                        {
                            entity.Serialize(new BooleanArray(i), updateStream);
                            if (updateStream.Length > 0)
                            {
                                updateBase64 = Convert.ToBase64String(updateStream.GetBuffer(), 0, (int)updateStream.Length);
                            }
                        }

                        if (updateBase64.Length > 0)
                        {
                            parts.Add(updateBase64);
                        }
                        else
                        {
                            partIds.Remove(i);
                        }

                        break;
                }
            }

            return new
            {
                ids = partIds,
                parts = parts
            };
        }

        public static void MakeRequest(SessionManager sessionManager, ISpatialOriginal entity, BooleanArray changedParts)
        {
            sessionManager.SessionPost("entities/" + Uri.EscapeDataString(entity.Guid.ToString()), UpdateEntityRequest.UpdateToJson(entity, changedParts));
        }
    }
}
