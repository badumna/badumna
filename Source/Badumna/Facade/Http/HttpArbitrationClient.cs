﻿//-----------------------------------------------------------------------
// <copyright file="HttpArbitrationClient.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;

using Badumna.Arbitration;

namespace Badumna.Facade.Http
{
    /// <summary>
    /// Implementation of IArbitrator for the HTTP tunnel facade
    /// </summary>
    internal class HttpArbitrationClient : IArbitrator
    {
        /// <summary>
        /// The name of the arbitration server
        /// </summary>
        private string name;

        /// <summary>
        /// The session manager for the current HTTP tunnel session
        /// </summary>
        private SessionManager sessionManager;

        /// <summary>
        /// Temporary storage for the connection result handler passed to the Connect(...) method.
        /// </summary>
        private ArbitrationConnectionResultHandler connectionResultHandler;

        /// <summary>
        /// Callback to notify connection failure.
        /// </summary>
        private HandleConnectionFailure connectionFailedHandler = null;

        /// <summary>
        /// A handler to be invoked when a new event arrives from the arbitration server.
        /// </summary>
        private HandleServerMessage serverEventHandler = null;

        /// <summary>
        /// Initializes a new instance of the HttpArbitrationClient class.
        /// </summary>
        /// <param name="sessionManager">The current HTTP tunnel session manager</param>
        /// <param name="name">The name of the arbitration server</param>
        public HttpArbitrationClient(SessionManager sessionManager, string name)
        {
            this.sessionManager = sessionManager;
            this.name = name;

            // this.sessionManager.SessionPost("/arbitrator", name);
        }

        /// <summary>
        /// Gets or sets a value indicating whether a connection with the arbitration server is currently established.
        /// </summary>
        public bool IsServerConnected { get; set; }  // TODO: This should perhaps track the same property on the tunnel server more closely, but not sure that it actually matters.

        /// <summary>
        /// Initiatiates a connection to the arbitration server.
        /// </summary>
        /// <param name="connectionResultHandler">A callback that will be called when connection has successfully completed, or failed.</param>
        /// <param name="connectionFailedHandler">A callback that will be called if connection fails.</param>
        /// <param name="serverEventHandler">A handler to be triggered when the arbitrator sends an event to this client.</param>
        public void Connect(
            ArbitrationConnectionResultHandler connectionResultHandler,
            HandleConnectionFailure connectionFailedHandler,
            HandleServerMessage serverEventHandler)
        {
            this.connectionResultHandler = connectionResultHandler;
            this.connectionFailedHandler = connectionFailedHandler;
            this.serverEventHandler = serverEventHandler;
            this.sessionManager.SessionPost("/arbitrator", this.name);
        }

        /// <summary>
        /// Send an event to the arbitration server.
        /// </summary>
        /// <param name="message">The arbitration message</param>
        public void SendEvent(byte[] message)
        {
            this.sessionManager.SessionPost("/arbitrator/" + Uri.EscapeDataString(this.name), Convert.ToBase64String(message));
        }

        /// <summary>
        /// Called when the arbitration client on the tunnel server completes a connection attempt to the arbitration server.
        /// </summary>
        /// <param name="result">The result type.</param>
        internal void OnConnected(ServiceConnectionResultType result)
        {
            this.IsServerConnected = result == ServiceConnectionResultType.Success;

            ArbitrationConnectionResultHandler handler = this.connectionResultHandler;
            this.connectionResultHandler = null;
            if (handler != null)
            {
                handler(result);
            }
        }

        /// <summary>
        /// Called when the connection on the arbitration client on the tunnel server fails.
        /// </summary>
        internal void OnConnectionFailed()
        {
            this.IsServerConnected = false;

            HandleConnectionFailure handler = this.connectionFailedHandler;
            if (handler != null)
            {
                handler();
            }
        }

        /// <summary>
        /// Called when the arbitration client on the tunnel server receives a new message from the arbitration server.
        /// </summary>
        /// <param name="message">The arbitration message</param>
        internal void OnMessage(byte[] message)
        {
            HandleServerMessage handler = this.serverEventHandler;
            if (handler != null)
            {
                handler(message);
            }
        }
    }
}
