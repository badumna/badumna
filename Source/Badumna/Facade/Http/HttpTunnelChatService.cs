﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Utilities;
using Badumna.Chat;
using Badumna.DataTypes;
using Badumna.Security;


namespace Badumna.Facade.Http
{
    class HttpTunnelChatService
    {
        /// <summary>
        /// The chat session. Only one session is currently supported (network facade currently only exposes a single session instance).
        /// </summary>
        private HttpTunnelChatSession session;

        /// <summary>
        /// The session manager, used to send requests to the tunnel server.
        /// </summary>
        private SessionManager sessionManager;
        
        /// <summary>
        /// Initializes a new instance of the HttpTunnelChatService class.
        /// </summary>
        /// <param name="sessionManager">The session manager, used to send requests to the tunnel server.</param>
        public HttpTunnelChatService(SessionManager sessionManager)
        {
            this.sessionManager = sessionManager;
        }

        public HttpTunnelChatSession CreateSession()
        {
            this.session = new HttpTunnelChatSession(this.sessionManager);
            return this.session;
        }

        internal HttpTunnelChatSession SessionFor(Character character)
        {
            return this.session;
        }

        public void ReceiveMessage(ChatChannelId channelId, BadumnaId senderId, string message)
        {
            this.session.ReceiveMessage(channelId, senderId, message);
        }

        public void ReceivePresence(ChatChannelId channelId, BadumnaId senderId, string displayName, ChatStatus status)
        {
            this.session.NewPresenceChange(channelId, senderId, displayName, status);
        }
    }
}
