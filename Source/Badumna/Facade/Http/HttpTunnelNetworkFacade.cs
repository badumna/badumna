﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading;

using Badumna.Arbitration;
using Badumna.Chat;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Matchmaking;
using Badumna.Security;
using Badumna.ServiceDiscovery;
using Badumna.SpatialEntities;
using Badumna.Streaming;
using Badumna.Utilities;
using Badumna.Utilities.LitJson;
using Badumna.Facade.Http.Streaming;
using Badumna.Facade.Http.Change;
using Badumna.Transport;

namespace Badumna.Facade.Http
{
    class HttpTunnelNetworkFacade : NetworkFacade, INetworkFacadeInternal
    {
        private class UpdatedEntityInfo
        {
            public ISpatialOriginal Entity { get; private set; }
            public BooleanArray ChangedParts { get; private set; }

            public UpdatedEntityInfo(ISpatialOriginal entity)
            {
                this.Entity = entity;
                this.ChangedParts = new BooleanArray();
            }
        }

        /* TODO:  How should this facade report errors to the app that are related to the tunnelling?
         *        Maybe report errors with NetworkContext.Connected / Disconnected events (or whatever they're called)? */

        private bool mIsLoggedIn;
        public override bool IsLoggedIn { get { return this.mIsLoggedIn; } }

        public override InitializationState InitializationProgress
        {
            get { return InitializationState.Complete; }
        }

        private ConnectivityStatusDelegate offlineEventDelegate;
        private ConnectivityStatusDelegate onlineEventDelegate;

        public override event ConnectivityStatusDelegate OfflineEvent
        {
            add { this.offlineEventDelegate += value; }
            remove { this.offlineEventDelegate -= value; }
        }

        public override event ConnectivityStatusDelegate OnlineEvent
        {
            add { this.onlineEventDelegate += value; }
            remove { this.offlineEventDelegate -= value; }
        }

#pragma warning disable 67  // The event 'x' is never used.
        // This event isn't applicable to the HTTP tunnel facade, but it's here so developers can subscribe to it
        // regardless of whether tunnel mode is enabled.
        public override event PublicAddressChangedDelegate AddressChangedEvent;
#pragma warning restore 67

        /// <inheritdoc/>
        public override TypeRegistry TypeRegistry
        {
            get { throw new NotImplementedException(); }
        }

        /// <inheritdoc />
        public override RPCManager RPCManager
        {
            get { throw new NotImplementedException(); }
        }

        private StreamingManager mStreamingManager;
        public override StreamingManager Streaming { get { return this.mStreamingManager; } }

        public override double InboundBytesPerSecond { get { return -1; } }
        
        public override double OutboundBytesPerSecond { get { return -1; } }

        public override double MaximumPacketLossRate { get { return -1; } }

        public override double AveragePacketLossRate { get { return -1; } }

        public override double TotalSendLimitBytesPerSecond { get { return -1; } }

        public override double MaximumSendLimitBytesPerSecond { get { return -1; } }

        /// <inheritdoc />
        public override bool IsOnline
        {
            get { return this.mIsLoggedIn; }
        }

        /// <inheritdoc />
        public override bool IsOffline
        {
            get { return !this.mIsLoggedIn; }
        }

        public override Badumna.Validation.IFacade ValidationFacade
        {
            get { throw new NotImplementedException(); }
        }

        public override Match.Facade Match
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// The configured list of tunnel servers
        /// </summary>
        private List<Uri> mTunnelUris;

        /// <summary>
        /// The tunnel server we're actually using.  Once we've connected, we only talk to this one.
        /// </summary>
        private Uri mTunnelUri;

        private PersistentHttpConnection mChangelistConnection;

        private Dictionary<BadumnaId, UpdatedEntityInfo> mUpdatedEntities = new Dictionary<BadumnaId, UpdatedEntityInfo>();

        private readonly object updatedEntitiesLock = new object();

        internal Dictionary<BadumnaId, ISpatialOriginal> Entities { get { return this.entities; } }

        private DeadReckoningHelper mDeadReckoningHelper = new DeadReckoningHelper();
        public DeadReckoningHelper DeadReckoningHelper { get { return this.mDeadReckoningHelper; } }

        // This is used to determine when an entity has left all scenes.  The Tunnel API requires explicit notification of
        // entity creation and deletion.  In contrast, Badumna's API requires the application to take care of creating and
        // deleting entities as required; it only requires notification of add to scene / remove from scene events.
        //
        // When an entity first joins a scene the tunnel facade will send a create entity message to the tunnel.
        // When an entity leaves all scenes the tunnel facade will send a delete message.
        //
        // TODO: This could probably be factored more nicely
        private Dictionary<BadumnaId, List<NetworkScene>> mEntityScenes = new Dictionary<BadumnaId, List<NetworkScene>>();
        internal Dictionary<BadumnaId, List<NetworkScene>> EntityScenes { get { return this.mEntityScenes; } }

        /// <summary>
        /// Manages handlers for incoming changelists.
        /// </summary>
        private ChangelistProcessor changelistProcessor = new ChangelistProcessor();

        private Dictionary<string, NetworkScene> scenes = new Dictionary<string, NetworkScene>();

        private Dictionary<BadumnaId, ISpatialOriginal> entities = new Dictionary<BadumnaId, ISpatialOriginal>();

        // TODO: Need some mechanism for garbage collecting old arbitration clients.
        private Dictionary<string, HttpArbitrationClient> arbitrationClients = new Dictionary<string, HttpArbitrationClient>();

        private Queue<string> mChangelists = new Queue<string>();
        private volatile bool mGetChangelists;
        private SessionRequest mGetChangelistRequest;
        private AutoResetEvent mChangelistLoopStopped = new AutoResetEvent(false);

        private TimeSpan mLastDispatchMark;

        private SessionManager mSessionManager;
        internal SessionManager SessionManager { get { return this.mSessionManager; } }

        private HttpTunnelChatService mChatService;

        /// <summary>
        /// The queue to use for events that must be executed in the application thread.
        /// </summary>
        private SimpleEventQueue applicationEventQueue = new SimpleEventQueue();

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        ITime INetworkFacadeInternal.TimeKeeper { get { return this.timeKeeper; } }

        /// <summary>
        /// Reports on network connectivity.
        /// </summary>
        private INetworkConnectivityReporter connectivityReporter;

        /// <summary>
        /// The token cache
        /// </summary>
        private TokenCache tokenCache;
        internal override TokenCache TokenCache
        {
            get
            {
                return this.tokenCache;
            }
        }

        public HttpTunnelNetworkFacade(Options options, NetworkEventQueue eventQueue, ITime timeKeeper)
        {
            this.options = options;
            this.eventQueue = eventQueue;
            this.timeKeeper = timeKeeper;
            this.connectivityReporter = new NetworkConnectivityReporter(eventQueue);

            this.mTunnelUris = new List<Uri>();
            foreach (Uri uri in this.options.Connectivity.TunnelUrisAsUris)
            {
                if (uri.Scheme != "http" && uri.Scheme != "https")
                {
                    throw new ConfigurationException(ConfigurationException.ErrorCode.InvalidTunnelUriScheme);
                }

                this.mTunnelUris.Add(uri);
            }

            if (this.mTunnelUris.Count == 0)
            {
                throw new ConfigurationException(ConfigurationException.ErrorCode.NoTunnelServersDefined);
            }

            this.mLastDispatchMark = this.timeKeeper.Now;
        }
        public override void RegisterEntityDetails(float areaOfInterestRadius, float maxEntitySpeed)
        {
            Vector3 maxVelocity = Vector3.UnitX * maxEntitySpeed;
            this.SessionManager.SessionPost("config/entityDetails", new { aoi = areaOfInterestRadius, vel = maxVelocity });
        }

        public override void Shutdown(bool blockUntilComplete)
        {
            this.mGetChangelists = false;
            SessionRequest changelistRequest = this.mGetChangelistRequest;
            if (changelistRequest != null)
            {
                changelistRequest.Abort();
            }
            this.mChangelistLoopStopped.WaitOne();
            this.mChangelists.Clear();
            this.mChangelistConnection.Close();
            this.mChangelistConnection = null;

            this.mChatService = null;
            this.mStreamingManager = null;

            this.SessionManager.SessionDelete("");
            this.mSessionManager.Close();
            this.mSessionManager = null;

            this.mIsLoggedIn = false;

            ConnectivityStatusDelegate offlineHandler = this.offlineEventDelegate;  // TODO: App may not expect this during the call?
            if (offlineHandler != null)
            {
                offlineHandler();
            }
        }

        public override bool Login(IIdentityProvider identityProvider)
        {
            if (this.mIsLoggedIn)
            {
                return true;
            }

            this.connectivityReporter.SetStatus(ConnectivityStatus.Online);

            // Create a token cache and call PreCache to ensure that the tokens are loaded in the correct order.
            // In particular the NetworkClock must be synchronized from a TimeToken before the validity period of other tokens is checked.
            // TODO: Subscribe to token expiry events and resend the tokens to the tunnel server
            this.tokenCache = new TokenCache(this.eventQueue, this.timeKeeper, this.connectivityReporter);
            this.tokenCache.SetIdentityProvider(identityProvider);
            if (!this.tokenCache.PreCache())
            {
                return false;
            }

            this.mTunnelUri = null;

            Random random = new Random();

            while (this.mTunnelUris.Count > 0)
            {
                int index = random.Next(this.mTunnelUris.Count);
                Uri tunnelUri = this.mTunnelUris[index];

                try
                {
                    this.mSessionManager = new SessionManager(tunnelUri, identityProvider);
                    this.mTunnelUri = tunnelUri;
                    break;
                }
                catch (TunnelRequestException tunnelException)
                {
                    WebException webException = tunnelException.InnerException as WebException;
                    if (webException != null)
                    {
                        SimpleWebResponse webResponse = webException.Response as SimpleWebResponse;
                        if (webResponse != null && webResponse.StatusCode == HttpStatusCode.Forbidden)
                        {
                            return false;
                        }
                    }
                }
                catch (Exception e)
                {
                    Logger.TraceException(LogLevel.Error, e, "Error connecting to tunnel server");
                }

                this.mTunnelUris.RemoveAt(index);
            }

            if (this.mTunnelUri == null)
            {
                throw new TunnelRequestException("Could not contact any tunnel server");
            }

            this.mIsLoggedIn = true;

            this.mChatService = new HttpTunnelChatService(this.mSessionManager);
            this.mStreamingManager = new HttpTunnelStreamingManager(
                this.mSessionManager,
                this.timeKeeper,
                x => { throw new NotSupportedException("The Tunnel does not support new-style replication."); });

            // We don't keep extra references to these change processors.  The
            // changelist processor holds the references.
            new SceneProcessor(this.changelistProcessor, this.DeadReckoningHelper, this.scenes, this.entities);
            new ChatProcessor(this.changelistProcessor, this.mChatService);
            new ArbitrationProcessor(this.changelistProcessor, arbitrationClients);
            new StreamingProcessor(this.changelistProcessor, (HttpTunnelStreamingManager)this.Streaming);

            this.mChangelistConnection = new PersistentHttpConnection(this.mTunnelUri);
            this.mGetChangelists = true;
            Thread changeThread = new Thread(this.ChangelistLoop);
            changeThread.IsBackground = true;
            changeThread.Start();

            ConnectivityStatusDelegate onlineHandler = this.onlineEventDelegate;  // TODO: App may not expect this during the call?
            if (onlineHandler != null)
            {
                onlineHandler();
            }

            return true;
        }

        private void ChangelistLoop()
        {
            while (this.mGetChangelists)
            {
                try
                {
                    using (this.mGetChangelistRequest = this.SessionManager.CreateRequestOnConnection(this.mChangelistConnection, "GET", true, "/changelists/latest", null, 5000))
                    using (StreamReader reader = new StreamReader(this.mGetChangelistRequest.MakeRequest()))
                    {
                        string changelist = reader.ReadToEnd();
                        lock (this.mChangelists)
                        {
                            this.mChangelists.Enqueue(changelist);
                        }
                    }
                }
                catch (TunnelRequestException)
                {
                    // Think we always get theses, not WebExceptions
                    // If it's due to an abort we can ignore it.  Otherwise we should notify the app.

                    // TODO: Don't throw TunnelRequestException outside of Badumna.  Instead, if
                    //       tunnel requests fail we should notify the app by an event handler.
                    //       This saves the app from having to try/catch for TunnelRequestExeceptions
                    //       around every single Badumna call.  However the event handler approach
                    //       doesn't necessarily give the app fine grained info about how much has
                    //       failed.  Need to investigate alternate solutions.
                }
                catch (WebException)
                {
                    // TODO: Attempt recovery if required

                    // TODO: Go offline!  keep retrying, go online if success (and re-establish session if it's timed out)
                }
                finally
                {
                    this.mGetChangelistRequest = null;
                }

                Thread.Sleep(100);  // TODO: This is a bit crude
            }

            this.mChangelistLoopStopped.Set();
        }

        public override NetworkStatus GetNetworkStatus()
        {
            throw new NotSupportedException();
        }

        public override NetworkScene JoinScene(string sceneName, CreateSpatialReplica createEntityDelegate, RemoveSpatialReplica removeEntityDelegate)
        {
            this.SessionManager.SessionPost("scenes/", sceneName);
            HttpTunnelNetworkScene scene = new HttpTunnelNetworkScene(this, sceneName, createEntityDelegate, removeEntityDelegate, this.eventQueue, this.applicationEventQueue.Add);
            this.scenes[sceneName] = scene;
            return scene;
        }

        public override NetworkScene JoinMiniScene(string sceneName, CreateSpatialReplica createEntityDelegate, RemoveSpatialReplica removeEntityDelegate)
        {
            throw new NotSupportedException();
        }

        private UpdatedEntityInfo GetOrCreateUpdatedEntityInfo(ISpatialOriginal entity)
        {
            lock (this.updatedEntitiesLock)
            {
                UpdatedEntityInfo info;
                if (!this.mUpdatedEntities.TryGetValue(entity.Guid, out info))
                {
                    info = new UpdatedEntityInfo(entity);
                    this.mUpdatedEntities[entity.Guid] = info;
                }

                return info;
            }
        }

        public override void FlagForUpdate(ISpatialOriginal localEntity, BooleanArray changedParts)
        {
            if (localEntity == null)
            {
                throw new ArgumentNullException("localEntity");
            }

            if (localEntity.Guid == null || !localEntity.Guid.IsValid)
            {
                return;
            }

            this.GetOrCreateUpdatedEntityInfo(localEntity).ChangedParts.Or(changedParts);
        }

        public override void FlagForUpdate(ISpatialOriginal localEntity, int changedPartIndex)
        {
            if (localEntity == null)
            {
                throw new ArgumentNullException("localEntity");
            }

            if (localEntity.Guid == null || !localEntity.Guid.IsValid)
            {
                return;
            }

            this.GetOrCreateUpdatedEntityInfo(localEntity).ChangedParts[changedPartIndex] = true;
        }

        public override void FlagFullUpdate(ISpatialEntity localEntity)
        {
            throw new NotImplementedException();
        }

        public override Vector3 GetDestination(IDeadReckonable deadReckonable)
        {
            return this.mDeadReckoningHelper.GetDestination(deadReckonable);
        }

        public override void SnapToDestination(IDeadReckonable deadReckonable)
        {
            this.mDeadReckoningHelper.SnapToDestination(deadReckonable);
        }

        protected override void OnProcessNetworkState()
        {
            // We don't actually have a NetworkEventQueue thread when using the HTTP facade
            // but SuspendEvents also sets the thread id that is checked by the AssertIsApplicationThread()
            // method.  This assert method is called in a number of places and we do want the
            // assert to function correctly here, hence the call to SuspendEvents.
            this.eventQueue.AcquirePerformLockForApplicationCode();
            try
            {
                TimeSpan delay = this.timeKeeper.Now - this.mLastDispatchMark;

                if (delay > Parameters.MaximumUpdateRate)
                {
                    Dictionary<BadumnaId, UpdatedEntityInfo> currentUpdatedEntities;
                    lock (this.updatedEntitiesLock)
                    {
                        currentUpdatedEntities = this.mUpdatedEntities;
                        this.mUpdatedEntities = new Dictionary<BadumnaId, UpdatedEntityInfo>();
                    }

                    foreach (UpdatedEntityInfo info in currentUpdatedEntities.Values)
                    {
                        UpdateEntityRequest.MakeRequest(this.SessionManager, info.Entity, info.ChangedParts);
                    }

                    this.mLastDispatchMark = this.timeKeeper.Now;
                }

                lock (this.mChangelists)
                {
                    while (this.mChangelists.Count > 0)
                    {
                        try
                        {
                            this.ProcessChangelist(this.mChangelists.Dequeue());
                        }
                        catch (Exception e)
                        {
                            Logger.TraceException(LogLevel.Error, e, "Got exception while processing changelist");
                        }
                    }
                }

                this.DeadReckoningHelper.PerformDeadReckoning();

                this.applicationEventQueue.Run();
            }
            finally
            {
                this.eventQueue.ReleasePerformLockForApplicationCode();
            }
        }

        public override void SendCustomMessageToRemoteCopies(ISpatialOriginal localEntity, MemoryStream eventData)
        {
            new CustomMessageRequest(this.SessionManager, localEntity.Guid, eventData).MakeRequest();
        }

        public override void SendCustomMessageToOriginal(ISpatialReplica remoteEntity, MemoryStream eventData)
        {
            new CustomMessageRequest(this.SessionManager, remoteEntity.Guid, eventData).MakeRequest();
        }

        protected override IChatSession CreateChatSession()
        {
            return this.mChatService.CreateSession();
        }

        public override void StartController<T>(string controllerUniqueName)
        {
            /* Controllers cannot currently migrate to tunnel servers so we simply ignore controller related
             * calls.  This is in line with the current distributed controller design which doesn't guarantee
             * that controllers will actually be run. */ 
        }

        public override string StartController<T>(string sceneName, string controllerName, ushort max)
        {
            /* Controllers cannot currently migrate to tunnel servers so we simply ignore controller related
             * calls.  This is in line with the current distributed controller design which doesn't guarantee
             * that controllers will actually be run. */
            return sceneName + "^" + controllerName + "_0";
        }

        public override void StopController<T>(string controllerUniqueName)
        {
            /* Controllers cannot currently migrate to tunnel servers so we simply ignore controller related
             * calls.  This is in line with the current distributed controller design which doesn't guarantee
             * that controllers will actually be run. */
        }

        public override void RegisterArbitrationHandler(HandleClientMessage handler, TimeSpan disconnectTimeout, HandleClientDisconnect disconnect)
        {
            throw new NotSupportedException();
        }

        public override IArbitrator GetArbitrator(string name)
        {
            HttpArbitrationClient client;
            if (!this.arbitrationClients.TryGetValue(name, out client))
            {
                client = new HttpArbitrationClient(this.SessionManager, name);
                this.arbitrationClients[name] = client;
            }

            return client;
        }

        public override void SendServerArbitrationEvent(int destinationSessionId, byte[] message)
        {
            throw new NotSupportedException();
        }

        public override long GetUserIdForSession(int sessionId)
        {
            throw new NotSupportedException();
        }

        public override Character GetCharacterForArbitrationSession(int sessionId)
        {
            throw new NotSupportedException();
        }

        public override BadumnaId GetBadumnaIdForArbitrationSession(int sessionId)
        {
            throw new NotSupportedException();
        }

        public override void AnnounceService(ServerType type)
        {
            throw new NotSupportedException();
        }

        private void ProcessChangelist(string changelist)
        {
            using (StringReader reader = new StringReader(changelist))
            {
                this.changelistProcessor.Process(reader);
            }
        }

        /// <inheritdoc />
        protected override List<IPEndPoint> DoGetRecentActiveOpenPeers()
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        protected override void DoAddInitialConnections(List<IPEndPoint> endpoints)
        {
            throw new NotImplementedException();
        }

        public override bool IsFullyConnected
        {
            get { return false; }
        }

        /// <inheritdoc />
        public override StatisticsTracker CreateTracker(string serverAddress, int serverPort, TimeSpan interval, string initialPayload)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public override MatchmakingAsyncResult BeginMatchmaking(HandleMatchmakingResult onCompletion, HandleMatchmakingProgress onProgress, MatchmakingOptions options)
        {
            throw new NotImplementedException();
        }
    }
}
