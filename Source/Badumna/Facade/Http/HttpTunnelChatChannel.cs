﻿//-----------------------------------------------------------------------
// <copyright file="HttpTunnelChatChannel.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2012 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;

using Badumna.Chat;
using Badumna.DataTypes;

namespace Badumna.Facade.Http
{
    /// <summary>
    /// Implements IChatChannel using the Http tunnel.
    /// </summary>
    internal class HttpChatChannel : IChatChannel
    {
        /// <summary>
        /// This channel's ID.
        /// </summary>
        private ChatChannelId id;

        /// <summary>
        /// The (url) path that corresponds to this channel on the tunnel server.
        /// </summary>
        private string path;

        /// <summary>
        /// The session manager.
        /// </summary>
        private SessionManager sessionManager;

        /// <summary>
        /// The message handler.
        /// </summary>
        private ChatMessageHandler messageHandler;

        /// <summary>
        /// The presence handler.
        /// </summary>
        private ChatPresenceHandler presenceHandler;

        /// <summary>
        /// Initializes a new instance of the <see cref="HttpChatChannel"/> class.
        /// </summary>
        /// <param name="id">The channel id.</param>
        /// <param name="messageHandler">The message handler.</param>
        /// <param name="presenceHandler">The presence handler.</param>
        /// <param name="sessionManager">The session manager.</param>
        public HttpChatChannel(ChatChannelId id, ChatMessageHandler messageHandler, ChatPresenceHandler presenceHandler, SessionManager sessionManager)
        {
            this.id = id;
            this.messageHandler = messageHandler;
            this.presenceHandler = presenceHandler;
            this.sessionManager = sessionManager;
            this.path = "/chat/1/channel/" + Uri.EscapeDataString(this.id.ToInvariantIDString());
        }

        /// <inheritdoc/>
        public ChatChannelId Id
        {
            get { return this.id; }
        }

        /// <inheritdoc/>
        public void Unsubscribe()
        {
            this.sessionManager.SessionDelete(this.path);
        }

        /// <inheritdoc/>
        public void SendMessage(string message)
        {
            this.sessionManager.SessionPost(this.path, message);
        }

        /// <summary>
        /// Handles an incoming message.
        /// </summary>
        /// <param name="userId">The sender's user id.</param>
        /// <param name="message">The message.</param>
        internal void HandleMessage(BadumnaId userId, string message)
        {
            if (this.messageHandler != null)
            {
                this.messageHandler(this, userId, message);
            }
        }

        /// <summary>
        /// Handles an incoming presence update.
        /// </summary>
        /// <param name="sender">The sender's user id.</param>
        /// <param name="displayName">The sender's display name.</param>
        /// <param name="status">The sender's new status.</param>
        internal void HandlePresence(BadumnaId sender, string displayName, ChatStatus status)
        {
            if (this.presenceHandler != null)
            {
                this.presenceHandler(this, sender, displayName, status);
            }
        }
    }
}
