﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Badumna.DataTypes;

namespace Badumna.Facade.Http.Streaming
{
    class StreamingRequest : Request
    {
        private string mId;
        public string Id { get { return this.mId; } }

        private string mStreamTag;
        public string StreamTag { get { return this.mStreamTag; } }
        private string mStreamName;
        public string StreamName { get { return this.mStreamName; } }
        private BadumnaId mPeerId;
        public BadumnaId PeerId { get { return this.mPeerId; } }
        private string mUserName;
        public string UserName { get { return this.mUserName; } }
        private bool mIsPull;
        public bool IsPull { get { return this.mIsPull; } }


        public StreamingRequest(SessionManager sessionManager, string streamTag, string streamName, BadumnaId peerId, string userName, bool isPull)
            : base(sessionManager)
        {
            this.mStreamTag = streamTag;
            this.mStreamName = streamName;
            this.mPeerId = peerId;
            this.mUserName = userName;
            this.mIsPull = isPull;
        }

        public StreamingRequest(TextReader reader)
            : base(null)
        {
            this.mIsPull = reader.ReadLine() == "Pull";
            this.mStreamTag = reader.ReadLine();
            this.mStreamName = reader.ReadLine();
            this.mPeerId = BadumnaId.TryParse(reader.ReadLine());
            this.mUserName = reader.ReadLine();

            if (this.mStreamName == null || this.mPeerId == null || this.mUserName == null)
            {
                throw new ArgumentException("Invalid streaming send request");
            }
        }

        public override void MakeRequest()
        {
            this.mId = this.mSessionManager.SessionPost("/streaming",
                (this.mIsPull ? "Pull" : "Push") + "\n" +
                this.mStreamTag + "\n" +
                this.mStreamName + "\n" +
                this.mPeerId.ToString() + "\n" +
                this.mUserName).Trim();
        }
    }
}
