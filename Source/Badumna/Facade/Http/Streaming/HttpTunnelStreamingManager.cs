﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;
using Badumna.Streaming;
using Badumna.Core;
using System.Globalization;
using Badumna.DataTypes;
using Badumna.SpatialEntities;

namespace Badumna.Facade.Http.Streaming
{
    class HttpTunnelStreamingManager : StreamingManager
    {
        private SessionManager sessionManager;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        public HttpTunnelStreamingManager(SessionManager sessionManager, ITime timeKeeper, EntityIDProvider<IReplicableEntity> entityIDProvider)
            : base(entityIDProvider)
        {
            this.sessionManager = sessionManager;
            this.timeKeeper = timeKeeper;
        }

        protected override IStreamController OnBeginSendReliableStream(String taggedName, Stream stream, 
            BadumnaId destination, string userName, AsyncCallback callback, object callbackState)
        {
            string streamTag;
            string streamName;
            StreamingManager.ParseTaggedName(taggedName, out streamTag, out streamName);

            StreamingRequest request = new StreamingRequest(this.sessionManager,
                streamTag, streamName, destination, userName, false);
            request.MakeRequest();
            return new HttpTunnelSendController(request.Id, stream, callback, callbackState, this.sessionManager, this.timeKeeper);
        }

        protected override IStreamController OnBeginRequestReliableStream(string taggedName, Stream destinationStream,
            BadumnaId source, string userName, AsyncCallback callback, object callbackState)
        {
            string streamTag;
            string streamName;
            StreamingManager.ParseTaggedName(taggedName, out streamTag, out streamName);

            StreamingRequest request = new StreamingRequest(this.sessionManager,
                streamTag, streamName, source, userName, true);
            request.MakeRequest();
            return new HttpTunnelReceiveController(request.Id, destinationStream, 0, callback, callbackState, this.sessionManager, this.timeKeeper);  // TODO: Length!
        }

        internal void ReceiveRequest(StreamRequest request, int requestId)
        {
            this.TriggerReceiveRequest(new ReceiveStreamEventArgs(request, null, 0, 
                delegate(StreamRequest req, PeerAddress source, int rate, StreamRequestEventArgs args, AsyncCallback completionCallback)
                {
                    if (!args.IsAccepted)
                    {
                        return null;
                    }

                    Stream destinationStream = ((ReceiveStreamEventArgs)args).DestinationStream;  // TODO: Dodgyy cast...
                    return new HttpTunnelReceiveController(
                        string.Format(CultureInfo.InvariantCulture, "{0}", requestId),
                        destinationStream,
                        request.FileSize,
                        completionCallback,
                        null,
                        this.sessionManager,
                        this.timeKeeper);
                }));
        }

        internal void SendRequest(StreamRequest request, int requestId)
        {
            this.TriggerSendRequest(new SendStreamEventArgs(request, null, 0,
                delegate(StreamRequest req, PeerAddress source, int rate, StreamRequestEventArgs args, AsyncCallback completionCallback)
                {
                    Stream sourceStream = ((SendStreamEventArgs)args).SourceStream;
                    return new HttpTunnelSendController(
                        string.Format(CultureInfo.InvariantCulture, "{0}", requestId),
                        sourceStream,
                        completionCallback,
                        null,
                        this.sessionManager,
                        this.timeKeeper);
                }));
        }
    }
}
