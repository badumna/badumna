﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;
using System.Globalization;

using Badumna.Streaming;
using Badumna.Core;
using Badumna.Utilities;

namespace Badumna.Facade.Http.Streaming
{
    abstract class HttpTunnelStreamController : IStreamController
    {
        private EventHandler informationChangedDelegate;
        public event EventHandler InformationChanged
        {
            add { this.informationChangedDelegate += value; }
            remove { this.informationChangedDelegate -= value; }
        }

        private StreamState mCurrentState = StreamState.InProgress;
        public StreamState CurrentState
        {
            get { return this.mCurrentState; }
            protected set
            {
                this.mCurrentState = value;
                if (this.IsCompleted)
                {
                    this.mWaitHandle.Set();
                    if (this.mCompletionCallback != null)
                    {
                        this.mCompletionCallback(this);
                    }
                }
            }
        }

        public bool WasSuccessful { get { return this.CurrentState == StreamState.Complete; } }

        public bool IsCompleted
        {
            get
            {
                return this.mCurrentState == StreamState.Canceled ||
                       this.mCurrentState == StreamState.Complete ||
                       this.mCurrentState == StreamState.Error ||
                       this.mCurrentState == StreamState.Disconnected;
            }
        }

        public long BytesTotal
        {
            get { return this.mTransferStatus.BytesTotal; }
        }

        public long BytesTransfered
        {
            get { return this.mTransferStatus.BytesTransfered; }
        }

        public double TransferRateKBps
        {
            get { return this.mTransferStatus.TransferRateKBps; }
        }

        public TimeSpan EstimatedTimeRemaining
        {
            get { return this.mTransferStatus.EstimatedTimeRemaining; }
        }

        public bool CompletedSynchronously
        {
            get { return false; }
        }

        private object mAsyncState;
        public object AsyncState { get { return this.mAsyncState; } }

        private ManualResetEvent mWaitHandle = new ManualResetEvent(false);  // TODO: Should be disposed!
        public WaitHandle AsyncWaitHandle { get { return this.mWaitHandle; } }


        protected string mId;

        private AsyncCallback mCompletionCallback;

        protected TransferStatus mTransferStatus;


        public HttpTunnelStreamController(string id, AsyncCallback completionCallback, object state, ITime timeKeeper)
        {
            this.mTransferStatus = new TransferStatus(timeKeeper);
            this.mId = id;
            this.mCompletionCallback = completionCallback;
            this.mAsyncState = state;
            this.mTransferStatus.InformationChanged += delegate { this.OnInformationChanged(); };
        }

        protected virtual void OnInformationChanged()
        {
            EventHandler handler = this.informationChangedDelegate;
            if (handler != null)
            {
                handler(this, null);
            }
        }

        public abstract void Cancel();
    }

    internal class HttpTunnelSendController : HttpTunnelStreamController
    {
        private MonitoredStream mMonitoredStream;

        private SessionManager sessionManager;

        // TODO: May have threading issues?
        public HttpTunnelSendController(
            string id,
            Stream sourceStream,
            AsyncCallback completionCallback,
            object asyncState,
            SessionManager sessionManager,
            ITime timeKeeper)
            : base(id, completionCallback, asyncState, timeKeeper)
        {
            // TODO: Set AllowWriteStreamBuffering false ??
            this.sessionManager = sessionManager;

            this.mTransferStatus.BytesTotal = sourceStream.CanSeek ? sourceStream.Length : 0;

            this.mMonitoredStream = new MonitoredStream(sourceStream);
            this.mMonitoredStream.BytesRead += this.mTransferStatus.AddTransferedAmount;

            Thread thread = new Thread(
                delegate()
                {
                    this.sessionManager.SessionPost("/streaming/" + id, this.mMonitoredStream);

                    if (this.mMonitoredStream.Interrupted)
                    {
                        this.CurrentState = StreamState.Canceled;
                    }
                    else
                    {
                        this.CurrentState = StreamState.Complete;
                    }
                });
            thread.IsBackground = true;
            thread.Start();
        }

        public override void Cancel()
        {
            this.mMonitoredStream.Interrupt();
        }
    }

    internal class HttpTunnelReceiveController : HttpTunnelStreamController
    {
        private MonitoredStream mMonitoredStream;

        private SessionManager sessionManager;

        public HttpTunnelReceiveController(
            string id,
            Stream destinationStream,
            long length,
            AsyncCallback completionCallback,
            object asyncState,
            SessionManager sessionManager,
            ITime timeKeeper)
            : base(id, completionCallback, asyncState, timeKeeper)
        {
            this.sessionManager = sessionManager;

            this.mTransferStatus.BytesTotal = length;

            this.mMonitoredStream = new MonitoredStream(destinationStream);
            this.mMonitoredStream.BytesWritten += this.mTransferStatus.AddTransferedAmount;

            Thread thread = new Thread(
                delegate()
                {
                    using (Stream stream = this.sessionManager.SessionGetToStream("/streaming/" + id))
                    {
                        byte[] buffer = new byte[32768];
                        int bytesRead;
                        while ((bytesRead = stream.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            this.mMonitoredStream.Write(buffer, 0, bytesRead);
                        }
                    }
                    this.mMonitoredStream.Close();

                    if (this.mMonitoredStream.Interrupted)
                    {
                        this.CurrentState = StreamState.Canceled;
                    }
                    else
                    {
                        this.CurrentState = StreamState.Complete;
                    }
                });
            thread.IsBackground = true;
            thread.Start();
        }

        public override void Cancel()
        {
            this.mMonitoredStream.Interrupt();
        }
    }
}
