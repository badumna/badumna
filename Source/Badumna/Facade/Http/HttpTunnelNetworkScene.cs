﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.IO;
using System.Diagnostics;
using Badumna.Replication;
using Badumna.SpatialEntities;
using Badumna.Core;
using Badumna.Utilities;

namespace Badumna.Facade.Http
{
    class HttpTunnelNetworkScene : NetworkScene
    {
        private HttpTunnelNetworkFacade mFacade;


        public HttpTunnelNetworkScene(
            HttpTunnelNetworkFacade facade,
            String name,
            CreateSpatialReplica createEntityDelegate,
            RemoveSpatialReplica removeEntityDelegate,
            NetworkEventQueue eventQueue,
            QueueInvokable queueApplicationEvent)
            : base(
            new QualifiedName("", name),
            createEntityDelegate,
            removeEntityDelegate,
            null,
            eventQueue,
            queueApplicationEvent,
            false)
        {
            // Note: mini scene is not supported in HttpTunnel.
            this.mFacade = facade;
        }

        protected override void RegisterEntityImplementation(ISpatialOriginal entity, uint entityType)
        {
            ISpatialOriginal oldEntity = null;
            if (entity.Guid != null &&
                this.mFacade.Entities.TryGetValue(entity.Guid, out oldEntity) &&
                oldEntity != entity)
            {
                throw new InvalidOperationException("Attempt to register duplicate BadumnaId");
            }

            if (oldEntity == null)
            {
                // Create the entity on the proxy

                // TODO: This will cause calls to Serialize.  Document that this
                //       can occur during RegisterEntity (maybe document that the tunnel might call these
                //       methods for any API that passes IOriginalEntity in?).
                entity.Guid = RegisterEntityRequest.MakeRequest(this.mFacade.SessionManager, entityType, entity);

                this.mFacade.Entities[entity.Guid] = entity;

                List<NetworkScene> sceneList = new List<NetworkScene>();
                sceneList.Add(this);
                this.mFacade.EntityScenes[entity.Guid] = sceneList;
            }
            else
            {
                List<NetworkScene> sceneList = this.mFacade.EntityScenes[entity.Guid];
                if (!sceneList.Contains(this))
                {
                    sceneList.Add(this);
                }
            }


            // Add the entity to the scene

            this.mFacade.SessionManager.SessionPost(
                "scenes/" + Uri.EscapeDataString(this.Name), entity.Guid.ToString());
        }

        protected override void UnregisterEntityImplementation(ISpatialOriginal entity)
        {
            this.mFacade.SessionManager.SessionDelete("scenes/" + Uri.EscapeDataString(this.Name) + "/" + Uri.EscapeDataString(entity.Guid.ToString()));

            List<NetworkScene> sceneList;
            if (this.mFacade.EntityScenes.TryGetValue(entity.Guid, out sceneList))
            {
                sceneList.Remove(this);
                if (sceneList.Count == 0)
                {
                    this.mFacade.EntityScenes.Remove(entity.Guid);
                    this.mFacade.SessionManager.SessionDelete("entities/" + Uri.EscapeDataString(entity.Guid.ToString()));
                    this.mFacade.Entities.Remove(entity.Guid);
                }
            }
        }

        protected override void LeaveImplimentation()
        {
            this.mFacade.SessionManager.SessionDelete("scenes/" + Uri.EscapeDataString(this.Name));
        }
    }
}
