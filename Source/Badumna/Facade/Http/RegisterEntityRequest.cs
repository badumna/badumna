﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Badumna.Replication;
using Badumna.DataTypes;
using Badumna.SpatialEntities;
using Badumna.Utilities;
using Badumna.Utilities.LitJson;
using Badumna.Core;

namespace Badumna.Facade.Http
{
    internal static class RegisterEntityRequest
    {
        public delegate void RegisterEntity(uint entityType, JsonData update);

        public static void FromJson(JsonData json, RegisterEntity registerEntity)
        {
            registerEntity((uint)json["type"], json["update"]);
        }

        public static BadumnaId MakeRequest(SessionManager sessionManager, uint entityType, ISpatialOriginal entity)
        {
            string guid = sessionManager.SessionPost("entities/",
                new
                {
                    type = entityType,
                    update = UpdateEntityRequest.UpdateToJson(entity, new BooleanArray(true))
                });

            BadumnaId badumnaId = BadumnaId.TryParse(guid);
            if (badumnaId == null)
            {
                throw new TunnelRequestException(string.Format("Register entity request returned invalid guid ({0})", guid));
            }

            return badumnaId;
        }
    }
}
