﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;

using Badumna.Core;
using Badumna.Security;
using Badumna.Utilities.LitJson;
using Badumna.Transport;

namespace Badumna.Facade.Http
{
    class SessionManager
    {
        private Uri mTunnelBaseUri;
        private string mSessionId;
        private PersistentHttpConnection mServerConnection;


        public SessionManager(Uri tunnelBaseUri, IIdentityProvider identityProvider)
        {
            this.mTunnelBaseUri = tunnelBaseUri;

            this.mServerConnection = new PersistentHttpConnection(tunnelBaseUri);

            try
            {
                CreateSessionRequest createSessionRequest = new CreateSessionRequest(this, identityProvider);
                createSessionRequest.MakeRequest();
                this.mSessionId = createSessionRequest.SessionId;
            }
            catch (Exception)
            {
                this.mServerConnection.Close();
                throw;
            }
        }

        public void Close()
        {
            this.mServerConnection.Close();
            this.mServerConnection = null;
        }

        private string ToSessionPath(string path)
        {
            if (String.IsNullOrEmpty(this.mSessionId))
            {
                throw new InvalidOperationException("Must login first");
            }

            if (!path.StartsWith("/"))
            {
                path = "/" + path;
            }
            return "/sessions/" + this.mSessionId + path;
        }

        public string MakeRequest(string method, bool inSession, string path, string body)
        {
            return this.MakeRequest(method, inSession, path, body, Parameters.DefaultHttpRequestTimeoutMs);
        }

        public string MakeRequest(string method, bool inSession, string path, string body, int timeoutMs)
        {
            using (SessionRequest request = this.CreateRequest(method, inSession, path, body, timeoutMs))
            using (Stream response = request.MakeRequest())
            {
                return SessionManager.ToString(response);
            }
        }

        private Stream MakeRequestAndReturnStream(string method, bool inSession, string path, Stream body, int timeoutMs)
        {
            using (SessionRequest request = this.CreateRequest(method, inSession, path, body, timeoutMs))
            {
                return request.MakeRequest();
            }
        }

        public SessionRequest CreateRequest(string method, bool inSession, string path, int timeoutMs)
        {
            return this.CreateRequest(method, inSession, path, (Stream)null, timeoutMs);
        }

        public SessionRequest CreateRequest(string method, bool inSession, string path, string body, int timeoutMs)
        {
            MemoryStream bodyStream = body != null ? new MemoryStream(Encoding.UTF8.GetBytes(body)) : null;
            return this.CreateRequest(method, inSession, path, bodyStream, timeoutMs);
        }

        public SessionRequest CreateRequest(string method, bool inSession, string path, Stream body, int timeoutMs)
        {
            return this.CreateRequestOnConnection(this.mServerConnection, method, inSession, path, body, timeoutMs);
        }

        public SessionRequest CreateRequestOnConnection(PersistentHttpConnection connection, string method, bool inSession, string path, Stream body, int timeoutMs)
        {
            if (inSession)
            {
                path = this.ToSessionPath(path);
            }

            Uri uri = new Uri(this.mTunnelBaseUri, path);

            return new SessionRequest(connection, uri, method, body, timeoutMs);
        }


        public string SessionPost(string path, object body)
        {
            if (body is Stream)
            {
                return SessionManager.ToString(
                    this.MakeRequestAndReturnStream("POST", true, path, (Stream)body, Parameters.DefaultHttpRequestTimeoutMs));
            }

            string bodyText = (body as string) ?? JsonMapper.ToJson(body);
            return this.MakeRequest("POST", true, path, bodyText);
        }

        public string SessionGet(string path)
        {
            return this.MakeRequest("GET", true, path, null);
        }

        /// <summary>
        /// Stream must be closed by caller.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public Stream SessionGetToStream(string path)
        {
            return this.MakeRequestAndReturnStream("GET", true, path, null, Parameters.DefaultHttpRequestTimeoutMs);
        }

        public string SessionDelete(string path)
        {
            return this.MakeRequest("DELETE", true, path, null);
        }

        private static string ToString(Stream stream)
        {
            using (StreamReader reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }
    }
}
