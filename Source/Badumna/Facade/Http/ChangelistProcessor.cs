﻿//-----------------------------------------------------------------------
// <copyright file="ChangelistProcessor.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

using Badumna.Utilities;
using Badumna.Utilities.LitJson;

namespace Badumna.Facade.Http
{
    /// <summary>
    /// Dispatches incoming changelists to a matching registered handler.
    /// </summary>
    internal class ChangelistProcessor
    {
        /// <summary>
        /// Collection of changelist handlers.
        /// </summary>
        private Dictionary<string, Action<JsonData>> handlers = new Dictionary<string, Action<JsonData>>();

        /// <summary>
        /// Registers a changelist handler.
        /// </summary>
        /// <param name="id">A unique string identifying the change type.</param>
        /// <param name="handler">The delegate to be called when a matching change arrives.  The JsonData
        /// parameter will contain the body of the change.</param>
        public void Register(string id, Action<JsonData> handler)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentException("id");
            }

            if (handler == null)
            {
                throw new ArgumentNullException("handler");
            }

            this.handlers[id] = handler;
        }

        /// <summary>
        /// Processes a changelist.
        /// </summary>
        /// <param name="reader">A reader containing the serialised changelist.</param>
        public void Process(TextReader reader)
        {
            JsonData changelist = JsonMapper.ToObject(reader);
            if (!changelist.IsArray)
            {
                throw new ArgumentException("Expected a JSON array", "reader");
            }

            foreach (JsonData change in changelist)
            {
                // Want to keep processing other changes regardless of exceptions thrown when attempting to handle this change
                try
                {
                    this.handlers[(string)change["_cid"]](change);
                }
                catch (Exception e)
                {
                    Logger.TraceException(LogLevel.Warning, e, "Change handler threw an exception");
                }
            }
        }
    }
}
