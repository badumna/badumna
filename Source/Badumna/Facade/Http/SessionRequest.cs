﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using Badumna.Core;
using Badumna.Transport;

namespace Badumna.Facade.Http
{
    class SessionRequest : IDisposable
    {
        private PersistentHttpConnection mConnection;
        private SimpleHttpWebRequest mRequest;
        private Stream mBody;


        /// <summary>
        /// Creates a new SessionRequest.
        /// </summary>
        /// <param name="connection">The connection to make the request on.</param>
        /// <param name="uri">The URI for the request.</param>
        /// <param name="method">The method for the request.</param>
        /// <param name="body">The body.  This becomes owned by the SessionRequest and will be closed when the SessionRequest is disposed.</param>
        /// <param name="timeoutMs">The timeout for the request.</param>
        public SessionRequest(PersistentHttpConnection connection, Uri uri, string method, Stream body, int timeoutMs)
        {
            this.mConnection = connection;
            this.mRequest = SimpleHttpWebRequest.Create(uri.ToString());
            this.mRequest.Method = method;
            this.mRequest.Timeout = timeoutMs;
            this.mRequest.KeepAlive = true;
            this.mRequest.AdvertiseAsHttp11 = true;
            this.mBody = body;
        }

        public void Dispose()
        {
            if (this.mBody != null)
            {
                this.mBody.Close();
            }

            this.mRequest = null;
        }

        /// <summary>
        /// Returned stream must be closed by caller.  Returned stream is valid
        /// even after SessionRequest has been disposed.
        /// </summary>
        public Stream MakeRequest()
        {
            if (this.mRequest == null)
            {
                throw new InvalidOperationException();
            }

            MemoryStream backupStream = null;
            long originalPosition = 0;
            if (this.mBody != null)
            {
                if (this.mBody.CanSeek)
                {
                    originalPosition = this.mBody.Position;
                }
                else
                {
                    backupStream = new MemoryStream();
                }
            }

            bool canRetry = true;  // Just one retry is allowed.  'body' is temporarily copied to backupStream if required

            while (true)
            {
                try
                {
                    int contentLength = 0;

                    if (this.mBody != null)
                    {
                        using (Stream requestStream = this.mRequest.GetRequestStream())
                        {
                            byte[] buffer = new byte[32768];
                            int bytesRead;
                            while ((bytesRead = this.mBody.Read(buffer, 0, buffer.Length)) > 0)
                            {
                                requestStream.Write(buffer, 0, bytesRead);
                                contentLength += bytesRead;

                                if (backupStream != null)
                                {
                                    backupStream.Write(buffer, 0, bytesRead);
                                }
                            }
                        }
                    }

                    this.mRequest.ContentLength = contentLength;
                    SimpleWebResponse response = this.mConnection.GetResponse(this.mRequest);
                    return response.GetResponseStream();
                }
                catch (WebException webException)
                {
                    // For some reason we get a secure channel failure if a https request has been made to
                    // the same server on a different port prior to this request.  The second request always
                    // seems to work though, and so long as we don't make other requests to the different port
                    // later requests will work first time.  This was seen in FlatChat, because FlatChat
                    // does a request to https://secure.badumna.com/ before the tunnel requests which are made
                    // to https://secure.badumna.com:21247/
                    if (webException.Status == WebExceptionStatus.SecureChannelFailure && canRetry)
                    {
                        if (backupStream != null)
                        {
                            backupStream.Position = 0;
                            this.mBody = backupStream;
                            backupStream = null;
                        }
                        else
                        {
                            this.mBody.Position = originalPosition;
                        }
                        canRetry = false;
                        continue;
                    }

                    throw new TunnelRequestException("Request failed", webException);
                }
                finally
                {
                    this.mRequest = null;
                }
            }
        }

        public void Abort()
        {
            SimpleHttpWebRequest request = this.mRequest;
            if (request != null)
            {
                request.Abort();
            }
        }
    }
}
