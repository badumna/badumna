﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

using Badumna.Core;
using Badumna.DistributedHashTable;
using Badumna.Transport;
using Badumna.Replication;
using Badumna.Overload;
using Badumna.Arbitration;
using Badumna.Utilities;

namespace Badumna
{
    /// <summary>
    /// Provides information about the state of Badumna's network connectivity.
    /// </summary>
    /// <remarks>
    /// This class is intended to provide useful information for users / network operators
    /// to diagnose issues with connectivity to a Badumna network.  An instance of the class
    /// should be retrieved via a call to NetworkFacade.GetNetworkStatus().  The values in
    /// this instance are valid at the time it is returned from GetNetworkStatus(), but will
    /// not be automatically updated.  To refresh the status, another call to GetNetworkStatus()
    /// should be made.
    /// </remarks>
    public class NetworkStatus
    {
        /// <summary>
        /// The network facade.
        /// </summary>
        private readonly INetworkFacade networkFacade;

        /// <summary>
        /// Public address.
        /// </summary>
        private string mPublicAddress;

        /// <summary>
        /// Private address.
        /// </summary>
        private string mPrivateAddress;

        /// <summary>
        /// The number of established connections. 
        /// </summary>
        private int mActiveConnectionCount;

        /// <summary>
        /// The number of connections being initialized.
        /// </summary>
        private int mInitializingConnectionCount;

        /// <summary>
        /// The discovery method.
        /// </summary>
        private string mDiscoveryMethod;

        /// <summary>
        /// Whether port forwarding feature is enabled.
        /// </summary>
        private bool mPortForwardingEnabled;

        /// <summary>
        /// Whether the UPnP port forwarding succeeded. 
        /// </summary>
        private bool mPortForwardingSucceeded;

        /// <summary>
        /// The last discovery time.
        /// </summary>
        private DateTime mLastDiscoveryTime;

        /// <summary>
        /// The status of the last discovery.
        /// </summary>
        private string mLastDiscoveryStatus;

        /// <summary>
        /// The number of results for the last discovery.
        /// </summary>
        private int mLastDiscoveryResultCount;

        /// <summary>
        /// Current scenes that the local peer has joined. 
        /// </summary>
        private List<string> mCurrentScenes;

        /// <summary>
        /// The number of registered local entities.
        /// </summary>
        private int mLocalObjectCount;

        /// <summary>
        /// The number of replica objects. 
        /// </summary>
        private int mRemoteObjectCount;

        /// <summary>
        /// The number of bytes sent per second.
        /// </summary>
        private long mTotalBytesSentPerSecond;

        /// <summary>
        /// The number of bytes received per second.
        /// </summary>
        private long mTotalBytesReceivedPerSecond;

        /// <summary>
        /// Whether fully connected.
        /// </summary>
        private bool mIsFullyConnected;

        /// <summary>
        /// The overload status.
        /// </summary>
        private OverloadStatus overloadStatus;

        /// <summary>
        /// The arbitration server status.
        /// </summary>
        private ArbitrationStatus arbitrationStatus;

        /// <summary>
        /// Gets the public address.
        /// </summary>
        /// <value>The public address.</value>
        public string PublicAddress 
        { 
            get { return this.mPublicAddress; } 
        }

        /// <summary>
        /// Gets the private address.
        /// </summary>
        /// <value>The private address.</value>
        public string PrivateAddress 
        { 
            get { return this.mPrivateAddress; } 
        }

        /// <summary>
        /// Gets the number of established connections count.
        /// </summary>
        /// <value>The active connection count.</value>
        public int ActiveConnectionCount 
        { 
            get { return this.mActiveConnectionCount; } 
        }

        /// <summary>
        /// Gets the initializing connection count.
        /// </summary>
        /// <value>The initializing connection count.</value>
        public int InitializingConnectionCount 
        { 
            get { return this.mInitializingConnectionCount; } 
        }

        /// <summary>
        /// Gets the discovery method.
        /// </summary>
        /// <value>The discovery method.</value>
        public string DiscoveryMethod 
        { 
            get { return this.mDiscoveryMethod; } 
        }

        /// <summary>
        /// Gets a value indicating whether port forwarding is enabled.
        /// </summary>
        /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
        public bool PortForwardingEnabled
        {
            get { return this.mPortForwardingEnabled; }
            private set { this.mPortForwardingEnabled = value; }
        }

        /// <summary>
        /// Gets a value indicating whether port forwarding succeeded.
        /// </summary>
        /// <value><c>true</c> if succeeded; otherwise, <c>false</c>.</value>
        public bool PortForwardingSucceeded
        {
            get { return this.mPortForwardingSucceeded; }
            private set { this.mPortForwardingSucceeded = value; }
        }

        /// <summary>
        /// Gets the last discovery time.
        /// </summary>
        /// <value>The last discovery time.</value>
        public DateTime LastDiscoveryTime 
        { 
            get { return this.mLastDiscoveryTime; } 
        }

        /// <summary>
        /// Gets the last discovery status.
        /// </summary>
        /// <value>The last discovery status.</value>
        public string LastDiscoveryStatus 
        { 
            get { return this.mLastDiscoveryStatus; } 
        }

        /// <summary>
        /// Gets the last discovery result count.
        /// </summary>
        /// <value>The last discovery result count.</value>
        public int LastDiscoveryResultCount 
        { 
            get { return this.mLastDiscoveryResultCount; } 
        }

        /// <summary>
        /// Gets the names of the current scenes the local peer has joined.
        /// </summary>
        /// <value>A list of the scene names.</value>
        public List<string> CurrentScenes 
        { 
            get { return this.mCurrentScenes; } 
        }

        /// <summary>
        /// Gets the number of registered local entities.
        /// </summary>
        /// <value>The local entity count.</value>
        public int LocalObjectCount 
        { 
            get { return this.mLocalObjectCount; } 
        }

        /// <summary>
        /// Gets the number of replicas.
        /// </summary>
        /// <value>The replica count.</value>
        public int RemoteObjectCount 
        { 
            get { return this.mRemoteObjectCount; } 
        }

        /// <summary>
        /// Gets the total bytes sent per second.
        /// </summary>
        /// <value>The total bytes sent per second.</value>
        public long TotalBytesSentPerSecond 
        { 
            get { return this.mTotalBytesSentPerSecond; } 
        }

        /// <summary>
        /// Gets the total bytes received per second.
        /// </summary>
        /// <value>The total bytes received per second.</value>
        public long TotalBytesReceivedPerSecond 
        { 
            get { return this.mTotalBytesReceivedPerSecond; } 
        }

        internal NetworkStatus(
            INetworkFacade networkFacade,
            bool includeOverloadStatus,
            bool includeArbitrationStatus,
            bool portForwardingEnabled,
            IPeerFinder peerFinder,
            INetworkAddressProvider addressProvider)
        {
            if (networkFacade == null)
            {
                throw new ArgumentNullException("networkFacade");
            }

            this.networkFacade = networkFacade;
            this.mPublicAddress = addressProvider.PublicAddress.ToString();
            this.mPrivateAddress = CollectionUtils.Join(addressProvider.PrivateAddresses, ", ");

            this.mIsFullyConnected = networkFacade.IsFullyConnected;

            ProtocolStack stack = null;
            MainNetworkFacade mainNetworkFacade = this.networkFacade as MainNetworkFacade;
            if (mainNetworkFacade != null)
            {
                stack = mainNetworkFacade.Stack;
            }

            this.PortForwardingEnabled = portForwardingEnabled;
            this.PortForwardingSucceeded = false;

            if (stack != null)
            {
                UdpTransport udpTransport = stack.TransportLayer as UdpTransport;
                this.PortForwardingSucceeded = udpTransport != null && udpTransport.UPnPSucceeded;

                if (stack.ProtocolRoot != null)
                {
                    this.mActiveConnectionCount = stack.ConnectionTable.GetActiveConnectionCount();
                    this.mInitializingConnectionCount = stack.ConnectionTable.GetInitializingConnectionCount();
                }

                EntityManager entityManager = stack.EntityManager;
                if (entityManager != null)
                {
                    this.mLocalObjectCount = entityManager.OriginalEntityCount;
                }

                if (stack.SpatialEntityManager != null)
                {
                    this.mCurrentScenes = stack.SpatialEntityManager.GetCurrentSceneNames();
                    this.mRemoteObjectCount = stack.SpatialEntityManager.ReplicaCount;
                }

                this.mTotalBytesSentPerSecond = stack.ConnectionTable.TotalBytesSentPerSecond;
                this.mTotalBytesReceivedPerSecond = stack.ConnectionTable.TotalBytesReceivedPerSecond;

                if (includeOverloadStatus)
                {
                    this.overloadStatus = stack.ForwardingManager.GetOverloadServerStatus();
                }

                if (includeArbitrationStatus)
                {
                    this.arbitrationStatus = stack.ArbitrationManager.GetServerStatus();
                }
            }

            if (peerFinder != null)
            {
                this.mDiscoveryMethod = peerFinder.Description;
                this.mLastDiscoveryTime = peerFinder.LastQueryTime;
                this.mLastDiscoveryStatus = peerFinder.LastQueryStatus;
                this.mLastDiscoveryResultCount = peerFinder.LastResultCount;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NetworkStatus"/> class.
        /// </summary>
        /// <param name="other">The other network status.</param>
        public NetworkStatus(NetworkStatus other)
        {
            this.mPublicAddress = other.mPublicAddress;
            this.mPrivateAddress = other.mPrivateAddress;
            this.mActiveConnectionCount = other.mActiveConnectionCount;
            this.mInitializingConnectionCount = other.mInitializingConnectionCount;
            this.mDiscoveryMethod = other.mDiscoveryMethod;
            this.mPortForwardingEnabled = other.mPortForwardingEnabled;
            this.mPortForwardingSucceeded = other.mPortForwardingSucceeded;

            this.mLastDiscoveryTime = other.mLastDiscoveryTime;
            this.mLastDiscoveryStatus = other.mLastDiscoveryStatus;
            this.mLastDiscoveryResultCount = other.mLastDiscoveryResultCount;

            this.mLocalObjectCount = other.mLocalObjectCount;
            this.mRemoteObjectCount = other.mRemoteObjectCount;
            this.mTotalBytesSentPerSecond = other.mTotalBytesSentPerSecond;
            this.mTotalBytesReceivedPerSecond = other.mTotalBytesReceivedPerSecond;

            if(other.overloadStatus != null)
            {
                this.overloadStatus = new OverloadStatus(other.overloadStatus);
            }

            if (other.arbitrationStatus != null)
            {
                this.arbitrationStatus = new ArbitrationStatus(other.arbitrationStatus);
            }

            this.mCurrentScenes = new List<string>();
            foreach (string scene in other.mCurrentScenes)
            {
                this.mCurrentScenes.Add(scene);
            }
        }

        /// TODO : Give a link to the schema.
        /// <summary>
        /// Creates an XML document containing the status information.
        /// </summary>
        /// <returns>The XML document.</returns>
        public XmlDocument ToXml()
        {
            XmlDocument document = new XmlDocument();

            XmlNode root = document.AppendChild(document.CreateElement("Status"));//, "http://www.badumna.com/NetworkStatus"));
            XmlNode connectivityNode = root.AppendChild(document.CreateElement("Connectivity"));
            XmlNode discoveryNode = root.AppendChild(document.CreateElement("Discovery"));
            XmlNode scenesNode = root.AppendChild(document.CreateElement("Scenes"));
            XmlNode transferNode = root.AppendChild(document.CreateElement("TransferRate"));

            connectivityNode.AppendChild(document.CreateElement("PublicAddress")).AppendChild(document.CreateTextNode(this.PublicAddress));
            connectivityNode.AppendChild(document.CreateElement("PrivateAddress")).AppendChild(document.CreateTextNode(this.PrivateAddress));
            connectivityNode.AppendChild(document.CreateElement("IsFullyConnected")).AppendChild(document.CreateTextNode(this.mIsFullyConnected.ToString()));
            connectivityNode.AppendChild(document.CreateElement("ActiveConnections")).AppendChild(document.CreateTextNode(this.ActiveConnectionCount.ToString()));
            connectivityNode.AppendChild(document.CreateElement("InitializingConnections")).AppendChild(document.CreateTextNode(this.InitializingConnectionCount.ToString()));
            connectivityNode.AppendChild(document.CreateElement("PortForwardingEnabled")).AppendChild(document.CreateTextNode(this.PortForwardingEnabled.ToString()));
            connectivityNode.AppendChild(document.CreateElement("PortForwardingSucceeded")).AppendChild(document.CreateTextNode(this.PortForwardingSucceeded.ToString()));

            discoveryNode.AppendChild(document.CreateElement("Method")).AppendChild(document.CreateTextNode(this.DiscoveryMethod));
            discoveryNode.AppendChild(document.CreateElement("Time")).AppendChild(document.CreateTextNode(this.LastDiscoveryTime.ToString("r")));
            discoveryNode.AppendChild(document.CreateElement("Status")).AppendChild(document.CreateTextNode(this.LastDiscoveryStatus));
            discoveryNode.AppendChild(document.CreateElement("ResultCount")).AppendChild(document.CreateTextNode(this.LastDiscoveryResultCount.ToString()));

            scenesNode.AppendChild(document.CreateElement("TotalLocalEntities")).AppendChild(document.CreateTextNode(this.LocalObjectCount.ToString()));
            scenesNode.AppendChild(document.CreateElement("TotalRemoteEntities")).AppendChild(document.CreateTextNode(this.RemoteObjectCount.ToString()));
            
            transferNode.AppendChild(document.CreateElement("TotalBytesSentPerSecond")).AppendChild(document.CreateTextNode(this.mTotalBytesSentPerSecond.ToString()));
            transferNode.AppendChild(document.CreateElement("TotalBytesReceivedPerSecond")).AppendChild(document.CreateTextNode(this.mTotalBytesReceivedPerSecond.ToString()));

            foreach (String sceneName in this.CurrentScenes)
            {
                XmlNode sceneNode = scenesNode.AppendChild(document.CreateElement("Scene"));

                sceneNode.Attributes.Append(document.CreateAttribute("Name")).Value = sceneName;
            }

            if (this.overloadStatus != null)
            {
                XmlNode overloadStatus = root.AppendChild(document.CreateElement("Overload"));
                overloadStatus.AppendChild(document.CreateElement("NumberOfClients")).AppendChild(document.CreateTextNode(this.overloadStatus.NumberOfClients.ToString()));
                overloadStatus.AppendChild(document.CreateElement("NumberOfReceivingPeers")).AppendChild(document.CreateTextNode(this.overloadStatus.NumberofReceivingPeers.ToString()));
                overloadStatus.AppendChild(document.CreateElement("IncomingUpdates")).AppendChild(document.CreateTextNode(this.overloadStatus.NumberOfIncomingUpdates.ToString()));
                overloadStatus.AppendChild(document.CreateElement("OutgoingUpdates")).AppendChild(document.CreateTextNode(this.overloadStatus.NumberOfOutgoingUpdates.ToString()));
                overloadStatus.AppendChild(document.CreateElement("IntervalSeconds")).AppendChild(document.CreateTextNode(this.overloadStatus.IntervalSeconds.ToString()));
            }

            if (this.arbitrationStatus != null)
            {
                XmlNode arbitration = root.AppendChild(document.CreateElement("Arbitration"));

                arbitration.AppendChild(document.CreateElement("NumberOfClients")).AppendChild(document.CreateTextNode(this.arbitrationStatus.NumberOfClients.ToString()));
                arbitration.AppendChild(document.CreateElement("NumberOfReceivedRequests")).AppendChild(document.CreateTextNode(this.arbitrationStatus.NumberOfReceivedRequests.ToString()));
                arbitration.AppendChild(document.CreateElement("NumberOfHandledRequests")).AppendChild(document.CreateTextNode(this.arbitrationStatus.NumberOfHandledRequests.ToString()));
                arbitration.AppendChild(document.CreateElement("NumberOfSentMessaages")).AppendChild(document.CreateTextNode(this.arbitrationStatus.NumberOfSentMessages.ToString()));
                arbitration.AppendChild(document.CreateElement("IntervalSeconds")).AppendChild(document.CreateTextNode(this.arbitrationStatus.Interval.TotalSeconds.ToString()));
            }

            return document;
        }

        /// <summary>
        /// Returns a detailed human readable description of the network status.
        /// </summary>
        /// <returns>A string containing the network status description.</returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("Public address: ").AppendLine(this.PublicAddress);
            sb.Append("Private address: ").AppendLine(this.PrivateAddress);
            sb.Append("Is fully connected: ").AppendLine(this.mIsFullyConnected.ToString());
            sb.Append("Active connections: ").AppendLine(this.ActiveConnectionCount.ToString());
            sb.Append("Initializing connections: ").AppendLine(this.InitializingConnectionCount.ToString());
            sb.Append("Port Forwarding Enabled: ").AppendLine(this.PortForwardingEnabled.ToString());
            sb.Append("Port Forwarding Succeeded: ").AppendLine(this.PortForwardingSucceeded.ToString());

            sb.AppendLine();
            sb.Append("Discovery method: ").AppendLine(this.DiscoveryMethod);
            sb.Append("Last discovery time: ").AppendLine(this.LastDiscoveryTime.ToString("r"));
            sb.Append("Last discovery status: ").AppendLine(this.LastDiscoveryStatus);
            sb.Append("Last discovery result count: ").AppendLine(this.LastDiscoveryResultCount.ToString());

            sb.AppendLine();
            sb.Append("Current scenes: ").AppendLine(string.Join(", ", this.CurrentScenes.ToArray()));
            sb.Append("Local object count: ").AppendLine(this.LocalObjectCount.ToString());
            sb.Append("Remote object count: ").AppendLine(this.RemoteObjectCount.ToString());
            sb.Append("Total bytes sent per second: ").AppendLine(this.mTotalBytesSentPerSecond.ToString());
            sb.Append("Total bytes received per second: ").AppendLine(this.mTotalBytesReceivedPerSecond.ToString());

            if (this.overloadStatus != null)
            {
                sb.AppendLine();
                sb.Append("Overload clients: ").AppendLine(this.overloadStatus.NumberOfClients.ToString());
                sb.Append("Overload receiving peers: ").AppendLine(this.overloadStatus.NumberofReceivingPeers.ToString());
                sb.Append("Overload incoming updates: ").AppendLine(this.overloadStatus.NumberOfIncomingUpdates.ToString());
                sb.Append("Overload outgoing updates: ").AppendLine(this.overloadStatus.NumberOfOutgoingUpdates.ToString());
                sb.Append("Stats refresh interval (sec): ").AppendLine(this.overloadStatus.IntervalSeconds.ToString());
            }

            if (this.arbitrationStatus != null)
            {
                sb.AppendLine();
                sb.Append("Arbitration clients: ").AppendLine(this.arbitrationStatus.NumberOfClients.ToString());
                sb.Append("Arbitration received requests: ").AppendLine(this.arbitrationStatus.NumberOfReceivedRequests.ToString());
                sb.Append("Arbitration handled requests: ").AppendLine(this.arbitrationStatus.NumberOfHandledRequests.ToString());
                sb.Append("Arbitration sent messages: ").AppendLine(this.arbitrationStatus.NumberOfSentMessages.ToString());
                sb.Append("Stats refresh interval (sec): ").AppendLine(this.arbitrationStatus.Interval.TotalSeconds.ToString());
            }

            return sb.ToString();
        }
    }

    /// <summary>
    /// The status of a DHT Tier.
    /// </summary>
    public class DhtStatus
    {
        /// <summary>
        /// The name of the tier.
        /// </summary>
        private string mTierName;

        /// <summary>
        /// The key of the local peer.
        /// </summary>
        private string mLocalDhtKey;

        /// <summary>
        /// Whether the tier has been initialized.
        /// </summary>
        private bool mIsInitialized;

        /// <summary>
        /// Whether the tier is routing messages.
        /// </summary>
        private bool mIsRouting;

        /// <summary>
        /// The number of unique neighbors. 
        /// </summary>
        private int mUniqueNeighbourCount;

        /// <summary>
        /// Gets the name of the tier.
        /// </summary>
        /// <value>The name of the tier.</value>
        public string TierName 
        { 
            get { return this.mTierName; } 
        }

        /// <summary>
        /// Gets a value indicating whether this tier has been initialized.
        /// </summary>
        /// <value>
        /// <c>true</c> if this tier has been initialized; otherwise, <c>false</c>.
        /// </value>
        public bool IsInitialized 
        { 
            get { return this.mIsInitialized; } 
        }

        /// <summary>
        /// Gets a value indicating whether this tier is routing messages. The peer is 
        /// considered as fully connected to the P2P network when the Open tier is routing
        /// messages.
        /// </summary>
        /// <value>
        /// <c>true</c> if this tier is routing; otherwise, <c>false</c>.
        /// </value>
        public bool IsRouting 
        { 
            get { return this.mIsRouting; } 
        }

        /// <summary>
        /// Gets the unique neighbour count.
        /// </summary>
        /// <value>The unique neighbour count.</value>
        public int UniqueNeighbourCount 
        { 
            get { return this.mUniqueNeighbourCount; } 
        }

        /// <summary>
        /// Gets the local DHT key.
        /// </summary>
        /// <value>The local DHT key.</value>
        internal string LocalDhtKey 
        { 
            get { return this.mLocalDhtKey; } 
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DhtStatus"/> class.
        /// </summary>
        /// <param name="tier">The tier.</param>
        /// <param name="facade">The facade.</param>
        internal DhtStatus(NamedTier tier, DhtFacade facade)
        {
            switch (tier)
            {
                case NamedTier.AllInclusive:
                    this.mTierName = "All";
                    break;

                case NamedTier.OpenAddresses:
                    this.mTierName = "Open";
                    break;

                case NamedTier.Trusted:
                    this.mTierName = "Trusted";
                    break;
            }

            this.mIsInitialized = facade.IsInitialized;
            this.mIsRouting = facade.IsRouting;
            this.mUniqueNeighbourCount = facade.UniqueNeighbourCount;
            if (this.IsInitialized)  // DhtFacade throws if LocalHashKey is accessed before initialization
            {
                this.mLocalDhtKey = facade.LocalHashKey.ToString();
            }
            else
            {
                this.mLocalDhtKey = "uninitialized";
            }
        }

        internal void ToXml(XmlNode parent, XmlDocument document)
        {
            XmlNode dhtNode = parent.AppendChild(document.CreateElement("DHT"));

            dhtNode.Attributes.Append(document.CreateAttribute("Tier")).Value = this.TierName;
            dhtNode.Attributes.Append(document.CreateAttribute("Initialized")).Value = this.IsInitialized.ToString();
            dhtNode.Attributes.Append(document.CreateAttribute("Active")).Value = this.IsRouting.ToString();

            dhtNode.AppendChild(document.CreateElement("NeighbourCount")).AppendChild(document.CreateTextNode(this.UniqueNeighbourCount.ToString()));
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("DHT Tier ").Append(this.TierName).AppendLine(":");
            sb.Append("   Initialized: ").Append(this.IsInitialized).AppendLine();
            sb.Append("   Routing: ").Append(this.IsRouting).AppendLine();
            sb.Append("   Unique neighbour count: ").Append(this.UniqueNeighbourCount).AppendLine();

            return sb.ToString();
        }
    }
}
