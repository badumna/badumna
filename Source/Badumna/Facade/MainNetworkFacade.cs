//------------------------------------------------------------------------------
// <copyright file="MainNetworkFacade.cs" company="National ICT Australia Limited">
//     Copyright (c) 2007-2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Net;
using Badumna.Arbitration;
using Badumna.Chat;
using Badumna.Configuration;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.DistributedHashTable;
using Badumna.Matchmaking;
using Badumna.Security;
using Badumna.ServiceDiscovery;
using Badumna.SpatialEntities;
using Badumna.Streaming;
using Badumna.Transport;
using Badumna.Utilities;
using Badumna.Utilities.Logging;

namespace Badumna
{
    /// <summary>
    /// The main implementation of Badumna's public API.
    /// </summary>
    internal class MainNetworkFacade : NetworkFacade, INetworkFacadeInternal
    {
        /// <summary>
        /// Time of last update dispatch.
        /// </summary>
        [SuppressMessage("Microsoft.StyleCop.CSharp.MaintainabilityRules", "SA1401:FieldsMustBePrivate", Justification = "It's an internal interface")]
        protected TimeSpan lastDispatchMark;

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        [SuppressMessage("Microsoft.StyleCop.CSharp.MaintainabilityRules", "SA1401:FieldsMustBePrivate", Justification = "It's an internal interface")]
        protected NetworkEventQueue eventQueue;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        [SuppressMessage("Microsoft.StyleCop.CSharp.MaintainabilityRules", "SA1401:FieldsMustBePrivate", Justification = "It's an internal interface")]
        protected ITime timeKeeper;

        /// <summary>
        /// A random number generator.
        /// </summary>
        private readonly IRandomNumberGenerator randomNumberGenerator;

        /// <summary>
        /// Shutdown fiber, responsible for calling PerformShutdown method.
        /// </summary>
        private Fiber shutdownFiber;

        /// <summary>
        /// ShutdownOverride fiber, responsible for calling ShutdownOverride method.
        /// </summary>
        private Fiber shutdownOverrideFiber;

        /// <summary>
        /// Thread manager responsible for starting a fiber in different thread.
        /// </summary>
        private ThreadManager threadManager;

        /// <summary>
        /// Indicates that the facade is initialized (not shutdown).
        /// </summary>
        private bool isInitialized;

        /// <summary>
        /// Backing field for IsLoggedIn property.
        /// </summary>
        private bool isLoggedIn;

        /// <summary>
        /// The event queue fiber.
        /// </summary>
        private Fiber eventFiber;

        /// <summary>
        /// A value indicating whether the shutdown override has been started.
        /// </summary>
        private bool shutdownOverrideStarted;

        /// <summary>
        /// Reports on the network connectivity status.
        /// </summary>
        private INetworkConnectivityReporter connectivityReporter;

        /// <summary>
        /// The transport layer.
        /// </summary>
        private ITransport transportLayer;

        /// <summary>
        /// Explicit backing delegate for offline events, required to prevent JITing on Unity iPhone.
        /// </summary>
        private ConnectivityStatusDelegate offlineEventDelegate;

        /// <summary>
        /// Explicit backing delegate for online events, required to prevent JITing on Unity iPhone.
        /// </summary>
        private ConnectivityStatusDelegate onlineEventDelegate;

        /// <summary>
        /// Explicit backing delegate for address changed events, required to prevent JITing on Unity iPhone.
        /// </summary>
        private PublicAddressChangedDelegate addressChangedDelegate;

        /// <summary>
        /// Feature policy, currently used for disabling access to features in cloud mode.
        /// </summary>
        private FeaturePolicy featurePolicy;
        
        /// <summary>
        /// A value indicating if Login has been called previously.
        /// </summary>
        private bool loginCalled;

        /// <summary>
        /// Initializes a new instance of the MainNetworkFacade class.
        /// </summary>
        /// <param name="options">The configuration options</param>
        /// <param name="transportLayer">The transport layer, for sending and receiving messages</param>
        /// <param name="eventQueue">The event queue, for scheduling events</param>
        /// <param name="timeKeeper">The time keeper, for tracking time</param>
        /// <param name="connectivityReporter">The connectivity reporter, for tracking network connectiviy status</param>
        /// <param name="randomNumberGenerator">A random number generator.</param>
        public MainNetworkFacade(
            Options options,
            ITransport transportLayer,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            INetworkConnectivityReporter connectivityReporter,
            IRandomNumberGenerator randomNumberGenerator)
        {
            if (randomNumberGenerator == null)
            {
                throw new ArgumentNullException("randomNumberGenerator");
            }

            this.options = options;
            this.transportLayer = transportLayer;
            this.eventQueue = eventQueue;
            this.timeKeeper = timeKeeper;
            this.connectivityReporter = connectivityReporter;
            this.randomNumberGenerator = randomNumberGenerator;
            this.featurePolicy = new FeaturePolicy(options);

            this.lastDispatchMark = timeKeeper.Now;

            this.Initialize();

            this.isInitialized = true;  // Very last thing in case of exceptions
        }

        /// <inheritdoc />
        public override sealed event ConnectivityStatusDelegate OfflineEvent
        {
            add { this.offlineEventDelegate += value; }
            remove { this.offlineEventDelegate -= value; }
        }

        /// <inheritdoc />
        public override sealed event ConnectivityStatusDelegate OnlineEvent
        {
            add { this.onlineEventDelegate += value; }
            remove { this.onlineEventDelegate -= value; }
        }

        /// <inheritdoc />
        public override sealed event PublicAddressChangedDelegate AddressChangedEvent
        {
            add { this.addressChangedDelegate += value; }
            remove { this.addressChangedDelegate -= value; }
        }

        /// <inheritdoc />
        public override InitializationState InitializationProgress
        {
            get
            {
                if (this.transportLayer.HasFinishedInitializing)
                {
                    return InitializationState.Complete;
                }
                else
                {
                    return InitializationState.Uninitialized;
                }
            }
        }

        /// <inheritdoc />
        public override sealed bool IsLoggedIn
        {
            get { return this.isLoggedIn; }
        }

        /// <inheritdoc/>
        public sealed override bool IsFullyConnected
        {
            get
            {
                if (this.isInitialized && this.IsLoggedIn)
                {
                    DhtFacade openDhtFacade = this.Stack.DhtFacades[NamedTier.OpenAddresses];
                    return openDhtFacade.IsRouting && openDhtFacade.UniqueNeighbourCount > 0;
                }

                return false;
            }
        }

        /// <inheritdoc/>
        public override bool IsOffline
        {
            get { return this.connectivityReporter.Status == ConnectivityStatus.Offline; }
        }

        /// <inheritdoc/>
        public override bool IsOnline
        {
            get { return this.connectivityReporter.Status == ConnectivityStatus.Online; }
        }

        /// <inheritdoc />
        public override double InboundBytesPerSecond
        {
            get { return this.Stack.TransportLayer.InboundBytesPerSecond; }
        }

        /// <inheritdoc />
        public override double OutboundBytesPerSecond
        {
            get { return this.Stack.TransportLayer.OutboundBytesPerSecond; }
        }

        /// <inheritdoc />
        public override double MaximumPacketLossRate
        {
            get { return this.Stack.ConnectionTable.MaximumPacketLossRate; }
        }

        /// <inheritdoc />
        public override double AveragePacketLossRate
        {
            get { return this.Stack.ConnectionTable.AveragePacketLossRate; }
        }

        /// <inheritdoc />
        public override double TotalSendLimitBytesPerSecond
        {
            get { return this.Stack.ConnectionTable.TotalSendLimitBytesPerSecond; }
        }

        /// <inheritdoc />
        public override double MaximumSendLimitBytesPerSecond
        {
            get { return this.Stack.ConnectionTable.MaximumSendLimitBytesPerSecond; }
        }

        /// <inheritdoc />
        public override TypeRegistry TypeRegistry
        {
            get
            {
                return this.Stack.TypeRegistry;
            }
        }

        /// <inheritdoc />
        public override RPCManager RPCManager
        {
            get
            {
                return this.Stack.RPCManager;
            }
        }

        /// <inheritdoc />
        public override StreamingManager Streaming
        {
            get
            {
                this.featurePolicy.AccessStreaming();
                if (!this.IsLoggedIn)
                {
                    throw new InvalidOperationException("Must login first");
                }

                return this.Stack == null ? null : this.Stack.StreamingManager;
            }
        }

        /// <inheritdoc />
        public override Badumna.Validation.IFacade ValidationFacade
        {
            get { return this.Stack.ValidationManager; }
        }

        /// <inheritdoc />
        public override Badumna.Match.Facade Match
        {
            get { return this.Stack.Match; }
        }

        /// <summary>
        /// Gets or sets the stack, which contains the major system components.
        /// </summary>
        /// <remarks>
        /// This is marked public rather than internal because this allows the setter to
        /// be marked protected.  The class itself is internal so this is more restrictive
        /// than marking both getter and setter as internal.
        /// </remarks>
        public ProtocolStack Stack { get; protected set; }

        /// <inheritdoc/>
        NetworkEventQueue INetworkFacadeInternal.EventQueue
        {
            get { return this.eventQueue; }
        }

        /// <inheritdoc/>
        ITime INetworkFacadeInternal.TimeKeeper
        {
            get { return this.timeKeeper; }
        }

        /// <inheritdoc />
        internal override TokenCache TokenCache
        {
            get
            {
                return this.Stack.TokenCache;
            }
        }

        /// <summary>
        /// Gets the diagnotics object, for debugging.
        /// </summary>
        internal Diagnostics Diagnostics { get; private set; }

        /// <inheritdoc />
        BadumnaId INetworkFacadeInternal.GenerateId()
        {
            return this.Stack.BadumnaIdAllocator.GetNextUniqueId();
        }

        /// <inheritdoc />
        public override sealed bool Login(IIdentityProvider identityProvider)
        {
            if (this.loginCalled)
            {
                throw new InvalidOperationException("Login can only be called once on each instance of Badumna.");
            }

            this.loginCalled = true;

            if (!this.isInitialized)
            {
                throw new InvalidOperationException("Facade has been shut down.");
            }

            // Block until STUN is complete to ensure that the public address is known  
            while (this.InitializationProgress != InitializationState.Complete)
            {
                System.Threading.Thread.Sleep(50);  // TODO: Ideally this should use a signal
            }

            this.Stack.TokenCache.SetIdentityProvider(identityProvider);

            if (!this.Stack.TokenCache.PreCache())
            {
                return false;
            }

            if (!this.Stack.TransportLayer.IsAddressKnownAndValid)
            {
                this.Stack.ConnectivityReporter.SetStatus(ConnectivityStatus.Offline);
                return false;
            }

            this.Stack.Login();

            this.isLoggedIn = true;

            return true;
        }

        /// <inheritdoc />
        public override void Shutdown(bool blockUntilComplete)
        {
            if (!this.isInitialized)
            {
                return;
            }

            try
            {
                if (this.Stack.ConnectivityReporter.Status != ConnectivityStatus.Offline)
                {
                    this.Stack.ConnectivityReporter.SetStatus(ConnectivityStatus.ShuttingDown);
                }

                this.DoProcessNetworkState(); // One last call to ensure that any lingering application events are called.
            }
            catch (Exception e)
            {
                Logger.TraceException(LogLevel.Error, e, "Caught exception when shutting down");
            }

            this.isInitialized = false;

            this.shutdownFiber = new Fiber(this.PerformShutdown);

            this.shutdownFiber.IsBackground = false;
            this.threadManager.Start(this.shutdownFiber);

            if (blockUntilComplete)
            {
                this.shutdownFiber.Join();
            }
        }

        /// <inheritdoc />
        public override NetworkStatus GetNetworkStatus()
        {
            this.eventQueue.AcquirePerformLockForApplicationCode();
            try
            {
                return new NetworkStatus(
                    this,
                    this.options.Overload.IsServer,
                    this.Stack.ArbitrationManager.IsServer,
                    this.options.Connectivity.IsPortForwardingEnabled,
                    this.Stack.PeerFinder,
                    this.Stack.TransportLayer);
            }
            finally
            {
                this.eventQueue.ReleasePerformLockForApplicationCode();
            }
        }

        ////public 

        /// <inheritdoc />
        public override NetworkScene JoinScene(string sceneName, CreateSpatialReplica createObjectDelegate, RemoveSpatialReplica removeObjectDelegate)
        {
            return this.JoinScene(sceneName, createObjectDelegate, removeObjectDelegate, false);
        }

        /// <inheritdoc/>
        public override NetworkScene JoinMiniScene(string sceneName, CreateSpatialReplica createEntityDelegate, RemoveSpatialReplica removeEntityDelegate)
        {
            return this.JoinScene(sceneName, createEntityDelegate, removeEntityDelegate, true);
        }

        /// <inheritdoc />
        public override sealed void FlagForUpdate(ISpatialOriginal localEntity, BooleanArray changedParts)
        {
            if (!this.IsLoggedIn)
            {
                throw new InvalidOperationException("Login must be called first");
            }

            if (localEntity == null)
            {
                throw new ArgumentNullException("localEntity");
            }

            // copy the BoolenArray
            BooleanArray localCopy = new BooleanArray(changedParts);
            this.Stack.SpatialEntityManager.MarkForUpdate(localEntity, localCopy);
        }

        /// <inheritdoc />
        public override sealed void FlagForUpdate(ISpatialOriginal localEntity, int changedPartIndex)
        {
            if (!this.IsLoggedIn)
            {
                throw new InvalidOperationException("Login must be called first");
            }

            if (localEntity == null)
            {
                throw new ArgumentNullException("localEntity");
            }

            // Called in event queue to ensure there is no conflict with online events and other threading issues.
            this.Stack.SpatialEntityManager.MarkForUpdate(localEntity, changedPartIndex);
        }

        /// <inheritdoc />
        public override void FlagFullUpdate(ISpatialEntity localEntity)
        {
            Logger.TraceInformation(LogTag.Facade, "In FlagFullUpdate");
            var spatialOriginal = localEntity as ISpatialOriginal;
            if (spatialOriginal != null)
            {
                Logger.TraceInformation(LogTag.Facade, "Calling FlagForUpdate");
                this.FlagForUpdate(spatialOriginal, new BooleanArray(true));
            }
            else
            {
                Logger.TraceInformation(LogTag.Facade, "Calling Autoreplication's FlagFullUpdate");
                this.Stack.AutoreplicationManager.FlagFullUpdate(localEntity);
            }
        }

        /// <inheritdoc />
        public override Vector3 GetDestination(IDeadReckonable deadReckonable)
        {
            return this.Stack.SpatialEntityManager.DeadReckoningHelper.GetDestination(deadReckonable);
        }

        /// <inheritdoc />
        public override void SnapToDestination(IDeadReckonable deadReckonable)
        {
            this.Stack.SpatialEntityManager.DeadReckoningHelper.SnapToDestination(deadReckonable);
        }

        /// <inheritdoc />
        public override sealed void SendCustomMessageToRemoteCopies(ISpatialOriginal localEntity, MemoryStream eventData)
        {
            if (!this.IsLoggedIn)
            {
                throw new InvalidOperationException("Login must be called first");
            }

            if (localEntity == null)
            {
                throw new ArgumentNullException("localEntity");
            }

            this.eventQueue.Push(this.Stack.SpatialEntityManager.SendCustomMessageToReplicas, localEntity, eventData);
        }

        /// <inheritdoc />
        public override sealed void SendCustomMessageToOriginal(ISpatialReplica remoteEntity, MemoryStream eventData)
        {
            if (!this.isLoggedIn)
            {
                throw new InvalidOperationException("Login must be called first");
            }

            if (this.IsOffline)
            {
                // TODO : In the future this operation will be able to continue.
                throw new InvalidOperationException("Network is offline, event not sent");
            }

            if (remoteEntity == null)
            {
                throw new ArgumentNullException("remoteEntity");
            }

            // TODO: Why does this happen via the spatial entity manager. Since it does not relate to spatial information
            // I think it should happend directly via the (non-spatial) entity manager.
            this.eventQueue.Push(this.Stack.SpatialEntityManager.SendCustomMessageToOriginal, remoteEntity, eventData);
        }

        /// <inheritdoc />
        public override void StartController<T>(string controllerUniqueName)
        {
            if (!this.isLoggedIn)
            {
                throw new InvalidOperationException("Login must be called first");
            }

            if (this.Stack != null && this.Stack.ControllerIntializer != null)
            {
                this.Stack.ControllerIntializer.StartController<T>(controllerUniqueName);
            }
        }

        /// <inheritdoc />
        public override string StartController<T>(string sceneName, string controllerName, ushort max)
        {
            if (!this.isLoggedIn)
            {
                throw new InvalidOperationException("Login must be called first");
            }

            if (sceneName == null)
            {
                throw new ArgumentNullException("sceneName can't be null.");
            }

            if (controllerName == null)
            {
                throw new ArgumentNullException("controllerName can't be null.");
            }

            int randomNumber = RandomSource.Generator.Next((int)max);
            string uniqueName = string.Format("{0}^{1}_{2}", sceneName, controllerName, randomNumber);
            this.StartController<T>(uniqueName);

            return uniqueName;
        }

        /// <inheritdoc />
        public override void StopController<T>(string controllerUniqueName)
        {
            if (!this.isLoggedIn)
            {
                throw new InvalidOperationException("Login must be called first");
            }

            if (this.Stack != null && this.Stack.ControllerIntializer != null)
            {
                this.Stack.ControllerIntializer.StopController<T>(controllerUniqueName);
            }
        }

        /// <inheritdoc />
        public override void RegisterArbitrationHandler(HandleClientMessage handler, TimeSpan disconnectTimeout, HandleClientDisconnect disconnect)
        {
            this.Stack.ArbitrationManager.RegisterArbitrationHandler(handler, disconnectTimeout, disconnect);
        }

        /// <inheritdoc />
        public override IArbitrator GetArbitrator(string name)
        {
            if (!this.isLoggedIn)
            {
                throw new InvalidOperationException("Login must be called first");
            }

            return this.Stack.ArbitrationManager.GetArbitrator(name);
        }

        /// <inheritdoc />
        public override void SendServerArbitrationEvent(int destinationSessionId, byte[] message)
        {
            if (message == null)
            {
                throw new ArgumentNullException("message");
            }

            this.Stack.ArbitrationManager.SendServerEvent(destinationSessionId, message);
        }

        /// <inheritdoc />
        public override long GetUserIdForSession(int sessionId)
        {
            return this.Stack.ArbitrationManager.GetUserIdForSession(sessionId);
        }

        /// <inheritdoc />
        public override Character GetCharacterForArbitrationSession(int sessionId)
        {
            return this.Stack.ArbitrationManager.GetCharacterForSession(sessionId);
        }

        /// <inheritdoc />
        public override BadumnaId GetBadumnaIdForArbitrationSession(int sessionId)
        {
            return this.Stack.ArbitrationManager.GetBadumnaIdForSession(sessionId);
        }

        /// <inheritdoc />
        public override MatchmakingAsyncResult BeginMatchmaking(
            HandleMatchmakingResult onCompletion,
            HandleMatchmakingProgress onProgress,
            MatchmakingOptions options)
        {
            if (!this.IsLoggedIn)
            {
                throw new InvalidOperationException("Must login first");
            }

            return this.Stack.BeginMatchmaking(onCompletion, onProgress, options);
        }

        /// <inheritdoc />
        public override void AnnounceService(ServerType type)
        {
            this.featurePolicy.AccessServiceDiscovery();

            if (!this.HasAnnouncePermission())
            {
                throw new SecurityException("Trying to announce service on regular peer.");
            }

            if (type == ServerType.Arbitration)
            {
                this.Stack.ServiceManager.AnnounceArbitrationService();
            }
            else if (type == ServerType.Overload)
            {
                this.Stack.ServiceManager.AnnounceOverloadService();
            }
            else
            {
                throw new ArgumentException("Not supported server type.");
            }
        }

        /// <inheritdoc />
        public override StatisticsTracker CreateTracker(string serverAddress, int serverPort, TimeSpan interval, string initialPayload)
        {
            StatisticsTracker tracker = new StatisticsTracker(this.eventQueue, this.Stack.ConnectivityReporter, interval, serverAddress, serverPort, initialPayload);

            ICertificateToken token = this.Stack.TokenCache.GetUserCertificateToken();
            if (token != null)
            {
                tracker.SetUserID(token.UserId);
            }

            return tracker;
        }

        /// <inheritdoc />
        public override void RegisterEntityDetails(float areaOfInterestRadius, float maxEntitySpeed)
        {
            this.Stack.InterestManagementService.Optimize(areaOfInterestRadius, maxEntitySpeed);
        }

        /// <summary>
        /// Gets a snapshot of state information from the network stack for diagnostic purposes.
        /// </summary>
        /// <remarks>
        /// This API is in development and is currently only for internal use.
        /// It will probably change sigificantly.
        /// WARNING: This method is currently not thread-safe!
        /// TODO: Figure out what kind of thread safety this method should support.
        /// </remarks>
        /// <returns>A dictionary containing the diagnostic information.</returns>
        DiagnosticInfo INetworkFacadeInternal.GetDiagnosticInformation()
        {
            this.eventQueue.AcquirePerformLockForApplicationCode();
            try
            {
                DiagnosticInfo result = new DiagnosticInfo();
                result.Add("connection_table", this.Stack.ConnectionTable.GetDiagnosticInformation());
                return result;
            }
            finally
            {
                this.eventQueue.ReleasePerformLockForApplicationCode();
            }
        }

        /// <summary>
        /// Does initialization specific to the facade.
        /// </summary>
        /// <remarks>
        /// This is overridden by SimulatorFacade.
        /// </remarks>
        protected virtual void Initialize()
        {
            this.threadManager = new ThreadManager();

            this.eventQueue.IsRunning = true;
            this.eventFiber = new Fiber(this.eventQueue.Run);
            this.eventFiber.Name = "Network event queue loop";
            this.eventFiber.IsBackground = true;
            this.threadManager.Start(this.eventFiber);

            this.Diagnostics = new Diagnostics((INetworkFacadeInternal)this);

            this.Stack = new FullStack(
                this,
                this.options,
                this.transportLayer,
                delegate(FullStack stack)
                {
                    stack.ConnectionTable.Parser.RegisterMethodsIn(this.Diagnostics);
                },
                this.eventQueue,
                this.timeKeeper,
                this.connectivityReporter,
                new RandomNumberGenerator());

            this.Diagnostics.Initialize(this);

            this.Stack.Initialize();

            // TODO : Should these be subscribed to before the stack is initialized - to ensure 
            //        that they are subscribed to before it can POSSIBLY be called.
            this.Stack.ConnectivityReporter.NetworkInitializingEvent += this.NetworkOfflineEvent;
            this.Stack.ConnectivityReporter.NetworkOfflineEvent += this.NetworkOfflineEvent;
            this.Stack.ConnectivityReporter.NetworkOnlineEvent += this.NetworkOnlineEvent;
            this.Stack.TransportLayer.PublicAddressChanged += this.NetworkAddressChangedEvent;
        }

        /// <inheritdoc />
        protected override sealed void OnProcessNetworkState()
        {
            if (!this.isLoggedIn)
            {
                throw new InvalidOperationException("Login must be called first");
            }

            // DoProcessNetworkStatus can be called to skip the IsLoggedIn check. It is required when called by
            // NetworkFacade.Shutdown to process events in the application event queue (e.g. see Distributed 
            // Controller's sleep all method).  
            this.DoProcessNetworkState();
        }

        /// <inheritdoc />
        protected override bool DoRegisterApplicationNode(BadumnaId index, ApplicationProtocolNode node)
        {
            if (!this.isLoggedIn)
            {
                throw new InvalidOperationException("Login must be called first");
            }

            if (this.Stack != null && this.Stack.ControllerIntializer != null)
            {
                return this.Stack.ControllerIntializer.AddNode(index, node);
            }

            return false;
        }

        /// <inheritdoc />
        protected override List<IPEndPoint> DoGetRecentActiveOpenPeers()
        {
            ConnectionTable ct = this.Stack.ConnectionTable;
            return ct.GetRecentActiveOpenPeers();
        }

        /// <inheritdoc />
        protected override void DoAddInitialConnections(List<IPEndPoint> endpoints)
        {
            ConnectionTable ct = this.Stack.ConnectionTable;
            ct.AddInitialConnections(endpoints);
        }

        /// <inheritdoc />
        protected override IChatSession CreateChatSession()
        {
            return this.Stack.CreateChatSession();
        }

        /// <summary>
        /// Performs any required regular processing.
        /// </summary>
        /// <remarks>
        /// Executes in an application thread.
        /// </remarks>
        private void DoProcessNetworkState()
        {
            Logger.TraceInformation(LogTag.Facade | LogTag.Periodic, "ProcessNetworkState()");

            if (this.eventQueue.RunException != null)
            {
                throw new BadumnaException("Network thread died, see inner exception for details", this.eventQueue.RunException);
            }

            this.eventQueue.AcquirePerformLockForApplicationCode();
            TimeSpan startTime = this.timeKeeper.Now;
            try
            {
                this.Stack.AutoreplicationManager.Tick(startTime);
                this.Stack.MatchAutoreplicationManager.Tick(startTime);

                // Update all entities that have been marked for update since last call.
                TimeSpan delay = startTime - this.lastDispatchMark;
                if (this.Stack.EntityManager != null && delay > Parameters.MaximumUpdateRate)
                {
                    this.Stack.EntityManager.DispatchUpdates();
                    this.lastDispatchMark = startTime;
                }

                this.Stack.ApplicationEventQueue.Run();

                // Handle updates that have arrived since last call.
                if (this.Stack.EntityManager != null)
                {
                    this.Stack.EntityManager.ProcessInboundUpdates();
                }

                if (this.Stack.SpatialEntityManager != null)
                {
                    this.Stack.SpatialEntityManager.OnProcessNetworkState();
                }

                // Give the application nodes a chance to receive messages
                if (this.Stack.ControllerIntializer != null)
                {
                    // Give processor to controllers for one iteration
                    this.Stack.ControllerIntializer.Process();

                    this.Stack.ControllerIntializer.ProcessMessages();
                }

                Validation.Facade validationManager = this.Stack.ValidationManager as Validation.Facade;
                if (validationManager != null)
                {
                    validationManager.RegularProcessing();
                }
            }
            finally
            {
                this.eventQueue.ReleasePerformLockForApplicationCode();
                Logger.TraceInformation(LogTag.Facade | LogTag.Periodic, "ProcessNetworkState() complete duration={0}ms", (this.timeKeeper.Now - startTime).TotalMilliseconds);
            }
        }

        /// <summary>
        /// Determines whether the local peer has has announce permission.
        /// </summary>
        /// <returns>
        /// <c>true</c> if has announce permission; otherwise, <c>false</c>.
        /// </returns>
        private bool HasAnnouncePermission()
        {
            return this.Stack.TokenCache.GetPermission(PermissionType.Announce) != null; // TODO: Shouldn't this validate that the permission hasn't expired?
        }

        /// <summary>
        /// Invokes the OnlineEvent.
        /// </summary>
        private void NetworkOnlineEvent()
        {
            ConnectivityStatusDelegate onlineEvent = this.onlineEventDelegate;
            if (null != onlineEvent)
            {
                try
                {
                    onlineEvent();
                }
                catch (Exception e)
                {
                    Logger.TraceException(LogLevel.Warning, e, "MainNetworkFacade.OnlineEvent threw exception");
                }
            }
        }

        /// <summary>
        /// Invokes the OfflineEvent.
        /// </summary>
        private void NetworkOfflineEvent()
        {
            ConnectivityStatusDelegate offlineEvent = this.offlineEventDelegate;
            if (null != offlineEvent)
            {
                try
                {
                    offlineEvent();
                }
                catch (Exception e)
                {
                    Logger.TraceException(LogLevel.Warning, e, "MainNetworkFacade.OfflineEvent threw exception");
                }
            }
        }

        /// <summary>
        /// Invokes the AddressChangedEvent on an application thread.
        /// </summary>
        private void NetworkAddressChangedEvent()
        {
            PublicAddressChangedDelegate addressChangedEvent = this.addressChangedDelegate;
            if (addressChangedEvent == null)
            {
                return;
            }

            this.Stack.ApplicationEventQueue.Add(Apply.Func(delegate
            {
                try
                {
                    addressChangedEvent();
                }
                catch (Exception e)
                {
                    Logger.TraceException(LogLevel.Warning, e, "MainNetworkFacade.NetworkAddressChangedEvent threw exception");
                }
            }));
        }

        /// <summary>
        /// Performs initial shutdown tasks.
        /// </summary>
        /// <param name="fiber">The fiber we're running on</param>
        private void PerformShutdown(Fiber fiber)
        {
            if (this.shutdownOverrideFiber == null)
            {
                this.shutdownOverrideFiber = new Fiber(this.LingeredShutdown);
            }

            if (!this.shutdownOverrideFiber.IsComplete)
            {
                this.shutdownOverrideFiber.Body(this.shutdownOverrideFiber);
                return;
            }

            this.Stack.Shutdown();

            try
            {
                this.Stack.TokenCache.Dispose();
            }
            catch (Exception e)
            {
                Logger.TraceException(LogLevel.Error, e, "Caught exception when shutting down token cache");
            }

            fiber.IsComplete = true;
            this.shutdownOverrideFiber = null;
        }

        /// <summary>
        /// Lingers for a while post-shutdown waiting for scheduled taks to complete.
        /// </summary>
        /// <param name="fiber">The fiber we're running on</param>
        private void LingeredShutdown(Fiber fiber)
        {
            if (this.eventFiber != null)
            {
                if (!this.shutdownOverrideStarted)
                {
                    this.eventQueue.IsPaused = false;  // TODO: Should disable pausing altogether at this point
                    this.eventQueue.Schedule(
                        Parameters.ShutdownDelayMs,
                        delegate
                        {
                            this.eventQueue.Stop();
                        });

                    this.shutdownOverrideStarted = true;
                }

                // Need to give enough extra time for the event queue to exit.
                if (!this.eventFiber.Join(Parameters.ShutdownDelayMs + 500))
                {
                    Logger.TraceInformation(LogTag.Facade, "Network event queue thread did not shutdown cleanly");
                    this.eventFiber.Abort();
                }

                fiber.IsComplete = true;
                this.eventFiber = null;
                this.shutdownOverrideStarted = false;
            }
        }

        /// <summary>
        /// Joins into a scene.
        /// </summary>
        /// <param name="sceneName">The unique name identifying the scene.</param>
        /// <param name="createEntityDelegate">Called when a new entity needs to be instantiated into the scene</param>
        /// <param name="removeEntityDelegate">Called when an entity in the scene departs.</param>
        /// <param name="isMiniScene">A value indicating whether the scene is a mini scene.</param>
        /// <returns>An instance of NetworkScene representing the mini scene.</returns>
        private NetworkScene JoinScene(string sceneName, CreateSpatialReplica createEntityDelegate, RemoveSpatialReplica removeEntityDelegate, bool isMiniScene)
        {
            if (!this.IsLoggedIn)
            {
                throw new InvalidOperationException("Login must be called first");
            }

            if (sceneName == null || sceneName.Length == 0)
            {
                throw new ArgumentNullException("sceneName");
            }

            return this.Stack.SpatialEntityManager.JoinScene(new QualifiedName(this.options.CloudIdentifier, sceneName), createEntityDelegate, removeEntityDelegate, isMiniScene);
        }
    }
}
