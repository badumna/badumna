﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Transport;
using Badumna.DistributedHashTable;
using Badumna.Security;

namespace Badumna
{
    interface ITrustedExtension
    {
        void Initialize();
        void Shutdown();
        IComplaintForwarder CreateComplaintForwarder(TransportProtocol parent, INetworkAddressProvider addressProvider, TokenCache tokens, INetworkConnectivityReporter connectivityReporter);
        DhtFacade CreateTrustedDhtFacade(RouterMultiplexer multiplexer, IConnectionTable connectionTable);
        SystemControlService CreateSystemControlService(DhtProtocol parent, ConnectionTable connectionTable, TokenCache tokens);
    }
}
