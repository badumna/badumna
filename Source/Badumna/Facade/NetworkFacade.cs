﻿//------------------------------------------------------------------------------
// <copyright file="NetworkFacade.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

using Badumna.Arbitration;
using Badumna.Chat;
using Badumna.Controllers;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Facade.Http;
using Badumna.Matchmaking;
using Badumna.Security;
using Badumna.ServiceDiscovery;
using Badumna.SpatialEntities;
using Badumna.Streaming;
using Badumna.Transport;
using Badumna.Utilities;
using Badumna.Utilities.Logging;

namespace Badumna
{
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.Xml;
    using System.Threading;

    /// <summary>
    /// The network engine.
    /// </summary>
    public abstract class NetworkFacade : INetworkFacadeInternal, INetworkFacade
    {
        private static GenericCallBackReturn<INetworkFacade, string> cloudCreateDelegate =
            (ID) => NetworkFacade.Create(ID);
        
        private static GenericCallBackReturn<INetworkFacade, Options> nonCloudCreateDelegate =
            (options) => NetworkFacade.Create(options);
        
        private static AutoResetEvent createSynchronizationEvent = new AutoResetEvent(true);

        /// <summary>
        /// Begin the asyncronously intialization of BadumnaCloud.
        /// </summary>
        /// <param name="id">Your Badumna Cloud application ID.</param>
        /// <param name="callback">A callback that will be invoked when initialization is complete.</param>
        /// <returns>A result object.</returns>
        public static IAsyncResult BeginCreate(string id, AsyncCallback callback)
        {
            createSynchronizationEvent.WaitOne();
            return cloudCreateDelegate.BeginInvoke(id, callback, "IsCloud");
        }

        /// <summary>
        /// Begin the asyncronously intialization of Badumna.
        /// </summary>
        /// <param name="options">Badumna configuration options.</param>
        /// <param name="callback">A callback that will be invoked when initialization is complete.</param>
        /// <returns>A result object.</returns>
        public static IAsyncResult BeginCreate(Options options, AsyncCallback callback)
        {
            createSynchronizationEvent.WaitOne();
            return nonCloudCreateDelegate.BeginInvoke(options, callback, null);
        }

        /// <summary>
        /// Retrieve the network facade after asynchronous BadumnaCloud initialization.
        /// </summary>
        /// <param name="result">The result of the initialization.</param>
        /// <returns>A new Badumna network facade.</returns>
        /// <exception cref="ConnectivityException">If the public address of the machine cannot be determined.</exception>
        /// <exception cref="ConfigurationException">If the configuration was invalid or if in cloud mode and the configuration could not be retrieved (see inner exception for details).</exception>
        public static INetworkFacade EndCreate(IAsyncResult result)
        {
            try
            {
                if (!string.IsNullOrEmpty((string)result.AsyncState) && ((string)result.AsyncState).Equals("IsCloud"))
                {
                    return cloudCreateDelegate.EndInvoke(result);
                }
                else
                {
                    return nonCloudCreateDelegate.EndInvoke(result);
                }
            }
            finally
            {
                createSynchronizationEvent.Set();
            }
        }        

        /// <summary>
        /// Delegate for shutdown request events.
        /// </summary>
        private EventHandler requestShutdownDelegate;

        /// <summary>
        /// Delegate for shutdown-for-update request events.
        /// </summary>
        private EventHandler requestShutdownForUpdateDelegate;

        /// <summary>
        /// Configuration options for the system.
        /// </summary>
        [SuppressMessage("Microsoft.StyleCop.CSharp.MaintainabilityRules", "SA1401:FieldsMustBePrivate", Justification = "It's an internal interface")]
        protected Options options;

        /// <summary>
        /// Creates a NetworkFacade using the given options.
        /// </summary>
        /// <remarks>
        /// The contents of <paramref name="options"/> must not be changed after calling this method.
        /// The effect of changing the options after creating the facade is undefined.
        /// </remarks>
        /// <param name="options">The options used to configure the new network facade.</param>
        /// <returns>The new NetworkFacade instance.</returns>
        /// <exception cref="ConnectivityException">If the public address of the machine cannot be determined.</exception>
        public static INetworkFacade Create(Options options)
        {
            return NetworkFacade.Create(options, true);
        }
        
        /// <summary>
        /// Creates a NetworkFacade using the given options.
        /// </summary>
        /// <remarks>
        /// The contents of <paramref name="options"/> must not be changed after calling this method.
        /// The effect of changing the options after creating the facade is undefined.
        /// </remarks>
        /// <param name="options">The options used to configure the new network facade.</param>
        /// <param name="blockUntilComplete">A value indicating whether initialization should be synchronous.</param>
        /// <returns>The new NetworkFacade instance.</returns>
        /// <exception cref="ConnectivityException">If <paramref name="blockUntilComplete"/> is true and the public address cannot be determined.</exception>
        internal static INetworkFacade Create(Options options, bool blockUntilComplete)
        {
            /* TODO: When the C++ wrapper supports the new non-blocking initialization the 'blockUntilComplete'
             *       parameter should be removed.
             */

            if (options == null)
            {
                throw new ArgumentNullException("options");
            }

            // Take a snapshot of the options so the client app can't modify them after we've started using
            // them.
            options = new Options(options);
            options.Validate();

            NetworkEventQueue eventQueue = new NetworkEventQueue();

            ITime timeKeeper = eventQueue;
            INetworkConnectivityReporter connectivityReporter = new NetworkConnectivityReporter(eventQueue);

            // Set up logging as soon as practicable
            NetworkFacade.ConfigureLogWithOverride(options.Logger, timeKeeper);

            // Create the transport layer here so we can check for connectivity if the tunnel mode is auto
            // TODO: Shouldn't do this if tunnel mode set to on!
            ITransport transportLayer = new UdpTransport(options.Connectivity, eventQueue, timeKeeper, connectivityReporter);

            transportLayer.Initialize(blockUntilComplete);

            if (blockUntilComplete && !transportLayer.IsAddressKnownAndValid)
            {
                var message = "Public address could not be determined.  Ensure there is a network interface with UDP connectivity.";
                var error = ConnectivityException.ErrorCode.Unspecified;
                if (transportLayer.PublicAddress.NatType == NatType.Blocked)
                {
                    error = ConnectivityException.ErrorCode.UdpBlocked;
                }

                throw new ConnectivityException(error, message);
            }

            bool haveUri = options.Connectivity.TunnelUris.Count > 0;

            bool useTunnel;
            switch (options.Connectivity.TunnelMode)
            {
                case TunnelMode.Auto:
                    if (!haveUri)
                    {
                        useTunnel = false;
                        break;
                    }

                    // Wait for UDPTransportLayer initialization to finish to see if it's working
                    while (!transportLayer.HasFinishedInitializing)
                    {
                        System.Threading.Thread.Sleep(50);
                    }

                    useTunnel = !transportLayer.IsAddressKnownAndValid;
                    break;

                case TunnelMode.On:
                    useTunnel = true;
                    break;

                case TunnelMode.Off:
                default:
                    useTunnel = false;
                    break;
            }

            if (useTunnel && !haveUri)
            {
                throw new InvalidOperationException("Attempting to use tunnel but no tunnel URI has been specified");
            }

            if (useTunnel)
            {
                return new HttpTunnelNetworkFacade(options, eventQueue, timeKeeper);
            }

            return new MainNetworkFacade(
                options, transportLayer, eventQueue, timeKeeper, connectivityReporter, new RandomNumberGenerator());
        }

        /// <summary>
        /// Creates a NetworkFacade configured for the given cloud identifier.
        /// </summary>
        /// <param name="identifier">The identifier of the cloud network to connect to.</param>
        /// <returns>The new NetworkFacade instance.</returns>
        /// <exception cref="ConfigurationException">Cloud configuration could not be retrieved (see inner exception for details).</exception>
        /// <exception cref="ConnectivityException">If the public address of the machine cannot be determined.</exception>
        public static INetworkFacade Create(string identifier)
        {
            if (string.IsNullOrEmpty(identifier))
            {
                throw new ArgumentException("Must not be null or empty", "identifier");
            }

            var config = new XmlDocument();
            string cloudURI = "http://cloud.badumna.com";
            var cloudOverride = System.Environment.GetEnvironmentVariable("BADUMNA_CLOUD_URI");
            if (cloudOverride != null)
            {
                cloudURI = cloudOverride;
            }
            var localConfigFile = System.Environment.GetEnvironmentVariable("BADUMNA_CONFIG_FILE");
            if (localConfigFile != null)
            {
                try
                {
                    config.Load(localConfigFile);
                }
                catch (Exception e)
                {
                    throw new ConfigurationException("Could not load config from local file " + localConfigFile, e);
                }
            }
            else
            {
                try
                {
                var configRequest = SimpleHttpWebRequest.Create(cloudURI + "/config/" + Uri.EscapeUriString(identifier));
                using (var configResponse = configRequest.GetResponse())
                using (var configStream = configResponse.GetResponseStream())
                    {
                        config.Load(configStream);
                    }
                }
                catch (Exception e)
                {
                    throw new ConfigurationException("Could not load cloud configuration", e);
                }
            }

            var options = new Options(config);
            options.CloudIdentifier = identifier;
            var facade = NetworkFacade.Create(options, true);

            var trackerAddress = "tracker.badumna.com";
            var trackerAddressOverride = System.Environment.GetEnvironmentVariable("BADUMNA_TRACKER");
            if (trackerAddressOverride != null)
            {
                trackerAddress = trackerAddressOverride;
            }
            facade.CreateAndStartTracker(trackerAddress, 21256, TimeSpan.FromMinutes(5), identifier);

            return facade;
        }

        /// <inheritdoc/>
        public abstract InitializationState InitializationProgress { get; }

        /// <inheritdoc/>
        public bool IsTunnelled
        {
            get
            {
                return this is HttpTunnelNetworkFacade;
            }
        }

        /// <inheritdoc/>
        public abstract bool IsLoggedIn { get; }

        /// <inheritdoc/>
        public abstract bool IsFullyConnected { get; }

        /// <inheritdoc/>
        public abstract bool IsOffline { get; }

        /// <inheritdoc/>
        public abstract bool IsOnline { get; }

        internal abstract TokenCache TokenCache { get; }

        /// <inheritdoc/>
        public Character Character
        {
            get
            {
                var tokenCache = this.TokenCache;
                return tokenCache == null ? null : tokenCache.Character;
            }
        }

        /// <inheritdoc />
        public IChatSession ChatSession
        {
            get
            {
                lock (this.mChatSessionLock)
                {
                    if (!this.IsLoggedIn)
                    {
                        throw new InvalidOperationException("Must login first");
                    }

                    if (this.mChatSession == null)
                    {
                        Logger.TraceInformation(LogTag.Facade, "NetworkFacade: Creating chat session...");
                        this.mChatSession = this.CreateChatSession();
                    }
                    return this.mChatSession;
                }
            }
        }

        /// <inheritdoc/>
        public abstract event ConnectivityStatusDelegate OfflineEvent;

        /// <inheritdoc/>
        public abstract event ConnectivityStatusDelegate OnlineEvent;

        /// <inheritdoc/>
        public abstract event PublicAddressChangedDelegate AddressChangedEvent;

        /// <inheritdoc/>
        public abstract double OutboundBytesPerSecond { get; }

        /// <inheritdoc/>
        public abstract double InboundBytesPerSecond { get; }

        /// <inheritdoc/>
        public abstract double MaximumPacketLossRate { get; }

        /// <inheritdoc/>
        public abstract double AveragePacketLossRate { get; }

        /// <inheritdoc/>
        public abstract double TotalSendLimitBytesPerSecond { get; }

        /// <inheritdoc/>
        public abstract double MaximumSendLimitBytesPerSecond { get; }

        /// <inheritdoc/>
        public abstract Validation.IFacade ValidationFacade { get; }

        /// <inheritdoc/>
        public abstract Match.Facade Match { get; }

        /// <inheritdoc/>
        public event EventHandler RequestShutdown
        {
            add { this.requestShutdownDelegate += value; }
            remove { this.requestShutdownDelegate -= value; }
        }

        // TODO : Add request cb to user for update download?

        /// <inheritdoc/>
        public event EventHandler RequestShutdownForUpdate
        {
            add { this.requestShutdownForUpdateDelegate += value; }
            remove { this.requestShutdownForUpdateDelegate -= value; }
        }

        private EventHandler processingNetworkState;

        private IChatSession mChatSession;

        private object mChatSessionLock = new object();

        /// <inheritdoc/>
        event EventHandler INetworkFacadeInternal.ProcessingNetworkState
        {
            add { this.processingNetworkState += value; }
            remove { this.processingNetworkState -= value; }
        }

        /// <inheritdoc/>
        Options INetworkFacadeInternal.Options { get { return this.options; } }

        /// <inheritdoc/>
        NetworkEventQueue INetworkFacadeInternal.EventQueue { get { return null; } }

        /// <inheritdoc/>
        ITime INetworkFacadeInternal.TimeKeeper { get { return null; } }

        /// <inheritdoc/>
        public abstract TypeRegistry TypeRegistry { get; }

        /// <inheritdoc/>
        public abstract RPCManager RPCManager { get; }

        /// <inheritdoc/>
        public abstract StreamingManager Streaming { get; }

        /// <inheritdoc />
        BadumnaId INetworkFacadeInternal.GenerateId()
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc/>
        public abstract void RegisterEntityDetails(float maxInterestRadius, float maxEntitySpeed);

        /// <inheritdoc/>
        public void Shutdown()
        {
            this.Shutdown(true);
        }

        /// <inheritdoc/>
        public abstract void Shutdown(bool blockUntilComplete);

        /// <inheritdoc/>
        public abstract bool Login(IIdentityProvider identityProvider);

        /// <inheritdoc/>
        public bool Login(string characterName)
        {
            return this.Login(new UnverifiedIdentityProvider(characterName, UnverifiedIdentityProvider.GenerateKeyPair()));
        }

        /// <inheritdoc/>
        public bool Login(string characterName, string keyPairXml)
        {
            return this.Login(new UnverifiedIdentityProvider(characterName, keyPairXml));
        }

        /// <inheritdoc/>
        public bool Login()
        {
            return this.Login(new UnverifiedIdentityProvider());
        }

        /// <inheritdoc/>
        public abstract NetworkStatus GetNetworkStatus();

        /// <inheritdoc/>
        public abstract NetworkScene JoinScene(string sceneName, CreateSpatialReplica createEntityDelegate, RemoveSpatialReplica removeEntityDelegate);

        /// <inheritdoc/>
        public abstract NetworkScene JoinMiniScene(string sceneName, CreateSpatialReplica createEntityDelegate, RemoveSpatialReplica removeEntityDelegate);

        /// <inheritdoc/>
        public abstract void FlagForUpdate(ISpatialOriginal localEntity, BooleanArray changedParts);

        /// <inheritdoc/>
        public abstract void FlagForUpdate(ISpatialOriginal localEntity, int changedPartIndex);

        /// <inheritdoc/>
        public abstract void FlagFullUpdate(ISpatialEntity localEntity);

        /// <inheritdoc/>
        public abstract Vector3 GetDestination(IDeadReckonable deadReckonable);

        /// <inheritdoc/>
        public abstract void SnapToDestination(IDeadReckonable deadReckonable);

        /// <inheritdoc/>
        public abstract void SendCustomMessageToRemoteCopies(ISpatialOriginal localEntity, MemoryStream eventData);

        /// <inheritdoc/>
        public abstract void SendCustomMessageToOriginal(ISpatialReplica remoteEntity, MemoryStream eventData);

        /// <inheritdoc/>
        DiagnosticInfo INetworkFacadeInternal.GetDiagnosticInformation()
        {
            return null;
        }
        
        /// <inheritdoc/>
        protected abstract IChatSession CreateChatSession();

        /// <inheritdoc/>
        bool INetworkFacadeInternal.RegisterApplicationNode(BadumnaId index, ApplicationProtocolNode node)
        {
            return this.DoRegisterApplicationNode(index, node);
        }

        /// <summary>
        /// Called internally.
        /// </summary>
        /// <remarks>
        /// This protected method is for internal use only and may change or be removed in future versions.
        /// The <see cref="NetworkFacade"/> is not intended to be subclassed by application developers.
        /// </remarks>
        /// <param name="index">For internal use only.</param>
        /// <param name="node">For internal use only.</param>
        /// <exclude/>
        protected virtual bool DoRegisterApplicationNode(BadumnaId index, ApplicationProtocolNode node)
        {
            return false;
        }

        /// <inheritdoc/>
        public abstract void StartController<T>(string controllerUniqueName) where T : DistributedController;

        /// <inheritdoc/>
        public abstract string StartController<T>(string sceneName, string controllerName, ushort max) where T : DistributedController;

        /// <inheritdoc/>
        public abstract void StopController<T>(string controllerUniqueName) where T : DistributedController;

        /// <inheritdoc/>
        public void ProcessNetworkState()
        {
            if (TrialVersionManager.HasTrialVersionRunTimeExpired(((INetworkFacadeInternal)this).TimeKeeper.Now, this.options.OnCloud))
            {
                return;
            }

            EventHandler processingNetworkState = this.processingNetworkState;
            if (processingNetworkState != null)
            {
                processingNetworkState(this, null);
            }

            this.OnProcessNetworkState();
        }

        /// <summary>
        /// Called when process network state is called.
        /// </summary>
        /// <remarks>
        /// This protected method is for internal use only and may change or be removed in future versions.
        /// The <see cref="NetworkFacade"/> is not intended to be subclassed by application developers.
        /// </remarks>
        /// <exclude/>
        protected abstract void OnProcessNetworkState();

        /// <inheritdoc/>
        public abstract void RegisterArbitrationHandler(HandleClientMessage handler, TimeSpan disconnectTimeout, HandleClientDisconnect disconnect);

        /// <inheritdoc/>
        public abstract IArbitrator GetArbitrator(string name);

        /// <inheritdoc/>
        public abstract void SendServerArbitrationEvent(int destinationSessionId, byte[] message);

        /// <inheritdoc/>
        public abstract long GetUserIdForSession(int sessionId);

        /// <inheritdoc/>
        public abstract Character GetCharacterForArbitrationSession(int sessionId);

        /// <inheritdoc/>
        public abstract BadumnaId GetBadumnaIdForArbitrationSession(int sessionId);

        /// <inheritdoc/>
        public abstract MatchmakingAsyncResult BeginMatchmaking(
            HandleMatchmakingResult onCompletion,
            HandleMatchmakingProgress onProgress,
            MatchmakingOptions options);

        /// <inheritdoc/>
        public MatchmakingAsyncResult BeginMatchmaking(
            HandleMatchmakingResult onCompletion,
            MatchmakingOptions options)
        {
            return this.BeginMatchmaking(onCompletion, null, options);
        }

        /// <inheritdoc/>
        public MatchmakingAsyncResult BeginMatchmaking(
            HandleMatchmakingProgress onProgress,
            MatchmakingOptions options)
        {
            return this.BeginMatchmaking(null, onProgress, options);
        }

        /// <inheritdoc/>
        public MatchmakingAsyncResult BeginMatchmaking(MatchmakingOptions options)
        {
            return this.BeginMatchmaking(null, null, options);
        }

        /// <inheritdoc/>
        public abstract void AnnounceService(ServerType type);

        /// <inheritdoc/>
        void INetworkFacadeInternal.ShutdownHandler(object sender, EventArgs e)
        {
            EventHandler requestShutdown = this.requestShutdownDelegate;
            if (null != requestShutdown)
            {
                requestShutdown(sender, e);
            }
        }

        /// <summary>
        /// Called internally.
        /// </summary>
        /// <remarks>
        /// This protected method is for internal use only and may change or be removed in future versions.
        /// The <see cref="NetworkFacade"/> is not intended to be subclassed by application developers.
        /// </remarks>
        /// <param name="sender">For internal use only.</param>
        /// <param name="e">For internal use only.</param>
        /// <exclude/>
        protected void UpdateShutdownHandler(object sender, EventArgs e)
        {
            EventHandler requestShutdownForUpdate = this.requestShutdownForUpdateDelegate;
            if (null != requestShutdownForUpdate)
            {
                requestShutdownForUpdate(sender, e);
            }
        }

        /// <inheritdoc/>
        /// <remarks>
        /// This method defers its implementation to DoGetRecentActiveOpenPeers.
        /// </remarks>
        List<IPEndPoint> INetworkFacadeInternal.GetRecentActiveOpenPeers()
        {
            return this.DoGetRecentActiveOpenPeers();
        }

        /// <summary>
        /// Gets the recent active open peers.
        /// </summary>
        /// <remarks>
        /// This protected method is for internal use only and may change or be removed in future versions.
        /// The <see cref="NetworkFacade"/> is not intended to be subclassed by application developers.
        /// </remarks>
        /// <returns>A list of addresses of recent active open peers.</returns>
        /// <exclude/>
        protected abstract List<IPEndPoint> DoGetRecentActiveOpenPeers();

        /// <inheritdoc/>
        void INetworkFacadeInternal.AddInitialConnections(List<IPEndPoint> peers)
        {
            this.DoAddInitialConnections(peers);
        }

        /// <summary>
        /// Does the add initial connections.
        /// </summary>
        /// <remarks>
        /// This protected method is for internal use only and may change or be removed in future versions.
        /// The <see cref="NetworkFacade"/> is not intended to be subclassed by application developers.
        /// </remarks>
        /// <param name="endpoints">The known peers.</param>
        /// <exclude/>
        protected abstract void DoAddInitialConnections(List<IPEndPoint> endpoints);

        /// <inheritdoc/>
        public abstract StatisticsTracker CreateTracker(string serverAddress, int serverPort, TimeSpan interval, string initialPayload);

        /// <inheritdoc/>
        public StatisticsTracker CreateAndStartTracker(string serverAddress, int serverPort, TimeSpan interval, string initialPayload)
        {
            StatisticsTracker tracker = this.CreateTracker(serverAddress, serverPort, interval, initialPayload);
            tracker.Start();
            return tracker;
        }

        /// <summary>
        /// Configures the log using either the content of the environment variable BADUMNA_LOG_OVERRIDE (if set),
        /// otherwise uses the config passed in.
        /// </summary>
        /// <remarks>Only uses $BADUMNA_LOG_OVERRIDE in TRACE builds.</remarks>
        private static void ConfigureLogWithOverride(LoggerModule config, ITime timeKeeper)
        {
#if TRACE
            var logOverride = System.Environment.GetEnvironmentVariable("BADUMNA_LOG_OVERRIDE");
            if (logOverride != null)
            {
                Console.Error.WriteLine("Note: $BADUMNA_LOG_OVERRIDE is being used");
                var doc = new System.Xml.XmlDocument();
                doc.LoadXml(logOverride);
                config = new LoggerModule(doc.DocumentElement);
            }

            if (config == null)
            {
                return;
            }
#endif

            Logger.Initialize(timeKeeper);
            Logger.Configure(config);
        }
    }
}
