using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Text;

using Badumna.Arbitration;
using Badumna.Chat;
using Badumna.Controllers;
using Badumna.Core;
using Badumna.DistributedHashTable;
using Badumna.InterestManagement;
using Badumna.Matchmaking;
using Badumna.Multicast;
using Badumna.Overload;
using Badumna.Replication;
using Badumna.Security;
using Badumna.ServiceDiscovery;
using Badumna.SpatialEntities;
using Badumna.Streaming;
using Badumna.Transport;
using Badumna.Utilities;
using Badumna.Utilities.UserTracker;

using SpatialAutoreplicationManager = Badumna.Autoreplication.SpatialAutoreplicationManager;
using MatchAutoreplicationManager = Badumna.Autoreplication.MatchAutoreplicationManager;

namespace Badumna
{    using Badumna.Autoreplication;
    using Badumna.Transport.Connections;
    using Badumna.Configuration;

    class ProtocolStack
    {
        protected ITransport mTransportLayer;
        protected ConnectionTable mConnectionTable;
        protected ProtocolRoot mProtocolRoot;
        protected RouterMultiplexer mRouterMultiplexer;
        protected DhtFacadeCollection mDhtFacades;
        protected EntityManager mEntityManager;
        protected Autoreplication.Serialization.Manager serialization;
        protected TypeRegistry typeRegistry;
        protected SpatialAutoreplicationManager autoreplicationManager;
        protected MatchAutoreplicationManager matchAutoreplicationManager;
        protected RPCManager rpcManager;
        protected Match.EntityManager matchEntityManager;
        protected SpatialEntityManager mSpatialEntityManager;
        protected MulticastFacade mMulticastFacade;
        protected IInternalChatService mChatService;
        protected PresenceService mPresenceService;
        protected StreamingManager mStreamingManager;
        protected ControllerInitializer mControllerInitializer;
        protected RelayManager mRelayManager;
        protected DhtTransport mDhtTransport;
        protected SystemControlService mControlService;
        protected ForwardingManager mForwardingManager;
        protected ArbitrationManager mArbitrationManager;
        protected ServiceDiscoveryService mServiceDiscoveryService;
        protected ServiceManager mServiceManager;
        protected UserTracker mUserTracker;
        protected IComplaintForwarder complaintForwarder;

        /// <summary>
        /// Matchmaking manager instance.
        /// </summary>
        protected MatchmakingRequest matchmakingRequest;

        // Mono doesn't fire any NetworkChange or SystemEvents event, so when running on Mono, Badumna uses the 
        // following workarounds.  
        protected MonoSleepDetector sleepDetector;
        protected MonoAddressChangedDetector addressChangedDetector;

        protected Validation.IFacade validationFacade;

        protected Match.Facade matchFacade;

        protected BroadcastLayer mBroadcastLayer;
        protected DiscoveryLayer mDiscoveryService;

        private readonly IRandomNumberGenerator randomNumberGenerator;
        protected IRandomNumberGenerator RandomNumberGenerator { get { return this.randomNumberGenerator; } }
        
        /// <summary>
        /// The network connectivity reporter.
        /// </summary>
        public INetworkConnectivityReporter ConnectivityReporter { get; private set; }

        public TokenCache TokenCache { get; private set; }

        public IInterestManagementService InterestManagementService { get; private set; }

        public EntityManager EntityManager { get { return this.mEntityManager; } }
        public SpatialAutoreplicationManager AutoreplicationManager { get { return this.autoreplicationManager; } }
        public MatchAutoreplicationManager MatchAutoreplicationManager { get { return this.matchAutoreplicationManager; } }
        public TypeRegistry TypeRegistry { get { return this.typeRegistry; } }
        public RPCManager RPCManager { get { return this.rpcManager; } }
        public Match.EntityManager MatchEntityManager { get { return this.matchEntityManager; } }
        public Match.Facade Match { get { return this.matchFacade; } }
        public SpatialEntityManager SpatialEntityManager { get { return this.mSpatialEntityManager; } }
        public ProtocolRoot ProtocolRoot { get { return this.mProtocolRoot; } }
        public StreamingManager StreamingManager { get { return this.mStreamingManager; } }
        public ConnectionTable ConnectionTable { get { return this.mConnectionTable; } }
        public DhtFacadeCollection DhtFacades { get { return this.mDhtFacades; } }
        public ControllerInitializer ControllerIntializer { get { return this.mControllerInitializer; } }
        public ForwardingManager ForwardingManager { get { return this.mForwardingManager; } }
        public ArbitrationManager ArbitrationManager { get { return this.mArbitrationManager; } }
        public ServiceDiscoveryService ServiceDiscoveryService { get { return this.mServiceDiscoveryService; } }
        public ServiceManager ServiceManager { get { return this.mServiceManager; } }
        public UserTracker UserTracker { get { return this.mUserTracker; } }
        public BadumnaIdAllocator BadumnaIdAllocator { get { return this.badumnaIdAllocator; } }

        public Validation.IFacade ValidationManager { get { return this.validationFacade; } }
        /// <summary>
        /// The mechanism for discovering intial peers.
        /// </summary>
        /// <remarks>
        /// TODO: !!! This property should not have a setter.  It's current only used by NetworkSimulator
        ///           in a very hacky way to inform simulated nodes of each other.  Do not use the setter
        ///           for anything else except from within ProtocolStack itself.
        /// </remarks>
        public IPeerFinder PeerFinder { get; set; }

        internal ITransport TransportLayer { get { return this.mTransportLayer; } }

        private bool mIsInitialized;

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        protected NetworkEventQueue eventQueue;

        /// <summary>
        /// The queue to use for events that must be executed on the application thread.
        /// </summary>
        public SimpleEventQueue ApplicationEventQueue { get; private set; }

        /// <summary>
        /// Allocates unique BadumnaId*s.
        /// </summary>
        protected BadumnaIdAllocator badumnaIdAllocator;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        protected ITime timeKeeper;

        /// <summary>
        /// The configuration options for the system.
        /// </summary>
        protected Options options;

        /// <summary>
        /// Dependency injection container.
        /// </summary>
        protected Nest nest;

        /// <summary>
        /// The badumna feature policy
        /// </summary>
        protected FeaturePolicy featurePolicy;
        
        private IArbitrator matchmakingArbitrator;

        /// <summary>
        /// The queue to use for scheduling key encryption.
        /// </summary>
        private NetworkEventQueue encryptionQueue;

        /// <summary>
        /// Thread manager responsible for starting a fiber in different thread.
        /// </summary>
        private ThreadManager threadManager;

        /// <summary>
        /// Encryption queue fiber.
        /// </summary>
        private Fiber encryptionFiber;

        /// <summary>
        /// Initializes a new instance of the ProtocolStack class.
        /// </summary>
        /// <param name="options">The configuration options.</param>
        /// <param name="transportLayer">The transport layer to use.  Note that ProtocolStack.Initialize does not call Initialize on the transport layer.</param>
        /// <param name="eventQueue">The event queue to schedule events on.</param>
        /// <param name="timeKeeper">The time source.</param>
        /// <param name="connectivityReporter">Reports on network connectivity.</param>
        /// <param name="randomNumberGenerator">A random number generator.</param>
        public ProtocolStack(
            Options options,
            ITransport transportLayer,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            INetworkConnectivityReporter connectivityReporter,
            IRandomNumberGenerator randomNumberGenerator)
        {
            if (transportLayer == null)
            {
                throw new ArgumentNullException("transportLayer");
            }

            if (options == null)
            {
                throw new ArgumentNullException("options");
            }

            if (randomNumberGenerator == null)
            {
                throw new ArgumentNullException("randomNumberGenerator");
            }

            this.options = options;
            this.mTransportLayer = transportLayer;
            this.eventQueue = eventQueue;
            this.timeKeeper = timeKeeper;
            this.randomNumberGenerator = randomNumberGenerator;
            this.featurePolicy = new FeaturePolicy(this.options);

            this.ApplicationEventQueue = new SimpleEventQueue();

            this.TokenCache = new TokenCache(this.eventQueue, this.timeKeeper, this.ConnectivityReporter);

            this.ConnectivityReporter = connectivityReporter;

            this.badumnaIdAllocator = new BadumnaIdAllocator(
                this.TransportLayer,
                this.ConnectivityReporter,
                this.RandomNumberGenerator);

            this.nest = new Nest();
            this.nest.Set<ITime>(this.timeKeeper);

            this.encryptionQueue = new NetworkEventQueue();
        }

        public void CreateAndForge()
        {
            if (this.mIsInitialized)
            {
                return;
            }

            if (this.options.IsServiceDiscoveryEnabled)
            {
                Logger.TraceInformation(LogTag.Facade, "Service Manager is being created. ");
                this.mServiceManager = new ServiceManager(this.options.Arbitration, this.options.Overload, this.eventQueue, this.timeKeeper, this.ConnectivityReporter);
            }

            if (this.PeerFinder == null)
            {
                this.PeerFinder = new SeedPeerFinder(this.options.Connectivity.SeedPeers);
            }

            this.mConnectionTable = new ConnectionTable(this.mTransportLayer, this.mTransportLayer, this.PeerFinder, this.eventQueue, this.encryptionQueue, this.timeKeeper, this.TransportLayer, this.ConnectivityReporter, this.options.Connectivity.ApplicationName, this.TokenCache, this.nest.Construct);
            this.mProtocolRoot = this.mConnectionTable.ProtocolRoot;
            this.mRouterMultiplexer = new RouterMultiplexer(this.mProtocolRoot, this.eventQueue, this.timeKeeper, this.TransportLayer, this.ConnectivityReporter, this.ConnectionTable, this.nest.Construct);
            this.mForwardingManager = this.CreateForwardingManager();
            this.mEntityManager = this.CreateEntityManager();
            this.mArbitrationManager = this.CreateArbitrationManager();
            
            DhtFacade trusted = this.CreateDht(NamedTier.Trusted);
            DhtFacade openAddresses = this.CreateDht(NamedTier.OpenAddresses);
            DhtFacade allInclusive = this.CreateDht(NamedTier.AllInclusive);

            this.mDhtFacades = new DhtFacadeCollection(trusted, openAddresses, allInclusive);
            this.InterestManagementService = this.CreateIms();
            this.serialization = this.CreateSerializationManager();
            this.typeRegistry = this.CreateTypeRegistry();
            this.autoreplicationManager = this.CreateAutoreplicationManager();
            this.matchAutoreplicationManager = this.CreateMatchAutoreplicationManager();
            this.rpcManager = this.CreateRPCManager();
            this.matchEntityManager = this.CreateMatchEntityManager();
            this.mSpatialEntityManager = this.CreateSpatialEntityManager();
            this.autoreplicationManager.Initialize(this.mSpatialEntityManager);
            this.matchAutoreplicationManager.Initialize(this.mEntityManager);

            this.mMulticastFacade = this.CreateMulticastFacade();

            this.mPresenceService = this.CreatePresenceService();
            this.mChatService = this.CreateChatService(this.mPresenceService);
            this.mStreamingManager = this.CreateStreamingManager();
            this.mControllerInitializer = this.CreateControllerInitializer();
            this.mRelayManager = this.CreateRelayManager();
            this.complaintForwarder = this.CreateComplaintForwarder();
            this.mDhtTransport = this.CreateDhtTransport();
            this.mControlService = this.CreateControlService();
            this.mServiceDiscoveryService = this.CreateServiceDiscoveryService();
            this.mUserTracker = this.CreateUserTracker();

            this.validationFacade = this.CreateValidationFacade(this.mSpatialEntityManager);

            this.matchFacade = this.CreateMatchFacade();

            if (this.options.Connectivity.IsBroadcastEnabled)
            {
                this.mBroadcastLayer = this.CreateBroadcastLayer();
                if (this.mBroadcastLayer != null)
                {
                    this.mDiscoveryService = this.CreateDiscoveryService(this.mBroadcastLayer);
                }
            }

            this.CreateOverride();
            this.ForgeProtocol();
        }

        public void Initialize()
        {
            if (this.mIsInitialized)
            {
                return;
            }

            this.CreateAndForge();

            if (this.options.Connectivity.IsBroadcastEnabled)
            {
                this.mBroadcastLayer.Initialize(this.options.Connectivity.BroadcastPort);
            }

            // DGC: Initialize is no longer called on the transport layer here.  The HTTP tunnel code needs to know if UDP is blocked
            //      early on so it can choose the correct network facade, so transport layer initialization is now done in NetworkFacade.CreateFacade.
            //this.mTransportLayer.Initialize();
            
            if (null != this.mChatService)
            {
                this.mChatService.Initialize();
            }

            this.threadManager = new ThreadManager();

            this.encryptionQueue.IsRunning = true;
            this.encryptionFiber = new Fiber(this.encryptionQueue.Run);
            this.encryptionFiber.Name = "Encryption event queue loop";
            this.encryptionFiber.IsBackground = true;
            this.threadManager.Start(this.encryptionFiber);

            this.mConnectionTable.Initialize(this.mRelayManager, this.mDhtTransport);

            this.mIsInitialized = true;
        }

        private void ForgeProtocol()
        {
            if (this.mConnectionTable != null)
            {
                this.mConnectionTable.ForgeMethodList();
            }

            if (this.mProtocolRoot != null)
            {
                this.mProtocolRoot.ForgeMethodList();
            }

            foreach (DhtFacade facade in this.mDhtFacades)
            {
                if (facade != null)
                {
                    facade.ForgeMethodList();
                }
            }

            if (this.mEntityManager != null)
            {
                this.mEntityManager.ForgeMethodList();
            }

            if (this.mMulticastFacade != null)
            {
                this.mMulticastFacade.ForgeMethodList();
            }

            if (this.mDiscoveryService != null)
            {
                this.mDiscoveryService.ForgeMethodList();
            }
        }

        protected virtual void CreateOverride()
        {
        }

        public void Login()
        {
            if (!this.mIsInitialized)
            {
                throw new InvalidOperationException("ProtocolStack.Initialize must be called first");
            }

            if (this.TransportLayer.IsAddressKnownAndValid)
            {
                this.ConnectivityReporter.SetStatus(ConnectivityStatus.Online);

                // This must be in Login because we require our authorization token to join the network.
                // Attempt to initialize the tiers starting at the top - stopping at the first available.
                this.mConnectionTable.InitializeConnections();

                if (this.mEntityManager != null)
                {
                    this.mEntityManager.Reset();
                }

                if (this.mSpatialEntityManager != null)
                {
                    this.mSpatialEntityManager.Reset();
                }

                if (this.mDiscoveryService != null)
                {
                    // TODO: Is this necessary?  It was originally added in r2225.  It seems like it will always be triggered because of the network online event anyway.
                    this.mDiscoveryService.Start();
                }
            }
            else
            {
                this.ConnectivityReporter.SetStatus(ConnectivityStatus.Offline);
            }
        }

        public void Shutdown()
        {
            try
            {
                this.ShutdownOverride();

                if (this.validationFacade != null)
                {
                    // TODO: Rename this method.
                    this.validationFacade.Shutdown();
                }

                if (this.mTransportLayer != null)
                {
                    this.mTransportLayer.Shutdown();
                }

                if (this.mBroadcastLayer != null)
                {
                    this.mBroadcastLayer.Shutdown();
                }

                if (this.mChatService != null)
                {
                    this.mChatService.Shutdown();
                }

                if (this.mPresenceService != null)
                {
                    this.mPresenceService.Shutdown();
                }

                if (this.mServiceDiscoveryService != null)
                {
                    this.mServiceDiscoveryService.Shutdown();
                }

                if (this.ControllerIntializer != null)
                {
                    this.ControllerIntializer.Shutdown();
                }

                if (this.mForwardingManager != null)
                {
                    this.mForwardingManager.Shutdown();
                }

                if (this.mUserTracker != null)
                {
                    this.mUserTracker.Stop();
                }

                if (this.addressChangedDetector != null)
                {
                    this.addressChangedDetector.Stop();
                }

                if (this.encryptionQueue != null)
                {
                    this.encryptionQueue.Stop();
                }
            }
            catch (Exception e)
            {
                Logger.TraceException(LogLevel.Error, e, "Failed to cleanup protocol stack");
            }
        }

        /// <summary>
        /// Extension point for code to be run during shutdown.  This base version does nothing, so classes
        /// that override it don't need to call it.
        /// </summary>
        protected virtual void ShutdownOverride()
        {
        }

        protected virtual IComplaintForwarder CreateComplaintForwarder()
        {
            return null;
        }

        protected virtual RelayManager CreateRelayManager()
        {
            return null;
        }

        protected virtual DhtTransport CreateDhtTransport()
        {
            return null;
        }

        protected virtual DhtFacade CreateDht(NamedTier tier)
        {
            return null;
        }

        protected virtual IInterestManagementService CreateIms()
        {
            return null;
        }

        protected virtual EntityManager CreateEntityManager()
        {
            return null;
        }

        protected virtual Autoreplication.Serialization.Manager CreateSerializationManager()
        {
            return null;
        }

        protected virtual TypeRegistry CreateTypeRegistry()
        {
            return null;
        }

        protected virtual SpatialAutoreplicationManager CreateAutoreplicationManager()
        {
            return null;
        }

        protected virtual MatchAutoreplicationManager CreateMatchAutoreplicationManager()
        {
            return null;
        }

        protected virtual RPCManager CreateRPCManager()
        {
            return null;
        }        

        protected virtual Match.EntityManager CreateMatchEntityManager()
        {
            return null;
        }

        protected virtual SpatialEntityManager CreateSpatialEntityManager()
        {
            return null;
        }

        protected virtual MulticastFacade CreateMulticastFacade()
        {
            return null;
        }

        protected virtual PresenceService CreatePresenceService()
        {
            return null;
        }

        protected virtual ChatService CreateChatService(PresenceService presenceService)
        {
            return null;
        }

        protected virtual StreamingManager CreateStreamingManager()
        {
            return null;
        }

        protected virtual ControllerInitializer CreateControllerInitializer()
        {
            return null;
        }

        internal Character Character
        {
            get
            {
                var certificate = this.TokenCache.GetUserCertificateToken();
                return certificate == null ? null : certificate.Character;
            }
        }

        internal virtual IChatSession CreateChatSession()
        {
            return this.mChatService.CreateSession(this.TokenCache);
        }

        protected IArbitrator GetMatchmakingArbitrator()
        {
            if (this.matchmakingArbitrator == null)
            {
                this.matchmakingArbitrator = this.ArbitrationManager.GetArbitrator(MatchmakingOptions.ArbitrationServer);
            }

            return this.matchmakingArbitrator;
        }

        protected virtual MatchmakingRequest CreateMatchmakingRequest(
            HandleMatchmakingResult onCompletion,
            HandleMatchmakingProgress onProgress,
            MatchmakingOptions options)
        {
            throw new NotImplementedException("Matchmaking is not supported in this environment");
        }

        /// <summary>
        /// Begin a match-making request.
        /// </summary>
        /// <returns>Matchmaking request.</returns>
        internal virtual MatchmakingAsyncResult BeginMatchmaking(
            HandleMatchmakingResult onCompletion,
            HandleMatchmakingProgress onProgress,
            MatchmakingOptions options
            )
        {
            var req = this.CreateMatchmakingRequest(onCompletion, onProgress, options);
            return req.Begin();
        }
        
        /// <summary>
        /// Create matchmaking manager instance.
        /// </summary>
        /// <returns>Matchmaking manager.</returns>
        internal virtual MatchmakingRequest StartMatchmaking()
        {
            return null;
        }

        protected virtual BroadcastLayer CreateBroadcastLayer()
        {
            return null;
        }

        protected virtual DiscoveryLayer CreateDiscoveryService(IMessageProducer<TransportEnvelope> messageProducer)
        {
            return null;
        }

        protected virtual SystemControlService CreateControlService()
        {
            return null;
        }

        protected virtual ForwardingManager CreateForwardingManager()
        {
            return null;
        }

        protected virtual ArbitrationManager CreateArbitrationManager()
        {
            return null;
        }

        protected virtual Validation.IFacade CreateValidationFacade(ISpatialEntityManager spatialEntityManager)
        {
            return null;
        }

        protected virtual Match.Facade CreateMatchFacade()
        {
            return null;
        } 

        protected virtual ServiceDiscoveryService CreateServiceDiscoveryService()
        {
            return null;
        }

        protected virtual UserTracker CreateUserTracker()
        {
            return null;
        }

        internal void InspectLayers(ProtocolStackCrawler crawler)
        {
            this.CrawlLayer(this.mProtocolRoot, crawler);
            this.CrawlLayer(this.mEntityManager, crawler);
            this.CrawlLayer(this.mMulticastFacade, crawler);
            this.CrawlLayer(this.mDiscoveryService, crawler);
            foreach (TransportProtocol protocol in this.mRouterMultiplexer)
            {
                this.CrawlLayer(protocol, crawler);
            }
            foreach (DhtFacade dhtFacade in this.mDhtFacades)
            {
                this.CrawlLayer(dhtFacade, crawler);
            }         
        }

        private void CrawlLayer<T>(ProtocolComponent<T> layer, ProtocolStackCrawler crawler) where T : BaseEnvelope
        {
            if (layer != null)
            {
                crawler.Begin(layer);
                layer.Traverse(crawler);
                crawler.End(layer);
            }
        }

        public string Describe(IEnvelope envelope)
        {
            try
            {
                StringBuilder builder = new StringBuilder();

                Queue<IEnvelope> envelopes = new Queue<IEnvelope>();
                envelopes.Enqueue(envelope);

                while (envelopes.Count > 0)
                {
                    this.Describe(envelopes.Dequeue(),
                        delegate(string description, MethodInfo methodInfo, object[] args)
                        {
                            if (methodInfo != null)
                            {
                                builder.Append(methodInfo.Name).Append("(");
                                for (int i = 0; i < args.Length; i++)
                                {
                                    if (i > 0)
                                    {
                                        builder.Append(", ");
                                    }

                                    if (args[i] is IEnvelope)
                                    {
                                        envelopes.Enqueue((IEnvelope)args[i]);
                                        builder.Append(args[i].GetType().Name);
                                    }
                                    else
                                    {
                                        builder.Append(args[i].ToString());
                                    }
                                }
                                builder.AppendLine(")");
                            }
                            else if (description == "Header")
                            {
                                builder.Append("Header: ");
                                ConnectionHeader header = (ConnectionHeader)args[0];
                                if (header.IsInitial)
                                {
                                    builder.Append("Init ");
                                }
                                if (!header.IsReliable)
                                {
                                    builder.Append("Unrel ");
                                }
                                if (header.ShouldEnforceOrder)
                                {
                                    builder.Append("Ord ");
                                }
                                if (header.IsFragmented)
                                {
                                    builder.Append("Frag ");
                                }
                                builder.AppendFormat("Seq = {0}", header.SequenceNumber);
                                builder.AppendLine();
                            }
                            else
                            {
                                builder.AppendLine(description);
                            }
                        });
                }

                return builder.ToString().Trim();
            }
            catch (Exception e)  // Catch all because we don't want to prevent further processing
            {
                Logger.TraceException(LogLevel.Warning, e, "ProtocolStack.Describe failed");
                return "<Unknown - Describe Failed>";
            }
        }

        public void Describe(IEnvelope envelope, GenericCallBack<string, MethodInfo, object[]> describerAction)
        {
            int originalOffset = envelope.Message.ReadOffset;
            try
            {
                if (envelope is TransportEnvelope)
                {
                    TransportEnvelope copy = new TransportEnvelope((TransportEnvelope)envelope);

                    copy.Message.RemoveChecksum();

                    ConnectionHeader header = copy.Message.Read<ConnectionHeader>();

                    describerAction("Header", null, new object[] { header });

                    if (header.ContainsAcknowledgements)
                    {
                        int offset = copy.Message.ReadOffset;
                        copy.Message.ReadOffset = copy.Message.Length - 1;
                        byte numberOfAcks = copy.Message.ReadByte();
                        int byteCount = (numberOfAcks * 2) + 1;
                        copy.Message.ReadOffset = copy.Message.Length - byteCount;

                        StringBuilder acks = new StringBuilder("Ack ");

                        for (int i = 0; i < numberOfAcks; i++)
                        {
                            if (i % 2 == 0)
                            {
                                acks.Append(" ");
                            }
                            else
                            {
                                acks.Append("-");
                            }

                            acks.Append(copy.Message.Read<CyclicalID.UShortID>().ToString());
                        }

                        copy.Message.DropLastNBytes(byteCount);
                        copy.Message.ReadOffset = offset;

                        describerAction(acks.ToString(), null, null);
                    }

                    if (header.IsConnectionless)
                    {
                        this.ConnectionTable.Parser.Describe(copy, describerAction);
                    }
                    else
                    {
                        this.ConnectionTable.ProtocolRoot.Parser.Describe(copy, describerAction);
                    }
                }
                else if (envelope is EntityEnvelope)
                {
                    this.EntityManager.Parser.Describe(envelope, describerAction);
                }
                else if (envelope is DhtEnvelope)
                {
                    this.DhtFacades[0].Parser.Describe(envelope, describerAction);  // TODO: ! Do all the tiers have the same registered methods?
                }
                else
                {
                    describerAction(envelope.GetType().Name, null, null);
                }
            }
            finally
            {
                envelope.Message.ReadOffset = originalOffset;
            }
        }
    }
}
