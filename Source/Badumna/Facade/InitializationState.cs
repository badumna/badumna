﻿//------------------------------------------------------------------------------
// <copyright file="InitializationState.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace Badumna
{
    /// <summary>
    /// The state of the network facade initialization process.
    /// </summary>
    public enum InitializationState
    {
        /// <summary>
        /// The initialization has just started.
        /// </summary>
        Uninitialized,

        /// <summary>
        /// The initialization process has completed.  Both successful and
        /// unsuccessful initialization end in this state.
        /// </summary>
        Complete,
    }
}
