using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

namespace Badumna
{
    /// <summary>
    /// An extension interface used for diagnostics
    /// </summary>
    internal interface IDiagnosticsExtension
    {
        /// <summary>
        /// </summary>
        /// <param name="facadeInternal"></param>
        void Initialize(INetworkFacadeInternal facadeInternal);

        /// <summary>
        /// </summary>
        /// <param name="serverEndPoint"></param>
        void Connect(IPEndPoint serverEndPoint);

        /// <summary>
        /// </summary>
        void Disconnect();

        /// <summary>
        /// </summary>
        void Shutdown();
    }
}
