﻿//------------------------------------------------------------------------------
// <copyright file="INetworkFacade.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.IO;
using Badumna.Arbitration;
using Badumna.Chat;
using Badumna.Controllers;
using Badumna.DataTypes;
using Badumna.Matchmaking;
using Badumna.Security;
using Badumna.ServiceDiscovery;
using Badumna.SpatialEntities;
using Badumna.Streaming;
using Badumna.Utilities;
using Badumna.Validation;

namespace Badumna
{
    using Badumna.Autoreplication.Serialization;

    /// <summary>
    /// A delegate used to indicate if the network has changed status from offline and online.
    /// </summary>
    public delegate void ConnectivityStatusDelegate();

    /// <summary>
    /// A delegate used to indicate that the public IP address, port or the NAT type has changed. 
    /// </summary>
    public delegate void PublicAddressChangedDelegate();

    /// <summary>
    /// A delegate signature for a factory method that creates a network facade.
    /// </summary>
    /// <param name="options">Configuration options.</param>
    /// <returns>A new network facade.</returns>
    public delegate INetworkFacade NetworkFacadeFactory(Options options);

    /// <summary>
    /// This interface defines the primary interface to Badumna.
    /// </summary>
    public interface INetworkFacade
    {
        /// <summary>
        /// This event is invoked when the network becomes unavailable. (Packets can not be sent)
        /// </summary>
        event ConnectivityStatusDelegate OfflineEvent;

        /// <summary>
        /// This event is called when the network becomes available. (Packets can be sent)
        /// </summary>
        event ConnectivityStatusDelegate OnlineEvent;

        /// <summary>
        /// This event is called when the network address, port or the NAT type has changed.
        /// It is invoked on the application thread during the next call to <see cref="ProcessNetworkState"/>.
        /// </summary>
        event PublicAddressChangedDelegate AddressChangedEvent;

        /// <summary>
        /// Indicates that the network layer requires the application to shutdown.
        /// </summary>
        event EventHandler RequestShutdown;

        /// <summary>
        /// Indicates that an update is available and the application should restart to apply them.
        /// </summary>
        event EventHandler RequestShutdownForUpdate;

        /// <summary>
        /// Gets the average packet loss rate.
        /// </summary>
        /// <remarks>When a tunneled connection is in use this property will return -1.</remarks>
        /// <value>The average packet loss rate.</value>        
        double AveragePacketLossRate { get; }

        /// <summary>
        /// Gets the estimated inbound bytes/second; the rate that we're receiving data from other peers.  This estimate
        /// does not include minor local traffic (such as local broadcast messages for discovery) or diagnostic traffic.
        /// </summary>
        /// <remarks>When a tunneled connection is in use this property will return -1.</remarks>
        double InboundBytesPerSecond { get; }

        /// <summary>
        /// Gets a value indicating whether the connection to the network is being tunnelled over HTTP.
        /// </summary>
        bool IsTunnelled { get; }

        /// <summary>
        /// Gets the current state of the initialization process.  Initialization begins as soon as
        /// <see cref="NetworkFacade.Create(string)"/> or <see cref="NetworkFacade.Create(Options)"/> is called.  Once initialization has completed this
        /// property will return <see cref="InitializationState.Complete"/>.  During initialization
        /// this property may transistion through other values of the <see cref="InitializationState"/>
        /// enumeration; see its documentation for details.
        /// </summary>
        InitializationState InitializationProgress { get; }

        /// <summary>
        /// Gets a value indicating whether login has succeeded.
        /// </summary>
        bool IsLoggedIn { get; }

        /// <summary>
        /// Gets a value indicating whether the local peer is fully connected to the network.
        /// </summary>
        /// <remarks>
        /// This property is always <c>false</c> for a tunnelled connection.
        /// </remarks>
        bool IsFullyConnected { get; }

        /// <summary>
        /// Gets a value indicating whether the network is unavailable or the facade has been shut down.
        /// </summary>
        bool IsOffline { get; }

        /// <summary>
        /// Gets a value indicating whether if the network is available and initialized.
        /// </summary>
        bool IsOnline { get; }

        /// <summary>
        /// Gets the maximum packet loss rate.
        /// </summary>
        /// <remarks>When a tunneled connection is in use this property will return -1.</remarks>
        /// <value>The maximum packet loss rate.</value>
        double MaximumPacketLossRate { get; }

        /// <summary>
        /// Gets the maximum send limit on any given connection in bytes per second.
        /// </summary>
        /// <remarks>When a tunneled connection is in use this property will return -1.</remarks>
        /// <value>The maximum send limit in bytes per second.</value>
        double MaximumSendLimitBytesPerSecond { get; }

        /// <summary>
        /// Gets the estimated outbound bytes/second; the rate that we're sending data to other peers.  This estimate
        /// does not include minor local traffic (such as local broadcast messages for discovery) or diagnostic traffic.
        /// </summary>
        /// <remarks>When a tunneled connection is in use this property will return -1.</remarks>
        double OutboundBytesPerSecond { get; }

        /// <summary>
        /// Gets the serialization manager for registering replicable types.
        /// </summary>
        TypeRegistry TypeRegistry { get; }

        /// <summary>
        /// Gets the RPC manager for making RPC calls to remote entities.
        /// </summary>
        RPCManager RPCManager { get; }

        /// <summary>
        /// Gets the StreamingManager
        /// </summary>
        StreamingManager Streaming { get; }

        /// <summary>
        /// Gets the validation manager.
        /// </summary>
        IFacade ValidationFacade { get; }

        /// <summary>
        /// Gets the Match facade.
        /// </summary>
        Match.Facade Match { get; }

        /// <summary>
        /// Gets the total send limit in bytes per second. 
        /// </summary>
        /// <remarks>When a tunneled connection is in use this property will return -1.</remarks>
        /// <value>The total send limit in bytes per second.</value>
        double TotalSendLimitBytesPerSecond { get; }

        /// <summary>
        /// Gets the chat session instance associated with the logged in character.
        /// </summary>
        /// <remarks>
        /// Accessing this property will fail if the user is not logged in with a character (see <see cref="INetworkFacade.Login(string)"/>).
        /// </remarks>
        /// <returns>The new chat session</returns>
        IChatSession ChatSession { get; }

        /// <summary>
        /// Gets the logged in character.
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <returns>The logged-in character, or null if there is no logged in character.</returns>
        Character Character { get; }

        /// <summary>
        /// Announces the service specified description. This method should only be called by hosted
        /// services such as on Overload/Arbitration servers. Game client should never call this method.
        /// </summary>
        /// <param name="type">The type of the service.</param>
        /// <exception cref="NotSupportedException">This method is not supported on a tunnelled connection.</exception>
        void AnnounceService(ServerType type);

        /// <summary>
        /// Indicates that the given entity has a state change that need to be propagated to interested peers.
        /// </summary>
        /// <remarks>If the entity has not been registered on a scene this method call will be silently ignored.</remarks>
        /// <param name="localEntity">The entity with changed state.</param>
        /// <param name="changedPartIndex">The index of the part of state that has changed.</param>
        void FlagForUpdate(ISpatialOriginal localEntity, int changedPartIndex);

        /// <summary>
        /// Indicates that the given entity has state changes that need to be propagated to interested peers.
        /// </summary>
        /// <remarks>If the entity has not been registered on a scene this method call will be silently ignored.</remarks>
        /// <param name="localEntity">The entity with changed state.</param>
        /// <param name="changedParts">A BooleanArray with bits set indicating which parts have changed.</param>
        void FlagForUpdate(ISpatialOriginal localEntity, BooleanArray changedParts);

        /// <summary>
        /// Get an <see cref="IArbitrator"/> instance that can be used to send arbitration events to the
        /// aribtration server.  Used by arbitration clients.
        /// </summary>
        /// <returns>The <see cref="IArbitrator"/> instance</returns>
        /// <param name="name">The name identifying the arbitration server.</param>
        IArbitrator GetArbitrator(string name);

        /// <summary>
        /// Returns the destination position for an <see cref="IDeadReckonable"/>.
        /// </summary>
        /// <remarks>
        /// This can be used in combination
        /// with the <see cref="IDeadReckonable"/>'s current Position and Velocity to determine the state of the
        /// dead reckoning.  With reference to the velocity, if the current position is before
        /// the destination then the dead reckoner is interpolating (smoothing).  If the current
        /// position is after the destination then the dead reckoner is extrapolating.
        /// </remarks>
        /// <param name="deadReckonable">The dead reckonable entity to query.</param>
        /// <returns>The current destination of the dead reckonable entity.</returns>
        Vector3 GetDestination(IDeadReckonable deadReckonable);

        /// <summary>
        /// Gets the current network status.
        /// </summary>
        /// <returns>The network status.</returns>
        /// <exception cref="NotSupportedException">This method is not supported on a tunnelled connection.</exception>
        NetworkStatus GetNetworkStatus();

        /// <summary>
        /// Gets the userId associated with the given <paramref name="sessionId"/>.  This is only valid on an
        /// arbitration server peer (arbitration clients only have one arbitration session
        /// per arbitration server).  If the <paramref name="sessionId"/> is unknown this function returns <c>-1</c>.
        /// </summary>
        /// <param name="sessionId">The session id, as passed to the registered <see cref="HandleClientMessage"/> delegate.</param>
        /// <returns>The user id.</returns>
        /// <exception cref="NotSupportedException">This method is not supported on a tunnelled connection.</exception>
        long GetUserIdForSession(int sessionId);

        /// <summary>
        /// Gets the character associated with the given <paramref name="sessionId"/>.  This is only valid on an
        /// arbitration server peer (arbitration clients only have one arbitration session
        /// per arbitration server).  If the <paramref name="sessionId"/> is unknown this function returns <c>null</c>.
        /// </summary>
        /// <param name="sessionId">The session id, as passed to the registered <see cref="HandleClientMessage"/> delegate.</param>
        /// <returns>The character.</returns>
        /// <exception cref="NotSupportedException">This method is not supported on a tunnelled connection.</exception>
        Character GetCharacterForArbitrationSession(int sessionId);
        
        /// <summary>
        /// Gets a <see cref="BadumnaId"/> identifying the peer associated with the given <paramref name="sessionId"/>.
        /// This is only valid on an arbitration server peer (arbitration clients only have one arbitration session
        /// per arbitration server).  If the <paramref name="sessionId"/> is unknown this function returns <see cref="BadumnaId.None"/>.
        /// </summary>
        /// <param name="sessionId">The session id, as passed to the registered <see cref="HandleClientMessage"/> delegate.</param>
        /// <returns>A <see cref="BadumnaId"/> identifying the peer, or <see cref="BadumnaId.None"/> if the session was not found.</returns>
        /// <exception cref="NotSupportedException">This method is not supported on a tunnelled connection.</exception>
        BadumnaId GetBadumnaIdForArbitrationSession(int sessionId);

        /// <summary>
        /// Begin a matchmaking request.
        /// </summary>
        /// <param name="onCompletion">The completion callback.</param>
        /// <param name="onProgress">The progress callback.</param>
        /// <param name="options">The matchmaking options.</param>
        /// <returns>
        /// Returns a MatchmakingAsyncResult object.
        /// </returns>
        MatchmakingAsyncResult BeginMatchmaking(
            HandleMatchmakingResult onCompletion,
            HandleMatchmakingProgress onProgress,
            MatchmakingOptions options);

        /// <summary>
        /// Begin a matchmaking request.
        /// </summary>
        /// <param name="onCompletion">The completion callback.</param>
        /// <param name="options">The matchmaking options.</param>
        /// <returns>
        /// Returns a MatchmakingAsyncResult object.
        /// </returns>
        MatchmakingAsyncResult BeginMatchmaking(
            HandleMatchmakingResult onCompletion,
            MatchmakingOptions options);

        /// <summary>
        /// Begin a matchmaking request.
        /// </summary>
        /// <param name="onProgress">The progress callback.</param>
        /// <param name="options">The matchmaking options.</param>
        /// <returns>
        /// Returns a MatchmakingAsyncResult object.
        /// </returns>
        MatchmakingAsyncResult BeginMatchmaking(
            HandleMatchmakingProgress onProgress,
            MatchmakingOptions options);

        /// <summary>
        /// Begin a matchmaking request.
        /// </summary>
        /// <param name="options">The matchmaking options.</param>
        /// <returns>
        /// Returns a MatchmakingAsyncResult object.
        /// </returns>
        MatchmakingAsyncResult BeginMatchmaking(MatchmakingOptions options);

        /// <summary>
        /// Joins a scene.
        /// </summary>
        /// <remarks>
        /// The create and remove entity delegates are called upon the arrival of a new entity and the 
        /// departure of an old entity respectively.  Only entities which are in the same scene and
        /// in proximity to a locally registered entity will be passed to these delegates.
        /// </remarks>
        /// <param name="sceneName">The unique name identifying the scene.</param>
        /// <param name="createEntityDelegate">Called when a new entity needs to be instantiated into the scene</param>
        /// <param name="removeEntityDelegate">Called when an entity in the scene departs.</param>
        /// <returns>An instance of NetworkScene representing the scene.</returns>
        NetworkScene JoinScene(string sceneName, CreateSpatialReplica createEntityDelegate, RemoveSpatialReplica removeEntityDelegate);

        /// <summary>
        /// Joins a mini scene.
        /// <para/>
        /// A mini scene acts much like a regular scene, except that Badumna performs no interest management or position tracking,
        /// rather it assumes that all entities in a mini scene are close enough to receive updates for every other peer in the scene.
        /// <para/>
        /// This reduces overhead for scenes that are small (in number of players or physical dimensions), but does not
        /// scale well for larger scenes, where Badumna's interest management saves on bandwidth and computation by only
        /// replicating to / from entities that are nearby.
        /// </summary>
        /// <param name="sceneName">The unique name identifying the scene.</param>
        /// <param name="createEntityDelegate">Called when a new entity needs to be instantiated into the scene</param>
        /// <param name="removeEntityDelegate">Called when an entity in the scene departs.</param>
        /// <returns>An instance of NetworkScene representing the mini scene.</returns>
        NetworkScene JoinMiniScene(string sceneName, CreateSpatialReplica createEntityDelegate, RemoveSpatialReplica removeEntityDelegate);

        /// <summary>
        /// Performs login-time initializations, and indicates that no authorization system is being used.
        /// </summary>
        /// <returns><c>true</c> if login was successful, <c>false</c> otherwise.</returns>
        bool Login();

        /// <summary>
        /// Performs login-time initializations, and indicates that no authorization system is being used.
        /// </summary>
        /// <remarks>
        /// Specifies a character name to be used by character-based APIs (e.g chat).
        /// Note that you need to use the overload of this method that takes an IdentityProvider
        /// if you wish to ensure that the character name is unique and belongs to the active user.
        /// </remarks>
        /// /// <param name="characterName">The character name to login as.</param>
        /// <returns><c>true</c> if login was successful, <c>false</c> otherwise.</returns>
        bool Login(string characterName);

        /// <summary>
        /// Performs login-time initializations, and indicates that no authorization system is being used.
        /// </summary>
        /// <remarks>
        /// Specifies a character name to be used by character-based APIs (e.g chat).
        /// Specifies a string containing xml for a key pair to be used for secure communications.
        /// This key pair can be automatically generated by using the overload that does not include
        /// this parameter. Use this overload on mobile platforms where generating a key pair is slow.
        /// The key pair can be generated by calling Badumna.Security.UnverifiedIdentityProvider.GenerateKeyPair()
        /// and this key pair can be cached for re-use to avoid having to generate it each time.
        /// Note that you need to use the overload of this method that takes an IdentityProvider
        /// if you wish to ensure that the character name is unique and belongs to the active user.
        /// </remarks>
        /// <param name="characterName">The character name to login as.</param>
        /// <param name="keyPairXml">A string containing xml for a key pair to be used for secure communication.</param>
        /// <returns><c>true</c> if login was successful, <c>false</c> otherwise.</returns>
        bool Login(string characterName, string keyPairXml);

        /// <summary>
        /// Logs in to the network, specifing a delegate that provider authorization tokens
        /// </summary>
        /// <remarks>
        /// Specifies a callback for the system to retrieve authorization tokens and performs
        /// login-time initializations.
        /// </remarks>
        /// <param name="identityProvider">An implementation of an identity provider that will return any required authorization tokens.  
        /// Calls to the identity provider may be made from a different thread to that which called Login.  Tokens returned by the delegate
        /// become owned by the network library and should no longer be accessed.  The provider must be usable until Shutdown is called.
        /// </param>
        /// <returns><c>true</c> if login was successful, <c>false</c> otherwise.</returns>
        bool Login(IIdentityProvider identityProvider);

        /// <summary>
        /// Performs any regular processing in Badumna that requires synchronisation with the application.
        /// </summary>
        /// <remarks>
        /// <para>
        /// This function must be called regularly by the application so that network events requiring
        /// sychronisation can be processed (e.g. it might be called once per frame rendered).
        /// </para><para>
        /// As a rule Badumna will not call application code asynchronously.  Particularly, all calls
        /// to methods or properties on Badumna interfaces implemented by the application will be made
        /// from within ProcessNetworkState().  Calls to delegates registered with Badumna such as
        /// the CreateEntityDelegate will also only be made from ProcessNetworkState().  Unless otherwise
        /// specified, the only application code that Badumna may invoke asynchronously is delegates that are subscribed
        /// to events (e.g. OfflineEvent, OnlineEvent).
        /// </para>
        /// </remarks>
        void ProcessNetworkState();

        /// <summary>
        /// Registers the handler that will be called when this peer receives an arbitration event
        /// from another peer.  Used by the arbitration server.
        /// </summary>
        /// <param name="handler">The arbitration event handler.</param>
        /// <param name="disconnectTimeout">The amount of time with no received events before a client will disconnected.</param>
        /// <param name="disconnect">The client disconnection handler.</param>
        /// <exception cref="NotSupportedException">This method is not supported on a tunnelled connection.</exception>
        void RegisterArbitrationHandler(HandleClientMessage handler, TimeSpan disconnectTimeout, HandleClientDisconnect disconnect);

        /// <summary>
        /// Registers limitations on replicable entities so Badumna can come up with optimized setting for the game environment.
        /// Cannot be called after joining a scene.
        /// </summary>
        /// <param name="maxInterestRadius">The largest area of interest radius of any replicable entity (or entity radius if that is larger).</param>
        /// <param name="maxEntitySpeed">The maximum speed any replicable entity will move at.</param>
        /// <remarks>Can be called multiple times, and system will use maximum values passed.</remarks>
        void RegisterEntityDetails(float maxInterestRadius, float maxEntitySpeed);

        /// <summary>
        /// Sends a single message directly to the controlling instance of this entity.
        /// </summary>
        /// <remarks>
        /// Events are intended to be used for state changes on entities that are infrequent so they are 
        /// sent reliably and are guaranteed to be applied in the same order as they are sent.
        /// When the message arrives the remote ISpatialOriginal.HandleEvent() method will be called 
        /// with the given arguments.
        /// </remarks>
        /// <param name="replica">The entity whose controlling instance should receive the event.</param>
        /// <param name="eventData">The event specific data to send.</param>
        void SendCustomMessageToOriginal(ISpatialReplica replica, MemoryStream eventData);

        /// <summary>
        /// Sends a single message directly to all remote replicas of the specified entity.
        /// </summary>
        /// <remarks>
        /// Events are intended to be used for state changes on entities that are infrequent so they are 
        /// sent reliably and are guaranteed to be applied in the same order as they are sent.
        /// When the message arrives the remote ISpatialReplica.HandleEvent() method will be called 
        /// with the given arguments.
        /// </remarks>
        /// <param name="original">The entity whose replicas should receive the event.</param>
        /// <param name="eventData">The event specific data to send.</param>
        void SendCustomMessageToRemoteCopies(ISpatialOriginal original, MemoryStream eventData);

        /// <summary>
        /// Sends an event from the arbitration server to the client identified by <paramref name="destinationSessionId"/>.
        /// This session id must already be known to the arbitration server (i.e. the client must always
        /// initiate any arbitration session).
        /// </summary>
        /// <param name="destinationSessionId">Identifies the session to send the event to.</param>
        /// <param name="message">The payload to send.</param>
        /// <exception cref="NotSupportedException">This method is not supported on a tunnelled connection.</exception>
        void SendServerArbitrationEvent(int destinationSessionId, byte[] message);

        /// <summary>
        /// Blocking shutdown.  This explicitly just calls Shutdown(true).
        /// </summary>
        void Shutdown();

        /// <summary>
        /// Shuts down the facade.
        /// </summary>
        /// <remarks>
        /// Should be called prior to application exit.
        /// If 'blockUntilComplete' is false this method will not block,
        /// but a thread may linger briefly while the network stack performs its finalization.
        /// Shutdown may try to perform application level operations that have been queued since the last 
        /// ProcessNetworkState() call. For this reason, Shutdown() should be called prior to other application shutdown operations.
        /// </remarks>
        /// <param name="blockUntilComplete">If true, this method will not return until shutdown has completed.</param>
        void Shutdown(bool blockUntilComplete);

        /// <summary>
        /// Forces the IDeadReckonable's position and velocity to the current destination
        /// values and begins extrapolation.
        /// </summary>
        /// <param name="deadReckonable">The dead reckonable entity.</param>
        void SnapToDestination(IDeadReckonable deadReckonable);

        /// <summary>
        /// Start a controller of the given type on the network.
        /// The controller must have a constructor that takes a single string argument.
        /// </summary>
        /// <typeparam name="T">The type of the controller. Must be derived from DistributedController.</typeparam>
        /// <param name="controllerUniqueName">The unique name of the controller.</param>
        /// <exception cref="MissingConstructorException">Thrown when the given type does not have 
        /// a constructor that takes a single string argument</exception>
        void StartController<T>(string controllerUniqueName) where T : DistributedController;

        /// <summary>
        /// Starts a controller of the given type on the network.
        /// </summary>
        /// <typeparam name="T">The type of the controller. Must be derived from DistributedController.</typeparam>
        /// <param name="sceneName">Name of the scene.</param>
        /// <param name="controllerName">Name of the controller.</param>
        /// <param name="max">The max number of controllers of this type on the network.</param>
        /// <returns>the unique name of the controller.</returns>
        /// <exception cref="MissingConstructorException">Thrown when the given type does not have
        /// a constructor that takes a single string argument</exception>
        string StartController<T>(string sceneName, string controllerName, ushort max) where T : DistributedController;

        /// <summary>
        /// Stops the controller of the given type.
        /// </summary>
        /// <typeparam name="T">The type of the controller. Must be derived from DistributedController</typeparam>
        /// <param name="controllerUniqueName">The unique name of the controller.</param>
        void StopController<T>(string controllerUniqueName) where T : DistributedController;

        /// <summary>
        /// Creates a <see cref="StatisticsTracker"/> which periodically
        /// sends packets to a tracking server to estimate the number of current users on the
        /// network.
        /// </summary>
        /// <remarks>
        /// <para>
        /// The packets sent are unreliable and may be dropped by firewalls, or due to congestion,
        /// or for a number of other reasons.  As such the tracking server can only provide an
        /// estimate of the active user count.
        /// </para><para>
        /// If a user is logged in when the tracker is created then the user's id will be sent in
        /// the tracking packet.  If a new user logs in then the user id should be updated by calling
        /// <see cref="StatisticsTracker.SetUserID(long)"/>, or by stopping this
        /// tracker and starting a new tracker.
        /// </para>
        /// </remarks>
        /// <seealso cref="CreateAndStartTracker(string, int, TimeSpan, string)"/>
        /// <param name="serverAddress">The address of the tracking server.</param>
        /// <param name="serverPort">The port on the tracking server.</param>
        /// <param name="interval">The interval between sending packets.</param>
        /// <param name="initialPayload">The initial payload data.</param>
        /// <returns>The <see cref="StatisticsTracker"/> instance which can be used
        /// to start and stop tracking, and to update the payload.</returns>
        /// <exception cref="NotSupportedException">This method is not supported on a tunnelled connection.</exception>
        StatisticsTracker CreateTracker(string serverAddress, int serverPort, TimeSpan interval, string initialPayload);

        /// <summary>
        /// A convenience method that creates a <see cref="StatisticsTracker"/> and
        /// starts it tracking.
        /// </summary>
        /// <seealso cref="CreateTracker(string, int, TimeSpan, string)"/>
        /// <param name="serverAddress">The address of the tracking server.</param>
        /// <param name="serverPort">The port on the tracking server.</param>
        /// <param name="interval">The interval between sending packets.</param>
        /// <param name="initialPayload">The initial payload data.</param>
        /// <returns>The <see cref="StatisticsTracker"/> instance which can be used
        /// to start and stop tracking, and to update the payload.</returns>
        /// <exception cref="NotSupportedException">This method is not supported on a tunnelled connection.</exception>
        StatisticsTracker CreateAndStartTracker(string serverAddress, int serverPort, TimeSpan interval, string initialPayload);
    }
}
