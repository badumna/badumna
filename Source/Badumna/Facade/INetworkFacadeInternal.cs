﻿//-----------------------------------------------------------------------
// <copyright file="INetworkFacadeInternal.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Net;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Transport;

namespace Badumna
{
    using Badumna.SpatialEntities;
    using Badumna.Utilities;

    /// <summary>
    /// This interface is for methods of NetworkFacade that are marked internal.
    /// </summary>
    /// <remarks>
    /// INetworkFacade defines the public interface of the network facade and facilitates creating a mock network
    /// facade for unit testing.  There are some methods on NetworkFacade that have internal accessibility.  These
    /// methods cannot be added to INetworkFacade because that would require making them public.  The methods also
    /// need to be mocked for the unit tests and hence this interface was born.
    /// </remarks>
    internal interface INetworkFacadeInternal
    {
        /// <summary>
        /// Triggered at the end of an ProcessNetworkState() call
        /// </summary>
        event EventHandler ProcessingNetworkState;

        /// <summary>
        /// Gets the event queue used by the facade.
        /// </summary>
        NetworkEventQueue EventQueue { get; }

        /// <summary>
        /// Gets the options used by the facade.
        /// </summary>
        Options Options { get; }

        /// <summary>
        /// Gets the time keeper used by the facade.
        /// </summary>
        ITime TimeKeeper { get; }

        /// <summary>
        /// Invokes the RequestShutdown event
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="e">Event arguments</param>
        void ShutdownHandler(object sender, EventArgs e);

        /// <summary>
        /// I think this method was added for an application developed as part of a student project.  Need
        /// to look at the code to find out what it does.
        /// </summary>
        /// <param name="index">The index id</param>
        /// <param name="node">The application node</param>
        /// <returns>A boolean value</returns>
        bool RegisterApplicationNode(BadumnaId index, ApplicationProtocolNode node);

        /// <summary>
        /// Gets the recent active open peers.
        /// </summary>
        /// <returns>A list of addresses of recent active open peers.</returns>
        List<IPEndPoint> GetRecentActiveOpenPeers();

        /// <summary>
        /// Adds the initial connections to connect the local peer to an existing network.
        /// </summary>
        /// <param name="peers">The known peers.</param>
        void AddInitialConnections(List<IPEndPoint> peers);

        /// <summary>
        /// Generates a unique BadumnaId.
        /// </summary>
        /// <returns>The new BadumnaId</returns>
        BadumnaId GenerateId();

        /// <summary>
        /// Flag a full update from an original entity to all its replicas.
        /// </summary>
        /// <param name="localEntity">The entity to trigger an update for.</param>
        void FlagFullUpdate(ISpatialEntity localEntity);

        /// <summary>
        /// Gets a snapshot of state information from the network stack for diagnostic purposes.
        /// </summary>
        /// <remarks>
        /// This API is in development and is currently only for internal use.
        /// It will probably change sigificantly.
        /// WARNING: This method is currently not thread-safe!
        /// TODO: Figure out what kind of thread safety this method should support.
        /// </remarks>
        /// <returns>A dictionary containing the diagnostic information.</returns>
        DiagnosticInfo GetDiagnosticInformation();
    }
}
