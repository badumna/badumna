using UnityEngine;
using System.Collections;
using UnityEditor;

[InitializeOnLoad]
public class BadumnaCloudEditor : EditorWindow 
{
    private const string GettingStartedUrl = "https://cloud.badumna.com/docs/get-started.html";

    private const string CloudDashboardUrl = "https://cloud.badumna.com/cp/apps/";

    private const string TermsConditionsUrl = "https://cloud.badumna.com/terms-and-conditions";

    private const string ForumUrl = "http://www.scalify.com/forum/forumdisplay.php?6-Badumna-Cloud";

    private const string BadumnaCloudSettingsPath = "Assets/Badumna/Resources/BadumnaCloudSettings.asset";

    private static Texture2D BadumnaCloudIcon;

    private static BadumnaCloudSettings currentSettings;

    private string emailAddress = string.Empty;

    private string password = string.Empty;

    private bool agreeTermsAndConditions;

    private string applicationId = string.Empty;

    private AccountManager client = new AccountManager();

    private Page currentPage = Page.MainMenu;

    private enum Page
    {
        MainMenu,

        SignUp,

        Setup
    }

    static BadumnaCloudEditor()
    {
        if(!System.IO.File.Exists(BadumnaCloudSettingsPath))
        {
            EditorApplication.update += OnUpdate;        
        }
        EditorApplication.playmodeStateChanged += PlayModeStateChanged;
        BadumnaCloudIcon = AssetDatabase.LoadAssetAtPath("Assets/Badumna/Resources/cloud-badumna.png", typeof(Texture2D)) as Texture2D;
    }

    public static BadumnaCloudSettings CurrentSettings
    {
        get
        {
            if (currentSettings == null)
            {
                // reload current settings
                UnityEngine.Object[] settingFiles = Resources.LoadAll("BadumnaCloudSettings", typeof(BadumnaCloudSettings));

                // should only have one setting file.
                if (settingFiles != null && settingFiles.Length == 1)
                {
                    currentSettings = settingFiles[0] as BadumnaCloudSettings;
                    return currentSettings;
                }

                currentSettings = CreateInstance("BadumnaCloudSettings") as BadumnaCloudSettings;

                if (currentSettings == null)
                {
                    Debug.LogError("BadumnaCloudSettings script is missing");
                    return null;
                }

                if (currentSettings != null)
                {
                    AssetDatabase.CreateAsset(currentSettings, "Assets/Badumna/Resources/BadumnaCloudSettings.asset");
                }
            }

            return currentSettings;
        }
    }

    [MenuItem("Window/Badumna Cloud")]
    protected static void Init()
    {
        var window = GetWindow(typeof(BadumnaCloudEditor), false, "Badumna Cloud editor", true) as BadumnaCloudEditor;
        window.Show();
    }    

    private static void OnUpdate()
    {
        var window = GetWindow(typeof(BadumnaCloudEditor), false, "Badumna Cloud editor", true) as BadumnaCloudEditor;
        window.Show();

        EditorApplication.update -= OnUpdate;
    }

    private static void PlayModeStateChanged()
    {
        if(string.IsNullOrEmpty(BadumnaCloudEditor.CurrentSettings.ApplicationIdentifier))
        {
            EditorUtility.DisplayDialog("Warning", "Please set your application identifier before running the game.", "Ok");
        }
    }

    private void OnGUI()
    {
        switch (this.currentPage)
        {
            case Page.MainMenu:
                this.DrawMainMenu();
                break;
            case Page.SignUp:
                this.DrawSignUpPage();
                break;
            case Page.Setup:
                this.DrawSetupPage();
                break;
        }

        EditorGUILayout.Separator();
        GUILayout.Space(10);
        this.DrawExtraResources();
    }

    private void DrawMainMenu()
    {
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.Label(BadumnaCloudIcon);
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        EditorGUILayout.Separator();

        GUILayout.Label("Badumna Cloud setup wizzard", EditorStyles.boldLabel);

        EditorGUILayout.Separator();

        // Sign Up and Setup buttons
        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Sign Up"))
        {
            this.currentPage = Page.SignUp;
            this.InitializeSignUpPage();
        }

        if (GUILayout.Button("Setup"))
        {
            this.currentPage = Page.Setup;
            this.InitializeSetupPage();
        }

        GUILayout.EndHorizontal();
    }

    private void InitializeSignUpPage()
    {
        this.agreeTermsAndConditions = false;
        this.emailAddress = string.Empty;
        this.password = string.Empty;
    }

    private void DrawSignUpPage()
    {
        this.DrawBackToMainMenu();
        EditorGUILayout.Separator();

        GUI.skin.label.fontStyle = FontStyle.Bold;
        GUILayout.Label("Sign Up");
        EditorGUILayout.Separator();
        GUI.skin.label.fontStyle = FontStyle.Normal;

        
        this.emailAddress = EditorGUILayout.TextField("E-mail", this.emailAddress);
        this.password = EditorGUILayout.PasswordField("Password", this.password);

        GUILayout.BeginHorizontal();
        this.agreeTermsAndConditions = EditorGUILayout.Toggle(this.agreeTermsAndConditions, new GUILayoutOption[] { GUILayout.Width(20) });
        GUILayout.Label("I agree to the Terms & Conditions", EditorStyles.boldLabel);
        GUILayout.EndHorizontal();

        if (GUILayout.Button("Sign Up"))
        {
            GUIUtility.keyboardControl = 0;

            if (this.agreeTermsAndConditions)
            {
                var appId = this.client.BadumnaCloudRegistration(this.emailAddress, this.password);

                if(!string.IsNullOrEmpty(appId))
                {
                    EditorUtility.DisplayDialog("Success", "Your account has been succesfully created. We've sent you an activation email, click the link inside it to activate", "Ok");
                    EditorUtility.DisplayDialog("Info", "A default application has been created for you. You can now start using Badumna Cloud", "Ok");
                    BadumnaCloudEditor.CurrentSettings.ApplicationIdentifier = appId;
                    EditorUtility.SetDirty(BadumnaCloudEditor.CurrentSettings);
                    this.currentPage = Page.Setup;
                    this.InitializeSetupPage();
                }
                else
                {
                    EditorUtility.DisplayDialog("Error", "Error has occured while registering a new user.", "Ok");
                }
            }
            else
            {
                EditorUtility.DisplayDialog("Error", "You must agree to the Terms and Conditions", "Ok");
            }
        }
    }

    private void InitializeSetupPage()
    {   
        this.applicationId = string.IsNullOrEmpty(BadumnaCloudEditor.CurrentSettings.ApplicationIdentifier) ? string.Empty : BadumnaCloudEditor.CurrentSettings.ApplicationIdentifier;        
    }

    private void DrawSetupPage()
    {
        this.DrawBackToMainMenu();
        EditorGUILayout.Separator();

        GUI.skin.label.fontStyle = FontStyle.Bold;
        GUILayout.Label("Setup your application identifier");
        EditorGUILayout.Separator();
        GUI.skin.label.fontStyle = FontStyle.Normal;

        GUILayout.Label("Application identifier");
        this.applicationId = EditorGUILayout.TextField(this.applicationId);

        if (GUILayout.Button("Save"))
        {
            GUIUtility.keyboardControl = 0;
            BadumnaCloudEditor.CurrentSettings.ApplicationIdentifier = this.applicationId;
            EditorUtility.SetDirty(BadumnaCloudEditor.CurrentSettings);
            EditorUtility.DisplayDialog("Success", "Your settings has been succesfully saved.", "Ok");
        }
    }

    private void DrawExtraResources()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label("Resources", EditorStyles.boldLabel);

        GUILayout.BeginVertical();
        if (GUILayout.Button("Terms & Conditions"))
        {
            EditorUtility.OpenWithDefaultApp(TermsConditionsUrl);
        }

        if (GUILayout.Button("Getting started"))
        {
            EditorUtility.OpenWithDefaultApp(GettingStartedUrl);
        }

        if (GUILayout.Button("Open Cloud dashboard"))
        {
            EditorUtility.OpenWithDefaultApp(CloudDashboardUrl);
        }

        if (GUILayout.Button("Open forum"))
        {
            EditorUtility.OpenWithDefaultApp(ForumUrl);
        }

        GUILayout.EndVertical();
        GUILayout.EndHorizontal();
    }

    private void DrawBackToMainMenu()
    {
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if (GUILayout.Button("Main menu"))
        {
            this.currentPage = Page.MainMenu;
        }
        GUILayout.EndHorizontal();
    }
}
