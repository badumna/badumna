using UnityEngine;
using System;
using System.Collections;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;

public class AccountManager
{
    private const string CloudBadumnaRegistrationUrl = "https://cloud.badumna.com/cp/account/register/";    

    // https://cloud.badumna.com certificate thumbprint.
    private const string CertificateThumbprint = "BBE2363D4F027B1920BAD4948D19C98C3CCEF24E";

    private CookieContainer cookieContainer = new CookieContainer();

    private string csrfToken;

    public AccountManager()
    {
        try
        {
            // automatically get the csrftoken for this session.
            ServicePointManager.ServerCertificateValidationCallback += this.CheckValidationResult;
            WebRequest request = HttpWebRequest.Create(CloudBadumnaRegistrationUrl);
            ((HttpWebRequest)request).CookieContainer = this.cookieContainer;
            ((HttpWebRequest)request).UserAgent = "Unity_Editor";

            request.Timeout = 5000;
            request.BeginGetResponse(this.OnRequestCompleted, request);
        }
        catch (Exception ex)
        {
            Debug.Log(ex.Message);
        }
    }

    public string BadumnaCloudRegistration(string email, string password)
    {
        var applicationIdentifier = string.Empty;

        if (!string.IsNullOrEmpty(this.csrfToken))
        {
            try
            {
                WebRequest request = HttpWebRequest.Create(CloudBadumnaRegistrationUrl);
                ((HttpWebRequest)request).CookieContainer = this.cookieContainer;
                ((HttpWebRequest)request).UserAgent = "Unity_Editor";
                ((HttpWebRequest)request).Referer = CloudBadumnaRegistrationUrl;
                
                var postData = string.Format("csrfmiddlewaretoken={0}&email={1}&password1={2}&password2={2}&agree_terms_and_conds=on&next=", this.csrfToken, email, password);
                var byteArray = Encoding.UTF8.GetBytes(postData);

                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = byteArray.Length;
                request.Headers.Add(HttpRequestHeader.Cookie, "csrftoken=" + this.csrfToken);
                var dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();

                var response = request.GetResponse() as HttpWebResponse;
                if (response != null && response.StatusCode == HttpStatusCode.OK)
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        while(reader.Peek() >= 0)
                        {
                            var line = reader.ReadLine().Trim();
                            if(line.StartsWith("key:"))
                            {
                                applicationIdentifier = line.Substring(line.IndexOf(':') + 1);
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.Log(ex.Message);
            }
        }

        return applicationIdentifier;
    }

    private void OnRequestCompleted(IAsyncResult asyncResult)
    {
        try
        {
            foreach (var cookie in this.cookieContainer.GetCookies(new Uri(CloudBadumnaRegistrationUrl)))
            {
                var split = cookie.ToString().Split(';');

                foreach(var element in split)
                {
                    if(element.Trim().StartsWith("csrftoken"))
                    {
                        this.csrfToken = element.Split('=')[1];
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Debug.Log(ex.Message);
            Debug.Log(ex.InnerException);
        }
    }

    private bool CheckValidationResult(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors error)
    {
        if (error == SslPolicyErrors.None)
            return true;
        
        if (error != SslPolicyErrors.RemoteCertificateChainErrors)
            return false;
        
        // RemoteCertificateChainErrors
        string thumbprint = certificate.GetCertHashString();

        if (thumbprint.Equals(CertificateThumbprint))
            return true;

        return false;
    }
}
