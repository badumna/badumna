using System.Collections;
using Badumna;
using Badumna.SpatialEntities;
using UnityEngine;
using Vector3 = Badumna.DataTypes.Vector3;

public class Player : MonoBehaviour , IReplicableEntity
{
    // Soldier target game object.
    public GameObject soldierTarget;

    // Soldier damage transform.
    public Transform soldierDamage;

    // Gui text for displaying local player name.
    public GUIText nameText;

    // Display overhead player name
    public GameObject overheadName;
    
    // The scene we're on.
    public NetworkScene scene;

    // Overhead name offset
    private UnityEngine.Vector3 overheadNameOffset = new UnityEngine.Vector3(0, 2.0f, 0);

    // A value indicating whether this script is attached to a local player object.
    public bool Local;

    // Dead splash object
    private GameObject deadSplash;

    // Player name
    private string playerName = string.Empty;

    // Attacker name
	private string attackerName;

    // Target rotation in y-axis.
    private float targetRotation;

    #region replicable properties
    // Gets or sets player position
    [Smoothing(Interpolation = 200, Extrapolation = 0)]
    public Vector3 Position
    {
        get
        {
            var position = this.gameObject.transform.position;
            return new Vector3(position.x, position.y, position.z);
        }

        set
        {
            this.gameObject.transform.position = new UnityEngine.Vector3(value.X, value.Y, value.Z);
        }
    }

    // Gets or sets player orientation
    [Replicable]
    public float Orientation
    {
        get
        {
            return this.gameObject.transform.rotation.eulerAngles.y;
        }

        set
        {
            this.targetRotation = value;            
        }
    }

    // Gets or sets the soldier target position.
    [Replicable]
    public Vector3 SoldierTarget
    {
        get
        {
            if (this.soldierTarget != null)
            {
                return new Vector3(
                    this.soldierTarget.transform.position.x,
                    this.soldierTarget.transform.position.y,
                    this.soldierTarget.transform.position.z);
            }

            return new Vector3(0, 0, 0);
        }

        set
        {
            if (this.soldierTarget != null)
            {
                this.soldierTarget.transform.position = new UnityEngine.Vector3(value.X, value.Y, value.Z);
            }
        }
    }

    // Gets or sets the current player's state
    [Replicable]
    public byte State { get; set; }

    // Gets or sets the player name
    [Replicable]
    public string PlayerName
    {
        get
        {
            return this.playerName;
        }

        set
        {
            this.playerName = value;

            if (this.Local)
            {
                this.nameText.text = value;
            }
            else
            {
                if (this.overheadName == null)
                {
                    this.overheadName = (GameObject)Instantiate(Resources.Load("OverheadName"));
                    this.overheadName.name = value;
                    this.overheadName.transform.localScale = new UnityEngine.Vector3(1.4f / 8, 1.4f / 8, 1.4f / 8);
                }

                var textMesh = this.overheadName.GetComponent<TextMesh>();
                textMesh.text = value;
            }
        }
    }

    // Gets or sets the current weapon type
    [Replicable]
    public byte CurrentWeapon { get; set; }

    // Boolean properties can be optimized more using the bit masking technique.

    // Gets or sets a value indicate whether the player is walking.
    [Replicable]
    public bool Walk { get; set; }

    // Gets or sets a value indicate whether the player is crouching.
    [Replicable]
    public bool Crouch { get; set; }

    // Gets or sets a value indicate whether the player is aiming.
    [Replicable]
    public bool Aim { get; set; }

    // Gets or sets a value indicate whether the player is in air.
    [Replicable]
    public bool InAir { get; set; }

    // Gets or sets a value indicate whether the player is reloading.
    [Replicable]
    public bool Reloading { get; set; }

    #endregion // replicable properties
    
    // Gets or sets a value indicate whether the player is firing.
    public bool Fire { get; set; }

    #region remote player RPC methods

    // Simulate weapon firing in replica side.
    //  - fire: A value indicating whether the player is firing
    //  - point: The direction of the shooting
    //  - outputPoint: The origin of the shooting.
    [Replicable]
    public void SimulateFire(bool fire, Vector3 point, Vector3 outputPoint)
    {
        this.Fire = fire;
        Transform gunManager = this.transform.FindChild("GunManager");

        var hashtable = new Hashtable();
        hashtable.Add("Fire", fire);
        hashtable.Add("Weapon", this.CurrentWeapon);
        hashtable.Add("Point", new UnityEngine.Vector3(point.X, point.Y, point.Z));

        if (outputPoint.Magnitude != 0)
            hashtable.Add("OutputPoint", new UnityEngine.Vector3(outputPoint.X, outputPoint.Y, outputPoint.Z));

        gunManager.SendMessage(
            "FireSimulate",
            hashtable);
    }

    // Animate death by spawning the DeadSplash object in the given location.
    //  - position: the position where the DeadSplash object should be initiated.
    //  - killer: the killer name
    [Replicable]
    public void AnimateDeath(Vector3 position, string killer)
    {
        if (this.deadSplash == null)
            this.deadSplash = GameObject.Find("DeadSoldier");
        
        Game.Announcer.AddMessage(string.Format("{0} killed by {1}", this.PlayerName, killer));
        var go = (GameObject)Instantiate(this.deadSplash, new UnityEngine.Vector3(position.X, position.Y + 1f, position.Z), Quaternion.Euler(new UnityEngine.Vector3(90, transform.rotation.y, 0)));
        Destroy(go, 8.0f);
    }
    #endregion // remote player RPC methods.

    #region local player RPC methods

    // Simulate hit by rifle in original side.
    //  - attackerName: The name of the attacker.
    [Replicable]
    public void SimulateHitByRiffle(string attackerName)
    {
        this.attackerName = attackerName;
        soldierDamage.SendMessage("HitSoldier", "RemotePlayer");
    }

    // Simulate hit by grenade in original side.
    //  - attackerName: The name of the attacker.
    [Replicable]
    public void SimulateHitByGrenade(string attackerName)
    {
        this.attackerName = attackerName;
        soldierDamage.SendMessage("HitSoldierGrenade", "RemotePlayer");
    }

    #endregion // local player RPC methods

    // Set weapon type
    public void SetWeapon(int weapon)
    {
        this.CurrentWeapon = (byte)weapon;
    }

    // Set walk
    public void SetWalk(bool walk)
    {
        this.Walk = walk;
    }

    // Set crouch
    public void SetCrouch(bool crouch)
    {
        this.Crouch = crouch;
    }

    // Set aim
    public void SetAim(bool aim)
    {
        this.Aim = aim;
    }

    // Set in air
    public void SetInAir(bool inAir)
    {
        this.InAir = inAir;
    }

    // Set reload
    public void SetReload(bool reloading)
    {
        this.Reloading = reloading;
    }

    // Set fire
    public void SetFire(Hashtable hashtable)
    {
        if (this.Local && this.scene != null)
        {
            this.Fire = (bool) hashtable["Fire"];
            var point = (UnityEngine.Vector3) hashtable["Point"];
            var outputPoint = UnityEngine.Vector3.zero;

            if (hashtable.Contains("OutputPoint"))
            {
                outputPoint = (UnityEngine.Vector3) hashtable["OutputPoint"];
            }

            this.scene.CallMethodOnReplicas(
                this.SimulateFire,
                this.Fire,
                new Vector3(point.x, point.y, point.z),
                new Vector3(outputPoint.x, outputPoint.y, outputPoint.z));
        }
    }

    #region HitByRifle method
    // Hit by rifle, this method is called by the remote player.
    public void HitByRifle()
    {
        if (!this.Local && this.scene != null)
            this.scene.CallMethodOnOriginal(this.SimulateHitByRiffle, GameObject.Find("PlayerName").GetComponent<GUIText>().text);
    }
    #endregion // HitByRifle method

    #region HitByGrenade method
    // Hi by grenade, this method is called by remote player.
    public void HitByGrenade()
    {
        if (!this.Local && this.scene != null)
            this.scene.CallMethodOnOriginal(this.SimulateHitByGrenade, GameObject.Find("PlayerName").GetComponent<GUIText>().text);
    }
    #endregion // HitByGrenade method

    #region Dead method
    // Dead method is called by the local player.
    public void Dead(UnityEngine.Vector3 spawnPosition)
    {
        if (this.Local && this.scene != null)
        {
            Game.Announcer.AddMessage(string.Format("{0} killed you", this.attackerName));
            this.scene.CallMethodOnReplicas(this.AnimateDeath, this.Position, this.attackerName);
            this.Position = new Vector3(spawnPosition.x, spawnPosition.y, spawnPosition.z);
        }
    }
    #endregion // Dead method
    
    // Update the state and overhead name position and rotation.
    private void Update()
    {
        this.ReadKeyboardInput();

        if (this.overheadName != null)
        {
            this.overheadName.transform.position = transform.position + this.overheadNameOffset;
            var camDistance = this.overheadName.transform.position - Camera.main.transform.position;
            this.overheadName.transform.LookAt(this.overheadName.transform.position + camDistance);
        }

        if(!this.Local)
        {
            // Apply rotation smoothing.
            this.gameObject.transform.rotation = Quaternion.Slerp(
                this.gameObject.transform.rotation,
                Quaternion.Euler(new UnityEngine.Vector3(0.0f, this.targetRotation, 0.0f)),
                Time.deltaTime * 10);
        }
    }

    // Destroy the overhead name object when the script became inactive.
    private void OnDisable()
    {
        if (this.overheadName != null)
        {
            Destroy(this.overheadName);
        }
    }

    // Read keyboard input and update the player state .
    private void ReadKeyboardInput()
    {
        if (Local)
        {
            if (Input.GetAxis("Horizontal") > 0.1f)
            {
                if (Walk)
                {
                    this.State = (byte)KeyState.WalkStrafeRight;
                }
                else
                {
                    this.State = (byte)KeyState.RunStrafeRight;
                }
            }
            else if (Input.GetAxis("Horizontal") < -0.1f)
            {
                if (Walk)
                {
                    this.State = (byte)KeyState.WalkStrafeLeft;
                }
                else
                {
                    this.State = (byte)KeyState.RunStrafeLeft;
                }
            }
            else if (Input.GetAxis("Vertical") > 0.1)
            {
                if (Walk)
                {
                    this.State = (byte)KeyState.Walking;
                }
                else
                {
                    this.State = (byte)KeyState.Runing;
                }
            }
            else if (Input.GetAxis("Vertical") < -0.1)
            {
                if (Walk)
                {
                    this.State = (byte)KeyState.WalkBack;
                }
                else
                {
                    this.State = (byte)KeyState.RuningBack;
                }
            }
            else
            {
                this.State = (byte)KeyState.Still;
            }
        }
    }
}
