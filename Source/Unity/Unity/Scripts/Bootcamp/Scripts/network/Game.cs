using System;
using System.Collections;
using Badumna;
using Badumna.DataTypes;
using Badumna.SpatialEntities;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

public class Game : MonoBehaviour
{
    // GUI Skin
    public GUISkin Skin;

    // Scalify logo
    public Texture2D ScalifyLogo;

    // Badumna Cloud logo
    public Texture2D BadumnaCloudLogo;

    // Windows background image
    public Texture2D WindowBackground;

    // Main menu screen object
    public GameObject MainMenuScreen;

    // Game manager object
    public GameObject GameManager;

    // Remote Player prefab.
    public GameObject PlayerModel;

    // Local player game object.
    public GameObject Player;

    // Entity type
    private const uint EntityType = 0;

    // Path to the badumna cloud setting path.
    private const string BadumnaCloudSettingsPath = "Assets/Badumna/Resources/BadumnaCloudSettings.asset";

    // Serialized badumna cloud settings, that has been setup through Badumna Cloud wizzard.
    private static BadumnaCloudSettings CloudSettings;

    // The Badumna network.
    private static INetworkFacade network;

    // Game announcer
    private static GameAnnouncer announcer;

    // A Badumna network scene.
    private NetworkScene networkScene;

    // The local player character.
    private Player localPlayer;

    // Local player name.
    private string playerName = "default";

    // A value indicating if the local player is registered with a Badumna network scene.
    private bool isRegistered;

    // A value indicating whether the player name has been entered.
    private bool isNameEntered;

    // For displaying initialization status on screen.
    private GUIText status;

    // Result of initializing Badumna.
    IAsyncResult initializationResult;

    // Logo index.
    private int currentLogoIndex;

    // Current screen opacity.
    private float opacity = 1;

    // Gets the Badumna network.
    public static INetworkFacade Badumna
    {
        get { return Game.network; }
    }

    // Gets the game announcer
    public static GameAnnouncer Announcer
    {
        get { return Game.announcer; }
    }
	
	#region joining scene
    // Join a Badumna network scene.
    //  - sceneName: The name of the scene to join.
    public void JoinScene(string sceneName)
    {
        this.CreateLocalPlayer();

        if (this.localPlayer != null && this.networkScene == null)
        {
            // Join the normal scene.
            this.networkScene = Game.network.JoinScene(sceneName, this.CreateReplica, this.RemoveReplica);
            this.networkScene.RegisterEntity(this.localPlayer, EntityType, 2f, 60f);
            this.isRegistered = true;
            this.localPlayer.scene = this.networkScene;
        }
    }
    #endregion // joining scene

    #region leaving scene
    // Leave the Badumna network scene.
    public void LeaveScene()
    {
        if (this.localPlayer != null && this.networkScene != null)
        {
            this.networkScene.UnregisterEntity(this.localPlayer);
            this.networkScene.Leave();
            this.networkScene = null;
            this.isRegistered = false;

            Destroy(this.localPlayer.gameObject);
        }
    }
    #endregion // leaving scene

    #region badumna initialization
    // Called by Unity when this script is loaded.
    private void Awake()
    {
    
        CloudSettings = (BadumnaCloudSettings) Resources.Load("BadumnaCloudSettings", typeof(BadumnaCloudSettings));

        if(CloudSettings == null)
        {
            Debug.LogError(string.Format("Missing Badumna cloud settings. The settings should be located in {0}", BadumnaCloudSettingsPath));
            return;
        }

        if(string.IsNullOrEmpty(CloudSettings.ApplicationIdentifier))
        {
            Debug.LogError("Please make sure that you have setup the application identifier correctly.");
            return;
        }

        var go = new GameObject();
        go.transform.position = new Vector3(0.05f, 0.05f, 0);
        this.status = go.AddComponent<GUIText>();
        this.status.anchor = TextAnchor.LowerLeft;
        this.status.font.material.color = Color.black;
        this.status.text = "Initializing Badumna...";

        this.initializationResult = NetworkFacade.BeginCreate(
            CloudSettings.ApplicationIdentifier,
            null);

        announcer = new GameAnnouncer();
    }
    #endregion // badumna initialization

    // Called by Unity before the first call to FixedUpdate.
    // Returns: An enumerator as per Unity coroutine requirements.
    private IEnumerator Start()
    {
        #region badumna login
        StartCoroutine(this.DisplayLogo());

        if(this.initializationResult == null)
            yield break;

        while (!this.initializationResult.IsCompleted)
        {
            yield return null;
        }

        if (Game.network == null)
        {
            try
            {
                Game.network = NetworkFacade.EndCreate(this.initializationResult);
                Destroy(this.status.gameObject);
            }
            catch (Exception ex)
            {
                var errorMessage = "Badumna initialization failed: " + ex.Message;
                this.status.text = errorMessage;
                Debug.LogError(errorMessage);
                yield break;
            }
        }

        while (!this.isNameEntered)
        {
            yield return null;
        }

        // we must login before attempting to use the network
        var loginResult = Badumna.Login(this.playerName);

        if (!loginResult)
        {
            Debug.LogError("Failed to login");
            yield break;
        }
        #endregion // badumna login

        // Register Entity Details.
        Game.network.RegisterEntityDetails(60, 6);

        // Register any custom types for replicable properties here.
        //// Game.network.TypeRegistry.RegisterValueType( ... );

        this.JoinScene("Scene-1");
    }

    #region FixedUpdate method
    // Called by Unity every fixed frame.
    private void FixedUpdate()
    {
        if (Game.network == null || !Game.network.IsLoggedIn)
        {
            return;
        }

        Game.network.ProcessNetworkState();
    }
    #endregion // FixedUpdate method
    
    // Display the register player screen.
    private void OnGUI()
    {
        if (!this.isNameEntered)
        {
            GUI.depth = -1;

            Color oldColor;
            Color auxColor;
            oldColor = auxColor = GUI.color;

            if (this.currentLogoIndex == 0)
            {
                auxColor.a = opacity;
                GUI.color = auxColor;
                this.DrawLogo(this.ScalifyLogo, 343, 280);
                GUI.color = oldColor;
            }
            else if (this.currentLogoIndex == 1)
            {
                auxColor.a = opacity;
                GUI.color = auxColor;
                this.DrawLogo(this.BadumnaCloudLogo, 280, 343);
                GUI.color = oldColor;
            }
            else
            {
                GUI.skin = this.Skin;
                GUI.DrawTexture(new Rect((int)((Screen.width / 2) - (WindowBackground.width / 2)), (int)((Screen.height / 2) - (WindowBackground.height / 2)), WindowBackground.width, WindowBackground.height), WindowBackground);
                GUILayout.BeginArea(new Rect((int)(((Screen.width / 2) - (WindowBackground.width / 2)) + 30), (int)(((Screen.height / 2) - (WindowBackground.height / 2)) + 20), WindowBackground.width - 30, WindowBackground.height - 20));

                GUILayout.Label("Join Deathmatch", GUI.skin.FindStyle("Titles"));

                GUILayout.Space(40);


                GUILayout.BeginArea(new Rect((WindowBackground.width / 2) - (WindowBackground.height / 2) - 30, (WindowBackground.height / 2) - 50, WindowBackground.width, WindowBackground.height));


                GUILayout.Label("Name:", GUI.skin.FindStyle("Create"));

                GUI.SetNextControlName("registerName");
                this.playerName = GUILayout.TextField(this.playerName, GUILayout.Width((int)((WindowBackground.width / 2))));

                var buttonDown = GUILayout.Button("OK", GUI.skin.FindStyle("Create"));
                var keyDown = Event.current.type == EventType.keyDown && Event.current.character == '\n';
                if (buttonDown || keyDown)
                {
                    if (!string.IsNullOrEmpty(this.playerName))
                    {
                        this.isNameEntered = true;
                        GameManager.SendMessage("Initialize");
                    }
                }
                GUILayout.EndArea();
                GUILayout.EndArea();

                GUI.FocusControl("registerName");
            }

            return;
        }

        announcer.DrawGUI(this.Skin.FindStyle("Killed"));
    }

    #region  shutting down badumna
    // Called by Unity when application is quitting.
    private void OnApplicationQuit()
    {
        if (!this.initializationResult.IsCompleted)
        {
            try
            {
                Game.network = NetworkFacade.EndCreate(this.initializationResult);
            }
            catch { }
        }

        if (this.isRegistered)
        {
            this.LeaveScene();
        }

        if (this.status != null)
        {
            Destroy(this.status.gameObject);
        }

        if (Game.network != null)
        {
            Game.network.Shutdown();
            Game.network = null;
        }
    }
    #endregion // shutting down badumna

    // Create a local player.
    //  - playerName: A name for the local player.
    private void CreateLocalPlayer()
    {
        this.localPlayer = this.Player.GetComponent<Player>();
        this.localPlayer.PlayerName = this.playerName;
    }

    #region CreateReplica method
    // Called by Badumna to create a replica entity.
    //  - scene: The scene the replica entity belongs to.
    //  - entityId: An ID for the entity.
    //  - entityType: An integer indicating the type of entity to create.
    // Returns: A new replica entity.
    private IReplicableEntity CreateReplica(NetworkScene scene, BadumnaId entityId, uint entityType)
    {
        if (entityType == EntityType)
        {
            var replica = (GameObject)Instantiate(this.PlayerModel, Vector3.zero, Quaternion.identity);
            var remotePlayer = replica.GetComponent<Player>();
            remotePlayer.scene = this.networkScene;
            var actorAnimator = replica.GetComponent<ActorAnimator>();
            actorAnimator.RemotePlayer = remotePlayer;

            return remotePlayer;
        }

        return null;
    }
    #endregion // CreateReplica method

    #region RemoveReplica method
    // Called by Badumna to remove a replica when it moves out of your local
    // entities' interest area.
    //  - scene: The scene the replica is being removed from.
    //  - replica: The replica to removed.
    private void RemoveReplica(NetworkScene scene, IReplicableEntity replica)
    {
        Destroy((replica as Player).gameObject);
    }
    #endregion // RemoveReplica method
    
    // Display logo.
    private IEnumerator DisplayLogo()
    {
        // Display first logo.
        StartCoroutine(this.FadeIn());
        yield return new WaitForSeconds(3);
        StartCoroutine(this.FadeOut());
        yield return new WaitForSeconds(1);

        // Display second logo.
        StartCoroutine(this.FadeIn());
        yield return new WaitForSeconds(3);
        StartCoroutine(this.FadeOut());
        yield return new WaitForSeconds(1);
    }

    // Fade in effect
    private IEnumerator FadeIn()
    {
        this.opacity = 0;
        while (this.opacity < 1)
        {
            opacity += Time.deltaTime * 2;
            yield return new WaitForEndOfFrame();
        }
        this.opacity = 1;
    }

    // Fade out effect
    private IEnumerator FadeOut()
    {
        while (this.opacity > 0)
        {
            opacity -= Time.deltaTime * 3;
            yield return new WaitForEndOfFrame();
        }

        this.opacity = 0;
        this.currentLogoIndex++;
    }

    // Draw texture logo
    //  - Logo: The logo image
    //  - height: height of the texture to be drawn
    //  - width: width of the texture to be drawn
    private void DrawLogo(Texture2D Logo, float height, float width)
    {
        GUI.DrawTexture(new Rect((int)((Screen.width / 2) - (width / 2)), (int)((Screen.height / 2) - (height / 2)), width, height), Logo);
    }
}
