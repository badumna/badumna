using UnityEngine;
using System.Collections;
using System;

public class ActorAnimator : MonoBehaviour 
{
    // Aim pivot
    public Transform aimPivot;

    // Remote player
    public Player RemotePlayer { get; set; }

    // private fields
    private string currentWeaponName;
    private float jumpLandCrouchAmount = 1.6f;

    private float groundedWeight = 1f;
    private float crouchWeight;
    private float aimWeight;
    private float fireWeight;

    private float aimAngleY = 0.0f;

    // On enable is called when the script become active.
    private void OnEnable()
    {
        this.SetAnimProperties();
    }
	
	// Update is called once per frame
    private void Update()
    {
        this.currentWeaponName = this.RemotePlayer.CurrentWeapon == 0 ? "M4" : "M203";
        this.SetAnimProperties();

        if (this.RemotePlayer.Crouch)
        {
            this.crouchWeight = this.CrossFadeUp(this.crouchWeight, 0.4f);
        }
        else if (this.RemotePlayer.InAir && this.jumpLandCrouchAmount > 0)
        {
            this.crouchWeight = this.CrossFadeUp(this.crouchWeight, 1 / this.jumpLandCrouchAmount);
        }
        else
        {
            this.crouchWeight = this.CrossFadeDown(this.crouchWeight, 0.45f);
        }

        float uprightWeight = 1 - this.crouchWeight;
        
        if (this.RemotePlayer.Fire)
        {
            this.aimWeight = this.CrossFadeUp(this.aimWeight, 0.2f);
            this.fireWeight = this.CrossFadeUp(this.fireWeight, 0.2f);
        }
        else if (this.RemotePlayer.Aim)
        {
            this.aimWeight = this.CrossFadeUp(this.aimWeight, 0.3f);
            this.fireWeight = this.CrossFadeDown(this.fireWeight, 0.3f);
        }
        else
        {
            this.aimWeight = this.CrossFadeDown(this.aimWeight, 0.5f);
            this.fireWeight = this.CrossFadeDown(this.fireWeight, 0.5f);
        }

        float nonAimWeight = 1 - this.aimWeight;

        if (this.RemotePlayer.InAir)
        {
            this.groundedWeight = this.CrossFadeDown(this.groundedWeight, 0.1f);
        }
        else
        {
            this.groundedWeight = this.CrossFadeUp(this.groundedWeight, 0.2f);
        }

        // Aiming up/down
        Vector3 aimDir = (this.RemotePlayer.soldierTarget.transform.position - this.aimPivot.position).normalized;
        float targetAngle = Mathf.Asin(aimDir.y) * Mathf.Rad2Deg;
        this.aimAngleY = Mathf.Lerp(this.aimAngleY, targetAngle, Time.deltaTime * 8);

        // Use HeadLookController when not aiming/firing
        SendMessage("SetEffect", nonAimWeight);

        // Use additive animations for aiming when aiming and firing
        animation["StandingAimUp"].weight = uprightWeight * aimWeight;
        animation["StandingAimDown"].weight = uprightWeight * aimWeight;
        animation["CrouchAimUp"].weight = crouchWeight * aimWeight;
        animation["CrouchAimDown"].weight = crouchWeight * aimWeight;

        // Set time of animations according to current vertical aiming angle
        animation["StandingAimUp"].time = Mathf.Clamp01(aimAngleY / 90);
        animation["StandingAimDown"].time = Mathf.Clamp01(-aimAngleY / 90);
        animation["CrouchAimUp"].time = Mathf.Clamp01(aimAngleY / 90);
        animation["CrouchAimDown"].time = Mathf.Clamp01(-aimAngleY / 90);

        if (this.RemotePlayer.Reloading)
        {
            animation.CrossFade("Reload" + currentWeaponName, 0.1f);
        }

        if (!this.RemotePlayer.InAir)
        {
            if (this.RemotePlayer.Crouch)
            {
                this.Crouch();
            }
            else
            {
                this.Idle();
            }
        }
        else
        {
            this.Jump();
        }

        if (this.RemotePlayer.State == (byte)KeyState.Walking) { this.WalkFoward(); }
        if (this.RemotePlayer.State == (byte)KeyState.WalkBack) { this.WalkBack(); }
        if (this.RemotePlayer.State == (byte)KeyState.Runing) { this.Run(); }
        if (this.RemotePlayer.State == (byte)KeyState.RuningBack) { this.RunBack(); }

        if (this.RemotePlayer.State == (byte)KeyState.WalkStrafeLeft) { this.WalkStrafeLeft(); }
        if (this.RemotePlayer.State == (byte)KeyState.WalkStrafeRight) { this.WalkStrafeRight(); }
        if (this.RemotePlayer.State == (byte)KeyState.RunStrafeLeft) { this.RunStrafeLeft(); }
        if (this.RemotePlayer.State == (byte)KeyState.RunStrafeRight) { this.RunStrafeRight(); }

        if (this.RemotePlayer.Fire)
        {
            Fire(this.RemotePlayer.CurrentWeapon);
        }	
    }

    private float CrossFadeUp(float weight, float fadeTime)
    {
        return Mathf.Clamp01(weight + Time.deltaTime / fadeTime);
    }

    private float CrossFadeDown(float weight, float fadeTime)
    {
        return Mathf.Clamp01(weight - Time.deltaTime / fadeTime);
    }

    private void SetAnimProperties()
    {
        // Set animation properties
        if (animation.GetClip("ReloadM4") == null && animation.GetClip("ReloadM203") == null)
        {
            //  Set all animations to loop
            animation.wrapMode = WrapMode.Loop;
            animation["RunJump"].wrapMode = WrapMode.Clamp;
            animation["StandingJump"].wrapMode = WrapMode.ClampForever;
            animation["RunJump"].layer = 1;
            animation["StandingJump"].layer = 1;


            animation.AddClip(animation["StandingReloadM4"].clip, "ReloadM4");
            animation["ReloadM4"].AddMixingTransform(transform.Find("Pelvis/Spine1/Spine2"));
            animation["ReloadM4"].wrapMode = WrapMode.Clamp;
            animation["ReloadM4"].layer = 3;
            animation["ReloadM4"].time = 0;
            animation["ReloadM4"].speed = 1.0f;

            animation.AddClip(animation["StandingReloadRPG1"].clip, "ReloadM203");
            animation["ReloadM203"].AddMixingTransform(transform.Find("Pelvis/Spine1/Spine2"));
            animation["ReloadM203"].wrapMode = WrapMode.Clamp;
            animation["ReloadM203"].layer = 3;
            animation["ReloadM203"].time = 0;
            animation["ReloadM203"].speed = 1.0f;
        }

        string[] anims = new string[] { "StandingAimUp", "StandingAimDown", "CrouchAimUp", "CrouchAimDown" };

        foreach (var anim in anims)
        {
            animation[anim].blendMode = AnimationBlendMode.Additive;
            animation[anim].enabled = true;
            animation[anim].weight = 1;
            animation[anim].layer = 4;
            animation[anim].time = 0;
            animation[anim].speed = 0;
        }
    }

    #region play animation

    private void Crouch()
    {
        if (animation.IsPlaying("Walk"))
            animation.Blend("Walk", 0.0f, 0.1f);

        if (animation.IsPlaying("Run"))
            animation.Blend("Run", 0.0f, 0.1f);

        if (animation.IsPlaying("Idle"))
            animation.Blend("Idle", 0.0f, 0.1f);

        if (animation.IsPlaying("StandingJump"))
            animation.Blend("StandingJump", 0.0f, 0.1f);

        if (this.RemotePlayer.Aim)
        {
            animation["CrouchAim"].speed = 1.0f;
            animation.CrossFade("CrouchAim");
        }
        else
        {
            animation["Crouch"].speed = 0.2f;
            animation.CrossFade("Crouch", 0.3f, PlayMode.StopAll);
        }
    }

    private void Idle()
    {
        if (animation.IsPlaying("Walk"))
            animation.Blend("Walk", 0.0f, 0.1f);

        if (animation.IsPlaying("Run"))
            animation.Blend("Run", 0.0f, 0.1f);

        if (animation.IsPlaying("StandingJump"))
            animation.Blend("StandingJump", 0.0f, 0.1f);

        if (this.RemotePlayer.Aim)
        {
            animation["StandingAim"].speed = 1.0f;
            animation.CrossFade("StandingAim");
        }
        else
        {
            animation.CrossFade("Standing");
        }
    }

    private void Jump()
    {
        animation["StandingJump"].speed = 1.0f;
        animation.CrossFade("StandingJump");
    }

    private void WalkFoward()
    {
        if (this.RemotePlayer.Crouch)
        {
            if (this.RemotePlayer.Aim)
            {
                animation["CrouchWalkAim"].speed = 1.0f;
                animation.CrossFade("CrouchWalkAim");
            }
            else
            {
                animation["CrouchWalk"].speed = 1.0f;
                animation.CrossFade("CrouchWalk");
            }
        }
        else
        {
            if (this.RemotePlayer.Aim)
            {
                animation["WalkAim"].speed = 1.0f;
                animation.CrossFade("WalkAim");
            }
            else
            {
                animation["Walk"].speed = 1.0f;
                animation.CrossFade("Walk");
            }
        }

    }

    private void WalkBack()
    {
        if (animation.IsPlaying("Walk"))
            animation.Blend("Walk", 0.0f, 0.1f);

        if (animation.IsPlaying("CrouchWalkAim"))
            animation.Blend("CrouchWalkAim", 0.0f, 0.1f);

        if (this.RemotePlayer.Crouch)
        {
            if (this.RemotePlayer.Aim)
            {
                animation["CrouchWalkBackwardsAim"].speed = 1.0f;
                animation.CrossFade("CrouchWalkBackwardsAim");
            }
            else
            {
                animation["CrouchWalkBackwards"].speed = 1.0f;
                animation.CrossFade("CrouchWalkBackwards");
            }
        }
        else
        {
            if (this.RemotePlayer.Aim)
            {
                animation["WalkBackwardsAim"].speed = 1.0f;
                animation.CrossFade("WalkBackwardsAim");
            }
            else
            {
                animation["WalkBackwards"].speed = 1.0f;
                animation.CrossFade("WalkBackwards");
            }
        }
    }

    private void Run()
    {

        if (animation.IsPlaying("Walk"))
            animation.Blend("Walk", 0.0f, 0.1f);

        if (animation.IsPlaying("Run"))
            animation.Blend("Run", 0.0f, 0.1f);

        if (animation.IsPlaying("Idle"))
            animation.Blend("Idle", 0.0f, 0.1f);

        if (animation.IsPlaying("StandingJump"))
            animation.Blend("StandingJump", 0.0f, 0.1f);

        if (this.RemotePlayer.Aim)
        {
            animation["RunAim"].speed = 1.0f;
            animation.CrossFade("RunAim");
        }
        else
        {
            animation["Run"].speed = 1.0f;
            animation.CrossFade("Run");
        }
    }

    private void RunBack()
    {
        if (animation.IsPlaying("Walk"))
            animation.Blend("Walk", 0.0f, 0.1f);

        if (animation.IsPlaying("Run"))
            animation.Blend("Run", 0.0f, 0.1f);

        if (animation.IsPlaying("Idle"))
            animation.Blend("Idle", 0.0f, 0.1f);

        if (animation.IsPlaying("StandingJump"))
            animation.Blend("StandingJump", 0.0f, 0.1f);

        if (this.RemotePlayer.Aim)
        {
            animation["RunBackwardsAim"].speed = 1.0f;
            animation.CrossFade("RunBackwardsAim");
        }
        else
        {
            animation["RunBackwards"].speed = 1.0f;
            animation.CrossFade("RunBackwards");
        }
    }

    private void WalkStrafeLeft()
    {
        if (this.RemotePlayer.Crouch)
        {
            if (this.RemotePlayer.Aim)
            {
                animation["CrouchStrafeWalkLeftAim"].speed = 1.0f;
                animation.CrossFade("CrouchStrafeWalkLeftAim");
            }
            else
            {
                animation["CrouchStrafeWalkLeft"].speed = 1.0f;
                animation.CrossFade("CrouchStrafeWalkLeft");
            }
        }
        else
        {
            if (this.RemotePlayer.Aim)
            {
                animation["StrafeWalkLeftAim"].speed = 1.0f;
                animation.CrossFade("StrafeWalkLeftAim");
            }
            else
            {
                animation["StrafeWalkLeft"].speed = 1.0f;
                animation.CrossFade("StrafeWalkLeft");
            }
        }
    }

    private void WalkStrafeRight()
    {
        if (this.RemotePlayer.Crouch)
        {
            if (this.RemotePlayer.Aim)
            {
                animation["CrouchStrafeWalkRightAim"].speed = 1.0f;
                animation.CrossFade("CrouchStrafeWalkRightAim");
            }
            else
            {
                animation["CrouchStrafeWalkRight"].speed = 1.0f;
                animation.CrossFade("CrouchStrafeWalkRight");
            }
        }
        else
        {
            if (this.RemotePlayer.Aim)
            {
                animation["StrafeWalkRightAim"].speed = 1.0f;
                animation.CrossFade("StrafeWalkRightAim");
            }
            else
            {
                animation["StrafeWalkRight"].speed = 1.0f;
                animation.CrossFade("StrafeWalkRight");
            }
        }
    }

    private void RunStrafeLeft()
    {
        if (this.RemotePlayer.Crouch)
        {
            if (this.RemotePlayer.Aim)
            {
                animation["CrouchStrafeRunLeftAim"].speed = 1.0f;
                animation.CrossFade("CrouchStrafeRunLeftAim");
            }
            else
            {
                animation["CrouchStrafeRunLeft"].speed = 1.0f;
                animation.CrossFade("CrouchStrafeRunLeft");
            }
        }
        else
        {
            if (this.RemotePlayer.Aim)
            {
                animation["StrafeRunLeftAim"].speed = 1.0f;
                animation.CrossFade("StrafeRunLeftAim");
            }
            else
            {
                animation["StrafeRunLeft"].speed = 1.0f;
                animation.CrossFade("StrafeRunLeft");
            }
        }
    }

    private void RunStrafeRight()
    {
        if (this.RemotePlayer.Crouch)
        {
            if (this.RemotePlayer.Aim)
            {
                animation["CrouchStrafeRunRightAim"].speed = 1.0f;
                animation.CrossFade("CrouchStrafeRunRightAim");
            }
            else
            {
                animation["CrouchStrafeRunRight"].speed = 1.0f;
                animation.CrossFade("CrouchStrafeRunRight");
            }
        }
        else
        {
            if (this.RemotePlayer.Aim)
            {
                animation["StrafeRunRightAim"].speed = 1.0f;
                animation.CrossFade("StrafeRunRightAim");
            }
            else
            {
                animation["StrafeRunRight"].speed = 1.0f;
                animation.CrossFade("StrafeRunRight");
            }
        }
    }

    private void Fire(int Gun)
    {
        if (animation.IsPlaying("Walk"))
            animation.Blend("Walk", 0.0f, 0.1f);

        if (animation.IsPlaying("WalkAim"))
            animation.Blend("WalkAim", 0.0f, 0.1f);

        if (animation.IsPlaying("CrouchWalkAim"))
            animation.Blend("CrouchWalkAim", 0.0f, 0.1f);

        if (animation.IsPlaying("CrouchWalk"))
            animation.Blend("CrouchWalk", 0.0f, 0.1f);

        if (animation.IsPlaying("RunAim"))
            animation.Blend("RunAim", 0.0f, 0.1f);

        if (animation.IsPlaying("Run"))
            animation.Blend("Run", 0.0f, 0.1f);

        if (animation.IsPlaying("StandingJump"))
            animation.Blend("StandingJump", 0.0f, 0.1f);

        if (this.RemotePlayer.Crouch)
        {
            if (animation.IsPlaying("CrouchWalk") || animation.IsPlaying("CrouchWalkAim"))
            {
                animation["CrouchWalkFire"].speed = 1.0f;
                animation.CrossFade("CrouchWalkFire");
            }
            else if (animation.IsPlaying("CrouchWalkBackwards") || animation.IsPlaying("CrouchWalkBackwardsAim"))
            {
                animation["CrouchWalkBackwardsFire"].speed = 1.0f;
                animation.CrossFade("CrouchWalkBackwardsFire");
            }
            else
            {
                if (Gun == 0)
                {
                    animation["CrouchFire"].speed = 3.0f;
                    animation.CrossFade("CrouchFire");
                }
                else
                {
                    animation["CrouchFireRPG"].speed = 1.0f;
                    animation.CrossFade("CrouchFireRPG");
                }
            }
        }
        else
        {
            if (animation.IsPlaying("Run") || animation.IsPlaying("RunAim"))
            {
                if (Gun == 0)
                {
                    animation["RunFire"].speed = 1.0f;
                    animation.CrossFade("RunFire");
                }
                else
                {
                    animation["RunFireRPG"].speed = 1.0f;
                    animation.CrossFade("RunFireRPG");
                }
            }
            else if (animation.IsPlaying("Walk") || animation.IsPlaying("WalkAim"))
            {
                if (Gun == 0)
                {
                    animation["WalkFire"].speed = 1.0f;
                    animation.CrossFade("WalkFire");

                }
                else
                {
                    animation["WalkFireRPG"].speed = 1.0f;
                    animation.CrossFade("WalkFireRPG");
                }
            }
            else if (animation.IsPlaying("RunBackwards") || animation.IsPlaying("RunBackwardsAim"))
            {
                if (Gun == 0)
                {
                    animation["RunBackwardsFire"].speed = 1.0f;
                    animation.CrossFade("RunBackwardsFire");
                }
                else
                {
                    animation["RunFireRPG"].speed = -1.0f;
                    animation.CrossFade("RunFireRPG");
                }

            }
            else if (animation.IsPlaying("WalkBackwards") || animation.IsPlaying("WalkBackwardsAim"))
            {
                if (Gun == 0)
                {
                    animation["WalkBackwardsFire"].speed = 1.0f;
                    animation.CrossFade("WalkBackwardsFire");
                }
                else
                {
                    animation["WalkFireRPG"].speed = -1.0f;
                    animation.CrossFade("WalkFireRPG");
                }

            }
            else if (animation.IsPlaying("CrouchStrafeWalkLeft") || animation.IsPlaying("CrouchStrafeWalkLeftAim"))
            {
                animation["CrouchStrafeWalkLeftFire"].speed = 1.0f;
                animation.CrossFade("CrouchStrafeWalkLeftFire");
            }
            else if (animation.IsPlaying("StrafeWalkLeft") || animation.IsPlaying("StrafeWalkLeftAim"))
            {
                animation["StrafeWalkLeftFire"].speed = 1.0f;
                animation.CrossFade("StrafeWalkLeftFire");
            }
            else if (animation.IsPlaying("CrouchStrafeWalkRight") || animation.IsPlaying("CrouchStrafeWalkRightAim"))
            {
                animation["CrouchStrafeWalkRightFire"].speed = 1.0f;
                animation.CrossFade("CrouchStrafeWalkRightFire");
            }
            else if (animation.IsPlaying("CrouchStrafeRunLeft") || animation.IsPlaying("CrouchStrafeRunLeftAim"))
            {
                animation["CrouchStrafeRunLeftFire"].speed = 1.0f;
                animation.CrossFade("CrouchStrafeRunLeftFire");
            }
            else if (animation.IsPlaying("StrafeRunLeft") || animation.IsPlaying("StrafeRunLeftAim"))
            {
                animation["StrafeRunLeftFire"].speed = 1.0f;
                animation.CrossFade("StrafeRunLeftFire");
            }
            else if (animation.IsPlaying("CrouchStrafeRunRight") || animation.IsPlaying("CrouchStrafeRunRightAim"))
            {
                animation["CrouchStrafeRunRightFire"].speed = 1.0f;
                animation.CrossFade("CrouchStrafeRunRightFire");
            }
            else if (animation.IsPlaying("StrafeRunRight") || animation.IsPlaying("StrafeRunRightAim"))
            {
                animation["StrafeRunRightFire"].speed = 1.0f;
                animation.CrossFade("StrafeRunRightFire");
            }
            else
            {
                if (Gun == 0)
                {
                    animation["StandingFire"].speed = 1.0f;
                    animation.Play("StandingFire");
                }
                else
                {
                    animation["StandingFireRPG"].speed = 1.0f;
                    animation.Play("StandingFireRPG");
                }
            }
        }
    }
    #endregion // play animation
}
