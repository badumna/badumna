#pragma strict
#pragma implicit
#pragma downcast

class SoldierDamageControl extends MonoBehaviour
{
    public var Respawn : Vector3[];

    public var life : float;

    public var hitTexture : Texture2D;
    public var blackTexture : Texture2D;

    private var hitAlpha : float;
    private var blackAlpha : float;

    private var recoverTime : float;

    public var hitSounds : AudioClip[];
    public var dyingSound : AudioClip;

    public var Soldier : GameObject;

    function Start()
    {
        SoldierController.dead = false;
        hitAlpha = 0.0;
        blackAlpha = 0.0;
        life = 1.0;
    }

    #region HitSoldier method
    function HitSoldier(hit : String)
    {
        if(GameManager.receiveDamage)
        {
            if(!audio.isPlaying)
            {
                if(life < 0.5 && (Random.Range(0, 100) < 30))
                {
                    audio.clip = dyingSound;
                }
                else
                {
                    audio.clip = hitSounds[Random.Range(0, hitSounds.length)];
                }

                audio.Play();
            }

            if(hit == "Player")
            {
                #region highlight
                // Remote player send RPC back to original, 
                // to notify that this player is being hit.
                Soldier.SendMessage("HitByRifle");
                return;
                #endregion // highlight
            }

            life -= 0.05;
            recoverTime = (1.0 - life) * 10.0;

            if(hit == "Dummy")
            {
                TrainingStatistics.dummiesHit++;
            }
            else if(hit == "Turret")
            {
                TrainingStatistics.turretsHit++;
            }

            if(life <= 0.0 && !SoldierController.dead)
            {
                SoldierController.dead = true;
                Soldier.SendMessage("Dead", Respawn[Random.Range(0, Respawn.length -1 )]);            
            }
        }
    }
    #endregion // HitSoldier method

    #region HitSoldierGrenade method
    function HitSoldierGrenade(hit : String)
    {
        if(GameManager.receiveDamage)
        {
            if(!audio.isPlaying)
            {
                if(life < 0.5 && (Random.Range(0, 100) < 30))
                {
                    audio.clip = dyingSound;
                }
                else
                {
                    audio.clip = hitSounds[Random.Range(0, hitSounds.length)];
                }

                audio.Play();
            }

            if(hit == "Player" && Soldier != null)
            { 
                #region highlight
                // Remote player send RPC back to original, 
                // to notify that this player is being hit.
                Soldier.SendMessage("HitByGrenade");
                return;
                #endregion // highlight
             }

            life -= 1.0;

            recoverTime = (1.0 - life) * 10.0;

            if(hit == "Dummy")
            {
                TrainingStatistics.dummiesHit++;
            }
            else if(hit == "Turret")
            {
                TrainingStatistics.turretsHit++;
            }

            if(life <= 0.0 && !SoldierController.dead)
            {
                SoldierController.dead = true;
                Soldier.SendMessage("Dead", Respawn[Random.Range(0, Respawn.length -1 )]);            
            }
        }
    }
    #endregion // HitSoldierGrenade method
    
    #region Update method    
    function Update()
    {
        recoverTime -= Time.deltaTime;

        if(recoverTime <= 0.0)
        {
            life += life * Time.deltaTime;

            life = Mathf.Clamp(life, 0.0, 1.0);

            hitAlpha = 0.0;
        }
        else
        {
            hitAlpha = recoverTime / ((1.0 - life) * 10.0);
        }

        if(!SoldierController.dead)
        {
            blackAlpha = 0.0;
            return;
        }

        blackAlpha += Time.deltaTime;

        if(blackAlpha >= 1.0)
        {
            // respawn
            life = 1.0;
            hitAlpha = 0.0;
            blackAlpha = 0.0;
            recoverTime = 0.0;
            SoldierController.dead = false;
        }
    }
    #endregion Update method

    function OnGUI()
    {
        if(!GameManager.receiveDamage) return;

        var oldColor : Color;
        var auxColor : Color;
        oldColor = auxColor = GUI.color;

        auxColor.a = hitAlpha;
        GUI.color = auxColor;
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), hitTexture);

        auxColor.a = blackAlpha;
        GUI.color = auxColor;
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), blackTexture);

        GUI.color = oldColor;
    }
}
