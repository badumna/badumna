#pragma strict
#pragma downcast
import System.Collections.Generic;

public var affected : List.<GameObject> = new List.<GameObject> ();

ActivateAffected (false);

function OnTriggerEnter (other : Collider) {
    if (other.tag == "Player")
        ActivateAffected (true);
}

function OnTriggerExit (other : Collider) {
    // Remove the original code to ensure that the RPC from the original is executed.
}

function ActivateAffected (state : boolean) {
    for (var go : GameObject in affected) {
        if (go == null)
            continue;
        if(state)
        {
            // Do not awaken the dead NPC
            var health : Health = go.GetComponent("Health") as Health;
            if(health != null && health.dead)
                continue;
        }

        go.SetActiveRecursively (state);
        yield;
    }
    for (var tr : Transform in transform) {
        if(state)
        {
            // Do not awaken the dead NPC
            health = tr.GetComponent("Health") as Health;
            if(health != null && health.dead)
                continue;
        }

        tr.gameObject.SetActiveRecursively (state);
        yield;
    }
}
