#pragma strict

var objectToDestroy : GameObject;

function OnSignal () {
    yield WaitForSeconds (0.25); // wait for 0.25 sec before destroying it.
    Spawner.Destroy (objectToDestroy);
}
