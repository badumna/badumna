using System.Collections;
using Badumna.DataTypes;
using Badumna.Match;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

public class PlayerManager : BaseBadumna
{
    // Remote Player prefab.
    public GameObject PlayerModel;

    // Local player game object.
    public GameObject Player;

    // Entity type
    private const uint EntityType = 0;

    // The local player character.
    private Player localPlayer;

    // A value indicating if the local player is registered with a Badumna network scene.
    private bool isRegistered;

    // The game instance.
    private Game game;

    #region RegisterEnemy method
    public void RegisterEnemy(Enemy enemy)
    {
        if(GameManager.Manager.IsHost &&
            this.game.State != GameState.MatchEnded)
        {
            if(!enemy.IsRegistered)
            {
                this.game.Match.RegisterHostedEntity(enemy, (uint)enemy.Id);
                enemy.Local = true;
                enemy.IsRegistered = true;
                enemy.Match = this.game.Match;
                enemy.OnUnregisterEntity += this.UnregisterEntity;
            }
        }
    }
    #endregion // RegisterEnemy method

    protected override void Awake()
    {
        base.Awake();
        this.game = GameObject.Find("Game").GetComponent<Game>();
        this.game.SetMatchDelegates(this.CreateReplica, this.RemoveReplica);
        this.game.Match.Controller.OnHostMigrated += this.HandleHostedEntityMigration;
    }

    private void Update()
    {
        switch(this.game.State)
        {
            case GameState.StartingMatch:
                this.RegisterEntity();
                this.RegisterProps();
                this.game.MatchStarted();
                break;
            case GameState.EndingMatch:
                this.UnregisterEntity();
                this.UnregisterHostedEntity();
                this.game.MatchEnded();
                this.game.LeaveMatch();                
                break;
        }
    }

    private void OnDisable()
    {
        if (this.isRegistered)
        {
            this.UnregisterEntity();
            this.UnregisterHostedEntity();
        }
    }

    #region RegisterProps method
    private void RegisterProps()
    {
        if (this.game.Match != null)
        {
            if(GameManager.Manager.IsHost &&
                this.game.State != GameState.MatchEnded)
            {
                foreach(var prop in GameManager.Manager.Props)
                {
                    if(!prop.IsRegistered)
                    {
                        this.game.Match.RegisterHostedEntity(prop, (uint)prop.Id);
                        prop.Local = true;
                        prop.IsRegistered = true;
                        prop.Match = this.game.Match;
                        prop.OnUnregisterEntity += this.UnregisterEntity;
                    }
                }
            }
        }
    }
    #endregion // RegisterProps method

    #region UnregisterEnemy method
    // Unregister entity handler.
    //  - entity: Entity need to be unregisterd from the match.
    private void UnregisterEntity(HostedEntity entity)
    {
        StartCoroutine(this.UnregisterEntityWithDelay(entity, 2));
    }

    // Unregister entity with delay.
    //  - entity: Entity need to be unregistered from the match.
    //  - delay: Delay in seconds.
    private IEnumerator UnregisterEntityWithDelay(HostedEntity entity, int delay)
    {
        yield return new WaitForSeconds(delay);
        this.game.Match.UnregisterHostedEntity(entity);
        entity.Local = false;
        entity.IsRegistered = false;
        entity.OnUnregisterEntity -= this.UnregisterEntity;
    }
    #endregion // UnregisterEnemy method

    #region UnregisterHostedEntity method
    // Unregister hosted entity from the match.
    private void UnregisterHostedEntity()
    {
        if (this.game.Match != null)
        {
            if(GameManager.Manager.IsHost && this.game.State == GameState.EndingMatch)
            {
                // Unregister all NPCs from the match.
                foreach(var enemy in GameManager.Manager.Enemies)
                {
                    if(enemy.IsRegistered)
                    {
                        this.game.Match.UnregisterHostedEntity(enemy);
                        enemy.Local = false;
                        enemy.IsRegistered = false;
                        enemy.OnUnregisterEntity -= this.UnregisterEntity;
                    }
                }

                // Unregister all Props from the match.
                foreach(var prop in GameManager.Manager.Props)
                {
                    if(prop.IsRegistered)
                    {
                        this.game.Match.UnregisterHostedEntity(prop);
                        prop.Local = false;
                        prop.IsRegistered = false;
                        prop.OnUnregisterEntity -= this.UnregisterEntity;
                    }
                }
            }
        }
    }
    #endregion // UnregisterHostedEntity method

    private void HandleHostedEntityMigration()
    {
        if(GameManager.Manager.IsHost)
        {
            foreach(var enemy in GameManager.Manager.Enemies)
            {
                if(enemy.IsRegistered)
                {
                    enemy.Local = true;
                }
            }

            foreach(var prop in GameManager.Manager.Props)
            {
                if(prop.IsRegistered)
                {
                    prop.Local = true;
                }
            }
        }
    }

    #region RegisterEntity method
    // Register entity with the match.
    private void RegisterEntity()
    {
        this.CreateLocalPlayer();

        if (this.game.Match != null)
        {
            if(this.localPlayer != null)
            {
                this.game.Match.RegisterEntity(this.localPlayer, EntityType);
                this.isRegistered = true;
            }            
        }
    }
    #endregion // RegisterEntity method

    #region UnregisterEntity method
    // Unregister entity from the match.
    private void UnregisterEntity()
    {
        if (this.localPlayer != null && this.game.Match != null)
        {            
            this.game.Match.UnregisterEntity(this.localPlayer);
            this.isRegistered = false;
            this.DisableLocalPlayer();
        }
    }
    #endregion // UnregisterEntity method

    // Create a local player.
    private void CreateLocalPlayer()
    {
        GameManager.Manager.AddPlayer(this.Player.transform);
        this.Player.transform.position = new Vector3(UnityEngine.Random.Range(-10.0f, -5.0f), 1.5f, UnityEngine.Random.Range(11.0f, 17.0f));
        this.localPlayer = this.Player.GetComponent<Player>();
        this.localPlayer.PlayerName = this.game.PlayerName;

        (this.Player.GetComponent("PlayerMoveController") as MonoBehaviour).enabled = true;
        var weapon = GameObject.Find("player/player_root/Bip001/Bip001 Spine/Bip001 Spine1/Bip001 Neck/Bip001 R Clavicle/Bip001 R UpperArm/Bip001 R Forearm/Bip001 R Hand/main_weapon001/WeaponSlot");
        (weapon.GetComponent("TriggerOnMouseOrJoystick") as MonoBehaviour).enabled = true;
    }

    // Disable local player.
    private void DisableLocalPlayer()
    {
        GameManager.Manager.RemovePlayer(this.Player.transform);
        this.Player.transform.position = new Vector3(UnityEngine.Random.Range(-10.0f, -5.0f), 1.5f, UnityEngine.Random.Range(11.0f, 17.0f));
        (this.Player.GetComponent("PlayerMoveController") as MonoBehaviour).enabled = false;
        var weapon = GameObject.Find("player/player_root/Bip001/Bip001 Spine/Bip001 Spine1/Bip001 Neck/Bip001 R Clavicle/Bip001 R UpperArm/Bip001 R Forearm/Bip001 R Hand/main_weapon001/WeaponSlot");
        (weapon.GetComponent("TriggerOnMouseOrJoystick") as MonoBehaviour).enabled = false;
    }

    #region replica delegates
    // Called by Badumna to create a replica entity.
    //  - match: The match the replica entity belongs to.
    //  - source: Type of entity source (e.g. peer, hosted entity).
    //  - entityType: An integer indicating the type of entity to create.
    // Returns: A new replica entity.
    private object CreateReplica(Match match, EntitySource source, uint entityType)
    {
        if (entityType == EntityType)
        {
            var replica = (GameObject)Instantiate(this.PlayerModel, Vector3.zero, Quaternion.identity);
            var remotePlayer = replica.GetComponent<Player>();

            GameManager.Manager.AddPlayer(replica.transform);
            return remotePlayer;
        }
        else if(entityType > 0)
        {
            var enemy = GameManager.Manager.Enemies.Find((Enemy e) => { return e.Id == (int)entityType; });
            var prop = GameManager.Manager.Props.Find((Prop e) => { return e.Id == (int)entityType; });

            if(enemy != null)
            {
                enemy.IsRegistered = true;
                enemy.Match = this.game.Match;
                enemy.OnUnregisterEntity += this.UnregisterEntity;
                enemy.gameObject.active = true;
                return enemy;
            }
            else if(prop != null)
            {
                prop.IsRegistered = true;
                prop.Match = this.game.Match;
                prop.OnUnregisterEntity += this.UnregisterEntity;
                prop.gameObject.active = true;
                return prop;
            }
            else
            {
                Debug.LogError(string.Format("Error: Enemy with id {0} is not found", entityType));
            }
        }

        return null;
    }

    // Called by Badumna to remove a replica when it leaves the match.
    //  - match: The match the replica is being removed from.
    //  - source: Type of entity source (e.g. peer, hosted entity).
    //  - replica: The replica to removed.
    private void RemoveReplica(Match match, EntitySource source, object replica)
    {
        if(replica is Player)
        {
            GameManager.Manager.RemovePlayer((replica as Player).transform);
            Destroy((replica as Player).gameObject);
        }
        else if (replica is HostedEntity)
        {
            var entity = replica as HostedEntity;
            entity.Local = false;
            entity.IsRegistered = false;
            entity.OnUnregisterEntity -= this.UnregisterEntity;
        }
    }
    #endregion // replica delegates
}
