using UnityEngine;
using System.Collections;

public class BaseBadumna : MonoBehaviour 
{
    // Badumna cloud instance.
    private BadumnaCloud badumnaCloud;

    // Gets the Badumna network.
    protected BadumnaCloud BadumnaCloud
    {
        get { return this.badumnaCloud; }
    }

    protected virtual void Awake()
    {
        if(GameObject.Find("Badumna") == null)
        {   
            Debug.Log("Can't found Badumna object");
            return;
        }

        this.badumnaCloud = GameObject.Find("Badumna").GetComponent<BadumnaCloud>();        
    }
}
