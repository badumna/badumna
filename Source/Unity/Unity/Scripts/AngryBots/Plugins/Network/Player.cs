using System.Collections;
using Badumna;
using Badumna.SpatialEntities;
using Badumna.Autoreplication;
using UnityEngine;

using Vector3 = Badumna.DataTypes.Vector3;

public class Player : BaseBadumna
{
    public GameObject weaponSlot;

    // Gui text for displaying local player name.
    public GUIText nameText;

    // Display overhead player name
    public GameObject overheadName;

    // A value indicating whether this script is attached to a local player object.
    public bool Local;

    // Player name
    private string playerName = string.Empty;

    // A value indicating whether the player is currently firing.
    private bool fire;

    // Target rotation in y-axis.
    private float targetRotation;

    #region replicable properties
    // Gets or sets player position
    [Replicable]
    [Smoothing(Interpolation = 200, Extrapolation = 0)]
    public Vector3 Position
    {
        get
        {
            var position = this.gameObject.transform.position;
            return new Vector3(position.x, position.y, position.z);
        }

        set
        {
            this.gameObject.transform.position = new UnityEngine.Vector3(value.X, value.Y, value.Z);
        }
    }

    // Gets or sets player orientation
    [Replicable]
    public float Orientation
    {
        get
        {
            return this.gameObject.transform.rotation.eulerAngles.y;
        }

        set
        {
            this.targetRotation = value;            
        }
    }    

    // Gets or sets the player name
    [Replicable]
    public string PlayerName
    {
        get
        {
            return this.playerName;
        }

        set
        {
            this.playerName = value;
            this.transform.name = value;
            
            if (this.Local)
            {
                this.nameText.text = value;
            }
            else
            {
                if (this.overheadName != null)
                {
                    var textMesh = this.overheadName.GetComponent<TextMesh>();
                    textMesh.text = value;                    
                }
            }
        }
    }

    // Gets or sets a value indicate whether the player is firing.
    [Replicable]
    public bool Fire 
    { 
        get
        {
            return this.fire;
        } 
        
        set
        {
            this.fire = value;
            if(value)
            {
                this.weaponSlot.SendMessage("OnStartFire");
            }
            else
            {
                this.weaponSlot.SendMessage("OnStopFire");                
            }
        } 
    }

    #endregion // replicable properties
    

    // Start fire
    public void OnStartFire()
    {       
        this.fire = true; 
    }    

    // Stop fire
    public void OnStopFire()
    {
        this.fire = false;
    }

    #region Dead method
    // Dead method is called by the local player.
    public void Dead()
    {        
    }
    #endregion // Dead method
    

    // Update the state and overhead name position and rotation.
    private void Update()
    {       
        if (this.overheadName != null)
        {
            var camDistance = this.overheadName.transform.position - Camera.main.transform.position;
            this.overheadName.transform.LookAt(this.overheadName.transform.position + camDistance);
        }

        if(!this.Local)
        {
            // Apply rotation smoothing.
            this.gameObject.transform.rotation = Quaternion.Slerp(
                this.gameObject.transform.rotation,
                Quaternion.Euler(new UnityEngine.Vector3(0.0f, this.targetRotation, 0.0f)),
                Time.deltaTime * 10);
        }
    }

    // Destroy the overhead name object when the script became inactive.
    private void OnDisable()
    {
        if (this.overheadName != null)
        {
            Destroy(this.overheadName);
        }
    }
}
