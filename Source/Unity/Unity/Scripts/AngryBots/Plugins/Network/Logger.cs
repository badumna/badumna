using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Logger : BaseBadumna 
{
    private const int maxMessageCount = 20;

    private List<string> messages = new List<string>();

    private List<string> pendingMessage = new List<string>();

    private Rect debugWindow = new Rect(0, Screen.height - 250, 300, 250);

    private Vector2 scrollPosition;

    public void LogMessage(string message)
    {
        var text = System.DateTime.Now.ToString("HH:mm:ss") + " " + message;
        this.pendingMessage.Add(text);
    }

    protected override void Awake()
    {
        base.Awake();
        //this.BadumnaCloud.Match.OnLogMessage += this.LogMessage;
    }

    private void Update()
    {
        lock(this.pendingMessage)
        {
            foreach(var m in this.pendingMessage)
            {
                if(this.messages.Count > maxMessageCount)
                    this.messages.RemoveAt(0);

                this.messages.Add(m);
                Debug.Log(m);
            }

            this.pendingMessage.Clear();
        }
    }

    private void OnGUI()
    {
        this.debugWindow = GUI.Window(0, this.debugWindow, this.DebugWindow, string.Empty);
    }

    private void OnDisable()
    {
    }

    private void DebugWindow(int id)
    {
        this.scrollPosition = GUILayout.BeginScrollView(this.scrollPosition);
        foreach(var message in this.messages)
        {
            GUILayout.Label(message);
        }
        GUILayout.EndScrollView();
        GUI.DragWindow();
    }
}
