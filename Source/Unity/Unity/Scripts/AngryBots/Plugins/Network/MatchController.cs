using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Badumna;
using Badumna.Autoreplication;
using Badumna.Match;

public delegate void MatchEventHandler();
public delegate void KickedFromMatchHandler(string reason);
public delegate void HostMigrationHandler();

public class MatchController 
{
    private Dictionary<string, int> playerScores = new Dictionary<string,int>();

    private List<MemberIdentity> members = new List<MemberIdentity>();

    private List<string> readyMembers = new List<string>();

    public event MatchEventHandler OnMatchStarted;

    public event MatchEventHandler OnMatchEnded;

    public event MatchEventHandler OnLoadGameScene;

    public event KickedFromMatchHandler OnKicked;

    public event HostMigrationHandler OnHostMigrated;

    public bool MembersReady
    {
        get
        {
            return this.readyMembers.Count == this.members.Count -1;
        }
    }

    public IEnumerable<MemberIdentity> Members
    {
        get { return this.members; }
    }   

    public void AddMember(Match match, MatchMembershipEventArgs e)
    {
        this.members.Add(e.Member);
        this.playerScores.Add(e.Member.Name, 0);
    }

    public void RemoveMember(Match match, MatchMembershipEventArgs e)
    {
        this.members.Remove(e.Member);
        this.playerScores.Remove(e.Member.Name);
    }

    public string GenerateGameScore()
    {
        var gameScore = string.Empty;

        foreach(var keyValuePair in this.playerScores)
        {
            gameScore += string.Format("{0}\t\t{1}\n", keyValuePair.Key, keyValuePair.Value);
        }

        return gameScore;
    }

    public void HostMigrated()
    {
        var handler = this.OnHostMigrated;
        if(handler != null)
        {
            handler.Invoke();
        }
    }

    #region RPC methods
    [Replicable]
    public void StartMatch()
    {
        var handler = this.OnMatchStarted;
        if(handler != null)
        {
            handler.Invoke();
        }
    }

    [Replicable]
    public void LoadingScene()
    {
        var handler = this.OnLoadGameScene;
        if(handler != null)
        {
            handler.Invoke();
        }
    }

    [Replicable]
    public void EndMatch()
    {
        var handler = this.OnMatchEnded;
        if(handler != null)
        {
            handler.Invoke();
        }
    }

    [Replicable]
    public void KickMember(string reason)
    {
        var handler = this.OnKicked;
        if(handler != null)
        {
            handler.Invoke(reason);
        }
    }   

    [Replicable]
    public void AddScore(string playerName)
    {
        if(!this.playerScores.ContainsKey(playerName))
        {
            Debug.LogError("Trying to add a score for unknown player");
        }   

        this.playerScores[playerName]++;
    }

    [Replicable]
    public void Ready(string name)
    {
        this.readyMembers.Add(name);
    }
    #endregion // RPC methods
}
