using UnityEngine;
using Badumna;

public class Prop : HostedEntity
{
    #region RPC method
	[Replicable]
    public void AnimateDeactivate()
    {
    	this.gameObject.SendMessage("Die");
    }
    #endregion // RPC method

    public void Deactivate()
    {
        if(Local)
        {
            Debug.Log("Deactivate is called");
            this.Match.CallMethodOnReplicas(this.AnimateDeactivate);
            this.UnregisterEntity();
        }
    }

    protected override void Awake()
    {
        base.Awake();
        GameManager.Manager.RegisterProp(this);
    }

    private void OnEnable()
    {
        // workaround for script execution order
        // see http://forum.unity3d.com/threads/98265-Script-Execution-Order-wrong-in-Unity3D-3-4
    }
}