using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Badumna;
using Badumna.DataTypes;
using Badumna.Match;
using Badumna.Security;
using Badumna.SpatialEntities;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

// Different game state.
public enum GameState
{
    Initializing,
    Lobby,
    CreatingMatch,
    FindingMatch,
    JoiningMatch,
    JoinedMatch,
    StartingMatch,
    MatchStarted,
    EndingMatch,
    MatchEnded,
    Loading,
    Kicked
};

public class Game : BaseBadumna
{            
    // Local player name.
    public string PlayerName = "default";

    // Current match name.
    public string MatchName = string.Empty;

    // The current game state.
    private GameState state = GameState.Initializing;

    // The current match.
    private Match<MatchController> match;

    // Match list.
    private List<MatchmakingResult> matchList = new List<MatchmakingResult>();

    // A value indiciating whether the match query has been handled.
    private bool matchQueryHandled;

    // Match controller
    private MatchController controller;

    // Create replica delegate;
    private CreateReplica createReplica;

    // Remove replica delegate;
    private RemoveReplica removeReplica;

    // Gets the current game state.
    public GameState State
    {
        get { return this.state; }
    }

    // Gets a value indicating whether the find match query is done.
    public bool FindMatchFinished
    {
        get { return this.matchQueryHandled; }
    }

    // Gets the match list.
    public List<MatchmakingResult> MatchList
    {
        get { return this.matchList; }
    }

    // Gets the current match.
    public Match<MatchController> Match
    {
        get { return this.match; }
    }

    public string GameScore { get; private set; }

    #region CreateMatch method
    // Creating a match
    public void CreateMatch(string matchName)
    {
        var criteria = new MatchmakingCriteria
            {
                PlayerGroup = 0,
                MatchName = matchName
            };
        this.MatchName = matchName;
        this.controller = new MatchController();
        this.state = GameState.CreatingMatch;
        this.match = this.BadumnaCloud.Match.CreateMatch<MatchController>(
            this.controller,
            criteria, 
            4, 
            this.PlayerName,
            this.CreateReplica,
            this.RemoveReplica);
        this.match.StateChanged += this.OnMatchStatusChange;
        this.match.MemberAdded += this.controller.AddMember;
        this.match.MemberAdded += this.NewMemberAdded;
        this.match.MemberRemoved += this.controller.RemoveMember;
    }
    #endregion // CreateMatch method  

    #region FindMatch method
    // Find existing matches.
    public void FindMatch()
    {
        var criteria = new MatchmakingCriteria
            {
                PlayerGroup = 0
            };
        this.matchQueryHandled = false;
        this.state = GameState.FindingMatch;
        this.BadumnaCloud.Match.FindMatches(
            criteria,
            this.HandleMatchQuery);
    }
    #endregion // FindMatch method

    // Back to lobby.
    public void GoToLobby()
    {
        this.state = GameState.Lobby;
    }

    #region JoinMatch method
    // Join an exsisting match
    //  - matchToJoin: Match to join.
    public void JoinMatch(MatchmakingResult matchToJoin)
    {
        this.MatchName = matchToJoin.Criteria.MatchName;
        this.controller = new MatchController();
        this.state = GameState.JoiningMatch;
        this.match = this.BadumnaCloud.Match.JoinMatch<MatchController>(
            this.controller,
            matchToJoin,
            this.PlayerName,
            this.CreateReplica,
            this.RemoveReplica);
        this.match.StateChanged += this.OnMatchStatusChange;
        this.match.MemberAdded += this.controller.AddMember;
        this.match.MemberRemoved += this.controller.RemoveMember;
    }
    #endregion // JoinMatch method

    // Starting a match.
    public void StartMatch()
    {
        this.Match.CallMethodOnMembers(this.Match.Controller.LoadingScene);
        this.HandleLoadGameScene();
    }
    
    // Leaving from a match.
    public void LeaveMatch()
    {
        if(this.match != null)
        {
            this.match.StateChanged -= this.OnMatchStatusChange;
            this.match.Leave();        
            this.match = null;
        }
    }

    // Reload the lobby scene.
    public void ReloadGameScene()
    {
        // Reload the Scene1-Lobby level
        GameManager.Manager = null;
        Application.LoadLevel("Scene1-Lobby");
        Destroy(this.gameObject);        
    }

    // Match just started.
    public void MatchStarted()
    {
        this.state = GameState.MatchStarted;
    }

    // Match just ended.
    public void MatchEnded()
    {
        this.GameScore = this.match.Controller.GenerateGameScore();
        this.state = GameState.MatchEnded;
        Screen.showCursor = true;                
    }

    // Set the creation and removal delegates.
    public void SetMatchDelegates(CreateReplica createReplica, RemoveReplica removeReplica)
    {
        this.createReplica = createReplica;
        this.removeReplica = removeReplica;
    }

    // Called by Unity when this script is loaded.
    protected override void Awake()
    {
        DontDestroyOnLoad(this.transform.gameObject);
        base.Awake(); 

        this.PlayerName = "Player-" + UnityEngine.Random.Range(0, 1000);

        if(this.BadumnaCloud.IsLoggedIn)
        {
            this.state = GameState.Lobby;
            this.PlayerName = this.BadumnaCloud.Character.Name;
        }       
    }

    // Called by Unity before the first call to FixedUpdate.
    // Returns: An enumerator as per Unity coroutine requirements.
    private IEnumerator Start()
    {        
        while (this.state != GameState.Lobby)
        {
            yield return null;
        }

        if(!this.BadumnaCloud.IsLoggedIn)
        {
            // we must login before attempting to use the network
            var loginResult = BadumnaCloud.Login(this.PlayerName);

            if (!loginResult)
            {
                Debug.LogError("Failed to login");
                yield break;
            }
        }        
    }

    private void OnApplicationPause(bool pause)
    {
        if(pause)
        {
            this.OnApplicationQuit();
        }
    }

    // Called by Unity when this script is disabled.
    private void OnApplicationQuit()
    {        
        if(this.match != null)
            this.match.Leave();

        this.BadumnaCloud.Shutdown();
        Destroy(this.BadumnaCloud.gameObject);        
    }

    
    // Handle changed match status
    //  - match: The match.
    //  - e: Match status event arguments.
    private  void OnMatchStatusChange(Match match, MatchStatusEventArgs e)
    {
        Debug.Log(e.Status.ToString());

        if(e.Status == MatchStatus.Closed && this.state != GameState.Kicked)
        {
            this.match = null;
            this.state = GameState.Lobby;
        }
        else if(e.Status == MatchStatus.Hosting || e.Status == MatchStatus.Connected)
        {
            GameManager.Manager.IsHost = e.Status == MatchStatus.Hosting;
            if(this.state != GameState.MatchStarted && this.state != GameState.MatchEnded)
            {
                this.match.Controller.OnMatchStarted += this.HandleMatchStart;
                this.match.Controller.OnMatchEnded += this.HandleMatchEnd;
                this.match.Controller.OnLoadGameScene += this.HandleLoadGameScene;
                this.match.Controller.OnKicked += this.OnKickedByHost;
                this.state = GameState.JoinedMatch; // TODO: handle host migration
            }
            else if(this.state == GameState.MatchStarted && GameManager.Manager.IsHost)
            {
                // Host just migrated
                this.match.Controller.HostMigrated();
            }
        }        

        if (e.Error != MatchError.None)
        {
            Debug.LogError("Error: " + e.Error.ToString());
        }  
    }

    // Handle match query with the results.
    //  - results: List of all suitable matches.
    private void HandleMatchQuery(MatchmakingQueryResult result)
    {
        this.matchList.Clear();

        foreach(var r in result.Results)
        {
            this.matchList.Add(r);
        }

        this.matchQueryHandled = true;

        if(result.Error != MatchError.None)
        {
            Debug.LogError("Error: " + result.Error.ToString());
        }
    }

    // Should load game scene in this handler.
    private void HandleLoadGameScene()
    {
        this.state = GameState.Loading;
        this.LoadGameScene();

        if(GameManager.Manager.IsHost)
        {
            StartCoroutine(this.StartGameDelay());
        }
        else
        {
            this.match.CallMethodOnHost(this.match.Controller.Ready, this.PlayerName);
        }
    }

    // Handle match start.
    private void HandleMatchStart()
    {
        // sync the time
        var timer = this.gameObject.GetComponent<Timer>();
        timer.StartTimer(
            () => 
            {
                // Handle end match.
                if(GameManager.Manager.IsHost)
                {
                    this.Match.CallMethodOnMembers(this.match.Controller.EndMatch);                    
                    this.HandleMatchEnd();
                }
            });
        this.state = GameState.StartingMatch;
    }

    // Handle match end.
    private void HandleMatchEnd()
    {
        this.state = GameState.EndingMatch;
    }

    // Will be called when a new member joined.
    //  - match: The match.
    //  - e: Match membership event arguments.
    private void NewMemberAdded(Match match, MatchMembershipEventArgs e)
    {
        // Kicked new member if match already started.
        if(GameManager.Manager.IsHost)
        {
            if(this.state != GameState.JoinedMatch)
            {
                this.match.CallMethodOnMember(e.Member, this.match.Controller.KickMember, "Match already started.");
            }
        }
    }

    // On kicked by host.
    //   - reason: Reason why the member being kicked by the host.
    private void OnKickedByHost(string reason)
    {
        this.state = GameState.Kicked;
        this.LeaveMatch();
        Debug.Log(reason);
    }

    // Load the angry bot scene.
    private void LoadGameScene()
    {
        Camera.main.enabled = false;
        Application.LoadLevelAdditive("AngryBots");
    }

    private IEnumerator StartGameDelay()
    {
        while(!this.match.Controller.MembersReady)
            yield return null;
        
        this.match.CallMethodOnMembers(this.match.Controller.StartMatch);
        this.HandleMatchStart();
    }

    // Called by Badumna to create a replica entity.
    //  - match: The match the replica entity belongs to.
    //  - source: Type of entity source (e.g. peer, hosted entity).
    //  - entityType: An integer indicating the type of entity to create.
    // Returns: A new replica entity.
    private object CreateReplica(Match match, EntitySource source, uint entityType)
    {
        if(this.createReplica == null)
            return null;

        return this.createReplica.Invoke(match, source, entityType);
    }

     // Called by Badumna to remove a replica when it leaves the match.
    //  - match: The match the replica is being removed from.
    //  - source: Type of entity source (e.g. peer, hosted entity).
    //  - replica: The replica to removed.
    private void RemoveReplica(Match match, EntitySource source, object replica)
    {
        if(this.removeReplica != null)
            this.removeReplica.Invoke(match, source, replica);
    }
}
