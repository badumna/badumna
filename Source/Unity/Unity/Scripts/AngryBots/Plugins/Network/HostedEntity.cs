using UnityEngine;
using Badumna.Match;

public delegate void UnregisterEntityHandler(HostedEntity entity);

public class HostedEntity : BaseBadumna
{
	// Entity id.
    public int Id;

    // A value indicating whether this script is attached to a local player object.
    public bool Local;

    // Match in which this entity belongs to.
    public Match<MatchController> Match;

    // Unregister entity handler.
    public event UnregisterEntityHandler OnUnregisterEntity;
   
    // Gets or sets a value indicating whether the entity is registered.
    public bool IsRegistered { get; set;}

    protected override void Awake()
    {
        base.Awake();
    }

    protected void UnregisterEntity()
    {
        var handler = this.OnUnregisterEntity;
        if(handler != null)
            handler.Invoke(this);
    }
}