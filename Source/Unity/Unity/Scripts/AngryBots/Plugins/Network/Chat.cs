using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Badumna.Match;

public class Chat : BaseBadumna
{
    private List<string> messages = new List<string>();

    private float height = 175;

    private float padding = 15;

    private Vector2 scrollPosition = Vector2.zero;

    private string text = string.Empty;

    // An instance of game class.
    private Game game;

    #region Awake method
    protected override void Awake()
    {
        base.Awake();
        this.game = GameObject.Find("Game").GetComponent<Game>();
        this.game.Match.ChatMessageReceived += this.HandleChatMessage;     
    }
    #endregion // Awake method

    private void OnGUI () 
    {
        GUILayout.BeginArea(new Rect(this.padding, Screen.height - this.height - this.padding, Screen.width, this.height));

        this.scrollPosition = GUILayout.BeginScrollView(scrollPosition);
        foreach(var message in this.messages)
        {
            GUI.color = Color.red;
            GUILayout.Label(message);
        }
        GUILayout.EndScrollView();

        GUI.color = Color.white;

        GUILayout.BeginHorizontal();
        GUI.SetNextControlName("chat");
        this.text = GUILayout.TextField(this.text, GUILayout.Width(200));

        if(Event.current.type == EventType.keyDown && Event.current.character == '\n')
        {
            if(GUI.GetNameOfFocusedControl() != "chat")
            {
                GUI.FocusControl("chat");                
            }
            else
            {
                this.SendChatMessage();
                GUI.FocusControl("");                
            }
        }

        if(GUILayout.Button("Send"))
        {
            this.SendChatMessage();
        }
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        GUILayout.EndArea();
        GUI.SetNextControlName("");        
    }

    #region HandleChatMessage method
    private void HandleChatMessage(Match match, MatchChatEventArgs e)
    {
        this.messages.Add(string.Format("{0}: {1}", e.Sender.Name, e.Message));
    }
    #endregion // HandleChatMessage method

    #region SendChatMessage method
    private void SendChatMessage()
    {
        this.messages.Add(string.Format("{0}: {1}", this.game.Match.MemberIdentity.Name, this.text));
        this.game.Match.Chat(this.text);
        this.text = string.Empty;
    }
    #endregion // SendChatMessage method
}
