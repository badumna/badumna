using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Badumna;
using Badumna.Autoreplication;
using Badumna.DataTypes;
using Badumna.Match;
using Badumna.Security;
using Badumna.SpatialEntities;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

public class BadumnaCloud : MonoBehaviour 
{
    // Path to the badumna cloud setting path.
    private const string BadumnaCloudSettingsPath = "Assets/Badumna/Resources/BadumnaCloudSettings.asset";

    // Serialized badumna cloud settings, that has been setup through Badumna Cloud wizzard.
    private static BadumnaCloudSettings CloudSettings;

    // The Badumna network.
    private INetworkFacade network;

    // For displaying initialization status on screen.
    private GUIText status;

    // Result of initializing Badumna.
    private IAsyncResult initializationResult;

    // Key pair in xml string representation.
    private string keyPairXml;

    // Gets a value indicating whether it is logged in to Badumna.
    public bool IsLoggedIn
    {
        get
        {
            if(this.network == null)
                return false;

            return this.network.IsLoggedIn;
        }
    }

    // Gets tha match facade.
    public Facade Match
    {
        get
        {
            if(this.network == null)
                return null;

            return this.network.Match;
        }
    }

    // Gets the character info.
    public Character Character
    {
        get
        {
            if(this.network == null)
                return null;

            return this.network.Character;
        }
    }

    #region login badumna
    // Login to Badumna.
    //  - characterName: The player/character name.
    public bool Login(string characterName)
    {
        if(this.network == null)
            return false;

        return this.network.Login(new UnverifiedIdentityProvider(characterName, this.keyPairXml));
    }
    #endregion // login badumna

    #region shutting down badumna
    public void Shutdown()
    {
        if(this.network == null)
            return;

        this.network.Shutdown();
        this.network = null;
    }
    #endregion // shutting down badumna

    #region badumna initialization
    // Called by Unity when this script is loaded.
    private void Awake () 
    {
        DontDestroyOnLoad(this.transform.gameObject);
        CloudSettings = (BadumnaCloudSettings) Resources.Load("BadumnaCloudSettings", typeof(BadumnaCloudSettings));

        if(CloudSettings == null)
        {
            Debug.LogError(string.Format("Missing Badumna cloud settings. The settings should be located in {0}", BadumnaCloudSettingsPath));
            return;
        }

        if(string.IsNullOrEmpty(CloudSettings.ApplicationIdentifier))
        {
            Debug.LogError("Please make sure that you have setup the application identifier correctly.");
            return;
        }

        var go = new GameObject();
        go.transform.position = new Vector3(0.05f, 0.05f, 0);
        this.status = go.AddComponent<GUIText>();
        this.status.anchor = TextAnchor.LowerLeft;
        this.status.text = "Initializing Badumna...";
        
#if DEBUG
        System.Environment.SetEnvironmentVariable(
            "BADUMNA_CLOUD_URI",
            "http://184.72.143.166");
        System.Environment.SetEnvironmentVariable(
            "BADUMNA_TRACKER",
            "184.72.143.166");

#endif // DEBUG
        this.initializationResult = NetworkFacade.BeginCreate(
            CloudSettings.ApplicationIdentifier,
            null);
    }
    #endregion // badumna initialization
    
    private IEnumerator Start()
    {
        if(this.initializationResult == null)
            yield break;

        this.keyPairXml = this.GenerateKeyPair();
        while (!this.initializationResult.IsCompleted)
        {
            yield return null;
        }

        if (this.network == null)
        {
            try
            {
                this.network = NetworkFacade.EndCreate(this.initializationResult);
                Destroy(this.status.gameObject);
            }
            catch (Exception ex)
            {
                var errorMessage = "Badumna initialization failed: " + ex.Message;
                this.status.text = errorMessage;
                Debug.LogError(errorMessage);
                yield break;
            }
        }
    }

    #region FixedUpdate method
    // Called by Unity once per frame.
    private void Update()
    {
        if (this.network == null || !this.network.IsLoggedIn)
        {
            return;
        }

        this.network.ProcessNetworkState();
    }
    #endregion // FixedUpdate method

    private void OnDisable()
    {
        if (!this.initializationResult.IsCompleted)
        {
            try
            {
                this.network = NetworkFacade.EndCreate(this.initializationResult);
            }
            catch { }
        }

        if (this.status != null)
        {
            Destroy(this.status.gameObject);
        }
    }

    // Since generating a key pair can be slow on some devices, this method attempts to load
    // a cached key pair from a file, and only generates (and caches) a key pair if none is
    // found.
    private string GenerateKeyPair()
    {
        string keyFileName = "key.bin";
        string keyPairXml;
        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
        {
            var saveFilePath = Path.Combine(Application.persistentDataPath, keyFileName);
            if (File.Exists(saveFilePath))
            {
                keyPairXml = File.ReadAllText(saveFilePath);
            }
            else
            {
                keyPairXml = UnverifiedIdentityProvider.GenerateKeyPair();
                File.WriteAllText(saveFilePath, keyPairXml);
            }
        }
        else
        {
            keyPairXml = UnverifiedIdentityProvider.GenerateKeyPair();
        }

        return keyPairXml;
    }
}
