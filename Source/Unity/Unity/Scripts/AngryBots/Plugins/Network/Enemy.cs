using UnityEngine;
using System.Collections;
using Badumna;
using Badumna.Autoreplication;
using Badumna.Match;
using Badumna.SpatialEntities;
using Vector3 = Badumna.DataTypes.Vector3;

public class Enemy : HostedEntity
{
    // AI Attack transform.
    public Transform AIAttack;

    // Target rotation in y-axis.
    private float targetRotation;

    // Target enemy position.
    private UnityEngine.Vector3 targetPosition;

    #region replicable properties
    // Gets or sets player position
    [Replicable]
    [Smoothing(Interpolation = 200, Extrapolation = 0)]
    public Vector3 Position
    {
        get
        {
            var position = this.gameObject.transform.position;
            return new Vector3(position.x, position.y, position.z);
        }

        set
        {
            this.targetPosition = new UnityEngine.Vector3(value.X, value.Y, value.Z);
        }
    }

    // Gets or sets player orientation
    [Replicable]
    public float Orientation
    {
        get
        {
            return this.gameObject.transform.rotation.eulerAngles.y;
        }

        set
        {
            this.targetRotation = value;
        }
    }  

    #endregion //replicable properties

    #region replica RPC method
    [Replicable]
    public void AnimateDeath()
    {
        this.gameObject.SendMessage("Die");
    }

    [Replicable]
    public void ExplodeEffect()
    {
        if(this.AIAttack != null)
        {
            this.AIAttack.SendMessage("Explode");
        }
        else
        {
            Debug.LogError("AIAttack transform is not set.");
        }
    }

    #endregion // replica RPC method

    #region Dead method

    // Dead method is called by the local player.
    // - killer : The killer name.
    public void Dead(string killer)
    {     
        if(Local)
        {
            if(string.IsNullOrEmpty(killer))
            {
                Debug.LogError("The killer name can't be empty");
            }

            this.Match.Controller.AddScore(killer);
            this.Match.CallMethodOnMembers(this.Match.Controller.AddScore, killer);
            this.Match.CallMethodOnReplicas(this.AnimateDeath);   
            this.UnregisterEntity();
        }
    }

    // Explode method is called by the local player.
    public void Exploded()
    {
        if(Local)
        {            
            this.Match.CallMethodOnReplicas(this.ExplodeEffect);   
            this.UnregisterEntity();            
        }
    }
    
    #endregion // Dead method

    protected override void Awake()
    {
        base.Awake();
        GameManager.Manager.RegisterNPC(this);
        this.targetPosition = this.gameObject.transform.position;
    }

    private void Update()
    {
        if(!this.Local)
        {
            // Apply target position
            this.gameObject.transform.position = this.targetPosition;

            // Apply rotation smoothing.
            this.gameObject.transform.rotation = Quaternion.Slerp(
                this.gameObject.transform.rotation,
                Quaternion.Euler(new UnityEngine.Vector3(0.0f, this.targetRotation, 0.0f)),
                Time.deltaTime * 10);
        }
    }

    private void OnEnable()
    {
        // workaround for script execution order
        // see http://forum.unity3d.com/threads/98265-Script-Execution-Order-wrong-in-Unity3D-3-4
        StartCoroutine(this.RegisterEnemy());
    }

    private IEnumerator RegisterEnemy()
    {
        var game = GameObject.Find("Game").GetComponent<Game>();

        while(game.State != GameState.MatchStarted)
            yield return null;

        var manager = GameObject.Find("PlayerManager").GetComponent<PlayerManager>();
        manager.RegisterEnemy(this);
    }
}
