using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour 
{
    public static GameManager Manager;
    
    private List<Enemy> enemies = new List<Enemy>();

    private List<Prop> props = new List<Prop>();

    public bool IsHost;

    private List<Transform> players = new List<Transform>();

    public List<Enemy> Enemies
    {
        get { return enemies; }
    }

    public List<Prop> Props
    {
        get { return props; }
    }

    public void RegisterNPC(Enemy enemy)
    {
        if(enemies.Contains(enemy))
            return;
            
        enemies.Add(enemy);
    }

    public void RegisterProp(Prop prop)
    {
        if(props.Contains(prop))
            return;

        props.Add(prop);
    }

    public Transform GetClosestPlayer(Vector3 relativePosition)
    {
        Transform player = null;
        var minDist = -1.0f;

        foreach(var t in this.players)
        {
            if(t == null)
                continue;
            
            var dist = Vector3.Distance(t.position, relativePosition);

            if(minDist == -1 || dist < minDist)
            {
                minDist = dist;
                player = t;
            }
        }

        return player;
    }

    public void AddPlayer(Transform t)
    {
        this.players.Add(t);
    }

    public void RemovePlayer(Transform t)
    {
        this.players.Remove(t);
    }   

    private void Awake () 
    {
        if(Manager == null)
        {
            Manager = this;
        }
    }

    private void OnEnable()
    {
        // workaround for script execution order
        // see http://forum.unity3d.com/threads/98265-Script-Execution-Order-wrong-in-Unity3D-3-4
    }

    private void OnApplicationPause(bool pause)
    {
        if(!pause)
        {
            Application.LoadLevel("Scene0-Logo");
            Destroy(this.gameObject);
        }
    }
}