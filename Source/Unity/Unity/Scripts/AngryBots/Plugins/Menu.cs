using UnityEngine;
using System.Collections;

public class Menu : BaseBadumna
{
    // GUI Skin
    public GUISkin Skin;
    
    // Game instance
    public Game Game;

    // Lobby background
    public Texture2D background;

    // Loading background
    public Texture2D loadingBackground;

    // GUI styles
    private GUIStyle labelStyle;
    private GUIStyle buttonStyle;
    private GUIStyle textfieldStyle;

    private string matchName = "default";

#if DEBUG
    private Logger logger;
#endif // DEBUG

    protected override void Awake()
    {
        base.Awake();
##if UNITY_IPHONE || UNITY_ANDROID
        this.labelStyle = this.Skin.FindStyle("label_mobile");
        this.buttonStyle = this.Skin.FindStyle("button_mobile");
        this.textfieldStyle = this.Skin.FindStyle("textfield_mobile");
##else
        this.labelStyle = this.Skin.label;
        this.buttonStyle = this.Skin.button;
        this.textfieldStyle = this.Skin.textField;
##endif
    
        this.matchName = "Match-" + UnityEngine.Random.Range(0, 1000);
#if DEBUG
        this.logger = GameObject.Find("Game").GetComponent<Logger>();
#endif // DEBUG
    }

    // Display the game lobby.
    private void OnGUI()
    {
        GUI.skin = this.Skin;

        var width = 410; //Screen.width / 2.5f;
        var height = 384; //Screen.height / 2f;

        if(this.Game.State == GameState.Loading)
        {
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), this.loadingBackground);
        }
        else if(this.Game.State != GameState.MatchStarted)
        {
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), this.background);
        }

        GUILayout.BeginArea(new Rect((int)(((Screen.width / 2) - (width / 2))), (int)(((Screen.height / 2) - (height / 2))), width, height));

        switch(this.Game.State)
        {
            case GameState.Initializing:
                GUILayout.Label("Angry Bot - Multiplayer", this.labelStyle);
                GUILayout.Space(20);

                GUILayout.Label("Name:", this.labelStyle);

                GUI.SetNextControlName("registerName");
                this.Game.PlayerName = GUILayout.TextField(this.Game.PlayerName, this.textfieldStyle, GUILayout.Width((int)((width / 1.5f))));

                var buttonDown = GUILayout.Button("OK", this.buttonStyle, GUILayout.Width((int)((width / 1.5f))));
                var keyDown = Event.current.type == EventType.keyDown && Event.current.character == '\n';
                if (buttonDown || keyDown)
                {
                    if (!string.IsNullOrEmpty(this.Game.PlayerName))
                    {
                       this.Game.GoToLobby();
                    }
                }

                GUI.FocusControl("registerName");
                break;
            case GameState.Lobby:
                GUILayout.Label("Lobby", this.labelStyle);
                GUILayout.Space(20);
            
                GUI.enabled = this.BadumnaCloud.IsLoggedIn;
                GUILayout.Label("Match name:", this.labelStyle);

                GUI.SetNextControlName("matchName");
                this.matchName = GUILayout.TextField(this.matchName, this.textfieldStyle, GUILayout.Width((int)((width / 1.5f))));
                if(GUILayout.Button("Create match", this.buttonStyle, GUILayout.Width((int)((width)))))
                {
                    this.Game.CreateMatch(this.matchName);
                }

                if(GUILayout.Button("Find match", this.buttonStyle, GUILayout.Width((int)((width)))))
                {
                    this.Game.FindMatch();
                }
                GUI.enabled = true;
                GUI.FocusControl("matchName");
                break;
            case GameState.CreatingMatch:
                GUILayout.Label("Creating match ...", this.labelStyle);
                break;
            case GameState.FindingMatch:
                if(this.Game.FindMatchFinished)
                {
                    if(this.Game.MatchList.Count == 0)
                    {
                        GUILayout.Label("No match found.", this.labelStyle);
                    }
                    else
                    {
                        foreach(var match in this.Game.MatchList)
                        {
                            GUILayout.BeginHorizontal();
                            GUILayout.Label(string.Format("{2} - ({0}/{1})", match.Capacity.NumSlots - match.Capacity.EmptySlots, match.Capacity.NumSlots, match.Criteria.MatchName), this.labelStyle);
                            if(GUILayout.Button("Join", this.buttonStyle))
                            {
                                this.Game.JoinMatch(match);
                            }
                            GUILayout.EndHorizontal();
                        }
                    }

                    GUILayout.Space(20);                        
                    if(GUILayout.Button("Back to lobby", this.buttonStyle))
                    {
                        this.Game.GoToLobby();
                    }
                }
                else
                {
                    GUILayout.Label("Searching for match ...");
                }                
                break;
            case GameState.JoiningMatch:
                GUILayout.Label("Joining match ...", this.labelStyle);
                break;
            case GameState.JoinedMatch:
                GUILayout.Label(this.Game.MatchName, this.labelStyle);
                GUILayout.Space(20);

                foreach(var member in this.Game.Match.Controller.Members)
                {
                    GUILayout.Label(member.Name, this.labelStyle);
                }

                GUILayout.Space(20);
                if(GameManager.Manager.IsHost)
                {
                    if(GUILayout.Button("Start Match", this.buttonStyle))
                    {
                        this.Game.StartMatch();
                    }
                }
                else
                {
                    GUILayout.Label("Waiting for match to start.");
                }

                if(GUILayout.Button("Back to lobby", this.buttonStyle))
                {
                    this.Game.LeaveMatch();
                    this.Game.GoToLobby();
                }
                break;
            case GameState.MatchEnded:
                GUILayout.Label("Score", this.labelStyle);
                GUILayout.Space(20);
                GUILayout.Label(this.Game.GameScore, this.labelStyle);
                if(GUILayout.Button("Back to lobby", this.buttonStyle))
                {
                    this.Game.ReloadGameScene();
                }
                break;
            case GameState.Kicked:
                GUILayout.Label("Match has already started.", this.labelStyle);
                if(GUILayout.Button("Back to lobby", this.buttonStyle))
                {
                    this.Game.GoToLobby();
                }
                break;
        }

        GUILayout.EndArea();
    }
}
