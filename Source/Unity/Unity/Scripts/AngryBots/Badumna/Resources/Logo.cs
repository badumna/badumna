using UnityEngine;
using System.Collections;

public class Logo : MonoBehaviour 
{
    // Scalify logo
    public Texture2D ScalifyLogo;

    // Badumna Cloud logo
    public Texture2D BadumnaCloudLogo;

    // Logo index.
    private int currentLogoIndex;

    // Current screen opacity.
    private float opacity = 1;

    // Use this for initialization
    private void Start () 
    {
        StartCoroutine(this.DisplayLogo()); 
    }

    private void Update()
    {
        if(this.currentLogoIndex == 2)
        {
            currentLogoIndex++;
            Application.LoadLevel("Scene1-Lobby");
        }
    }
    
    private void OnGUI () 
    {
        GUI.depth = -1;

        Color oldColor;
        Color auxColor;
        oldColor = auxColor = GUI.color;

        if (this.currentLogoIndex == 0)
        {
            auxColor.a = opacity;
            GUI.color = auxColor;
            this.DrawLogo(this.ScalifyLogo, 343, 280);
            GUI.color = oldColor;
        }
        else if (this.currentLogoIndex == 1)
        {
            auxColor.a = opacity;
            GUI.color = auxColor;
            this.DrawLogo(this.BadumnaCloudLogo, 280, 343);
            GUI.color = oldColor;
        }
    }

    // Display logo.
    private IEnumerator DisplayLogo()
    {
        // Display first logo.
        StartCoroutine(this.FadeIn());
        yield return new WaitForSeconds(3);
        StartCoroutine(this.FadeOut());
        yield return new WaitForSeconds(1);

        // Display second logo.
        StartCoroutine(this.FadeIn());
        yield return new WaitForSeconds(3);
        StartCoroutine(this.FadeOut());
        yield return new WaitForSeconds(1);
    }

    // Fade in effect
    private IEnumerator FadeIn()
    {
        this.opacity = 0;
        while (this.opacity < 1)
        {
            opacity += Time.deltaTime * 2;
            yield return new WaitForEndOfFrame();
        }
        this.opacity = 1;
    }

    // Fade out effect
    private IEnumerator FadeOut()
    {
        while (this.opacity > 0)
        {
            opacity -= Time.deltaTime * 3;
            yield return new WaitForEndOfFrame();
        }

        this.opacity = 0;
        this.currentLogoIndex++;
    }

    // Draw texture logo
    //  - Logo: The logo image
    //  - height: height of the texture to be drawn
    //  - width: width of the texture to be drawn
    private void DrawLogo(Texture2D Logo, float height, float width)
    {
        GUI.DrawTexture(new Rect((int)((Screen.width / 2) - (width / 2)), (int)((Screen.height / 2) - (height / 2)), width, height), Logo);
    }
}
