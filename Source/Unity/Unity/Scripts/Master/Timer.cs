//-----------------------------------------------------------------------
// <copyright file="Timer.cs" company="Scalify">
//     Copyright (c) 2013 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using UnityEngine;
using System.Collections;

public delegate void EndTimerHandler();

public class Timer : MonoBehaviour {

	public GUISkin Skin;

	private string textTime;

	private float timerDuration;

	private bool startTimer;

	private EndTimerHandler endMatchDelegate;

	public void StartTimer(EndTimerHandler endMatchDelegate)
	{
		this.endMatchDelegate = endMatchDelegate;
		this.startTimer = true;
		this.timerDuration = Time.time + 1.0f * 60;
	}
	
	private void OnGUI()
	{
		if(!startTimer)
			return;

		var width = 100;
        var height = 30;

		GUI.skin = this.Skin;

		var guiTime = timerDuration - Time.time;

		if(guiTime > 0)
		{
			var minutes = (int)(guiTime / 60);
			var seconds = (int)(guiTime % 60);

			this.textTime = string.Format("{0:00}:{1:00}", minutes, seconds);

		    GUILayout.BeginArea(new Rect((int)(((Screen.width / 2) - (width / 2))), 40 * GameManager.Manager.GuiScale, width, height));
			GUILayout.Label(this.textTime);
			GUILayout.EndArea();
		}
		else
		{
			this.startTimer = false;

			if(this.endMatchDelegate != null)
			{
				this.endMatchDelegate.Invoke();
			}
		}
	}
}
