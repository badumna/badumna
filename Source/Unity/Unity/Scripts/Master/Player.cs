﻿//-----------------------------------------------------------------------
// <copyright file="Player.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.IO;
using Badumna;
using Badumna.DataTypes;
using Badumna.SpatialEntities;
using UnityEngine;
using Vector3 = Badumna.DataTypes.Vector3;

/// <summary>
/// Class for representing a player's position and any additional properties. 
/// As this class represent a replicable entity, it has to implement the
/// IReplicableEntity interface.
/// 
/// Define all replicable properties and RPC methods in this class and remember
/// to tag them with [Replicable] attribute.
/// 
/// Replicable properties you define here will typicallly wrap a property on the
/// Unity game object representing the player. For example, the existing Position
/// and Orientation properties wrap the Unity game object's transform's position
/// and rotation.
/// </summary>
#if MATCH
public class Player : MonoBehaviour
#else
public class Player : MonoBehaviour, IReplicableEntity
#endif // MATCH
{
#if REPLICATION || MATCH_REPLICATION
    #region Demo 1 Fields
    /// <summary>
    /// Animation controller.
    /// </summary>
    private ThirdPersonSimpleAnimation animationController;
    #endregion // Demo 1 Fields
#endif //REPLICATION

#if MATCH_REPLICATION
    #region Match field
    /// <summary>
    /// The match.
    /// </summary>
    public Badumna.Match.Match<MatchController> Match;
    #endregion // Match field
#endif // MATCH_REPLICATION
#if REPLICATION
    #region Scene field
    /// <summary>
    /// The scene.
    /// </summary>
    public Badumna.SpatialEntities.NetworkScene Scene;
    #endregion // Scene field
#endif // REPLICATION

    /// <summary>
    /// Gets or sets the position of the entity.
    /// This property is automatically replicated by Badumna.
    /// </summary>
    [Smoothing(Interpolation = 200, Extrapolation = 0)]
#if MATCH
    [Replicable]
#endif // MATCH
    public Vector3 Position
    {
        get
        {
            var position = this.gameObject.transform.position;
            return new Vector3(position.x, position.y, position.z);
        }

        set
        {
            this.gameObject.transform.position = new UnityEngine.Vector3(value.X, value.Y, value.Z);
        }
    }

    /// <summary>
    /// Gets or sets the direction the player is facing.
    /// </summary>
    [Replicable]
    public float Orientation
    {
        get
        {
            return this.gameObject.transform.rotation.eulerAngles.y;
        }

        set
        {
            this.gameObject.transform.rotation = Quaternion.AngleAxis(value, UnityEngine.Vector3.up);
        }
    }

#if REPLICATION || MATCH_REPLICATION
    #region CharacterName property
    /// <summary>
    /// Gets or sets the name for the player.
    /// </summary>
#if MATCH_ADVANCE
    [Replicable]
#endif
    public string CharacterName { get; set; }
    #endregion // CharacterName property

    #region AnimationName property
    /// <summary>
    /// Gets or sets the name of the player animation currently playing.
    /// </summary>
    [Replicable]
    public string AnimationName 
    {
        get
        {
            return this.animationController.animationName;
        }

        set
        {
            this.animationController.animationName = value;
        }    
    }
    #endregion // AnimationName property

    #region Awake method
    /// <summary>
    /// Grab the ThirdPersonSimpleAnimation on Awake.
    /// </summary>
    private void Awake()
    {
        this.animationController = this.GetComponent<ThirdPersonSimpleAnimation>();
    }
    #endregion // Awake method
#endif // REPLICATION || MATCH_REPLICATION

#if RPC || MATCH_RPC
    #region Update method
    private void Update()
    {
        // only apply if this is a local player.
        if(this.GetComponent<ThirdPersonController>() != null)
        {
##if !UNITY_IPHONE && !UNITY_ANDROID
            if(Input.GetButtonDown("Fire3"))
            {
                this.SendExplosionEffect(Input.mousePosition);
            }
##else        
            var fingerCount = 0;
            UnityEngine.Vector3 fingerPosition = UnityEngine.Vector3.zero;

            foreach (var ev in Input.touches)
            {
                if (ev.phase == TouchPhase.Ended && ev.tapCount == 2)
                {
                    fingerCount++;
                    fingerPosition = new UnityEngine.Vector3(ev.position.x, ev.position.y, 0);
                }
            }

            if(fingerCount == 1)
            {
                this.SendExplosionEffect(fingerPosition);
            }        
##endif
        }
    }
    #endregion // Update method

    #region RPC methods
    [Replicable]
    public void ExplosionEffect(Vector3 position)
    {
        GameObject go = (GameObject)Instantiate(
            GameManager.Manager.ExplosionPrefab, 
            new UnityEngine.Vector3(position.X,position.Y, position.Z), 
            gameObject.transform.rotation);
        go.transform.parent = transform;
    }

    public void SendExplosionEffect(UnityEngine.Vector3 pointerPosition)
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(pointerPosition), out hit))
        {
            var position = new Vector3(hit.point.x, hit.point.y, hit.point.z);
#if RPC
            this.Scene.CallMethodOnReplicas(this.ExplosionEffect, position);
#endif // RPC
#if MATCH_RPC
            this.Match.CallMethodOnReplicas(this.ExplosionEffect, position);
#endif // MATCH_RPC
            this.ExplosionEffect(position);
        }
    }
    #endregion // RPC methods
#endif // RPC || MATCH_RPC
}
