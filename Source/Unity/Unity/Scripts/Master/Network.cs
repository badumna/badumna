﻿//-----------------------------------------------------------------------
// <copyright file="Network.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using Badumna;
using Badumna.Chat;
using Badumna.Security;
using Badumna.SpatialEntities;
using UnityEngine;
using BadumnaId = Badumna.DataTypes.BadumnaId;
using BadumnaVector3 = Badumna.DataTypes.Vector3;

/// <summary>
/// This class provides a template network script for a Badumna enabled game.
/// It takes care of initializing the Badumna network..
/// 
/// The steps required to set up Badumna network are listed below.
/// 
/// Summary:
#if PRO
/// 1. Call NetworkFacade.BeginCreate and pass your Badumna options to 
///    start asynchronously initializing Badumna.
#else
/// 1. Call NetworkFacade.BeginCreate and pass your cloud application identifier to 
///    start asynchronously initializing Badumna.
#endif // PRO
/// 2. Call NetworkFacade.EndCreate when the IsCompleted property on the AsyncResult
///    object returned by NetworkFacade.BeginCreate has been set to true. This will
///    return the network facade for accessing Badumna functionailty.
/// 3. Login to Badumna by calling INetworkFacade.Login(characterName, xmlKeyPair).
/// 4. Register Entity Details and register any custom types for replicable properties
///    before joining a Badumna scene.
/// 5. Regularly update Badumna by calling INetworkFacade.ProcessNetworkState()
///    in the game's update loop (e.g. in Update()).
/// 7. Call INetworkFacade.Shutdown() to shutdown Badumna network.
/// </summary>
public class Network : MonoBehaviour
{
    /// <summary>
    /// The Badumna network.
    /// </summary>
    private static INetworkFacade network;    

    /// <summary>
    /// For displaying initialization status on screen.
    /// </summary>
    private GUIText status;

#if REPLICATION
    #region Demo Scene-1 field
    /// <summary>
    /// Scene instance.
    /// </summary>
    private Scene scene;
    #endregion // Demo Scene-1 field    
#endif

    /// <summary>
    /// Result of initializing Badumna.
    /// </summary>
    IAsyncResult initializationResult;

    /// <summary>
    /// Gets the Badumna network.
    /// </summary>
    public static INetworkFacade Badumna
    {
        get { return Network.network; }
    }   

    /// <summary>
    /// Called by Unity when this script is loaded.
    /// </summary>
    private void Awake()
    {
        var go = new GameObject();
        go.transform.position = new Vector3(0.05f, 0.05f, 0);
        this.status = go.AddComponent<GUIText>();
        this.status.anchor = TextAnchor.LowerLeft;
        this.status.font.material.color = Color.black;
        this.status.text = "Initializing Badumna...";
        
#if PRO
        var options = this.GetComponent<NetworkConfiguration>().GenerateNetworkConfiguration();
        this.initializationResult = NetworkFacade.BeginCreate(
            options,
            null);
#else
        this.initializationResult = NetworkFacade.BeginCreate(
            GameManager.Manager.ApplicationIdentifier,
            null);
#endif // PRO
    }

    /// <summary>
    /// Called by Unity before the first call to Update.
    /// </summary>
    /// <returns>An enumerator as per Unity coroutine requirements.</returns>
    #region Start method (Advance)
    #region Start method (Chat)
    #region Start method (Replication)
    #region Start method (Dei)
    private IEnumerator Start()
    {
        while (!this.initializationResult.IsCompleted)
        {
            yield return null;
        }

        if (Network.network == null)
        {
            try
            {
                Network.network = NetworkFacade.EndCreate(this.initializationResult);
                Network.network.AddressChangedEvent += GameManager.Manager.AddressChangedEventHandler;
                Destroy(this.status.gameObject);
            }
            catch (Exception ex)
            {
                var errorMessage = "Badumna initialization failed: " + ex.Message;
                this.status.text = errorMessage;
                Debug.LogError(errorMessage);
                yield break;
            }
        }

#if CHAT || MATCH_BASIC
#region // highlight Start method (Chat)
        // get the login script object, so we can retrieve the user name
        var loginScreen = gameObject.GetComponent<LoginScreen>();
        if (loginScreen != null)
        {
            while(!loginScreen.HaveLogin)
            {
                yield return null;
            }
#if DEI
#region // highlight Start method (Dei)
            GameManager.Manager.PlayerName = loginScreen.CharacterName;
#endregion // highlight Start method (Dei)
#else
            GameManager.Manager.PlayerName = loginScreen.UserName;
#endif // DEI
        }
#endregion // highlight Start method (Chat)

#endif // CHAT || MATCH_BASIC
        // we must login before attempting to use the network
#if DEI
#region // highlight Start method (Dei)
        var loginResult = Badumna.Login(loginScreen.IdentityProvider);    
#endregion // highlight Start method (Dei)
#else
        var loginResult = Badumna.Login(GameManager.Manager.PlayerName, GameManager.Manager.KeyPairXml);
#endif

        if (!loginResult)
        {
            Debug.LogError("Failed to login");
            yield break;
        }

#if MATCH
#else
        // Register Entity Details. 
        Network.network.RegisterEntityDetails(20, 6);
        
#endif // MATCH
        // Register any custom types for replicable properties here.
        //// Network.network.TypeRegistry.RegisterValueType( ... );
#if MATCH_ADVANCE
        #region highlight Start method (Advance)
        Network.network.TypeRegistry.RegisterMutableReferenceType<Dictionary<string, int>>(
            (d, w) =>
            {
                w.Write(d.Count);
                foreach(var pair in d)
                {
                    w.Write(pair.Key);
                    w.Write(pair.Value);
                }
            },
            r =>
            {
                var d = new Dictionary<string,int>();
                var count = r.ReadInt32();
                for(var i = 0; i < count; i++)
                {
                    d.Add(r.ReadString(), r.ReadInt32());
                }

                return d;
            },
            d => 
            {
                var nd = new Dictionary<string, int>();
                foreach(var pair in d)
                {
                    nd.Add(pair.Key, pair.Value);                    
                }
                return nd;
            });
        Network.network.RPCManager.RegisterRPCSignature<Dictionary<string,int>>();
        #endregion // highlight Start method (Advance)

#endif // MATCH_ADVANCE
#if REPLICATION
        #region highlight Start method (Replication)
        this.scene = gameObject.AddComponent<Scene>();        
        this.scene.JoinScene("Scene-1");
        #endregion // highlight Start method (Replication)
#endif // REPLICATION
    }
    #endregion // Start method (Dei)
    #endregion // Start method (Replication)
    #endregion // Start method (Chat)
    #endregion // Start method (Advance)

    /// <summary>
    /// Called by Unity once every frame.
    /// </summary>
    private void Update()
    {
        if (Network.network == null || !Network.network.IsLoggedIn)
        {
            return;
        }

        Network.network.ProcessNetworkState();
    }

    /// <summary>
    /// Called by Unity when this script is disabled.
    /// </summary>
    #region OnDisable method
    private void OnDisable()
    {
        if (!this.initializationResult.IsCompleted)
        {
            try
            {
                Network.network = NetworkFacade.EndCreate(this.initializationResult);
            }
            catch { }
        }

#if REPLICATION
        #region highlight
        this.scene.LeaveScene();
        #endregion // highlight
#endif

        if(this.status != null)
        {
            Destroy(this.status.gameObject);
        }

        if (Network.network != null)
        {
            Network.network.Shutdown();
            Network.network = null;
        }
    }
    #endregion // OnDisable method    
}
