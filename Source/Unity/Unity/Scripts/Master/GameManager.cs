//-----------------------------------------------------------------------
// <copyright file="GameManager.cs" company="Scalify">
//     Copyright (c) 2012 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.IO;
using UnityEngine;

/// <summary>
/// The game manager.
/// </summary>
/// <remarks>
/// This script provides a template game manager for use in a Badumna-enabled Unity game.
/// It is intended that developers can add this script to a game manager object, and then
/// customise it for their own game.
/// 
/// It provides a place to add customizable fields that can be edited in the Unity editor,
/// and takes care of loading and destroying the Network script.
/// Since Badumna must maintain active connections to other peers, it will trigger
/// re-initialization of Badumna when the Unity application stops/starts running.
/// </remarks>
public class GameManager : MonoBehaviour 
{
#if PRO
#else
    /// <summary>
    /// The application Identifier should be set to your BadumnaCloud ID.
    /// </summary>
    public string ApplicationIdentifier;

#endif // PRO
    /// <summary>
    /// GUI skin.
    /// </summary>
    public GUISkin IOSSkin;

    /// <summary>
    /// Player name
    /// </summary>
    public string PlayerName;

    /// <summary>
    /// Skin to use for GUI.
    /// </summary>
    internal GUISkin Skin;

    /// <summary>
    /// Factor to scale GUI by.
    /// </summary>
    internal int GuiScale = 1;

    /// <summary>
    /// A key pair generated automatically and used by Badumna for encryption.
    /// </summary>
    private string keyPairXml;

    /// <summary>
    /// A file to cache a pre-generated key pair.
    /// </summary>
    private string keyFileName = "key.bin";
    
    /// <summary>
    /// The Badumna network script.
    /// </summary>
    private Network network;

    /// <summary>
    /// A value indicating whether Badumna is initialized.
    /// </summary>
    private bool isInitialized;

#if REPLICATION || MATCH_REPLICATION
    #region Demo Replication Fields
    /// <summary>
    /// Prefab for player.
    /// </summary>
    public GameObject PlayerPrefab;

    /// <summary>
    /// Prefab for on-screen joystick for mobile platforms.
    /// </summary>
    public GameObject JoystickPrefab;

    #endregion // Demo Replication Fields
#else
    // Add any custom fields here...

#endif // REPLICATION || MATCH_REPLICATION
#if RPC || MATCH_RPC
    #region Demo Scene-2 Fields
    /// <summary>
    /// Prefab for explosion effect.
    /// </summary>
    public GameObject ExplosionPrefab;

    #endregion // Demo Scene-2 Fields
#endif // RPC
#if MATCH_HOSTED_ENTITY
    #region Demo Match-5 Fields
    /// <summary>
    /// Prefab for hosted entity.
    /// </summary>
    public GameObject ObjectPrefab;

    #endregion // Demo Match-5 Fields
#endif //MATCH_HOSTED_ENTITY
#if MATCH_BASIC
    #region Demo Match-0 Fields        

    /// <summary>
    /// Matchmaking
    /// </summary>
    private Matchmaking matchmaking;

    /// <summary>
    /// A value indicating if this client is a host. 
    /// </summary>
    public bool IsHost;
    #endregion // Demo Match-0 Fields

#endif // MATCH_BASIC
#if CHAT || MATCH_BASIC
    #region Demo Fields
    /// <summary>
    /// Login screen
    /// </summary>
    private LoginScreen loginScreen;

    #endregion // Demo Fields
#endif // CHAT || MATCH_BASIC
#if CHAT
    #region Demo Scene-3 Fields    
    /// <summary>
    /// Proximity chat.
    /// </summary>
    private Chat proximityChat;
    #endregion // Demo Scene-3 Fields

#endif // CHAT
#if ARBITRATION_SERVER
    #region Demo Scene-5 Fields
    /// <summary>
    /// Friend list.
    /// </summary>
    private FriendList friendList;
    #endregion // Demo Scene-5 Fields

#endif // ARBITRATION_SERVER
    /// <summary>
    /// Gets the GameManager.
    /// This provides global access to the game manager for convenience.
    /// </summary>
    public static GameManager Manager
    {
        get;
        private set;
    }

    /// <summary>
    /// Gets the key pair.
    /// </summary>
    public string KeyPairXml
    {
        get { return this.keyPairXml; }
    }    

    /// <summary>
    /// Handle IP address changes.
    /// </summary>
    public void AddressChangedEventHandler()
    {
        // If the local IP address has changed Badumna will need to be restarted.
        Debug.Log("Address changed dectected.");
        this.Shutdown();
        this.Initialize();
    }

    /// <summary>
    /// Called by Unity when this script loads.
    /// </summary>
    private void Awake()
    {
        if (GameManager.Manager != null)
        {
            return;
        }
           
        GameManager.Manager = this;
        this.GenerateKeyPair();
    }

    /// <summary>
    /// Called by Unity before the first time any Update method is called.
    /// </summary>
    #region Start method.
    private void Start()
    {
        // Initialize Badumna.
        this.Initialize();
    }
    #endregion // Start method.

    /// <summary>
    /// Called by Unity when the player is paused or unpaused.
    /// </summary>
    /// <param name="pause">A value indicating if pausing or unpausing.</param>
    private void OnApplicationPause(bool pause)
    {
        // Since Badumna needs to be updated regularly to maintain its connection
        // to the P2P network, it needs to be shutdown when the player is paused
        // and re-initialized when the player is un-paused.
        if (pause)
        {
            this.Shutdown();
        }
        else
        {
            if (Manager != null)
            {
                this.Initialize();
            }
        }
    }

    /// <summary>
    /// Called by Unity when the application is closed.
    /// </summary>
    private void OnApplicationQuit()
    {
        this.Shutdown();
    }
    
    /// <summary>
    /// Load the network script.
    /// </summary>
    #region // Initialize method (Chat)
    #region // Initialize method (Matchmaking)
    #region // Initialize method (ArbitrationServer)
    private void Initialize()
    {
        if (this.isInitialized)
        {
            return;
        }

        if (Application.platform == RuntimePlatform.IPhonePlayer
            || Application.platform == RuntimePlatform.Android)
        {
            this.Skin = this.IOSSkin;
            this.GuiScale = 2;
        }

#if CHAT || MATCH_BASIC
#region // highlight Initialize method (Chat)
#region // highlight Initialize method (Matchmaking)
        this.loginScreen = gameObject.AddComponent<LoginScreen>();
#endregion // highlight Initialize method (Matchmaking)
#endregion // highlight Initialize method (Chat)
#endif // CHAT || MATCH_BASIC
#if CHAT
#region // highlight Initialize method (Chat)
        this.proximityChat = gameObject.AddComponent<Chat>();
#endregion // highlight Initialize method (Chat)
#endif // CHAT
#if ARBITRATION_SERVER
#region // highlight Initialize method (ArbitrationServer)
        this.friendList = gameObject.AddComponent<FriendList>();
#endregion // highlight Initialize method (ArbitrationServer)
#endif
#if MATCH_BASIC
#region // highlight Initialize method (Matchmaking)
        this.matchmaking = gameObject.AddComponent<Matchmaking>();
#endregion // highlight Initialize method (Matchmaking)
#endif // MATCH_BASIC
        this.network = gameObject.AddComponent<Network>();
        this.isInitialized = true;
    }
    #endregion // Initialize method (ArbitrationServer)
    #endregion // Initialize method (Chat)
    #endregion // Initialize method (Matchmaking)

    /// <summary>
    /// Unload the network script.
    /// </summary>
    #region // Shutdown method
    #region // Shutdown method (ArbitrationServer)
    private void Shutdown()
    {
#if CHAT || MATCH_BASIC
#region // highlight Shutdown method   
        if (this.loginScreen != null)
            Destroy(this.loginScreen);
#endregion // highlight Shutdown method
#endif // CHAT || MATCH_BASIC
#if CHAT
#region // highlight Shutdown method
        if (this.proximityChat != null)
            Destroy(this.proximityChat);
#endregion // highlight Shutdown method
#endif // CHAT
#if ARBITRATION_SERVER
#region // highlight Shutdown method (ArbitrationServer)        
        if (this.friendList != null)
            Destroy(this.friendList);
#endregion // highlight Shutdown method (ArbitrationServer)        
#endif // ARBITRATION_SERVER
#if MATCH_BASIC
#region // highlight Shutdown method
        if (this.matchmaking != null)
            Destroy(this.matchmaking);
#endregion // highlight Shutdown method
#endif //MATCH_BASIC
        if (this.network != null)
            Destroy(this.network);

        this.isInitialized = false;
    }
    #endregion // Shutdown method (ArbitrationServer)
    #endregion // Shutdown method

    /// <summary>
    /// Since generating a key pair can be slow on some devices, this method attempts to load
    /// a cached key pair from a file, and only generates (and caches) a key pair if none is
    /// found.
    /// </summary>
    private void GenerateKeyPair()
    {
        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
        {
            var saveFilePath = Path.Combine(Application.persistentDataPath, this.keyFileName);
            if (File.Exists(saveFilePath))
            {
                this.keyPairXml = File.ReadAllText(saveFilePath);
            }
            else
            {
                this.keyPairXml = Badumna.Security.UnverifiedIdentityProvider.GenerateKeyPair();
                File.WriteAllText(saveFilePath, this.keyPairXml);
            }
        }
        else
        {
            this.keyPairXml = Badumna.Security.UnverifiedIdentityProvider.GenerateKeyPair();
        }
    }
}
