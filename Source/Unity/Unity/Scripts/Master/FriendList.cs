//-----------------------------------------------------------------------
// <copyright file="OnlineFriendListProvider.cs" company="Scalify">
//     Copyright (c) 2013 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using UnityEngine;
using System;
using Badumna;
using Badumna.Arbitration;
using DemoArbitrationEvents;
#if PRIVATE_CHAT
#region Friend class
using Badumna.Chat;

/// <summary>
/// Friend class store friend information.
/// </summary>
public class Friend
{
    /// <summary>
    /// Gets or sets buddy name.
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Gets or sets the buddy status.
    /// </summary>
    public ChatStatus Status { get; set; }

    /// <summary>
    /// Gets or sets the buddy chat channel id.
    /// </summary>
    public ChatChannelId ChannelId { get; set; }

    /// <summary>
    /// Gets or sets the chat channel.
    /// </summary>
    public IChatChannel Channel { get; set; }
}
#endregion // Friend class
#endif // PRIVATE_CHAT

/// <summary>
/// Retrieves a list of known friends from an arbitration server for private chat.
/// </summary>
public class FriendList : MonoBehaviour
{
    /// <summary>
    /// Arbitration server to retrieve friend list from.
    /// </summary>
    private IArbitrator arbitrator;

    /// <summary>
    /// The name of the user whose friends should be retrieved.
    /// </summary>
    private string userName;

#region friendList field
    /// <summary>
    /// The friend list.
    /// </summary>
#if PRIVATE_CHAT
    private Friend[] friendList;
#else
    private string[] friendList;
#endif // PRIVATE_CHAT
#endregion // friendList field

    /// <summary>
    /// FriendList window rectangle.
    /// </summary>
    private Rect friendListRect;

    /// <summary>
    /// A value indicating whether a request to the arbitration server is awaiting a reply.
    /// </summary>
    private bool replyExpected;

#if PRIVATE_CHAT
#region Friends property
    /// <summary>
    /// Gets the friend list.
    /// </summary>
    public Friend[] Friends
    {
        get { return this.friendList; }
    }
#endregion // Friends property
    
#endif //PRIVATE_CHAT
    /// <summary>
    /// Request a friend list from the provider.
    /// </summary>
    /// <param name="userName">THe name of the user whose friend list should be provided.</param>
    public void RequestFriendsList(string userName)
    {
        this.userName = userName;
        this.arbitrator = Network.Badumna.GetArbitrator("friendserver");
        this.arbitrator.Connect(
            HandleConnectionResult,
            HandleConnectionFailure,
            HandleServerMessage);
    }

    private void Start()
    {
        this.friendListRect = new Rect(
            2 * GameManager.Manager.GuiScale, 
            150 * GameManager.Manager.GuiScale, 
            220 * GameManager.Manager.GuiScale, 
            100 * GameManager.Manager.GuiScale);
    }

    private void OnGUI()
    {
        if(this.friendList == null)
            return;
        GUI.skin = GameManager.Manager.Skin;
        this.friendListRect = GUI.Window(0, this.friendListRect, this.FriendListWindow, "Friend List");
    }

#region FriendListWindow method
    /// <summary>
    /// Friend list window.
    /// </summary>
    /// <param name="id">The window id.</param>
    private void FriendListWindow(int id)
    {
        GUILayout.BeginVertical();

        // draw each buddy entry, at the same time allowing the user to toggle the private chat window
        // with that buddy
        foreach (var friend in this.friendList)
        {
#if PRIVATE_CHAT
#region highlight
            GUILayout.Label(string.Format("{0} - {1}", friend.Name, friend.Status));
#endregion highlight
#else
            GUILayout.Label(friend);
#endif
        }

        GUILayout.EndVertical();
        GUI.DragWindow();
    }
#endregion // FriendListWindow method

    /// <summary>
    /// Handles the arbitration server connection result.
    /// </summary>
    /// <param name="result">The result of the attempt to connect to the arbitration server.</param>
    private void HandleConnectionResult(ServiceConnectionResultType result)
    {
        if (result == ServiceConnectionResultType.Success)
        {
            // Send a request to the arbitration server for a friend list.
            FriendListRequest request = new FriendListRequest(this.userName);
            this.arbitrator.SendEvent(
                DemoArbitrationEvents.FriendListEventSet.Serialize(request));
            this.replyExpected = true;
        }
        else
        {
            Debug.LogError("Could not connect to server.");
        }
    }

    /// <summary>
    /// Handles a arbitration server connection failure.
    /// </summary>
    private void HandleConnectionFailure()
    {
        if (this.replyExpected)
        {
            Debug.LogError("Connection to server failed.");
            this.replyExpected = false;
        }
    }

#region HandleServerMessage method
    /// <summary>
    /// Handle messages from the arbitration server.
    /// </summary>
    /// <param name="message">A serialized arbitration event.</param>
    private void HandleServerMessage(byte[] message)
    {
        if (this.replyExpected)
        {
            ArbitrationEvent reply = FriendListEventSet.Deserialize(message);
            if (reply is FriendListReply)
            {
                // Return the friend list received.
                FriendListReply friendListReply = reply as FriendListReply;
#if PRIVATE_CHAT
                this.friendList = new Friend[friendListReply.FriendNames.Count];        
                for(var i = 0; i < friendListReply.FriendNames.Count; i++)
                {
                    this.friendList[i] = new Friend(){ Name = friendListReply.FriendNames[i], Status = ChatStatus.Offline};
                }

                this.gameObject.SendMessage("InitializePrivateChat", SendMessageOptions.RequireReceiver);
#else
                this.friendList = new string[friendListReply.FriendNames.Count];
                friendListReply.FriendNames.CopyTo(friendList, 0);
#endif
            }
        }
    }
#endregion // HandleServerMessage method
}
