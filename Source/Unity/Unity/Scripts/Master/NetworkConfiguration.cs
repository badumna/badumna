﻿//-----------------------------------------------------------------------
// <copyright file="NetworkConfiguration.cs" company="Scalify">
//     Copyright (c) 2013 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
using UnityEngine;
using Badumna;

/// <summary>
/// Handle the Badumna network configuration.
/// </summary>
public class NetworkConfiguration : MonoBehaviour 
{
#if PRO
    /// <summary>
    /// A value indicating whether network configuration should use on a local area network.
    /// </summary>
    public bool IsConfigureForLan = true;

    /// <summary>
    /// A value indicating whether local network broadcast should be used to discover peers. Defaults to true.
    /// </summary>
    public bool IsBroadcastEnabled = true;

    /// <summary>
    /// List of seed peer addresses.
    /// </summary>
    public string[] SeedPeers;

    /// <summary>
    /// The UDP port to be used for broadcast discovery of peers.
    /// </summary>
    public int BroadcastPort = 21250;
    
    /// <summary>
    /// The start of the UDP port range to use for Badumna traffic.
    /// </summary>
    public int StartPortRange = 21300;
    
    /// <summary>
    /// The end of the UDP port range to use for Badumna traffic
    /// </summary>
    public int EndPortRange = 21399;

    /// <summary>
    /// The name uniquely identifying the application.
    /// </summary>
    public string ApplicationName = "unity-demo";
#if DEI

#region Demo Scene-4 Fields
#region highlight
    /// <summary>
    /// Dei server address.
    /// </summary>
    public string DeiServer = "deiserver.example.com:21248";
#endregion // highlight
#endregion // Demo-Scene-4 Fields
#endif // DEI

#if ARBITRATION_SERVER
#region Demo Scene-5 fields
    /// <summary>
    /// The name identifying the arbitration server.
    /// </summary>
    public string ArbitrationServiceName = "friendserver";

    /// <summary>
    /// The arbitration server address.
    /// </summary>
    public string ArbitrationServerAddress = "friendserver.example.com:21260";
#endregion // Demo Scene-5 fields

#endif
#if MATCH
    /// <summary>
    /// The matchmaking server address.
    /// </summary>
    public string MatchmakingServerAddress = "matchmaking.example.com:21261";

#endif // MATCH
#region GenerateNetworkConfiguration method (Arbitration server)
    /// <summary>
    /// Generate network configuration based on the information given.
    /// </summary>
    /// <returns>Options configuration instance.</returns>
    public Options GenerateNetworkConfiguration()
    {
        // The Options must be specified before a call to Network.System.Initialize.         
        // set the options programatically
        var options = new Options();
        var connectivity = options.Connectivity;

        // The type of discovery mechanism to use. 
        // Badumna requires the knowledge of one existing peer to connect with in 
        // order to join the network. The address of any other peer can be 'discovered' through
        // SeedPeers(peers that are 'always' running).

        // Use LAN mode for testing
        if(IsConfigureForLan)
            connectivity.ConfigureForLan();

        // To use on the Internet, comment out the ConfigureForLan() line above and
        // add a known seed peer.  e.g.:

        foreach (var seedPeer in SeedPeers)
        {
            connectivity.SeedPeers.Add(seedPeer);
        }

        // Disable/enable the use of broadcast
        connectivity.IsBroadcastEnabled = IsBroadcastEnabled;

        // The port used for discovery of peers via UDP broadcast.
        connectivity.BroadcastPort = BroadcastPort;

        // The broadcast port shouldn't be withtin the peer port range, 
        // otherwise badumna will throw an exception
        connectivity.StartPortRange = StartPortRange;
        connectivity.EndPortRange = EndPortRange;

        // set the application name
        connectivity.ApplicationName = ApplicationName;

#if ARBITRATION_SERVER
#region highlight GenerateNetworkConfiguration method (Arbitration server)
        // add an external server that will supply our friends list
        options.Arbitration.Servers.Add(new ArbitrationServerDetails(this.ArbitrationServiceName, this.ArbitrationServerAddress));
#endregion // highlight GenerateNetworkConfiguration method (Arbitration server)
        
#endif // ARBITRATION_SERVER
#if MATCH
        options.Matchmaking.ServerAddress = this.MatchmakingServerAddress;
        
#endif //MATCH
        return options;
    }
#endregion // GenerateNetworkConfiguration method (Arbitration server)
#endif // PRO
}