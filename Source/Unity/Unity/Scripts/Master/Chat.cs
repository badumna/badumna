//-----------------------------------------------------------------------
// <copyright file="Chat.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.Collections.Generic;
using Badumna.Chat;
using Badumna.DataTypes;
using UnityEngine;
#if MATCH
using Badumna.Match;
#endif // MATCH

#if MATCH
/// <summary>
/// The Chat class demonstrates a simple way to use chat system in Badumna match.
/// 
/// To enable proximity chat you need to :
/// - Hooked the ChatMessageReceived event with your own HandleChatMessage implementation.
///    Match.ChatMessageReceived += this.HandleChatMessage;
/// </summary>
#else
/// <summary>
/// The Chat class demonstrates a simple way to use Badumna proximity
/// chat functionality.
/// 
/// To enable proximity chat you need to do these two steps:
/// 1. Subscribe your local player to the proximity channel and pass a delegate for
///    receiving proximity chat message, by calling:
///    NetworkFacade.ChatSession.SubscribeToProximityChannel(
///        IReplicableEntity,
///        ChatMessageHandler);
/// 2. Then you can send proximity chat message by calling SendMessage(string) on
///    that proximity channel object that you get from the previous step.
#if PRIVATE_CHAT
///
/// To enable private chat you need to do these following steps:
/// 1. Open private channel and pass a delegate for handling channel invitation, 
///    by calling:
///    NetworkFacade.ChatSession.OpenPrivateChannels(HandleChannelInvitation);
/// 2. Change your presence status to online, by calling:
///    NetworkFacade.ChateSession.ChangePresence(ChatStatus.Online);
/// 3. For each friend from your friend list, call the following API:
///    NetworkFacade.ChatSession.InviteUserToPrivateChannel('friend name');
/// 4. Then you can send private chat message by calling SendMessage(string) on
///    friend chat channel object that you obtain when accepting the invitation.
#endif // PRIVATE_CHAT
/// </summary>
#endif
public class Chat : MonoBehaviour
{
#if MATCH_CHAT
    #region Demo Match-4 Field
    /// <summary>
    /// The match.
    /// </summary>
    public Badumna.Match.Match<MatchController> Match;    
    #endregion // Demo Match-4 Field
#endif
#if MATCH
#else
    /// <summary>
    /// The proximity chat channel.
    /// </summary>
    private IChatChannel channel;

    /// <summary>
    /// The local player.
    /// </summary>
    private Player localPlayer;

    /// <summary>
    /// A value indicating whether the local player is subscribed to a proximity channel.
    /// </summary>
    private bool isSubscribed;
#endif // MATCH
#if CHAT || MATCH_CHAT
    #region Demo 3 Fields

    /// <summary>
    /// The chat window.
    /// </summary>
    private Rect chatRect;

    /// <summary>
    /// The scroll position of the chat window.
    /// </summary>
    private Vector2 scrollPosition;

    /// <summary>
    /// The text of a message to send.
    /// </summary>
    private string messageText = string.Empty;

    /// <summary>
    /// The log of sent messages.
    /// </summary>
    private List<string> messageHistory = new List<string>();
    #endregion Demo 3 Fields
#endif // CHAT || MATCH_CHAT

#if PRIVATE_CHAT
#region Demo Scene-6 field-property
    /// <summary>
    /// Friend list instance.
    /// </summary>
    private FriendList friendList;

    /// <summary>
    /// Gets the friend list.
    /// </summary>
    private FriendList FriendList
    {
        get
        {
            if(this.friendList == null)
            {
                this.friendList = this.GetComponent<FriendList>();
            }

            return this.friendList;
        }
    }
#endregion // Demo Scene-6 field-property

#endif // PRIVATE_CHAT
    /// <summary>
    /// Called by Unity before the first time any Update method is called.
    /// </summary>
    #region Start method
    private void Start()
    {
#if MATCH_CHAT
        #region // highlight
        this.Match.ChatMessageReceived += this.HandleChatMessage;
        #endregion // highlight
#endif // MATCH_CHAT
#if CHAT || MATCH_CHAT
        #region // highlight
        this.chatRect = new Rect(
            15,
            15 * GameManager.Manager.GuiScale,
            Screen.width,
            200 * GameManager.Manager.GuiScale);
        #endregion // highlight
#endif // CHAT
    }
    #endregion Start method

#if CHAT || MATCH_CHAT
    /// <summary>
    /// Called by Unity for GUI events.
    /// </summary>
    #region OnGUI method
    private void OnGUI()
    {
#if CHAT
        if (!this.isSubscribed)
        {
            return;
        }
#endif // CHAT
        GUI.skin = GameManager.Manager.Skin;

        GUILayout.BeginArea(this.chatRect);
        GUILayout.BeginHorizontal();
        GUI.SetNextControlName("chat");
        this.messageText = GUILayout.TextField(
            this.messageText,
            new GUILayoutOption[]
            {
                GUILayout.Width(200 * GameManager.Manager.GuiScale),
                GUILayout.Height(20 * GameManager.Manager.GuiScale)
            });
        
        if(GUILayout.Button(
            "Send",
            new GUILayoutOption[]
            {
                GUILayout.Width(98 * GameManager.Manager.GuiScale),
                GUILayout.Height(20 * GameManager.Manager.GuiScale) 
            }))
        {
            this.SendMessage();
        }

        if (GUILayout.Button(
            "Clear",
            new GUILayoutOption[]
            {
                GUILayout.Width(98 * GameManager.Manager.GuiScale),
                GUILayout.Height(20 * GameManager.Manager.GuiScale)
            }))
        {
            this.messageHistory.Clear();
        }

        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        this.scrollPosition = GUILayout.BeginScrollView(this.scrollPosition);
        foreach(var message in this.messageHistory)
        {
            GUILayout.Label(message);
        }
        GUILayout.EndScrollView();
        GUILayout.EndArea();        
        GUI.SetNextControlName("");

        if(Event.current.type == EventType.keyDown && Event.current.character == '\n')
        {
            if(GUI.GetNameOfFocusedControl() != "chat")
            {
                GUI.FocusControl("chat");                
            }
            else
            {
                this.SendMessage();
                GUI.FocusControl("");                
            }
        }     
    }
    #endregion // OnGUI method

#endif // CHAT
    #region OnDisable method
    /// <summary>
    /// Called by Unity when this behaviour becomes disabled.
    /// </summary>
    private void OnDisable()
    {
#if MATCH
#else
        this.Unsubscribe();
#endif // MATCH
#if MATCH_CHAT
        #region highlight
        this.Match.ChatMessageReceived -= this.HandleChatMessage;
        #endregion // highlight
#endif // MATCH_CHAT
        Destroy(this);
    }
    #endregion // OnDisable method

#if MATCH
#else
    /// <summary>
    /// Subscribe a local player to proximity chat.
    /// </summary>
    /// <param name="localPlayer">The local player.</param>
    public void Subscribe(Player localPlayer)
    {
        this.isSubscribed = true;
        this.localPlayer = localPlayer;
        this.channel = Network.Badumna.ChatSession.SubscribeToProximityChannel(
            localPlayer,
            this.HandleChatMessage);
    }

#region Unsubscribe method
    /// <summary>
    /// Unsubscribe from proximity chat.
    /// </summary>
    public void Unsubscribe()
    {
        if (this.channel == null)
        {
            return;
        }

        this.isSubscribed = false;
        this.channel.Unsubscribe();
        this.channel = null;
#if PRIVATE_CHAT
        
#region highlight
        foreach(var friend in this.FriendList.Friends)
        {
            if(friend.Channel != null)
            {
                friend.Channel.Unsubscribe();
                friend.Channel = null;
            }
        }
#endregion // highlight
#endif // PRIVATE_CHAT
    }
#endregion // Unsubscribe method
#endif // MATCH

#if PRIVATE_CHAT
#region InitializePrivateChat method
    /// <summary>
    /// Initialize the private chat system.
    /// This method will be called by the FriendList instance as soon as the friend
    /// list is populated.
    /// </summary>
    public void InitializePrivateChat()
    {
        // open private channels, go online
        Network.Badumna.ChatSession.OpenPrivateChannels(this.HandleChannelInvitation);
        Network.Badumna.ChatSession.ChangePresence(ChatStatus.Online);

        foreach(var friend in this.FriendList.Friends)
        {
            Network.Badumna.ChatSession.InviteUserToPrivateChannel(friend.Name);
        }
    }
#endregion // InitializePrivateChat method

#endif // PRIVATE_CHAT
    /// <summary>
    /// Send a chat message.
    /// </summary>
    #region SendMessage method
    private void SendMessage()
    {
#if CHAT || MATCH_CHAT
#else
        // Put your message sending code here. E.g.:
#endif
#if PRIVATE_CHAT
        if(this.messageText.Trim().StartsWith("@"))
        {
            var name = this.messageText.Split(' ')[0].Trim('@');
            var message = this.messageText.Trim().Split(new char[]{' '}, 2)[1];
            var friend = System.Array.Find(this.FriendList.Friends, f => f.Name == name);

            if(friend == null)
            {
                Debug.LogError(string.Format("Friend with name '{0}' is not found ", name));
                return;
            }

            if(friend.Channel == null)
            {
                Debug.LogError(string.Format("Chat channel for '{0}' is not found ", name));
                return;
            }

            friend.Channel.SendMessage(
                string.Format("[{0}] whispers: {1}", this.localPlayer.CharacterName, message));
            this.HandleChatMessage(
                this.channel,
                null,
                string.Format("To [{0}]: {1}", name, message));
        }
        else
        {
            this.channel.SendMessage(
                string.Format("[{0}] says: {1}", this.localPlayer.CharacterName, this.messageText));
            this.HandleChatMessage(
                this.channel,
                null,
                string.Format("You says: {0}", this.messageText));
        }
        
        this.messageText = string.Empty;
#else
#if CHAT
#region // highlight
        this.channel.SendMessage(
            string.Format("[{0}] says: {1}", this.localPlayer.CharacterName, this.messageText));
        this.HandleChatMessage(
            this.channel,
            null,
            string.Format("You says: {0}", this.messageText));
        this.messageText = string.Empty;
#endregion // highlight
#endif // CHAT
#endif // PRIVATE_CHAT
#if MATCH_CHAT
#region // highlight
        this.Match.Chat(
            string.Format("{0}: {1}", this.Match.MemberIdentity.Name, this.messageText));
        this.messageHistory.Add(string.Format("You: {0}", this.messageText));
        this.messageText = string.Empty;
#endregion // highlight
#endif
    }
    #endregion // SendMessage method

#if MATCH
    /// <summary>
    /// Handle incoming chat messages.
    /// </summary>
    /// <param name="match">The match.</param>
    /// <param name="e">The match chat event arguments.</param>
#else
    /// <summary>
    /// Handle incoming chat messages.
    /// </summary>
    /// <param name="channel">The channel the message was received on.</param>
    /// <param name="userId">The ID of the message sender.</param>
    /// <param name="message">The message text.</param>
#endif
    #region HandleMessage method
#if MATCH
    private void HandleChatMessage(Match match, MatchChatEventArgs e)
    {
#if MATCH_CHAT
        this.scrollPosition += new Vector2(0, 25);
        this.messageHistory.Add(e.Message);
#else
        // Call your game's chat message handling code here.
        // E.g. for drawing messages to the screen.
#endif // MATCH_CHAT
    }
#else
    private void HandleChatMessage(IChatChannel channel, BadumnaId userId, string message)
    {
#if CHAT
#region // highlight
        this.scrollPosition += new Vector2(0, 25);
        this.messageHistory.Add(message);
#endregion // highlight
#else
        // Call your game's chat message handling code here.
        // E.g. for drawing messages to the screen.
#endif
    }
#endif // MATCH
    #endregion // HandleMessage method

#if PRIVATE_CHAT
#region HandlePresence method
    /// <summary>
    /// Handle presence change 
    /// </summary>
    /// <param name="channel">Chat channel id.</param>
    /// <param name="userId">The user id.</param>
    /// <param name="username">The user name.</param>
    /// <param name="status">The user status.</param>
    private void HandlePresence(IChatChannel channel, BadumnaId userId, string username, ChatStatus status)
    {
        Debug.Log(string.Format("PrivateChat.HandlePresence: {0} is {1}", username, status.ToString()));
        var friend = System.Array.Find(this.friendList.Friends, f => f.Name == username);

        if(friend == null)
            return;

        friend.Status = status;
        if(status == ChatStatus.Offline)
        {
            friend.ChannelId = null;
        }
    }
#endregion // HandlePresence method

#region HandleChannelInvitation method
    /// <summary>
    /// Handle private channel invitation from other user.
    /// </summary>
    /// <param name="channel">Chat channel id.</param>
    /// <param name="username">The user name.</param>
    private void HandleChannelInvitation(ChatChannelId channel, string username)
    {        
        Debug.Log(string.Format("ChatManager.HandleChannelInvitation: {0} {1}", channel.Name, username));
        var friend = System.Array.Find(this.friendList.Friends, f => f.Name == username);

        if(friend == null)
            return;
        
        if (friend.ChannelId != null && !friend.ChannelId.Equals(channel))
        {
            friend.Channel.Unsubscribe();
            friend.Channel = null;
        }

        friend.ChannelId = channel;
        friend.Channel = Network.Badumna.ChatSession.AcceptInvitation(channel, this.HandleChatMessage, this.HandlePresence);
    }
#endregion // HandleChannelInvitation method
#endif // PRIVATE_CHAT
}
