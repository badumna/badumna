//-----------------------------------------------------------------------
// <copyright file="NPC.cs" company="Scalify">
//     Copyright (c) 2013 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections;
using System.IO;
using Badumna;
using Badumna.DataTypes;
using Badumna.SpatialEntities;
using UnityEngine;
using Vector3 = Badumna.DataTypes.Vector3;

public class NPC : MonoBehaviour
{
#if MATCH_ADVANCE
    #region Demo Match-6 Fields
    /// <summary>
    /// Badumna match.
    /// </summary>
    public Badumna.Match.Match<MatchController> Match;
    #endregion // Demo Match-6 Fields

#endif
	/// <summary>
    /// Gets or sets the position of the entity.
    /// This property is automatically replicated by Badumna.
    /// </summary>
    [Replicable]
    public Vector3 Position
    {
        get
        {
            var position = this.gameObject.transform.position;
            return new Vector3(position.x, position.y, position.z);
        }

        set
        {
            this.gameObject.transform.position = new UnityEngine.Vector3(value.X, value.Y, value.Z);
        }
    }

    /// <summary>
    /// Gets or sets the direction the player is facing.
    /// </summary>
    [Replicable]
    public float Orientation
    {
        get
        {
            return this.gameObject.transform.rotation.eulerAngles.y;
        }

        set
        {
            this.gameObject.transform.rotation = Quaternion.AngleAxis(value, UnityEngine.Vector3.up);
        }
    }

#if MATCH_ADVANCE
    #region EnableRenderer method
    /// <summary>
    /// Enable or disable the renderer.
    /// </summary>
    [Replicable]
    public void EnableRenderer(bool enabled)
    {
        this.gameObject.renderer.enabled = enabled;
    }
    #endregion // EnableRenderer method

    #region OnTriggerEnter method
    /// <summary>
    /// On trigger enter.
    /// </summary>
    private void OnTriggerEnter(Collider other)
    {
        if(GameManager.Manager.IsHost)
        {
            var player = other.gameObject.GetComponent<Player>();

            if(player != null)
            {
                this.Match.Controller.AddScore(player.CharacterName);
                this.Match.CallMethodOnMembers(this.Match.Controller.AddScore, player.CharacterName);
            }

            StartCoroutine(this.RespawnObject());
        }
    }
    #endregion // OnTriggerEnter method

    #region RespawnObject method
    /// <summary>
    /// Respawn the NPC in the new random position after 2 seconds.
    /// </summary>
    private IEnumerator RespawnObject()
    {
        // Randomly moved to different location
        this.Match.CallMethodOnReplicas(this.EnableRenderer, false);
        this.EnableRenderer(false);
        this.transform.position = new UnityEngine.Vector3(UnityEngine.Random.Range(-10, 10), -16.5f, UnityEngine.Random.Range(-10,10));
        yield return new WaitForSeconds(2);
        this.Match.CallMethodOnReplicas(this.EnableRenderer, true);
        this.EnableRenderer(true);
    }
    #endregion // RespawnObject method
#endif
}