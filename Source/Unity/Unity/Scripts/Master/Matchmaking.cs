﻿//-----------------------------------------------------------------------
// <copyright file="Matchmaking.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using Badumna.Match;
using UnityEngine;
using BadumnaId = Badumna.DataTypes.BadumnaId;
using BadumnaVector3 = Badumna.DataTypes.Vector3;

#if MATCH_BASIC
/// <summary>
/// Matchmaking game state.
/// </summary>
#region Game state enumeration
public enum GameState
{
    Lobby,
    CreatingMatch,
    FindingMatch,
    JoiningMatch,
    JoinedMatch,
    StartingMatch,
    MatchStarted,
    EndingMatch,
    MatchEnded,
    Loading,
    Spectator
};
#endregion // Game state enumeration
#endif // MATCH_BASIC

/// <summary>
/// Matchmaking class show how to use the Badumna match API in a Unity game.
/// </summary>
public class Matchmaking : MonoBehaviour
{
#if MATCH_REPLICATION
    #region Demo Match-2 Constant
    /// <summary>
    /// The entity type for the player character.
    /// </summary>
    private const uint EntityType = 0;
    #endregion // Demo Match-2 Constant

#endif // MATCH_REPLICATION
#if MATCH_HOSTED_ENTITY
    #region Demo Match-5 Constant
    /// <summary>
    /// The entity type for the non playable character.
    /// </summary>
    private const uint HostedEntityType = 1;
    #endregion // Demo Match-5 Constant

#endif // MATCH_HOSTED_ENTITY
    /// <summary>
    /// Badumna match.
    /// </summary>
    private Match<MatchController> match;

    /// <summary>
    /// Match controller.
    /// </summary>
    private MatchController controller;

    /// <summary>
    /// List of matches.
    /// </summary>
    private List<MatchmakingResult> matchList = new List<MatchmakingResult>();

    /// <summary>
    /// A value indicating whether the match query is handled.
    /// </summary>
    private bool matchQueryHandled;

    /// <summary>
    /// Match name.
    /// </summary>
    private string matchName = "default";

    /// <summary>
    /// Match status, use for debugging.
    /// </summary>
    private string status;

#if MATCH_BASIC
    #region Demo Match-0 Fields
    /// <summary>
    /// A value indicating whether the matchmaking window is hidden.
    /// </summary>
    private bool hideMatchmakingWindow;

    /// <summary>
    /// Matchmaking GUI window.
    /// </summary>
    private Rect matchmakingWindowRect;    

    /// <summary>
    /// The current matchmaking state.
    /// </summary>
    private GameState state;
    #endregion // Demo Match-0 Fields

#endif // MATCH_BASIC
#if MATCH_REPLICATION
    #region Demo Match-2 Fields
    /// <summary>
    /// Local player.
    /// </summary>
    private Player localPlayer;
    #endregion // Demo Match-2 Fields

#endif // MATCH_REPLICATION
#if MATCH_HOSTED_ENTITY
    #region Demo Match-5 Fields
    /// <summary>
    /// List of hosted entities.
    /// </summary>
    private List<NPC> hostedEntities = new List<NPC>();
    #endregion // Demo Match-5 Fields

#endif //MATCH_HOSTED_ENTITY
#if MATCH_ADVANCE
    #region Demo Match-6 Fields
    /// <summary>
    /// Store the final score for display.
    /// </summary>
    private string finalScore;
    #endregion // Demo Match-6 Fields

#endif // MATCH_ADVANCE
#if MATCH_BASIC
    #region Start method
    private void Start()
    {
        this.state = GameState.Lobby;        

        this.matchmakingWindowRect = new Rect(
            ((Screen.width / 2) - 150 * GameManager.Manager.GuiScale),
            ((Screen.height / 2) - 112.5f * GameManager.Manager.GuiScale),
            300 * GameManager.Manager.GuiScale,
            225 * GameManager.Manager.GuiScale);        
    }
    #endregion // Start method

    #region OnGui method
    private void OnGUI()
    {
        if (Network.Badumna == null || !Network.Badumna.IsLoggedIn)
            return;
        
        GUI.skin = GameManager.Manager.Skin;
#if MATCH_ADVANCE
        #region highlight
        if(this.state == GameState.MatchStarted)
        {
            GUILayout.Space(50);
            this.match.Controller.OnGUI();
        }

        if(this.state == GameState.Spectator)
        {
            GUILayout.BeginArea(new Rect((int)(((Screen.width / 2) - 100)), 20, 200, 50));
            GUILayout.Label("Please wait for the next round!");
            GUILayout.EndArea();
        }
        #endregion // highlight
#endif // MATCH_ADVANCE

        if (this.hideMatchmakingWindow)
            return;

        this.matchmakingWindowRect = GUI.Window(1, this.matchmakingWindowRect, MatchmakingWindow, "Matchmaking-" + this.state.ToString());
    }
    #endregion // OnGui method

    #region OnDisable method
    private void OnDisable()
    {
        this.LeaveMatch();
        Destroy(this);
    }
    #endregion // OnDisable method
#else
#endif // MATCH_BASIC

    #region OnMatchStatusChange method (Basic)
    #region OnMatchStatusChange method (Controller)
    #region OnMatchStatusChange method (Advance)
    /// <summary>
    /// OnMatchStatusChange callback is called when the match status is changed.
    /// </summary>
    /// <param name="match">The match.</param>
    /// <param name="e">Match status event arguments.</param>
    private  void OnMatchStatusChange(Match match, MatchStatusEventArgs e)
    {
        this.Trace(e.Status.ToString());

        if(e.Status == MatchStatus.Closed)
        {
            this.match = null;
#if MATCH_BASIC
            #region highlight OnMatchStatusChange method (Basic)
            this.state = GameState.Lobby;
            #endregion // highlight OnMatchStatusChange method (Basic)
#endif // MATCH_BASIC
        }
        else if(e.Status == MatchStatus.Hosting || e.Status == MatchStatus.Connected)
        {
#if MATCH_BASIC
            #region highlight OnMatchStatusChange method (Basic)
            GameManager.Manager.IsHost = e.Status == MatchStatus.Hosting;

            if(this.state != GameState.MatchStarted && this.state != GameState.MatchEnded)
            {
                this.state = GameState.JoinedMatch;
#if MATCH_CONTROLLER
                #region highlight OnMatchStatusChange method (Controller)
                this.match.Controller.OnMatchStarted += this.HandleMatchStart;
                this.match.Controller.OnMatchEnded += this.HandleMatchEnd;
                #endregion //highlight OnMatchStatusChange method (Controller)
#endif //MATCH_CONTROLLER
#if MATCH_ADVANCE
                #region highlight OnMatchStatusChange method (Advance)
                this.match.Controller.OnStateChanged += this.StateChangedHandler;
                #endregion //highlight OnMatchStatusChange method (Advance)
#endif // MATCH_ADVANCE
            }
            #endregion // highlight OnMatchStatusChange method (Basic)
#endif // MATCH_BASIC
        }        

        if (e.Error != MatchError.None)
        {
            this.Trace("Error: " + e.Error.ToString());
        }  
    }
    #endregion // OnMatchStatusChange method (Advance)
    #endregion // OnMatchStatusChange method (Controller)
    #endregion // OnMatchStatusChange method (Basic)

    #region CreateMatch method
    /// <summary>
    /// Create a new match.
    /// </summary>
    private void CreateMatch()
    {
        var criteria = new MatchmakingCriteria
            {
                PlayerGroup = 0,
                MatchName = this.matchName
            };
        this.controller = new MatchController();
        this.match = Network.Badumna.Match.CreateMatch<MatchController>(
            this.controller,
            criteria, 
            4, 
            GameManager.Manager.PlayerName,
            this.CreateReplica,
            this.RemoveReplica);
        this.match.StateChanged += this.OnMatchStatusChange;
        this.match.MemberAdded += this.controller.AddMember;
#if MATCH_ADVANCE
        #region highlight
        this.match.MemberAdded += this.AddMember;
        #endregion // highlight
#endif // MATCH_ADVANCE
        this.match.MemberRemoved += this.controller.RemoveMember;
    }
    #endregion // CreateMatch method

    #region FindMatch method
    /// <summary>
    /// Find available matches with a given matchmaking criteria.
    /// </summary>
    private void FindMatch()
    {
        var criteria = new MatchmakingCriteria
        {
            PlayerGroup = 0,
        };

        this.matchQueryHandled = false;
#if MATCH_BASIC
        #region highlight
        this.state = GameState.FindingMatch;
        #endregion // highlight        
#endif // MATCH_BASIC
        Network.Badumna.Match.FindMatches(
            criteria,
            this.HandleMatchQuery);
    }
    #endregion // FindMatch method

    #region JoinMatch method  
    #region JoinMatch method (Advance)  
    /// <summary>
    /// Joining an existing match.
    /// </summary>
    /// <param name="matchToJoin">Match to join.</param>
    private void JoinMatch(MatchmakingResult matchToJoin)
    {        
#if MATCH_BASIC
        #region highlight JoinMatch method
        this.state = GameState.JoiningMatch;
        #endregion // highlight JoinMatch method     
#endif // MATCH_BASIC
        this.controller = new MatchController();
        this.match = Network.Badumna.Match.JoinMatch<MatchController>(
            this.controller,
            matchToJoin,
            GameManager.Manager.PlayerName,
            this.CreateReplica,
            this.RemoveReplica);
        this.match.StateChanged += this.OnMatchStatusChange;
        this.match.MemberAdded += this.controller.AddMember;
#if MATCH_ADVANCE
        #region highlight JoinMatch method (Advance)
        this.match.MemberAdded += this.AddMember;
        #endregion // highlight JoinMatch method (Advance)
#endif // MATCH_ADVANCE
        this.match.MemberRemoved += this.controller.RemoveMember;
    }
    #endregion // JoinMatch method (Advance)
    #endregion // JoinMatch method

    #region LeaveMatch method    
    /// <summary>
    /// Leave from match.
    /// </summary>    
    private void LeaveMatch()
    {
        if(this.match != null)
        {
            this.match.Leave();
        }
    }
    #endregion // LeaveMatch method

    /// <summary>
    /// Handle the match query
    /// </summary>
    /// <param name="result"></param>
    private void HandleMatchQuery(MatchmakingQueryResult result)
    {
        this.matchList.Clear();

        foreach(var r in result.Results)
        {
            this.matchList.Add(r);
        }

        this.matchQueryHandled = true;

        if(result.Error != MatchError.None)
        {
            Debug.LogError("Error: " + result.Error.ToString());
        }
    }

#if MATCH_ADVANCE
    #region AddMember method
    private void AddMember(Match match, MatchMembershipEventArgs e)
    {
        // Handle player that join in the middle of the game or at the end of the game.
        if(GameManager.Manager.IsHost)
        {
            if(this.state == GameState.MatchStarted)
            {
                this.match.Controller.SyncGameScoreTo(this.match, e.Member);
                this.match.CallMethodOnMember(e.Member, this.match.Controller.SetGameState, (int)GameState.Spectator);
            }
            else if(this.state == GameState.MatchEnded)
            {
                this.match.Controller.SyncGameScoreTo(this.match, e.Member);
                this.match.CallMethodOnMember(e.Member, this.match.Controller.SetGameState, (int)GameState.MatchEnded);
            }
        }
    }
    #endregion // AddMember method

    #region StateChangedHandler method
    private void StateChangedHandler(GameState state)
    {
        this.state = state;

        if(this.state == GameState.Spectator)
        {
            this.hideMatchmakingWindow = true;
            var cam = Camera.main.transform;
            cam.position = new UnityEngine.Vector3(0, -1.3f, -15.2f);
            cam.rotation = Quaternion.AngleAxis(45, UnityEngine.Vector3.right);
        }
        else if(this.state == GameState.MatchEnded)
        {
            this.finalScore = this.match.Controller.GameScore;
        }
    }
    #endregion // StateChangedHandler method
#endif // MATCH_ADVANCE

    /// <summary>
    /// Use for tracing debug messages.
    /// </summary>
    /// <param name="message">Message to be traced.</param>
    private void Trace(string message)
    {
        Debug.Log(message);
        this.status += message + "\n";
    }

#if MATCH_BASIC
    #region MatchmakingWindow method
    #region MatchmakingWindow method (Controller)
    #region MatchmakingWindow method (Advance)
    /// <summary>
    /// Matchmaking window GUI layout.
    /// </summary>
    /// <param name="id">Window id.</param>
    private void MatchmakingWindow(int id)
    {
        switch(this.state)
        {
            case GameState.Lobby:
                GUILayout.BeginVertical();
                GUI.enabled = Network.Badumna.IsLoggedIn;
                GUILayout.Label("Match name:");
                this.matchName = GUILayout.TextField(this.matchName);
                if(GUILayout.Button("Create match"))
                {
                    this.CreateMatch();
                    this.state = GameState.CreatingMatch;
                }
                
                if(GUILayout.Button("Find match"))
                {
                    this.FindMatch();
                }

                GUI.enabled = true;
                GUILayout.EndVertical();
                break;
            case GameState.CreatingMatch:
                GUILayout.Label("Creating match ...");
                break;
            case GameState.FindingMatch:
                if(this.matchQueryHandled)
                {
                    if(this.matchList.Count == 0)
                    {
                        GUILayout.Label("No match found.");
                    }
                    else
                    {
                        foreach(var match in this.matchList)
                        {
                            GUILayout.BeginHorizontal();
                            GUILayout.Label(string.Format(
                                "{2} - ({0}/{1})", 
                                match.Capacity.NumSlots - match.Capacity.EmptySlots, 
                                match.Capacity.NumSlots, match.Criteria.MatchName));
                            
                            if(GUILayout.Button("Join"))
                            {
                                this.JoinMatch(match);
                            }
                            GUILayout.EndHorizontal();
                        }
                    }

                    GUILayout.Space(20);                        
                    if(GUILayout.Button("Back to lobby"))
                    {
                        this.state = GameState.Lobby;
                    }
                }
                else
                {
                    GUILayout.Label("Searching for match ...");
                }
                break;
            case GameState.JoiningMatch:
                GUILayout.Label("Joining match ...");
                break;
            case GameState.JoinedMatch:
                foreach(var member in this.match.Controller.Members)
                {
                    GUILayout.Label(member.Name);
                }

                GUILayout.Space(20);
#if MATCH_CONTROLLER
                #region highlight MatchmakingWindow method (Controller)
                if(GameManager.Manager.IsHost)
                {
                    GUI.enabled = this.match.Controller.MatchReady;
                    if(GUILayout.Button("Start Match"))
                    {
                        this.match.CallMethodOnMembers(this.match.Controller.StartMatch);
                        this.HandleMatchStart();
                    }
                    GUI.enabled = true;
                }
                else
                {
                    GUI.enabled = !this.match.Controller.IsReady;
                    if(GUILayout.Button("Ready"))
                    {
                        this.match.Controller.IsReady = true;
                        this.match.CallMethodOnHost(this.match.Controller.Ready, GameManager.Manager.PlayerName);
                    }
                    GUI.enabled = true;
                }
                #endregion // highlight MatchmakingWindow method (Controller)
#endif // MATCH_CONTROLLER

                if(GUILayout.Button("Back to lobby"))
                {
                    this.LeaveMatch();
                    this.state = GameState.Lobby;
                }
                break;
#if MATCH_CONTROLLER                
            #region highlight MatchmakingWindow method (Controller)
            case GameState.MatchEnded:
#if MATCH_ADVANCE
                #region highlight MatchmakingWindow method (Advance)
                GUILayout.Label(this.finalScore);
                #endregion // highlight MatchmakingWindow method (Advance)
#endif // MATCH_ADVANCE
                if(GameManager.Manager.IsHost)
                {
                    if(GUILayout.Button("Restart match"))
                    {
                        this.match.CallMethodOnMembers(this.match.Controller.StartMatch);
                        this.HandleMatchStart();
                    }
                }

                if(GUILayout.Button("Back to lobby"))
                {
                    this.LeaveMatch();
                    this.state = GameState.Lobby;
                }
                break;
            #endregion // highlight MatchmakingWindow method (Controller)
#endif // MATCH_CONTROLLER
        }
    }
    #endregion // MatchmakingWindow method (Advance)
    #endregion // MatchmakingWindow method (Controller)
    #endregion // MatchmakingWindow method

#endif // MATCH_BASIC
#if MATCH_CONTROLLER
    #region HandleMatchStart method
    #region HandleMatchStart method (Replication)
    #region HandleMatchStart method (Chat)
    #region HandleMatchStart method (HostedEntity)
    #region HandleMatchStart method (Advance)
    /// <summary>
    /// Handle match start.
    /// </summary>
    private void HandleMatchStart()
    {
        this.state = GameState.Loading;
        this.hideMatchmakingWindow = true;

        // sync the time
        var timer = this.gameObject.GetComponent<Timer>();

        if(timer == null)
        {
            timer = this.gameObject.AddComponent<Timer>();
        }

        timer.Skin = GameManager.Manager.Skin;
        timer.StartTimer(
            () => 
            {
                // Handle end match.
                if(GameManager.Manager.IsHost)
                {
                    this.match.CallMethodOnMembers(this.match.Controller.EndMatch);
                    this.HandleMatchEnd();
                }
            });
#if MATCH_REPLICATION
        #region highlight HandleMatchStart method (Replication)
        this.RegisterLocalPlayer();
        #endregion // highlight HandleMatchStart method (Replication)
#endif // MATCH_REPLICATION
#if MATCH_CHAT
        #region highlight HandleMatchStart method (Chat)
        var chat = this.gameObject.AddComponent<Chat>();
        chat.Match = this.match;
        #endregion // highlight HandleMatchStart method (Chat)
#endif // MATCH_CHAT
#if MATCH_HOSTED_ENTITY
        #region highlight HandleMatchStart method (HostedEntity)
        this.RegisterHostedEntity();
        #endregion // highlight HandleMatchStart method (HostedEntity)
#endif // MATCH_HOSTED_ENTITY
#if MATCH_ADVANCE
        #region highlight HandleMatchStart method (Advance)
        this.match.Controller.ResetGameScore();
        #endregion // highlight HandleMatchStart method (Advance)
#endif // MATCH_ADVANCE
        this.state = GameState.MatchStarted;
    }
    #endregion // HandleMatchStart method (Advance)
    #endregion // HandleMatchStart method (HostedEntity)
    #endregion // HandleMatchStart method (Chat)
    #endregion // HandleMatchStart method (Replication)
    #endregion // HandleMatchStart method

    #region HandleMatchEnd method
    #region HandleMatchEnd method (Advance)
    #region HandleMatchEnd method (Replication)
    #region HandleMatchEnd method (Chat)
    #region HandleMatchEnd method (HostedEntity)
    /// <summary>
    /// Handle match end.
    /// </summary>
    private void HandleMatchEnd()
    {
        this.state = GameState.MatchEnded;
        this.hideMatchmakingWindow = false;
#if MATCH_REPLICATION
        #region highlight HandleMatchEnd method (Replication)
        this.UnregisterLocalPlayer();
        #endregion // highlight HandleMatchEnd method (Replication)
#endif // MATCH_REPLICATION
#if MATCH_CHAT
        #region highlight HandleMatchEnd method (Chat)
        Destroy(this.gameObject.GetComponent<Chat>());
        #endregion // highlight HandleMatchEnd method (Chat)
#endif // MATCH_CHAT
#if MATCH_HOSTED_ENTITY
        #region highlight HandleMatchEnd method (HostedEntity)
        if(this.state == GameState.MatchEnded)
        {
            this.UnregisterHostedEntity();
        }
        #endregion // highlight HandleMatchEnd method (HostedEntity)
#endif // MATCH_HOSTED_ENTITY
#if MATCH_ADVANCE
        #region highlight HandleMatchEnd method (Advance)
        this.finalScore = this.match.Controller.GameScore;
        #endregion // highlight HandleMatchEnd method (Advance)
#endif // MATCH_ADVANCE
    }

    #endregion // HandleMatchEnd method (HostedEntity)
    #endregion // HandleMatchEnd method (Chat)
    #endregion // HandleMatchEnd method (Replication)
    #endregion // HandleMatchEnd method (Advance)
    #endregion // HandleMatchEnd method
#endif // MATCH_CONTROLLER
#if MATCH_REPLICATION
    #region LocalPlayer registration
    /// <summary>
    /// Register local player with the match.
    /// </summary>
    private void RegisterLocalPlayer()
    {
        this.CreateLocalPlayer();

        if(this.match != null && this.localPlayer != null)
        {
            this.match.RegisterEntity(this.localPlayer, EntityType);
        }
    }

    /// <summary>
    /// Unregister local player from the match.
    /// </summary>
    private void UnregisterLocalPlayer()
    {
        if(this.localPlayer!= null)
        {
            this.match.UnregisterEntity(this.localPlayer);
            Destroy(this.localPlayer.gameObject);
        }
    }
    #endregion // LocalPlayer registration

#endif // MATCH_REPLICATION
#if MATCH_HOSTED_ENTITY
    #region Hosted entity registration
    /// <summary>
    /// Register hosted entity with the match.
    /// </summary>
    private void RegisterHostedEntity()
    {
        if(GameManager.Manager.IsHost)
        {
            for (var i=0; i < 5; i++)
            {
                var npcObject = (GameObject)Instantiate(
                    GameManager.Manager.ObjectPrefab, 
                    new UnityEngine.Vector3(UnityEngine.Random.Range(-10, 10), -16.5f, UnityEngine.Random.Range(-10,10)), 
                    transform.rotation);
                var npcScript = npcObject.AddComponent<NPC>();
#if MATCH_ADVANCE
                #region highlight
                npcScript.Match = this.match;
                #endregion // highlight
#endif
                this.hostedEntities.Add(npcScript);
                this.match.RegisterHostedEntity(npcScript, HostedEntityType);
            }
        }
    }

    /// <summary>
    /// Unregister hosted entity from the match.
    /// </summary>
    private void UnregisterHostedEntity()
    {
        if(GameManager.Manager.IsHost)
        {
            foreach(var npc in this.hostedEntities)
            {
                this.match.UnregisterHostedEntity(npc);
                Destroy(npc.gameObject);
            }

            this.hostedEntities.Clear();
        }
    }
    #endregion // Hosted entity registration
#endif

    /// <summary>
    /// Create a local player.
    /// </summary>
    #region CreateLocalPlayer method
    private void CreateLocalPlayer()
    {
        #region highlight        
#if MATCH_REPLICATION
        var player = (GameObject)Instantiate(GameManager.Manager.PlayerPrefab, UnityEngine.Vector3.zero, transform.rotation);
        if (player == null)
        {
            return;
        }

        // create all the components required
        player.AddComponent<ThirdPersonController>();
        player.AddComponent<ThirdPersonSimpleAnimation>();
        var controller = player.GetComponent<CharacterController>();

        // set the center and radius of the character
        controller.radius = 0.4f;
        controller.center = new UnityEngine.Vector3(0, 1.1f, 0);

        this.localPlayer = player.AddComponent<Player>();
        this.localPlayer.CharacterName = GameManager.Manager.PlayerName;
        this.localPlayer.Match = this.match;

##if UNITY_IPHONE || UNITY_ANDROID
        var thirdPersonController = player.GetComponent<ThirdPersonController>();
        var joystick = (GameObject)Instantiate(GameManager.Manager.JoystickPrefab);
        thirdPersonController.joystickController = joystick.GetComponent<Joystick>();
##endif
#else // MATCH_REPLICATION
        // Create a player object that will to be rendered in the scene.
        // The player object must include a script component that implements
        // Badumna's ISpatialEntity interface.
        throw new NotImplementedException("You must provide player creation code.");
#endif // MATCH_REPLICATION
        #endregion // highlight
    }
    #endregion // CreateLocalPlayer method

    #region Replica delegates
    #region Replica delegates (HostedEntity)
    #region Replica delegates (Advance)
    /// <summary>
    /// Called by Badumna to create a replica entity.
    /// </summary>
    /// <param name="match">The match the replica entity belongs to.</param>
    /// <param name="source">Type of entity source (e.g. peer, hosted entity).</param>
    /// <param name="entityType">An integer indicating the type of entity to create.</param>
    /// <returns>A new replica entity.</returns>
    private object CreateReplica(Match match, EntitySource source, uint entityType)
    {
#if MATCH_HOSTED_ENTITY
#region highlight Replica delegates (HostedEntity)
        if(source == EntitySource.Host)
        {
            var npc = (GameObject)Instantiate(GameManager.Manager.ObjectPrefab, UnityEngine.Vector3.zero, transform.rotation);
            var npcScript = npc.AddComponent<NPC>();
#if MATCH_ADVANCE
            #region highlight Replica delegates (Advance)
            npcScript.Match = this.match;
            #endregion // highlight Replica delegates (Advance)
#endif // MATCH_ADVANCE
            this.hostedEntities.Add(npcScript);
            return npcScript;
        }
#endregion //highlight Replica delegates (HostedEntity)

#endif // MATCH_HOSTED_ENTITY
#if MATCH_REPLICATION
#region highlight Replica delegates
        var remotePlayer = (GameObject)Instantiate(GameManager.Manager.PlayerPrefab, UnityEngine.Vector3.zero, transform.rotation);
        if (remotePlayer == null)
        {
            Debug.LogError("Failed to instantiate avatar prefab for entity type " + entityType);
            return null;
        }

        remotePlayer.AddComponent<ThirdPersonSimpleAnimation>();
#if MATCH_ADVANCE
        #region highlight Replica delegates (Advance)
        // set the center and radius of the character
        var controller = remotePlayer.AddComponent<CharacterController>();
        controller.radius = 0.4f;
        controller.center = new UnityEngine.Vector3(0, 1.1f, 0);
        #endregion // highlight Replica delegates (Advance)
#endif // MATCH_ADVANCE
        var replica = remotePlayer.AddComponent<Player>();
        return replica;
#endregion //highlight Replica delegates
#else // MATCH_REPLICATION
        // Add your game's replica creation code here.
        throw new NotImplementedException("You must provide replica creation code.");
#endif // MATCH_REPLICATION
    }

    /// <summary>
    /// Called by Badumna to remove a replica when it moves out of your local
    /// entities' interest area.
    /// </summary>
    /// <param name="match">The match the replica is being removed from.</param>
    /// <param name="source">Type of entity source (e.g. peer, hosted entity).</param>
    /// <param name="replica">The replica to removed.</param>
    private void RemoveReplica(Match match, EntitySource source, object replica)
    {
#if MATCH_HOSTED_ENTITY
#region highlight Replica delegates (HostedEntity)
        if(source == EntitySource.Host)
        {
            var npcScript = replica as NPC;
            if(npcScript != null)
            {
                Destroy(npcScript.gameObject);
                this.hostedEntities.Remove(npcScript);
            }

            return;
        }
#endregion //highlight Replica delegates (HostedEntity)

#endif // MATCH_HOSTED_ENTITY
#if MATCH_REPLICATION
#region highlight Replica delegates
        var player = replica as Player;
        if (player != null)
        {
           Destroy(player.gameObject);
        }
#endregion // highlight Replica delegates
#else // MATCH_REPLICATION
        // Add your game's replica removal code here.
        throw new NotImplementedException("You must provide replica removal code.");
#endif // MATCH_REPLICATION
    }
    #endregion // Replica delegates (Advance)
    #endregion // Replica delegates (HostedEntity)
    #endregion // Replica delegates
}
