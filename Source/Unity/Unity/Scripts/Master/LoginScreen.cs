﻿//-----------------------------------------------------------------------
// <copyright file="LoginScreen.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using UnityEngine;
#if DEI
#region using statements
using System.Collections.Generic;
using Badumna.Security;
using Dei;
#endregion // using statements
#endif

/// <summary>
/// The LoginScreen class is used to display the Login GUI window,
/// and trigger login.
/// </summary>
public class LoginScreen : MonoBehaviour
{
    /// <summary>
    /// A value indicating whether to show the login window.
    /// </summary>
    private bool hideLoginWindow;
    
    /// <summary>
    /// The position of the window.
    /// </summary>
    private Rect windowRect;

#if DEI
#region dei fields
    /// <summary>
    /// The player password.
    /// </summary>
    private string password;

    /// <summary>
    /// A value indicating whether the character window is hidden.
    /// </summary>
    private bool hideCharacterWindow;

    /// <summary>
    /// Character selection windows.
    /// </summary>
    private Rect characterWindow;

    /// <summary>
    /// Character selection scroll bar.
    /// </summary>
    private Vector2 scrollPosition;

    /// <summary>
    /// List of characters.
    /// </summary>
    private List<Character> characters = new List<Character>();

    /// <summary>
    /// The session used to authenticate and obtain a Dei IdentityProvider
    /// </summary>
    private Session session;

    /// <summary>
    /// The identity provider - available after successfully authenticating and selecting an identity.
    /// </summary>
    private IIdentityProvider identity;
#endregion // dei fields

#region dei properties
    /// <summary>
    /// Gets the character name.
    /// </summary>
    public string CharacterName { get; private set; }

    /// <summary>
    /// Gets the identity provider.
    /// </summary>
    public IIdentityProvider IdentityProvider
    {
        get
        {
            return identity;
        }
    }
#endregion //dei properties

#endif // DEI
    /// <summary>
    /// Gets the user name chosen by the player.
    /// </summary>
    public string UserName { get; private set; }

    /// <summary>
    /// Gets a value indicating whether the use have login.
    /// </summary>
    public bool HaveLogin { get; private set; }

#region Start method
    /// <summary>
    /// Called by Unity before any Update method is called for the first time.
    /// </summary>
    private void Start()
    {
        this.UserName = string.Empty;
        this.hideLoginWindow = false;
#if DEI
#region highlight
        this.CharacterName = string.Empty;
        this.password = string.Empty;
        this.hideCharacterWindow = true;

        this.characterWindow = new Rect(
            ((Screen.width / 2) - 120 * GameManager.Manager.GuiScale), 
            ((Screen.height / 2) - 95 * GameManager.Manager.GuiScale), 
            240 * GameManager.Manager.GuiScale, 
            190 * GameManager.Manager.GuiScale);
#endregion // highlight
        this.windowRect = new Rect(
            ((Screen.width / 2) - 100 * GameManager.Manager.GuiScale),
            ((Screen.height / 2) - 40 * GameManager.Manager.GuiScale),
            200 * GameManager.Manager.GuiScale,
#region highlight
            100 * GameManager.Manager.GuiScale);
#endregion // highlight
#else

        this.windowRect = new Rect(
            ((Screen.width / 2) - 100 * GameManager.Manager.GuiScale),
            ((Screen.height / 2) - 40 * GameManager.Manager.GuiScale),
            200 * GameManager.Manager.GuiScale,
            80 * GameManager.Manager.GuiScale);
#endif // DEI        
    }
#endregion // Start method

#region OnGUI method
    /// <summary>
    /// Called by Unity for rendering and GUI events.
    /// </summary>
    private void OnGUI()
    {
#if DEI
        if (this.hideLoginWindow && hideCharacterWindow)
        {
            return;
        }

        if (Network.Badumna == null)
        {
            return;
        }

        GUI.skin = GameManager.Manager.Skin;

        if (!this.hideLoginWindow)
        {
            this.windowRect = GUI.Window(0, this.windowRect, this.LoginWindow, "Login");
        }
        else if (!this.hideCharacterWindow)
        {
            this.characterWindow = GUI.Window(1, this.characterWindow, this.CharacterWindow, "Character");
        }
#else
        if (this.hideLoginWindow || Network.Badumna == null)
        {
            return;
        }

        GUI.skin = GameManager.Manager.Skin;
  
        this.windowRect = GUI.Window(0, this.windowRect, this.LoginWindow, "Login");
#endif // DEI
    }
#endregion // OnGUI method

#region LoginWindow method
    /// <summary>
    /// Function for creating Login window.
    /// </summary>
    /// <param name="id">ID not used (but required for passing to Gui.Window).</param>
    private void LoginWindow(int id)
    {
        GUILayoutOption[] options =
        {
            GUILayout.Width(100 * GameManager.Manager.GuiScale),
            GUILayout.Height(20 * GameManager.Manager.GuiScale)
        };
        GUILayout.BeginHorizontal();
        GUILayout.Label("UserName");
        this.UserName = GUILayout.TextField(this.UserName, options);
        GUILayout.EndHorizontal();
#if DEI
#region highlight
        GUILayout.BeginHorizontal();
        GUILayout.Label("Password");
        this.password = GUILayout.PasswordField(this.password, '*', 25, options);
        GUILayout.EndHorizontal();
#endregion // highlight
#endif // DEI
        var buttonDown = GUILayout.Button("Login");
        var keyDown = Event.current.type == EventType.keyDown &&
            Event.current.character == '\n';
        if (buttonDown || keyDown)
        {
            this.DoLogin();
        }
    }
#endregion // LoginWindow method

#region DoLogin method
    /// <summary>
    /// Trigger login.
    /// </summary>
    private void DoLogin()
    {
#if DEI
        var split = this.GetComponent<NetworkConfiguration>().DeiServer.Split(':');

        if(split.Length != 2)
        {
            Debug.LogError("Failed to login: invalid Dei server address");
            return;
        }

        this.session = new Session(split[0], ushort.Parse(split[1]), Session.GenerateKeyPair(), null);
        this.session.UseSslConnection = false;

        var result = this.session.Authenticate(this.UserName, this.password);
        if (result.WasSuccessful)
        {
            this.hideLoginWindow = true;
            this.hideCharacterWindow = false;
            this.characters = result.Characters;
        }
        else
        {
            Debug.LogError(string.Format("Failed to login: {0}", result.ErrorDescription));
            this.CloseSession();
        }
#else
        this.hideLoginWindow = true;
        this.HaveLogin = true;
#endif // DEI
    }   
#endregion // DoLogin method
#if DEI
#region Character management methods

    /// <summary>
    /// Function for creating Character window.
    /// </summary>
    /// <param name="id">ID not used (but required for passing to Gui.Window).</param>
    private void CharacterWindow(int id)
    {
        GUILayout.BeginVertical();

        // New character
        GUILayout.Box("New Character");
        GUILayout.BeginHorizontal();
        GUILayout.Label(
            "Character name:", 
            new GUILayoutOption[] { GUILayout.Width(100 * GameManager.Manager.GuiScale) });

        CharacterName = GUILayout.TextField(
            CharacterName, 
            new GUILayoutOption[] { GUILayout.Width(75 * GameManager.Manager.GuiScale) });
        
        var addButton = GUILayout.Button(
            "Add", 
            new GUILayoutOption[] { GUILayout.Width(35 * GameManager.Manager.GuiScale) });
        GUILayout.EndHorizontal();

        // Selecting or deleting character
        GUILayout.Box("Select Character");
        scrollPosition = GUILayout.BeginScrollView(scrollPosition);

        // can't delete character directly from _characters list 
        // as the DeleteCharacter is called inside the foreach clause.
        List<Character> deletedCharacters = new List<Character>();

        foreach (var character in characters)
        {
            GUILayout.BeginHorizontal();

            if (GUILayout.Button(
                "Select", 
                new GUILayoutOption[] { GUILayout.Width(50 * GameManager.Manager.GuiScale) }))
            {
                SelectCharacter(character.Name);
            }

            if (GUILayout.Button(
                "Delete", 
                new GUILayoutOption[] { GUILayout.Width(50 * GameManager.Manager.GuiScale) }))
            {
                DeleteCharacter(character.Name, deletedCharacters);
            }

            GUILayout.Box(character.Name);
            GUILayout.EndHorizontal();
        }

        GUILayout.EndScrollView();
        GUILayout.EndVertical();

        if (addButton)
        {
            AddCharacter(CharacterName);
            CharacterName = string.Empty;
        }

        foreach (var character in deletedCharacters)
        {
            characters.Remove(character);
        }

        GUI.DragWindow();
    }

    /// <summary>
    /// Selecting character.
    /// </summary>
    private void SelectCharacter(string characterName)
    {
        var character = characters.Find((Character x) => x.Name == characterName);

        if (character != null)
        {
            var result = this.session.SelectIdentity(character, out identity);
            if (!result.WasSuccessful)
            {
                Debug.LogError("Failed to select identity.");
                return;
            }

            this.CloseSession();
            this.CharacterName = character.Name;
            this.hideCharacterWindow = true;
            this.HaveLogin = true;
        }
    }

    /// <summary>
    /// Creating a new character.
    /// </summary>
    /// <param name="characterName">New character name.</param>
    private void AddCharacter(string characterName)
    {
        var character = session.CreateCharacter(characterName);

        if (character != null)
            this.characters.Add(character);
    }

    /// <summary>
    /// Deleting a character from Dei server. 
    /// </summary>
    /// <param name="characterName">Character name.</param>
    /// <param name="deletedCharacters">Deleted character list.</param>
    private void DeleteCharacter(string characterName, List<Character> deletedCharacters)
    {
        var character = characters.Find((Character x) => x.Name == characterName);

        if (character != null)
        {
            if (this.session.DeleteCharacter(character))
            {
                Debug.Log(
                    string.Format("Successfully delete {0} from dei server.", characterName));
                // Cannot remove the character from characters directly.
                deletedCharacters.Add(character);
            }
            else
            {
                Debug.Log(
                    string.Format("Deletion of {0} from dei server failed.", characterName));
            }
        }
    }

    /// <summary>
    /// Disposes the active session, if present.
    /// </summary>
    private void CloseSession()
    {
        if (this.session != null)
        {
            this.session.Dispose();
            this.session = null;
        }
    }
#endregion // Character management methods
#endif // DEI
}
