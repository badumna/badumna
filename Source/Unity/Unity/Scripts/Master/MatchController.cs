//-----------------------------------------------------------------------
// <copyright file="MatchController.cs" company="Scalify">
//     Copyright (c) 2013 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Badumna;
using Badumna.Match;

/// <summary>
/// Start match handler.
/// </summary>
public delegate void StartMatchHandler();

/// <summary>
/// End match handler.
/// </summary>
public delegate void EndMatchHandler();
#if MATCH_ADVANCE

#region StateChangedHandler delegate
/// <summary>
/// Game state changed handler.
/// </summary>
public delegate void StateChangedHandler(GameState state);
#endregion // StateChangedHandler delegate

#endif // MATCH_ADVANCE
/// <summary>
/// Match controller class is used to control the match logic from host to clients and vice versa.
/// </summary>
public class MatchController 
{
    /// <summary>
    /// Match members list.
    /// </summary>
    private List<MemberIdentity> members = new List<MemberIdentity>();

#if MATCH_ADVANCE
    #region Demo Match-6 Fields 
    /// <summary>
    /// Player scores list. 
    /// </summary>
    private Dictionary<string, int> playerScores = new Dictionary<string,int>();
    #endregion // Demo Match-6 Fields 

#endif // MATCH_ADVANCE
#if MATCH_CONTROLLER
    #region Demo Match-1 fields
    /// <summary>
    /// On match started event.
    /// </summary>
    public event StartMatchHandler OnMatchStarted;

    /// <summary>
    /// On match ended event.
    /// </summary>
    public event EndMatchHandler OnMatchEnded;

#if MATCH_ADVANCE
    #region Demo Match-6 event 
    /// <summary>
    /// On state change event.
    /// </summary>
    public event StateChangedHandler OnStateChanged;
    #endregion // Demo Match-6 event 

#endif // MATCH_ADVANCE
    /// <summary>
    /// List of ready members.
    /// </summary>
    private List<string> readyMembers = new List<string>();
    #endregion // Demo Match-1 fields

    #region Demo Match-1 properties
    /// <summary>
    /// Gets a value indicating whether all the match member except the host are ready.
    /// </summary>
    public bool MatchReady
    {
        get
        {
            return this.readyMembers.Count == this.members.Count - 1;
        }
    }

    /// <summary>
    /// Gets or sets a value indicating whether this member is ready.
    /// </summary>
    public bool IsReady { get; set; }
    #endregion // Demo Match-1 properties

#endif // MATCH_CONTROLLER
    /// <summary>
    /// Gets the members list.
    /// </summary>
    public IEnumerable<MemberIdentity> Members
    {
        get { return this.members; }
    }

#if MATCH_ADVANCE
    #region GameScore property
    /// <summary>
    /// Gets the game score representation in string.
    /// </summary>
    public string GameScore
    {
        get 
        {            
            return this.GenerateGameScore(); 
        }
    }
    #endregion GameScore property

    #region AddScore method
    /// <summary>
    /// Add score to the particular match member.
    /// </summary>
    /// <param name="playerName">Player name.</param>
    [Replicable]
    public void AddScore(string playerName)
    {
        if(!this.playerScores.ContainsKey(playerName))
        {
            Debug.LogError("Trying to add a score for unknown player");
        }   

        this.playerScores[playerName]++;
    }
    #endregion // AddScore method

    #region GenerateGameScore method
    /// <summary>
    /// This method is called by the host to generate the game score represents in string. 
    /// </summary>
    public string GenerateGameScore()
    {
        var gameScore = string.Empty;

        foreach(var keyValuePair in this.playerScores)
        {
            gameScore += string.Format("{0}\t\t{1}\n", keyValuePair.Key, keyValuePair.Value);
        }

        return gameScore;
    }    
    #endregion // GenerateGameScore method  

    #region ResetGameScore method
    /// <summary>
    /// Reset the game score. 
    /// </summary>
    public void ResetGameScore()
    {
        var keys = new List<string>(this.playerScores.Keys);
        foreach(var key in keys)
        {
            this.playerScores[key] = 0;
        }
    }
    #endregion // ResetGameScore method

    #region SyncGameScoreTo method
    /// <summary>
    /// Sync the game score to a specific member. 
    /// </summary>
    public void SyncGameScoreTo(Match<MatchController> match, MemberIdentity member)
    {
        match.CallMethodOnMember(member, this.SyncGameScore, this.playerScores);
    }
    #endregion // SyncGameScoreTo method
#endif // MATCH_ADVANCE

    #region AddMember method
    /// <summary>
    /// Adding a new member to member list.
    /// </summary>
    /// <param name="match">Match where the new member belong to.</param>
    /// <param name="e">Match membership event arguments.</param>
    public void AddMember(Match match, MatchMembershipEventArgs e)
    {
        this.members.Add(e.Member);
#if MATCH_ADVANCE
        #region highlight
        this.playerScores.Add(e.Member.Name, 0);
        #endregion // highlight
#endif // MATCH_ADVANCE
    }
    #endregion // AddMember method

    #region RemoveMember method (Controller)
    #region RemoveMember method (Advance)
    /// <summary>
    /// Remove an existing member from the list.
    /// </summary>
    /// <param name="match">Match where the member belong to.</param>
    /// <param name="e">Match membership event arguments.</param>
    public void RemoveMember(Match match, MatchMembershipEventArgs e)
    {
        this.members.Remove(e.Member);
#if MATCH_CONTROLLER
        #region highlight RemoveMember method (Controller)
        this.readyMembers.Remove(e.Member.Name);
        #endregion // highlight RemoveMember method (Controller)
#endif
#if MATCH_ADVANCE
        #region highlight RemoveMember method (Advance)
        this.playerScores.Remove(e.Member.Name);
        #endregion // highlight RemoveMember method (Advance)
#endif // MATCH_ADVANCE
    }
    #endregion // RemoveMember method (Advance)
    #endregion // RemoveMember method (Controller)

#if MATCH_CONTROLLER
    #region Match controller RPCs
    /// <summary>
    /// This RPC method is called when the match is started in the client side.
    /// </summary>
    [Replicable]
    public void StartMatch()
    {
        var handler = this.OnMatchStarted;
        if(handler != null)
        {
            handler.Invoke();
        }
    }

    /// <summary>
    /// This RPC method is called when the match is ended in the client side.
    /// </summary>
    [Replicable]
    public void EndMatch()
    {
        var handler = this.OnMatchEnded;
        if(handler != null)
        {
            handler.Invoke();
        }
    }

    /// <summary>
    /// This RPC method is called on the host when a member is ready to start match.
    /// </summary>
    /// <param name="name">Player name.</param>
    [Replicable]
    public void Ready(string name)
    {
        this.readyMembers.Add(name);        
    }
    #endregion // Match controller RPCs
#endif // MATCH_CONTROLLER

#if MATCH_ADVANCE  
    #region SetGameState method  
    [Replicable]
    public void SetGameState(int state)
    {
        var handler = this.OnStateChanged;
        if(handler != null)
        {
            handler.Invoke((GameState)state);
        }
    }
    #endregion // SetGameState method  

    #region SyncGameScore method  
    [Replicable]
    public void SyncGameScore(Dictionary<string,int> scores)
    {
        this.playerScores = scores;
    }
    #endregion //SyncGameScore method  

    #region OnGUI method  
    public void OnGUI()
    {
        var width = 150; 
        var height = 200;

        GUILayout.BeginArea(new Rect((int)(((Screen.width) - width)), 40 * GameManager.Manager.GuiScale, width, height));
        GUILayout.Label(this.GameScore);
        GUILayout.EndArea();
    }
    #endregion // OnGUI method  
#endif // MATCH_ADVANCE
}
