//-----------------------------------------------------------------------
// <copyright file="Scene.cs" company="Scalify">
//     Copyright (c) 2013 Scalify. All rights reserved.
// </copyright>
//----------------------------------------------------------------------

using System;
using UnityEngine;
using Badumna;
using Badumna.SpatialEntities;
using BadumnaId = Badumna.DataTypes.BadumnaId;
using BadumnaVector3 = Badumna.DataTypes.Vector3;

/// <summary>
/// Handle Badumna scene functionalities.
/// </summary>
public class Scene : MonoBehaviour
{
    /// <summary>
    /// The entity type for the player character.
    /// </summary>
    private const uint EntityType = 0;

	/// <summary>
    /// A Badumna network scene.
    /// </summary>
    private NetworkScene networkScene;

    /// <summary>
    /// The local player character.
    /// </summary>
    private Player localPlayer;

    /// <summary>
    /// A value indicating if the local player is registered with a Badumna network scene.
    /// </summary>
    private bool isRegistered;

    /// <summary>
    /// Join a Badumna network scene.
    /// </summary>
    /// <param name="sceneName">The name of the scene to join.</param>
    #region JoinScene method (Replication)
    #region JoinScene method (Chat)
    #region JoinScene method (ArbitrationServer)
    public void JoinScene(string sceneName)
    {
        this.CreateLocalPlayer();

        if (this.localPlayer != null && this.networkScene == null)
        {
            // Join the normal scene or a mini scene.  
            this.networkScene = Network.Badumna.JoinScene(sceneName, this.CreateReplica, this.RemoveReplica);
            this.networkScene.RegisterEntity(this.localPlayer, EntityType, 2f, 20f);
            this.isRegistered = true;
#if REPLICATION
            #region highlight JoinScene method (Replication)
            this.localPlayer.Scene = this.networkScene;
            #endregion // highlight JoinScene method (Replication)
#endif // REPLICATION
#if CHAT

#region // highlight JoinScene method (Chat)
            gameObject.GetComponent<Chat>().Subscribe(this.localPlayer);
#endregion // highlight JoinScene method (Chat)
#endif // CHAT
#if ARBITRATION_SERVER
#region // highlight JoinScene method (ArbitrationServer)
            gameObject.GetComponent<FriendList>().RequestFriendsList(this.localPlayer.CharacterName);
#endregion // highlight JoinScene method (ArbitrationServer)
#endif // ARBITRATION_SERVER
        }
    }
    #endregion // JoinScene method (ArbitrationServer)
    #endregion // JoinScene method (Chat)
    #endregion // JoinScene method (Replication)

    /// <summary>
    /// Leave the Badumna network scene.
    /// </summary>
    #region LeaveScene method
    public void LeaveScene()
    {
        if (this.isRegistered && this.localPlayer != null && this.networkScene != null)
        {
            this.networkScene.UnregisterEntity(this.localPlayer);
            this.networkScene.Leave();
            this.networkScene = null;
            this.isRegistered = false;
#if CHAT

#region // highlight
            gameObject.GetComponent<Chat>().Unsubscribe();
#endregion // highlight
#endif // CHAT

            Destroy(this.localPlayer.gameObject);
        }
    }
    #endregion // LeaveScene method

    /// <summary>
    /// Create a local player.
    /// </summary>
    #region CreateLocalPlayer method
    private void CreateLocalPlayer()
    {
#region highlight
#if REPLICATION
        var player = (GameObject)Instantiate(GameManager.Manager.PlayerPrefab, UnityEngine.Vector3.zero, transform.rotation);
        if (player == null)
        {
            return;
        }

        // create all the components required
        player.AddComponent<ThirdPersonController>();
        player.AddComponent<ThirdPersonSimpleAnimation>();
        var controller = player.GetComponent<CharacterController>();

        // set the center and radius of the character
        controller.radius = 0.4f;
        controller.center = new UnityEngine.Vector3(0, 1.1f, 0);

        this.localPlayer = player.AddComponent<Player>();
        this.localPlayer.CharacterName = GameManager.Manager.PlayerName;

##if UNITY_IPHONE || UNITY_ANDROID
        var thirdPersonController = player.GetComponent<ThirdPersonController>();
        var joystick = (GameObject)Instantiate(GameManager.Manager.JoystickPrefab);
        thirdPersonController.joystickController = joystick.GetComponent<Joystick>();
##endif
#else // REPLICATION
        // Create a player object that will to be rendered in the scene.
        // The player object must include a script component that implements
        // Badumna's ISpatialEntity interface.
        throw new NotImplementedException("You must provide player creation code.");
#endif // REPLICATION
#endregion // highlight
    }
    #endregion // CreateLocalPlayer method

    #region Replica delegates
    /// <summary>
    /// Called by Badumna to create a replica entity.
    /// </summary>
    /// <param name="scene">The scene the replica entity belongs to.</param>
    /// <param name="entityId">An ID for the entity.</param>
    /// <param name="entityType">An integer indicating the type of entity to create.</param>
    /// <returns>A new replica entity.</returns>
    private IReplicableEntity CreateReplica(NetworkScene scene, BadumnaId entityId, uint entityType)
    {
#if REPLICATION
#region highlight
        var remotePlayer = (GameObject)Instantiate(GameManager.Manager.PlayerPrefab, UnityEngine.Vector3.zero, transform.rotation);
        if (remotePlayer == null)
        {
            Debug.LogError("Failed to instantiate avatar prefab for entity type " + entityType);
            return null;
        }
        
        remotePlayer.AddComponent<ThirdPersonSimpleAnimation>();
        var replica = remotePlayer.AddComponent<Player>();
        return replica;
#endregion //highlight
#else // REPLICATION
        // Add your game's replica creation code here.
        throw new NotImplementedException("You must provide replica creation code.");
#endif // REPLICATION
    }

    /// <summary>
    /// Called by Badumna to remove a replica when it moves out of your local
    /// entities' interest area.
    /// </summary>
    /// <param name="scene">The scene the replica is being removed from.</param>
    /// <param name="replica">The replica to removed.</param>
    private void RemoveReplica(NetworkScene scene, IReplicableEntity replica)
    {
#if REPLICATION
#region highlight
        var player = replica as Player;
        if (player != null)
        {
           Destroy(player.gameObject);
        }
#endregion highlight
#else // REPLICATION
        // Add your game's replica removal code here.
        throw new NotImplementedException("You must provide replica removal code.");
#endif // REPLICATION
    }
    #endregion // Replica delegates
}