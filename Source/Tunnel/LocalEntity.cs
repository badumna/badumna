﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using Badumna;
using Badumna.DataTypes;
using Badumna.Facade.Http;
using Badumna.Facade.Http.Change;
using Badumna.Utilities;
using Badumna.SpatialEntities;
using Tunnel.Handlers;
using Badumna.Utilities.LitJson;
using System.Diagnostics;


namespace Tunnel
{
    class LocalEntity : ProxiedEntity, ISpatialOriginal
    {
        private ChangelistHandler mChangelistHandler;

        /// <summary>
        /// The normal Badumna network facade.
        /// </summary>
        private INetworkFacade networkFacade;

        private SortedDictionary<int, byte[]> mChangedParts = new SortedDictionary<int, byte[]>();

        public override Vector3 Position
        {
            get { return base.Position; }

            set
            {
                this.networkFacade.FlagForUpdate(this, (int)SpatialEntityStateSegment.Position);
                base.Position = value;
            }
        }

        public override Vector3 Velocity
        {
            get { return base.Velocity; }

            set
            {
                this.networkFacade.FlagForUpdate(this, (int)SpatialEntityStateSegment.Velocity);
                base.Velocity = value;
            }
        }

        public override float Radius
        {
            get { return base.Radius; }

            set
            {
                this.networkFacade.FlagForUpdate(this, (int)SpatialEntityStateSegment.Radius);
                base.Radius = value;
            }
        }

        public override float AreaOfInterestRadius
        {
            get { return base.AreaOfInterestRadius; }

            set
            {
                this.networkFacade.FlagForUpdate(this, (int)SpatialEntityStateSegment.InterestRadius);
                base.AreaOfInterestRadius = value;
            }
        }

        public LocalEntity(uint entityType, ChangelistHandler changelistHandler, INetworkFacade networkFacade)
        {
            this.mChangelistHandler = changelistHandler;
            this.networkFacade = networkFacade;
            this.Guid = ((INetworkFacadeInternal)networkFacade).GenerateId();
            this.Type = entityType;
        }

        public static LocalEntity CreateLocalEntityFrom(TextReader reader, ChangelistHandler changelistHandler, INetworkFacade networkFacade)
        {
            LocalEntity entity = null;

            RegisterEntityRequest.FromJson(JsonMapper.ToObject(reader),
                delegate(uint entityType, JsonData update)
                {
                    entity = new LocalEntity(entityType, changelistHandler, networkFacade);
                    UpdateEntityRequest.UpdateEntityFromJson(update, entity, entity.AddChangedPart);
                });
           
            return entity;
        }

        public void AddChangedPart(int partId, byte[] part)
        {
            // We require that parts always arrive in order and only apply the last one received for each partId.
            this.mChangedParts[partId] = part;
            this.networkFacade.FlagForUpdate(this, partId);
        }

        [OnePartPerCall]
        public void Serialize(BooleanArray requiredParts, Stream stream)
        {
            bool writtenOne = false;

            foreach (KeyValuePair<int, byte[]> part in this.mChangedParts)
            {
                if (!requiredParts[part.Key])
                {
                    continue;
                }

                Debug.Assert(!writtenOne, "Tunnel.LocalEntity can only write one part at a time " +
                    "because we don't know the order the client app expects the parts in if multiple " + 
                    "parts occur in one update");  // This condition should be created by thte OnePartPerCall attribute

                stream.Write(part.Value, 0, part.Value.Length);

                writtenOne = true;
            }
        }

        public override void HandleEvent(Stream stream)
        {
            byte[] buffer;
            RemoteEntity.ReadToEnd(stream, out buffer);
            this.mChangelistHandler.WithCurrent(c => c.Add(SceneProcessor.MakeCustomMessage(this.Guid, false, buffer)));
        }
    }
}
