﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Globalization;
using System.Threading;

using Badumna;
using Badumna.Chat;
using Badumna.Facade.Http;
using Badumna.Utilities.LitJson;


namespace Tunnel
{
    class Changelist
    {
        private int mId;
        public int Id { get { return this.mId; } }

        private List<object> mChanges = new List<object>();

        private ManualResetEvent mNonEmpty = new ManualResetEvent(false);

        private bool isComplete;


        public Changelist(int id)
        {
            this.mId = id;
        }

        public void Add(object change)
        {
            if (this.isComplete)
            {
                throw new InvalidOperationException();
            }

            this.mChanges.Add(change);
            this.mNonEmpty.Set();
        }

        /// <summary>
        /// Marks the changelist as completed, meaning no more changes will be added to it.
        /// This MUST be called on every changelist created.
        /// </summary>
        public void Complete()
        {
            if (this.isComplete)
            {
                return;
            }

            this.isComplete = true;
            this.mNonEmpty.Set();
        }

        public bool WaitUntilNonEmpty(TimeSpan timeout)
        {
            return this.mNonEmpty.WaitOne(timeout, false);
        }

        public void SerializeTo(TextWriter writer)
        {
            writer.Write(JsonMapper.ToJson(this.mChanges));
        }
    }
}
