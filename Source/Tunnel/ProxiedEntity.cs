﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Diagnostics;
using System.Globalization;

using Badumna;
using Badumna.DataTypes;
using Badumna.SpatialEntities;


namespace Tunnel
{
    // Currently all proxied entities implement IDeadReckonable because there's no way in InstantiateRemoteEntity to determine
    // whether the entity is dead reckonable -- normally the application would create an instance that is dead reckonable or
    // not based on its knowledge of the entityTag.
    abstract class ProxiedEntity : ISpatialEntity, IDeadReckonable
    {
        public BadumnaId Guid { get; set; }
        
        public virtual Vector3 Position { get; set; }
        public virtual Vector3 Velocity { get; set; }

        public virtual float Radius { get; set; }
        public virtual float AreaOfInterestRadius { get; set; }

        public uint Type { get; protected set; }

        public void SerializeTo(TextWriter writer)
        {
            writer.WriteLine(String.Format(CultureInfo.InvariantCulture, "{0},{1},{2}", this.Position.X, this.Position.Y, this.Position.Z));
            writer.WriteLine(String.Format(CultureInfo.InvariantCulture, "{0},{1},{2}", this.Velocity.X, this.Velocity.Y, this.Velocity.Z));
        }

        public void AttemptMovement(Vector3 reckonedPosition)
        {
            this.Position = reckonedPosition;
        }

        public abstract void HandleEvent(Stream stream);
    }
}
