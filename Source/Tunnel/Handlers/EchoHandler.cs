﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;


namespace Tunnel.Handlers
{
    class EchoHandler : IUriHandler
    {
        public EchoHandler()
        {
        }

        public bool Handle(string path, MarshalableListenerContext context)
        {
            context.ResponseCode = HttpStatusCode.OK;
            using (StreamWriter writer = new StreamWriter(context.ResponseStream))
            {
                writer.WriteLine("HttpMethod = {0}", context.RequestMethod);
                //writer.WriteLine("LocalEndPoint = {0}", context.Request.LocalEndPoint);
                //writer.WriteLine("RemoteEndPoint = {0}", context.Request.RemoteEndPoint);
                //writer.WriteLine("IsAuthenticated = {0}", context.Request.IsAuthenticated);
                //writer.WriteLine("IsSecureConnection = {0}", context.Request.IsSecureConnection);
                //writer.WriteLine("AcceptTypes = {0}", context.Request.AcceptTypes);
                //if (context.Request.HasEntityBody)
                {
                    writer.WriteLine("Body\n===================");
                    using (StreamReader reader = new StreamReader(context.RequestStream))
                    {
                        writer.WriteLine(reader.ReadToEnd());
                    }
                }
            }

            return true;
        }
    }
}
