﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

using Badumna;
using Badumna.Facade.Http;
using Badumna.Facade.Http.Change;
using Badumna.SpatialEntities;
using Badumna.DataTypes;


namespace Tunnel.Handlers
{
    class SceneHandler : CollectionHandler
    {
        private NetworkScene mScene;
        private EntityHandler mEntityHandler;
        private ChangelistHandler mChangelistHandler;

        /// <summary>
        /// The normal Badumna network facade.
        /// </summary>
        private INetworkFacade networkFacade;

        public SceneHandler(string sceneName, EntityHandler entityHandler, ChangelistHandler changelistHandler, INetworkFacade networkFacade)
            : base(sceneName)
        {
            this.networkFacade = networkFacade;
            this.mScene = this.networkFacade.JoinScene(sceneName, this.CreateEntity, this.RemoveEntity);
            this.mEntityHandler = entityHandler;
            this.mChangelistHandler = changelistHandler;
        }


        public void Leave()
        {
            this.mScene.Leave();
        }

        private IReplicableEntity CreateEntity(NetworkScene scene, BadumnaId newEntityId, uint entityType)
        {
            RemoteEntity remoteEntity;
            if (!this.mEntityHandler.RemoteEntities.TryGetValue(newEntityId.ToString(), out remoteEntity))
            {
                remoteEntity = new RemoteEntity(entityType, newEntityId, this.mChangelistHandler, this.networkFacade);
                this.mEntityHandler.RemoteEntities[newEntityId.ToString()] = remoteEntity;
            }

            this.mChangelistHandler.WithCurrent(c => c.Add(SceneProcessor.MakeSceneJoin(scene.Name, newEntityId, entityType)));
            if (!remoteEntity.CurrentScenes.Contains(scene.Name))
            {
                remoteEntity.CurrentScenes.Add(scene.Name);
            }

            return remoteEntity;
        }

        private void RemoveEntity(NetworkScene scene, IReplicableEntity replica)
        {
            RemoteEntity entity = (RemoteEntity)replica;
            entity.CurrentScenes.Remove(scene.Name);
            if (entity.CurrentScenes.Count == 0)
            {
                this.mEntityHandler.RemoteEntities.Remove(entity.Guid.ToString());
            }

            this.mChangelistHandler.WithCurrent(c => c.Add(SceneProcessor.MakeScenePart(scene.Name, entity.Guid)));
        }

        protected override void CollectionGet(MarshalableListenerContext context)
        {
            Helpers.HttpResponseStatus(context, HttpStatusCode.OK, "Scene: " + this.mScene.Name);
        }

        protected override void CollectionPost(MarshalableListenerContext context)
        {
            string entityId = "";

            using (StreamReader reader = new StreamReader(context.RequestStream))
            {
                entityId = reader.ReadLine() ?? "";
                entityId.Trim();
            }

            if (entityId.Length == 0)
            {
                Helpers.HttpResponseStatus(context, HttpStatusCode.BadRequest, "No entity id specified");
                return;
            }

            LocalEntity localEntity;
            if (!this.mEntityHandler.LocalEntities.TryGetValue(entityId, out localEntity))
            {
                Helpers.HttpResponseStatus(context, HttpStatusCode.BadRequest, "Entity not found");  // TODO: Better error code?
                return;
            }

            // TODO: Prevent multiple registrations of the same entity on a given scene?
            this.mScene.RegisterEntity(localEntity, localEntity.Type);
            Helpers.HttpResponseStatus(context, HttpStatusCode.OK, "Entity '" + entityId + "' joined scene '" + this.mScene.Name + "'");
        }

        protected override bool ElementDelete(MarshalableListenerContext context, string elementId, string path)
        {
            LocalEntity localEntity;
            if (!this.mEntityHandler.LocalEntities.TryGetValue(elementId, out localEntity))
            {
                return false;
            }

            this.mScene.UnregisterEntity(localEntity);
            Helpers.HttpResponseStatus(context, HttpStatusCode.OK, "Entity '" + elementId + "' left scene '" + this.mScene.Name + "'");
            return true;
        }
    }
}
