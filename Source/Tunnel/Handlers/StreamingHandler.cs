﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Diagnostics;

using Badumna;
using Badumna.Facade.Http;
using Badumna.Facade.Http.Change;
using Badumna.Facade.Http.Streaming;
using Badumna.Streaming;

namespace Tunnel.Handlers
{
    class StreamingHandler : CollectionHandler
    {
        private class PullRequest
        {
            public IStreamController Controller;
            public Pipe Pipe;

            public PullRequest(IStreamController controller, Pipe pipe)
            {
                this.Pipe = pipe;
                this.Controller = controller;
            }
        }

        // TODO: !! Thread safety
        // TODO: Refactor
        private int mRequestId;

        private Dictionary<int, Pipe> mRequests = new Dictionary<int, Pipe>();
        private Dictionary<int, PullRequest> mPullRequests = new Dictionary<int, PullRequest>();

        private Dictionary<int, ReceiveStreamEventArgs> mReceiveRequests = new Dictionary<int, ReceiveStreamEventArgs>();
        private Dictionary<int, SendStreamEventArgs> mSendRequests = new Dictionary<int, SendStreamEventArgs>();

        private ChangelistHandler mChangelistHandler;

        /// <summary>
        /// The normal Badumna network facade.
        /// </summary>
        private INetworkFacade networkFacade;

        public StreamingHandler(ChangelistHandler changelistHandler, INetworkFacade networkFacade)
            : base("streaming")
        {
            this.mChangelistHandler = changelistHandler;
            this.networkFacade = networkFacade;
            this.networkFacade.Streaming.AddDefaultHandlers(this.ReceiveFileRequest, this.SendFileRequest);
        }

        private void ReceiveFileRequest(object sender, ReceiveStreamEventArgs e)
        {
            this.mRequestId++;
            this.mReceiveRequests[this.mRequestId] = e;
            this.mChangelistHandler.WithCurrent(c => c.Add(StreamingProcessor.MakeReceiveFile(e, this.mRequestId)));
        }

        private void SendFileRequest(object sender, SendStreamEventArgs e)
        {
            this.mRequestId++;
            this.mSendRequests[this.mRequestId] = e;
            this.mChangelistHandler.WithCurrent(c => c.Add(StreamingProcessor.MakeSendFile(e, this.mRequestId)));
        }

        protected override void CollectionPost(MarshalableListenerContext context)
        {
            try
            {
                using (StreamReader reader = new StreamReader(context.RequestStream))
                {
                    StreamingRequest request = new StreamingRequest(reader);
                    if (request.IsPull)
                    {
                        Pipe pipe = new Pipe();
                        IStreamController controller = this.networkFacade.Streaming.BeginRequestReliableStream(
                            request.StreamTag, request.StreamName, pipe.In, request.PeerId, request.UserName, null, null);

                        this.mRequestId++;
                        this.mPullRequests[this.mRequestId] = new PullRequest(controller, pipe);
                        Helpers.HttpResponseStatus(context, HttpStatusCode.OK, this.mRequestId.ToString());
                    }
                    else
                    {
                        Pipe pipe = new Pipe();
                        this.networkFacade.Streaming.BeginSendReliableStream(
                            request.StreamTag, request.StreamName, pipe.Out, request.PeerId, request.UserName, null, null);


                        // TODO: Make sure the state of the streaming controller is set correctly.
                        // i.e,. Starts in waiting and goes to inprogress or cancelled (??).
                        // In post, block until it goes out of the waiting state and also
                        // make sure the http stream controller sets waiting/inprogress states correctly

                        this.mRequestId++;
                        this.mRequests[this.mRequestId] = pipe;
                        Helpers.HttpResponseStatus(context, HttpStatusCode.OK, this.mRequestId.ToString());
                    }
                }
            }
            catch (Exception)
            {
                Helpers.HttpResponseStatus(context, HttpStatusCode.BadRequest, "Bad request");
                return;
            }
        }

        private static void CopyStreamContents(Stream source, Stream destination)
        {
            byte[] buffer = new byte[32768];
            int bytesRead;

            while ((bytesRead = source.Read(buffer, 0, buffer.Length)) > 0)
            {
                destination.Write(buffer, 0, bytesRead);
            }

        }

        protected override bool ElementGet(MarshalableListenerContext context, string elementId, string path)
        {
            int id;
            if (!int.TryParse(elementId, out id))
            {
                return false;
            }

            ReceiveStreamEventArgs receiveEvent;
            PullRequest pullRequest = null;
            if (!this.mReceiveRequests.TryGetValue(id, out receiveEvent) &&
                !this.mPullRequests.TryGetValue(id, out pullRequest))
            {
                return false;
            }

            if (receiveEvent != null)
            {
                context.ResponseCode = HttpStatusCode.OK;

                using (Stream responseStream = context.ResponseStream)
                {
                    IStreamController controller = receiveEvent.AcceptStream(responseStream, null, null);
                    controller.AsyncWaitHandle.WaitOne();
                }

                // TODO: Remove completed files from mReceiveRequests
                return true;
            }
            else
            {
                Debug.Assert(pullRequest != null);
                context.ResponseCode = HttpStatusCode.OK;

                using (Stream responseStream = context.ResponseStream)
                {
                    StreamingHandler.CopyStreamContents(pullRequest.Pipe.Out, responseStream);
                }
                pullRequest.Controller.AsyncWaitHandle.WaitOne();

                // TODO: Remove completed files from mRequests
                return true;
            }
        }

        protected override bool ElementPost(MarshalableListenerContext context, string elementId, string path)
        {
            int id;
            if (!int.TryParse(elementId, out id))
            {
                return false;
            }

            SendStreamEventArgs sendEvent;
            Pipe pipe = null;
            if (!this.mSendRequests.TryGetValue(id, out sendEvent) &&
                !this.mRequests.TryGetValue(id, out pipe))
            {
                return false;
            }

            if (sendEvent != null)
            {
                IStreamController controller = sendEvent.AcceptStream(context.RequestStream, null, null);
                controller.AsyncWaitHandle.WaitOne();

                // TODO: Remove completed request
                return true;
            }
            else
            {
                // TODO: Have to check that the request was accepted!
                Debug.Assert(pipe != null);
                using (Stream stream = context.RequestStream)
                {
                    StreamingHandler.CopyStreamContents(stream, pipe.In);
                }
                pipe.In.Close();
                // TODO: Remove completed files from mRequests
                Helpers.HttpResponseStatus(context, HttpStatusCode.OK, "OK");
                return true;
            }
        }
    }
}
