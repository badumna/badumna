﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace Tunnel.Handlers
{
    class HandlerMultiplexer : IUriHandler
    {
        private List<IUriHandler> mHandlers;

        public HandlerMultiplexer(List<IUriHandler> handlers)
        {
            this.mHandlers = handlers;
        }

        public bool Handle(string path, MarshalableListenerContext context)
        {
            foreach (IUriHandler handler in this.mHandlers)
            {
                if (handler.Handle(path, context))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
