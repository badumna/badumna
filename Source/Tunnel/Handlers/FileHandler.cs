﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;

namespace Tunnel.Handlers
{
    class FileHandler : IUriHandler
    {
        private Regex mRegex;

        private string mFileName;


        public FileHandler(string uri, string fileName)
        {
            this.mRegex = new Regex(uri);
            this.mFileName = fileName;
        }

        public bool Handle(string path, MarshalableListenerContext context)
        {
            if (!this.mRegex.Match(path).Success)
            {
                return false;
            }

            string content;
            try
            {
                content = File.ReadAllText(this.mFileName);
            }
            catch (Exception)
            {
                Helpers.HttpResponseStatus(context, HttpStatusCode.InternalServerError, "Internal server error");
                return true;
            }
            
            context.ResponseCode = HttpStatusCode.OK;
            using (StreamWriter writer = new StreamWriter(context.ResponseStream))
            {
                writer.Write(content);
            }

            return true;
        }
    }
}
