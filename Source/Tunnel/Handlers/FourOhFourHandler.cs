﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;


namespace Tunnel.Handlers
{
    class FourOhFourHandler : IUriHandler
    {
        public bool Handle(string path, MarshalableListenerContext context)
        {
            Helpers.HttpResponseStatus(context, HttpStatusCode.NotFound, "Not found");

            return true;
        }
    }
}
