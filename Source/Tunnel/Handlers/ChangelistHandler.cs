﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Globalization;
using System.Threading;
using System.Diagnostics;


namespace Tunnel.Handlers
{
    class ChangelistHandler : CollectionHandler
    {
        private const int HistoryLength = 5;
        private static readonly TimeSpan DelayBeforeEmptyResponse = TimeSpan.FromSeconds(2);


        private Dictionary<int, Changelist> mChangelists = new Dictionary<int, Changelist>();
        private int mCurrentId;

        private Changelist mCurrentChange;
        private readonly object mCurrentChangeLock = new object();

        private bool hasShutdown;


        public ChangelistHandler()
            : base("changelists")
        {
            this.ReplaceWithNewChangelist();
        }

        public void Shutdown()
        {
            lock (this.mCurrentChangeLock)
            {
                this.mCurrentChange.Complete();
                this.hasShutdown = true;
            }
        }

        //protected override void CollectionGet(HttpListenerContext context)
        //{
        //    using (StreamWriter writer = new StreamWriter(context.Response.OutputStream))
        //    {
        //        foreach (int key in this.mChangelists.Keys)
        //        {
        //            writer.WriteLine(String.Format(CultureInfo.InvariantCulture, "{0}", key));
        //        }
        //    }
        //}

        /// <summary>
        /// Atomically sets a new changelist, returning the previous changelist.
        /// ElementGet() relies on this only being called by the constructor and ElementGet itself.
        /// </summary>
        /// <returns></returns>
        private Changelist ReplaceWithNewChangelist()
        {
            lock (this.mCurrentChangeLock)
            {
                Changelist oldChangelist = this.mCurrentChange;

                // If we've shut down, then leave the current changelist as the latest.
                if (!this.hasShutdown)
                {
                    this.mCurrentId++;
                    this.mCurrentChange = new Changelist(this.mCurrentId);
                    this.mChangelists[this.mCurrentId] = this.mCurrentChange;

                    while (this.mChangelists.Count > ChangelistHandler.HistoryLength)
                    {
                        this.mChangelists.Remove(this.mChangelists.Keys.Min());
                    }
                }

                // oldChangelist is only null during the call made from the constructor
                if (oldChangelist != null)
                {
                    oldChangelist.Complete();
                }
                return oldChangelist;
            }
        }

        public void WithCurrent(Action<Changelist> action)
        {
            lock (this.mCurrentChangeLock)
            {
                action(this.mCurrentChange);
            }
        }

        /// <summary>
        /// Accessing this as "/latest" will result in a new changelist being created.  Accessing
        /// via a numerical id will not.  Accessing the actual latest by its numerical id may
        /// give different results each time (if new changes have occured since the last access).
        /// </summary>
        protected override bool ElementGet(MarshalableListenerContext context, string elementId, string path)
        {
            Changelist resultChangelist = null;

            if (elementId == "latest")
            {
                Changelist latestChangelist = this.mCurrentChange;
                latestChangelist.WaitUntilNonEmpty(ChangelistHandler.DelayBeforeEmptyResponse);

                resultChangelist = this.ReplaceWithNewChangelist();
            }
            else
            {
                int id;

                if (!int.TryParse(elementId, out id) ||
                    !this.mChangelists.TryGetValue(id, out resultChangelist))
                {
                    return false;
                }
            }

            Debug.Assert(resultChangelist != null);

            context.ResponseCode = HttpStatusCode.OK;
            using (StreamWriter writer = new StreamWriter(context.ResponseStream))
            {
                resultChangelist.SerializeTo(writer);
            }
            return true;
        }
    }
}
