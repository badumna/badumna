﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;
using System.Globalization;
using System.Diagnostics;

using Badumna;
using Badumna.Facade.Http;
using Badumna.Utilities.LitJson;

namespace Tunnel.Handlers
{
    class EntityHandler : CollectionHandler
    {
        private Dictionary<string, LocalEntity> mLocalEntities = new Dictionary<string, LocalEntity>();
        public Dictionary<string, LocalEntity> LocalEntities { get { return this.mLocalEntities; } }

        private Dictionary<string, RemoteEntity> mRemoteEntities = new Dictionary<string, RemoteEntity>();
        public Dictionary<string, RemoteEntity> RemoteEntities { get { return this.mRemoteEntities; } }

        private ChangelistHandler mChangelistHandler;

        /// <summary>
        /// The normal Badumna network facade.
        /// </summary>
        private INetworkFacade networkFacade;

        public EntityHandler(ChangelistHandler changelistHandler, INetworkFacade networkFacade)
            : base("entities")
        {
            this.mChangelistHandler = changelistHandler;
            this.networkFacade = networkFacade;
        }

        protected override void CollectionPost(MarshalableListenerContext context)
        {
            try
            {
                using (StreamReader reader = new StreamReader(context.RequestStream))
                {
                    LocalEntity localEntity = LocalEntity.CreateLocalEntityFrom(reader, this.mChangelistHandler, this.networkFacade);
                    this.LocalEntities[localEntity.Guid.ToString()] = localEntity;
                    Helpers.HttpResponseStatus(context, HttpStatusCode.Created, localEntity.Guid.ToString());
                }
            }
            catch (Exception e)  // TODO: Proper exception
            {
                Helpers.HttpResponseStatus(context, HttpStatusCode.BadRequest, e.Message);
            }
        }

        protected override void CollectionGet(MarshalableListenerContext context)
        {
            context.ResponseCode = HttpStatusCode.OK;
            using (StreamWriter writer = new StreamWriter(context.ResponseStream))
            {
                foreach (string id in this.LocalEntities.Keys)
                {
                    writer.WriteLine(id);
                }
                foreach (string id in this.RemoteEntities.Keys)
                {
                    writer.WriteLine(id);
                }
            }
        }

        protected override bool ElementDelete(MarshalableListenerContext context, string elementId, string path)
        {
            LocalEntity localEntity;
            if (!this.LocalEntities.TryGetValue(elementId, out localEntity))
            {
                if (this.RemoteEntities.ContainsKey(elementId))
                {
                    Helpers.HttpResponseStatus(context, HttpStatusCode.MethodNotAllowed, "Cannot delete remote entities");
                    return true;
                }

                return false;
            }

            this.LocalEntities.Remove(localEntity.Guid.ToString());
            Helpers.HttpResponseStatus(context, HttpStatusCode.OK, "Deleted " + localEntity.Guid.ToString());
            return true;
        }

        protected override bool ElementGet(MarshalableListenerContext context, string elementId, string path)
        {
            LocalEntity localEntity = null;
            RemoteEntity remoteEntity = null;
            if (!this.LocalEntities.TryGetValue(elementId, out localEntity) &&
                !this.RemoteEntities.TryGetValue(elementId, out remoteEntity))
            {
                return false;
            }
            ProxiedEntity entity = (ProxiedEntity)localEntity ?? (ProxiedEntity)remoteEntity;

            context.ResponseCode = HttpStatusCode.OK;
            using (StreamWriter writer = new StreamWriter(context.ResponseStream))
            {
                entity.SerializeTo(writer);
            }
            return true;
        }

        protected override bool ElementPost(MarshalableListenerContext context, string elementId, string path)
        {
            if (path == "custom")
            {
                LocalEntity localEntity;
                RemoteEntity remoteEntity = null;

                if (!this.LocalEntities.TryGetValue(elementId, out localEntity) &&
                    !this.RemoteEntities.TryGetValue(elementId, out remoteEntity))
                {
                    return false;
                }

                CustomMessageRequest request;
                using (StreamReader reader = new StreamReader(context.RequestStream))
                {
                    try
                    {
                        request = new CustomMessageRequest(reader);
                    }
                    catch (Exception)
                    {
                        Helpers.HttpResponseStatus(context, HttpStatusCode.BadRequest, "Missing eventTag or data");
                        return true;
                    }
                }

                if (localEntity != null)
                {
                    this.networkFacade.SendCustomMessageToRemoteCopies(localEntity, request.EventData);
                }
                else
                {
                    Debug.Assert(remoteEntity != null);
                    this.networkFacade.SendCustomMessageToOriginal(remoteEntity, request.EventData);
                }

                Helpers.HttpResponseStatus(context, HttpStatusCode.OK, "Custom message sent");
                return true;
            }
            else
            {
                LocalEntity localEntity;
                if (!this.LocalEntities.TryGetValue(elementId, out localEntity))
                {
                    if (this.RemoteEntities.ContainsKey(elementId))
                    {
                        Helpers.HttpResponseStatus(context, HttpStatusCode.MethodNotAllowed, "Cannot post updates on remote entities");
                        return true;
                    }

                    return false;
                }

                try
                {
                    using (StreamReader reader = new StreamReader(context.RequestStream))
                    {
                        UpdateEntityRequest.UpdateEntityFromJson(JsonMapper.ToObject(reader), localEntity, localEntity.AddChangedPart);
                        Helpers.HttpResponseStatus(context, HttpStatusCode.OK, "Entity updated");
                    }
                }
                catch (Exception e)  // TODO: Proper exception
                {
                    Helpers.HttpResponseStatus(context, HttpStatusCode.BadRequest, e.Message);
                }

                return true;
            }
        }
    }
}
