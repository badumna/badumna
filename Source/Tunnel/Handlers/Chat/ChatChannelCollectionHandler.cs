﻿//-----------------------------------------------------------------------
// <copyright file="ChatChannelCollectionHandler.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2012 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.IO;
using System.Net;
using Badumna.Chat;
using Badumna.DataTypes;
using Badumna.Facade.Http;
using Badumna.Facade.Http.Change;

namespace Tunnel.Handlers.Chat
{
    /// <summary>
    /// Handler for a collection of chat channels.
    /// </summary>
    internal class ChatChannelCollectionHandler : CollectionHandler
    {
        /// <summary>
        /// The chat session
        /// </summary>
        private IInternalChatSession chatSession;
        
        /// <summary>
        /// The changelist handler
        /// </summary>
        private ChangelistHandler changelistHandler;

        /// <summary>
        /// Initializes a new instance of the <see cref="ChatChannelCollectionHandler"/> class.
        /// </summary>
        /// <param name="changelistHandler">The changelist handler.</param>
        /// <param name="chatSession">The chat session.</param>
        public ChatChannelCollectionHandler(ChangelistHandler changelistHandler, IInternalChatSession chatSession)
            : base("channel")
        {
            this.chatSession = chatSession;
            this.changelistHandler = changelistHandler;
        }

        /// <inheritdoc/>
        protected override void CollectionGet(MarshalableListenerContext context)
        {
            using (StreamWriter writer = new StreamWriter(context.ResponseStream))
            {
                this.WithSubHandlerKeys((channelName) => writer.WriteLine(channelName));
            }

            context.ResponseCode = HttpStatusCode.OK;
        }

        /// <inheritdoc/>
        protected override void CollectionPost(MarshalableListenerContext context)
        {
            this.SubscribeToChannel(context);
            context.ResponseCode = HttpStatusCode.OK;
        }

        /// <inheritdoc/>
        protected override bool ElementDelete(MarshalableListenerContext context, string elementId, string path)
        {
            if (path != "")
            {
                return false;
            }

            Action<IUriHandler> removeHandler = delegate(IUriHandler handlerIface)
            {
                ChatChannelHandler handler = (ChatChannelHandler)handlerIface;
                handler.Channel.Unsubscribe();
                this.Remove(handler.CollectionName);
            };

            if (this.WithSubHandler(elementId, removeHandler))
            {
                Helpers.HttpResponseStatus(context, HttpStatusCode.OK, "Left channel " + elementId);
            }
            else
            {
                Helpers.HttpResponseStatus(context, HttpStatusCode.NotFound, "Channel not found");
            }

            return true;
        }

        /// <summary>
        /// Subscribes to a new chat channel.
        /// </summary>
        /// <param name="context">The request context.</param>
        private void SubscribeToChannel(MarshalableListenerContext context)
        {
            ChatChannelId channelId = ChatChannelId.TryParse(new StreamReader(context.RequestStream).ReadToEnd());
            var channel = this.chatSession.SubscribeToChatChannel(channelId, this.HandleMessage, this.HandlePresence);
            this.Add(new ChatChannelHandler(channel));
        }

        /// <summary>
        /// Handles an incoming message.
        /// </summary>
        /// <param name="channel">The channel.</param>
        /// <param name="sender">The sender.</param>
        /// <param name="message">The message.</param>
        private void HandleMessage(IChatChannel channel, BadumnaId sender, string message)
        {
            this.changelistHandler.WithCurrent(c => c.Add(ChatProcessor.MakeChatMessage(channel.Id, sender, message)));
        }

        /// <summary>
        /// Handles an incoming presence change notification.
        /// </summary>
        /// <param name="channel">The channel.</param>
        /// <param name="sender">The sender.</param>
        /// <param name="displayName">The display name.</param>
        /// <param name="status">The status.</param>
        private void HandlePresence(IChatChannel channel, BadumnaId sender, string displayName, ChatStatus status)
        {
            this.changelistHandler.WithCurrent((changelist) => changelist.Add(ChatProcessor.MakePresenceChange(channel.Id, sender, displayName, status)));
        }
    }
}
