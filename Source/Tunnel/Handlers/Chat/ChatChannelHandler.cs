﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;

using Badumna;
using Badumna.Facade.Http;
using Badumna.Facade.Http.Change;
using Badumna.DataTypes;
using Badumna.Chat;


namespace Tunnel.Handlers.Chat
{
    internal class ChatChannelHandler : CollectionHandler
    {
        private IChatChannel mChannel;

        public ChatChannelHandler(IChatChannel channel)
            : base(channel.Id.ToInvariantIDString())
        {
            this.mChannel = channel;
        }

        public IChatChannel Channel
        {
            get { return this.mChannel; }
        }

        protected override void CollectionPost(MarshalableListenerContext context)
        {
            string message;
            using (StreamReader reader = new StreamReader(context.RequestStream))
            {
                message = reader.ReadToEnd();
            }
            if (message == null)
            {
                Helpers.HttpResponseStatus(context, HttpStatusCode.BadRequest, "Message required");
            }

            this.mChannel.SendMessage(message);
            Helpers.HttpResponseStatus(context, HttpStatusCode.OK, "Message sent");
        }
    }
}
