﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

using Badumna;
using Badumna.Chat;
using Badumna.DataTypes;
using Badumna.Facade.Http;
using Badumna.Facade.Http.Change;


namespace Tunnel.Handlers.Chat
{
    internal class ChatInviteHandler : CollectionHandler
    {
        private Dictionary<string, ChatChannelId> mInvites = new Dictionary<string, ChatChannelId>();
        public Dictionary<string, ChatChannelId> Invites { get { return this.mInvites; } }

        private ChangelistHandler mChangelistHandler;
        private IChatSession mChatSession;


        public ChatInviteHandler(ChangelistHandler changelistHandler, IChatSession chatSession)
            : base("invite")
        {
            this.mChangelistHandler = changelistHandler;
            this.mChatSession = chatSession;
            this.mChatSession.OpenPrivateChannels(this.ChatInvitationHandler);
        }

        private void ChatInvitationHandler(ChatChannelId channel, string username)
        {
            this.mInvites[username] = channel;
            this.mChangelistHandler.WithCurrent(c => c.Add(ChatProcessor.MakePrivateChannelInvite(channel, username)));
        }

        protected override void CollectionPost(MarshalableListenerContext context)
        {
            string username;
            using (StreamReader reader = new StreamReader(context.RequestStream))
            {
                username = (reader.ReadLine() ?? "").Trim();
            }

            if (username.Length == 0)
            {
                Helpers.HttpResponseStatus(context, HttpStatusCode.BadRequest, "Must include username in invite request");
                return;
            }

            this.mChatSession.InviteUserToPrivateChannel(username);
            Helpers.HttpResponseStatus(context, HttpStatusCode.OK, "Sent private chat invite to '" + username + "'");
        }

        protected override void CollectionGet(MarshalableListenerContext context)
        {
            context.ResponseCode = HttpStatusCode.OK;
            using (StreamWriter writer = new StreamWriter(context.ResponseStream))
            {
                foreach (string invite in this.mInvites.Keys)
                {
                    writer.WriteLine(invite);
                }
            }
        }
    }
}
