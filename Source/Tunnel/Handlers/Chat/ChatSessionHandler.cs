﻿//-----------------------------------------------------------------------
// <copyright file="ChatSessionHandler.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2012 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;

using Badumna.Chat;

namespace Tunnel.Handlers.Chat
{
    /// <summary>
    /// Chat session handler, delegates to "channel", "presence" and "channel" sub-handlers.
    /// </summary>
    internal class ChatSessionHandler : CollectionHandler
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ChatSessionHandler"/> class.
        /// </summary>
        /// <param name="changelistHandler">The changelist handler.</param>
        /// <param name="chatSession">The chat session.</param>
        public ChatSessionHandler(ChangelistHandler changelistHandler, IInternalChatSession chatSession)
            : base("1") ////Uri.EscapeDataString(chatSession.Character.Name))
        {
            this.Add(new ChatInviteHandler(changelistHandler, chatSession));
            this.Add(new PresenceHandler(chatSession));
            this.Add(new ChatChannelCollectionHandler(changelistHandler, chatSession));
        }
    }
}
