﻿//-----------------------------------------------------------------------
// <copyright file="PresenceHandler.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2012 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.IO;
using System.Net;
using Badumna.Chat;

namespace Tunnel.Handlers.Chat
{
    /// <summary>
    /// Handler for changing a user's presence
    /// </summary>
    internal class PresenceHandler : CollectionHandler
    {
        /// <summary>
        /// The user's current chat status
        /// </summary>
        private ChatStatus chatStatus;

        /// <summary>
        /// The chat session.
        /// </summary>
        private IChatSession chatSession;

        /// <summary>
        /// Initializes a new instance of the <see cref="PresenceHandler"/> class.
        /// </summary>
        /// <param name="chatSession">The chat session.</param>
        public PresenceHandler(IChatSession chatSession) : base("presence")
        {
            this.chatSession = chatSession;
            this.chatStatus = ChatStatus.Offline;
        }

        /// <inheritdoc/>
        protected override void CollectionGet(MarshalableListenerContext context)
        {
            Helpers.HttpResponseStatus(context, HttpStatusCode.OK, ((int)this.chatStatus).ToString());
        }

        /// <inheritdoc/>
        protected override void CollectionPost(MarshalableListenerContext context)
        {
            this.ChangePresence(context);
        }

        /// <summary>
        /// Changes the user's current presence status.
        /// </summary>
        /// <param name="context">The context.</param>
        private void ChangePresence(MarshalableListenerContext context)
        {
            ChatStatus? newStatus = null;
            using (StreamReader reader = new StreamReader(context.RequestStream))
            {
                int newStatusCode;
                if (int.TryParse(reader.ReadLine() ?? "", out newStatusCode))
                {
                    newStatus = (ChatStatus)newStatusCode;  // TODO: Can't detect errors easily using this form of serialization
                }
            }

            if (newStatus == null)
            {
                Helpers.HttpResponseStatus(context, HttpStatusCode.BadRequest, "Must specify new presence code");
                return;
            }

            this.chatStatus = newStatus.Value;
            this.chatSession.ChangePresence(this.chatStatus);
            Helpers.HttpResponseStatus(context, HttpStatusCode.OK, "Presence updated");
        }
    }
}
