﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

using Badumna;
using Badumna.Chat;


namespace Tunnel.Handlers.Chat
{
    internal class ChatHandler : IUriHandler
    {
        private INetworkFacade mNetworkFacade;
        private ChangelistHandler mChangelistHandler;
        private ChatSessionHandler handler;

        public ChatHandler(ChangelistHandler changelistHandler, INetworkFacade networkFacade)
        {
            this.mChangelistHandler = changelistHandler;
            this.mNetworkFacade = networkFacade;
        }

        public bool Handle(string path, MarshalableListenerContext context)
        {
            // We want to instantiate `this.handler` only when we actually need to handle the request,
            // because construction of a chat session may fail if the user is not actually authorized as a character.
            // This is a poor way to achieve that goal, and should be fixed (with actual laziness).

            // TODO: Refactor this code with the code in CollectionHandler.Handle to eliminate code duplication.
            if (path.StartsWith("/"))
            {
                path = path.Substring(1);
            }
            string[] pathParts = path.Split('/');
            for (int i = 0; i < pathParts.Length; i++)
            {
                pathParts[i] = Uri.UnescapeDataString(pathParts[i]);
            }

            if (pathParts.Length == 0 || pathParts[0] != "chat")
            {
                return false;
            }

            if (this.handler == null)
            {
                IInternalChatSession session;
                try
                {
                    session = (IInternalChatSession)this.mNetworkFacade.ChatSession;
                }
                catch (ArgumentException e)
                {
                    Helpers.HttpResponseStatus(context, HttpStatusCode.InternalServerError, e.Message);
                    return true;
                }
                this.handler = new ChatSessionHandler(this.mChangelistHandler, session);
            }

            string subPath = path.Substring(path.IndexOf('/') + 1);
            return this.handler.Handle(subPath, context);
        }
    }
}
