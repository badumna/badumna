﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;

using Badumna;
using Badumna.Arbitration;
using Badumna.Facade.Http;
using Badumna.Facade.Http.Change;

namespace Tunnel.Handlers
{
    class ArbitrationHandler : CollectionHandler
    {
        private ChangelistHandler mChangelistHandler;

        private Dictionary<string, IArbitrator> arbitrationClients = new Dictionary<string, IArbitrator>();  // TODO: Ensure old clients get garbage collected

        /// <summary>
        /// The normal Badumna network facade.
        /// </summary>
        private INetworkFacade networkFacade;

        public ArbitrationHandler(ChangelistHandler changelistHandler, INetworkFacade networkFacade)
            : base("arbitrator")
        {
            this.mChangelistHandler = changelistHandler;
            this.networkFacade = networkFacade;
        }

        private IArbitrator GetArbitrator(string name)
        {
            IArbitrator arbitrator;
            if (!this.arbitrationClients.TryGetValue(name, out arbitrator))
            {
                arbitrator = this.networkFacade.GetArbitrator(name);
                this.arbitrationClients[name] = arbitrator;
            }

            return arbitrator;
        }

        protected override void CollectionPost(MarshalableListenerContext context)
        {
            string name;
            using (StreamReader reader = new StreamReader(context.RequestStream))
            {
                name = reader.ReadToEnd();
            }

            IArbitrator arbitrator = this.GetArbitrator(name);
            arbitrator.Connect(
                connected => this.mChangelistHandler.WithCurrent(c => c.Add(ArbitrationProcessor.MakeConnectedEvent(name, connected))),
                () => this.mChangelistHandler.WithCurrent(c => c.Add(ArbitrationProcessor.MakeConnectionFailedEvent(name))),
                message => this.mChangelistHandler.WithCurrent(c => c.Add(ArbitrationProcessor.MakeServerEvent(name, message))));
            Helpers.HttpResponseStatus(context, HttpStatusCode.OK, "Arbitration connection pending");
        }

        protected override bool ElementPost(MarshalableListenerContext context, string elementId, string path)
        {
            IArbitrator arbitrator;
            if (!this.arbitrationClients.TryGetValue(elementId, out arbitrator))
            {
                return false;
            }

            byte[] message;
            using (StreamReader reader = new StreamReader(context.RequestStream))
            {
                message = ChangeOld.TryParseBytes(reader);
                if (message == null)
                {
                    Helpers.HttpResponseStatus(context, HttpStatusCode.BadRequest, "Could not decode message");
                    return true;
                }
            }

            arbitrator.SendEvent(message);
            Helpers.HttpResponseStatus(context, HttpStatusCode.OK, "Event sent");
            return true;
        }
    }
}
