﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;

namespace Tunnel.Handlers
{
    interface IUriHandler
    {
        bool Handle(string path, MarshalableListenerContext context);
    }
}
