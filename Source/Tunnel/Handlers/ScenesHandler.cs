﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;
using Badumna;


namespace Tunnel.Handlers
{
    class ScenesHandler : CollectionHandler
    {
        private EntityHandler mEntityHandler;
        private ChangelistHandler mChangelistHandler;

        /// <summary>
        /// The normal Badumna network facade.
        /// </summary>
        private INetworkFacade networkFacade;

        public ScenesHandler(EntityHandler entityHandler, ChangelistHandler changelistHandler, INetworkFacade networkFacade)
            : base("scenes")
        {
            this.mEntityHandler = entityHandler;
            this.mChangelistHandler = changelistHandler;
            this.networkFacade = networkFacade;
        }

        protected override void CollectionGet(MarshalableListenerContext context)
        {
            context.ResponseCode = HttpStatusCode.OK;
            using (StreamWriter writer = new StreamWriter(context.ResponseStream))
            {
                this.WithSubHandlerKeys(sceneName => writer.WriteLine(sceneName));
            }
        }

        protected override void CollectionPost(MarshalableListenerContext context)
        {
            string sceneName = "";

            using (StreamReader reader = new StreamReader(context.RequestStream))
            {
                sceneName = reader.ReadLine() ?? "";
                sceneName.Trim();
            }

            if (sceneName.Length == 0)
            {
                Helpers.HttpResponseStatus(context, HttpStatusCode.BadRequest, "No scene name specified");
                return;
            }

            lock (this.mSubHandlersLock)
            {
                if (!this.mSubHandlers.ContainsKey(sceneName))
                {
                    this.mSubHandlers[sceneName] = new SceneHandler(sceneName, this.mEntityHandler, this.mChangelistHandler, this.networkFacade);
                    Helpers.HttpResponseStatus(context, HttpStatusCode.OK, "Joined '" + sceneName + "'");
                }
                else
                {
                    Helpers.HttpResponseStatus(context, HttpStatusCode.OK, "Already in '" + sceneName + "'");
                }
            }
        }

        protected override bool ElementDelete(MarshalableListenerContext context, string elementId, string path)
        {
            SceneHandler sceneHandler = (SceneHandler)this.Remove(elementId);

            if (sceneHandler == null)
            {
                return false;
            }

            sceneHandler.Leave();

            Helpers.HttpResponseStatus(context, HttpStatusCode.OK, "Left scene '" + elementId + "'");
            return true;
        }
    }
}
