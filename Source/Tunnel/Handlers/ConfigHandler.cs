﻿//-----------------------------------------------------------------------
// <copyright file="ConfigHandler.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
using System;
using System.IO;
using System.Net;

using Badumna;
using Badumna.DataTypes;
using Badumna.Utilities.LitJson;

namespace Tunnel.Handlers
{
    /// <summary>
    /// Handles API methods related to configuration.
    /// </summary>
    internal class ConfigHandler : CollectionHandler
    {
        /// <summary>
        /// The normal Badumna network facade.
        /// </summary>
        private INetworkFacade networkFacade;

        /// <summary>
        /// Initializes a new instance of the ConfigHandler class, registering the path "config".
        /// </summary>
        /// <param name="networkFacade">The Badumna network facade.</param>
        public ConfigHandler(INetworkFacade networkFacade)
            : base("config")
        {
            this.networkFacade = networkFacade;
        }

        /// <summary>
        /// Processes element POST requests.  Currently only handles an element named "entityDetails" which
        /// maps to Badumna API RegisterEntityDetails(...).
        /// </summary>
        /// <param name="context">The HTTP context for the request</param>
        /// <param name="elementId">The name of the element to POST to</param>
        /// <param name="path">Not used by this override</param>
        /// <returns>True if the request was handled by this method, or false if the system should look for other handlers.</returns>
        protected override bool ElementPost(MarshalableListenerContext context, string elementId, string path)
        {
            JsonData message;
            using (StreamReader reader = new StreamReader(context.RequestStream))
            {
                try
                {
                    message = JsonMapper.ToObject(reader);
                }
                catch (Exception)
                {
                    Helpers.HttpResponseStatus(context, HttpStatusCode.BadRequest, "Could not decode message");
                    return true;
                }
            }

            switch (elementId)
            {
                case "entityDetails":
                    /* TODO: What happens if we throw an exception from here?  Ideally we should just be able to
                             ignore exceptions here and just have some top level handler report failure if we hit one. */

                    float aoi;
                    Vector3 vel;
                    try
                    {
                        aoi = (float)message["aoi"];
                        vel = Vector3.FromJson(message["vel"]);  // TODO: This should be sent as just the magnitude now
                    }
                    catch (Exception)
                    {
                        Helpers.HttpResponseStatus(context, HttpStatusCode.BadRequest, "Could not decode message");
                        return true;
                    }

                    this.networkFacade.RegisterEntityDetails(aoi, vel.Magnitude);
                    Helpers.HttpResponseStatus(context, HttpStatusCode.OK, "Entity config set");
                    return true;

                default:
                    return false;
            }
        }
    }
}
