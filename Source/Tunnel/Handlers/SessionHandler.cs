﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Threading;

namespace Tunnel.Handlers
{
    class SessionHandler : CollectionHandler, IDisposable
    {
        private Func<Session> makeSession;

        private ManualResetEvent stopReaping = new ManualResetEvent(false);

        public SessionHandler(Func<Session> makeSession)
            : base("sessions")
        {
            this.makeSession = makeSession;

            Thread reaper = new Thread(this.Reaper);
            reaper.IsBackground = true;
            reaper.Start();
        }

        public Session GetSession(string sessionId)
        {
            IUriHandler result;
            lock (this.mSubHandlersLock)
            {
                this.mSubHandlers.TryGetValue(sessionId, out result);
            }
            return (Session)result;
        }

        protected override void CollectionPost(MarshalableListenerContext context)
        {
            try
            {
                Session session = this.makeSession();
                if (session.Login(context))
                {
                    this.Add(session.Id, session);
                    Helpers.HttpResponseStatus(context, HttpStatusCode.OK, session.Id);
                }
                else
                {
                    Helpers.HttpResponseStatus(context, HttpStatusCode.Forbidden, "Login failed");
                }
            }
            catch (ArgumentException e)
            {
                Helpers.HttpResponseStatus(context, HttpStatusCode.BadRequest, e.Message);
            }
        }

        protected override bool ElementDelete(MarshalableListenerContext context, string elementId, string path)
        {
            Session handler = (Session)this.Remove(elementId);
            if (handler == null)
            {
                return false;
            }

            this.DestroySession(handler);
            
            Helpers.HttpResponseStatus(context, HttpStatusCode.OK, "Session closed");
            return true;
        }

        // Destruction is done in a worker thread because it can take a looong time.
        // Also, separate function for this to avoid issues with closing over the
        // loop variable in the foreach statement in ReapSessions.
        private void DestroySession(Session session)
        {
            ThreadPool.QueueUserWorkItem(delegate { Session.Destroy(session); });
        }

        private void ReapSessions()
        {
            Dictionary<string, Session> toReap = new Dictionary<string, Session>();

            lock (this.mSubHandlersLock)
            {
                foreach (KeyValuePair<string, IUriHandler> pair in this.mSubHandlers)
                {
                    Session session = (Session)pair.Value;
                    if (session.HasTimedOut)
                    {
                        toReap[pair.Key] = session;
                    }
                }

                // Remove the sessions while we have the lock to ensure the session doesn't get
                // any new requests while we try to destroy it.
                foreach (string id in toReap.Keys)
                {
                    this.Remove(id);
                }
            }

            foreach (Session session in toReap.Values)
            {
                this.DestroySession(session);
            }
        }

        private void Reaper()
        {
            while (true)
            {
                if (this.stopReaping.WaitOne(
                    TimeSpan.FromMilliseconds(Session.IdleTimeout.TotalMilliseconds / 3.0),
                    false))
                {
                    return;
                }

                this.ReapSessions();
            }
        }

        public void Dispose()
        {
            this.stopReaping.Set();

            foreach (IUriHandler handler in this.mSubHandlers.Values)
            {
                Session session = (Session)handler;
                this.DestroySession(session);
            }
        }
    }
}
