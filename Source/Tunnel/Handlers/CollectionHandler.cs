﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;


namespace Tunnel.Handlers
{
    class CollectionHandler : IUriHandler
    {
        private string mCollectionName;
        protected Dictionary<string, IUriHandler> mSubHandlers = new Dictionary<string, IUriHandler>();
        protected readonly object mSubHandlersLock = new object();

        public string CollectionName
        {
            get { return this.mCollectionName; }
        }

        public CollectionHandler(string collectionName)
        {
            this.mCollectionName = collectionName;
        }

        public void Add(CollectionHandler subCollection)
        {
            this.Add(subCollection.CollectionName, subCollection);
        }

        public void Add(string id, IUriHandler subCollection)
        {
            lock (this.mSubHandlersLock)
            {
                this.mSubHandlers.Add(id, subCollection);
            }
        }

        public IUriHandler Remove(string id)
        {
            IUriHandler handler;

            lock (this.mSubHandlersLock)
            {
                if (this.mSubHandlers.TryGetValue(id, out handler))
                {
                    this.mSubHandlers.Remove(id);
                }
            }

            return handler;
        }

        public void Clear()
        {
            lock (this.mSubHandlersLock)
            {
                this.mSubHandlers.Clear();
            }
        }

        /// <summary>
        /// Note: This holds the sub handlers lock for the duration!
        /// </summary>
        /// <param name="action"></param>
        public void WithSubHandlerKeys(Action<string> action)
        {
            lock (this.mSubHandlersLock)
            {
                foreach (string id in this.mSubHandlers.Keys)
                {
                    action(id);
                }
            }
        }

        /// <summary>
        /// Perform an action on the sub-handler with the given key, if present.
        /// </summary>
        /// <param name="key">The desired sub-handler's key</param>
        /// <param name="action">The action to perform.</param>
        /// <returns>True if the subhandler was found (and action performed), false otherwise.</returns>
        protected bool WithSubHandler(string key, Action<IUriHandler> action)
        {
            lock (this.mSubHandlersLock)
            {
                IUriHandler handler;
                if (this.mSubHandlers.TryGetValue(key, out handler))
                {
                    action(handler);
                    return true;
                }
            }
            return false;
        }

        public bool Handle(string path, MarshalableListenerContext context)
        {
            if (path.StartsWith("/"))
            {
                path = path.Substring(1);
            }
            string[] pathParts = path.Split('/');
            for (int i = 0; i < pathParts.Length; i++)
            {
                pathParts[i] = Uri.UnescapeDataString(pathParts[i]);
            }

            if (pathParts.Length == 0 || pathParts[0] != this.mCollectionName)
            {
                return false;
            }

            string subPath = "";
            if (pathParts.Length > 1)
            {
                subPath = path.Substring(path.IndexOf('/') + 1);

                // Local copy in case the handler needs to change the set of handlers and to limit
                // the length of time we hold the lock.
                List<IUriHandler> handlers;
                lock (this.mSubHandlersLock)
                {
                    handlers = new List<IUriHandler>(this.mSubHandlers.Values);
                }

                foreach (IUriHandler uriHandler in handlers)
                {
                    if (uriHandler.Handle(subPath, context))
                    {
                        return true;
                    }
                }
            }


            string elementId = "";
            if (pathParts.Length > 1)
            {
                elementId = pathParts[1];
            }

            if (elementId.Length == 0)
            {
                switch (context.RequestMethod)
                {
                    case "GET":
                        this.CollectionGet(context);
                        return true;

                    case "POST":
                        this.CollectionPost(context);
                        return true;

                    default:
                        return false;
                }
            }

            string subPath2 = "";
            if (pathParts.Length > 2)
            {
                subPath2 = subPath.Substring(subPath.IndexOf('/') + 1);
            }

            switch (context.RequestMethod)
            {
                case "GET":
                    return this.ElementGet(context, elementId, subPath2);

                case "POST":
                    return this.ElementPost(context, elementId, subPath2);

                case "DELETE":
                    return this.ElementDelete(context, elementId, subPath2);

                default:
                    return false;
            }
        }

        /// <summary>
        /// Processes GET requests for the collection.
        /// </summary>
        /// <param name="context">The HTTP request</param>
        protected virtual void CollectionGet(MarshalableListenerContext context)
        {
            Helpers.HttpResponseStatus(context, HttpStatusCode.MethodNotAllowed, "Not allowed");
        }

        /// <summary>
        /// Processes POST requests for the collection.
        /// </summary>
        /// <param name="context">The HTTP request</param>
        protected virtual void CollectionPost(MarshalableListenerContext context)
        {
            Helpers.HttpResponseStatus(context, HttpStatusCode.MethodNotAllowed, "Not allowed");
        }

        /// <summary>
        /// Processes GET requests for elements of the collection.
        /// </summary>
        /// <param name="context">The HTTP request</param>
        /// <param name="elementId">The name of the element</param>
        /// <param name="path">The original (possibly escaped) request path underneath the element, or the empty string if the request is on the element itself.
        /// The request URI is broken up as "collectionName/elementId/path" where path captures the entire remaining URI.</param>
        /// <returns>True if the request was handled by this method, or false if the system should look for other handlers.</returns>
        protected virtual bool ElementGet(MarshalableListenerContext context, string elementId, string path)
        {
            Helpers.HttpResponseStatus(context, HttpStatusCode.MethodNotAllowed, "Not allowed");
            return true;
        }

        /// <summary>
        /// Processes POST requests for elements of the collection.
        /// </summary>
        /// <param name="context">The HTTP request</param>
        /// <param name="elementId">The name of the element</param>
        /// <param name="path">The original (possibly escaped) request path underneath the element, or the empty string if the request is on the element itself.
        /// The request URI is broken up as "collectionName/elementId/path" where path captures the entire remaining URI.</param>
        /// <returns>True if the request was handled by this method, or false if the system should look for other handlers.</returns>
        protected virtual bool ElementPost(MarshalableListenerContext context, string elementId, string path)
        {
            Helpers.HttpResponseStatus(context, HttpStatusCode.MethodNotAllowed, "Not allowed");
            return true;
        }

        /// <summary>
        /// Processes DELETE requests for elements of the collection.
        /// </summary>
        /// <param name="context">The HTTP request</param>
        /// <param name="elementId">The name of the element</param>
        /// <param name="path">The original (possibly escaped) request path underneath the element, or the empty string if the request is on the element itself.
        /// The request URI is broken up as "collectionName/elementId/path" where path captures the entire remaining URI.</param>
        /// <returns>True if the request was handled by this method, or false if the system should look for other handlers.</returns>
        protected virtual bool ElementDelete(MarshalableListenerContext context, string elementId, string path)
        {
            Helpers.HttpResponseStatus(context, HttpStatusCode.MethodNotAllowed, "Not allowed");
            return true;
        }
    }
}
