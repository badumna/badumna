﻿//-----------------------------------------------------------------------
// <copyright file="TunnelServer.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
// #define LOG

/*
 * Login    -- proxied peer should log into dei directly.  how will the proxying peer impersonate the originator?
 * Logout
 * 
 * Create entity  POST /entities
 * Delete entity DELETE /entities/entity-id
 * 
 * JoinScene    POST /scenes [scene name]           -- also needs to support callbacks for create / remote remote entity
 * Scene.Leave  DELETE /scenes/scenename
 * 
 * RegisterEntity  POST /scenes/scenename [entity-id]
 * UnregisterEntity  DELETE /scenes/scenename/entity-id
 * 
 * FlagForUpdate            )
 * GenerateCompleteUpdate   >  PUT(?) /entities/entity-id
 * GeneratePartialUpdate    )
 * 
 * ProcessNetworkState   )
 * ApplyCompleteUpdate   >  GET /entities/entity-id      -- too inefficient probably;  how to get new entities?  just GET/entities
 * ApplyPartialUpdate    )
 * 
 * 
 * /entities/local
 * /entities/remote
 * /entities/remote/changed
 * 
 * 
 * 
 */

using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading;
using System.Xml;

using Badumna;
using Tunnel.Handlers;

namespace Tunnel
{
    /// <summary>
    /// TunnelServer listens for incoming HTTP requests and dispatches them to a request handler.
    /// The HttpListenerContext from the request is wrapped inside a MarshalableListenerContext
    /// so that requests can be handled in different app domains.
    /// </summary>
    internal class TunnelServer
    {
        /// <summary>
        /// Maximum number of requests that can be processed simultaneously.  This needs to allow for changelist
        /// requests that may be outstanding for a while.
        /// </summary>
        private const int ConcurrentRequestLimit = 200; // TODO: Figure out what this should actually be based on a limit of total sessions

        /// <summary>
        /// Minimum number of Sessions to keep ready in SessionPool at all times.
        /// </summary>
        private const int RequiredReadySessions = 5;

        /// <summary>
        /// The error code associated with "The handle is invalid" exception.
        /// </summary>
        private const int HttpListenerInvalidHandleError = 6;

        /// <summary>
        /// Period between checking to see if we've been signalled to stop.
        /// </summary>
        private static readonly TimeSpan StopCheckPeriod = TimeSpan.FromMilliseconds(500);

#if LOG
        /// <summary>
        /// Syncronizes access to the console so that colours are set correctly.
        /// </summary>
        private readonly object logLock = new object();
#endif

        /// <summary>
        /// The HttpListener.
        /// </summary>
        private HttpListener listener;

        /// <summary>
        /// Flag to signal the worker thread to exit.
        /// </summary>
        private volatile bool shouldStop;

        /// <summary>
        /// True if the worker thread has been started and has not been requested to stop.
        /// </summary>
        private bool started;

        /// <summary>
        /// Signalled when the worker thread actually stops.
        /// </summary>
        private ManualResetEvent stopped = new ManualResetEvent(false);

        /// <summary>
        /// Semaphore to limit the maximum number of concurrent requests.
        /// </summary>
        private Semaphore currentRequests = new Semaphore(TunnelServer.ConcurrentRequestLimit, TunnelServer.ConcurrentRequestLimit);

        /// <summary>
        /// The top level handler that requests are dispatched to.
        /// </summary>
        private HandlerMultiplexer handler;

        /// <summary>
        /// The session handler, responsible for creating and destroying sesssions.
        /// </summary>
        private SessionHandler sessionHandler;

        /// <summary>
        /// A pool of Sessions ready for use.
        /// </summary>
        /// <remarks>
        /// Each Session runs in its own AppDomain which can take some time to set up.  The SessionPool keeps
        /// a number of Sessions ready for immediate use to speed up new connections.
        /// </remarks>
        private SessionPool sessionPool;
       
        /// <summary>
        /// Initializes a new instance of the TunnelServer class.
        /// </summary>
        /// <param name="prefix">The HTTP Server API UrlPrefix to listen on</param>
        /// <param name="options">The Badumna options to use for the tunnel sessions.</param>
        public TunnelServer(string prefix, Options options)
        {
            StringBuilder optionsStringBuilder = new StringBuilder();
            using (XmlWriter xmlWriter = XmlWriter.Create(optionsStringBuilder))
            {
                options.Save(xmlWriter);
            }

            string optionsString = optionsStringBuilder.ToString();

            this.sessionPool = new SessionPool(TunnelServer.RequiredReadySessions, () => Session.Create(optionsString));
            this.Initialize(prefix, this.sessionPool.GetSession);
        }

        /// <summary>
        /// Initializes a new instance of the TunnelServer class with a specific INetworkFacade
        /// to use.  Only one Session should be created at a time because multiple sessions will
        /// share the same network facade.  Only to be used for testing!
        /// </summary>
        /// <param name="prefix">The HTTP Server API UrlPrefix to listen on</param>
        /// <param name="networkFacade">The network facade to use.</param>
        public TunnelServer(string prefix, INetworkFacade networkFacade)
        {
            this.Initialize(prefix, () => Session.CreateInCurrentDomain(networkFacade));
        }

        /// <summary>
        /// Gets the session handler.  Used to display diagnostic information.
        /// </summary>
        internal SessionHandler SessionHandler
        {
            get { return this.sessionHandler; }
        }

        /// <summary>
        /// Start listening for requests.
        /// </summary>
        public void Start()
        {
            if (this.listener == null)
            {
                throw new InvalidOperationException();
            }

            if (this.started)
            {
                return;
            }

            new Thread(this.Loop).Start();
            this.started = true;
        }

        /// <summary>
        /// Stop listening for requests.
        /// </summary>
        public void Stop()
        {
            if (!this.started)
            {
                return;
            }

            this.shouldStop = true;
            this.listener.Close();

            bool succeeded = this.stopped.WaitOne(TimeSpan.FromSeconds(2), false);  // TODO: Config timeout? Also, do something if it fails.

            this.listener = null;

            this.sessionHandler.Dispose();

            if (this.sessionPool != null)
            {
                this.sessionPool.Shutdown();
                this.sessionPool = null;
            }

            this.started = false;
        }

        /// <summary>
        /// Creates the HTTP listener and registers URI handlers.
        /// </summary>
        /// <param name="prefix">The HTTP Server API UrlPrefix to listen on</param>
        /// <param name="makeSession">A degelate that creates a Session in a new application domain</param>
        private void Initialize(string prefix, Func<Session> makeSession)
        {
            if (!HttpListener.IsSupported)
            {
                throw new NotSupportedException("HttpListener not supported");
            }

            this.listener = new HttpListener();

            try
            {
                this.listener.Prefixes.Add(prefix);
            }
            catch (ArgumentException e)
            {
                string[] messageParts = e.Message.Split('\n');
                if (messageParts.Length > 0)
                {
                    throw new TunnelConfiguationException("Invalid listen prefix: " + messageParts[0]);
                }
                else
                {
                    throw new TunnelConfiguationException("Invalid listen prefix");
                }
            }

            this.sessionHandler = new SessionHandler(makeSession);

            this.handler = new HandlerMultiplexer(new List<IUriHandler>()
            {
                this.sessionHandler,
#if DEBUG
                new FileHandler("/files/test.html", "TestPage/test.html"),
                new FileHandler("/files/prototype.js", "TestPage/prototype.js"),
#endif
                new FourOhFourHandler()
            });

            try
            {
                this.listener.Start();
            }
            catch (HttpListenerException e)
            {
                if (e.ErrorCode == 5)
                {
                    throw new TunnelConfiguationException(
                        "Access denied when starting listener.  A reservation for prefix \"" + prefix + "\" with permission for the current user needs to be added using netsh.exe http add urlacl, see the manual for details.",
                        e);
                }
                else if (e.ErrorCode == 32 || e.ErrorCode == 183)
                {
                    throw new TunnelConfiguationException("Port already in use when starting listener.", e);
                }

                throw new TunnelConfiguationException("Listener failed to start for an unrecognised reason, see inner exception for details", e);
            }
        }

        /// <summary>
        /// Main request processing loop.
        /// </summary>
        private void Loop()
        {
            try
            {
                while (!this.shouldStop)
                {
                    /* TODO: Some way of killing run-away requests?  At the moment we can probably get DOSed
                             by someone just keeping ConcurrentRequestLimit number of requests open all the time. */

                    if (!this.currentRequests.WaitOne(TunnelServer.StopCheckPeriod, false))
                    {
                        continue;  // Check stop condition
                    }

                    HttpListenerContext context = this.listener.GetContext();

                    bool succeeded = false;
                    try
                    {
                        succeeded = ThreadPool.QueueUserWorkItem(this.HandleRequest, context);
                    }
                    catch (NotSupportedException)
                    {
                    }

                    if (!succeeded)
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        context.Response.StatusDescription = "Queue work item failed";
                        this.currentRequests.Release();
                    }
                }
            }
            catch (Exception e)
            {
                if (e is HttpListenerException && ((HttpListenerException)e).ErrorCode == 995)
                {
                    // normal exit
                }
                else
                {
                    Console.WriteLine("{0}: {1}", e.GetType().Name, e.Message);
                }
            }

            this.stopped.Set();
        }

        /// <summary>
        /// Handle a single request.  Intended to be invoked from a worker thread.
        /// </summary>
        /// <param name="state">The HttpListenerContext for the request</param>
        private void HandleRequest(object state)
        {
            // Catch all exceptions because a crashing worker thread will bring down the domain
            try
            {
                HttpListenerContext context = (HttpListenerContext)state;

                try
                {
                    this.handler.Handle(context.Request.Url.AbsolutePath, new MarshalableListenerContext(context));
                }
                catch (Exception e)
                {
                    bool suppressMessage = false;

                    HttpListenerException httpListenerException = e as HttpListenerException;
                    if (httpListenerException != null && httpListenerException.ErrorCode == TunnelServer.HttpListenerInvalidHandleError)
                    {
                        // These exceptions occur if the listener is torn down while a request is pending.  Usually this
                        // happens during shutdown when there's an outstanding changelist request.  May be worth investigating
                        // in more depth, but probably not.
                        suppressMessage = true;
                    }

                    if (!suppressMessage)
                    {
                        this.DumpException("HandleRequest / Invoke handler", e);
                    }

                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                }

#if LOG && !ANDROID
                lock (this.logLock)
                {
                    bool success = 
                        context.Response.StatusCode / 100 == 2;
                    if (!success)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                    }

                    Console.WriteLine("{0,-6} {1} {2}", context.Request.HttpMethod, context.Response.StatusCode, context.Request.Url);
                    if (!success)
                    {
                        Console.ForegroundColor = ConsoleColor.Gray;
                    }
                }
#endif

                context.Response.Close();
            }
            catch (Exception e)
            {
                this.DumpException("HandleRequest", e);
            }
            finally
            {
                this.currentRequests.Release();
            }
        }

        /// <summary>
        /// Write an exception to the console for diagnostic purposes.
        /// </summary>
        /// <param name="location">Description of the call site</param>
        /// <param name="e">Exception to dump</param>
        private void DumpException(string location, Exception e)
        {
            if (e == null)
            {
                return;
            }

#if LOG && !ANDROID
            Console.ForegroundColor = ConsoleColor.Red;
#endif

            Console.WriteLine("*** Caught exception in {0}:", location);
            Console.Write("    ");
            while (e.InnerException != null)
            {
                Console.Write("{0} -> ", e.GetType().Name);
                e = e.InnerException;
            }

            Console.WriteLine("{0}", e.GetType().Name);
            Console.WriteLine("    {0}", e.Message);
            Console.WriteLine("    {0}", e.StackTrace);
            Console.WriteLine("***************");

#if LOG && !ANDROID
            Console.ForegroundColor = ConsoleColor.Gray;
#endif
        }
    }
}
