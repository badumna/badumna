﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using Badumna;
using Badumna.DataTypes;
using Badumna.Facade.Http;
using Badumna.Utilities;
using Badumna.SpatialEntities;
using Tunnel.Handlers;
using Badumna.Facade.Http.Change;


namespace Tunnel
{
    class RemoteEntity : ProxiedEntity, ISpatialReplica
    {
        private ChangelistHandler mChangelistHander;

        private INetworkFacade networkFacade;

        private List<string> mCurrentScenes = new List<string>();
        public List<string> CurrentScenes { get { return this.mCurrentScenes; } }


        public RemoteEntity(uint entityType, BadumnaId guid, ChangelistHandler changelistHandler, INetworkFacade networkFacade)
        {
            this.Type = entityType;
            this.Guid = guid;
            this.mChangelistHander = changelistHandler;
            this.networkFacade = networkFacade;
        }

        public void Deserialize(BooleanArray includedParts, Stream stream, int estimatedMillisecondsSinceDeparture)
        {
            this.networkFacade.SnapToDestination(this);
            byte[] update;
            RemoteEntity.ReadToEnd(stream, out update);
            this.mChangelistHander.WithCurrent(c =>
                c.Add(SceneProcessor.MakeEntityUpdate(this.Guid, this.Position, this.Velocity, this.Radius, this.AreaOfInterestRadius, includedParts, update)));
        }

        // TODO: This assumes the stream is seekable, can read its contents in one go, and isn't more
        //       than 2^31-1 bytes long.  These assumptions will probably always be satisfied but 
        //       might be a good idea to make it more robust anyway.
        public static void ReadToEnd(Stream stream, out byte[] buffer)
        {
            int remaining = checked((int)(stream.Length - stream.Position));
            buffer = new byte[remaining];
            stream.Read(buffer, 0, remaining);
        }

        public override void HandleEvent(Stream stream)
        {
            byte[] buffer;
            RemoteEntity.ReadToEnd(stream, out buffer);
            this.mChangelistHander.WithCurrent(c => c.Add(SceneProcessor.MakeCustomMessage(this.Guid, true, buffer)));
        }
    }
}
