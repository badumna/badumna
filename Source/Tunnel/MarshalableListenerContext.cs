﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;

namespace Tunnel
{
    class MarshalableListenerContext : MarshalByRefObject
    {
        private HttpListenerContext mContext;

        public string RequestMethod
        {
            get { return this.mContext.Request.HttpMethod; }
        }

        public Stream RequestStream
        {
            get { return this.mContext.Request.InputStream; }
        }

        /// <summary>
        /// This must be set before accessing ResponseStream, otherwise the response
        /// will send 200 (OK) instead of whatever code is set here.
        /// </summary>
        public HttpStatusCode ResponseCode
        {
            get { return (HttpStatusCode)this.mContext.Response.StatusCode; }
            set
            {
                if (this.mResponseStreamUsed)
                {
                    throw new InvalidOperationException("Must be set before accessing ResponseStream");
                }

                this.mContext.Response.StatusCode = (int)value;
            }
        }

        /// <summary>
        /// This must be set before accessing ResponseStream, otherwise the response
        /// will send 200 (OK) instead of whatever code is set here.
        /// </summary>
        public string ResponseDescription
        {
            get { return this.mContext.Response.StatusDescription; }
            set
            {
                if (this.mResponseStreamUsed)
                {
                    throw new InvalidOperationException("Must be set before accessing ResponseStream");
                }

                this.mContext.Response.StatusDescription = value;
            }
        }

        private bool mResponseStreamUsed;
        public Stream ResponseStream
        {
            get
            {
                this.mResponseStreamUsed = true;
                return this.mContext.Response.OutputStream;
            }
        }

        public MarshalableListenerContext(HttpListenerContext context)
        {
            this.mContext = context;
        }


        public override object InitializeLifetimeService()
        {
            return null;  // Live forever.  TODO: This probably prevents garbage collection altogether?
        }
    }
}
