﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Tunnel
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using GermHarness;
    using Mono.Options;

    /// <summary>
    /// Server providing http tunnelling for peers that can't use UDP.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// The peer harness for hosting the process and communicating with the Control Center.
        /// </summary>
        private static ProcessHarness harness = new ProcessHarness();

        /// <summary>
        /// Option set for parsing command line arguments.
        /// </summary>
        private static OptionSet optionSet = new OptionSet()
            {
                { "h|help", "show this message and exit.", v =>
                    {
                        if (v != null)
                        {
                            Program.WriteUsageAndExit();
                        }
                    }
                }
            };

        /// <summary>
        /// The entry point to the program.
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        public static void Main(string[] args)
        {
            // Parse program-specific command line arguments.
            try
            {
                BadumnaCommandLineOptions badumnaCommandLineOptions = new BadumnaCommandLineOptions(optionSet);

                List<string> extras = optionSet.Parse(args);
                string[] harnessArgs = extras.ToArray();

                Badumna.Options badumnaOptions = badumnaCommandLineOptions.ApplyOptions(new Badumna.Options(), true);
                badumnaCommandLineOptions.SaveIfRequired(badumnaOptions);

                if (Program.harness.Initialize(ref harnessArgs, new TunnelProcess(badumnaOptions)))
                {
                    if (harnessArgs.Length > 0)
                    {
                        Program.HandleUnknownArguments(harnessArgs);
                    }
                    else
                    {
                        Program.harness.Start();
                        Console.WriteLine("Tunnel has stopped.");
                    }
                }
                else
                {
                    Console.WriteLine("Tunnel: Could not initialize harness.");
                }
            }
            catch (OptionException ex)
            {
                Console.WriteLine("Tunnel: {0}", ex.Message);
                Console.WriteLine("Try `Tunnel.exe --help' for more information.");
            }
        }

        /// <summary>
        /// Write error message listing unknown arguments.
        /// </summary>
        /// <param name="arguments">Unknown arguments.</param>
        private static void HandleUnknownArguments(string[] arguments)
        {
            Console.Write("Tunnel: Unknown argument(s):");
            foreach (string arg in arguments)
            {
                Console.Write(" " + arg);
            }

            Console.WriteLine(".");
            Console.WriteLine("Try `Tunnel.exe --help' for more information.");
        }

        /// <summary>
        /// Write usage instructions and exit.
        /// </summary>
        private static void WriteUsageAndExit()
        {
            Console.WriteLine("Usage: Tunnel.exe [Options] [Prefix]");
            Console.WriteLine("Start a Tunnel server on the local machine.");
            Console.WriteLine(string.Empty);
            Console.WriteLine("Options:");
            Program.harness.WriteOptionDescriptions(Console.Out);
            Program.optionSet.WriteOptionDescriptions(Console.Out);
            Environment.Exit(1);
        }
    }
}