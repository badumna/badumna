﻿//-----------------------------------------------------------------------
// <copyright file="TunnelConfigurationException.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
using System;
using System.Runtime.Serialization;

namespace Tunnel
{
    /// <summary>
    /// An exception that is thrown if the tunnel server cannot start due to a configuration error.
    /// </summary>
    [Serializable]
    public sealed class TunnelConfiguationException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the TunnelConfiguationException class.
        /// </summary>
        internal TunnelConfiguationException()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the TunnelConfiguationException class.
        /// </summary>
        /// <param name="message">A message describing the exception</param>
        internal TunnelConfiguationException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the TunnelConfiguationException class.
        /// </summary>
        /// <param name="message">A message describing the exception</param>
        /// <param name="innerException">The exception that triggered this exception</param>
        internal TunnelConfiguationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the TunnelConfiguationException class from
        /// serialized data.
        /// </summary>
        /// <param name="info">Streaming context</param>
        /// <param name="context">Serialization info</param>
        private TunnelConfiguationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
