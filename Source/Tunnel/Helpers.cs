﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;

namespace Tunnel
{
    static class Helpers
    {
        private static Random RandomSource = new Random();


        public static float? ReadFloat(TextReader reader)
        {
            float f;
            if (float.TryParse(reader.ReadLine(), out f))
            {
                return f;
            }
            return null;
        }

        public static void HttpResponseStatus(MarshalableListenerContext context, HttpStatusCode status, string message)
        {
            context.ResponseCode = status;

            // Use the default description, because message may contain arbitrary characters
            // (but description is not allowed to contain ctrl characters such as new lines).
            //context.ResponseDescription = message;  

            using (StreamWriter writer = new StreamWriter(context.ResponseStream))
            {
                writer.WriteLine("{0}", message);
            }
        }

        public static string GenerateSessionId(ICollection<string> existing)
        {
            string idChars = "abcdefghijklmnopqrstuvwxyz0123456789";
            int idLength = 20;

            while (true)
            {
                StringBuilder id = new StringBuilder();

                for (int i = 0; i < idLength; i++)
                {
                    id.Append(idChars[RandomSource.Next(idChars.Length)]);
                }

                string result = id.ToString();
                if (!existing.Contains(result))
                {
                    return result;
                }
            }
        }
    }
}
