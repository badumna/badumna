﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;

namespace Tunnel
{
    class SessionPool
    {
        private int mRequiredReadySessionCount;
        private Queue<Session> mReadySessions = new Queue<Session>();
        private readonly object mAcquireLock = new object();
        private ManualResetEvent mNotFull = new ManualResetEvent(true);
        private Func<Session> makeSession;

        private ManualResetEvent mShouldStop = new ManualResetEvent(false);
        private ManualResetEvent mHasStopped = new ManualResetEvent(false);

        public SessionPool(int requiredReadySessionCount, Func<Session> makeSession)
        {
            this.makeSession = makeSession;

            if (requiredReadySessionCount < 1)
            {
                throw new ArgumentOutOfRangeException("requiredReadySessionCount");
            }

            this.mRequiredReadySessionCount = requiredReadySessionCount;

            this.Start();
        }

        private void Start()
        {
            this.mShouldStop.Reset();

            Thread factoryThread = new Thread(this.SessionFactory);
            factoryThread.IsBackground = true;
            factoryThread.Start();
        }

        public void Shutdown()
        {
            this.mHasStopped.Reset();
            this.mShouldStop.Set();
            this.mHasStopped.WaitOne();

            lock (this.mAcquireLock)
            {
                while (this.mReadySessions.Count > 0)
                {
                    Session session = this.mReadySessions.Dequeue();
                    Session.Destroy(session);
                }
            }
        }

        private void SessionFactory()
        {
            while (true)
            {
                if (WaitHandle.WaitAny(new WaitHandle[] { this.mShouldStop, this.mNotFull }) == 0)  // WaitAny returns the smallest array index of all the signalled objects
                {
                    break;
                }

                Debug.Assert(this.mReadySessions.Count < this.mRequiredReadySessionCount);

                Session newSession = this.makeSession();
                lock (this.mAcquireLock)
                {
                    this.mReadySessions.Enqueue(newSession);
                    if (this.mReadySessions.Count >= this.mRequiredReadySessionCount)
                    {
                        this.mNotFull.Reset();
                    }
                }
            }

            this.mHasStopped.Set();
        }

        
        public Session GetSession()
        {
            lock (this.mAcquireLock)
            {
                if (this.mReadySessions.Count > 0)
                {
                    Session session = this.mReadySessions.Dequeue();

                    if (this.mReadySessions.Count < this.mRequiredReadySessionCount)
                    {
                        this.mNotFull.Set();
                    }

                    return session;
                }
            }

            return this.makeSession();  // None were available, may as well just create one directly
        }
    }
}
