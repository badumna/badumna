﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Reflection;
using System.IO;
using System.Threading;
using System.Xml;

using Tunnel.Handlers;
using Badumna;
using Badumna.Facade.Http;
using Badumna.Security;
using Tunnel.Handlers.Chat;


namespace Tunnel
{
    /// <summary>
    /// Represents a Badumna session.  Each session is created in its own AppDomain (so all public members
    /// on this class must have serializable parameters / return values).
    /// </summary>
    class Session : MarshalByRefObject, IUriHandler
    {
        // We expect to have a request for the changelist quite frequently (see ChangelistHandler.DelayBeforeEmptyResponse).
        // Don't want to set this too low because transient connection failures would then tear down the
        // session.  Don't want it too high either because it results in stale avatars in the world.
        public static readonly TimeSpan IdleTimeout = TimeSpan.FromMinutes(2.0);


        private static List<string> mUsedSessionIds = new List<string>();

        /// <summary>
        /// Creates a new <see cref="Session"/>.
        /// </summary>
        /// <param name="options">The Badumna options as a string containing XML (so they can easily be passed over
        /// the app domain boundary).</param>
        /// <returns>The new session</returns>
        public static Session Create(string options)
        {
            string id;
            lock (Session.mUsedSessionIds)
            {
                id = Helpers.GenerateSessionId(Session.mUsedSessionIds);
                Session.mUsedSessionIds.Add(id);
            }

            AppDomain domain = AppDomain.CreateDomain("session-" + id);
            return (Session)domain.CreateInstanceAndUnwrap(Assembly.GetExecutingAssembly().FullName, typeof(Session).FullName,
                false, BindingFlags.Instance | BindingFlags.NonPublic, null, new object[] { id, options }, null, null, null);
        }

        /// <summary>
        /// Creates a session in the current domain.  For testing purposes only!
        /// </summary>
        /// <param name="networkFacade">The network facade to use.</param>
        /// <returns>The newly created session.</returns>
        public static Session CreateInCurrentDomain(INetworkFacade networkFacade)
        {
            return new Session("test-session", networkFacade);
        }

        public static void Destroy(Session session)
        {
            lock (Session.mUsedSessionIds)
            {
                Session.mUsedSessionIds.Remove(session.Id);
            }

            session.Shutdown();

            if (session.AppDomain != AppDomain.CurrentDomain)
            {
                Thread.Sleep(200);  // Wait a small amount of time for threads to complete.  TODO: Do this in a more robust way.
                AppDomain.Unload(session.AppDomain);
            }
        }


        private HandlerMultiplexer mHandler;

        private string mId;
        public string Id { get { return this.mId; } }

        private IIdentityProvider mIdentityProvider;

        private volatile bool mStopProcessing;
        private ManualResetEvent mHasStopped;

        private AppDomain mAppDomain = AppDomain.CurrentDomain;
        public AppDomain AppDomain { get { return this.mAppDomain; } }

        private DateTime mLastActivity = DateTime.Now;

        public bool HasTimedOut { get { return this.mLastActivity + Session.IdleTimeout < DateTime.Now; } }

        private ChangelistHandler changelistHandler;

        /// <summary>
        /// The INetworkFacade for this session.
        /// </summary>
        private INetworkFacade networkFacade;

        /// <summary>
        /// The Badumna options as a string containing XML.
        /// </summary>
        private string optionsString;

        /// <summary>
        /// Initializes a new instance of the <see cref="Session"/> class.
        /// </summary>
        /// <param name="id">The session id.</param>
        /// <param name="options">The Badumna options as a string containing XML.</param>
        private Session(string id, string options)
        {
            this.mId = id;
            this.optionsString = options;
        }

        /// <summary>
        /// Construct a Session using the specified network facade.  For testing purposes only!
        /// </summary>
        /// <param name="id">The session id.</param>
        /// <param name="networkFacade">The test network facade.</param>
        private Session(string id, INetworkFacade networkFacade)
        {
            this.mId = id;
            this.networkFacade = networkFacade;
        }

        public bool Login(MarshalableListenerContext context)
        {
            CreateSessionRequest request;
            try
            {
                using (StreamReader reader = new StreamReader(context.RequestStream))
                {
                    request = new CreateSessionRequest(reader);
                }
            }
            catch (Exception e)
            {
                throw new ArgumentException("CreateSessionRequest parsing failed", e);
            }

            // this.networkFacade is already initialized if we were constructed by Session(string, INetwrokFacade).
            // This only happens when we're testing; in normal use we'll enter the body of this if statement.
            if (this.networkFacade == null)
            {
                XmlDocument optionsXml = new XmlDocument();
                optionsXml.LoadXml(this.optionsString);
                this.networkFacade = NetworkFacade.Create(new Options(optionsXml));
            }

            this.mIdentityProvider = request.IdentityProvider;

            if (!this.networkFacade.Login(this.mIdentityProvider))
            {
                return false;
            }

            this.changelistHandler = new ChangelistHandler();
            EntityHandler entityHandler = new EntityHandler(this.changelistHandler, this.networkFacade);

            List<IUriHandler> handlers = new List<IUriHandler>()
            {
                new ScenesHandler(entityHandler, this.changelistHandler, this.networkFacade),
                entityHandler,
                this.changelistHandler,
                new ChatHandler(this.changelistHandler, this.networkFacade),
                new ArbitrationHandler(this.changelistHandler, this.networkFacade),
                new ConfigHandler(this.networkFacade),
            };

            if (this.networkFacade.Streaming != null)
            {
                handlers.Add(new StreamingHandler(this.changelistHandler, this.networkFacade));
            }

            this.mHandler = new HandlerMultiplexer(handlers);
            
            Thread processingThread = new Thread(this.ProcessBadumna);
            processingThread.IsBackground = true;
            this.mHasStopped = new ManualResetEvent(false);
            processingThread.Start();

            return true;
        }

        public void DumpNetworkStatus()
        {
            Console.WriteLine(this.networkFacade.GetNetworkStatus());
        }

        private void ProcessBadumna()
        {
            try
            {
                while (!this.mStopProcessing)
                {
                    this.networkFacade.ProcessNetworkState();  // TODO: Should prevent request handling during this call...
                    Thread.Sleep(100);
                }
            }
            catch (Exception)
            {
                // Crashing thread must not crash server
            }
            finally
            {
                this.mHasStopped.Set();
            }
        }

        private void Shutdown()
        {
            if (this.mHasStopped != null)
            {
                this.mStopProcessing = true;
                this.mHasStopped.WaitOne();
            }
            this.networkFacade.Shutdown(true);

            if (this.changelistHandler != null)
            {
                this.changelistHandler.Shutdown();
            }
        }

        public override object InitializeLifetimeService()
        {
            return null;  // Live forever.  TODO: This probably prevents garbage collection altogether?
        }

        public bool Handle(string path, MarshalableListenerContext context)
        {
            // Check if it's for this session
            if (path.StartsWith("/"))
            {
                path = path.Substring(1);
            }
            string[] pathParts = path.Split('/');
            for (int i = 0; i < pathParts.Length; i++)
            {
                pathParts[i] = Uri.UnescapeDataString(pathParts[i]);
            }

            if (pathParts.Length == 0 || pathParts[0] != this.mId)
            {
                return false;
            }

            // Something for this session, mark as active
            this.mLastActivity = DateTime.Now;

            // Process it
            if (pathParts.Length > 1)
            {
                string subPath = path.Substring(path.IndexOf('/') + 1);

                if (this.mHandler.Handle(subPath, context))
                {
                    return true;
                }

                return false;
            }

            Helpers.HttpResponseStatus(context, HttpStatusCode.MethodNotAllowed, "Method not allowed");
            return true;
        }
    }
}
