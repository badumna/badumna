﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;

namespace Tunnel
{
    class Pipe
    {
        private class InStream : Stream
        {
            public override bool CanRead { get { return false; } }
            public override bool CanSeek { get { return false; } }
            public override bool CanWrite{ get { return true; } }
            public override long Length { get { throw new NotSupportedException(); } }
            public override long Position
            {
                get { throw new NotSupportedException(); }
                set { throw new NotSupportedException(); }
            }

            public event Action Closed;

            private OutStream mOutStream;

            public InStream(OutStream outStream)
            {
                this.mOutStream = outStream;
            }

            public override void Close()
            {
                base.Close();

                Action handler = this.Closed;
                if (handler != null)
                {
                    handler();
                }
            }

            public override void Flush()
            {
            }

            public override int Read(byte[] buffer, int offset, int count)
            {
                throw new NotSupportedException();
            }

            public override long Seek(long offset, SeekOrigin origin)
            {
                throw new NotSupportedException();
            }

            public override void SetLength(long value)
            {
                throw new NotSupportedException();
            }

            public override void Write(byte[] buffer, int offset, int count)
            {
                this.mOutStream.PipeWrite(buffer, offset, count);
            }
        }

        private class OutStream : Stream
        {
            public override bool CanRead { get { return true; } }
            public override bool CanSeek { get { return false; } }
            public override bool CanWrite{ get { return false; } }
            public override long Length { get { throw new NotSupportedException(); } }
            public override long Position
            {
                get { throw new NotSupportedException(); }
                set { throw new NotSupportedException(); }
            }

            private MemoryStream mBuffer = new MemoryStream();
            private readonly object mBufferLock = new object();
            private bool mIsPipeClosed;

            public override void Flush()
            {
            }

            internal void PipeWrite(byte[] buffer, int offset, int count)
            {
                lock (this.mBufferLock)
                {
                    long oldPosition = this.mBuffer.Position;

                    this.mBuffer.Seek(0, SeekOrigin.End);
                    this.mBuffer.Write(buffer, offset, count);

                    this.mBuffer.Position = oldPosition;
                    Monitor.PulseAll(this.mBufferLock);
                }
            }

            internal void PipeClosed()
            {
                lock (this.mBufferLock)
                {
                    this.mIsPipeClosed = true;
                    Monitor.PulseAll(this.mBufferLock);
                }
            }

            public override int Read(byte[] buffer, int offset, int count)
            {
                lock (this.mBufferLock)
                {
                    while (true)
                    {
                        int bytesRead = this.mBuffer.Read(buffer, offset, count);

                        if (bytesRead > 0 || this.mIsPipeClosed)
                        {
                            return bytesRead;
                        }

                        Monitor.Wait(this.mBufferLock);
                    }
                }
            }

            public override long Seek(long offset, SeekOrigin origin)
            {
                throw new NotSupportedException();
            }

            public override void SetLength(long value)
            {
                throw new NotSupportedException();
            }

            public override void Write(byte[] buffer, int offset, int count)
            {
                throw new NotSupportedException();
            }
        }

        private InStream mIn;
        public Stream In { get { return this.mIn; } }

        private OutStream mOut;
        public Stream Out { get { return this.mOut; } }

        public Pipe()
        {
            this.mOut = new OutStream();
            this.mIn = new InStream(this.mOut);
            this.mIn.Closed += this.mOut.PipeClosed;
        }
    }
}
