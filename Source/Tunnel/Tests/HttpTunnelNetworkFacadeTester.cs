﻿//-----------------------------------------------------------------------
// <copyright file="HttpTunnelNetworkFacadeTester.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Threading;
using Badumna;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Facade.Http;
using Badumna.SpatialEntities;
using Badumna.Utilities;
using NUnit.Framework;
using Rhino.Mocks;
using Tunnel;

namespace TunnelTests
{
    [TestFixture]
    public class HttpTunnelNetworkFacadeTester
    {
#if false
        Badumna.Chat.IChatService CreateChatService();

        Badumna.Arbitration.IArbitrator GetArbitrator(string name);

        Badumna.Streaming.StreamingManager Streaming { get; }

        void FlagForUpdate(Badumna.SpatialEntities.ISpatialOriginal localEntity, int changedPartIndex);
        void FlagForUpdate(Badumna.SpatialEntities.ISpatialOriginal localEntity, Badumna.Utilities.BooleanArray changedParts);
        
        Badumna.DataTypes.Vector3 GetDestination(Badumna.SpatialEntities.IDeadReckonable deadReckonable);
        void Initialize(string appName);
        void Initialize();

        bool Login(Badumna.Security.IIdentityProvider identityProvider);
        void Logout();

        void ProcessNetworkState();
        void SendCustomMessageToOriginal(Badumna.SpatialEntities.ISpatialReplica remoteEntity, System.IO.MemoryStream eventData);
        void SendCustomMessageToRemoteCopies(Badumna.SpatialEntities.ISpatialOriginal localEntity, System.IO.MemoryStream eventData);
        void Shutdown(bool blockUntilComplete);
        void SnapToDestination(Badumna.SpatialEntities.IDeadReckonable deadReckonable);
#endif

        /// <summary>
        /// Tests that AveragePacketLossRate returns a negative value indicating that the packet loss
        /// rate is unknown when using a tunnelled connection.
        /// </summary>
        [Test]
        public void AveragePacketLossRateIsNegative()
        {
            this.WithTunnelFacade(t => Assert.Less(t.AveragePacketLossRate, 0, "Expected negative value"));
        }

        /// <summary>
        /// Tests that MaximumPacketLossRate returns a negative value indicating that the packet loss
        /// rate is unknown when using a tunnelled connection.
        /// </summary>
        [Test]
        public void MaximumPacketLossRateIsNegative()
        {
            this.WithTunnelFacade(t => Assert.Less(t.MaximumPacketLossRate, 0, "Expected negative value"));
        }

        /// <summary>
        /// Tests that TotalSendLimitBytesPerSecond returns a negative value indicating that the packet loss
        /// rate is unknown when using a tunnelled connection.
        /// </summary>
        [Test]
        public void TotalSendLimitBytesPerSecondIsNegative()
        {
            this.WithTunnelFacade(t => Assert.Less(t.TotalSendLimitBytesPerSecond, 0, "Expected negative value"));
        }

        /// <summary>
        /// Tests that MaximumSendLimitBytesPerSecond returns a negative value indicating that the packet loss
        /// rate is unknown when using a tunnelled connection.
        /// </summary>
        [Test]
        public void MaximumSendLimitBytesPerSecondIsNegative()
        {
            this.WithTunnelFacade(t => Assert.Less(t.MaximumSendLimitBytesPerSecond, 0, "Expected negative value"));
        }

        /// <summary>
        /// Tests that InboundBytesPerSecond returns a negative value indicating that the packet loss
        /// rate is unknown when using a tunnelled connection.
        /// </summary>
        [Test]
        public void InboundBytesPerSecondIsNegative()
        {
            this.WithTunnelFacade(t => Assert.Less(t.InboundBytesPerSecond, 0, "Expected negative value"));
        }

        /// <summary>
        /// Tests that OutboundBytesPerSecond returns a negative value indicating that the packet loss
        /// rate is unknown when using a tunnelled connection.
        /// </summary>
        [Test]
        public void OutboundBytesPerSecondIsNegative()
        {
            this.WithTunnelFacade(t => Assert.Less(t.OutboundBytesPerSecond, 0, "Expected negative value"));
        }

        [Test]
        public void IsTunnelledIsTrue()
        {
            this.WithTunnelFacade(t => Assert.IsTrue(t.IsTunnelled));
        }

        [Test]
        public void IsLoggedInIsCorrect()
        {
            INetworkFacade networkFacade = (INetworkFacade)MockRepository.GenerateMock<INetworkFacade, INetworkFacadeInternal>();
            networkFacade.Expect(x => x.Login(Arg<Badumna.Security.IIdentityProvider>.Is.Anything)).Return(true);
            this.WithTunnelFacade(
                networkFacade,
                false,
                true,
                delegate(HttpTunnelNetworkFacade tunnel)
                {
                    Assert.IsFalse(tunnel.IsLoggedIn, "IsLoggedIn should be false before login");
                    tunnel.Login();
                    Assert.IsTrue(tunnel.IsLoggedIn, "IsLoggedIn should be true after login");
                });
        }

        [Test]
        public void IsActiveIsFalse()
        {
            this.WithTunnelFacade(t => Assert.IsFalse(t.IsFullyConnected));
        }

        [Test, ExpectedException(typeof(NotSupportedException))]
        public void AnnounceServiceNotSupported()
        {
            this.WithTunnelFacade(t => t.AnnounceService(Badumna.ServiceDiscovery.ServerType.Arbitration));
        }

        [Test, ExpectedException(typeof(NotSupportedException))]
        public void GetNetworkStatusNotSupported()
        {
            this.WithTunnelFacade(t => t.GetNetworkStatus());
        }

        [Test, ExpectedException(typeof(NotSupportedException))]
        public void RegisterArbitrationHandlerNotSupported()
        {
            this.WithTunnelFacade(t => t.RegisterArbitrationHandler(null, TimeSpan.Zero, null));
        }

        [Test, ExpectedException(typeof(NotSupportedException))]
        public void SendServerArbitrationEventNotSupported()
        {
            this.WithTunnelFacade(t => t.SendServerArbitrationEvent(0, null));
        }

        [Test, ExpectedException(typeof(NotSupportedException))]
        public void GetUserIdForSessionNotSupported()
        {
            this.WithTunnelFacade(t => t.GetUserIdForSession(0));
        }

        /// <summary>
        /// Test that the return value from Login() on the tunnel server is correctly propagated to the client.
        /// </summary>
        /// <param name="result">The result to expect from Login()</param>
        [TestCase(true), TestCase(false)]
        public void LoginResultOnServerReplicatedOnClient(bool result)
        {
            INetworkFacade networkFacade = (INetworkFacade)MockRepository.GenerateMock<INetworkFacade, INetworkFacadeInternal>();
            networkFacade.Expect(x => x.Login(Arg<Badumna.Security.IIdentityProvider>.Is.Anything)).Return(result);
            this.WithTunnelFacade(
                networkFacade,
                false,
                result,
                delegate(HttpTunnelNetworkFacade tunnel)
                {
                    Assert.AreEqual(result, tunnel.Login(), "Login() returned wrong result");

                    if (result)
                    {
                        tunnel.Shutdown();
                    }
                });
        }

        /// <summary>
        /// Test that calling RegisterEntityDetails on the tunnel facade invokes RegisterEntityDetails on the
        /// tunnel server with the same parameters.
        /// </summary>
        [Test]
        public void RegisterEntityDetailsCalledViaTunnel()
        {
            INetworkFacade networkFacade = (INetworkFacade)MockRepository.GenerateMock<INetworkFacade, INetworkFacadeInternal>();

            float aoi = 42;
            Vector3 maxVelocity = new Vector3(5, 7, 11);
            networkFacade.Expect(x => x.RegisterEntityDetails(aoi, maxVelocity.Magnitude));

            this.WithTunnelFacade(
                networkFacade,
                delegate(HttpTunnelNetworkFacade tunnel)
                {
                    tunnel.RegisterEntityDetails(aoi, maxVelocity.Magnitude);
                });
        }

        [Test]
        public void JoinSceneParametersPassedCorrectly()
        {
            INetworkFacade networkFacade = (INetworkFacade)MockRepository.GenerateMock<INetworkFacade, INetworkFacadeInternal>();

            string sceneName = "test-scene";
            NetworkScene result = MockRepository.GenerateMock<NetworkScene>(
                new QualifiedName("", ""), 
                null,
                null,
                null,
                new NetworkEventQueue(),
                new QueueInvokable(new SimpleEventQueue().Add),
                false);

            networkFacade.Expect(x => x.JoinScene(Arg.Is(sceneName), Arg<CreateSpatialReplica>.Is.Anything, Arg<RemoveSpatialReplica>.Is.Anything)).Return(result);

            this.WithTunnelFacade(
                networkFacade,
                delegate(HttpTunnelNetworkFacade tunnel)
                {
                    tunnel.JoinScene(sceneName, (CreateSpatialReplica)null, null);
                });
        }

        [Test, ExpectedException(typeof(TunnelRequestException))]
        public void FlagForUpdateOnUnregisteredEntityThrowsTunnelRequestException()
        {
            INetworkFacade networkFacade = (INetworkFacade)MockRepository.GenerateMock<INetworkFacade, INetworkFacadeInternal>();

            BadumnaId guid = BadumnaId.TryParse("Open|127.0.0.1:1-1");
            int changedPart = 13;
            networkFacade.Expect(x => x.FlagForUpdate(Arg<ISpatialOriginal>.Is.Anything, Arg.Is(changedPart)));

            this.WithTunnelFacade(
                networkFacade,
                delegate(HttpTunnelNetworkFacade tunnel)
                {
                    ISpatialOriginal original = MockRepository.GenerateStub<ISpatialOriginal>();
                    original.Guid = guid;
                    tunnel.FlagForUpdate(original, changedPart);
                    tunnel.ProcessNetworkState();
                });
        }

        // TODO: Merge with JoinScene*() test.
        // TODO: Fix this test.  It's currently ignored because it deadlocks.  The deadlock
        //       is a result of both the client side and the server side locking the same
        //       NetworkEventQueue.  They use the same NetworkEventQueue because they both
        //       access it through the NetworkEventQueue.Instance singleton and they both
        //       live in the same app domain (dynamic mocks are difficult/impossible to use
        //       across an app domain boundary, hence why same app domain).
        [Test, Ignore]
        public void FlagForUpdateParametersPassedCorrectly()
        {
            INetworkFacade networkFacade = (INetworkFacade)MockRepository.GenerateMock<INetworkFacade, INetworkFacadeInternal>();

            string sceneName = "test-scene";
            NetworkScene mockScene = MockRepository.GenerateMock<NetworkScene>();
            networkFacade.Expect(x => x.JoinScene(Arg.Is(sceneName), Arg<CreateSpatialReplica>.Is.Anything, Arg<RemoveSpatialReplica>.Is.Anything)).Return(mockScene);

            int changedPart = 13;
            networkFacade.Expect(x => x.FlagForUpdate(Arg<ISpatialOriginal>.Is.Anything, Arg.Is(changedPart)));

            this.WithTunnelFacade(
                networkFacade,
                delegate(HttpTunnelNetworkFacade tunnel)
                {
                    ISpatialOriginal original = MockRepository.GenerateStub<ISpatialOriginal>();
                    uint entityType = 0;

                    NetworkScene scene = tunnel.JoinScene(sceneName, (CreateSpatialReplica)null, null);
                    scene.RegisterEntity(original, entityType);

                    tunnel.FlagForUpdate(original, changedPart);
                    tunnel.ProcessNetworkState();
                });
        }

        private void WithTunnelFacade(Action<HttpTunnelNetworkFacade> action)
        {
            this.WithTunnelFacade(null, action);
        }

        private void WithTunnelFacade(INetworkFacade actualFacade, Action<HttpTunnelNetworkFacade> action)
        {
            this.WithTunnelFacade(actualFacade, true, true, action);
        }

        /// <summary>
        /// Creates a tunnel server that uses actualFacade and then executes action with a tunnel client attached
        /// to this server.
        /// </summary>
        /// <param name="actualFacade">The real facade, run on the server.</param>
        /// <param name="performLogin">Indicates whether Login should be performed automatically by this method.</param>
        /// <param name="waitForShutdown">Indicates whether this method should wait synchronously for the shutdown of the
        /// session on the server (this should be set true if the client successfully logs in either automatically or manually).</param>
        /// <param name="action">The action to perform on the tunnel client.</param>
        private void WithTunnelFacade(INetworkFacade actualFacade, bool performLogin, bool waitForShutdown, Action<HttpTunnelNetworkFacade> action)
        {
            actualFacade = actualFacade ?? (INetworkFacade)MockRepository.GenerateMock<INetworkFacade, INetworkFacadeInternal>();

            string prefix = "http://localhost:20192/";
            TunnelServer tunnelServer = new TunnelServer(prefix, actualFacade);

            if (performLogin)
            {
                actualFacade.Stub(x => x.Login(Arg<Badumna.Security.IIdentityProvider>.Is.Anything)).Return(true);

                Action<HttpTunnelNetworkFacade> originalAction = action;
                action =
                    delegate(HttpTunnelNetworkFacade t)
                    {
                        t.Login();
                        try
                        {
                            originalAction(t);
                        }
                        finally
                        {
                            t.Shutdown();
                        }
                    };
            }

            ManualResetEvent hasShutdown = new ManualResetEvent(false);
            if (waitForShutdown)
            {
                actualFacade.Stub(x => x.Shutdown(Arg<bool>.Is.Anything)).WhenCalled(delegate { hasShutdown.Set(); });
            }

            tunnelServer.Start();
            try
            {
                NetworkEventQueue eventQueue = new NetworkEventQueue();
                ITime timeKeeper = eventQueue;
                Options options = new Options();
                options.Connectivity.ApplicationName = "TunnelTest";
                options.Connectivity.TunnelMode = TunnelMode.On;
                options.Connectivity.TunnelUris.Add(prefix);
                options.Validate();
                HttpTunnelNetworkFacade tunnel = new HttpTunnelNetworkFacade(options, eventQueue, timeKeeper);
                action(tunnel);
            }
            finally
            {
                tunnelServer.Stop();
            }

            if (waitForShutdown)
            {
                // If we don't wait for shutdown then the tunnel server thread might make more calls on the mocked
                // facade which can cause exceptions if it happens during verification.
                Assert.IsTrue(hasShutdown.WaitOne(TimeSpan.FromSeconds(10), true), "Didn't get shutdown cleanly");
            }

            actualFacade.VerifyAllExpectations();
        }
    }
}