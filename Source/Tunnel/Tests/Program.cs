﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
using System;
using System.Collections.Generic;
using System.Reflection;

namespace TunnelTests
{
    /// <summary>
    /// A program to launch the NUnit GUI and run the unit tests in this assembly.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// The entry point for the program.
        /// </summary>
        /// <param name="args">Command line arguments</param>
        [STAThread]
        internal static void Main(string[] args)
        {
            bool useGui = args.Length > 0 && args[0].ToUpper() == "GUI";
            List<string> arguments = new List<string>();
            arguments.Add(Assembly.GetExecutingAssembly().Location);
            arguments.AddRange(args);
            if (useGui)
            {
                arguments.Remove("GUI");
                arguments.Add("/run");
                NUnit.Gui.AppEntry.Main(arguments.ToArray());
            }
            else
            {
                arguments.Add("/wait");
                NUnit.ConsoleRunner.Runner.Main(arguments.ToArray());
            }
        }
    }
}
