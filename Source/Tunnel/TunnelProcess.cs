﻿using System;
using System.IO;
using System.Net;
using System.Threading;

using GermHarness;
using Badumna;


namespace Tunnel
{
    class TunnelProcess : IHostedProcess
    {
        private const string DefaultPrefix = "http://+:21247/";
        private TunnelServer tunnelServer;

        private Options badumnaOptions;

        public TunnelProcess(Options badumnaOptions)
        {
            this.badumnaOptions = badumnaOptions;
        }

        #region IHostedProcess Members

        public void OnInitialize(ref string[] arguments)
        {
            ManualResetEvent done = new ManualResetEvent(false);
            Console.CancelKeyPress += delegate { done.Set(); };

            Console.WriteLine("Starting tunnel server...");

            string prefix = TunnelProcess.DefaultPrefix;

            if (arguments.Length > 1)
            {
                // Too many arguments, let program report error.
                string[] extras = new string[arguments.Length - 1];
                for (int i = 0; i < arguments.Length - 1; ++i)
                {
                    extras[i] = arguments[i];
                }

                arguments = extras;               
                return;
            }
            else if (arguments.Length ==1)
            {
                prefix = arguments[0];
                // Removed consumed prefix from arguments.
                arguments = new string[0];
            }

            try
            {
                this.tunnelServer = new TunnelServer(prefix, this.badumnaOptions);
            }
            catch (TunnelConfiguationException e)
            {
                Console.WriteLine(e.Message);
                if (e.InnerException is HttpListenerException)
                {
                    Environment.Exit(((HttpListenerException)e.InnerException).ErrorCode);
                }

                Environment.Exit(1);  // Unknown error code
            }
        }

        public bool OnPerformRegularTasks(int delayMilliseconds)
        {
            if (Console.KeyAvailable)
            {
                string command = Console.ReadLine();

                if (command.StartsWith("?"))
                {
                    this.tunnelServer.SessionHandler.WithSubHandlerKeys(id => Console.WriteLine(id));
                }
                else
                {
                    Session session = this.tunnelServer.SessionHandler.GetSession(command);
                    if (session != null)
                    {
                        session.DumpNetworkStatus();
                    }
                    else
                    {
                        Console.WriteLine("Session not found");
                    }
                }
            }

            return true;
        }

        public byte[] OnProcessRequest(int requestType, byte[] request)
        {
            return null;
        }

        public void OnShutdown()
        {
            Console.WriteLine("Shutting down...");
            this.tunnelServer.Stop();
        }

        public bool OnStart()
        {
            this.tunnelServer.Start();

            Console.WriteLine("Listening, Ctrl-C to stop");

            return true;
        }

        /// <summary>
        /// Write a list of supported command line options.
        /// </summary>
        /// <param name="tw">The text writer to use.</param>
        public void WriteOptionsDescription(TextWriter tw)
        {
            // No process-specific options.
        }

        #endregion
    }
}
