#!/bin/bash

wc -l BadumnaCppWrapper/Badumna/Core/*.cpp BadumnaCppWrapper/Badumna/Core/*.h BadumnaCppWrapper/Badumna/DataTypes/*.cpp BadumnaCppWrapper/Badumna/DataTypes/*.h BadumnaCppWrapper/Badumna/Replication/*.cpp BadumnaCppWrapper/Badumna/Replication/*.h BadumnaCppWrapper/Badumna/Chat/*.cpp BadumnaCppWrapper/Badumna/Chat/*.h BadumnaCppWrapper/Badumna/Arbitration/*.cpp BadumnaCppWrapper/Badumna/Arbitration/*.h BadumnaCppWrapper/Badumna/Utils/*.cpp BadumnaCppWrapper/Badumna/Utils/*.h BadumnaCppWrapper/Badumna/Dei/*.cpp BadumnaCppWrapper/Badumna/Dei/*.h BadumnaCppWrapper/Badumna/Configuration/*.cpp BadumnaCppWrapper/Badumna/Configuration/*.h

wc -l BadumnaCppTest/Source/*.cpp BadumnaCppTest/Source/*.h

wc -l BadumnaAutoTest/*.cpp BadumnaAutoTest/*.h
