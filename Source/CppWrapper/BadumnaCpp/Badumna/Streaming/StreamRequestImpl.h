//---------------------------------------------------------------------------------
// <copyright file="StreamRequestImpl.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_STREAM_REQUEST_IMPL_H
#define BADUMNA_STREAM_REQUEST_IMPL_H

#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Core/ProxyObject.h"
#include "Badumna/Core/MonoRuntime.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Streaming/StreamingServiceManager.h"
#include "Badumna/Streaming/IStreamController.h"
#include "Badumna/Streaming/StreamingCompleteDelegate.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class StreamRequestImpl : public ProxyObject
    {
        friend class BadumnaRuntime;
    public:
        String GetStreamTag() const;
        String GetStreamName() const;
        double GetSizeKiloBytes() const;
        String GetUserName() const;
        bool IsAccepted() const;

        void Reject();
        IStreamController *AcceptFile(String const &path_name, StreamingCompleteDelegate receive_complete_delegate);

    private:
        StreamRequestImpl(MonoRuntime *runtime, DotNetObject object, StreamingServiceManager *manager);
        StreamingServiceManager * const manager_;

        DISALLOW_COPY_AND_ASSIGN(StreamRequestImpl);
    };
}

#endif // BADUMNA_STREAM_REQUEST_IMPL_H