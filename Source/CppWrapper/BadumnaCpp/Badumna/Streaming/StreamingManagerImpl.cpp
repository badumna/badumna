//---------------------------------------------------------------------------------
// <copyright file="StreamingManagerImpl.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include "Badumna/Core/TypeRegistry.h"
#include "Badumna/Core/InternalUniqueId.h"
#include "Badumna/Core/ManagedObjectGuard.h"
#include "Badumna/DataTypes/BadumnaIdImpl.h"
#include "Badumna/Streaming/StreamingManagerImpl.h"
#include "Badumna/Streaming/LocalStreamController.h"

namespace Badumna
{
    REGISTER_MANAGED_MONO_TYPE(CppWrapperStub, StreamingManagerStub);
    StreamingManagerImpl::StreamingManagerImpl(
        MonoRuntime *runtime,
        DotNetObject object,
        StreamingServiceManager *manager)
        : ProxyObject(runtime, object, "Badumna.CppWrapperStub", "StreamingManagerStub"),
        manager_(manager)
    {
        assert(manager != NULL);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, StreamingManagerStub, BeginSendReliableFile, 4)
    IStreamController *StreamingManagerImpl::BeginSendReliableFile(
        String const &stream_tag,
        String const &full_path,
        BadumnaId const &destination,
        String const &username,
        StreamingCompleteDelegate complete_delegate)
    {
        UniqueId unique_id = InternalUniqueIdManager::GetStreamingCompleteDelegateId(
            stream_tag,
            full_path,
            destination);

        manager_->AddSendCompleteDelegate(unique_id, complete_delegate);

        Arguments args;
        args.AddArgument(MonoRuntime::GetString(stream_tag));
        args.AddArgument(MonoRuntime::GetString(full_path));
        args.AddArgument(ManagedObjectGuard::GetManagedObject(destination));
        args.AddArgument(MonoRuntime::GetString(username));

        ExceptionDetails exception_details;
        DotNetObject controller = managed_object_.InvokeMethod("BeginSendReliableFile", args, &exception_details);
        if(exception_details.exception_caught)
        {
            return NULL;
        }
        
        return new LocalStreamController(
            BadumnaRuntime::Instance().CreateLocalStreamControllerImpl(controller, manager_));
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, StreamingManagerStub, SubscribeToReceiveStreamRequests, 1)
    void StreamingManagerImpl::SubscribeToReceiveStreamRequests(
        String const &stream_tag, 
        ReceiveStreamRequestDelegate request_delegate)
    {
        UniqueId unique_id = InternalUniqueIdManager::GetStreamingRequestDelegateId(stream_tag);
        manager_->AddRequestDelegate(unique_id, request_delegate);

        Arguments args;
        args.AddArgument(MonoRuntime::GetString(stream_tag));

        managed_object_.InvokeMethod("SubscribeToReceiveStreamRequests", args);
    }
}