//---------------------------------------------------------------------------------
// <copyright file="StreamRequest.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include "Badumna/Streaming/StreamRequest.h"
#include "Badumna/Streaming/StreamRequestImpl.h"

namespace Badumna
{
    StreamRequest::StreamRequest(StreamRequestImpl *impl)
        : impl_(impl)
    {
        assert(impl != NULL);
    }

    StreamRequest::~StreamRequest()
    {
    }

    String StreamRequest::GetStreamTag() const
    {
        return impl_->GetStreamTag();
    }

    String StreamRequest::GetStreamName() const
    {
        return impl_->GetStreamName();
    }

    double StreamRequest::GetSizeKiloBytes() const
    {
        return impl_->GetSizeKiloBytes();
    }
    
    String StreamRequest::GetUserName() const
    {
        return impl_->GetUserName();
    }
    
    bool StreamRequest::IsAccepted() const
    {
        return impl_->IsAccepted();
    }

    void StreamRequest::Reject()
    {
        impl_->Reject();
    }

    IStreamController *StreamRequest::AcceptFile(
        String const &path_name, 
        StreamingCompleteDelegate receive_complete_delegate)
    {
        return impl_->AcceptFile(path_name, receive_complete_delegate);
    }
}