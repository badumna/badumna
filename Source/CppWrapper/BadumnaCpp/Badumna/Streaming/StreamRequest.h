//---------------------------------------------------------------------------------
// <copyright file="StreamRequest.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_STREAM_REQUEST_H
#define BADUMNA_STREAM_REQUEST_H

#include <memory>

#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/Streaming/StreamingCompleteDelegate.h"
#include "Badumna/Streaming/IStreamController.h"

namespace Badumna
{
    class StreamRequestImpl;

    /**
     * @class StreamRequest
     * @brief StreamRequest provides details of the streaming request. 
     */
    class BADUMNA_API StreamRequest
    {
    public:
        /**
         * Constructor.
         */
        explicit StreamRequest(StreamRequestImpl *impl);

        /**
         * Destructor.
         */
        ~StreamRequest();

        /**
         * Gets the stream tag of the request.
         *
         * @return The stream tag.
         */
        String GetStreamTag() const;
        
        /**
         * Gets the stream name.
         *
         * @return The stream name.
         */
        String GetStreamName() const;
        
        /**
         * Gets the total size of the stream in bytes.
         *
         * @return The total size of the stream.
         */
        double GetSizeKiloBytes() const;
        
        /**
         * Gets the username associated with the stream.
         *
         * @return The username.
         */
        String GetUserName() const;
        
        /**
         * Gets whether this stream has been accepted.
         *
         * @return Whether this stream has been accepted.
         */
        bool IsAccepted() const;

        /**
         * Rejects this request.
         */
        void Reject();
        
        /**
         * Accepts this file streaming request. The incoming file will be stored in the location specified by 
         * the parameter full_path_name. full_path_name must be a full path or this method will fail. 
         *
         * @param full_path_name Where this received file is going to be stored. 
         * @param receive_complete_delegate The delegate to be called on completion. 
         * @return A IStreamController object or NULL on failure. 
         */
        IStreamController *AcceptFile(String const &full_path_name, StreamingCompleteDelegate receive_complete_delegate);

    private:
        std::auto_ptr<StreamRequestImpl> const impl_;

        DISALLOW_COPY_AND_ASSIGN(StreamRequest);
    };
}

#endif // BADUMNA_STREAM_REQUEST_H