//---------------------------------------------------------------------------------
// <copyright file="LocalStreamController.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include "Badumna/Streaming/LocalStreamController.h"
#include "Badumna/Streaming/LocalStreamControllerImpl.h"

namespace Badumna
{
    LocalStreamController::LocalStreamController(LocalStreamControllerImpl *impl)
        : impl_(impl)
    {
        assert(impl != NULL);
    }

    LocalStreamController::~LocalStreamController()
    {
    }

    int64_t LocalStreamController::GetBytesTotal() const
    {
        return impl_->GetBytesTotal();
    }

    int64_t LocalStreamController::GetBytesTransfered() const
    {
        return impl_->GetBytesTransfered();
    }

    double LocalStreamController::GetTransferRateKBps() const
    {
        return impl_->GetTransferRateKBps();
    }

    int32_t LocalStreamController::GetEstimatedTimeRemaining() const
    {
        return impl_->GetEstimatedTimeRemaining();
    }

    StreamState LocalStreamController::GetCurrentState() const
    {
        return impl_->GetCurrentState();
    }
    
    bool LocalStreamController::WasSuccessful() const
    {
        return impl_->WasSuccessful();
    }
    
    void LocalStreamController::Cancel()
    {
        impl_->Cancel();
    }
}