//---------------------------------------------------------------------------------
// <copyright file="StreamingServiceManager.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_STREAMING_SERVICE_MANAGER_H
#define BADUMNA_STREAMING_SERVICE_MANAGER_H

#include "Badumna/Core/BadumnaMap.h"
#include "Badumna/Core/InternalUniqueId.h"
#include "Badumna/Streaming/StreamingCompleteDelegate.h"
#include "Badumna/Streaming/ReceiveStreamRequestDelegate.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class StreamingServiceManager
    {
    public:
        StreamingServiceManager();
        ~StreamingServiceManager();

        void AddSendCompleteDelegate(UniqueId const &id, StreamingCompleteDelegate complete_delegate);
        void AddReceiveCompleteDelegate(UniqueId const &id, StreamingCompleteDelegate receive_delegate);
        void AddRequestDelegate(UniqueId const &id, ReceiveStreamRequestDelegate request_delegate);

        void InvokeSendCompleteDelegate(UniqueId const &id, String const &tag, String const &name) const;
        void InvokeReceiveCompleteDelegate(UniqueId const &id, String const &tag, String const &name) const;
        void InvokeRequestDelegate(UniqueId const &id, StreamRequest *request) const;
            
    private:
        typedef BadumnaMap<StreamingCompleteDelegate> CompleteDelegate;
        typedef BadumnaMap<ReceiveStreamRequestDelegate> RequestDelegate;

        CompleteDelegate send_complete_delegates_;
        CompleteDelegate receive_complete_delegates_;
        RequestDelegate request_delegate_;

        DISALLOW_COPY_AND_ASSIGN(StreamingServiceManager);
    };
}

#endif // BADUMNA_STREAMING_SERVICE_MANAGER_H