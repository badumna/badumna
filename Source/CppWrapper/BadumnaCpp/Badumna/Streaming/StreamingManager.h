//---------------------------------------------------------------------------------
// <copyright file="StreamingManager.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_STREAMING_MANAGER_H
#define BADUMNA_STREAMING_MANAGER_H

#include <memory>
#include "Badumna/DataTypes/String.h"
#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/Streaming/StreamingCompleteDelegate.h"
#include "Badumna/Streaming/ReceiveStreamRequestDelegate.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class StreamingManagerImpl;

    /**
     * @class StreamingManager
     * @brief The streaming manager provides API for streaming operations.
     */
    class BADUMNA_API StreamingManager
    {
        friend class NetworkFacadeImpl;
    public:
        /**
         * Destructor.
         */
        ~StreamingManager();

        /**
         * Sends a file to the specified destination in a reliable way. Call to this method will fail if the specified
         * file doesn't exist or is inaccessible. Call to this method will also fail if the specified full_path 
         * parameter is not actually a full path. This method returns NULL on failure. 
         *
         * @param stream_tag A unique string to differentiate streaming classes.
         * @param full_path The full path of the file to send.
         * @param destination The destination BadumnaId.
         * @param username The username associated with the send operation.
         * @param complete_delegate The delegate to be invoked when the send is completed.
         * @return IStreamController object or NULL on failure. 
         */
        IStreamController *BeginSendReliableFile(
            String const &stream_tag,
            String const &full_path,
            BadumnaId const &destination,
            String const &username,
            StreamingCompleteDelegate complete_delegate);

        /**
         * Subscribes to receive stream. Different stream classes are differentiate by different stream tags. 
         *
         * @param stream_tag A unique string to differentiate streaming classes.
         * @param request_delegate The delegate to be called when there is incoming streaming request matching the 
         * stream_tag tag.
         */
        void SubscribeToReceiveStreamRequests(
            String const &stream_tag, 
            ReceiveStreamRequestDelegate request_delegate);

    private:
        explicit StreamingManager(StreamingManagerImpl *impl);
        std::auto_ptr<StreamingManagerImpl> const impl_;

        DISALLOW_COPY_AND_ASSIGN(StreamingManager);
    };
}

#endif // BADUMNA_STREAMING_MANAGER_H