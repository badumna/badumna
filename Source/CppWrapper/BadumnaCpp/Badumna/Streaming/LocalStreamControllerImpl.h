//---------------------------------------------------------------------------------
// <copyright file="LocalStreamControllerImpl.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_LOCAL_STREAM_CONTROLLER_IMPL_H
#define BADUMNA_LOCAL_STREAM_CONTROLLER_IMPL_H

#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Core/ProxyObject.h"
#include "Badumna/Core/MonoRuntime.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Streaming/StreamingServiceManager.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class LocalStreamControllerImpl : public ProxyObject
    {
        friend class BadumnaRuntime;
    public:
        int64_t GetBytesTotal() const;
        int64_t GetBytesTransfered() const;
        double GetTransferRateKBps() const;
        int32_t GetEstimatedTimeRemaining() const;
        StreamState GetCurrentState() const;
        bool WasSuccessful() const;
        void Cancel();

    private:
        LocalStreamControllerImpl(
            MonoRuntime *runtime, 
            DotNetObject object, 
            StreamingServiceManager *manager);

        StreamingServiceManager * const manager_;

        DISALLOW_COPY_AND_ASSIGN(LocalStreamControllerImpl);
    };
}

#endif // BADUMNA_LOCAL_STREAM_CONTROLLER_IMPL_H