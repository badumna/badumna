//---------------------------------------------------------------------------------
// <copyright file="StreamState.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_STREAM_STATE_H
#define BADUMNA_STREAM_STATE_H

namespace Badumna
{
    /**
     * @enum StreamState
     *
     * The state of a streaming session.   
     */
    enum StreamState
    {
        StreamState_Waiting,      /**< A streaming request has been created, but streaming of the data has not yet begun. */ 
        StreamState_InProgress,   /**< Data is currently being streamed. */
        StreamState_Completed,    /**< Streaming has finished successfully. */
        StreamState_Canceled,     /**< Streaming was canceled by the user. */
        StreamState_Error,        /**< Streaming failed due to an unknown error. */
        StreamState_Disconnected  /**< The streaming connection was lost. */
    };
}

#endif // BADUMNA_STREAM_STATE_H