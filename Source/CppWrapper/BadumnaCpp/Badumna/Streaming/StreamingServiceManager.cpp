//---------------------------------------------------------------------------------
// <copyright file="StreamingServiceManager.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include "Badumna/Streaming/StreamingServiceManager.h"

namespace Badumna
{
    StreamingServiceManager::StreamingServiceManager()
        : send_complete_delegates_(),
        receive_complete_delegates_(),
        request_delegate_()
    {
    }

    StreamingServiceManager::~StreamingServiceManager() 
    {
    }

    void StreamingServiceManager::AddSendCompleteDelegate(
        UniqueId const &id, 
        StreamingCompleteDelegate complete_delegate)
    {
        send_complete_delegates_.TryAdd(id, complete_delegate);
    }

    void StreamingServiceManager::AddReceiveCompleteDelegate(
        UniqueId const &id, 
        StreamingCompleteDelegate receive_delegate)
    {
        receive_complete_delegates_.TryAdd(id, receive_delegate);
    }
    
    void StreamingServiceManager::AddRequestDelegate(UniqueId const &id, ReceiveStreamRequestDelegate request_delegate)
    {
        request_delegate_.AddOrReplace(id, request_delegate);
    }

    void StreamingServiceManager::InvokeSendCompleteDelegate(
        UniqueId const &id, 
        String const &tag, 
        String const &name) const
    {
        send_complete_delegates_.InvokeDelegate(id, tag, name);
    }

    void StreamingServiceManager::InvokeReceiveCompleteDelegate(
        UniqueId const &id, 
        String const &tag, 
        String const &name) const
    {
        receive_complete_delegates_.InvokeDelegate(id, tag, name);
    }

    void StreamingServiceManager::InvokeRequestDelegate(UniqueId const &id, StreamRequest *request) const
    {
        request_delegate_.InvokeDelegate(id, request);
    }
}