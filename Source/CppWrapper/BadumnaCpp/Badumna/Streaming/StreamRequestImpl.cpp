//---------------------------------------------------------------------------------
// <copyright file="StreamRequestImpl.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include "Badumna/Core/TypeRegistry.h"
#include "Badumna/Core/InternalUniqueId.h"
#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/Streaming/LocalStreamController.h"
#include "Badumna/Streaming/StreamRequestImpl.h"

namespace Badumna
{
    REGISTER_MANAGED_MONO_TYPE(CppWrapperStub, StreamRequestStub);
    StreamRequestImpl::StreamRequestImpl(MonoRuntime *runtime, DotNetObject object, StreamingServiceManager *manager)
        : ProxyObject(runtime, object, "Badumna.CppWrapperStub", "StreamRequestStub"),
        manager_(manager)
    {
        assert(manager != NULL);
    }

    REGISTER_PROPERTY(CppWrapperStub, StreamRequestStub, StreamTag)
    String StreamRequestImpl::GetStreamTag() const
    {
        return managed_object_.GetPropertyFromName<String>("StreamTag");
    }

    REGISTER_PROPERTY(CppWrapperStub, StreamRequestStub, StreamName)
    String StreamRequestImpl::GetStreamName() const
    {
        return managed_object_.GetPropertyFromName<String>("StreamName");
    }

    REGISTER_PROPERTY(CppWrapperStub, StreamRequestStub, SizeKiloBytes)
    double StreamRequestImpl::GetSizeKiloBytes() const
    {
        return managed_object_.GetPropertyFromName<double>("SizeKiloBytes");
    }
    
    REGISTER_PROPERTY(CppWrapperStub, StreamRequestStub, UserName)
    String StreamRequestImpl::GetUserName() const
    {
        return managed_object_.GetPropertyFromName<String>("UserName");
    }
    
    REGISTER_PROPERTY(CppWrapperStub, StreamRequestStub, IsAccepted)
    bool StreamRequestImpl::IsAccepted() const
    {
        return managed_object_.GetPropertyFromName<bool>("IsAccepted");
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, StreamRequestStub, Reject, 0)
    void StreamRequestImpl::Reject()
    {
        managed_object_.InvokeMethod("Reject");
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, StreamRequestStub, AcceptFile, 1)
    IStreamController *StreamRequestImpl::AcceptFile(
        String const &path_name, 
        StreamingCompleteDelegate receive_complete_delegate)
    {
        UniqueId id = InternalUniqueIdManager::GetStreamingReceiveCompleteDelegateId(
            GetStreamTag(), 
            GetStreamName(), 
            GetUserName());
        manager_->AddReceiveCompleteDelegate(id, receive_complete_delegate);
        
        Arguments args;
        args.AddArgument(MonoRuntime::GetString(path_name));

        ExceptionDetails exception_details;
        DotNetObject controller = managed_object_.InvokeMethod("AcceptFile", args, &exception_details);
        if(exception_details.exception_caught)
        {
            return NULL;
        }

        return new LocalStreamController(
            BadumnaRuntime::Instance().CreateLocalStreamControllerImpl(controller, manager_));
    }
}