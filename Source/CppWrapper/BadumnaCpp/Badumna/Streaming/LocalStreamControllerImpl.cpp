//---------------------------------------------------------------------------------
// <copyright file="LocalStreamControllerImpl.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include "Badumna/Core/TypeRegistry.h"
#include "Badumna/Streaming/LocalStreamControllerImpl.h"

namespace Badumna
{
    REGISTER_MANAGED_MONO_TYPE(CppWrapperStub, StreamControllerStub);
    LocalStreamControllerImpl::LocalStreamControllerImpl(
        MonoRuntime *runtime, 
        DotNetObject object, 
        StreamingServiceManager *manager)
        : ProxyObject(runtime, object, "Badumna.CppWrapperStub", "StreamControllerStub"),
        manager_(manager)
    {
        assert(manager != NULL);
    }

    REGISTER_PROPERTY(CppWrapperStub, StreamControllerStub, BytesTotal)
    int64_t LocalStreamControllerImpl::GetBytesTotal() const
    {
        return managed_object_.GetPropertyFromName<int64_t>("BytesTotal");
    }

    REGISTER_PROPERTY(CppWrapperStub, StreamControllerStub, BytesTransfered)
    int64_t LocalStreamControllerImpl::GetBytesTransfered() const
    {
        return managed_object_.GetPropertyFromName<int64_t>("BytesTransfered");
    }

    REGISTER_PROPERTY(CppWrapperStub, StreamControllerStub, TransferRateKBps)
    double LocalStreamControllerImpl::GetTransferRateKBps() const
    {
        return managed_object_.GetPropertyFromName<double>("TransferRateKBps");
    }

    REGISTER_PROPERTY(CppWrapperStub, StreamControllerStub, EstimatedTimeRemaining)
    int32_t LocalStreamControllerImpl::GetEstimatedTimeRemaining() const
    {
        return managed_object_.GetPropertyFromName<int32_t>("EstimatedTimeRemaining");
    }

    REGISTER_PROPERTY(CppWrapperStub, StreamControllerStub, CurrentState)
    StreamState LocalStreamControllerImpl::GetCurrentState() const
    {
        int32_t ret = managed_object_.GetPropertyFromName<int32_t>("CurrentState");
        return static_cast<StreamState>(ret);
    }
    
    REGISTER_PROPERTY(CppWrapperStub, StreamControllerStub, WasSuccessful)
    bool LocalStreamControllerImpl::WasSuccessful() const
    {
        return managed_object_.GetPropertyFromName<bool>("WasSuccessful");
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, StreamControllerStub, Cancel, 0)
    void LocalStreamControllerImpl::Cancel()
    {
        managed_object_.InvokeMethod("Cancel");
    }
}