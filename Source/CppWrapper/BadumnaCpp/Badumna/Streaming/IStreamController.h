//---------------------------------------------------------------------------------
// <copyright file="IStreamController.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_ISTREAM_CONTROLLER_H
#define BADUMNA_ISTREAM_CONTROLLER_H

#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/Streaming/StreamState.h"

namespace Badumna
{
    /**
     * @class IStreamController
     * @brief An interface that provides methods for controlling a streaming session.
     */
    class BADUMNA_API IStreamController
    {
    public:
        /**
         * Destructor.
         */
        virtual ~IStreamController() {}

        /**
         * Gets the total bytes to be transferred in the streaming operation.
         *
         * @return The total number of bytes in the stream.
         */
        virtual int64_t GetBytesTotal() const = 0;

        /**
         * Gets the number of bytes transferred so far.
         * 
         * @return The number of bytes transferred.
         */
        virtual int64_t GetBytesTransfered() const = 0;
        
        /**
         * Gets the estimated transfer rate in kilobytes / second.
         * 
         * @return The estimated transfer rate.
         */
        virtual double GetTransferRateKBps() const = 0;
        
        /**
         * Gets the estimated time required to complete the streaming operation.
         *
         * @return The estimated remaining time.
         */
        virtual int32_t GetEstimatedTimeRemaining() const = 0;
        
        /**
         * Gets the current state of the streaming operation.
         *
         * @return The current state.
         */
        virtual StreamState GetCurrentState() const = 0;
        
        /**
         * Gets whether the streaming operation has completed successfully.
         *
         * @return Whether the streaming operation was successful.
         */
        virtual bool WasSuccessful() const = 0;

        /**
         * Cancels the streaming operation.
         */
        virtual void Cancel() = 0;
    };
}

#endif // BADUMNA_ISTREAM_CONTROLLER_H