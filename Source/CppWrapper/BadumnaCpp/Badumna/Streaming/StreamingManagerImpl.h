//---------------------------------------------------------------------------------
// <copyright file="StreamingManagerImpl.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_STREAMING_MANAGER_IMPL_H
#define BADUMNA_STREAMING_MANAGER_IMPL_H

#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Core/MonoRuntime.h"
#include "Badumna/Core/ProxyObject.h"
#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/Streaming/IStreamController.h"
#include "Badumna/Streaming/StreamingCompleteDelegate.h"
#include "Badumna/Streaming/StreamingServiceManager.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class StreamingManagerImpl : public ProxyObject
    {
        friend class BadumnaRuntime;
    public:
        IStreamController *BeginSendReliableFile(
            String const &stream_tag,
            String const &full_path,
            BadumnaId const &destination,
            String const &username,
            StreamingCompleteDelegate complete_delegate);

        void SubscribeToReceiveStreamRequests(
            String const &stream_tag, 
            ReceiveStreamRequestDelegate request_delegate);

    private:
        StreamingManagerImpl(
            MonoRuntime *runtime,
            DotNetObject object,    
            StreamingServiceManager *manager);

        StreamingServiceManager * const manager_;

        DISALLOW_COPY_AND_ASSIGN(StreamingManagerImpl);
    };
}

#endif // BADUMNA_STREAMING_MANAGER_IMPL_H