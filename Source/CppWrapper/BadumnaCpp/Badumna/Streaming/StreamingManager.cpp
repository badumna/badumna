//---------------------------------------------------------------------------------
// <copyright file="StreamingManager.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include "Badumna/Streaming/StreamingManager.h"
#include "Badumna/Streaming/StreamingManagerImpl.h"

namespace Badumna
{
    StreamingManager::StreamingManager(StreamingManagerImpl *impl)
        : impl_(impl)
    {
        assert(impl != NULL);
    }

    StreamingManager::~StreamingManager()
    {
    }

    IStreamController *StreamingManager::BeginSendReliableFile(
        String const &stream_tag,
        String const &full_path,
        BadumnaId const &destination,
        String const &username,
        StreamingCompleteDelegate complete_delegate)
    {
        return impl_->BeginSendReliableFile(stream_tag, full_path, destination, username, complete_delegate);
    }

    void StreamingManager::SubscribeToReceiveStreamRequests(
        String const &stream_tag, 
        ReceiveStreamRequestDelegate request_delegate)
    {
        impl_->SubscribeToReceiveStreamRequests(stream_tag, request_delegate);
    }
}