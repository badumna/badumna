//---------------------------------------------------------------------------------
// <copyright file="LocalStreamController.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_LOCAL_STREAM_CONTROLLER_H
#define BADUMNA_LOCAL_STREAM_CONTROLLER_H

#include <memory>
#include "Badumna/Streaming/IStreamController.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class LocalStreamControllerImpl;

    class LocalStreamController : public IStreamController
    {
        friend class StreamingManagerImpl;
        friend class StreamRequestImpl;
    public:
        ~LocalStreamController();

        int64_t GetBytesTotal() const;
        int64_t GetBytesTransfered() const;
        double GetTransferRateKBps() const;
        int32_t GetEstimatedTimeRemaining() const;
        StreamState GetCurrentState() const;
        bool WasSuccessful() const;
        void Cancel();

    private:
        explicit LocalStreamController(LocalStreamControllerImpl *imp);
        std::auto_ptr<LocalStreamControllerImpl> const impl_;

        DISALLOW_COPY_AND_ASSIGN(LocalStreamController);
    };
}

#endif // BADUMNA_LOCAL_STREAM_CONTROLLER_H