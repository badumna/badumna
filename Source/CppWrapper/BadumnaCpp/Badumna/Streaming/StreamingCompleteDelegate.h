//---------------------------------------------------------------------------------
// <copyright file="StreamingCompleteDelegate.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_STREAMING_COMPLETE_DELEGATE_H
#define BADUMNA_STREAMING_COMPLETE_DELEGATE_H

#include "Badumna/Core/Delegate.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    /**
     * @class StreamingCompleteDelegate
     * @brief The delegate to be called when the streaming operation is completed. 
     *
     * The delegate has the following signature:\n
     * \n
     * <code>void function_name(String const &stream_tag, String const &stream_name);</code>\n
     * \n
     */
    class BADUMNA_API StreamingCompleteDelegate : public Delegate2<void, String const &, String const &>
    {
    public:
        /**
         * Constructor that constructs an ChatMessageHandler with specified pointer to member function and object. 
         *
         * @tparam K Object type.
         * @param fp A pointer to a member function.
         * @param object The object. 
         */
        template<typename K>
        StreamingCompleteDelegate(void (K::*fp)(String const &, String const &), K &object)
            : Delegate2<void, String const &, String const &>(fp, object)
        {
        }
        
        /**
         * Constructor that constructs an ChatMessageHandler with specified pointer to free function.
         *
         * @param fp A pointer to a free function.
         */
        StreamingCompleteDelegate(void (*fp)(String const &, String const &))
            : Delegate2<void, String const &, String const &>(fp)
        {
        }
    };
}

#endif // BADUMNA_STREAMING_COMPLETE_DELEGATE_H