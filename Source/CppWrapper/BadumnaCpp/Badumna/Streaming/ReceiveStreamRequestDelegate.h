//---------------------------------------------------------------------------------
// <copyright file="ReceiveStreamRequestDelegate.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_RECEIVE_STREAM_REQUEST_DELEGATE_H
#define BADUMNA_RECEIVE_STREAM_REQUEST_DELEGATE_H

#include "Badumna/Core/Delegate.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/Streaming/StreamRequest.h"

namespace Badumna
{
    /**
     * @class ReceiveStreamRequestDelegate
     * @brief The delegate to be called when there is incoming streaming request. 
     * 
     * The delegate has the following signature:\n
     * \n
     * <code>void function_name(StreamRequest *);</code>\n
     * \n
     */
    class BADUMNA_API ReceiveStreamRequestDelegate : public Delegate1<void, StreamRequest *>
    {
    public:
        /**
         * Constructor that constructs an ChatMessageHandler with specified pointer to member function and object. 
         *
         * @tparam K Object type.
         * @param fp A pointer to a member function.
         * @param object The object. 
         */
        template<typename K>
        ReceiveStreamRequestDelegate(void (K::*fp)(StreamRequest *), K &object)
            : Delegate1<void, StreamRequest *>(fp, object)
        {
        }

        /**
         * Constructor that constructs an ChatMessageHandler with specified pointer to free function.
         *
         * @param fp A pointer to a free function.
         */
        ReceiveStreamRequestDelegate(void (*fp)(StreamRequest *))
            : Delegate1<void, StreamRequest *>(fp)
        {
        }
    };
}

#endif // BADUMNA_RECEIVE_STREAM_REQUEST_DELEGATE_H