//---------------------------------------------------------------------------------
// <copyright file="IdentityProvider.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_IDENTITY_PROVIDER_H
#define BADUMNA_IDENTITY_PROVIDER_H

#include <memory>

#include "Badumna/DataTypes/String.h"
#include "Badumna/Dei/LoginResult.h"
#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/Utils/Optional.h"

// forward declaration
namespace Badumna
{
    class ManagedObjectGuard;
    class NetworkFacadeImpl;
}

/**
 * @namespace Dei
 *
 * The Dei namespace contains all Dei related classes.
 */
namespace Dei
{
    class IdentityProviderImpl;

    /**
     * @class IdentityProvider
     * @brief The Dei identity provider is an implementation of IdentityProvider which used to log into badumna securely.
     *
     * You should not create an IdentityProvider yourself, instances are constructed upon successful
     * identity selection using a Dei.Session.
     */
    class BADUMNA_API IdentityProvider
    {
        friend class Badumna::ManagedObjectGuard;
        friend class Badumna::NetworkFacadeImpl;
        friend class SessionImpl;
    public:
        IdentityProvider();
        ~IdentityProvider();

    private:
        IdentityProvider(IdentityProviderImpl * impl);
        void reset(IdentityProviderImpl * impl);

        std::auto_ptr<IdentityProviderImpl> impl_;

        DISALLOW_COPY_AND_ASSIGN(IdentityProvider);
    };
}

#endif // BADUMNA_IDENTITY_PROVIDER_H
