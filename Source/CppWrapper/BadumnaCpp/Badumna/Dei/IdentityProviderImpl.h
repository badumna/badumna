//---------------------------------------------------------------------------------
// <copyright file="IdentityProviderImpl.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_IDENTITY_PROVIDER_IMPL_H
#define BADUMNA_IDENTITY_PROVIDER_IMPL_H

#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Core/MonoRuntime.h"
#include "Badumna/Core/ProxyObject.h"

namespace Dei
{
    class IdentityProviderImpl : public Badumna::ProxyObject
    {
        friend class Badumna::BadumnaRuntime;
    public:
        ~IdentityProviderImpl() {};
    private:
        IdentityProviderImpl(Badumna::MonoRuntime *runtime, DotNetObject object) : Badumna::ProxyObject(runtime, object){};
        DISALLOW_COPY_AND_ASSIGN(IdentityProviderImpl);
    };
}

#endif // BADUMNA_IDENTITY_PROVIDER_IMPL_H
