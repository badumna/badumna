//---------------------------------------------------------------------------------
// <copyright file="Session.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_DEI_SESSION_H
#define BADUMNA_DEI_SESSION_H

#include <memory>

#include "Badumna/DataTypes/String.h"
#include "Badumna/Dei/LoginResult.h"
#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/Utils/Optional.h"

// forward declaration
namespace Badumna
{
    class ManagedObjectGuard;
}

/**
 * @namespace Dei
 *
 * The Dei namespace contains all Dei related classes.
 */
namespace Dei
{
    class SessionImpl;
    class IdentityProvider;

    /**
     * @class Session
     * @brief The Dei identity provider remotely authenticates a user and retrieves signed certificates
     *
     * The Dei identity provider remotely authenticates a user and retrieves signed certificates which are used by Badumna
     * to maintain network integrity. Once constructed the Authenticate and SelectIdentity methods must be called
     * before passing it to the Badumna's NetworkFacade.Login method.
     */
    class BADUMNA_API Session
    {
        friend class Badumna::ManagedObjectGuard;
    public:
        /**
         * Generates a new RSA key pair and returns an XML string containing a newly generated key pair.
         * @return Returns an XML string containing a newly generated key pair.
         */
        static Badumna::String GenerateKeyPair();

        /**
         * Constructor that constructs an Session object by using specified server host and port details.
         * Generates a new private key.
         *
         * @param server_host The Dei server host. 
         * @param server_port The Dei server port.
         */ 
        Session(Badumna::String const &server_host, int server_port);
        
        /**
         * Constructor that constructs an Session object by using specified server host, port details and private key xml string
         *
         * @param server_host The Dei server host. 
         * @param server_port The Dei server port.
         * @param string_xml The Rsa key pair information from an XML string.
         */ 
        Session(Badumna::String const &server_host, int server_port, Badumna::String const &string_xml);

        /**
         * Destructor.
         */
        ~Session();

        /**
         * Disposes resources (e.g connection to the dei server) held by this object - further methods on
         * the object will almost certainly fail. This method is called by this object's destructor, so it
         * should only be needed in client code if you wish to release this object's resources prematurely.
         */
        void Dispose();

        /**
         * Sets whether to use an SSL connection to the dei server (true by default).
         */
        void SetUseSslConnection(bool useSsl);

        /**
         * Ignores SSL errors.
         */
        void SetIgnoreSslErrors();

        /**
         * Ignores SSL certificate name mismatch error.
         */
        void SetIgnoreSslCertificateNameMismatch();

        /**
         * Ignores SSL self signed certificate error.
         */
        void SetIgnoreSslSelfSignedCertificateErrors();

        /**
         * Authenticates the specified user. 
         *
         * @param username The username.
         * @param password The password.
         * @return A LoginResult object that indicates success or an error description.
         */
        LoginResult Authenticate(Badumna::String const &username, Badumna::String const &password) const;

        /**
         * Ties this user session with the specified character, which will be used
         * as the user's identity.
         *
         * Badumna::Character::None may be used if the peer does not need to participate
         * in character-based services (e.g Chat).
         *
         * @param character The character to act as.
         * @param identityProvider A reference to an IdentityProvider object which will be updated
                                   with the credentials required to log into the badumna network
                                   (using a NetworkFacade) if the method succeeds.
         * @return A LoginResult object that indicates success or an error description.
         */
        LoginResult SelectIdentity(Badumna::Character character, IdentityProvider &identityProvider) const;

        /**
         * Create a new character with the given name, belonging to the authenticated user.
         *
         * @param character_name The name of the character
         * @returns The newly-created character object, or NULL on failure.
         */
        std::auto_ptr<Badumna::Character> CreateCharacter(Badumna::String const &character_name) const;

        /**
         * Delete a character belonging to the authenticated user.
         *
         * @param character The character to delete
         * @returns A value indicating success.
         */
        bool DeleteCharacter(Badumna::Character const &character) const;

        /**
         * List all characters belonging to the authenticated user.
         *
         * @returns A vector of character objects.
         */
        std::vector<Badumna::Character> ListCharacters() const;
    
    private:
        std::auto_ptr<SessionImpl> const impl_;

        DISALLOW_COPY_AND_ASSIGN(Session);
    };
}

#endif // BADUMNA_DEI_SESSION_H
