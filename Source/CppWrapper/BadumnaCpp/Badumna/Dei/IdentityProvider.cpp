//---------------------------------------------------------------------------------
// <copyright file="IdentityProvider.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Dei/IdentityProvider.h"
#include "Badumna/Dei/IdentityProviderImpl.h"
#include "Badumna/Utils/Optional.h"

namespace Dei
{
    using Badumna::String;
    using Badumna::BadumnaRuntime;

    IdentityProvider::IdentityProvider()
        : impl_(NULL)
    {
    }

    IdentityProvider::IdentityProvider(IdentityProviderImpl *impl)
        : impl_(impl)
    {
    }

    void IdentityProvider::reset(IdentityProviderImpl *impl)
    {
        impl_.reset(impl);
    }

    IdentityProvider::~IdentityProvider()
    {
    }

}
