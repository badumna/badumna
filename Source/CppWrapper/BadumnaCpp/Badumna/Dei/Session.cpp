//---------------------------------------------------------------------------------
// <copyright file="Session.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Dei/Session.h"
#include "Badumna/Dei/SessionImpl.h"
#include "Badumna/Utils/Optional.h"

namespace Dei
{
    using Badumna::String;
    using Badumna::BadumnaRuntime;

    Session::Session(String const &server_host, int server_port, String const &string_xml)
        : impl_(BadumnaRuntime::Instance().CreateSessionImpl(server_host, server_port, &string_xml))
    {
    }

    Session::Session(String const &server_host, int server_port)
        : impl_(BadumnaRuntime::Instance().CreateSessionImpl(server_host, server_port, NULL))
    {
    }

    Session::~Session()
    {
    }

    void Session::Dispose()
    {
        impl_->Dispose();
    }

    String Session::GenerateKeyPair()
    {
        return SessionImpl::GenerateKeyPair();
    }

    void Session::SetUseSslConnection(bool useSsl)
    {
        impl_->SetUseSslConnection(useSsl);
    }

    void Session::SetIgnoreSslErrors()
    {
        impl_->SetIgnoreSslErrors();
    }

    void Session::SetIgnoreSslCertificateNameMismatch()
    {
        impl_->SetIgnoreSslCertificateNameMismatch();
    }

    void Session::SetIgnoreSslSelfSignedCertificateErrors()
    {
        impl_->SetIgnoreSslSelfSignedCertificateErrors();
    }

    LoginResult Session::Authenticate(String const &username, String const &password) const
    {
        return impl_->Authenticate(username, password);
    }

    LoginResult Session::SelectIdentity(Badumna::Character character, IdentityProvider &identityProvider) const
    {
        return impl_->SelectIdentity(character, identityProvider);
    }

    std::auto_ptr<Badumna::Character> Session::CreateCharacter(Badumna::String const &character_name) const
    {
        return impl_->CreateCharacter(character_name);
    }

    bool Session::DeleteCharacter(Badumna::Character const &character) const
    {
        return impl_->DeleteCharacter(character);
    }

    std::vector<Badumna::Character> Session::ListCharacters() const
    {
        return impl_->ListCharacters();
    }
}
