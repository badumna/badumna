//---------------------------------------------------------------------------------
// <copyright file="SessionImpl.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include <iostream>
#include <vector>

#include "Badumna/Core/TypeRegistry.h"
#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/Core/ManagedObject.h"
#include "Badumna/Core/Arguments.h"
#include "Badumna/Dei/IdentityProvider.h"
#include "Badumna/Dei/SessionImpl.h"
#include "Badumna/Security/Character.h"
#include "Badumna/Utils/Optional.h"
#include "Badumna/Utils/Logger.h"

namespace Dei
{
    using Badumna::String;
    using Badumna::Arguments;
    using Badumna::Character;
    using Badumna::Optional;
    using Badumna::ManagedObject;
    using Badumna::MonoRuntime;
    using Badumna::BadumnaRuntime;

    SessionImpl::SessionImpl(
            MonoRuntime *runtime,
            String const &server_host,
            int server_port,
            String const *string_xml)
        : ProxyObject(runtime)
    {
        assert(server_port > 0 && server_port < 65536);

        DotNetClass klass = runtime->GetClass("Dei", "Session");
        DotNetString host = MonoRuntime::GetString(server_host);
        int16_t short_port = static_cast<int16_t>(server_port);
        
        Arguments args;
        args.AddArgument(host);
        args.AddArgument(&short_port);
        
        if(string_xml != NULL)
        {
            DotNetString xml = MonoRuntime::GetString(*string_xml);
            args.AddArgument(xml);
        }

        // progressHandler:
        args.AddNullArgument();

        DotNetObject obj = runtime->CreateObject(klass, args, NULL);
        managed_object_.Initialize(obj, "Dei", "Session");
    }

    SessionImpl::~SessionImpl()
    {
        Dispose();
    }

    void SessionImpl::Dispose()
    {
        managed_object_.InvokeMethod("Dispose");
    }

    String SessionImpl::GenerateKeyPair()
    {
        return BadumnaRuntime::Instance().GenerateKeyPair();
    }

    void SessionImpl::SetUseSslConnection(bool useSsl)
    {
        BADUMNA_LOG_DEBUG("SetUseSslConnection(" << useSsl << ")");
        managed_object_.SetPropertyValue("UseSslConnection", useSsl);
    }

    void SessionImpl::SetIgnoreSslErrors()
    {
        managed_object_.SetPropertyValue("IgnoreSslErrors", true);
    }

    void SessionImpl::SetIgnoreSslCertificateNameMismatch()
    {
        managed_object_.SetPropertyValue("IgnoreSslCertificateNameMismatch", true);
    }

    void SessionImpl::SetIgnoreSslSelfSignedCertificateErrors()
    {
        managed_object_.SetPropertyValue("IgnoreSslSelfSignedCertificateErrors", true);
    }

    LoginResult SessionImpl::Authenticate(String const &username, String const &password) const
    {
        Arguments args;
        args.AddArgument(MonoRuntime::GetString(username));
        args.AddArgument(MonoRuntime::GetString(password));

        DotNetObject ret = managed_object_.InvokeMethod("Authenticate", args);
        return this->ConvertLoginResult(ret);
    }

    LoginResult SessionImpl::SelectIdentity(Character const &character, IdentityProvider &identityProvider)
    {
        Arguments args;
        if(character == Character::None)
        {
            args.AddNullArgument();
        }
        else
        {
            DotNetObject character_object = BadumnaRuntime::Instance().ConvertCharacter(character);
            args.AddArgument(character_object);
        }

        DotNetObject identityProvider_dno;
        args.AddArgument(&identityProvider_dno);

        DotNetObject ret = managed_object_.InvokeMethod("SelectIdentity", args);
        LoginResult result = this->ConvertLoginResult(ret);
        if(result.WasSuccessful())
        {
            identityProvider.reset(BadumnaRuntime::Instance().CreateIdentityProviderImpl(identityProvider_dno));
        }
        return result;
    }

    std::auto_ptr<Badumna::Character> SessionImpl::CreateCharacter(Badumna::String const &character_name) const
    {
        Arguments args;
        args.AddArgument(MonoRuntime::GetString(character_name));

        DotNetObject character_dno = managed_object_.InvokeMethod("CreateCharacter", args);
        return BadumnaRuntime::Instance().GetMaybeCharacter(character_dno);
    }

    bool SessionImpl::DeleteCharacter(Badumna::Character const &character) const
    {
        Arguments args;
        args.AddArgument(BadumnaRuntime::Instance().ConvertCharacter(character));
        return managed_object_.InvokeMethod<bool>("DeleteCharacter", args);
    }

    std::vector<Badumna::Character> SessionImpl::ListCharacters() const
    {
        DotNetObject characters = managed_object_.InvokeMethod("ListCharacters");
        return this->ConvertCharacterList(characters);
    }

    LoginResult SessionImpl::ConvertLoginResult(DotNetObject const &obj) const
    {
        ManagedObject managed_login_result(managed_object_.GetMonoRuntime(), obj);

        bool success = managed_login_result.GetPropertyFromName<bool>("WasSuccessful");
        String desc = managed_login_result.GetPropertyFromName<String>("ErrorDescription");
        
        DotNetObject characters_dno = managed_login_result.GetPropertyFromName("Characters");

        std::vector<Character> characters = this->ConvertCharacterList(characters_dno);
        return LoginResult(success, desc, characters);
    }

    std::vector<Character> SessionImpl::ConvertCharacterList(DotNetObject const &characters_dno) const
    {
        BadumnaRuntime &badumna_runtime = BadumnaRuntime::Instance();
        std::vector<Character> characters;
        if(characters_dno != NULL)
        {
            ManagedObject characters_m(managed_object_.GetMonoRuntime(), characters_dno);
            int length = characters_m.GetPropertyFromName<int>("Count");
            BADUMNA_LOG_DEBUG("Creating character list of size " << length);
            for (int i=0; i<length; i++)
            {
                Arguments indexArg;
                indexArg.AddArgument(&i);
                DotNetObject character = characters_m.GetPropertyFromName("Item", indexArg, NULL);
                characters.push_back(badumna_runtime.GetCharacter(character));
            }
        }
        else
        {
            BADUMNA_LOG_DEBUG("Empty character list");
        }
        return characters;
    }
}
