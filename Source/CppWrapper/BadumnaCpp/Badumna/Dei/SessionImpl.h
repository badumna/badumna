//---------------------------------------------------------------------------------
// <copyright file="SessionImpl.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_DEI_SESSION_IMPL_H
#define BADUMNA_DEI_SESSION_IMPL_H

#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Core/MonoRuntime.h"
#include "Badumna/Core/ProxyObject.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Dei/LoginResult.h"
#include "Badumna/Security/Character.h"
#include "Badumna/Utils/Optional.h"

// forward declaration
namespace Badumna
{
    class BadumnaRuntime;
}

namespace Dei
{
    class IdentityProvider;
    class SessionImpl : public Badumna::ProxyObject
    {
        friend class Badumna::BadumnaRuntime;
    public:
        ~SessionImpl();
        void Dispose();
        static Badumna::String GenerateKeyPair();

        void SetUseSslConnection(bool useSsl);
        void SetIgnoreSslErrors();
        void SetIgnoreSslCertificateNameMismatch();
        void SetIgnoreSslSelfSignedCertificateErrors();
        

        LoginResult Authenticate(Badumna::String const &username, Badumna::String const &password) const;
        LoginResult SelectIdentity(Badumna::Character const &character, IdentityProvider &identityProvider);
        std::auto_ptr<Badumna::Character> CreateCharacter(Badumna::String const &character_name) const;
        bool DeleteCharacter(Badumna::Character const &character) const;
        std::vector<Badumna::Character> ListCharacters() const;

    private:
        SessionImpl(
            Badumna::MonoRuntime *runtime,  
            Badumna::String const &server_host, 
            int server_port,
            Badumna::String const *string_xml);

        std::vector<Badumna::Character> ConvertCharacterList(DotNetObject const &characters_dno) const;

        LoginResult ConvertLoginResult(DotNetObject const &obj) const;

        DISALLOW_COPY_AND_ASSIGN(SessionImpl);
    };
}

#endif // BADUMNA_DEI_SESSION_IMPL_H
