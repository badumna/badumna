//---------------------------------------------------------------------------------
// <copyright file="LoginResult.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_LOGIN_RESULT_H
#define BADUMNA_LOGIN_RESULT_H
#include <vector>

#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/Security/Character.h"

namespace Dei
{
    /**
     * @class LoginResult
     * @brief The result of Dei server login attempt
     *
     * A sumary of the result of the login process.
     */
    class BADUMNA_API LoginResult
    {
        friend class SessionImpl;
    public:
        /**
         * Copy constructor that constructs a LoginResult object from another LoginResult object.
         *
         * @param other Another LoginResult object.
         */
        LoginResult(LoginResult const &other)
            : was_successful_(other.WasSuccessful()),
            error_description_(other.GetErrorDescription()),
            characters_(other.Characters())
        {
        }

        /**
         * Assignment operator.
         *
         * @param rhs The right hand side parameter of the assignment operation. 
         * @return A reference to this LoginResult object.
         */
        LoginResult &operator=(LoginResult const &rhs)
        {
            if(this != &rhs)
            {
                was_successful_ = rhs.WasSuccessful();
                error_description_ = rhs.GetErrorDescription();
                characters_ = rhs.Characters();
            }

            return *this;
        }

        /**
         * Gets a boolean value indicating whether the login was successful.
         *
         * @return A bool value indicating whether the login was successful.
         */
        bool WasSuccessful() const
        {
            return was_successful_;
        }

        /**
         * Gets the error description.
         *
         * @return A string that contains the error description.
         */
        Badumna::String GetErrorDescription() const
        {
            return error_description_;
        }

        /**
         * Gets the user's list of characters
         *
         * @return An vector of characters.
         *
         * The return value is owned by the LoginResult instance - it
         * will be deleted when this instance is destructed.
         */
        std::vector<Badumna::Character> Characters() const
        {
            return characters_;
        }

    private:
        LoginResult(bool successful, Badumna::String description, std::vector<Badumna::Character> characters)
            : was_successful_(successful),
            error_description_(description),
            characters_(characters)
        {
        }

        bool was_successful_;
        Badumna::String error_description_;
        std::vector<Badumna::Character> characters_;
    };
}

#endif // BADUMNA_LOGIN_RESULT_H