//---------------------------------------------------------------------------------
// <copyright file="ArbitrationClientImpl.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include "Badumna/Arbitration/ArbitrationClientImpl.h"
#include "Badumna/Core/TypeRegistry.h"

namespace Badumna
{
    REGISTER_MANAGED_MONO_TYPE(CppWrapperStub, ArbitrationClientStub);
    ArbitrationClientImpl::ArbitrationClientImpl(
        MonoRuntime *runtime, 
        String const &id, 
        DotNetObject object, 
        ArbitrationServiceManager *manager)
        : ProxyObject(runtime, object, "Badumna.CppWrapperStub", "ArbitrationClientStub"),
        arbitration_manager_(manager),
        unique_id_(id)
    {
        assert(manager != NULL);
    }

    String ArbitrationClientImpl::Id() const
    {
        return unique_id_;
    }

    REGISTER_PROPERTY(CppWrapperStub, ArbitrationClientStub, IsServerConnected)
    bool ArbitrationClientImpl::IsServerConnected()
    {
        return managed_object_.GetPropertyFromName<bool>("IsServerConnected");
    }
        
    REGISTER_MANAGED_METHOD(CppWrapperStub, ArbitrationClientStub, Connect, 0)
    void ArbitrationClientImpl::Connect(
        ArbitrationConnectionDelegate connection_delegate,
        ArbitrationConnectionFailureDelegate connection_failure_delegate,
        ArbitrationServerMessageDelegate server_message_delegate)
    {
        arbitration_manager_->SetClientDelegates(
            unique_id_, 
            connection_delegate,
            connection_failure_delegate,
            server_message_delegate);

        managed_object_.InvokeMethod("Connect");
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, ArbitrationClientStub, SendEvent, 1)
    void ArbitrationClientImpl::SendEvent(OutputStream *stream) const
    {
        assert(stream != NULL);

        Arguments args;
        DotNetArray data = MonoRuntime::OutputStreamToMonoArray(stream);
        assert(data != NULL);
        args.AddArgument(data);

        managed_object_.InvokeMethod("SendEvent", args);
    }
}