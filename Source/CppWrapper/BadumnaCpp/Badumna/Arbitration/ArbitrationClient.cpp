//---------------------------------------------------------------------------------
// <copyright file="ArbitrationClient.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include "Badumna/Arbitration/ArbitrationClientImpl.h"
#include "Badumna/Arbitration/ArbitrationClient.h"

namespace Badumna
{
    ArbitrationClient::ArbitrationClient(ArbitrationClientImpl *pimpl)
        : impl_(pimpl)
    {
        assert(pimpl != NULL);
    }

    ArbitrationClient::~ArbitrationClient()
    {
    }

    bool ArbitrationClient::IsServerConnected() const
    {
        return impl_->IsServerConnected();
    }

    void ArbitrationClient::Connect(
        ArbitrationConnectionDelegate connection_delegate,
        ArbitrationConnectionFailureDelegate connection_failure_delegate,
        ArbitrationServerMessageDelegate server_message_delegate)
    {
        impl_->Connect(connection_delegate, connection_failure_delegate, server_message_delegate);
    }

    void ArbitrationClient::SendEvent(OutputStream *stream) const
    {
        assert(stream != NULL);
        impl_->SendEvent(stream);
    }
}