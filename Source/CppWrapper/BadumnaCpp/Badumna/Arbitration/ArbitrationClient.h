//---------------------------------------------------------------------------------
// <copyright file="ArbitrationClient.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_ARBITRATION_CLIENT_H
#define BADUMNA_ARBITRATION_CLIENT_H

#include <memory>

#include "Badumna/Arbitration/IArbitrator.h"
#include "Badumna/DataTypes/OutputStream.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class ArbitrationClientImpl;
    
    class BADUMNA_API ArbitrationClient : public IArbitrator
    {
        friend class NetworkFacadeImpl;
    public:
        ~ArbitrationClient();

        bool IsServerConnected() const;
        
        void Connect(
            ArbitrationConnectionDelegate connection_delegate,
            ArbitrationConnectionFailureDelegate connection_failure_delegate,
            ArbitrationServerMessageDelegate server_message_delegate);

        void SendEvent(OutputStream *stream) const;

    private:
        ArbitrationClient(ArbitrationClientImpl *impl);

        std::auto_ptr<ArbitrationClientImpl> const impl_;
        DISALLOW_COPY_AND_ASSIGN(ArbitrationClient);
    };
}

#endif // BADUMNA_ARBITRATION_CLIENT_H