//---------------------------------------------------------------------------------
// <copyright file="ArbitrationServerImpl.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_ARBITRATION_SERVER_IMPL_H
#define BADUMNA_ARBITRATION_SERVER_IMPL_H

#include "Badumna/Arbitration/ArbitrationDelegates.h"
#include "Badumna/Arbitration/ArbitrationServiceManager.h"
#include "Badumna/Core/ProxyObject.h"
#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class ArbitrationServerImpl : public ProxyObject
    {
        friend class BadumnaRuntime;
    public:
        String Id() const;

        void Register(
            ArbitrationClientMessageDelegate client_message_delegate,
            ArbitrationClientDisconnectDelegate client_disconnect_delegate,
            int32_t timeout_seconds);

    private:
        ArbitrationServerImpl(
            MonoRuntime *runtime, 
            String const &id, 
            DotNetObject object, 
            ArbitrationServiceManager *manager);

        ArbitrationServiceManager * const arbitration_manager_;
        String unique_id_;

        DISALLOW_COPY_AND_ASSIGN(ArbitrationServerImpl);
    };
}

#endif // BADUMNA_ARBITRATION_SERVER_IMPL_H
