//---------------------------------------------------------------------------------
// <copyright file="ArbitrationServiceManager.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_ARBITRATION_SERVICE_MANAGER_H
#define BADUMNA_ARBITRATION_SERVICE_MANAGER_H

#include "Badumna/Core/BadumnaMap.h"
#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/Arbitration/ArbitrationDelegates.h"

namespace Badumna
{
    class ArbitrationServiceManager
    {
    public:
        ArbitrationServiceManager();

        // client side
        void SetClientDelegates(
            UniqueId const &id,
            ArbitrationConnectionDelegate connection_delegate,
            ArbitrationConnectionFailureDelegate connection_failure_delegate,
            ArbitrationServerMessageDelegate server_message_delegate);

        void InvokeConnectionDelegate(UniqueId const &id, ServiceConnectionResultType type) const;
        void InvokeConnectionFailureDelegate(UniqueId const &id) const;
        void InvokeServerMessageDelegate(UniqueId const &id, InputStream *stream) const;

        // server side
        void SetServerDelegates(
            UniqueId const &id,
            ArbitrationClientMessageDelegate client_message_delegate,
            ArbitrationClientDisconnectDelegate client_disconnect_delegate);

        void InvokeClientMessageDelegate(UniqueId const &id, int session_id, InputStream *stream) const;
        void InvokeClientDisconnectDelegate(UniqueId const &id, int session_id) const; 

    private:
        typedef BadumnaMap<ArbitrationConnectionDelegate> ConnectionDelegateMap;
        typedef BadumnaMap<ArbitrationConnectionFailureDelegate> ConnectionFailureDelegateMap;
        typedef BadumnaMap<ArbitrationServerMessageDelegate> ServerMessageDelegateMap;
        typedef BadumnaMap<ArbitrationClientMessageDelegate> ClientMessageDelegateMap;
        typedef BadumnaMap<ArbitrationClientDisconnectDelegate> ClientDisconnectDelegateMap;

        // client side
        ConnectionDelegateMap connection_delegate_map_;
        ConnectionFailureDelegateMap connection_failure_delegate_map_;
        ServerMessageDelegateMap server_message_delegate_map_;

        // server side
        ClientMessageDelegateMap client_message_delegate_map_;
        ClientDisconnectDelegateMap client_disconnect_delegate_map_;

        DISALLOW_COPY_AND_ASSIGN(ArbitrationServiceManager);
    };
}

#endif // BADUMNA_ARBITRATION_SERVICE_MANAGER_H