//---------------------------------------------------------------------------------
// <copyright file="ArbitrationDelegates.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_ARBITRATION_DELEGATES_H
#define BADUMNA_ARBITRATION_DELEGATES_H

#include "Badumna/Arbitration/ServiceConnectionResultType.h"
#include "Badumna/Core/Delegate.h"
#include "Badumna/DataTypes/InputStream.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    //
    // client side delegates
    //

    /**
     * @class ArbitrationConnectionDelegate
     * @brief The delegate for handling arbitration connection results.
     *
     * A delegate for handling arbitration connection results. The delegate has the following signature:\n
     * \n
     * <code>void function_name(ServiceConnectionResultType type);</code>\n
     * \n
     * It can be a member function or a free function. 
     */
    class BADUMNA_API ArbitrationConnectionDelegate : public Delegate1<void, ServiceConnectionResultType>
    {
    public:
        /**
         * Constructor that constructs an ArbitrationConnectionDelegate with specified pointer to member function and 
         * object. 
         *
         * @tparam K Object type.
         * @param fp A pointer to a member function.
         * @param object The object. 
         */
        template<typename K>
        ArbitrationConnectionDelegate(void (K::*fp)(ServiceConnectionResultType), K& object)
            : Delegate1<void, ServiceConnectionResultType>(fp, object)
        {
        }

        /**
         * Constructor that constructs an ArbitrationConnectionDelegate with specified pointer to free function.
         *
         * @param fp A pointer to a free function.
         */
        ArbitrationConnectionDelegate(void (*fp)(ServiceConnectionResultType))
            : Delegate1<void, ServiceConnectionResultType>(fp)
        {
        }
    };

    /**
     * @class ArbitrationConnectionFailureDelegate
     * @brief The delegate invoked on client when its connection to server fails for any reason.
     *
     * A delegate invoked on client when its connection to server fails for any reason. The delegate has the 
     * following signature:\n
     * \n
     * <code>void function_name();</code>\n
     * \n
     * It can be a member function or a free function. 
     */
    class BADUMNA_API ArbitrationConnectionFailureDelegate : public Delegate0<void>
    {
    public:
        /**
         * Constructor that constructs an ArbitrationConnectionFailureDelegate with specified pointer to member function 
         * and the object. 
         *
         * @tparam K Object type.
         * @param fp A pointer to a member function.
         * @param object The object. 
         */
        template<typename K>
        ArbitrationConnectionFailureDelegate(void (K::*fp)(), K& object)
            : Delegate0<void>(fp, object)
        {
        }

        /**
         * Constructor that constructs an ArbitrationConnectionFailureDelegate with specified pointer to free function.
         *
         * @param fp A pointer to a free function.
         */
        ArbitrationConnectionFailureDelegate(void (*fp)())
            : Delegate0<void>(fp)
        {
        }
    };

    /**
     * @class ArbitrationServerMessageDelegate
     * @brief The delegate invoked on client that processes messages received from arbitrator.
     *
     * A delegate invoked on client that processes messages received from arbitrator. The delegate has the 
     * following signature:\n
     * \n
     * <code>void function_name(InputStream *stream);</code>\n
     * \n
     * It can be a member function or a free function. 
     */
    class BADUMNA_API ArbitrationServerMessageDelegate : public Delegate1<void, InputStream *>
    {
    public:
        /**
         * Constructor that constructs an ArbitrationServerMessageDelegate with specified pointer to member function 
         * and the object. 
         *
         * @tparam K Object type.
         * @param fp A pointer to a member function.
         * @param object The object. 
         */
        template<typename K>
        ArbitrationServerMessageDelegate(void (K::*fp)(InputStream *), K& object)
            : Delegate1<void, InputStream *>(fp, object)
        {
        }

        /**
         * Constructor that constructs an ArbitrationServerMessageDelegate with specified pointer to free function.
         *
         * @param fp A pointer to a free function.
         */
        ArbitrationServerMessageDelegate(void (*fp)(InputStream *))
            : Delegate1<void, InputStream *>(fp)
        {
        }
    };

    //
    // server side delegates
    //

    /**
     * @class ArbitrationClientMessageDelegate
     * @brief The delegate invoked on arbitrator that processes messages received from clients.
     *
     * A delegate invoked on arbitrator that processes messages received from clients. The delegate has the 
     * following signature:\n
     * \n
     * <code>void function_name(int32_t session_id, InputStream *stream);</code>\n
     * \n
     * It can be a member function or a free function. 
     */
    class BADUMNA_API ArbitrationClientMessageDelegate : public Delegate2<void, int32_t, InputStream *>
    {
    public:
        /**
         * Constructor that constructs an ArbitrationClientMessageDelegate with specified pointer to member function 
         * and the object. 
         *
         * @tparam K Object type.
         * @param fp A pointer to a member function.
         * @param object The object. 
         */
        template<typename K>
        ArbitrationClientMessageDelegate(void (K::*fp)(int32_t, InputStream *), K& object)
            : Delegate2<void, int32_t, InputStream *>(fp, object)
        {
        }

        /**
         * Constructor that constructs an ArbitrationClientMessageDelegate with specified pointer to free function.
         *
         * @param fp A pointer to a free function.
         */
        ArbitrationClientMessageDelegate(void (*fp)(int, InputStream *))
            : Delegate2<void, int32_t, InputStream *>(fp)
        {
        }
    };

    /**
     * @class ArbitrationClientDisconnectDelegate
     * @brief The delegate invoked on arbitrator when a client disconnects or times out.
     *
     * A delegate invoked on arbitrator when a client disconnects or times out. The delegate has the following 
     * signature:\n
     * \n
     * <code>void function_name(int32_t session_id);</code>\n
     * \n
     * It can be a member function or a free function. 
     */
    class BADUMNA_API ArbitrationClientDisconnectDelegate : public Delegate1<void, int32_t>
    {
    public:
        /**
         * Constructor that constructs an ArbitrationClientDisconnectDelegate with specified pointer to member function 
         * and the object. 
         *
         * @tparam K Object type.
         * @param fp A pointer to a member function.
         * @param object The object. 
         */
        template<typename K>
        ArbitrationClientDisconnectDelegate(void (K::*fp)(int32_t), K& object)
            : Delegate1<void, int32_t>(fp, object)
        {
        }

        /**
         * Constructor that constructs an ArbitrationClientDisconnectDelegate with specified pointer to free function.
         *
         * @param fp A pointer to a free function.
         */
        ArbitrationClientDisconnectDelegate(void (*fp)(int32_t))
            : Delegate1<void, int32_t>(fp)
        {
        }
    };
}

#endif // BADUMNA_ARBITRATION_DELEGATES_H