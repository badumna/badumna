//---------------------------------------------------------------------------------
// <copyright file="ArbitrationServerImpl.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include "Badumna/Core/Arguments.h"
#include "Badumna/Core/TypeRegistry.h"
#include "Badumna/Arbitration/ArbitrationServerImpl.h"

namespace Badumna
{
    REGISTER_MANAGED_MONO_TYPE(CppWrapperStub, ArbitrationServerStub);
    ArbitrationServerImpl::ArbitrationServerImpl(
        MonoRuntime *runtime, 
        String const &id, 
        DotNetObject object, 
        ArbitrationServiceManager *manager)
        : ProxyObject(runtime, object, "Badumna.CppWrapperStub", "ArbitrationServerStub"),
        arbitration_manager_(manager),
        unique_id_(id)
    {
        assert(manager != NULL);
    }

    String ArbitrationServerImpl::Id() const
    {
        return unique_id_;
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, ArbitrationServerStub, Register, 1)
    void ArbitrationServerImpl::Register(
        ArbitrationClientMessageDelegate client_message_delegate,
        ArbitrationClientDisconnectDelegate client_disconnect_delegate,
        int32_t timeout_seconds)
    {
        arbitration_manager_->SetServerDelegates(unique_id_, client_message_delegate, client_disconnect_delegate);

        Arguments args;
        args.AddArgument(&timeout_seconds);
        managed_object_.InvokeMethod("Register", args);
    }
}