//---------------------------------------------------------------------------------
// <copyright file="ArbitrationServiceManager.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include "Badumna/Arbitration/ArbitrationServiceManager.h"

namespace Badumna
{
    ArbitrationServiceManager::ArbitrationServiceManager()
        : connection_delegate_map_(),
        connection_failure_delegate_map_(),
        server_message_delegate_map_(),
        client_message_delegate_map_(),
        client_disconnect_delegate_map_()
    {
    }

    void ArbitrationServiceManager::SetClientDelegates(
        UniqueId const &id,
        ArbitrationConnectionDelegate connection_delegate,
        ArbitrationConnectionFailureDelegate connection_failure_delegate,
        ArbitrationServerMessageDelegate server_message_delegate)
    {
        connection_delegate_map_.AddOrReplace(id, connection_delegate);
        connection_failure_delegate_map_.AddOrReplace(id, connection_failure_delegate);
        server_message_delegate_map_.AddOrReplace(id, server_message_delegate);
    }

    void ArbitrationServiceManager::InvokeConnectionDelegate(UniqueId const &id, ServiceConnectionResultType type) const
    {
        connection_delegate_map_.InvokeDelegate(id, type);
    }
    
    void ArbitrationServiceManager::InvokeConnectionFailureDelegate(UniqueId const &id) const
    {
        connection_failure_delegate_map_.InvokeDelegate(id);
    }
    
    void ArbitrationServiceManager::InvokeServerMessageDelegate(UniqueId const &id, InputStream *stream) const
    {
        assert(stream != NULL);
        server_message_delegate_map_.InvokeDelegate(id, stream);
    }

    void ArbitrationServiceManager::SetServerDelegates(
        UniqueId const &id,
        ArbitrationClientMessageDelegate client_message_delegate,
        ArbitrationClientDisconnectDelegate client_disconnect_delegate)
    {
        client_message_delegate_map_.AddOrReplace(id, client_message_delegate);
        client_disconnect_delegate_map_.AddOrReplace(id, client_disconnect_delegate);
    }

    void ArbitrationServiceManager::InvokeClientMessageDelegate(
        UniqueId const &id, 
        int session_id, 
        InputStream *stream) const
    {
        assert(stream != NULL);
        client_message_delegate_map_.InvokeDelegate(id, session_id, stream);
    }

    void ArbitrationServiceManager::InvokeClientDisconnectDelegate(UniqueId const &id, int session_id) const
    {
        client_disconnect_delegate_map_.InvokeDelegate(id, session_id);
    }
}