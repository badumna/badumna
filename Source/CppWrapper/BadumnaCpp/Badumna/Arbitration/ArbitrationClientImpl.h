//---------------------------------------------------------------------------------
// <copyright file="ArbitrationClientImpl.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_ARBITRATION_CLIENT_IMPL_H
#define BADUMNA_ARBITRATION_CLIENT_IMPL_H

#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Core/ProxyObject.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Arbitration/IArbitrator.h"
#include "Badumna/Arbitration/ArbitrationServiceManager.h"

#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class ArbitrationClientImpl : public ProxyObject
    {
        friend class BadumnaRuntime;
    public:
        String Id() const;

        bool IsServerConnected();
        
        void Connect(
            ArbitrationConnectionDelegate connection_delegate,
            ArbitrationConnectionFailureDelegate connection_failure_delegate,
            ArbitrationServerMessageDelegate server_message_delegate);

        void SendEvent(OutputStream *stream) const;

    private:
        ArbitrationClientImpl(
            MonoRuntime *runtime, 
            String const &id, 
            DotNetObject object, 
            ArbitrationServiceManager *manager);

        // doesn't own this object
        ArbitrationServiceManager * const arbitration_manager_;
        String unique_id_;

        DISALLOW_COPY_AND_ASSIGN(ArbitrationClientImpl);
    };
}

#endif // BADUMNA_ARBITRATION_CLIENT_IMPL_H