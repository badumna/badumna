//---------------------------------------------------------------------------------
// <copyright file="IArbitrator.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_IARBITRATOR_H
#define BADUMNA_IARBITRATOR_H

#include "Badumna/DataTypes/OutputStream.h"
#include "Badumna/Arbitration/ArbitrationDelegates.h"

namespace Badumna
{
    /**
     * @class IArbitrator
     *
     * @brief IArbitrator represents a session with an arbitrator.
     */
    class BADUMNA_API IArbitrator 
    {
    public:
        /**
         * Destructor
         */
        virtual ~IArbitrator() {}

        /**
         * Gets a boolean value indicating whether the connection to the server has been established.
         *
         * @return A boolean value indicating whether the connection to the server has been established.
         */
        virtual bool IsServerConnected() const = 0;
        
        /**
         * Initiate the connection to the arbitration server.
         *
         * @param connection_delegate A callback that will be called to report the result of the connection attempt.
         * @param connection_failure_delegate A callback that will be called if an established connection fails.
         * @param server_message_delegate A callback to be called to handle messages received from the arbitration server.
         *
         * @remarks \a connectionResultHandler is guaranteed to be called to deliver the result of the connection attempt (success or failure). \a connectionFailedHandler will only be called if a connection subsequently fails after \a connectionResultHandler has already reported that it was successfully established.
         */
        virtual void Connect(
            ArbitrationConnectionDelegate connection_delegate,
            ArbitrationConnectionFailureDelegate connection_failure_delegate,
            ArbitrationServerMessageDelegate server_message_delegate) = 0;

        /**
         * Send an event to the arbitrator. This will cause the arbitrator's HandleClientMessage handler to be called 
         * with the given message and a client_id that identifies this client.
         *
         * @param stream The data stream to be sent.
         */
        virtual void SendEvent(OutputStream *stream) const = 0;
    };
}

#endif // BADUMNA_IARBITRATOR_H