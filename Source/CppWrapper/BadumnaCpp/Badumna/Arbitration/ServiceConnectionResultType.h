//---------------------------------------------------------------------------------
// <copyright file="ServiceConnectionResultType.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_SERVICE_CONNECTION_RESULT_TYPE_H
#define BADUMNA_SERVICE_CONNECTION_RESULT_TYPE_H

namespace Badumna
{
    /**
     * @enum ServiceConnectionResultType
     *
     * Arbitration connection result type. 
     */
    enum ServiceConnectionResultType
    {
        ServiceConnection_Success = 0,              /**< Success */
        ServiceConnection_ConnectionTimeout,        /**< Timeout */
        ServiceConnection_ServiceNotAvailavle       /**< Service not available */
    };
}

#endif // BADUMNA_SERVICE_CONNECTION_RESULT_TYPE_H