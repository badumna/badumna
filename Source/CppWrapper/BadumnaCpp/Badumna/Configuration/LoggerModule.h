//---------------------------------------------------------------------------------
// <copyright file="LoggerModule.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_LOGGER_MODULE_H
#define BADUMNA_LOGGER_MODULE_H

#include <memory>

#include "Badumna/Configuration/LoggerType.h"
#include "Badumna/Configuration/LogLevel.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class LoggerModuleImpl;
    
    /**
     * @class LoggerModule
     * @brief Configuration options for the logging system.
     *
     * Configuration options for the logging system. Logging is only available in trace builds of the Badumna DLL. In 
     * normal builds these options will have no effect.
     */
    class BADUMNA_API LoggerModule
    {
        friend class Options;
    public:
        /**
         * Destructor
         */
        ~LoggerModule();

        /**
         * Enable default trace.
         */
        void EnableDotNetTraceForwarding();

        /**
         * Gets a value indicating whether the messages sent to the .NET Trace system should be forwarded to Badumna's 
         * logging system. Defaults to false.
         * @return A value indicating default trace is enabled.
         */
        bool IsDotNetTraceForwardingEnabled() const;

        /**
         * Suppresses the .NET's assert dialogs.
         */
        void SuppressAssertUi();

        /**
         * Gets a value indicating whether .NET's assert dialogs should be suppressed. Defaults to false.
         * @return A value indicating whether .NET's assert dialogs should be suppressed.
         */
        bool IsAssertUiSuppressed() const;

        /**
         * Sets the type of logging system to use.
         * @param type The logging system type to use. 
         */
        void SetLoggerType(LoggerType type);

        /**
         * Gets a value indicating the type of logging system to use. Defaults to DotNetTrace.
         * @return A value indicating the type of logging system to use.
         */
        LoggerType GetLoggerType() const;

        /*
         * Sets a value indicating the minimum severity for messages to be logged.
         * @param level The level to use.
         */
        void SetLogLevel(LogLevel level);

        /*
         * Sets a value indicating the minimum severity for messages to be logged.  Defaults to Warning.
         * @return The current level.
         */
        LogLevel GetLogLevel() const;

        /**
         * Sets configuration options for the logger. 
         * @param config The logger configuration options. 
         */
        void SetLoggerConfig(String const &config);

        /**
         * Gets a value containing configuration options specific to the logger type. Defaults to "<Logger></Logger>".
         * @return The logger configuration options.
         */
        String GetLoggerConfig() const;
    
    private:
        explicit LoggerModule(LoggerModuleImpl *impl);
        std::auto_ptr<LoggerModuleImpl> const impl_;

        DISALLOW_COPY_AND_ASSIGN(LoggerModule);
    };
}

#endif // BADUMNA_LOGGER_MODULE_H