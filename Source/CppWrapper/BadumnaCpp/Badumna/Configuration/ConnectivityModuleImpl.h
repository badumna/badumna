//---------------------------------------------------------------------------------
// <copyright file="ConnectivityModuleImpl.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_CONNECTIVITY_MODULE_IMPL_H
#define BADUMNA_CONNECTIVITY_MODULE_IMPL_H

#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Core/ProxyObject.h"
#include "Badumna/Configuration/TunnelMode.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class ConnectivityModuleImpl : public ProxyObject
    {
        friend class BadumnaRuntime;
    public:
        void SetApplicationName(String const &app_name);
        String GetApplicationName() const;

        bool IsTransportLimiterEnabled() const;

        int GetBandwidthLimit() const;
        void SetBadumnaLimit(int limit);
        
        void AddStunServer(String const &address);
        String GetStunServers() const;

        void ConfigureForSpecificPort(int port);

        int GetStartPortRange() const;
        void SetStartPortRange(int start_port);
        int GetEndPortRange() const;
        void SetEndPortRange(int end_port);

        int GetMaxPortsToTry() const;
        void SetMaxPortsToTry(int num);

        bool IsPortForwardingEnabled() const;
        void EnablePortForwarding();

        bool IsBroadcastEnabled() const;
        void EnableBroadcast();
        int GetBroadcastPort() const;
        void SetBroadcastPort(int port);

        TunnelMode GetTunnelMode() const;
        void SetTunnelMode(TunnelMode mode);
        
        void AddTunnelUri(String const &uri);
        String GetTunnelUris() const;

        bool IsHostedService() const;
        void EnableHostedService();

        void AddSeedPeer(String const &address);
        String GetSeedPeers() const;

        void EnableTransportLimiter(String const &testing_only_acknowledgement);
        void DisableTransportLimiter();

        void ConfigureForLan();

    private:
        ConnectivityModuleImpl(MonoRuntime *runtime, DotNetObject object);

        DISALLOW_COPY_AND_ASSIGN(ConnectivityModuleImpl);
    };
}

#endif // BADUMNA_CONNECTIVITY_MODULE_IMPL_H