//---------------------------------------------------------------------------------
// <copyright file="LoggerModule.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include "Badumna/Configuration/LoggerModule.h"
#include "Badumna/Configuration/LoggerModuleImpl.h"

namespace Badumna
{
    LoggerModule::LoggerModule(LoggerModuleImpl *imp)
        : impl_(imp)
    {
        assert(imp != NULL);
    }

    LoggerModule::~LoggerModule()
    {
    }

    bool LoggerModule::IsDotNetTraceForwardingEnabled() const
    {
        return impl_->IsDotNetTraceForwardingEnabled();
    }

    void LoggerModule::EnableDotNetTraceForwarding()
    {
        impl_->SetDotNetTraceForwardingEnabled();
    }

    bool LoggerModule::IsAssertUiSuppressed() const
    {
        return impl_->IsAssertUiSuppressed();
    }

    void LoggerModule::SuppressAssertUi()
    {
        impl_->SetAssertUiSuppressed();
    }

    LoggerType LoggerModule::GetLoggerType() const
    {
        return impl_->GetLoggerType();
    }

    void LoggerModule::SetLoggerType(LoggerType type)
    {
        impl_->SetLoggerType(type);
    }

    LogLevel LoggerModule::GetLogLevel() const
    {
        return impl_->GetLogLevel();
    }

    void LoggerModule::SetLogLevel(LogLevel level)
    {
        impl_->SetLogLevel(level);
    }

    String LoggerModule::GetLoggerConfig() const
    {
        return impl_->GetLoggerConfig();
    }

    void LoggerModule::SetLoggerConfig(String const &config)
    {
        impl_->SetLoggerConfig(config);
    }
}