//---------------------------------------------------------------------------------
// <copyright file="OverloadModule.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_OVERLOAD_MODULE_H
#define BADUMNA_OVERLOAD_MODULE_H

#include <memory>

#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class OverloadModuleImpl;

    /**
     * @class OverloadModule
     * @brief Configuration options for the overload system.
     *
     * Configuration options for the overload system. 
     * For overload servers call SetAsServer to set IsServer true.
     * For overload clients call SetClientEnabled to set IsClientEnabled to true and optionally call SetServerAddress to
     * set overload server's address. 
     */
    class BADUMNA_API OverloadModule
    {
        friend class Options;
    public:
        /**
         * Destructor
         */
        ~OverloadModule();

        /**
         * Sets the server address.
         * @param address The server address.
         */
        void SetServerAddress(String const &address);

        /**
         * Gets the server address.
         */
        String GetServerAddress() const;

        /**
         * Set as server.
         */
        void SetAsServer();

        /**
         * Get a boolean value indicating whether the local peer has been set as an overload server.
         * @return A value indicating whether the local peer has been set as an overload server.
         */
        bool IsServer() const;

        /**
         * Sets to let this peer to send its excess outbound traffic via an overload server.
         */
        void SetClientEnabled();

        /**
         * Get a boolean value indicating whether this peer will send its excess outbound traffic via an overload server.
         * Defaults to false.
         * @return Whether the peer will send its excess outbound traffic via an overload server.
         */
        bool IsClientEnabled() const;

        /**
         * Enable the forced overload mode. This is for testing purpose only, the forced overload mode should never be
         * used in production system. 
         */
        void EnableForcedOverload(String const &testing_only_acknowledgement);

        /**
         * Diable the forced overload mode.
         */
        void DisableForcedOverload();

        /**
         * Get a boolean value indicating whether forced overload has been enabled. This is for testing purpose only.
         * @return A boolean value indicating whether forced overload has been enabled.
         */
        bool IsForced() const;

    private:
        explicit OverloadModule(OverloadModuleImpl *impl);
        std::auto_ptr<OverloadModuleImpl> const impl_;

        DISALLOW_COPY_AND_ASSIGN(OverloadModule);
    };
}

#endif // BADUMNA_OVERLOAD_MODULE_H