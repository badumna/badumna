//-----------------------------------------------------------------------
// <copyright file="MatchmakingModuleImpl.cpp" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include <cassert>

#include "Badumna/Core/TypeRegistry.h"
#include "Badumna/Core/Arguments.h"
#include "Badumna/Configuration/MatchmakingModuleImpl.h"

namespace Badumna
{
    REGISTER_MANAGED_MONO_TYPE(Badumna, MatchmakingModule);
    MatchmakingModuleImpl::MatchmakingModuleImpl(MonoRuntime *runtime, DotNetObject object)
        : ProxyObject(runtime, object)
    {
    }
        
    REGISTER_PROPERTY(Badumna, MatchmakingModule, ServerAddress)
    void MatchmakingModuleImpl::SetServerAddress(String const &server_address)
    {
        managed_object_.SetPropertyValue("ServerAddress", server_address);
    }

    String MatchmakingModuleImpl::GetServerAddress()
    {
        return managed_object_.GetPropertyFromName<String>("ServerAddress");
    }

    REGISTER_PROPERTY(Badumna, MatchmakingModule, ActiveMatchLimit)
    void MatchmakingModuleImpl::SetActiveMatchLimit(int limit)
    {
        managed_object_.SetPropertyValue("ActiveMatchLimit", limit);
    }

    int MatchmakingModuleImpl::GetActiveMatchLimit()
    {
        return managed_object_.GetPropertyFromName<int>("ActiveMatchLimit");
    }
}