//---------------------------------------------------------------------------------
// <copyright file="ArbitrationModuleImpl.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_ARBITRATION_MODULE_IMPL_H
#define BADUMNA_ARBITRATION_MODULE_IMPL_H

#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Core/ProxyObject.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class ArbitrationModuleImpl : public ProxyObject
    {
        friend class BadumnaRuntime;
    public:
        void AddServer(String const &name);
        void AddServer(String const &name, String const &address);

        String GetServers() const;

    private:
        DotNetObject GetServersObject() const;
        ArbitrationModuleImpl(MonoRuntime *runtime, DotNetObject object);

        DISALLOW_COPY_AND_ASSIGN(ArbitrationModuleImpl);
    };
}

#endif // BADUMNA_ARBITRATION_MODULE_IMPL_H