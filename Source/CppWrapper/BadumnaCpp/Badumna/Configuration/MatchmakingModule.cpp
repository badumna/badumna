//-----------------------------------------------------------------------
// <copyright file="MatchmakingModule.cpp" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include <cassert>
#include "Badumna/Configuration/MatchmakingModule.h"
#include "Badumna/Configuration/MatchmakingModuleImpl.h"

namespace Badumna
{
    MatchmakingModule::MatchmakingModule(MatchmakingModuleImpl *imp)
        : impl_(imp)
    {
        assert(imp != NULL);
    }

    MatchmakingModule::~MatchmakingModule()
    {
    }

    void MatchmakingModule::SetServerAddress(String const &server_address)
    {
        impl_->SetServerAddress(server_address);
    }

    String MatchmakingModule::GetServerAddress()
    {
        return impl_->GetServerAddress();
    }

    void MatchmakingModule::SetActiveMatchLimit(int limit)
    {
        impl_->SetActiveMatchLimit(limit);
    }

    int MatchmakingModule::GetActiveMatchLimit()
    {
        return impl_->GetActiveMatchLimit();
    }
}
