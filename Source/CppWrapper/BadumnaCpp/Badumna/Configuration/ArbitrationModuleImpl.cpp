//---------------------------------------------------------------------------------
// <copyright file="ArbitrationModuleImpl.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>

#include "Badumna/Core/TypeRegistry.h"
#include "Badumna/Core/Arguments.h"
#include "Badumna/Configuration/ArbitrationModuleImpl.h"

namespace Badumna
{
    REGISTER_MANAGED_MONO_TYPE(Badumna, ArbitrationModule);
    ArbitrationModuleImpl::ArbitrationModuleImpl(MonoRuntime *runtime, DotNetObject object)
        : ProxyObject(runtime, object)
    {
    }
        
    void ArbitrationModuleImpl::AddServer(String const &name)
    {
        ManagedObject servers_object(mono_runtime_, GetServersObject());

        Arguments args;
        args.AddArgument(MonoRuntime::GetString(name));

        DotNetClass klass = mono_runtime_->GetClass("Badumna", "ArbitrationServerDetails");
        assert(klass != NULL);

        DotNetObject server_details = mono_runtime_->CreateObject(klass, args, NULL);
        assert(server_details != NULL);

        Arguments args2;
        args2.AddArgument(server_details);
        servers_object.InvokeMethod("Add", args2);
    }

    void ArbitrationModuleImpl::AddServer(String const &name, String const &address)
    {
        ManagedObject servers_object(mono_runtime_, GetServersObject());

        Arguments args;
        args.AddArgument(MonoRuntime::GetString(name));
        args.AddArgument(MonoRuntime::GetString(address));

        DotNetClass klass = mono_runtime_->GetClass("Badumna", "ArbitrationServerDetails");
        assert(klass != NULL);
        DotNetObject server_details = mono_runtime_->CreateObject(klass, args, NULL);
        assert(server_details != NULL);
        
        Arguments args2;
        args2.AddArgument(server_details);
        servers_object.InvokeMethod("Add", args2);
    }

    String ArbitrationModuleImpl::GetServers() const
    {
        ManagedObject servers_object(mono_runtime_, GetServersObject());
        return servers_object.InvokeVirtualMethod<String>("ToString");
    }

    DotNetObject ArbitrationModuleImpl::GetServersObject() const
    {
        DotNetObject servers_object = managed_object_.GetPropertyFromName("Servers");
        assert(servers_object != NULL);
        return servers_object;
    }
}