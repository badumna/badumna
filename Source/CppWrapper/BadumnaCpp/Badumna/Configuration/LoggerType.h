//---------------------------------------------------------------------------------
// <copyright file="LoggerType.h" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_LOGGER_TYPE_H
#define BADUMNA_LOGGER_TYPE_H

namespace Badumna
{
    /**
     * @enum LoggerType
     *
     * Identifies the different types of logging systems.
     */
    enum LoggerType
    {
        LoggerType_NoLogger = 0,        /**< No logging system. */
        LoggerType_DotNetTrace,         /**< Logging using the .NET Framework's Trace system. */
        LoggerType_UnityTrace,          /**< Logging using Unity's Trace system (no effect in C++). */
        LoggerType_Log4Net,             /**< Logging using Log4Net. */
        LoggerType_Android,             /**< Logging using Android's log system (no effect in C++). */
        LoggerType_Console,             /**< Logging to console. */
        LoggerType_File,                /**< Logging to file. */
    };  
}

#endif // BADUMNA_LOGGER_TYPE_H