//---------------------------------------------------------------------------------
// <copyright file="OverloadModuleImpl.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_OVERLOAD_MODULE_IMPL_H
#define BADUMNA_OVERLOAD_MODULE_IMPL_H

#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Core/ProxyObject.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class OverloadModuleImpl : public ProxyObject
    {
        friend class BadumnaRuntime;
    public:
        String GetServerAddress() const;
        void SetServerAddress(String const &address);

        bool IsServer() const;
        void SetAsServer();

        bool IsClientEnabled() const;
        void SetClientEnabled();

        bool IsForced() const;

        void EnableForcedOverload(String const &testing_only_acknowledgement);
        void DisableForcedOverload();
    private:
        OverloadModuleImpl(MonoRuntime *runtime, DotNetObject object);

        DISALLOW_COPY_AND_ASSIGN(OverloadModuleImpl);
    };
}

#endif // BADUMNA_OVERLOAD_MODULE_IMPL_H