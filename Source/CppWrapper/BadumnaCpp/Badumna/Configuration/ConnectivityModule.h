//---------------------------------------------------------------------------------
// <copyright file="ConnectivityModule.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_CONNECTIVITY_MODULE_H
#define BADUMNA_CONNECTIVITY_MODULE_H

#include <memory>

#include "Badumna/Configuration/TunnelMode.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class ConnectivityModuleImpl;

    /**
     * @class ConnectivityModule
     *
     * @brief Configuration options for connectivity
     *
     * Configuration options for the connectivity system.
     */
    class BADUMNA_API ConnectivityModule
    {
        friend class Options;
    public:
        /**
         * Destructor
         */
        ~ConnectivityModule();

        /**
         * Sets the name uniquely identifying the application. Defaults to the empty string. Only peers initialized with 
         * the same application name will be able to connect to each other. The recommended format is a reverse domain 
         * name style string to ensure uniqueness. e.g. "com.example.epic-elevator-conflict".
         * @param app_name The name to uniquely identifying the application.
         */
        void SetApplicationName(String const &app_name);

        /**
         * Gets the name uniquely identifying the application.
         * @return The name to uniquely identifying the application.
         */
        String GetApplicationName() const;

        /**
         * Enables the transport limiter. The transport limit feature is for testing purpose only.
         * @param testing_only_acknowledgement An string to indicate acknowledgement that this feature is not intended 
         * for use in production.
         */ 
        void EnableTransportLimiter(String const &testing_only_acknowledgement);
        
        /**
         * Disables the transport limiter. The transport limit feature is for testing purpose only.
         */
        void DisableTransportLimiter();
        
        /**
         * Gets a value indicating whether the transport limiter is enabled. The transport limit feature is for testing 
         * purpose only.
         * @return A boolean value indicating whether the transport limiter is enabled.
         */ 
        bool IsTransportLimiterEnabled() const;
        
        /**
         * Sets the bandwidth limit in bytes per second. This is the bandwidth limit used when the transport limiter
         * is enabled. The transport limit feature is for testing purpose only.
         * @param limit The bandwidth limit in bytes per second.
         */
        void SetBadumnaLimit(int limit);

        /**
         * Gets the bandwidth limit in bytes per second. This is the bandwidth limit used when the transport limiter
         * is enabled. The transport limit feature is for testing purpose only.
         * @return the bandwidth limit in bytes per second.
         */
        int GetBandwidthLimit() const;
        
        /**
         * Adds a STUN server.
         * @param address The address of the STUN server.
         */
        void AddStunServer(String const &address);
        
        /**
         * Gets all added STUN servers.
         * @return Details of all added STUN servers.
         */
        String GetStunServers() const;

        /**
         * Sets the UDP port to use.
         * @param port The port to use for Badumna traffic.
         */
        void ConfigureForSpecificPort(int port);
        
        /**
         * Sets the start of the UDP port range to use for Badumna traffic. Defaults to 21300.
         * @param start_port The start of the UDP port range to use for Badumna traffic.
         */
        void SetStartPortRange(int start_port);

        /**
         * Gets the start of the UDP port range to use for Badumna traffic. Defaults to 21300.
         * @return The start of the UDP port range to use for Badumna traffic.
         */
        int GetStartPortRange() const;

        /**
         * Sets the end of the UDP port range to use for Badumna traffic. Defaults to 21399.
         * @param end_port The end of the UDP port range to use for Badumna traffic.
         */
        void SetEndPortRange(int end_port);

        /**
         * Gets the end of the UDP port range to use for Badumna traffic. Defaults to 21399.
         * @return The end of the UDP port range to use for Badumna traffic.
         */
        int GetEndPortRange() const;

        /**
         * Sets the maximum number of ports to try when a port range is configured. Defaults to 5.
         * @param num The maximum number of ports to try.
         */
        void SetMaxPortsToTry(int num);

        /**
         * Gets the maximum number of ports to try when a port range is configured. Defaults to 5.
         * @return The maximum number of ports to try.
         */
        int GetMaxPortsToTry() const;

        /**
         * Enable port forwarding.
         */
        void EnablePortForwarding();

        /**
         * Gets a value indicating whether an attempt should be made to configure a port forwarding rule on the local 
         * router using a mechanism such as UPnP. Defaults to true.
         * @return A value indicating whether an attempt should be made to configure a port forwarding rule.
         */
        bool IsPortForwardingEnabled() const;

        /**
         * Enable broadcast.
         */
        void EnableBroadcast();

        /**
         * Gets a value indicating whether local network broadcast should be used to discover peers. Defaults to true.
         * @return A value indicating whether local network broadcast should be used.
         */
        bool IsBroadcastEnabled() const;
        
        /**
         * Sets the UDP port to be used for broadcast discovery of peers on the local network. 
         * @param port The UDP port to be used for broadcast.
         */
        void SetBroadcastPort(int port);

        /**
         * Gets the UDP port to be used for broadcast discovery of peers on the local network. Defaults to 39817.
         * @return The UDP port to be used for broadcast.
         */
        int GetBroadcastPort() const;

        /**
         * Sets the option indicating when a tunnelled connection should be used.
         * @param mode The tunnel mode to set. 
         */
        void SetTunnelMode(TunnelMode mode);

        /**
         * Gets an option indicating when a tunnelled connection should be used. Defaults to off.
         * @return A tunnel mode option. 
         */
        TunnelMode GetTunnelMode() const;
        
        /**
         * Adds a tunnel server URI.
         * @param uri The tunnel server URI to add.
         */
        void AddTunnelUri(String const &uri);
        
        /**
         * Gets the list of added tunnel server URIs. 
         * @return Added tunnel server URIs.
         */
        String GetTunnelUris() const;

        /**
         * Set the current process as a hosted service.
         */
        void EnableHostedService();

        /**
         * Gets a value indicating whether the current process is a hosted service, such as a seedpeer, overload server, 
         * or arbitration server. Defaults to false.
         * @return A value indicating whether the current process is a hosted service.
         */
        bool IsHostedService() const;

        /**
         * Added a seedpeer address.
         * @param address The seedpeer address to add.
         */
        void AddSeedPeer(String const &address);
        
        /**
         * Gets a list of added seedpeer addresses.
         * @return Added seedpeer addresses.
         */
        String GetSeedPeers() const;

        /**
         * Sets configuration options appropriate for use on a local area network.  
         */ 
        void ConfigureForLan();

    private:
        explicit ConnectivityModule(ConnectivityModuleImpl *impl);
        std::auto_ptr<ConnectivityModuleImpl> const impl_;
    
        DISALLOW_COPY_AND_ASSIGN(ConnectivityModule);
    };
}

#endif // BADUMNA_CONNECTIVITY_MODULE_H