//-----------------------------------------------------------------------
// <copyright file="MatchmakingModule.h" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef BADUMNA_MATCHMAKING_MODULE_H
#define BADUMNA_MATCHMAKING_MODULE_H

#include <memory>

#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class MatchmakingModuleImpl;
    
    /**
     * @class MatchmakingModule
     * @brief Configuration options for matchmaking.
     *
     * Configuration options for the matchmaking system.
     */
    class BADUMNA_API MatchmakingModule
    {
        friend class Options;
    public:
        /**
         * Destructor
         */
        ~MatchmakingModule();
        
        /**
         * Sets the address of the matchmaking server.
         *
         * Defaults to the empty string.
         */
        void SetServerAddress(String const &server_address);

        /**
         * Gets the address of the matchmaking server.
         */
        String GetServerAddress();

        /**
         * Sets the maximum number of active matches allowed on the matchmaking server.
         *
         * This defaults to 0, meaning the peer will not act as a matchmaking server.
         */
        void SetActiveMatchLimit(int limit);

        /**
         * Gets the maximum number of active matches allowed on the matchmaking server.
         */
        int GetActiveMatchLimit();

    private:
        explicit MatchmakingModule(MatchmakingModuleImpl *impl);
        std::auto_ptr<MatchmakingModuleImpl> const impl_;

        DISALLOW_COPY_AND_ASSIGN(MatchmakingModule);
    };
}

#endif // BADUMNA_MATCHMAKING_MODULE_H