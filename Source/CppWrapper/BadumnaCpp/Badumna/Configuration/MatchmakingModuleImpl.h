//-----------------------------------------------------------------------
// <copyright file="MatchmakingModuleImpl.h" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef BADUMNA_MATCHMAKING_MODULE_IMPL_H
#define BADUMNA_MATCHMAKING_MODULE_IMPL_H

#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Core/ProxyObject.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class MatchmakingModuleImpl : public ProxyObject
    {
        friend class BadumnaRuntime;
    public:
        void SetServerAddress(String const &server_address);
        String GetServerAddress();
        void SetActiveMatchLimit(int limit);
        int GetActiveMatchLimit();

    private:
        MatchmakingModuleImpl(MonoRuntime *runtime, DotNetObject object);

        DISALLOW_COPY_AND_ASSIGN(MatchmakingModuleImpl);
    };
}

#endif // BADUMNA_MATCHMAKING_MODULE_IMPL_H