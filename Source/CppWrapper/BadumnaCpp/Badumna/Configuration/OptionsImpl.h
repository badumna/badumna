//---------------------------------------------------------------------------------
// <copyright file="OptionsImpl.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_OPTIONS_IMPL_H
#define BADUMNA_OPTIONS_IMPL_H

#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Core/ProxyObject.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class OptionsImpl : public ProxyObject
    {
        friend class BadumnaRuntime;
    public:
        void Validate() const;
        void Save(String const &filename) const;

        DotNetObject GetManagedOverloadModule() const;
        DotNetObject GetManagedConnectivityModule() const;
        DotNetObject GetManagedArbitrationModule() const;
        DotNetObject GetManagedLoggerModule() const;
        DotNetObject GetManagedMatchmakingModule() const;
    private:
        void SetToCppMode();

        explicit OptionsImpl(MonoRuntime *runtime);
        OptionsImpl(MonoRuntime *runtime, String const &filename);

        DISALLOW_COPY_AND_ASSIGN(OptionsImpl);
    };
}

#endif // BADUMNA_OPTIONS_IMPL_H