//---------------------------------------------------------------------------------
// <copyright file="ArbitrationModule.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_ARBITRATION_MODULE_H
#define BADUMNA_ARBITRATION_MODULE_H

#include <memory>

#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class ArbitrationModuleImpl;
    
    /**
     * @class ArbitrationModule
     * @brief Configuration options for arbitration.
     *
     * Configuration options for the arbitration system.
     */
    class BADUMNA_API ArbitrationModule
    {
        friend class Options;
    public:
        /**
         * Destructor
         */
        ~ArbitrationModule();
        
        /**
         * Adds a server that will be located using distributed lookup. 
         * @param name The name of the server to be added. 
         */
        void AddServer(String const &name);

        /**
         * Adds a server with a specific address. 
         * @param name The name of the server to be added.
         * @param address The address of the server to be added. 
         */
        void AddServer(String const &name, String const &address);

        /**
         * Gets the details of all added servers.
         * @return The details of all added servers.
         */
        String GetServers() const;

    private:
        explicit ArbitrationModule(ArbitrationModuleImpl *impl);
        std::auto_ptr<ArbitrationModuleImpl> const impl_;

        DISALLOW_COPY_AND_ASSIGN(ArbitrationModule);
    };
}

#endif // BADUMNA_ARBITRATION_MODULE_H