//---------------------------------------------------------------------------------
// <copyright file="OverloadModuleImpl.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include "Badumna/DataTypes/String.h"
#include "Badumna/Core/TypeRegistry.h"
#include "Badumna/Configuration/OverloadModuleImpl.h"

namespace Badumna
{
    REGISTER_MANAGED_MONO_TYPE(Badumna, OverloadModule);
    OverloadModuleImpl::OverloadModuleImpl(MonoRuntime *runtime, DotNetObject object)
        : ProxyObject(runtime, object)
    {
    }

    REGISTER_PROPERTY(Badumna, OverloadModule, ServerAddress)
    String OverloadModuleImpl::GetServerAddress() const
    {
        return managed_object_.GetPropertyFromName<String>("ServerAddress");
    }
    
    void OverloadModuleImpl::SetServerAddress(String const &address)
    {
        managed_object_.SetPropertyValue("ServerAddress", address);
    }

    REGISTER_PROPERTY(Badumna, OverloadModule, IsServer)
    bool OverloadModuleImpl::IsServer() const
    {
        return managed_object_.GetPropertyFromName<bool>("IsServer");
    }
    
    void OverloadModuleImpl::SetAsServer()
    {
        managed_object_.SetPropertyValue("IsServer", true);
    }

    REGISTER_PROPERTY(Badumna, OverloadModule, IsClientEnabled)
    bool OverloadModuleImpl::IsClientEnabled() const
    {   
        return managed_object_.GetPropertyFromName<bool>("IsClientEnabled");
    }
    
    void OverloadModuleImpl::SetClientEnabled()
    {
        managed_object_.SetPropertyValue("IsClientEnabled", true);
    }

    REGISTER_PROPERTY(Badumna, OverloadModule, IsForced)
    bool OverloadModuleImpl::IsForced() const
    {
        return managed_object_.GetPropertyFromName<bool>("IsForced");
    }
    
    REGISTER_MANAGED_METHOD(Badumna, OverloadModule, EnableForcedOverload, 1)
    void OverloadModuleImpl::EnableForcedOverload(String const &testing_only_acknowledgement)
    {
        Arguments args;
        args.AddArgument(MonoRuntime::GetString(testing_only_acknowledgement));

        managed_object_.InvokeMethod("EnableForcedOverload", args);
    }
    
    REGISTER_MANAGED_METHOD(Badumna, OverloadModule, DisableForcedOverload, 0)
    void OverloadModuleImpl::DisableForcedOverload()
    {
        managed_object_.InvokeMethod("DisableForcedOverload");
    }
}