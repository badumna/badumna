//---------------------------------------------------------------------------------
// <copyright file="LogLevel.h" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_LOG_LEVEL_H
#define BADUMNA_LOG_LEVEL_H

namespace Badumna
{
    /**
     * @enum LogLevel
     *
     * Identifies the class of log messages.
     */
    enum LogLevel
    {
        LogLevel_Information = 0,     /**< Informational messages, describing normal progress of the system. */
        LogLevel_Warning,             /**< Warning messages, indicating an potential problem. */
        LogLevel_Error,               /**< Error messages, indicating a serious problem. */
    };  
}

#endif // BADUMNA_LOG_LEVEL_H