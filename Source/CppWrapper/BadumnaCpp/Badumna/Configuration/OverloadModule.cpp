//---------------------------------------------------------------------------------
// <copyright file="OverloadModule.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include "Badumna/Configuration/OverloadModule.h"
#include "Badumna/Configuration/OverloadModuleImpl.h"

namespace Badumna
{
    OverloadModule::OverloadModule(OverloadModuleImpl *imp)
        : impl_(imp)
    {
        assert(imp != NULL);
    }
    
    OverloadModule::~OverloadModule()
    {
    }
    
    String OverloadModule::GetServerAddress() const
    {
        return impl_->GetServerAddress();
    }

    void OverloadModule::SetServerAddress(String const &address)
    {
        impl_->SetServerAddress(address);
    }

    bool OverloadModule::IsServer() const
    {
        return impl_->IsServer();
    }

    void OverloadModule::SetAsServer()
    {
        impl_->SetAsServer();
    }

    bool OverloadModule::IsClientEnabled() const
    {
        return impl_->IsClientEnabled();
    }

    void OverloadModule::SetClientEnabled()
    {
        impl_->SetClientEnabled();
    }

    bool OverloadModule::IsForced() const
    {
        return impl_->IsForced();
    }

    void OverloadModule::EnableForcedOverload(String const &testing_only_acknowledgement)
    {
        impl_->EnableForcedOverload(testing_only_acknowledgement);
    }

    void OverloadModule::DisableForcedOverload()
    {
        impl_->DisableForcedOverload();
    }
}