//---------------------------------------------------------------------------------
// <copyright file="Options.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include "Badumna/Configuration/Options.h"
#include "Badumna/Configuration/OptionsImpl.h"
#include "Badumna/Core/BadumnaRuntime.h"

namespace Badumna
{
    Options::Options()
        : impl_(BadumnaRuntime::Instance().CreateOptionsImpl()),
        connectivity_(new ConnectivityModule(BadumnaRuntime::Instance().CreateConnectivityModuleImpl(impl_->GetManagedConnectivityModule()))),
        overload_(new OverloadModule(BadumnaRuntime::Instance().CreateOverloadModuleImpl(impl_->GetManagedOverloadModule()))),
        arbitration_(new ArbitrationModule(BadumnaRuntime::Instance().CreateArbitrationModuleImpl(impl_->GetManagedArbitrationModule()))),
        logger_(new LoggerModule(BadumnaRuntime::Instance().CreateLoggerModuleImpl(impl_->GetManagedLoggerModule()))),
        matchmaking_(new MatchmakingModule(BadumnaRuntime::Instance().CreateMatchmakingModuleImpl(impl_->GetManagedMatchmakingModule())))
    {
    }
    
    Options::Options(String const &filename)
        : impl_(BadumnaRuntime::Instance().CreateOptionsImpl(filename)),
        connectivity_(new ConnectivityModule(BadumnaRuntime::Instance().CreateConnectivityModuleImpl(impl_->GetManagedConnectivityModule()))),
        overload_(new OverloadModule(BadumnaRuntime::Instance().CreateOverloadModuleImpl(impl_->GetManagedOverloadModule()))),
        arbitration_(new ArbitrationModule(BadumnaRuntime::Instance().CreateArbitrationModuleImpl(impl_->GetManagedArbitrationModule()))),
        logger_(new LoggerModule(BadumnaRuntime::Instance().CreateLoggerModuleImpl(impl_->GetManagedLoggerModule()))),
        matchmaking_(new MatchmakingModule(BadumnaRuntime::Instance().CreateMatchmakingModuleImpl(impl_->GetManagedMatchmakingModule())))
    {
    }

    Options::~Options()
    {
    }

    void Options::Validate() const
    {
        impl_->Validate();
    }
    
    void Options::Save(String const &filename) const
    {
        impl_->Save(filename);
    }
    
    ConnectivityModule &Options::GetConnectivityModule() const
    {
        return *(connectivity_.get());
    }

    OverloadModule &Options::GetOverloadModule() const
    {
        return *(overload_.get());
    }

    ArbitrationModule &Options::GetArbitrationModule() const
    {
        return *(arbitration_.get());
    }

    LoggerModule &Options::GetLoggerModule() const
    {
        return *(logger_.get());
    }

    MatchmakingModule &Options::GetMatchmakingModule() const
    {
        return *(matchmaking_.get());
    }
}