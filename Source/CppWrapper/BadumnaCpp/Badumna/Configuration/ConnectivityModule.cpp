//---------------------------------------------------------------------------------
// <copyright file="ConnectivityModule.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include "Badumna/Configuration/ConnectivityModule.h"
#include "Badumna/Configuration/ConnectivityModuleImpl.h"

namespace Badumna
{
    ConnectivityModule::ConnectivityModule(ConnectivityModuleImpl *imp)
        : impl_(imp)
    {
        assert(imp != NULL);
    }

    ConnectivityModule::~ConnectivityModule()
    {
    }

    void ConnectivityModule::SetApplicationName(String const &app_name)
    {
        impl_->SetApplicationName(app_name);
    }

    String ConnectivityModule::GetApplicationName() const
    {
        return impl_->GetApplicationName();
    }

    bool ConnectivityModule::IsTransportLimiterEnabled() const
    {
        return impl_->IsTransportLimiterEnabled();
    }
        
    int ConnectivityModule::GetBandwidthLimit() const
    {
        return impl_->GetBandwidthLimit();
    }
    
    void ConnectivityModule::SetBadumnaLimit(int limit)
    {
        impl_->SetBadumnaLimit(limit);
    }
        
    void ConnectivityModule::AddStunServer(String const &address)
    {
        impl_->AddStunServer(address);
    }

    String ConnectivityModule::GetStunServers() const
    {
        return impl_->GetStunServers();
    }

    void ConnectivityModule::ConfigureForSpecificPort(int port)
    {
        impl_->ConfigureForSpecificPort(port);
    }

    int ConnectivityModule::GetStartPortRange() const
    {
        return impl_->GetStartPortRange();
    }
    
    void ConnectivityModule::SetStartPortRange(int start_port)
    {
        impl_->SetStartPortRange(start_port);
    }
    
    int ConnectivityModule::GetEndPortRange() const
    {
        return impl_->GetEndPortRange();
    }

    void ConnectivityModule::SetEndPortRange(int end_port)
    {
        impl_->SetEndPortRange(end_port);
    }

    int ConnectivityModule::GetMaxPortsToTry() const
    {
        return impl_->GetMaxPortsToTry();
    }
    
    void ConnectivityModule::SetMaxPortsToTry(int num)
    {
        impl_->SetMaxPortsToTry(num);
    }

    bool ConnectivityModule::IsPortForwardingEnabled() const
    {
        return impl_->IsPortForwardingEnabled();
    }
    
    void ConnectivityModule::EnablePortForwarding()
    {
        impl_->EnablePortForwarding();
    }

    bool ConnectivityModule::IsBroadcastEnabled() const
    {
        return impl_->IsBroadcastEnabled();
    }
    
    void ConnectivityModule::EnableBroadcast()
    {
        impl_->EnableBroadcast();
    }
    
    int ConnectivityModule::GetBroadcastPort() const
    {
        return impl_->GetBroadcastPort();
    }
    
    void ConnectivityModule::SetBroadcastPort(int port)
    {
        return impl_->SetBroadcastPort(port);
    }

    TunnelMode ConnectivityModule::GetTunnelMode() const
    {
        return impl_->GetTunnelMode();
    }
    
    void ConnectivityModule::SetTunnelMode(TunnelMode mode)
    {
        impl_->SetTunnelMode(mode);
    }

    void ConnectivityModule::AddTunnelUri(String const &uri)
    {
        impl_->AddTunnelUri(uri);
    }

    String ConnectivityModule::GetTunnelUris() const
    {
        return impl_->GetTunnelUris();
    }
        
    bool ConnectivityModule::IsHostedService() const
    {
        return impl_->IsHostedService();
    }
    
    void ConnectivityModule::EnableHostedService()
    {
        impl_->EnableHostedService();
    }

    void ConnectivityModule::AddSeedPeer(String const &address)
    {
        impl_->AddSeedPeer(address);
    }
    
    String ConnectivityModule::GetSeedPeers() const
    {
        return impl_->GetSeedPeers();
    }

    void ConnectivityModule::EnableTransportLimiter(String const &testing_only_acknowledgement)
    {
        impl_->EnableTransportLimiter(testing_only_acknowledgement);
    }

    void ConnectivityModule::DisableTransportLimiter()
    {
        impl_->DisableTransportLimiter();
    }

    void ConnectivityModule::ConfigureForLan()
    {
        impl_->ConfigureForLan();
    }
}