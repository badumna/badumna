//---------------------------------------------------------------------------------
// <copyright file="TunnelMode.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_TUNNEL_MODE_H
#define BADUMNA_TUNNEL_MODE_H

namespace Badumna
{
    /**
     * @enum TunnelMode
     *
     * Specifies when a tunnelling connection should be used.
     */
    enum TunnelMode
    {        
        TunnelMode_Off,     /**< Never use tunnelling. */
        TunnelMode_Auto,    /**< Use tunnelling if a direct connection cannot be established. */
        TunnelMode_On       /**< Always use tunnelling. */
    };
}

#endif // BADUMNA_TUNNEL_MODE_H