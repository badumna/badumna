//---------------------------------------------------------------------------------
// <copyright file="ArbitrationModule.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include "Badumna/Configuration/ArbitrationModule.h"
#include "Badumna/Configuration/ArbitrationModuleImpl.h"

namespace Badumna
{
    ArbitrationModule::ArbitrationModule(ArbitrationModuleImpl *imp)
        : impl_(imp)
    {
        assert(imp != NULL);
    }

    ArbitrationModule::~ArbitrationModule()
    {
    }

    void ArbitrationModule::AddServer(String const &name)
    {
        impl_->AddServer(name);
    }

    void ArbitrationModule::AddServer(String const &name, String const &address)
    {
        impl_->AddServer(name, address);
    }

    String ArbitrationModule::GetServers() const
    {
        return impl_->GetServers();
    }
}