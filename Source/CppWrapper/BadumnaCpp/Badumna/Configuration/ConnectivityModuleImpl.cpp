//---------------------------------------------------------------------------------
// <copyright file="ConnectivityModuleImpl.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include "Badumna/Core/TypeRegistry.h"
#include "Badumna/Configuration/ConnectivityModuleImpl.h"

namespace Badumna
{
    REGISTER_MANAGED_MONO_TYPE(Badumna, ConnectivityModule);
    ConnectivityModuleImpl::ConnectivityModuleImpl(MonoRuntime *runtime, DotNetObject object)
        : ProxyObject(runtime, object)
    {
    }

    REGISTER_PROPERTY(Badumna, ConnectivityModule, ApplicationName)
    void ConnectivityModuleImpl::SetApplicationName(String const &app_name)
    {
        managed_object_.SetPropertyValue("ApplicationName", app_name);
    }

    String ConnectivityModuleImpl::GetApplicationName() const
    {
        return managed_object_.GetPropertyFromName<String>("ApplicationName");
    }

    REGISTER_PROPERTY(Badumna, ConnectivityModule, IsTransportLimiterEnabled)
    bool ConnectivityModuleImpl::IsTransportLimiterEnabled() const
    {
        return managed_object_.GetPropertyFromName<bool>("IsTransportLimiterEnabled");
    }
    
    REGISTER_PROPERTY(Badumna, ConnectivityModule, BandwidthLimit)
    int ConnectivityModuleImpl::GetBandwidthLimit() const
    {
        return managed_object_.GetPropertyFromName<int>("BandwidthLimit");
    }
    
    void ConnectivityModuleImpl::SetBadumnaLimit(int limit)
    {
        managed_object_.SetPropertyValue("BandwidthLimit", limit);
    }

    REGISTER_PROPERTY(Badumna, ConnectivityModule, StunServers)
    void ConnectivityModuleImpl::AddStunServer(String const &address)
    {
        DotNetObject list = managed_object_.GetPropertyFromName("StunServers");
        ManagedObject returned_object(mono_runtime_, list);
        DotNetString address_string = MonoRuntime::GetString(address);

        Arguments args;
        args.AddArgument(address_string);

        returned_object.InvokeMethod("Add", args);
    }

    String ConnectivityModuleImpl::GetStunServers() const
    {
        DotNetObject list = managed_object_.GetPropertyFromName("StunServers");
        assert(list != NULL);
        ManagedObject returned_object(mono_runtime_, list);

        return returned_object.InvokeVirtualMethod<String>("ToString");
    }

    REGISTER_MANAGED_METHOD(Badumna, ConnectivityModule, ConfigureForSpecificPort, 1)
    void ConnectivityModuleImpl::ConfigureForSpecificPort(int port)
    {
        Arguments args;
        args.AddArgument(&port);

        managed_object_.InvokeMethod("ConfigureForSpecificPort", args);
    }

    REGISTER_PROPERTY(Badumna, ConnectivityModule, StartPortRange)
    int ConnectivityModuleImpl::GetStartPortRange() const
    {
        return managed_object_.GetPropertyFromName<int>("StartPortRange");
    }
    
    void ConnectivityModuleImpl::SetStartPortRange(int start_port)
    {
        managed_object_.SetPropertyValue("StartPortRange", start_port);
    }
    
    REGISTER_PROPERTY(Badumna, ConnectivityModule, EndPortRange)
    int ConnectivityModuleImpl::GetEndPortRange() const
    {
        return managed_object_.GetPropertyFromName<int>("EndPortRange");
    }

    void ConnectivityModuleImpl::SetEndPortRange(int end_port)
    {
        managed_object_.SetPropertyValue("EndPortRange", end_port);
    }

    REGISTER_PROPERTY(Badumna, ConnectivityModule, MaxPortsToTry)
    int ConnectivityModuleImpl::GetMaxPortsToTry() const
    {
        return managed_object_.GetPropertyFromName<int>("MaxPortsToTry");
    }
    
    void ConnectivityModuleImpl::SetMaxPortsToTry(int num)
    {
        managed_object_.SetPropertyValue("MaxPortsToTry", num);
    }

    REGISTER_PROPERTY(Badumna, ConnectivityModule, IsPortForwardingEnabled)
    bool ConnectivityModuleImpl::IsPortForwardingEnabled() const
    {
        return managed_object_.GetPropertyFromName<bool>("IsPortForwardingEnabled");
    }
    
    void ConnectivityModuleImpl::EnablePortForwarding()
    {
        managed_object_.SetPropertyValue("IsPortForwardingEnabled", true);
    }

    REGISTER_PROPERTY(Badumna, ConnectivityModule, IsBroadcastEnabled)
    bool ConnectivityModuleImpl::IsBroadcastEnabled() const
    {
        return managed_object_.GetPropertyFromName<bool>("IsBroadcastEnabled");
    }
    
    void ConnectivityModuleImpl::EnableBroadcast()
    {
        managed_object_.SetPropertyValue("IsBroadcastEnabled", true);
    }
    
    REGISTER_PROPERTY(Badumna, ConnectivityModule, BroadcastPort)
    int ConnectivityModuleImpl::GetBroadcastPort() const
    {
        return managed_object_.GetPropertyFromName<int>("BroadcastPort");
    }
    
    void ConnectivityModuleImpl::SetBroadcastPort(int port)
    {
        managed_object_.SetPropertyValue("BroadcastPort", port);
    }

    REGISTER_PROPERTY(Badumna, ConnectivityModule, TunnelMode)
    TunnelMode ConnectivityModuleImpl::GetTunnelMode() const
    {
        return static_cast<TunnelMode>(managed_object_.GetPropertyFromName<int>("TunnelMode"));
    }
    
    void ConnectivityModuleImpl::SetTunnelMode(TunnelMode mode)
    {
        managed_object_.SetPropertyValue("TunnelMode", static_cast<int>(mode));
    }

    REGISTER_PROPERTY(Badumna, ConnectivityModule, TunnelUris)
    void ConnectivityModuleImpl::AddTunnelUri(String const &uri)
    {
        DotNetObject list = managed_object_.GetPropertyFromName("TunnelUris");
        assert(list != NULL);
        ManagedObject returned_object(mono_runtime_, list);
        DotNetString address_string = MonoRuntime::GetString(uri);
        assert(address_string != NULL);

        Arguments args;
        args.AddArgument(address_string);

        returned_object.InvokeMethod("Add", args);
    }

    String ConnectivityModuleImpl::GetTunnelUris() const
    {
        DotNetObject list = managed_object_.GetPropertyFromName("TunnelUris");
        assert(list != NULL);
        ManagedObject returned_object(mono_runtime_, list);

        return returned_object.InvokeVirtualMethod<String>("ToString");
    }
        
    REGISTER_PROPERTY(Badumna, ConnectivityModule, IsHostedService)
    bool ConnectivityModuleImpl::IsHostedService() const
    {
        return managed_object_.GetPropertyFromName<bool>("IsHostedService");
    }
    
    void ConnectivityModuleImpl::EnableHostedService()
    {
        managed_object_.SetPropertyValue("IsHostedService", true);
    }

    REGISTER_PROPERTY(Badumna, ConnectivityModule, SeedPeers)
    void ConnectivityModuleImpl::AddSeedPeer(String const &address)
    {
        DotNetObject list = managed_object_.GetPropertyFromName("SeedPeers");
        assert(list != NULL);
        ManagedObject returned_object(mono_runtime_, list);
        DotNetString address_string = MonoRuntime::GetString(address);
        assert(address_string != NULL);

        Arguments args;
        args.AddArgument(address_string);

        returned_object.InvokeMethod("Add", args);
    }

    String ConnectivityModuleImpl::GetSeedPeers() const
    {
        DotNetObject list = managed_object_.GetPropertyFromName("SeedPeers");
        assert(list != NULL);
        ManagedObject returned_object(mono_runtime_, list);

        return returned_object.InvokeVirtualMethod<String>("ToString");
    }

    REGISTER_MANAGED_METHOD(Badumna, ConnectivityModule, EnableTransportLimiter, 1)
    void ConnectivityModuleImpl::EnableTransportLimiter(String const &testing_only_acknowledgement)
    {
        Arguments args;
        args.AddArgument(MonoRuntime::GetString(testing_only_acknowledgement));

        managed_object_.InvokeMethod("EnableTransportLimiter", args);
    }

    REGISTER_MANAGED_METHOD(Badumna, ConnectivityModule, DisableTransportLimiter, 0)
    void ConnectivityModuleImpl::DisableTransportLimiter()
    {
        managed_object_.InvokeMethod("DisableTransportLimiter");
    }

    REGISTER_MANAGED_METHOD(Badumna, ConnectivityModule, ConfigureForLan, 0)
    void ConnectivityModuleImpl::ConfigureForLan()
    {
        managed_object_.InvokeMethod("ConfigureForLan");
    }
}