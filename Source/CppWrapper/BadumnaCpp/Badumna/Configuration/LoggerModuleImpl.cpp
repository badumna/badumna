//---------------------------------------------------------------------------------
// <copyright file="LoggerModuleImpl.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include "Badumna/Core/TypeRegistry.h"
#include "Badumna/Configuration/LoggerModuleImpl.h"

namespace Badumna
{
    REGISTER_MANAGED_MONO_TYPE(Badumna, LoggerModule);
    LoggerModuleImpl::LoggerModuleImpl(MonoRuntime *runtime, DotNetObject object)
        : ProxyObject(runtime, object)
    {
    }
    
    REGISTER_PROPERTY(Badumna, LoggerModule, IsDotNetTraceForwardingEnabled)
    bool LoggerModuleImpl::IsDotNetTraceForwardingEnabled() const
    {
        return managed_object_.GetPropertyFromName<bool>("IsDotNetTraceForwardingEnabled");
    }

    void LoggerModuleImpl::SetDotNetTraceForwardingEnabled()
    {
        managed_object_.SetPropertyValue("IsDotNetTraceForwardingEnabled", true);
    }

    REGISTER_PROPERTY(Badumna, LoggerModule, IsAssertUiSuppressed)
    bool LoggerModuleImpl::IsAssertUiSuppressed() const
    {
        return managed_object_.GetPropertyFromName<bool>("IsAssertUiSuppressed");
    }
    void LoggerModuleImpl::SetAssertUiSuppressed()
    {
        managed_object_.SetPropertyValue("IsAssertUiSuppressed", true);
    }

    REGISTER_PROPERTY(Badumna, LoggerModule, LoggerType)
    LoggerType LoggerModuleImpl::GetLoggerType() const
    {
        return managed_object_.GetPropertyFromName<LoggerType>("LoggerType");
    }
    
    void LoggerModuleImpl::SetLoggerType(LoggerType type)
    {
        managed_object_.SetPropertyValue("LoggerType", static_cast<int>(type));
    }

    REGISTER_PROPERTY(Badumna, LoggerModule, LogLevel)
    LogLevel LoggerModuleImpl::GetLogLevel() const
    {
        return managed_object_.GetPropertyFromName<LogLevel>("LogLevel");
    }

    void LoggerModuleImpl::SetLogLevel(LogLevel level)
    {
        managed_object_.SetPropertyValue("LogLevel", static_cast<int>(level));
    }

    REGISTER_PROPERTY(Badumna, LoggerModule, LoggerConfig)
    String LoggerModuleImpl::GetLoggerConfig() const
    {
        return managed_object_.GetPropertyFromName<String>("LoggerConfig");
    }
    
    void LoggerModuleImpl::SetLoggerConfig(String const &config)
    {
        return managed_object_.SetPropertyValue("LoggerConfig", config);
    }
}