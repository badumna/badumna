//---------------------------------------------------------------------------------
// <copyright file="Options.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_OPTIONS_H
#define BADUMNA_OPTIONS_H

#include <memory>

#include "Badumna/Configuration/ConnectivityModule.h"
#include "Badumna/Configuration/OverloadModule.h"
#include "Badumna/Configuration/ArbitrationModule.h"
#include "Badumna/Configuration/LoggerModule.h"
#include "Badumna/Configuration/MatchmakingModule.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class OptionsImpl;

    /**
     * @class Options
     * @brief Configuration option for the Badumna library.
     *
     * The Options class specifies the configuration options for the Badumna library.
     */
    class BADUMNA_API Options
    {
        friend class ManagedObjectGuard;
    public:
        /**
         * Constructor that constructs a new instance of Options with all configuration options set to its default values.
         */
        Options();

        /**
         * Constructor that constructs a new instance of Options from a specified XML document. To generate such a XML
         * file: construct a default instance, configure it programmatically, then call <code>Save()</code>.
         * @param filename The path of the file to load the options from.
         */
        Options(String const &filename);

        /**
         * Destructor.
         */
        ~Options();

        /**
         * Validates all options to ensure they are consistent.
         */
        void Validate() const;

        /**
         * Stores the current options in XML format.
         * @param filename The name of the file to store the options into.
         */
        void Save(String const &filename) const;
    
        /**
         * Gets a reference to the options for the connectivity module.
         * @return a reference to the connectivity configuration module.
         */
        ConnectivityModule& GetConnectivityModule() const;

        /**
         * Gets a reference to the options for the overload module.
         * @return a reference to the overload configuration module.
         */
        OverloadModule& GetOverloadModule() const;

        /**
         * Gets a reference to the options for the arbitration module.
         * @return a reference to the arbitration configuration module.
         */
        ArbitrationModule& GetArbitrationModule() const;

        /**
         * Gets a reference to the options for the logger module.
         * @return a reference to the logger configuration module.
         */
        LoggerModule& GetLoggerModule() const;


        /**
         * Gets a reference to the options for the matchmaking module.
         * @return a reference to the matchmaking configuration module.
         */
        MatchmakingModule& GetMatchmakingModule() const;

    private:

        std::auto_ptr<OptionsImpl> const impl_;
        std::auto_ptr<ConnectivityModule> const connectivity_;
        std::auto_ptr<OverloadModule> const overload_;
        std::auto_ptr<ArbitrationModule> const arbitration_;
        std::auto_ptr<LoggerModule> const logger_;
        std::auto_ptr<MatchmakingModule> const matchmaking_;

        DISALLOW_COPY_AND_ASSIGN(Options);
    };
}

#endif // BADUMNA_OPTIONS_H