//---------------------------------------------------------------------------------
// <copyright file="LoggerModuleImpl.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_LOGGER_MODULE_IMPL_H
#define BADUMNA_LOGGER_MODULE_IMPL_H

#include "Badumna/Configuration/LoggerType.h"
#include "Badumna/Configuration/LogLevel.h"
#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Core/ProxyObject.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class LoggerModuleImpl : public ProxyObject
    {
        friend class BadumnaRuntime;
    public:
        bool IsDotNetTraceForwardingEnabled() const;
        void SetDotNetTraceForwardingEnabled();

        bool IsAssertUiSuppressed() const;
        void SetAssertUiSuppressed();

        LoggerType GetLoggerType() const;
        void SetLoggerType(LoggerType type);

        LogLevel GetLogLevel() const;
        void SetLogLevel(LogLevel level);

        String GetLoggerConfig() const;
        void SetLoggerConfig(String const &config);
    
    private:
        LoggerModuleImpl(MonoRuntime *runtime, DotNetObject object);

        DISALLOW_COPY_AND_ASSIGN(LoggerModuleImpl);
    };
}

#endif // BADUMNA_LOGGER_MODULE_IMPL_H