//---------------------------------------------------------------------------------
// <copyright file="OptionsImpl.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include "Badumna/Core/TypeRegistry.h"
#include "Badumna/Configuration/OptionsImpl.h"

namespace Badumna
{
    REGISTER_MANAGED_MONO_TYPE(Badumna, Options);
    OptionsImpl::OptionsImpl(MonoRuntime *runtime)
        : ProxyObject(runtime)
    {
        DotNetClass klass = runtime->GetClass("Badumna", "Options");
        DotNetObject created_object = runtime->CreateObject(klass, NULL);
        managed_object_.Initialize(created_object, "Badumna", "Options");
        SetToCppMode();
    }

    OptionsImpl::OptionsImpl(MonoRuntime *runtime, String const &filename)
        : ProxyObject(runtime)
    {
        DotNetClass klass = runtime->GetClass("Badumna", "Options");

        Arguments args;
        args.AddArgument(MonoRuntime::GetString(filename));
        DotNetObject created_object = runtime->CreateObject(klass, args, NULL);
        managed_object_.Initialize(created_object, "Badumna", "Options");
        SetToCppMode();
    }

    REGISTER_MANAGED_METHOD(Badumna, Options, Validate, 0)
    void OptionsImpl::Validate() const
    {
        managed_object_.InvokeMethod("Validate");
    }

    REGISTER_MANAGED_METHOD(Badumna, Options, Save, 1)
    void OptionsImpl::Save(String const &filename) const
    {
        Arguments args;
        args.AddArgument(MonoRuntime::GetString(filename));
        managed_object_.InvokeMethod("Save", args);
    }

    REGISTER_PROPERTY(Badumna, Options, Overload)
    DotNetObject OptionsImpl::GetManagedOverloadModule() const
    {
        return managed_object_.GetPropertyFromName("Overload");
    }
    
    REGISTER_PROPERTY(Badumna, Options, Connectivity)
    DotNetObject OptionsImpl::GetManagedConnectivityModule() const
    {
        return managed_object_.GetPropertyFromName("Connectivity");
    }

    REGISTER_PROPERTY(Badumna, Options, Arbitration)
    DotNetObject OptionsImpl::GetManagedArbitrationModule() const
    {
        return managed_object_.GetPropertyFromName("Arbitration");
    }
    
    REGISTER_PROPERTY(Badumna, Options, Logger)
    DotNetObject OptionsImpl::GetManagedLoggerModule() const
    {
        return managed_object_.GetPropertyFromName("Logger");
    }
        
    REGISTER_PROPERTY(Badumna, Options, Matchmaking)
    DotNetObject OptionsImpl::GetManagedMatchmakingModule() const
    {
        return managed_object_.GetPropertyFromName("Matchmaking");
    }

    REGISTER_PROPERTY(Badumna, Options, IsInCppMode)
    void OptionsImpl::SetToCppMode()
    {
        bool v = true;
        Arguments args;
        args.AddArgument(&v);
        managed_object_.SetPropertyValue("IsInCppMode", args);
    }
}