//---------------------------------------------------------------------------------
// <copyright file="String.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_STRING_H
#define BADUMNA_STRING_H

#include <memory>
#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/Utils/ScopedPtr.h"

namespace Badumna
{
    class StringImpl;

    /**
     * @class String
     * @brief The string class used in the Badumna.
     *
     * The string class used in the Badumna library.
     */
    class BADUMNA_API String
    {
    public:
        /**
         * Constructor that constructs an empty string instance.
         */
        String();

        /**
         * Constructor that constructs a string instance from the specified ANSI C string.
         * @param str NULL terminated ANSI c string.
         */
        String(char const *str);
        
        /**
         * Constructor that constructs a string instance from the specified wide char string.
         * @param str NULL terminated wide char string.
         */
        String(wchar_t const *str);
        
        /**
         * Copy constructor that constructs a string instance from another string.
         * @param other The string object to copy from. 
         */
        String(String const &other);

        /**
         * Assignment operator. 
         * @param rhs The hand side parameter of the assignment operation. 
         * @return A reference to this string object.
         */
        String &operator=(String const &rhs);

        /**
         * Destructor
         */
        ~String();

        /**
         * Equals to operator.
         * @param rhs The right hand side of the operator.
         * @return Whether two strings are equal to each other.
         */
        bool operator==(String const &rhs) const;
        
        /**
         * Not equals to operator.
         * @param rhs The right hand side of the operator.
         * @return Whether two strings are not equal to each other.
         */
        bool operator!=(String const &rhs) const;
        
        /**
         * The addition assignment operator.
         * @param rhs The right hand side of the operator.
         * @return A reference to this string object.
         */
        String &operator+=(String const &rhs);
        
        /**
         * The less than operator.
         * @param rhs The right hand side of the operator.
         * @return Whether this string is less than the right hand side string.
         */
        bool operator<(String const &rhs) const;

        /**
         * Gets a value indicating whether this is a empty string.
         * @return a value indicating whether this is a empty string.
         */
        bool Empty() const;

        /**
         * Gets a value indicating the length of this string.
         * @return a value indicating the length of this string.
         */
        size_t Length() const;

        /**
         * Generates a null-terminated sequence of wide characters with the same content as the string object and 
         * returns it as a pointer to an array of characters.
         * @return A null-terminated sequence of wide characters representing the string content. 
         */
        wchar_t const *CStr() const;

        /**
         * Generates a null-terminated sequence of ANSI characters with the same content as the string object and 
         * returns it as a pointer to an array of characters.
         * @return A null-terminated sequence of ANSI characters representing the string content. 
         */
        char const *UTF8CStr() const;

        /**
         * Compares the content of this object to the content of a comparing string.
         * @param pos1 Position of the beginning of the compared substring.
         * @param n1 Length of the compared substring.
         * @param str String object with the content to be used as comparing string.
         * @return 0 if the compared characters sequences are equal, otherwise a number different from 0 is returned, 
         * with its sign indicating whether the object is considered greater than the comparing string passed as 
         * parameter (positive sign), or smaller (negative sign).
         */
        int Compare(size_t pos1, size_t n1, String const &str) const;

        /**
         * Searches the string for the content specified in either str, s or c, and returns the position of the first 
         * occurrence in the string.
         * @param str String to be searched for in the object. 
         * @param pos Position of the first character in the string to be taken into consideration for possible matches.
         * @return The position of the first occurrence in the string of the searched content. If the content is not 
         * found, the member value npos is returned.
         */
        size_t Find(String const &str, size_t pos = 0) const;

        /**
         * Maximum value for size_t.
         */
        static const size_t npos = static_cast<size_t>(-1);
    private:
        std::auto_ptr<StringImpl> const impl_;
        mutable scoped_array<char> tmp_;
    };

    /**
     * Addition operator.
     * 
     * @param lhs The left hand side string object.
     * @param rhs The right hand side string object.
     * @return A String object that contains the result of string concatenation. 
     */
    BADUMNA_API String operator+(String const &lhs, String const &rhs);
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
namespace Badumna
{
#ifndef WIN32
    template<typename T>
    void UTF32ToUTF16(wchar_t *src, T* dest, size_t len)
    {
        BADUMNA_STATIC_ASSERT(sizeof(wchar_t) == 4);
        BADUMNA_STATIC_ASSERT(sizeof(T) == 2);

        for(size_t i = 0; i < len; i++)
        {
            dest[i] = (T)src[i];
        }

        dest[len] = 0;
    }

    template<typename T>
    void UTF16ToUTF32(T *src, wchar_t *dest, size_t len)
    {
        BADUMNA_STATIC_ASSERT(sizeof(wchar_t) == 4);
        BADUMNA_STATIC_ASSERT(sizeof(T) == 2);

        for(size_t i = 0; i < len; i++)
        {
            dest[i] = (wchar_t)src[i];
        }
        
        dest[len] = 0;
    }
#endif // WIN32
}
#endif // DOXYGEN_SHOULD_SKIP_THIS

#endif // BADUMNA_STRING_H