//---------------------------------------------------------------------------------
// <copyright file="OutputStream.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include "Badumna/DataTypes/OutputStream.h"

namespace Badumna
{
    OutputStream::OutputStream() 
        : output_stream_(std::ios_base::binary | std::ios_base::out | std::ios::in)
    {
    }

    int OutputStream::GetBuffer(char *buffer, size_t length)
    {
        assert(buffer != NULL && length != 0);

        if(length < GetLength())
        {
            // buffer not long enough
            return -1;
        }

        output_stream_.read(buffer, GetLength());

        return GetLength();
    }

    size_t OutputStream::GetLength()
    {
        std::streamoff pos = output_stream_.tellp();
        if(pos == -1)
        {
            return 0;
        }
        else
        {
            return static_cast<size_t>(pos);
        }
    }
}