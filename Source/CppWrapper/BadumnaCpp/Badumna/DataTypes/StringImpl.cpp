//---------------------------------------------------------------------------------
// <copyright file="StringImpl.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include <cstring>
#include "Badumna/DataTypes/StringImpl.h"

namespace Badumna
{
    StringImpl::StringImpl()
        : str_()
    {
    }
    
    StringImpl::StringImpl(char const *s)
        : str_()
    {
        assert(s != NULL);
        size_t len = strlen(s);
        str_.resize(len);
        
        for(size_t i = 0; i < len; i++)
        {
            str_[i] = s[i];
        }
    }
    
    StringImpl::StringImpl(wchar_t const *s)
        : str_(s)
    {
        assert(s != NULL);
    }
        
    StringImpl::StringImpl(StringImpl const &other)
        : str_(other.str_)
    {
    }

    bool StringImpl::Empty() const
    {
        return str_.size() == 0;
    }
    
    StringImpl &StringImpl::operator=(StringImpl const &rhs)
    {
        if(this != &rhs)
        {
            str_ = rhs.str_;
        }

        return *this;
    }

    bool StringImpl::operator==(StringImpl const &rhs) const
    {
        return str_ == rhs.str_;
    }

    bool StringImpl::operator!=(StringImpl const &rhs) const
    {
        return str_ != rhs.str_;
    }

    StringImpl &StringImpl::operator+=(StringImpl const &rhs)
    {
        str_ += rhs.str_;
        return *this;
    }

    bool StringImpl::operator<(StringImpl const &rhs) const
    {
        return str_ < rhs.str_;
    }

    wchar_t const *StringImpl::CStr() const
    {
        return str_.c_str();
    }

    size_t StringImpl::Length() const
    {
        return str_.length();
    }

    int StringImpl::Compare(size_t pos1, size_t n1, StringImpl const &s) const
    {
        return str_.compare(pos1, n1, s.str_);
    }

    size_t StringImpl::Find(StringImpl const &s, size_t pos) const
    {
        return str_.find(s.str_, pos);
    }
}