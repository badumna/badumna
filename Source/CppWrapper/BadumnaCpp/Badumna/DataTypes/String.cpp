//---------------------------------------------------------------------------------
// <copyright file="String.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include "Badumna/DataTypes/StringImpl.h"
#include "Badumna/DataTypes/String.h"

namespace Badumna
{
    String::String()
        : impl_(new StringImpl()), 
        tmp_(NULL)
    {
    }
    
    String::String(char const *s)
        : impl_(new StringImpl(s)),
        tmp_(NULL)
    {
        assert(s != NULL);
    }
    
    String::String(wchar_t const *s)
        : impl_(new StringImpl(s)),
        tmp_(NULL)
    {
        assert(s != NULL);
    }
        
    String::String(String const &other)
        : impl_(new StringImpl(*(other.impl_))),
        tmp_(NULL)
    {
    }
    
    String& String::operator=(String const &rhs)
    {
        if(this != &rhs)
        {
            (*impl_) = *(rhs.impl_);
        }

        return *this;
    }

    String::~String()
    {
    }

    bool String::Empty() const
    {
        return impl_->Empty();
    }

    bool String::operator==(String const &rhs) const
    {
        return (*impl_) == *(rhs.impl_);
    }

    bool String::operator!=(String const &rhs) const
    {
        return !operator==(rhs);
    }
    
    String &String::operator+=(String const &rhs)
    {
        (*impl_) += *(rhs.impl_);

        return *this;
    }

    bool String::operator<(String const &rhs) const
    {
        return (*impl_) < *(rhs.impl_);
    }

    wchar_t const *String::CStr() const
    {
        return impl_->CStr();
    }

    char const *String::UTF8CStr() const
    {
        wchar_t const *data = impl_->CStr();
        tmp_.reset(new char[impl_->Length() + 1]);

        for(size_t i = 0; i < impl_->Length(); ++i)
        {
            (tmp_.get())[i] = static_cast<char>(data[i]);
        }

        (tmp_.get())[impl_->Length()] = 0;

        return tmp_.get();
    }

    String operator+(String const &lhs, String const &rhs)
    {
        String result(lhs);
        result.operator+= (rhs);
        return result;
    }

    int String::Compare(size_t pos1, size_t n1, String const &str) const
    {
        return impl_->Compare(pos1, n1, *(str.impl_));
    }

    size_t String::Length() const
    {
        return impl_->Length();
    }

    size_t String::Find(String const &str, size_t pos) const
    {
        return impl_->Find(*(str.impl_), pos);
    }
}