//---------------------------------------------------------------------------------
// <copyright file="BooleanArray.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_BOOLEAN_ARRAY_H
#define BADUMNA_BOOLEAN_ARRAY_H

#include <memory>
#include <iosfwd>

#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class BooleanArrayImpl;

    /**
     * @class BooleanArray
     *
     * @brief An automatically sized array of booleans. 
     *
     * BooleanArray is an automatically sized array of booleans, it supports a default value for elements which are not 
     * explicitly set.  
     */
    class BADUMNA_API BooleanArray
    {
        friend class ManagedObjectGuard;
        friend class BadumnaRuntime;
    public:
        /**
         * Constructor that constructs a new BooleanArray with all elements initialized to false.
         */
        BooleanArray();

        /**
         * Constructor that constructs a new BooleanArray with all elements initialized to the specified value.
         * @param val The initial value.
         */
        explicit BooleanArray(bool val);

        /**
         * Constructor that constructs a new BooleanArray with the specified indexes set to true and the remaining
         * elements set to false.
         *
         * @param indexes An int array consist the element index that need to be set to true initially.
         * @param length length of int array.
         */
        BooleanArray(int const *indexes, size_t length);

        /**
         * Copy constructor that constructs a new BooleanArray from another BooleanArray object.
         *
         * @param other The BooleanArray to copy from.
         */
        BooleanArray(BooleanArray const &other);

        /**
         * Destructor.
         */
        ~BooleanArray();

        /**
         * Assignment operator. 
         *
         * @param rhs The right hand side parameter of the assignment operation. 
         * @return A reference of this BooleanArray object. 
         */
        BooleanArray& operator=(BooleanArray const &rhs);

        /**
         * Sets the element at the specified index.
         *
         * @param index The index to set.
         * @param val The value to set. 
         */
        void Set(size_t index, bool val);

        /**
         * Gets the element at the specified index.
         *
         * @param index The index.
         * @return The element at the specified index.
         */
        bool Get(size_t index) const;
        
        /**
         * The index operator.
         *
         * @param index The index.
         * @return The element at the specified index.
         */
        bool operator[](size_t index) const;

        /**
         * Gets the highest index actually referenced (directly or indirectly, e.g. via an Or). A value less than 0 
         * indicates that no index has been referenced yet.
         *
         * @return The highest used index. 
         */
        size_t HighestUsedIndex() const;

        /**
         * Set all elements of the BooleanArray to the specified value.
         *
         * @param val The value to set for all elements.
         */
        void SetAll(bool val);

        /**
         * Get a boolean valud indicating whether any of the element in the array is true. 
         *
         * @return true if any element of the array is true, false otherwise.
         */
        bool Any() const;

        /**
         * Performs an elementwise logical OR with another BooleanArray, storing the result in this BooleanArray.
         *
         * @param other The other BooleanArray to 'OR' with.
         */
        void Or(BooleanArray const &other);

        /**
         * Returns a string representing the contents of the instance.
         *
         * @return A string object that contains the string representation of this BooleanArray object. 
         */
        String ToString() const;

        /**
         * Attempts to construct a BooleanArray initialized according to the given string. The string should be in the 
         * same format as returned by the ToString() method.
         * 
         * @param bits The string to parse.
         * @return A BooleanArray initialized according to bits.
         */
        static BooleanArray TryParse(String const &bits);

    private:
        explicit BooleanArray(BooleanArrayImpl *impl);
        std::auto_ptr<BooleanArrayImpl> const impl_;
    };
    
    /**
     * Insertion operator.
     * 
     * @param out The output stream reference. 
     * @param boolean_array The BooleanArray object. 
     * @return A reference to the specified ostream object.
     */
    BADUMNA_API std::wostream& operator<<(std::wostream& out, const Badumna::BooleanArray& boolean_array);
}

#endif // BADUMNA_BOOLEAN_ARRAY_H