//---------------------------------------------------------------------------------
// <copyright file="Vector3.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cmath>
#include <iostream>

#include <sstream>
#include "Badumna/DataTypes/Vector3.h"

namespace Badumna
{
    Vector3::Vector3() 
        : x_(0.0f), 
        y_(0.0f), 
        z_(0.0f)
    {
    }

    Vector3::Vector3(float x, float y, float z)
        : x_(x),
        y_(y),
        z_(z)
    {
    }

    Vector3::Vector3(Vector3 const &other)
        : x_(other.GetX()), 
        y_(other.GetY()),
        z_(other.GetZ())
    {
    }

    Vector3& Vector3::operator=(Vector3 const &rhs)
    {
        if(this != &rhs)
        {
            SetX(rhs.GetX());
            SetY(rhs.GetY());
            SetZ(rhs.GetZ());
        }

        return *this;
    }

    float Vector3::Magnitude() const
    {
        return sqrt(x_ * x_ + y_ * y_ + z_ * z_);
    }

    String Vector3::ToString() const
    {
        std::wstringstream ss;
        ss << '(' << x_ << ',' << y_ << ',' << z_ << ')';
        return ss.str().c_str();
    }

    Vector3 &Vector3::operator+= (Vector3 const &rhs)
    {
        SetX(GetX() + rhs.GetX());
        SetY(GetY() + rhs.GetY());
        SetZ(GetZ() + rhs.GetZ());

        return *this;
    }

    Vector3 &Vector3::operator-= (Vector3 const &rhs)
    {
        SetX(GetX() - rhs.GetX());
        SetY(GetY() - rhs.GetY());
        SetZ(GetZ() - rhs.GetZ());

        return *this;
    }

    Vector3 &Vector3::operator*= (float rhs)
    {
        SetX(GetX() * rhs);
        SetY(GetY() * rhs);
        SetZ(GetZ() * rhs);

        return *this;
    }

    Vector3 &Vector3::operator/= (float rhs)
    {
        SetX(GetX() / rhs);
        SetY(GetY() / rhs);
        SetZ(GetZ() / rhs);

        return *this;
    }

    std::wostream& operator<<(std::wostream& out, Vector3 const &vector)
    {
        std::wstring ws(vector.ToString().CStr());
        out << ws;
        return out;
    }

    Vector3 operator* (Vector3 const &lhs, float rhs)
    {
        Vector3 result(lhs);
        result.operator*= (rhs);
        return result;
    }

    Vector3 operator* (float lhs, Vector3 const& rhs)
    {
        Vector3 result(rhs);
        result.operator*= (lhs);
        return result;
    }

    Vector3 operator/ (Vector3 const &lhs, float rhs)
    {
        Vector3 result(lhs);
        result.operator/= (rhs);
        return result;
    }

    Vector3 operator+ (Vector3 const &lhs, Vector3 const &rhs)
    {
        Vector3 result(lhs);
        result.operator+=(rhs);
        return result;
    }

    Vector3 operator- (Vector3 const &lhs, Vector3 const &rhs)
    {
        Vector3 result(lhs);
        result.operator-= (rhs);
        return result;
    }

    bool Vector3::operator== (Vector3 const &rhs) const
    {
        return (fabs(GetX() - rhs.GetX()) < 1E-5) &&
            (fabs(GetY() - rhs.GetY()) < 1E-5) &&
            (fabs(GetZ() - rhs.GetZ()) < 1E-5);
    }

    bool Vector3::operator!= (Vector3 const &rhs) const
    {
        return !operator==(rhs);
    }
}