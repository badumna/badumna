//---------------------------------------------------------------------------------
// <copyright file="BadumnaId.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_BADUMNA_ID_H
#define BADUMNA_BADUMNA_ID_H

#include <memory>
#include <iosfwd>

#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class BadumnaIdImpl;
    
    /**
     * @class BadumnaId
     * @brief A global unique id. 
     *
     * BadumnaId is the globally unique object/entity identifier used in the Badumna network suite.
     */
    class BADUMNA_API BadumnaId
    {
        friend class BadumnaRuntime;
        friend class MatchImpl;
        friend class ManagedObjectGuard;
    public:
        /**
         * Copy constructor that construct a BadumnaId object according to another BadumnaId object. 
         * 
         * @param other Another BadumnaId object to copy from. 
         */
        BadumnaId(BadumnaId const &other);

        /**
         * Destructor.
         */
        virtual ~BadumnaId();

        /**
         * Gets an empty and invalid identifier.
         *
         * @return An empty and invalid id.
         */
        static BadumnaId None();

        /**
         * Gets a boolean value indicating whether this object is valid or not. 
         * 
         * @return A boolean value set to whether this BadumnaId is valid or not.  
         */
        bool IsValid() const;

        /**
         * Renders a BadumnaId as a string for debugging purposes.  No guarantee is made that the format
         * of this string will not change. BadumnaId should be treated as an opaque identifier.
         * 
         * @return A string object that contains the string representation of the BadumnaId object. 
         */
        String ToString() const;

        /**
         * Creates a unique string ID that will remain invariant.
         *
         * @return A unique string that will remain invariant.
         */
        String ToInvariantIDString() const;

        /**
         * The assignment operator. 
         * 
         * @param rhs The right hand side BadumnaId object in the assignment operation.
         * @return A reference of this BadumnaId object. 
         */
        BadumnaId& operator=(BadumnaId const &rhs);

        /**
         * The equal to operator that determines whether this BadumnaId object equals to another BadumnaId object.
         * 
         * @param rhs The right hand side BadumnaId object.
         * @return A boolean value set to whether two BadumnaId objects equals to each other. 
         */
        bool operator ==(BadumnaId const &rhs) const;

        /**
         * The not equal to operator that determines whether this BadumnaId object not equals to another BadumnaId 
         * object.
         * 
         * @param rhs The right hand side BadumnaId object.
         * @return A boolean value set to whether two BadumnaId objects not equal to each other. 
         */
        bool operator !=(BadumnaId const &rhs) const;
    
    protected:
        void SetBadumnaIdImplImpl(BadumnaIdImpl *p);
        explicit BadumnaId(BadumnaIdImpl *p);

        std::auto_ptr<BadumnaIdImpl> impl_;
    };

    /**
     * Insertion operator.
     * 
     * @param out The output stream reference. 
     * @param id The BadumnaId object. 
     * @return A reference to the specified ostream object.
     */
    BADUMNA_API std::wostream& operator<<(std::wostream& out, Badumna::BadumnaId const& id);
}

#endif // BADUMNA_BADUMNA_ID_H