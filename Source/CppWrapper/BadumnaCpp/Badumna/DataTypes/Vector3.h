//---------------------------------------------------------------------------------
// <copyright file="Vector3.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_VECTOR3_H
#define BADUMNA_VECTOR3_H

#include <iosfwd>

#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    /**
     * @class Vector3
     * @brief A 3 dimensional vector. 
     *
     * Vector3 implements a 3 dimensional vector. 
     */
    class BADUMNA_API Vector3
    {
    public:
        /**
         * Constructor that constructs a Vector3 object with default value. 
         */
        Vector3();

        /**
         * Constructor that constructs a Vector3 object with specified values. 
         * @param x The x value.
         * @param y The y value.
         * @param z The z value.
         */
        Vector3(float x, float y, float z);

        /**
         * Copy constructor that constructs a Vector3 object from another Vector3 object.
         *
         * @param other The Vector3 object to copy from. 
         */
        Vector3(Vector3 const &other);

        /**
         * Assignment operator.
         *
         * @param rhs The right hand side parameter of the assignment operation. 
         * @return The reference to this Vector3 object.
         */
        Vector3 &operator=(Vector3 const &rhs);

        /**
         * Gets the X component of the vector.
         *
         * @return The x value.
         */
        inline float GetX() const
        {
            return x_;
        }

        /** 
         * Sets the X component of the vector.
         *
         * @param val The x value.
         */
        inline void SetX(float val)
        {
            x_ = val;
        }

        /**
         * Gets the Y component of the vector.
         *
         * @return The y value.
         */
        inline float GetY() const
        {
            return y_;
        }

        /** 
         * Sets the Y component of the vector.
         *
         * @param val The y value.
         */
        inline void SetY(float val)
        {
            y_ = val;
        }

        /**
         * Gets the Z component of the vector.
         *
         * @return The z value.
         */
        inline float GetZ() const
        {
            return z_;
        }

        /** 
         * Sets the Z component of the vector.
         *
         * @param val The z value.
         */
        inline void SetZ(float val)
        {
            z_ = val;
        }

        /**
         * Gets the length of the vector.
         *
         * @return The length of the vector.
         */
        float Magnitude() const;

        /**
         * Dot product of this and the given vector.
         *
         * @param other given vector.
         * @return The dot product.
         */
        float Dot(Vector3 const &other) const;

        /**
         * Dot product of two given vectors.
         *
         * @param vectorA Vector A
         * @param vectorB Vector B
         * @return The dot product of Vector A and B.
         */
        static float DotProduct(Vector3 const &vectorA, Vector3 const &vectorB);

        /**
         * Returns a human readable string represenation of this vector.
         * 
         * @return A string object that contains the string representation of the vector.
         */
        String ToString() const;

        /**
         * Multiplication assignment operator.
         *
         * @param value Multiplies the vector by the value.
         * @return A reference to this vector object.
         */
        Vector3 &operator*= (float value);

        /**
         * Division assignment operator.
         *
         * @param value Divides the vector by the value.
         * @return A reference to this vector object.
         */
        Vector3 &operator/= (float value);
        
        /**
         * Addition assignment operator.
         *
         * @param rhs Adds the value of the rhs vector to this vector.
         * @return A reference to this vector object.
         */
        Vector3 &operator+= (Vector3 const &rhs);
        
        /** 
         * Subtraction assignment operator.
         *
         * @param rhs Subtracts the value of the rhs vector from this vector.
         * @return A reference to this vector object.
         */
        Vector3 &operator-= (Vector3 const &rhs);

        /**
         * Equal to operator.
         *
         * @param rhs The right hand side variable in the equal to comparison.
         * @return A boolean value indicating whether two vectors are equal.
         */
        bool operator== (Vector3 const &rhs) const;
        
        /**
         * Not equal to operator.
         * 
         * @param rhs The right hand side variable in the not equal to comparison.
         * @return A boolean value indicating whether two vectors are not equal.
         */
        bool operator!= (Vector3 const &rhs) const;

    private:
        float x_;
        float y_;
        float z_;
    };

    /**
     * Insertion operator.
     * 
     * @param out The output stream reference. 
     * @param vector The Vector3 object. 
     * @return A reference to the specified ostream object.
     */
    BADUMNA_API std::wostream& operator<<(std::wostream &out, Vector3 const &vector);

    /**
     * Addition operator.
     * 
     * @param lhs The left hand side Vector3 object.
     * @param rhs The right hand side Vector3 object.
     * @return A Vector3 object that equals to the result of the addition. 
     */
    BADUMNA_API Vector3 operator+ (Vector3 const &lhs, Vector3 const &rhs);

    /**
     * Subtraction operator.
     * 
     * @param lhs The left hand side Vector3 object.
     * @param rhs The right hand side Vector3 object.
     * @return A Vector3 object that equals to the result of the subtraction. 
     */
    BADUMNA_API Vector3 operator- (Vector3 const &lhs, Vector3 const &rhs);
    
    /**
     * Multiplication operator.
     * 
     * @param lhs The left hand side Vector3 object.
     * @param rhs The right hand side Vector3 object.
     * @return A Vector3 object that equals to the result of the multiplication. 
     */
    BADUMNA_API Vector3 operator* (Vector3 const &lhs, float rhs);
    
    /**
     * Multiplication operator.
     * 
     * @param lhs The left hand side float number.
     * @param rhs The right hand side Vector3 object.
     * @return A Vector3 object that equals to the result of the multiplication. 
     */
    BADUMNA_API Vector3 operator* (float lhs, Vector3 const &rhs);
    
    /**
     * Division operator.
     * 
     * @param lhs The left hand side Vector3 object.
     * @param rhs The right hand side Vector3 object.
     * @return A Vector3 object that equals to the result of the division. 
     */
    BADUMNA_API Vector3 operator/ (Vector3 const &lhs, float rhs);
}

#endif // BADUMNA_VECTOR3_H