//---------------------------------------------------------------------------------
// <copyright file="BadumnaIdImpl.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include "Badumna/Core/Arguments.h"
#include "Badumna/Core/TypeRegistry.h"
#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/DataTypes/BadumnaIdImpl.h"
#include "Badumna/DataTypes/String.h"

namespace Badumna
{
    REGISTER_MANAGED_MONO_TYPE(DataTypes, BadumnaId);
    BadumnaIdImpl::BadumnaIdImpl(MonoRuntime *runtime, DotNetObject object) 
        : ProxyObject(runtime, object, "Badumna.DataTypes", "BadumnaId")
    {
    }

    BadumnaIdImpl::BadumnaIdImpl(
        MonoRuntime *runtime, 
        DotNetObject object, 
        NameType name_space, 
        NameType class_name) 
        : ProxyObject(runtime, object, name_space, class_name)
    {
    }

    BadumnaIdImpl *BadumnaIdImpl::None()
    {
        return BadumnaRuntime::Instance().CreateBadumnaIdImpl();
    }

    REGISTER_PROPERTY(DataTypes, BadumnaId, IsValid)
    bool BadumnaIdImpl::IsValid() const
    {
        return managed_object_.GetPropertyFromName<bool>("IsValid");
    }    

    REGISTER_MANAGED_METHOD(DataTypes, BadumnaId, ToInvariantIDString, 0)
    String BadumnaIdImpl::ToInvariantIDString() const
    {
        return managed_object_.InvokeMethod<String>("ToInvariantIDString");
    }

    REGISTER_MANAGED_METHOD(DataTypes, BadumnaId, Equals, 1)
    bool BadumnaIdImpl::operator==(BadumnaIdImpl const &rhs) const
    {
        Arguments args;
        args.AddArgument(rhs.managed_object_.GetManagedObject());
        
        bool result = managed_object_.InvokeMethod<bool>("Equals", args);
        return result;
    }

    bool BadumnaIdImpl::operator !=(BadumnaIdImpl const &rhs) const
    {
        return !BadumnaIdImpl::operator ==(rhs);
    }

    DEFINE_WRAPPER_CLASS_IMPL_COPYCTOR_ASSIGN_OPERATOR(BadumnaIdImpl)
}