//---------------------------------------------------------------------------------
// <copyright file="BooleanArray.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>

#include "Badumna/DataTypes/BooleanArray.h"
#include "Badumna/DataTypes/BooleanArrayImpl.h"
#include "Badumna/Core/BadumnaRuntime.h"

namespace Badumna
{
    BooleanArray::BooleanArray() 
        : impl_(BadumnaRuntime::Instance().CreateBooleanArrayImpl())
    {
    }

    BooleanArray::BooleanArray(BooleanArrayImpl *impl) 
        : impl_(impl)
    {
        assert(impl != NULL);
    }

    BooleanArray::BooleanArray(bool val) 
        : impl_(BadumnaRuntime::Instance().CreateBooleanArrayImpl(val))
    {
    }

    BooleanArray::BooleanArray(int const *indexes_set_to_true, size_t length) 
        : impl_(BadumnaRuntime::Instance().CreateBooleanArrayImpl(indexes_set_to_true, length))
    {
        assert(indexes_set_to_true != NULL && length > 0);
    }

    BooleanArray::~BooleanArray()
    {
    }

    void BooleanArray::Set(size_t index , bool val)
    {
        impl_->Set(index, val);
    }

    bool BooleanArray::Get(size_t index) const
    {
        return impl_->Get(index);
    }

    bool BooleanArray::operator[](size_t index) const
    {
        return Get(index);
    }

    size_t BooleanArray::HighestUsedIndex() const
    {
        return impl_->HighestUsedIndex();
    }

    void BooleanArray::SetAll(bool value)
    {
        return impl_->SetAll(value);
    }

    bool BooleanArray::Any() const
    {
        return impl_->Any();
    }

    void BooleanArray::Or(BooleanArray const &other) 
    {
        impl_->Or(*(other.impl_));
    }

    String BooleanArray::ToString() const
    {
        return impl_->ToString();
    }

    BooleanArray BooleanArray::TryParse(String const &bits)
    {
        return BooleanArray(BooleanArrayImpl::TryParse(bits));
    }

    std::wostream& operator<<(std::wostream& out, BooleanArray const &boolean_array)
    {
        std::wstring ws(boolean_array.ToString().CStr());
        out << ws;
        return out;
    }

    DEFINE_WRAPPER_CLASS_COPYCTOR_ASSIGN_OPERATOR(BooleanArray, BooleanArrayImpl, impl_, other)
}