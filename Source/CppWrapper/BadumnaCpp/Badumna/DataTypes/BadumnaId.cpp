//---------------------------------------------------------------------------------
// <copyright file="BadumnaId.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include <sstream>

#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/DataTypes/BadumnaIdImpl.h"

namespace Badumna
{
    BadumnaId::BadumnaId(BadumnaIdImpl *badumna_id_impl) 
        : impl_(badumna_id_impl)
    {
    }

    BadumnaId::~BadumnaId()
    {
    }

    void BadumnaId::SetBadumnaIdImplImpl(BadumnaIdImpl *p)
    {
        assert(p != NULL);
        impl_.reset(p);
    }

    BadumnaId BadumnaId::None()
    {
        return BadumnaId(BadumnaIdImpl::None());
    }

    bool BadumnaId::IsValid() const
    {
        return impl_->IsValid();
    }

    String BadumnaId::ToString() const
    {
        return impl_->ToString();
    }

    String BadumnaId::ToInvariantIDString() const
    {
        return impl_->ToInvariantIDString();
    }

    bool BadumnaId::operator ==(BadumnaId const &rhs) const
    {
        return impl_->operator ==(*(rhs.impl_));
    }

    bool BadumnaId::operator !=(BadumnaId const &rhs) const
    {
        return !BadumnaId::operator ==(rhs);
    }

    std::wostream& operator<<(std::wostream& out, BadumnaId const &id)
    {
        std::wstring ws(id.ToString().CStr());
        out << ws;
        return out;
    }

    DEFINE_WRAPPER_CLASS_COPYCTOR_ASSIGN_OPERATOR(BadumnaId, BadumnaIdImpl, impl_, other)
}