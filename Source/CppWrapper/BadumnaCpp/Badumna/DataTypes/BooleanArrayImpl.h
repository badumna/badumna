//---------------------------------------------------------------------------------
// <copyright file="BooleanArrayImpl.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_BOOLEAN_ARRAY_IMPL_H
#define BADUMNA_BOOLEAN_ARRAY_IMPL_H

#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Core/ProxyObject.h"
#include "Badumna/Core/ProxyObjectHelper.h"
#include "Badumna/DataTypes/String.h"

namespace Badumna
{
    class BooleanArrayImpl : public ProxyObject
    {
        friend class BadumnaRuntime;
    public:
        void Set(size_t index , bool val);
        bool Get(size_t index) const;
        size_t HighestUsedIndex() const;
        void SetAll(bool val);
        bool Any() const;
        void Or(BooleanArrayImpl const &other) const;

        static BooleanArrayImpl* TryParse(String const &bits);

        DECLARE_COPY_AND_ASSIGN(BooleanArrayImpl);
    private:
        explicit BooleanArrayImpl(MonoRuntime *runtime);

        BooleanArrayImpl(MonoRuntime *runtime, DotNetObject object);
        BooleanArrayImpl(MonoRuntime *runtime, bool val);
        BooleanArrayImpl(MonoRuntime *runtime, int const *indexes_set_to_true, size_t length);
    };
}

#endif // BADUMNA_BOOLEAN_ARRAY_IMPL_H