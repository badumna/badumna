//---------------------------------------------------------------------------------
// <copyright file="OutputStream.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_OUTPUT_STREAM_H
#define BADUMNA_OUTPUT_STREAM_H

#include <sstream>
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    /**
     * @class OutputStream
     * @brief An output stream of bytes
     */
    class BADUMNA_API OutputStream
    {
    public:
        /**
         * Constructor.
         */
        OutputStream();

        /**
         * Insertion operator.
         * 
         * @tparam T type of the value to insert into the stream.
         * @param val The value to be inserted into the stream.
         * @return The reference of this stream.
         */
        template<typename T>
        OutputStream& operator<<(T const &val)
        {
            BADUMNA_STATIC_ASSERT(IsFundamentalType<T>::IsTrue);

            output_stream_.write(reinterpret_cast<char *>(const_cast<T *>(&val)), sizeof(T));
            return *this;
        }

        /**
         * Get the content of the stream. 
         *
         * @param buffer The buffer to hold the content.
         * @param length The length of the buffer.
         * @return The number of bytes written into the buffer, or -1 on error. 
         */
        int GetBuffer(char *buffer, size_t length);

        /**
         * Gets the number of bytes in the stream.
         *
         * @return The number of bytes in the stream. 
         */
        size_t GetLength();
    private:
        std::stringstream output_stream_;

        DISALLOW_COPY_AND_ASSIGN(OutputStream);
    };
}

#endif // BADUMNA_OUTPUT_STREAM_H