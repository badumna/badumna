//---------------------------------------------------------------------------------
// <copyright file="BadumnaIdImpl.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_BADUMNA_ID_IMPL_H
#define BADUMNA_BADUMNA_ID_IMPL_H

#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Core/ProxyObject.h"
#include "Badumna/Core/ProxyObjectHelper.h"
#include "Badumna/Core/MonoRuntime.h"
#include "Badumna/DataTypes/String.h"

namespace Badumna
{
    class BadumnaIdImpl : public ProxyObject
    {
        friend class BadumnaId;
        friend class BadumnaRuntime;
    public: 
        bool IsValid() const;       
        String ToInvariantIDString() const;

        bool operator ==(BadumnaIdImpl const &rhs) const;
        bool operator !=(BadumnaIdImpl const &rhs) const;

        DECLARE_COPY_AND_ASSIGN(BadumnaIdImpl);
    protected:
        BadumnaIdImpl(MonoRuntime *runtime, DotNetObject object);

        BadumnaIdImpl(
            MonoRuntime *runtime, 
            DotNetObject object, 
            NameType name_space, 
            NameType class_name);

        static BadumnaIdImpl *None();
    };
}

#endif // BADUMNA_BADUMNA_ID_IMPL_H