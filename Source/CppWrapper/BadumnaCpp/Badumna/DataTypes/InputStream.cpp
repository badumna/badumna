//---------------------------------------------------------------------------------
// <copyright file="InputStream.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include <string>

#include "Badumna/DataTypes/InputStream.h"

namespace Badumna
{
    InputStream::InputStream(char const *data, size_t length) 
        : input_stream_(std::string(data, data + length), std::ios_base::binary | std::ios_base::in)
    {
        assert(data != NULL && length > 0);
    }
}