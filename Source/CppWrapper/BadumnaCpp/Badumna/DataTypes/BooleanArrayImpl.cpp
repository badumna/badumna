//---------------------------------------------------------------------------------
// <copyright file="BooleanArrayImpl.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include "Badumna/Core/Arguments.h"
#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/Core/TypeRegistry.h"
#include "Badumna/DataTypes/BooleanArrayImpl.h"

namespace Badumna
{
    REGISTER_MANAGED_MONO_TYPE(Utilities, BooleanArray);
    BooleanArrayImpl::BooleanArrayImpl(MonoRuntime *runtime) 
        : ProxyObject(runtime, "Badumna.Utilities", "BooleanArray")
    {
    }

    BooleanArrayImpl::BooleanArrayImpl(MonoRuntime *runtime, DotNetObject object) 
        : ProxyObject(runtime, object, "Badumna.Utilities", "BooleanArray")
    {
    }

    BooleanArrayImpl::BooleanArrayImpl(MonoRuntime *runtime, bool val) 
        : ProxyObject(runtime, "Badumna.Utilities", "BooleanArray")
    {
        SetAll(val);
    }

    BooleanArrayImpl::BooleanArrayImpl(MonoRuntime *runtime, int const *indexes_set_to_true, size_t length) 
        : ProxyObject(runtime, "Badumna.Utilities", "BooleanArray")
    {
        assert(indexes_set_to_true != NULL);

        for(size_t i = 0; i < length; ++i)
        {
            Set(indexes_set_to_true[i], true);
        }
    }

    void BooleanArrayImpl::Set(size_t index , bool val)
    {
        Arguments args;
        args.AddArgument(&index);
        args.AddArgument(&val);
        managed_object_.SetPropertyValue("Item", args);
    }

    REGISTER_PROPERTY(Utilities, BooleanArray, Item)
    bool BooleanArrayImpl::Get(size_t index) const
    {
        Arguments args;
        args.AddArgument(&index);
        return managed_object_.GetPropertyFromName<bool>("Item", args);
    }

    REGISTER_PROPERTY(Utilities, BooleanArray, HighestUsedIndex)
    size_t BooleanArrayImpl::HighestUsedIndex() const
    {
        return managed_object_.GetPropertyFromName<size_t>("HighestUsedIndex");
    }

    REGISTER_MANAGED_METHOD(Utilities, BooleanArray, SetAll, 1)
    void BooleanArrayImpl::SetAll(bool value)
    {
        Arguments args;
        args.AddArgument(&value);
        managed_object_.InvokeVirtualMethod("SetAll", args);
    }

    REGISTER_MANAGED_METHOD(Utilities, BooleanArray, Any, 0)
    bool BooleanArrayImpl::Any() const
    {
        return managed_object_.InvokeMethod<bool>("Any");
    }

    REGISTER_MANAGED_METHOD(Utilities, BooleanArray, Or, 1)
    void BooleanArrayImpl::Or(BooleanArrayImpl const &other) const
    {
        Arguments args;
        args.AddArgument(other.GetManagedMonoObject());
        managed_object_.InvokeVirtualMethod("Or", args);
    }

    BooleanArrayImpl* BooleanArrayImpl::TryParse(String const &bits)
    {
        return BadumnaRuntime::Instance().CreateBooleanArrayImpl(bits);
    }
    
    DEFINE_WRAPPER_CLASS_IMPL_COPYCTOR_ASSIGN_OPERATOR(BooleanArrayImpl)
}