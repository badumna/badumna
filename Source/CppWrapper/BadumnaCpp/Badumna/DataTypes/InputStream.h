//---------------------------------------------------------------------------------
// <copyright file="InputStream.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_INPUT_STREAM_H
#define BADUMNA_INPUT_STREAM_H

#include <sstream>

#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    /**
     * @class InputStream
     * @brief An input stream of bytes
     */
    class BADUMNA_API InputStream
    {
    public:
        /**
         * Constructor.
         *
         * @param data Byte array.
         * @param length The length of the array.
         */
        InputStream(char const *data, size_t length);

        /**
         * Extract value of T from the stream. 
         *
         * @tparam T The type of the value to read.
         * @param val The destination.
         * @return The reference of this object. 
         */
        template<typename T>
        InputStream& operator>>(T &val)
        {
            BADUMNA_STATIC_ASSERT(IsFundamentalType<T>::IsTrue);
    
            input_stream_.read(reinterpret_cast<char *>(&val), sizeof(T));
            return *this;
        }

    private:
        std::stringstream input_stream_;

        DISALLOW_COPY_AND_ASSIGN(InputStream);
    };
}

#endif // BADUMNA_INPUT_STREAM_H