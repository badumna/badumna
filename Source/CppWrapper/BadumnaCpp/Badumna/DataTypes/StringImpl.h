//---------------------------------------------------------------------------------
// <copyright file="StringImpl.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_STRING_IMPL_H
#define BADUMNA_STRING_IMPL_H

#include <string>

namespace Badumna
{
    class StringImpl
    {
    public:
        StringImpl();
        StringImpl(char const *s);
        StringImpl(wchar_t const *s);
        
        StringImpl(StringImpl const &other);
        StringImpl &operator=(StringImpl const &rhs);

        bool operator==(StringImpl const &rhs) const;
        bool operator!=(StringImpl const &rhs) const;
        StringImpl &operator+=(StringImpl const &rhs);
        bool operator<(StringImpl const &rhs) const;

        bool Empty() const;
        size_t Length() const;

        int Compare(size_t pos1, size_t n1, StringImpl const &s) const;
        size_t Find(StringImpl const &str, size_t pos = 0) const;

        wchar_t const *CStr() const;
        
    private:
        std::wstring str_;
    };
}

#endif // BADUMNA_STRING_IMPL_H