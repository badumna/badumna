//-----------------------------------------------------------------------
// <copyright file="MatchImpl.cpp" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include "Badumna/Core/TypeRegistry.h"
#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/Core/ManagedObjectGuard.h"
#include "Badumna/Match/MatchImpl.h"
#include "Badumna/Match/MemberIdentityImpl.h"
#include "Badumna/DataTypes/BooleanArrayImpl.h"

namespace Badumna
{
    MatchImpl::MatchImpl(
        MonoRuntime *runtime,
        CreateMatchReplicaDelegate create_delegate,
        RemoveMatchReplicaDelegate remove_delegate,
        CreateMatchHostedReplicaDelegate create_hosted_delegate,
        RemoveMatchHostedReplicaDelegate remove_hosted_delegate)
        : ProxyObject(runtime),
        state_changed_delegate_(),
        member_added_delegate_(),
        member_removed_delegate_(),
        chat_message_delegate_(),
        create_delegate_(create_delegate),
        remove_delegate_(remove_delegate),
        create_hosted_delegate_(create_hosted_delegate),
        remove_hosted_delegate_(remove_hosted_delegate)
    {
    }

    REGISTER_MANAGED_MONO_TYPE(CppWrapperStub, MatchStub);
    void MatchImpl::Initialize(DotNetObject object)
    {
        managed_object_.Initialize(object, "Badumna.CppWrapperStub", "MatchStub");
    }

    REGISTER_PROPERTY(CppWrapperStub, MatchStub, Match)
    REGISTER_PROPERTY(Match, Match, MemberIdentity)
    std::auto_ptr<MemberIdentity> MatchImpl::MemberIdentity() const
    {
        ManagedObject managed_match(mono_runtime_, managed_object_.GetPropertyFromName("Match"));
        return std::auto_ptr< ::Badumna::MemberIdentity>(
            new ::Badumna::MemberIdentity(
                new MemberIdentityImpl(
                    mono_runtime_,
                    managed_match.GetPropertyFromName("MemberIdentity"))));
    }

    REGISTER_PROPERTY(Match, Match, MatchIdentifier)
    std::auto_ptr<BadumnaId> MatchImpl::MatchIdentifier() const
    {
        ManagedObject managed_match(mono_runtime_, managed_object_.GetPropertyFromName("Match"));
        return std::auto_ptr<BadumnaId>(
            new BadumnaId(
                BadumnaRuntime::Instance().CreateBadumnaIdImpl(managed_match.GetPropertyFromName("MatchIdentifier"))));
    }

    REGISTER_PROPERTY(CppWrapperStub, MatchStub, Members)
    std::vector<linked_ptr< ::Badumna::MemberIdentity> > MatchImpl::Members() const
    {
        std::vector<linked_ptr< ::Badumna::MemberIdentity> > results;
        ManagedObject managed_results(mono_runtime_, managed_object_.GetPropertyFromName("Members"));
        int32_t count = managed_results.GetPropertyFromName<int32_t>("Count");
        for (int32_t i = 0; i < count; ++i)
        {
            Arguments args;
            args.AddArgument(&i);
            DotNetObject result = managed_results.GetPropertyFromName("Item", args);
            results.push_back(
                linked_ptr< ::Badumna::MemberIdentity>(
                    new ::Badumna::MemberIdentity(
                        new MemberIdentityImpl(
                            mono_runtime_,
                            result))));
        }

        return results;
    }

    REGISTER_PROPERTY(Match, Match, MemberCount)
    int32_t MatchImpl::MemberCount() const
    {
        ManagedObject managed_match(mono_runtime_, managed_object_.GetPropertyFromName("Match"));
        return managed_match.GetPropertyFromName<int32_t>("MemberCount");
    }

    REGISTER_PROPERTY(Match, Match, State)
    MatchStatus MatchImpl::State() const
    {
        ManagedObject managed_match(mono_runtime_, managed_object_.GetPropertyFromName("Match"));
        return managed_match.GetPropertyFromName<MatchStatus>("State");
    }

    REGISTER_PROPERTY(Match, Match, Error)
    MatchError MatchImpl::Error() const
    {
        ManagedObject managed_match(mono_runtime_, managed_object_.GetPropertyFromName("Match"));
        return managed_match.GetPropertyFromName<MatchError>("Error");
    }
        
    void MatchImpl::SetStateChangedDelegate(MatchStateChangedDelegate handler)
    {
        state_changed_delegate_.reset(new MatchStateChangedDelegate(handler));
    }
    
    void MatchImpl::RemoveStateChangedDelegate()
    {
        state_changed_delegate_.reset();
    }

    void MatchImpl::SetMemberAddedDelegate(MatchMembershipDelegate handler)
    {
        member_added_delegate_.reset(new MatchMembershipDelegate(handler));
    }

    void MatchImpl::RemoveMemberAddedDelegate()
    {
        member_added_delegate_.reset();
    }

    void MatchImpl::SetMemberRemovedDelegate(MatchMembershipDelegate handler)
    {
        member_removed_delegate_.reset(new MatchMembershipDelegate(handler));
    }

    void MatchImpl::RemoveMemberRemovedDelegate()
    {
        member_removed_delegate_.reset();
    }

    void MatchImpl::SetChatMessageReceivedDelegate(MatchChatMessageDelegate handler)
    {
        chat_message_delegate_.reset(new MatchChatMessageDelegate(handler));
    }

    void MatchImpl::RemoveChatMessageReceivedDelegate()
    {
        chat_message_delegate_.reset();
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, MatchStub, RegisterEntity, 2)
    void MatchImpl::RegisterEntity(IMatchOriginal *original, uint32_t entity_type)
    {
        Arguments args;
        args.AddArgument(&original);
        args.AddArgument(&entity_type);
        managed_object_.InvokeMethod("RegisterEntity", args);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, MatchStub, UnregisterEntity, 1)
    void MatchImpl::UnregisterEntity(IMatchOriginal *original)
    {
        Arguments args;
        args.AddArgument(&original);
        managed_object_.InvokeMethod("UnregisterEntity", args);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, MatchStub, RegisterHostedEntity, 2)
    void MatchImpl::RegisterHostedEntity(IMatchHosted *hosted, uint32_t entity_type)
    {
        Arguments args;
        args.AddArgument(&hosted);
        args.AddArgument(&entity_type);
        managed_object_.InvokeMethod("RegisterHostedEntity", args);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, MatchStub, UnregisterHostedEntity, 1)
    void MatchImpl::UnregisterHostedEntity(IMatchHosted *hosted)
    {
        Arguments args;
        args.AddArgument(&hosted);
        managed_object_.InvokeMethod("UnregisterHostedEntity", args);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, MatchStub, FlagForUpdate, 2)
    void MatchImpl::FlagForUpdate(void *entity, BooleanArray const &changed_parts)
    {
        Arguments args;
        args.AddArgument(&entity);
        args.AddArgument(ManagedObjectGuard::GetManagedObject(changed_parts));
        managed_object_.InvokeMethod("FlagForUpdate", args);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, MatchStub, FlagIndexForUpdate, 2)
    void MatchImpl::FlagForUpdate(void *entity, int changed_part_index)
    {
        Arguments args;
        args.AddArgument(&entity);
        args.AddArgument(&changed_part_index);
        managed_object_.InvokeMethod("FlagIndexForUpdate", args);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, MatchStub, Chat, 1)
    void MatchImpl::Chat(String message)
    {
        Arguments args;
        args.AddArgument(MonoRuntime::GetString(message));
        managed_object_.InvokeMethod("Chat", args);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, MatchStub, Chat, 2)
    void MatchImpl::Chat(::Badumna::MemberIdentity const &member_id, String message)
    {
        Arguments args;
        args.AddArgument(ManagedObjectGuard::GetManagedObject(member_id));
        args.AddArgument(MonoRuntime::GetString(message));
        managed_object_.InvokeMethod("Chat", args);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, MatchStub, SendCustomMessageToReplicas, 2)
    void MatchImpl::SendCustomMessageToReplicas(void *entity, OutputStream *message_data)
    {
        Arguments args;
        args.AddArgument(&entity);

        size_t buffer_len = message_data->GetLength();
        scoped_array<char> buffer(new char[buffer_len]);
        message_data->GetBuffer(buffer.get(), buffer_len);
        args.AddArgument(MonoRuntime::GetMonoByteArray(buffer.get(), buffer_len));

        managed_object_.InvokeMethod("SendCustomMessageToReplicas", args);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, MatchStub, SendCustomMessageToOriginal, 2)
    void MatchImpl::SendCustomMessageToOriginal(void *entity, OutputStream *message_data)
    {
        Arguments args;
        args.AddArgument(&entity);

        size_t buffer_len = message_data->GetLength();
        scoped_array<char> buffer(new char[buffer_len]);
        message_data->GetBuffer(buffer.get(), buffer_len);
        args.AddArgument(MonoRuntime::GetMonoByteArray(buffer.get(), buffer_len));

        managed_object_.InvokeMethod("SendCustomMessageToOriginal", args);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, MatchStub, SendCustomMessageToMember, 2)
    void MatchImpl::SendCustomMessageToMember(::Badumna::MemberIdentity const &member_id, OutputStream *message_data)
    {
        Arguments args;
        args.AddArgument(ManagedObjectGuard::GetManagedObject(member_id));

        size_t buffer_len = message_data->GetLength();
        scoped_array<char> buffer(new char[buffer_len]);
        message_data->GetBuffer(buffer.get(), buffer_len);
        args.AddArgument(MonoRuntime::GetMonoByteArray(buffer.get(), buffer_len));

        managed_object_.InvokeMethod("SendCustomMessageToMember", args);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, MatchStub, SendCustomMessageToMembers, 1)
    void MatchImpl::SendCustomMessageToMembers(OutputStream *message_data)
    {
        Arguments args;
        size_t buffer_len = message_data->GetLength();
        scoped_array<char> buffer(new char[buffer_len]);
        message_data->GetBuffer(buffer.get(), buffer_len);
        args.AddArgument(MonoRuntime::GetMonoByteArray(buffer.get(), buffer_len));

        managed_object_.InvokeMethod("SendCustomMessageToMembers", args);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, MatchStub, SendCustomMessageToHost, 1)
    void MatchImpl::SendCustomMessageToHost(OutputStream *message_data)
    {
        Arguments args;
        size_t buffer_len = message_data->GetLength();
        scoped_array<char> buffer(new char[buffer_len]);
        message_data->GetBuffer(buffer.get(), buffer_len);
        args.AddArgument(MonoRuntime::GetMonoByteArray(buffer.get(), buffer_len));

        managed_object_.InvokeMethod("SendCustomMessageToHost", args);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, MatchStub, Leave, 0)
    void MatchImpl::Leave()
    {
        managed_object_.InvokeMethod("Leave", Arguments::None());
    }

    DotNetArray MatchImpl::Serialize(IMatchOriginal *original, DotNetObject managed_required_parts)
    {
        OutputStream output_stream;
        BooleanArray required_parts = BadumnaRuntime::Instance().CreateBooleanArray(managed_required_parts);
        original->Serialize(required_parts, &output_stream);
        return MonoRuntime::OutputStreamToMonoArray(&output_stream);
    }

    DotNetArray MatchImpl_Serialize(IMatchOriginal *original, DotNetObject required_parts)
    {
        return MatchImpl::Serialize(original, required_parts);
    }

    void MatchImpl::Deserialize(IMatchReplica *replica, DotNetObject managed_included_parts, DotNetArray data, int32_t estimated_milliseconds_since_departure)
    {
        std::pair<char *, size_t> array_data = MonoRuntime::GetArrayData(data);
        if (array_data.second == 0)
        {
            return;
        }

        scoped_array<char> buffer(array_data.first);
        BooleanArray included_parts = BadumnaRuntime::Instance().CreateBooleanArray(managed_included_parts);
        InputStream stream(buffer.get(), array_data.second);
        replica->Deserialize(included_parts, &stream, estimated_milliseconds_since_departure);
    }

    void MatchImpl_Deserialize(IMatchReplica *replica, DotNetObject included_parts, DotNetArray data, int32_t estimated_milliseconds_since_departure)
    {
        MatchImpl::Deserialize(replica, included_parts, data, estimated_milliseconds_since_departure);
    }
    
    DotNetArray MatchImpl_HostedSerialize(IMatchHosted *hosted, DotNetObject required_parts)
    {
        // This can just call Serialize(IMatchOriginal *, ...), but we need this separate
        // method so the C++ compiler knows that the pointer we have is a pointer to an IMatchHosted.
        return MatchImpl::Serialize(hosted, required_parts);
    }
    
    void MatchImpl_HostedDeserialize(IMatchHosted *hosted, DotNetObject included_parts, DotNetArray data, int32_t estimated_milliseconds_since_departure)
    {
        // This can just call Deserialize(IMatchReplica *, ...), but we need this separate
        // method so the C++ compiler knows that the pointer we have is a pointer to an IMatchHosted.
        MatchImpl::Deserialize(hosted, included_parts, data, estimated_milliseconds_since_departure);
    }

    void MatchImpl::HandleEvent(IMatchEntity *entity, DotNetArray data)
    {
        std::pair<char *, size_t> array_data = MonoRuntime::GetArrayData(data);
        scoped_array<char> buffer(array_data.first);
        InputStream stream(buffer.get(), array_data.second);
        entity->HandleEvent(&stream);
    }

    void MatchImpl_OriginalHandleEvent(IMatchOriginal *original, DotNetArray data)
    {
        MatchImpl::HandleEvent(original, data);
    }

    void MatchImpl_ReplicaHandleEvent(IMatchReplica *replica, DotNetArray data)
    {
        MatchImpl::HandleEvent(replica, data);
    }

    void MatchImpl_HostedHandleEvent(IMatchHosted *hosted, DotNetArray data)
    {
        MatchImpl::HandleEvent(hosted, data);
    }

    IMatchReplica *MatchImpl::InvokeMatchCreateReplicaDelegate(Match *match, uint32_t entity_type)
    {
        return match->impl_->create_delegate_(*match, entity_type);
    }

    IMatchReplica *MatchImpl_InvokeMatchCreateReplicaDelegate(Match *match, uint32_t entity_type)
    {
        return MatchImpl::InvokeMatchCreateReplicaDelegate(match, entity_type);
    }

    void MatchImpl::InvokeMatchRemoveReplicaDelegate(Match *match, IMatchReplica *replica)
    {
        match->impl_->remove_delegate_(*match, replica);
    }

    void MatchImpl_InvokeMatchRemoveReplicaDelegate(Match *match, IMatchReplica *replica)
    {
        return MatchImpl::InvokeMatchRemoveReplicaDelegate(match, replica);
    }
        
    IMatchHosted *MatchImpl::InvokeMatchCreateHostedReplicaDelegate(Match *match, uint32_t entity_type)
    {
        return match->impl_->create_hosted_delegate_(*match, entity_type);
    }

    IMatchHosted *MatchImpl_InvokeMatchCreateHostedReplicaDelegate(Match *match, uint32_t entity_type)
    {
        return MatchImpl::InvokeMatchCreateHostedReplicaDelegate(match, entity_type);
    }

    void MatchImpl::InvokeMatchRemoveHostedReplicaDelegate(Match *match, IMatchHosted *hosted)
    {
        match->impl_->remove_hosted_delegate_(*match, hosted);
    }

    void MatchImpl_InvokeMatchRemoveHostedReplicaDelegate(Match *match, IMatchHosted *hosted)
    {
        return MatchImpl::InvokeMatchRemoveHostedReplicaDelegate(match, hosted);
    }

    void MatchImpl::InvokeMatchStateChangedDelegate(Match *match, int32_t status, int32_t error)
    {
        MatchStateChangedDelegate *handler = match->impl_->state_changed_delegate_.get();
        if (handler != NULL)
        {
            (*handler)(*match, MatchStatusEventArgs(static_cast<MatchStatus>(status), static_cast<MatchError>(error)));
        }
    }

    void MatchImpl_InvokeMatchStateChangedDelegate(Match *match, int32_t status, int32_t error)
    {
        MatchImpl::InvokeMatchStateChangedDelegate(match, status, error);
    }

    void MatchImpl::InvokeMemberAddedDelegate(Match *match, DotNetObject managed_member_id)
    {
        MatchMembershipDelegate *handler = match->impl_->member_added_delegate_.get();
        if (handler != NULL)
        {
            ::Badumna::MemberIdentity member_id(new MemberIdentityImpl(match->impl_->mono_runtime_, managed_member_id));
            (*handler)(*match, member_id);
        }
    }

    void MatchImpl_InvokeMemberAddedDelegate(Match *match, DotNetObject managed_member_id)
    {
        MatchImpl::InvokeMemberAddedDelegate(match, managed_member_id);
    }

    void MatchImpl::InvokeMemberRemovedDelegate(Match *match, DotNetObject managed_member_id)
    {
        MatchMembershipDelegate *handler = match->impl_->member_removed_delegate_.get();
        if (handler != NULL)
        {
            ::Badumna::MemberIdentity member_id(new MemberIdentityImpl(match->impl_->mono_runtime_, managed_member_id));
            (*handler)(*match, member_id);
        }
    }

    void MatchImpl_InvokeMemberRemovedDelegate(Match *match, DotNetObject managed_member_id)
    {
        MatchImpl::InvokeMemberRemovedDelegate(match, managed_member_id);
    }
    
    void MatchImpl::InvokeChatMessageDelegate(Match *match, DotNetString message, DotNetObject sender_id, uint8_t chat_type)
    {
        MatchChatMessageDelegate *handler = match->impl_->chat_message_delegate_.get();
        if (handler != NULL)
        {
            MatchChatEventArgs args(
                MonoRuntime::MonoStringToWString(message),
                std::auto_ptr< ::Badumna::MemberIdentity>(new ::Badumna::MemberIdentity(new MemberIdentityImpl(match->impl_->mono_runtime_, sender_id))),
                static_cast<ChatType>(chat_type));
            (*handler)(*match, args);
        }
    }

    void MatchImpl_InvokeChatMessageDelegate(Match *match, DotNetString message, DotNetObject sender_id, uint8_t chat_type)
    {
        MatchImpl::InvokeChatMessageDelegate(match, message, sender_id, chat_type);
    }
}