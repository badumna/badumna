//-----------------------------------------------------------------------
// <copyright file="IMatchOriginal.h" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef BADUMNA_IMATCHORIGINAL_H
#define BADUMNA_IMATCHORIGINAL_H

#include "Badumna/Match/IMatchEntity.h"
#include "Badumna/DataTypes/OutputStream.h"
#include "Badumna/DataTypes/BooleanArray.h"

namespace Badumna
{
    /**
     * @class IMatchOriginal
     * @brief An entity that can be replicated.
     */
    class BADUMNA_API IMatchOriginal : public virtual IMatchEntity
    {
    public:
        /**
         * Destructor.
         */
        virtual ~IMatchOriginal() {}

        /**
         * Serializes the required_parts of the entity on to the stream.
         *
         * @param required_parts Indicates which parts should be serialized. Exactly these parts must be written to the 
         * stream.
         * @param stream The stream to serialize to.
         */
        virtual void Serialize(BooleanArray const &required_parts, OutputStream *stream) = 0;
    };
}

#endif // BADUMNA_IMATCHORIGINAL_H