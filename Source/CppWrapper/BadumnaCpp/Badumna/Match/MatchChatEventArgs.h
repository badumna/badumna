//-----------------------------------------------------------------------
// <copyright file="MatchChatEventArgs.h" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef BADUMNA_MATCH_CHAT_EVENT_ARGS_H
#define BADUMNA_MATCH_CHAT_EVENT_ARGS_H

#include <memory>
#include "Badumna/DataTypes/String.h"
#include "Badumna/Match/MemberIdentity.h"
#include "Badumna/Match/ChatType.h"

namespace Badumna
{
    /**
     * @struct MatchChatEventArgs
     * @brief Provides data for a chat message received event.
     */
    struct BADUMNA_API MatchChatEventArgs
    {
    public:
        MatchChatEventArgs(String message, std::auto_ptr<MemberIdentity> sender, ChatType type) : message(message), sender(sender), type(type) {};

        /**
         * The text of the message.
         */
        String message;

        /**
         * The member that sent the message.
         */
        std::auto_ptr<MemberIdentity> sender;

        /**
         * The type of message.
         */
        ChatType type;

    private:
        DISALLOW_COPY_AND_ASSIGN(MatchChatEventArgs);
    };
}

#endif // BADUMNA_MATCH_CHAT_EVENT_ARGS_H
