//-----------------------------------------------------------------------
// <copyright file="MatchmakingQueryResult.h" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef BADUMNA_MATCHMAKING_QUERY_RESULT_H
#define BADUMNA_MATCHMAKING_QUERY_RESULT_H

#include <memory>
#include <vector>
#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/Utils/LinkedPtr.h"
#include "Badumna/Match/MatchError.h"
#include "Badumna/Match/MatchmakingResult.h"

namespace Badumna
{
    class MatchmakingQueryResultImpl;
        
    /**
     * @class MatchmakingQueryResult
     * @brief The response to a matchmaking query.
     */
    class BADUMNA_API MatchmakingQueryResult
    {
        friend class MatchFacadeImpl;
    public:
        ~MatchmakingQueryResult();

        /**
         * Gets the error returned by the query.
         * 
         * Will be MatchError_None if the query was successful.
         */
        MatchError GetError() const;

        /**
         * Gets the matchmaking results.
         *
         * If this collection is empty and the query was successful (GetError() returns
         * MatchError_None) then no matches fit the query.
         */
        std::vector<linked_ptr<MatchmakingResult> > GetResults() const;

    private:
        explicit MatchmakingQueryResult(MatchmakingQueryResultImpl *impl);

        std::auto_ptr<MatchmakingQueryResultImpl> const impl_;
        DISALLOW_COPY_AND_ASSIGN(MatchmakingQueryResult);
    };
}

#endif // BADUMNA_MATCHMAKING_QUERY_RESULT_H
