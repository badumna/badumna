//-----------------------------------------------------------------------
// <copyright file="ChatType.h" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef BADUMNA_CHAT_TYPE_H
#define BADUMNA_CHAT_TYPE_H

namespace Badumna
{
    /**
     * @enum ChatType
     *
     * Enumeration of possible types of chat message.
     */
    enum ChatType
    {
        ChatType_None = 0,  /**< Unused default state. */
        ChatType_Public,    /**< A public chat message for all members of a match. */
        ChatType_Private,   /**< A private chat message for a particular member of a match. */
    };
}

#endif // BADUMNA_CHAT_TYPE_H