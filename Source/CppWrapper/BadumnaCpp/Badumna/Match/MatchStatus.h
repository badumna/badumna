//-----------------------------------------------------------------------
// <copyright file="MatchStatus.h" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef BADUMNA_MATCH_STATUS_H
#define BADUMNA_MATCH_STATUS_H

namespace Badumna
{
    /**
     * @enum MatchStatus
     *
     * Enumeration of possible states of the match.
     */
    enum MatchStatus
    {
        MatchStatus_Initializing = 0,     /**< Default state on match creation. */
        MatchStatus_Hosting,              /**< The match is currently being hosted on this peer. */
        MatchStatus_Connected,            /**< This peer is currently connected to the match host. */
        MatchStatus_Connecting,           /**< This peer is trying to connect to the match host. */
        MatchStatus_Closed,               /**< The match has been closed. */
    };
}

#endif // BADUMNA_MATCH_STATUS_H