//-----------------------------------------------------------------------
// <copyright file="MatchmakingResultImpl.h" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef BADUMNA_MATCHMAKING_RESULT_IMPL_H
#define BADUMNA_MATCHMAKING_RESULT_IMPL_H

#include "Badumna/Core/ProxyObject.h"
#include "Badumna/Match/MatchmakingResult.h"

namespace Badumna
{
    class MatchmakingResultImpl : public ProxyObject
    {
        friend class BadumnaRuntime;
    public:
        HostAuthorizationCertificate GetCertificate() const;
        MatchCapacity GetCapacity() const;
        MatchmakingCriteria GetCriteria() const;

    private:
        MatchmakingResultImpl(MonoRuntime *runtime, DotNetObject object);

        DISALLOW_COPY_AND_ASSIGN(MatchmakingResultImpl);
    };
}

#endif // BADUMNA_MATCHMAKING_RESULT_IMPL_H