//-----------------------------------------------------------------------
// <copyright file="IMatchEntity.h" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef BADUMNA_IMATCHENTITY_H
#define BADUMNA_IMATCHENTITY_H

#include "Badumna/DataTypes/InputStream.h"

namespace Badumna
{
    /**
     * @class IMatchEntity
     * @brief An entity that can receive events.
     */
    class BADUMNA_API IMatchEntity
    {
    public:
        /**
         * Destructor.
         */
        virtual ~IMatchEntity() {}
        
        /**
         * Processes an event sent to this entity.
         *
         * @param stream A stream containing the serialized event data. 
         */
        virtual void HandleEvent(InputStream *stream) = 0;
    };
}

#endif // BADUMNA_IMATCHENTITY_H