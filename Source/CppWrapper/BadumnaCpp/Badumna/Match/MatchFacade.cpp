//-----------------------------------------------------------------------
// <copyright file="MatchFacade.cpp" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include <cassert>
#include "Badumna/Match/MatchFacade.h"
#include "Badumna/Match/MatchFacadeImpl.h"

namespace Badumna
{
    MatchFacade::MatchFacade(MatchFacadeImpl *pimpl)
        : impl_(pimpl)
    {
        assert(pimpl != NULL);
    }

    MatchFacade::~MatchFacade()
    {
    }

    std::auto_ptr<Match> MatchFacade::CreateMatch(
        MatchmakingCriteria const &criteria,
        int max_players,
        Badumna::String const &player_name,
        CreateMatchReplicaDelegate create_delegate,
        RemoveMatchReplicaDelegate remove_delegate,
        CreateMatchHostedReplicaDelegate create_hosted_delegate,
        RemoveMatchHostedReplicaDelegate remove_hosted_delegate)
    {
        return impl_->CreateMatch(criteria, max_players, player_name, create_delegate, remove_delegate, create_hosted_delegate, remove_hosted_delegate);
    }

    std::auto_ptr<Match> MatchFacade::CreateMatch(
        IMatchHosted *controller,
        MatchmakingCriteria const &criteria,
        int max_players,
        String const &player_name,
        CreateMatchReplicaDelegate create_delegate,
        RemoveMatchReplicaDelegate remove_delegate,
        CreateMatchHostedReplicaDelegate create_hosted_delegate,
        RemoveMatchHostedReplicaDelegate remove_hosted_delegate)
    {
        return impl_->CreateMatch(controller, criteria, max_players, player_name, create_delegate, remove_delegate, create_hosted_delegate, remove_hosted_delegate);
    }

    std::auto_ptr<Match> MatchFacade::CreatePrivateMatch(
        MatchmakingCriteria const &criteria,
        int max_players,
        String const &player_name,
        CreateMatchReplicaDelegate create_delegate,
        RemoveMatchReplicaDelegate remove_delegate,
        CreateMatchHostedReplicaDelegate create_hosted_delegate,
        RemoveMatchHostedReplicaDelegate remove_hosted_delegate)
    {
        return impl_->CreatePrivateMatch(criteria, max_players, player_name, create_delegate, remove_delegate, create_hosted_delegate, remove_hosted_delegate);
    }

    std::auto_ptr<Match> MatchFacade::CreatePrivateMatch(
        IMatchHosted *controller,
        MatchmakingCriteria const &criteria,
        int max_players,
        String const &player_name,
        CreateMatchReplicaDelegate create_delegate,
        RemoveMatchReplicaDelegate remove_delegate,
        CreateMatchHostedReplicaDelegate create_hosted_delegate,
        RemoveMatchHostedReplicaDelegate remove_hosted_delegate)
    {
        return impl_->CreatePrivateMatch(controller, criteria, max_players, player_name, create_delegate, remove_delegate, create_hosted_delegate, remove_hosted_delegate);
    }

    std::auto_ptr<Match> MatchFacade::JoinMatch(
        MatchmakingResult const &matchmaking_result,
        Badumna::String const &player_name,
        CreateMatchReplicaDelegate create_delegate,
        RemoveMatchReplicaDelegate remove_delegate,
        CreateMatchHostedReplicaDelegate create_hosted_delegate,
        RemoveMatchHostedReplicaDelegate remove_hosted_delegate)
    {
        return impl_->JoinMatch(matchmaking_result, player_name, create_delegate, remove_delegate, create_hosted_delegate, remove_hosted_delegate);
    }

    std::auto_ptr<Match> MatchFacade::JoinMatch(
        IMatchHosted *controller,
        MatchmakingResult const &matchmaking_result,
        String const &player_name,
        CreateMatchReplicaDelegate create_delegate,
        RemoveMatchReplicaDelegate remove_delegate,
        CreateMatchHostedReplicaDelegate create_hosted_delegate,
        RemoveMatchHostedReplicaDelegate remove_hosted_delegate)
    {
        return impl_->JoinMatch(controller, matchmaking_result, player_name, create_delegate, remove_delegate, create_hosted_delegate, remove_hosted_delegate);
    }

    void MatchFacade::FindMatches(
        MatchmakingCriteria const &criteria,
        MatchmakingResultDelegate result_handler)
    {
        impl_->FindMatches(criteria, result_handler);
    }
}