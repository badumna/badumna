//-----------------------------------------------------------------------
// <copyright file="MatchStatusEventArgs.h" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef BADUMNA_MATCH_STATUS_EVENT_ARGS_H
#define BADUMNA_MATCH_STATUS_EVENT_ARGS_H

#include "Badumna/Match/MatchStatus.h"
#include "Badumna/Match/MatchError.h"

namespace Badumna
{
    /**
     * @struct MatchStatusEventArgs
     * @brief Provides data for a StateChanged event.
     */
    struct BADUMNA_API MatchStatusEventArgs
    {
    public:
        MatchStatusEventArgs(MatchStatus status, MatchError error) : status(status), error(error) {};

        /**
         * The match status.
         */
        MatchStatus status;

        /**
         * The error.
         */
        MatchError error;
    };
}

#endif // BADUMNA_MATCH_STATUS_EVENT_ARGS_H
