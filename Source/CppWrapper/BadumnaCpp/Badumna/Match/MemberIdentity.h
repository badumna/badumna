//-----------------------------------------------------------------------
// <copyright file="MemberIdentity.h" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef BADUMNA_MEMBER_IDENTITY_H
#define BADUMNA_MEMBER_IDENTITY_H

#include <memory>
#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/DataTypes/String.h"

namespace Badumna
{
    class MemberIdentityImpl;

    /**
     * @class MemberIdentity
     * @brief Identifies a member of a match.
     *
     */
    class BADUMNA_API MemberIdentity
    {
        friend class MatchImpl;
        friend class ManagedObjectGuard;

    public:
        /**
         * Copy constructor.
         * 
         * @param other MemberIdentity instance to copy. 
         */
        MemberIdentity(MemberIdentity const &other);

        ~MemberIdentity();

        /**
         * Gets the name chosen by the member. Uniqueness not enforced.
         */
        String Name() const;
        
        /**
         * The assignment operator.
         * 
         * @param rhs The right hand side MemberIdentity instance.
         * @return A reference to this MemberIdentity instance. 
         */
        MemberIdentity& operator=(MemberIdentity const &rhs);

        /**
         * Determines whether this MemberIdentity instance is logically equal to the given MemberIdentity instance.
         * 
         * @param rhs The right hand side MemberIdentity instance.
         * @return A boolean value indicating whether the two MemberIdentity instances are logically equal. 
         */
        bool operator ==(MemberIdentity const &rhs) const;

        /**
         * Determines whether this MemberIdentity instance is logically unequal to the given MemberIdentity instance.
         * 
         * @param rhs The right hand side MemberIdentity instance.
         * @return A boolean value indicating whether the two MemberIdentity instances are logically unequal. 
         */
        bool operator !=(MemberIdentity const &rhs) const;

    private:
        explicit MemberIdentity(MemberIdentityImpl *impl);

        std::auto_ptr<MemberIdentityImpl> const impl_;
    };
}

#endif // BADUMNA_MEMBER_IDENTITY_H
