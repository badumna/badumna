//-----------------------------------------------------------------------
// <copyright file="MatchError.h" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef BADUMNA_MATCH_ERROR_H
#define BADUMNA_MATCH_ERROR_H

namespace Badumna
{
    /**
     * @enum MatchError
     *
     * Enumeration of possible match errors.
     */
    enum MatchError
    {
        MatchError_None = 0,                      /**< No error encountered. */
        MatchError_CannotConnectToHost,           /**< This peer is unable to connect to the host. */
        MatchError_MatchFull,                     /**< This peer is unable to join the match because it is full. */
        MatchError_CannotConnectToMatchmaker,     /**< This peer is unable to connect to the matchmaker. */
    };
}

#endif // BADUMNA_MATCH_ERROR_H