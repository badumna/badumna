//-----------------------------------------------------------------------
// <copyright file="MatchmakingQueryResultImpl.cpp" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include "Badumna/Core/TypeRegistry.h"
#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/Match/MatchmakingQueryResultImpl.h"

namespace Badumna
{
    REGISTER_MANAGED_MONO_TYPE(Match, MatchmakingQueryResult);
    MatchmakingQueryResultImpl::MatchmakingQueryResultImpl(MonoRuntime *runtime, DotNetObject object)
        : ProxyObject(runtime, object, "Badumna.Match", "MatchmakingQueryResult")
    {
    }

    REGISTER_PROPERTY(Match, MatchmakingQueryResult, Error)
    MatchError MatchmakingQueryResultImpl::GetError() const
    {
        return managed_object_.GetPropertyFromName<MatchError>("Error");
    }

    REGISTER_PROPERTY(Match, MatchmakingQueryResult, Results)
    std::vector<linked_ptr<MatchmakingResult> > MatchmakingQueryResultImpl::GetResults() const
    {
        std::vector<linked_ptr<MatchmakingResult> > results;

        ManagedObject managed_results(mono_runtime_, managed_object_.GetPropertyFromName("Results"));
        int count = managed_results.GetPropertyFromName<int>("Count");
        for (int i = 0; i < count; ++i)
        {
            Arguments args;
            args.AddArgument(&i);

            DotNetObject result = managed_results.GetPropertyFromName("Item", args);
            results.push_back(linked_ptr<MatchmakingResult>(new MatchmakingResult(BadumnaRuntime::Instance().CreateMatchmakingResultImpl(result))));
        }

        return results;
    }
}