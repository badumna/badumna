//-----------------------------------------------------------------------
// <copyright file="HostAuthorizationCertificate.h" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef BADUMNA_HOST_AUTHORIZATION_CERTIFICATE_H
#define BADUMNA_HOST_AUTHORIZATION_CERTIFICATE_H

#include <memory>
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    /**
     * @class HostAuthorizationCertificate
     * @brief Issued by the matchmaking server that a known peer is authorized to act as host for a given match.
     */
    class BADUMNA_API HostAuthorizationCertificate
    {
    public:
        ~HostAuthorizationCertificate();

    private:
        HostAuthorizationCertificate();
        //explicit HostAuthorizationCertificate(HostAuthorizationCertificateImpl *impl);

        //std::auto_ptr<HostAuthorizationCertificateImpl> const impl_;
        DISALLOW_COPY_AND_ASSIGN(HostAuthorizationCertificate);
    };
}

#endif // BADUMNA_HOST_AUTHORIZATION_CERTIFICATE_H
