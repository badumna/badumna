//-----------------------------------------------------------------------
// <copyright file="MatchImpl.h" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef BADUMNA_MATCH_IMPL_H
#define BADUMNA_MATCH_IMPL_H

#include "Badumna/Core/ProxyObject.h"
#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Match/Match.h"
#include "Badumna/Match/IMatchOriginal.h"
#include "Badumna/Match/IMatchReplica.h"
#include "Badumna/Match/MatchDelegate.h"

namespace Badumna
{
    class MatchImpl : public ProxyObject
    {
        friend class MatchFacadeImpl;
    public:
        std::auto_ptr< ::Badumna::MemberIdentity> MemberIdentity() const;
        std::auto_ptr<BadumnaId> MatchIdentifier() const;
        std::vector<linked_ptr< ::Badumna::MemberIdentity> > Members() const;
        int32_t MemberCount() const;
        MatchStatus State() const;
        MatchError Error() const;

        void SetStateChangedDelegate(MatchStateChangedDelegate handler);
        void RemoveStateChangedDelegate();
        void SetMemberAddedDelegate(MatchMembershipDelegate handler);
        void RemoveMemberAddedDelegate();
        void SetMemberRemovedDelegate(MatchMembershipDelegate handler);
        void RemoveMemberRemovedDelegate();
        void SetChatMessageReceivedDelegate(MatchChatMessageDelegate handler);
        void RemoveChatMessageReceivedDelegate();

        void RegisterEntity(IMatchOriginal *entity, uint32_t entity_type);
        void UnregisterEntity(IMatchOriginal *entity);
        void RegisterHostedEntity(IMatchHosted *hosted, uint32_t entity_type);
        void UnregisterHostedEntity(IMatchHosted *hosted);

        void FlagForUpdate(void *entity, BooleanArray const &changed_parts);
        void FlagForUpdate(void *entity, int changed_part_index);

        void Chat(String message);
        void Chat(::Badumna::MemberIdentity const &member_id, String message);

        void SendCustomMessageToReplicas(void *entity, OutputStream *message_data);
        void SendCustomMessageToOriginal(void *entity, OutputStream *message_data);
        void SendCustomMessageToMember(::Badumna::MemberIdentity const &member_id, OutputStream *message_data);
        void SendCustomMessageToMembers(OutputStream *message_data);
        void SendCustomMessageToHost(OutputStream *message_data);

        void Leave();

        static void InvokeMatchStateChangedDelegate(Match *match, int32_t status, int32_t error);
        static void InvokeMemberAddedDelegate(Match *match, DotNetObject managed_member_id);
        static void InvokeMemberRemovedDelegate(Match *match, DotNetObject managed_member_id);
        static void InvokeChatMessageDelegate(Match *match, DotNetString message, DotNetObject sender_id, uint8_t chat_type);

        static IMatchReplica *InvokeMatchCreateReplicaDelegate(Match *match, uint32_t entity_type);
        static void InvokeMatchRemoveReplicaDelegate(Match *match, IMatchReplica *replica);

        static IMatchHosted *InvokeMatchCreateHostedReplicaDelegate(Match *match, uint32_t entity_type);
        static void InvokeMatchRemoveHostedReplicaDelegate(Match *match, IMatchHosted *hosted);

        static DotNetArray Serialize(IMatchOriginal *original, DotNetObject required_parts);
        static void Deserialize(IMatchReplica *replica, DotNetObject included_parts, DotNetArray data, int32_t estimated_milliseconds_since_departure);
        static void HandleEvent(IMatchEntity *entity, DotNetArray data);

    private:
        MatchImpl(
            MonoRuntime *runtime,
            CreateMatchReplicaDelegate create_delegate,
            RemoveMatchReplicaDelegate remove_delegate,
            CreateMatchHostedReplicaDelegate create_hosted_delegate,
            RemoveMatchHostedReplicaDelegate remove_hosted_delegate);
        void Initialize(DotNetObject object);

        std::auto_ptr<MatchStateChangedDelegate> state_changed_delegate_;
        std::auto_ptr<MatchMembershipDelegate> member_added_delegate_;
        std::auto_ptr<MatchMembershipDelegate> member_removed_delegate_;
        std::auto_ptr<MatchChatMessageDelegate> chat_message_delegate_;

        CreateMatchReplicaDelegate create_delegate_;
        RemoveMatchReplicaDelegate remove_delegate_;

        CreateMatchHostedReplicaDelegate create_hosted_delegate_;
        RemoveMatchHostedReplicaDelegate remove_hosted_delegate_;

        DISALLOW_COPY_AND_ASSIGN(MatchImpl);
    };

    extern "C" void MatchImpl_InvokeMatchStateChangedDelegate(Match *match, int32_t status, int32_t error);
    extern "C" void MatchImpl_InvokeMemberAddedDelegate(Match *match, DotNetObject managed_member_id);
    extern "C" void MatchImpl_InvokeMemberRemovedDelegate(Match *match, DotNetObject managed_member_id);
    extern "C" void MatchImpl_InvokeChatMessageDelegate(Match *match, DotNetString message, DotNetObject sender_id, uint8_t chat_type);

    extern "C" IMatchReplica *MatchImpl_InvokeMatchCreateReplicaDelegate(Match *match, uint32_t entity_type);
    extern "C" void MatchImpl_InvokeMatchRemoveReplicaDelegate(Match *match, IMatchReplica *replica);

    extern "C" IMatchHosted *MatchImpl_InvokeMatchCreateHostedReplicaDelegate(Match *match, uint32_t entity_type);
    extern "C" void MatchImpl_InvokeMatchRemoveHostedReplicaDelegate(Match *match, IMatchHosted *hosted);

    extern "C" DotNetArray MatchImpl_Serialize(IMatchOriginal *original, DotNetObject required_parts);
    extern "C" void MatchImpl_OriginalHandleEvent(IMatchOriginal *original, DotNetArray data);

    extern "C" void MatchImpl_Deserialize(IMatchReplica *replica, DotNetObject included_parts, DotNetArray data, int32_t estimated_milliseconds_since_departure);
    extern "C" void MatchImpl_ReplicaHandleEvent(IMatchReplica *replica, DotNetArray data);

    extern "C" DotNetArray MatchImpl_HostedSerialize(IMatchHosted *hosted, DotNetObject required_parts);
    extern "C" void MatchImpl_HostedDeserialize(IMatchHosted *hosted, DotNetObject included_parts, DotNetArray data, int32_t estimated_milliseconds_since_departure);
    extern "C" void MatchImpl_HostedHandleEvent(IMatchHosted *hosted, DotNetArray data);
}

#endif // BADUMNA_MATCH_IMPL_H