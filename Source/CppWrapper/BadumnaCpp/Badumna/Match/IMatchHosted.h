//-----------------------------------------------------------------------
// <copyright file="IMatchHosted.h" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef BADUMNA_IMATCHHOSTED_H
#define BADUMNA_IMATCHHOSTED_H

#include "Badumna/Match/IMatchOriginal.h"
#include "Badumna/Match/IMatchReplica.h"

namespace Badumna
{
    /**
     * @class IMatchHosted
     * @brief A hosted entity.
     *
     * A hosted entity must support both serialization and deserialization so that
     * control of the entity can migrate if the host of the match migrates.
     */
    class BADUMNA_API IMatchHosted : public IMatchOriginal, public IMatchReplica
    {
    public:
        /**
         * Destructor.
         */
        virtual ~IMatchHosted() {}
    };
}

#endif // BADUMNA_IMATCHHOSTED_H