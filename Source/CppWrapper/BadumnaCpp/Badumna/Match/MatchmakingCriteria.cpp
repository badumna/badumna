//-----------------------------------------------------------------------
// <copyright file="MatchmakingCriteria.cpp" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include "Badumna/Core/TypeRegistry.h"
#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/Match/MatchmakingCriteria.h"

namespace Badumna
{
    REGISTER_MANAGED_MONO_TYPE(Match, MatchmakingCriteria);
    REGISTER_PROPERTY(Match, MatchmakingCriteria, MatchName);
    REGISTER_PROPERTY(Match, MatchmakingCriteria, PlayerGroup);
    std::auto_ptr<ManagedObject> MatchmakingCriteria::ToManaged(MonoRuntime *mono_runtime, MatchmakingCriteria const &criteria)
    {
        std::auto_ptr<ManagedObject> managed_criteria(new ManagedObject(mono_runtime, "Badumna.Match", "MatchmakingCriteria"));
        managed_criteria->SetPropertyValue("MatchName", criteria.match_name);
        managed_criteria->SetPropertyValue("PlayerGroup", criteria.player_group);
        return managed_criteria;
    }

    MatchmakingCriteria MatchmakingCriteria::FromManaged(ManagedObject const &managed_criteria)
    {
        MatchmakingCriteria criteria;
        criteria.match_name = managed_criteria.GetPropertyFromName<String>("MatchName");
        criteria.player_group = managed_criteria.GetPropertyFromName<uint16_t>("PlayerGroup");
        return criteria;
    }
}
