//-----------------------------------------------------------------------
// <copyright file="MatchFacadeImpl.cpp" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include "Badumna/Core/TypeRegistry.h"
#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/Core/ManagedObjectGuard.h"
#include "Badumna/Match/MatchFacadeImpl.h"
#include "Badumna/Match/MatchFacade.h"
#include "Badumna/Match/MatchImpl.h"
#include "Badumna/Match/MatchmakingResultImpl.h"

namespace Badumna
{
    REGISTER_MANAGED_MONO_TYPE(CppWrapperStub, MatchFacadeStub);
    MatchFacadeImpl::MatchFacadeImpl(MonoRuntime *runtime, DotNetObject object)
        : ProxyObject(runtime, object, "Badumna.CppWrapperStub", "MatchFacadeStub")
    {
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, MatchFacadeStub, CreateMatch, 4)
    std::auto_ptr<Match> MatchFacadeImpl::CreateMatch(
        MatchmakingCriteria const &criteria,
        int max_players,
        String const &player_name,
        CreateMatchReplicaDelegate create_delegate,
        RemoveMatchReplicaDelegate remove_delegate,
        CreateMatchHostedReplicaDelegate create_hosted_delegate,
        RemoveMatchHostedReplicaDelegate remove_hosted_delegate)
    {
        MatchImpl *match_impl = new MatchImpl(mono_runtime_, create_delegate, remove_delegate, create_hosted_delegate, remove_hosted_delegate);
        std::auto_ptr<Match> match(new Match(match_impl));

        Arguments args;

        std::auto_ptr<ManagedObject> managed_criteria = MatchmakingCriteria::ToManaged(mono_runtime_, criteria);
        args.AddArgument(managed_criteria->GetManagedObject());
        
        args.AddArgument(&max_players);
        args.AddArgument(MonoRuntime::GetString(player_name));
        Match *p_match = match.get();
        args.AddArgument(&p_match);

        match_impl->Initialize(managed_object_.InvokeMethod("CreateMatch", args));

        return match;
    }
    
    REGISTER_MANAGED_METHOD(CppWrapperStub, MatchFacadeStub, CreateMatch, 5)
    std::auto_ptr<Match> MatchFacadeImpl::CreateMatch(
        IMatchHosted *controller,
        MatchmakingCriteria const &criteria,
        int max_players,
        String const &player_name,
        CreateMatchReplicaDelegate create_delegate,
        RemoveMatchReplicaDelegate remove_delegate,
        CreateMatchHostedReplicaDelegate create_hosted_delegate,
        RemoveMatchHostedReplicaDelegate remove_hosted_delegate)
    {
        MatchImpl *match_impl = new MatchImpl(mono_runtime_, create_delegate, remove_delegate, create_hosted_delegate, remove_hosted_delegate);
        std::auto_ptr<Match> match(new Match(match_impl));

        Arguments args;
        args.AddArgument(&controller);

        std::auto_ptr<ManagedObject> managed_criteria = MatchmakingCriteria::ToManaged(mono_runtime_, criteria);
        args.AddArgument(managed_criteria->GetManagedObject());
        
        args.AddArgument(&max_players);
        args.AddArgument(MonoRuntime::GetString(player_name));
        Match *p_match = match.get();
        args.AddArgument(&p_match);

        match_impl->Initialize(managed_object_.InvokeMethod("CreateMatch", args));

        return match;
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, MatchFacadeStub, CreatePrivateMatch, 4)
    std::auto_ptr<Match> MatchFacadeImpl::CreatePrivateMatch(
        MatchmakingCriteria const &criteria,
        int max_players,
        String const &player_name,
        CreateMatchReplicaDelegate create_delegate,
        RemoveMatchReplicaDelegate remove_delegate,
        CreateMatchHostedReplicaDelegate create_hosted_delegate,
        RemoveMatchHostedReplicaDelegate remove_hosted_delegate)
    {
        MatchImpl *match_impl = new MatchImpl(mono_runtime_, create_delegate, remove_delegate, create_hosted_delegate, remove_hosted_delegate);
        std::auto_ptr<Match> match(new Match(match_impl));

        Arguments args;

        std::auto_ptr<ManagedObject> managed_criteria = MatchmakingCriteria::ToManaged(mono_runtime_, criteria);
        args.AddArgument(managed_criteria->GetManagedObject());
        
        args.AddArgument(&max_players);
        args.AddArgument(MonoRuntime::GetString(player_name));
        Match *p_match = match.get();
        args.AddArgument(&p_match);

        match_impl->Initialize(managed_object_.InvokeMethod("CreatePrivateMatch", args));

        return match;
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, MatchFacadeStub, CreatePrivateMatch, 5)
    std::auto_ptr<Match> MatchFacadeImpl::CreatePrivateMatch(
        IMatchHosted *controller,
        MatchmakingCriteria const &criteria,
        int max_players,
        String const &player_name,
        CreateMatchReplicaDelegate create_delegate,
        RemoveMatchReplicaDelegate remove_delegate,
        CreateMatchHostedReplicaDelegate create_hosted_delegate,
        RemoveMatchHostedReplicaDelegate remove_hosted_delegate)
    {
        MatchImpl *match_impl = new MatchImpl(mono_runtime_, create_delegate, remove_delegate, create_hosted_delegate, remove_hosted_delegate);
        std::auto_ptr<Match> match(new Match(match_impl));

        Arguments args;
        args.AddArgument(&controller);

        std::auto_ptr<ManagedObject> managed_criteria = MatchmakingCriteria::ToManaged(mono_runtime_, criteria);
        args.AddArgument(managed_criteria->GetManagedObject());
        
        args.AddArgument(&max_players);
        args.AddArgument(MonoRuntime::GetString(player_name));
        Match *p_match = match.get();
        args.AddArgument(&p_match);

        match_impl->Initialize(managed_object_.InvokeMethod("CreatePrivateMatch", args));

        return match;
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, MatchFacadeStub, JoinMatch, 3)
    std::auto_ptr<Match> MatchFacadeImpl::JoinMatch(
        MatchmakingResult const &matchmaking_result,
        String const &player_name,
        CreateMatchReplicaDelegate create_delegate,
        RemoveMatchReplicaDelegate remove_delegate,
        CreateMatchHostedReplicaDelegate create_hosted_delegate,
        RemoveMatchHostedReplicaDelegate remove_hosted_delegate)
    {
        MatchImpl *match_impl = new MatchImpl(mono_runtime_, create_delegate, remove_delegate, create_hosted_delegate, remove_hosted_delegate);
        std::auto_ptr<Match> match(new Match(match_impl));

        Arguments args;

        args.AddArgument(ManagedObjectGuard::GetManagedObject(matchmaking_result));
        args.AddArgument(MonoRuntime::GetString(player_name));
        Match *p_match = match.get();
        args.AddArgument(&p_match);

        match_impl->Initialize(managed_object_.InvokeMethod("JoinMatch", args));

        return match;
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, MatchFacadeStub, JoinMatch, 4)
    std::auto_ptr<Match> MatchFacadeImpl::JoinMatch(
        IMatchHosted *controller,
        MatchmakingResult const &matchmaking_result,
        String const &player_name,
        CreateMatchReplicaDelegate create_delegate,
        RemoveMatchReplicaDelegate remove_delegate,
        CreateMatchHostedReplicaDelegate create_hosted_delegate,
        RemoveMatchHostedReplicaDelegate remove_hosted_delegate)
    {
        MatchImpl *match_impl = new MatchImpl(mono_runtime_, create_delegate, remove_delegate, create_hosted_delegate, remove_hosted_delegate);
        std::auto_ptr<Match> match(new Match(match_impl));

        Arguments args;
        args.AddArgument(&controller);

        args.AddArgument(ManagedObjectGuard::GetManagedObject(matchmaking_result));
        args.AddArgument(MonoRuntime::GetString(player_name));
        Match *p_match = match.get();
        args.AddArgument(&p_match);

        match_impl->Initialize(managed_object_.InvokeMethod("JoinMatch", args));

        return match;
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, MatchFacadeStub, FindMatches, 2)
    void MatchFacadeImpl::FindMatches(
        MatchmakingCriteria const &criteria,
        MatchmakingResultDelegate result_handler)
    {
        Arguments args;

        std::auto_ptr<ManagedObject> managed_criteria = MatchmakingCriteria::ToManaged(mono_runtime_, criteria);
        args.AddArgument(managed_criteria->GetManagedObject());
        MatchmakingResultDelegate *result_delegate = new MatchmakingResultDelegate(result_handler);  // newed copy so it stays alive until we invoke it.
        args.AddArgument(&result_delegate);

        managed_object_.InvokeMethod("FindMatches", args);
    }

    void MatchFacadeImpl_InvokeFindMatchesResultsDelegate(MatchmakingResultDelegate *handler, DotNetObject result)
    {
        MatchFacadeImpl::InvokeFindMatchesResultsDelegate(handler, result);
    }

    void MatchFacadeImpl::InvokeFindMatchesResultsDelegate(MatchmakingResultDelegate *handler, DotNetObject managed_result)
    {
        assert(handler != NULL && managed_result != NULL);
        MatchmakingQueryResult result(BadumnaRuntime::Instance().CreateMatchmakingQueryResultImpl(managed_result));
        (*handler)(result);
        delete handler;
    }
}