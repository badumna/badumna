//-----------------------------------------------------------------------
// <copyright file="MatchCapacity.h" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef BADUMNA_MATCH_CAPACITY_H
#define BADUMNA_MATCH_CAPACITY_H

#include <memory>
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class ManagedObject;

    /**
     * @struct MatchCapacity
     * @brief Match status sent to server from host.
     */
    struct BADUMNA_API MatchCapacity
    {
        friend class MatchmakingResultImpl;

    public:
        MatchCapacity() : num_slots(0), empty_slots(0) {};

        /**
         * The number of players who can be in the match.
         */
        int32_t num_slots;

        /**
         * The number of empty places for players.
         */
        int32_t empty_slots;

    private:
         static MatchCapacity FromManaged(ManagedObject const &managed_capacity);
    };
}

#endif // BADUMNA_MATCH_CAPACITY_H
