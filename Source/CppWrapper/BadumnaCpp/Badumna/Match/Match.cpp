//-----------------------------------------------------------------------
// <copyright file="Match.cpp" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include <cassert>
#include "Badumna/Match/Match.h"
#include "Badumna/Match/MatchImpl.h"

namespace Badumna
{
    Match::Match(MatchImpl *pimpl)
        : impl_(pimpl)
    {
        assert(pimpl != NULL);
    }

    Match::~Match()
    {
    }

    std::auto_ptr<MemberIdentity> Match::MemberIdentity() const
    {
        return impl_->MemberIdentity();
    }

    std::auto_ptr<BadumnaId> Match::MatchIdentifier() const
    {
        return impl_->MatchIdentifier();
    }

    std::vector<linked_ptr< ::Badumna::MemberIdentity> > Match::Members() const
    {
        return impl_->Members();
    }

    int32_t Match::MemberCount() const
    {
        return impl_->MemberCount();
    }

    MatchStatus Match::State() const
    {
        return impl_->State();
    }

    MatchError Match::Error() const
    {
        return impl_->Error();
    }

    void Match::SetStateChangedDelegate(MatchStateChangedDelegate handler)
    {
        impl_->SetStateChangedDelegate(handler);
    }

    void Match::RemoveStateChangedDelegate()
    {
        impl_->RemoveStateChangedDelegate();
    }

    void Match::SetMemberAddedDelegate(MatchMembershipDelegate handler)
    {
        impl_->SetMemberAddedDelegate(handler);
    }

    void Match::RemoveMemberAddedDelegate()
    {
        impl_->RemoveMemberAddedDelegate();
    }

    void Match::SetMemberRemovedDelegate(MatchMembershipDelegate handler)
    {
        impl_->SetMemberRemovedDelegate(handler);
    }

    void Match::RemoveMemberRemovedDelegate()
    {
        impl_->RemoveMemberRemovedDelegate();
    }

    void Match::SetChatMessageReceivedDelegate(MatchChatMessageDelegate handler)
    {
        impl_->SetChatMessageReceivedDelegate(handler);
    }

    void Match::RemoveChatMessageReceivedDelegate()
    {
        impl_->RemoveChatMessageReceivedDelegate();
    }

    void Match::RegisterEntity(IMatchOriginal *original, uint32_t entity_type)
    {
        impl_->RegisterEntity(original, entity_type);
    }

    void Match::UnregisterEntity(IMatchOriginal *original)
    {
        impl_->UnregisterEntity(original);
    }

    void Match::RegisterHostedEntity(IMatchHosted *hosted, uint32_t entity_type)
    {
        impl_->RegisterHostedEntity(hosted, entity_type);
    }

    void Match::UnregisterHostedEntity(IMatchHosted *hosted)
    {
        impl_->UnregisterHostedEntity(hosted);
    }

    void Match::FlagForUpdate(IMatchOriginal *original, BooleanArray const &changed_parts)
    {
        impl_->FlagForUpdate(original, changed_parts);
    }

    void Match::FlagForUpdate(IMatchOriginal *original, int changed_part_index)
    {
        impl_->FlagForUpdate(original, changed_part_index);
    }
    
    void Match::FlagForUpdate(IMatchHosted *hosted, BooleanArray const &changed_parts)
    {
        impl_->FlagForUpdate(hosted, changed_parts);
    }

    void Match::FlagForUpdate(IMatchHosted *hosted, int changed_part_index)
    {
        impl_->FlagForUpdate(hosted, changed_part_index);
    }

    void Match::Chat(String message)
    {
        impl_->Chat(message);
    }

    void Match::Chat(::Badumna::MemberIdentity const &member_id, String message)
    {
        impl_->Chat(member_id, message);
    }

    void Match::SendCustomMessageToReplicas(IMatchOriginal *original, OutputStream *message_data)
    {
        impl_->SendCustomMessageToReplicas(original, message_data);
    }

    void Match::SendCustomMessageToReplicas(IMatchHosted *hosted, OutputStream *message_data)
    {
        impl_->SendCustomMessageToReplicas(hosted, message_data);
    }

    void Match::SendCustomMessageToOriginal(IMatchReplica *replica, OutputStream *message_data)
    {
        impl_->SendCustomMessageToOriginal(replica, message_data);
    }

    void Match::SendCustomMessageToOriginal(IMatchHosted *hosted, OutputStream *message_data)
    {
        impl_->SendCustomMessageToOriginal(hosted, message_data);
    }

    void Match::SendCustomMessageToMember(::Badumna::MemberIdentity const &member_id, OutputStream *message_data)
    {
        impl_->SendCustomMessageToMember(member_id, message_data);
    }

    void Match::SendCustomMessageToMembers(OutputStream *message_data)
    {
        impl_->SendCustomMessageToMembers(message_data);
    }

    void Match::SendCustomMessageToHost(OutputStream *message_data)
    {
        impl_->SendCustomMessageToHost(message_data);
    }

    void Match::Leave()
    {
        impl_->Leave();
    }
}