//---------------------------------------------------------------------------------
// <copyright file="MemberIdentity.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2013 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include <sstream>

#include "Badumna/Match/MemberIdentity.h"
#include "Badumna/Match/MemberIdentityImpl.h"

namespace Badumna
{
    MemberIdentity::MemberIdentity(MemberIdentityImpl *impl) 
        : impl_(impl)
    {
    }

    MemberIdentity::~MemberIdentity()
    {
    }

    String MemberIdentity::Name() const
    {
        return impl_->Name();
    }

    bool MemberIdentity::operator ==(MemberIdentity const &rhs) const
    {
        return impl_->operator ==(*(rhs.impl_));
    }

    bool MemberIdentity::operator !=(MemberIdentity const &rhs) const
    {
        return !MemberIdentity::operator ==(rhs);
    }

    DEFINE_WRAPPER_CLASS_COPYCTOR_ASSIGN_OPERATOR(MemberIdentity, MemberIdentityImpl, impl_, other)
}