//-----------------------------------------------------------------------
// <copyright file="MatchmakingCriteria.h" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef BADUMNA_MATCHMAKING_CRITERIA_H
#define BADUMNA_MATCHMAKING_CRITERIA_H

#include <memory>
#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/DataTypes/String.h"

namespace Badumna
{
    class ManagedObject;
    class MonoRuntime;

    /**
     * @struct MatchmakingCriteria
     * @brief Criteria used for matchmaking.
     */
    struct BADUMNA_API MatchmakingCriteria
    {
        friend class MatchmakingResultImpl;
        friend class MatchFacadeImpl;

    public:
        MatchmakingCriteria() : match_name(), player_group(0) {};

        /**
         * Gets or sets the match name.
         *
         * An empty or <code>NULL</code> match name indicates no match name is specified.
         * 
         * If no match name is specified when searching for matches then public matches with any
         * name will be returned.
         * Otherwise, only matches that have exactly the name specified will be returned.
         * If a match is created without specifying a name then it can only be matched
         * by a query that doesn't specify a match name (such a query will also return
         * matches that are named).
         */
        String match_name;

        /**
         * Gets or sets the player group.
         *
         * Player group is used to divide the players into smaller groups, only players with the same player group can be matched together.
         * This can be useful to group players based on the skill level, different type of matches, different type of game maps, etc. 
         * The player group will be ignored if it has a value of 0.
         */
        uint16_t player_group;

    private:
        static std::auto_ptr<ManagedObject> ToManaged(MonoRuntime *mono_runtime, MatchmakingCriteria const &criteria);
        static MatchmakingCriteria FromManaged(ManagedObject const &managed_criteria);
    };
}

#endif // BADUMNA_MATCHMAKING_CRITERIA_H
