//-----------------------------------------------------------------------
// <copyright file="MatchmakingResultImpl.cpp" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include "Badumna/Core/TypeRegistry.h"
#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/Match/MatchmakingResultImpl.h"

namespace Badumna
{
    REGISTER_MANAGED_MONO_TYPE(Match, MatchmakingResult);
    MatchmakingResultImpl::MatchmakingResultImpl(MonoRuntime *runtime, DotNetObject object)
        : ProxyObject(runtime, object, "Badumna.Match", "MatchmakingResult")
    {
    }

    //REGISTER_PROPERTY(Match, MatchmakingResult, Certificate)
    //HostAuthorizationCertificate MatchmakingResultImpl::GetCertificate() const
    //{
    //}

    REGISTER_PROPERTY(Match, MatchmakingResult, Capacity)
    MatchCapacity MatchmakingResultImpl::GetCapacity() const
    {
        ManagedObject managed_capacity(mono_runtime_, managed_object_.GetPropertyFromName("Capacity"));
        return MatchCapacity::FromManaged(managed_capacity);
    }

    REGISTER_PROPERTY(Match, MatchmakingResult, Criteria)
    MatchmakingCriteria MatchmakingResultImpl::GetCriteria() const
    {
        ManagedObject managed_criteria(mono_runtime_, managed_object_.GetPropertyFromName("Criteria"));
        return MatchmakingCriteria::FromManaged(managed_criteria);
    }
}