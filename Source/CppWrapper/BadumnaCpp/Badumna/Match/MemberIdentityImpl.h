//---------------------------------------------------------------------------------
// <copyright file="MemberIdentityImpl.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_MEMBER_IDENTITY_IMPL_H
#define BADUMNA_MEMBER_IDENTITY_IMPL_H

#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Core/MonoRuntime.h"
#include "Badumna/Core/ProxyObject.h"
#include "Badumna/Core/ProxyObjectHelper.h"
#include "Badumna/DataTypes/String.h"

namespace Badumna
{
    class MemberIdentityImpl : public ProxyObject
    {
        friend class MatchImpl;

    public: 
        String Name() const;

        bool operator ==(MemberIdentityImpl const &rhs) const;
        bool operator !=(MemberIdentityImpl const &rhs) const;

        DECLARE_COPY_AND_ASSIGN(MemberIdentityImpl);

    private:
        MemberIdentityImpl(MonoRuntime *runtime, DotNetObject object);
    };
}

#endif // BADUMNA_MEMBER_IDENTITY_IMPL_H