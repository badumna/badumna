//-----------------------------------------------------------------------
// <copyright file="MatchCapacity.cpp" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include "Badumna/Core/TypeRegistry.h"
#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/Match/MatchCapacity.h"

namespace Badumna
{
    REGISTER_MANAGED_MONO_TYPE(Match, MatchCapacity);
    REGISTER_PROPERTY(Match, MatchCapacity, NumSlots);
    REGISTER_PROPERTY(Match, MatchCapacity, EmptySlots);
    MatchCapacity MatchCapacity::FromManaged(ManagedObject const &managed_capacity)
    {
        MatchCapacity capacity;
        capacity.num_slots = managed_capacity.GetPropertyFromName<int32_t>("NumSlots");
        capacity.empty_slots = managed_capacity.GetPropertyFromName<int32_t>("EmptySlots");
        return capacity;
    }
}
