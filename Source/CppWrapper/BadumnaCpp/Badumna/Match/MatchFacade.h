//-----------------------------------------------------------------------
// <copyright file="MatchFacade.h" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef BADUMNA_MATCH_FACADE_H
#define BADUMNA_MATCH_FACADE_H

#include <memory>
#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/Match/Match.h"
#include "Badumna/Match/MatchDelegate.h"
#include "Badumna/Match/MatchmakingCriteria.h"
#include "Badumna/Match/MatchmakingResult.h"

namespace Badumna
{
    class MatchFacadeImpl;
        
    /**
     * @class MatchFacade
     * @brief The Match API.
     */
    class BADUMNA_API MatchFacade
    {
        friend class NetworkFacadeImpl;
    public:
        ~MatchFacade();

        /**
         * Create a match as host.
         *
         * The create and remove delegates must remain valid until Match::Leave() is called
         * on the match.
         *
         * @param criteria Matchmaking criteria that will be published to matchmaking server.
         * @param max_players The maximum number of players permitted in the match.
         * @param player_name A name for the local player in the match (uniqueness not enforced).
         * @param create_delegate Called when a new entity needs to be instantiated into the match.
         * @param remove_delegate Called when an entity leaves the match.
         * @param create_hosted_delegate Called when a new hosted entity needs to be instantiaed into the match.
         * @param remove_hosted_delegate Called whan a hosted entity leaves the match.
         *
         * @return A new match.
         */
        std::auto_ptr<Match> CreateMatch(
            MatchmakingCriteria const &criteria,
            int max_players,
            String const &player_name,
            CreateMatchReplicaDelegate create_delegate,
            RemoveMatchReplicaDelegate remove_delegate,
            CreateMatchHostedReplicaDelegate create_hosted_delegate,
            RemoveMatchHostedReplicaDelegate remove_hosted_delegate);
        
        /**
         * Create a match as host with a controller.
         *
         * The create and remove delegates must remain valid until Match::Leave() is called
         * on the match.
         *
         * @param controller The object to use as match controller.
         * @param criteria Matchmaking criteria that will be published to matchmaking server.
         * @param max_players The maximum number of players permitted in the match.
         * @param player_name A name for the local player in the match (uniqueness not enforced).
         * @param create_delegate Called when a new entity needs to be instantiated into the match.
         * @param remove_delegate Called when an entity leaves the match.
         * @param create_hosted_delegate Called when a new hosted entity needs to be instantiaed into the match.
         * @param remove_hosted_delegate Called whan a hosted entity leaves the match.
         *
         * @return A new match.
         */
        std::auto_ptr<Match> CreateMatch(
            IMatchHosted *controller,
            MatchmakingCriteria const &criteria,
            int max_players,
            String const &player_name,
            CreateMatchReplicaDelegate create_delegate,
            RemoveMatchReplicaDelegate remove_delegate,
            CreateMatchHostedReplicaDelegate create_hosted_delegate,
            RemoveMatchHostedReplicaDelegate remove_hosted_delegate);

        /**
         * Create a private match as host.
         *
         * Private matches are only included in FindMatches results
         * if their name was specified in the search criteria.
         *
         * The create and remove delegates must remain valid until Match::Leave() is called
         * on the match.
         *
         * @param criteria Matchmaking criteria that will be published to matchmaking server.
         * @param max_players The maximum number of players permitted in the match.
         * @param player_name A name for the local player in the match (uniqueness not enforced).
         * @param create_delegate Called when a new entity needs to be instantiated into the match.
         * @param remove_delegate Called when an entity leaves the match.
         * @param create_hosted_delegate Called when a new hosted entity needs to be instantiaed into the match.
         * @param remove_hosted_delegate Called whan a hosted entity leaves the match.
         *
         * @return A new match.
         */
        std::auto_ptr<Match> CreatePrivateMatch(
            MatchmakingCriteria const &criteria,
            int max_players,
            String const &player_name,
            CreateMatchReplicaDelegate create_delegate,
            RemoveMatchReplicaDelegate remove_delegate,
            CreateMatchHostedReplicaDelegate create_hosted_delegate,
            RemoveMatchHostedReplicaDelegate remove_hosted_delegate);

        /**
         * Create a private match as host with a controller.
         *
         * Private matches are only included in FindMatches results
         * if their name was specified in the search criteria.
         *
         * The create and remove delegates must remain valid until Match::Leave() is called
         * on the match.
         *
         * @param controller The object to use as match controller.
         * @param criteria Matchmaking criteria that will be published to matchmaking server.
         * @param max_players The maximum number of players permitted in the match.
         * @param player_name A name for the local player in the match (uniqueness not enforced).
         * @param create_delegate Called when a new entity needs to be instantiated into the match.
         * @param remove_delegate Called when an entity leaves the match.
         * @param create_hosted_delegate Called when a new hosted entity needs to be instantiaed into the match.
         * @param remove_hosted_delegate Called whan a hosted entity leaves the match.
         *
         * @return A new match.
         */
        std::auto_ptr<Match> CreatePrivateMatch(
            IMatchHosted *controller,
            MatchmakingCriteria const &criteria,
            int max_players,
            String const &player_name,
            CreateMatchReplicaDelegate create_delegate,
            RemoveMatchReplicaDelegate remove_delegate,
            CreateMatchHostedReplicaDelegate create_hosted_delegate,
            RemoveMatchHostedReplicaDelegate remove_hosted_delegate);

        /**
         * Join a known match.
         *
         * The create and remove delegates must remain valid until Match::Leave() is called
         * on the match.
         *
         * @param matchmaking_result A match obtained by querying the matchmaking server.
         * @param player_name A name for the local player in the match (uniqueness not enforced).
         * @param create_delegate Called when a new entity needs to be instantiated into the match.
         * @param remove_delegate Called when an entity leaves the match.
         * @param create_hosted_delegate Called when a new hosted entity needs to be instantiaed into the match.
         * @param remove_hosted_delegate Called whan a hosted entity leaves the match.
         *
         * @return A match object for managing the match.
         */
        std::auto_ptr<Match> JoinMatch(
            MatchmakingResult const &matchmaking_result,
            String const &player_name,
            CreateMatchReplicaDelegate create_delegate,
            RemoveMatchReplicaDelegate remove_delegate,
            CreateMatchHostedReplicaDelegate create_hosted_delegate,
            RemoveMatchHostedReplicaDelegate remove_hosted_delegate);
        
        /**
         * Join a known match with a controller.
         *
         * The create and remove delegates must remain valid until Match::Leave() is called
         * on the match.
         *
         * @param controller The object to use as match controller.
         * @param matchmaking_result A match obtained by querying the matchmaking server.
         * @param player_name A name for the local player in the match (uniqueness not enforced).
         * @param create_delegate Called when a new entity needs to be instantiated into the match.
         * @param remove_delegate Called when an entity leaves the match.
         * @param create_hosted_delegate Called when a new hosted entity needs to be instantiaed into the match.
         * @param remove_hosted_delegate Called whan a hosted entity leaves the match.
         *
         * @return A match object for managing the match.
         */
        std::auto_ptr<Match> JoinMatch(
            IMatchHosted *controller,
            MatchmakingResult const &matchmaking_result,
            String const &player_name,
            CreateMatchReplicaDelegate create_delegate,
            RemoveMatchReplicaDelegate remove_delegate,
            CreateMatchHostedReplicaDelegate create_hosted_delegate,
            RemoveMatchHostedReplicaDelegate remove_hosted_delegate);

        /**
         * Query the matchmaking server to find matches available to join.
         *
         * The result delegate must remain valid until it is called (i.e.
         * it will be called exactly once).
         *
         * @param criteria Matchmaking criteria.
         * @param result_handler A delegate for handling the matchmaking query result.
         */
        void FindMatches(
            MatchmakingCriteria const &criteria,
            MatchmakingResultDelegate result_handler);


    private:
        explicit MatchFacade(MatchFacadeImpl *impl);

        std::auto_ptr<MatchFacadeImpl> const impl_;
        DISALLOW_COPY_AND_ASSIGN(MatchFacade);
    };
}

#endif // BADUMNA_MATCH_FACADE_H
