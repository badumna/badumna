//-----------------------------------------------------------------------
// <copyright file="MatchFacadeImpl.h" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef BADUMNA_MATCH_FACADE_IMPL_H
#define BADUMNA_MATCH_FACADE_IMPL_H

#include "Badumna/Core/ProxyObject.h"
#include "Badumna/Match/MatchFacade.h"

namespace Badumna
{
    class MatchFacadeImpl : public ProxyObject
    {
        friend class BadumnaRuntime;
    public:
        std::auto_ptr<Match> CreateMatch(
            MatchmakingCriteria const &criteria,
            int max_players,
            String const &player_name,
            CreateMatchReplicaDelegate create_delegate,
            RemoveMatchReplicaDelegate remove_delegate,
            CreateMatchHostedReplicaDelegate create_hosted_delegate,
            RemoveMatchHostedReplicaDelegate remove_hosted_delegate);
                
        std::auto_ptr<Match> CreateMatch(
            IMatchHosted *controller,
            MatchmakingCriteria const &criteria,
            int max_players,
            String const &player_name,
            CreateMatchReplicaDelegate create_delegate,
            RemoveMatchReplicaDelegate remove_delegate,
            CreateMatchHostedReplicaDelegate create_hosted_delegate,
            RemoveMatchHostedReplicaDelegate remove_hosted_delegate);

        std::auto_ptr<Match> CreatePrivateMatch(
            MatchmakingCriteria const &criteria,
            int max_players,
            String const &player_name,
            CreateMatchReplicaDelegate create_delegate,
            RemoveMatchReplicaDelegate remove_delegate,
            CreateMatchHostedReplicaDelegate create_hosted_delegate,
            RemoveMatchHostedReplicaDelegate remove_hosted_delegate);
               
        std::auto_ptr<Match> CreatePrivateMatch(
            IMatchHosted *controller,
            MatchmakingCriteria const &criteria,
            int max_players,
            String const &player_name,
            CreateMatchReplicaDelegate create_delegate,
            RemoveMatchReplicaDelegate remove_delegate,
            CreateMatchHostedReplicaDelegate create_hosted_delegate,
            RemoveMatchHostedReplicaDelegate remove_hosted_delegate);

        std::auto_ptr<Match> JoinMatch(
            MatchmakingResult const &matchmaking_result,
            String const &player_name,
            CreateMatchReplicaDelegate create_delegate,
            RemoveMatchReplicaDelegate remove_delegate,
            CreateMatchHostedReplicaDelegate create_hosted_delegate,
            RemoveMatchHostedReplicaDelegate remove_hosted_delegate);
              
        std::auto_ptr<Match> JoinMatch(
            IMatchHosted *controller,
            MatchmakingResult const &matchmaking_result,
            String const &player_name,
            CreateMatchReplicaDelegate create_delegate,
            RemoveMatchReplicaDelegate remove_delegate,
            CreateMatchHostedReplicaDelegate create_hosted_delegate,
            RemoveMatchHostedReplicaDelegate remove_hosted_delegate);

        void FindMatches(
            MatchmakingCriteria const &criteria,
            MatchmakingResultDelegate result_handler);

        static void InvokeFindMatchesResultsDelegate(MatchmakingResultDelegate *handler, DotNetObject result);

    private:
        MatchFacadeImpl(MonoRuntime *runtime, DotNetObject object);

        DISALLOW_COPY_AND_ASSIGN(MatchFacadeImpl);
    };

    extern "C" void MatchFacadeImpl_InvokeFindMatchesResultsDelegate(MatchmakingResultDelegate *handler, DotNetObject result);
}

#endif // BADUMNA_MATCH_FACADE_IMPL_H