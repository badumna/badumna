//---------------------------------------------------------------------------------
// <copyright file="MemberIdentityImpl.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2013 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include "Badumna/Core/Arguments.h"
#include "Badumna/Core/TypeRegistry.h"
#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/Match/MemberIdentityImpl.h"

namespace Badumna
{
    REGISTER_MANAGED_MONO_TYPE(Match, MemberIdentity);
    MemberIdentityImpl::MemberIdentityImpl(MonoRuntime *runtime, DotNetObject object) 
        : ProxyObject(runtime, object, "Badumna.Match", "MemberIdentity")
    {
    }

    REGISTER_PROPERTY(Match, MemberIdentity, Name)
    String MemberIdentityImpl::Name() const
    {
        return managed_object_.GetPropertyFromName<String>("Name");
    }

    REGISTER_MANAGED_METHOD(Match, MemberIdentity, Equals, 1)
    bool MemberIdentityImpl::operator==(MemberIdentityImpl const &rhs) const
    {
        Arguments args;
        args.AddArgument(rhs.managed_object_.GetManagedObject());
        
        bool result = managed_object_.InvokeMethod<bool>("Equals", args);
        return result;
    }

    bool MemberIdentityImpl::operator !=(MemberIdentityImpl const &rhs) const
    {
        return !MemberIdentityImpl::operator ==(rhs);
    }

    DEFINE_WRAPPER_CLASS_IMPL_COPYCTOR_ASSIGN_OPERATOR(MemberIdentityImpl)
}