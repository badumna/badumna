//-----------------------------------------------------------------------
// <copyright file="MatchmakingResult.h" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef BADUMNA_MATCHMAKING_RESULT_H
#define BADUMNA_MATCHMAKING_RESULT_H

#include <memory>
#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/Match/HostAuthorizationCertificate.h"
#include "Badumna/Match/MatchCapacity.h"
#include "Badumna/Match/MatchmakingCriteria.h"

namespace Badumna
{
    class MatchmakingResultImpl;
        
    /**
     * @class MatchmakingResult
     * @brief Details of a match reported by the matchmaking system.
     */
    class BADUMNA_API MatchmakingResult
    {
        friend class MatchmakingQueryResultImpl;
        friend class ManagedObjectGuard;
    public:
        ~MatchmakingResult();

        HostAuthorizationCertificate GetCertificate() const;

        MatchCapacity GetCapacity() const;

        MatchmakingCriteria GetCriteria() const;

    private:
        explicit MatchmakingResult(MatchmakingResultImpl *impl);

        std::auto_ptr<MatchmakingResultImpl> const impl_;
        DISALLOW_COPY_AND_ASSIGN(MatchmakingResult);
    };
}

#endif // BADUMNA_MATCHMAKING_RESULT_H
