//-----------------------------------------------------------------------
// <copyright file="IMatchReplica.h" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef BADUMNA_IMATCHREPLICA_H
#define BADUMNA_IMATCHREPLICA_H

#include "Badumna/Match/IMatchEntity.h"
#include "Badumna/DataTypes/InputStream.h"
#include "Badumna/DataTypes/BooleanArray.h"

namespace Badumna
{
    /**
     * @class IMatchReplica
     * @brief A replica of an entity.
     */
    class BADUMNA_API IMatchReplica : public virtual IMatchEntity
    {
    public:
        /**
         * Destructor.
         */
        virtual ~IMatchReplica() {}

        /**
         * Deserialize the entity state from the given stream.
         * The data in stream is the same as was written by the original entity in IMatchOriginal.Serialize. 
         * included_part specifies which sections are included in the stream. State changes are guaranteed to be 
         * replicated eventually, however intermediate states may be skipped (i.e. if a particular piece of state 
         * changes twice quickly, the first state change may not make it to all (any) replicas).
         *
         * @param included_part List of parts included in the serialization.
         * @param stream The stream containing the serialized data.
         * @param estimated_milliseconds_since_departure Estimated time in milliseconds since serialization.
         */
        virtual void Deserialize(
            BooleanArray const &included_part, 
            InputStream *stream, 
            int estimated_milliseconds_since_departure) = 0;
    };
}

#endif // BADUMNA_IMATCHREPLICA_H