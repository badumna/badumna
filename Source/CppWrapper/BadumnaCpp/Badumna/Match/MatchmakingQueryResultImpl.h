//-----------------------------------------------------------------------
// <copyright file="MatchmakingQueryResultImpl.h" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef BADUMNA_MATCHMAKING_QUERY_RESULT_IMPL_H
#define BADUMNA_MATCHMAKING_QUERY_RESULT_IMPL_H

#include "Badumna/Core/ProxyObject.h"
#include "Badumna/Match/MatchmakingQueryResult.h"

namespace Badumna
{
    class MatchmakingQueryResultImpl : public ProxyObject
    {
        friend class BadumnaRuntime;
    public:
        MatchError GetError() const;
        std::vector<linked_ptr<MatchmakingResult> > GetResults() const;

    private:
        MatchmakingQueryResultImpl(MonoRuntime *runtime, DotNetObject object);

        DISALLOW_COPY_AND_ASSIGN(MatchmakingQueryResultImpl);
    };
}

#endif // BADUMNA_MATCHMAKING_QUERY_RESULT_IMPL_H