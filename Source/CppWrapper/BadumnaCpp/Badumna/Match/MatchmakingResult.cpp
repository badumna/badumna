//-----------------------------------------------------------------------
// <copyright file="MatchmakingResult.cpp" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include <cassert>
#include "Badumna/Match/MatchmakingResult.h"
#include "Badumna/Match/MatchmakingResultImpl.h"

namespace Badumna
{
    MatchmakingResult::MatchmakingResult(MatchmakingResultImpl *pimpl)
        : impl_(pimpl)
    {
        assert(pimpl != NULL);
    }

    MatchmakingResult::~MatchmakingResult()
    {
    }
    
    //HostAuthorizationCertificate MatchmakingResult::GetCertificate() const
    //{
    //    return impl_->GetCertificate();
    //}

    MatchCapacity MatchmakingResult::GetCapacity() const
    {
        return impl_->GetCapacity();
    }

    MatchmakingCriteria MatchmakingResult::GetCriteria() const
    {
        return impl_->GetCriteria();
    }
}