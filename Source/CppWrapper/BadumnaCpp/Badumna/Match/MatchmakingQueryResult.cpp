//-----------------------------------------------------------------------
// <copyright file="MatchmakingQueryResult.cpp" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include <cassert>
#include "Badumna/Match/MatchmakingQueryResult.h"
#include "Badumna/Match/MatchmakingQueryResultImpl.h"

namespace Badumna
{
    MatchmakingQueryResult::MatchmakingQueryResult(MatchmakingQueryResultImpl *pimpl)
        : impl_(pimpl)
    {
        assert(pimpl != NULL);
    }

    MatchmakingQueryResult::~MatchmakingQueryResult()
    {
    }

    MatchError MatchmakingQueryResult::GetError() const
    {
        return impl_->GetError();
    }

    std::vector<linked_ptr<MatchmakingResult> > MatchmakingQueryResult::GetResults() const
    {
        return impl_->GetResults();
    }
}