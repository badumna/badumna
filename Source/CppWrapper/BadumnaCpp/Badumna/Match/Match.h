//-----------------------------------------------------------------------
// <copyright file="Match.h" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef BADUMNA_MATCH_H
#define BADUMNA_MATCH_H

#include <memory>
#include <vector>
#include "Badumna/Utils/LinkedPtr.h"
#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/Match/IMatchOriginal.h"
#include "Badumna/Match/IMatchHosted.h"
#include "Badumna/Match/MatchDelegate.h"
#include "Badumna/Match/MemberIdentity.h"

namespace Badumna
{
    class MatchImpl;
        
    /**
     * @class Match
     * @brief Represents a match for a finite set of players.
     *
     * Matches support:
     *   - replication
     *   - chat
     *   - host logic
     */
    class BADUMNA_API Match
    {
        friend class MatchFacadeImpl;
        friend class MatchImpl;
    public:
        ~Match();

        /**
         * Gets the identity of the local member.
         */
        std::auto_ptr< ::Badumna::MemberIdentity> MemberIdentity() const;

        /**
         * Gets the unique ID of the match.
         */
        std::auto_ptr<BadumnaId> MatchIdentifier() const;

        /**
         * Gets the current members of the match.
         */
        std::vector<linked_ptr< ::Badumna::MemberIdentity> > Members() const;

        /**
         * Gets the count of members in the match.
         */
        int32_t MemberCount() const;

        /**
         * Gets the current state of the match.
         */
        MatchStatus State() const;

        /**
         * Gets the error, if the match has failed.
         */
        MatchError Error() const;

        /**
         * Sets a delegate to handle match state changes.
         *
         * The delegate will only be called during a call to NetworkFacade.ProcessNetworkState().
         * It must remain valid until Leave() or RemoveStateChangedDelegate() is called.
         *
         * @param handler The delegate to invoke.
         */
        void SetStateChangedDelegate(MatchStateChangedDelegate handler);

        /**
         * Removes the state changed handler set by SetStateChangedDelegate(...).
         */
        void RemoveStateChangedDelegate();

        /**
         * Sets a delegate that handles notification of a new member joining the match.
         *
         * The delegate will only be called during a call to NetworkFacade.ProcessNetworkState().
         * It must remain valid until Leave() or RemoveMemberAddedDelegate() is called.
         *
         * @param handler The delegate to invoke.
         */
        void SetMemberAddedDelegate(MatchMembershipDelegate handler);

        /**
         * Removes the handler set by SetMemberAddedDelegate(...).
         */
        void RemoveMemberAddedDelegate();

        /**
         * Sets a delegate that handles notification of an existing member leaving the match.
         *
         * The delegate will only be called during a call to NetworkFacade.ProcessNetworkState().
         * It must remain valid until Leave() or RemoveMemberRemovedDelegate() is called.
         *
         * @param handler The delegate to invoke.
         */
        void SetMemberRemovedDelegate(MatchMembershipDelegate handler);

        /**
         * Removes the handler set by SetMemberRemovedDelegate(...).
         */
        void RemoveMemberRemovedDelegate();

        /**
         * Sets a delegate that handles notification of an incoming chat message.
         *
         * The delegate will only be called during a call to NetworkFacade.ProcessNetworkState().
         * It must remain valid until Leave() or RemoveChatMessageReceivedDelegate() is called.
         *
         * @param handler The delegate to invoke.
         */
        void SetChatMessageReceivedDelegate(MatchChatMessageDelegate handler);

        /**
         * Removes the handler set by SetChatMessageReceivedDelegate(...).
         */
        void RemoveChatMessageReceivedDelegate();

        /**
         * Register an original entity into a match.
         *
         * @param original Entity to be registered.
         * @param entity_type The entity type.
         */
        void RegisterEntity(IMatchOriginal *original, uint32_t entity_type);

        /**
         * Unregister an original entity from a match.
         *
         * @param original Entity to be unregistered.
         */
        void UnregisterEntity(IMatchOriginal *original);

        /**
         * Register a hosted entity into a match.
         *
         * Entities will only be successfully registered if this peer is the current host.
         *
         * @param original Entity to be registered.
         * @param entity_type The entity type.
         */
        void RegisterHostedEntity(IMatchHosted *hosted, uint32_t entity_type);

        /**
         * Unregister a hosted entity from a match.
         *
         * Entities will only be successfully unregistered if this peer is the current host.
         *
         * @param original Entity to be unregistered.
         */
        void UnregisterHostedEntity(IMatchHosted *hosted);

        /**
         * Indicates that the given entity has state changes that need to be propagated to the match members.
         *
         * @param original The entity with changed state.
         * @param changed_parts A BooleanArray with bits set indicating which parts have changed.
         */
        void FlagForUpdate(IMatchOriginal *original, BooleanArray const &changed_parts);
        
        /**
         * Indicates that the given entity has a state change that need to be propagated to the match members.
         * 
         * @param original The entity with changed state.
         * @param changed_part_index The index of the part of state that has changed.
         */
        void FlagForUpdate(IMatchOriginal *original, int changed_part_index);
        
        /**
         * Indicates that the given entity has state changes that need to be propagated to the match members.
         *
         * @param hosted The entity with changed state.
         * @param changed_parts A BooleanArray with bits set indicating which parts have changed.
         */
        void FlagForUpdate(IMatchHosted *hosted, BooleanArray const &changed_parts);
        
        /**
         * Indicates that the given entity has a state change that need to be propagated to the match members.
         * 
         * @param hosted The entity with changed state.
         * @param changed_part_index The index of the part of state that has changed.
         */
        void FlagForUpdate(IMatchHosted *hosted, int changed_part_index);

        /**
         * Send a chat message to all other members of the match.
         *
         * @param message The message to send.
         */
        void Chat(String message);

        /**
         * Send a chat message to a member of the match.
         *
         * @param member_id The member to send the message to.
         * @param message The message to send.
         */
        void Chat(::Badumna::MemberIdentity const &member_id, String message);

        /**
         * Sends a message to all replicas of the given original.
         *
         * @param original The original entity.
         * @param message_data The message data.
         */
        void SendCustomMessageToReplicas(IMatchOriginal *original, OutputStream *message_data);
        
        /**
         * Sends a message to all replicas of a hosted entity.
         *
         * @param hosted The hosted entity.
         * @param message_data The message data.
         */
        void SendCustomMessageToReplicas(IMatchHosted *hosted, OutputStream *message_data);

        /**
         * Sends a message to the original of a replica.
         *
         * @param replica The replica entity.
         * @param message_data The message data.
         */
        void SendCustomMessageToOriginal(IMatchReplica *replica, OutputStream *message_data);
        
        /**
         * Sends a message to the original of a hosted entity.
         *
         * @param hosted The hosted entity.
         * @param message_data The message data.
         */
        void SendCustomMessageToOriginal(IMatchHosted *hosted, OutputStream *message_data);

        /**
         * Sends a custom message to a member of a match.
         *
         * The message will be received by the match controller entity on the specified member.
         * This method can only be used if there's a match controller.
         *
         * @param member_id The id of the destination member.
         * @param message_data The message data.
         */
        void SendCustomMessageToMember(::Badumna::MemberIdentity const &member_id, OutputStream *message_data);

        /**
         * Sends a custom message to all members of a match.
         *
         * The message will be received by the match controller entity on all members.
         * This method can only be used if there's a match controller.
         *
         * @param message_data The message data.
         */
        void SendCustomMessageToMembers(OutputStream *message_data);

        /**
         * Sends a custom message to the host of a match.
         *
         * The message will be received by the match controller entity on the host.
         * This method can only be used if there's a match controller.
         *
         * @param message_data The message data.
         */
        void SendCustomMessageToHost(OutputStream *message_data);

        /**
         * Close the match after use to free resources.
         */
        void Leave();

    private:
        explicit Match(MatchImpl *impl);

        std::auto_ptr<MatchImpl> const impl_;
        DISALLOW_COPY_AND_ASSIGN(Match);
    };
}

#endif // BADUMNA_MATCH_H
