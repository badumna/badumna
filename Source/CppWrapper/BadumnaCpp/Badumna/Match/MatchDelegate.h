//-----------------------------------------------------------------------
// <copyright file="MatchDelegate.h" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef BADUMNA_MATCH_DELEGATE_H
#define BADUMNA_MATCH_DELEGATE_H

#include "Badumna/Core/Delegate.h"
#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/Match/IMatchReplica.h"
#include "Badumna/Match/MatchmakingQueryResult.h"
#include "Badumna/Match/MatchStatusEventArgs.h"
#include "Badumna/Match/MatchChatEventArgs.h"
#include "Badumna/Match/MemberIdentity.h"

namespace Badumna
{
    class Match;

    /**
     * @class CreateMatchReplicaDelegate
     * @brief The delegate invoked when a new replica needs to be created.
     *
     * A delegate called by the network layer when a new match entity arrives.
     * The delegate should return an instance of <code>IMatchReplica</code> to which remote 
     * updates and custom messages will be applied. The delegate has the following signature:\n
     * \n
     * <code>IMatchReplica * function_name(Match const &match, uint32_t entity_type);</code>\n
     * \n
     * It can be a member function or a free function. The application has the ownership of the returned replica object.
     */
    class BADUMNA_API CreateMatchReplicaDelegate 
        : public Delegate2<IMatchReplica *, Match const &, uint32_t>
    {
    public:
        /**
         * Constructor that constructs a CreateMatchReplicaDelegate with the specified pointers to a member function and 
         * object. 
         *
         * @tparam K Object type.
         * @param fp A pointer to a member function.
         * @param object The object. 
         */
        template<typename K>
        CreateMatchReplicaDelegate(IMatchReplica *(K::*fp)(Match const &, uint32_t),
            K &object) : Delegate2<IMatchReplica *, Match const &, uint32_t>(fp, object)
        {
        }

        /**
         * Constructor that constructs a CreateMatchReplicaDelegate with the specified pointer to a free function.
         *
         * @param fp A pointer to a free function.
         */
        CreateMatchReplicaDelegate(IMatchReplica *(*fp)(Match const &, uint32_t)) 
            : Delegate2<IMatchReplica *, Match const &, uint32_t>(fp)
        {
        }
    };

    /**
     * @class RemoveMatchReplicaDelegate
     * @brief The delegate invoked when a replica needs to be removed. 
     *
     * A delegate called by the network layer when an entity leaves the match.
     * This delegate gives the application layer the opportunity to cleanup any references to the given replica.
     * It indicates that no more updates or custom messages will arrive for this replica.
     * The delegate has the following signature:\n
     * \n
     * <code>void function_name(Match const &scene, IMatchReplica *replica);</code>\n
     * \n
     * It can be a member function or a free function. 
     */
    class BADUMNA_API RemoveMatchReplicaDelegate : public Delegate2<void, Match const &, IMatchReplica *>
    {
    public:
        /**
         * Constructor that constructs an RemoveMatchReplicaDelegate with the specified pointers to a member function and 
         * object. 
         *
         * @tparam K Object type.
         * @param fp A pointer to a member function.
         * @param object The object. 
         */
        template<typename K>
        RemoveMatchReplicaDelegate(void (K::*fp)(Match const &, IMatchReplica *), K &object)
            : Delegate2<void, Match const &, IMatchReplica *>(fp, object)
        {
        }

        /**
         * Constructor that constructs an RemoveMatchReplicaDelegate with the specified pointer to a free function.
         *
         * @param fp A pointer to a free function.
         */
        RemoveMatchReplicaDelegate(void (*fp)(Match const &, IMatchReplica *))
            : Delegate2<void, Match const &, IMatchReplica *>(fp)
        {
        }
    };

    /**
     * @class CreateMatchHostedReplicaDelegate
     * @brief The delegate invoked when a new hosted replica needs to be created.
     *
     * A delegate called by the network layer when a new match hosted entity arrives.
     * The delegate should return an instance of <code>IMatchHosted</code> to which remote 
     * updates and custom messages will be applied. The delegate has the following signature:\n
     * \n
     * <code>IMatchHosted * function_name(Match const &match, uint32_t entity_type);</code>\n
     * \n
     * It can be a member function or a free function. The application has the ownership of the returned hosted entity.
     */
    class BADUMNA_API CreateMatchHostedReplicaDelegate 
        : public Delegate2<IMatchHosted *, Match const &, uint32_t>
    {
    public:
        /**
         * Constructor that constructs a CreateMatchHostedReplicaDelegate with the specified pointers to a member function and 
         * object. 
         *
         * @tparam K Object type.
         * @param fp A pointer to a member function.
         * @param object The object. 
         */
        template<typename K>
        CreateMatchHostedReplicaDelegate(IMatchHosted *(K::*fp)(Match const &, uint32_t),
            K &object) : Delegate2<IMatchHosted *, Match const &, uint32_t>(fp, object)
        {
        }

        /**
         * Constructor that constructs a CreateMatchHostedReplicaDelegate with the specified pointer to a free function.
         *
         * @param fp A pointer to a free function.
         */
        CreateMatchHostedReplicaDelegate(IMatchHosted *(*fp)(Match const &, uint32_t)) 
            : Delegate2<IMatchHosted *, Match const &, uint32_t>(fp)
        {
        }
    };

    /**
     * @class RemoveMatchHostedReplicaDelegate
     * @brief The delegate invoked when a hosted replica needs to be removed. 
     *
     * A delegate called by the network layer when a hosted entity leaves the match.
     * This delegate gives the application layer the opportunity to cleanup any references to the given replica.
     * It indicates that no more updates or custom messages will arrive for this replica.
     * The delegate has the following signature:\n
     * \n
     * <code>void function_name(Match const &scene, IMatchHosted *hosted);</code>\n
     * \n
     * It can be a member function or a free function. 
     */
    class BADUMNA_API RemoveMatchHostedReplicaDelegate : public Delegate2<void, Match const &, IMatchHosted *>
    {
    public:
        /**
         * Constructor that constructs an RemoveMatchHostedReplicaDelegate with the specified pointers to a member function and 
         * object. 
         *
         * @tparam K Object type.
         * @param fp A pointer to a member function.
         * @param object The object. 
         */
        template<typename K>
        RemoveMatchHostedReplicaDelegate(void (K::*fp)(Match const &, IMatchHosted *), K &object)
            : Delegate2<void, Match const &, IMatchHosted *>(fp, object)
        {
        }

        /**
         * Constructor that constructs an RemoveMatchHostedReplicaDelegate with the specified pointer to a free function.
         *
         * @param fp A pointer to a free function.
         */
        RemoveMatchHostedReplicaDelegate(void (*fp)(Match const &, IMatchHosted *))
            : Delegate2<void, Match const &, IMatchHosted *>(fp)
        {
        }
    };

    /**
     * @class MatchmakingResultDelegate
     * @brief Delegate for handling matchmaking query results.
     *
     * A delegate called by the network layer with the results of a <code>FindMatches</code> call.
     * The delegate has the following signature:\n
     * \n
     * <code>void function_name(MatchmakingQueryResult const &result);</code>\n
     * \n
     * It can be a member function or a free function. 
     */
    class BADUMNA_API MatchmakingResultDelegate : public Delegate1<void, MatchmakingQueryResult const &>
    {
    public:
        /**
         * Constructor that constructs an MatchmakingResultDelegate with the specified pointers to a member function and 
         * object. 
         *
         * @tparam K Object type.
         * @param fp A pointer to a member function.
         * @param object The object. 
         */
        template<typename K>
        MatchmakingResultDelegate(void (K::*fp)(MatchmakingQueryResult const &), K &object)
            : Delegate1<void, MatchmakingQueryResult const &>(fp, object)
        {
        }

        /**
         * Constructor that constructs an MatchmakingResultDelegate with the specified pointer to a free function.
         *
         * @param fp A pointer to a free function.
         */
        MatchmakingResultDelegate(void (*fp)(MatchmakingQueryResult const &))
            : Delegate1<void, MatchmakingQueryResult const &>(fp)
        {
        }
    };

    /**
     * @class MatchStateChangedDelegate
     * @brief Delegate for handling match state changes.
     *
     * The delegate has the following signature:\n
     * \n
     * <code>void function_name(Match &match, MatchStatusEventArgs const &status_args);</code>\n
     * \n
     * It can be a member function or a free function.
     */
    class BADUMNA_API MatchStateChangedDelegate : public Delegate2<void, Match &, MatchStatusEventArgs const &>
    {
    public:
        /**
         * Constructor that constructs an MatchStateChangedDelegate with the specified pointers to a member function and 
         * object. 
         *
         * @tparam K Object type.
         * @param fp A pointer to a member function.
         * @param object The object. 
         */
        template<typename K>
        MatchStateChangedDelegate(void (K::*fp)(Match &, MatchStatusEventArgs const &), K &object)
            : Delegate2<void, Match &, MatchStatusEventArgs const &>(fp, object)
        {
        }

        /**
         * Constructor that constructs an MatchStateChangedDelegate with the specified pointer to a free function.
         *
         * @param fp A pointer to a free function.
         */
        MatchStateChangedDelegate(void (*fp)(Match &, MatchStatusEventArgs const &))
            : Delegate2<void, Match &, MatchStatusEventArgs const &>(fp)
        {
        }
    };

    /**
     * @class MatchMembershipDelegate
     * @brief Delegate for handling match membership changes.
     *
     * The delegate has the following signature:\n
     * \n
     * <code>void function_name(Match &match, MemberIdentity const &member_id);</code>\n
     * \n
     * It can be a member function or a free function.
     */
    class BADUMNA_API MatchMembershipDelegate : public Delegate2<void, Match &, MemberIdentity const &>
    {
    public:
        /**
         * Constructor that constructs an MatchMembershipDelegate with the specified pointers to a member function and 
         * object. 
         *
         * @tparam K Object type.
         * @param fp A pointer to a member function.
         * @param object The object. 
         */
        template<typename K>
        MatchMembershipDelegate(void (K::*fp)(Match &, MemberIdentity const &), K &object)
            : Delegate2<void, Match &, MemberIdentity const &>(fp, object)
        {
        }

        /**
         * Constructor that constructs an MatchMembershipDelegate with the specified pointer to a free function.
         *
         * @param fp A pointer to a free function.
         */
        MatchMembershipDelegate(void (*fp)(Match &, MemberIdentity const &))
            : Delegate2<void, Match &, MemberIdentity const &>(fp)
        {
        }
    };

    /**
     * @class MatchChatMessageDelegate
     * @brief Delegate for handling chat messages
     *
     * The delegate has the following signature:\n
     * \n
     * <code>void function_name(Match &match, MatchChatEventArgs const &chat_args);</code>\n
     * \n
     * It can be a member function or a free function.
     */
    class BADUMNA_API MatchChatMessageDelegate : public Delegate2<void, Match &, MatchChatEventArgs const &>
    {
    public:
        /**
         * Constructor that constructs an MatchChatMessageDelegate with the specified pointers to a member function and 
         * object. 
         *
         * @tparam K Object type.
         * @param fp A pointer to a member function.
         * @param object The object. 
         */
        template<typename K>
        MatchChatMessageDelegate(void (K::*fp)(Match &, MatchChatEventArgs const &), K &object)
            : Delegate2<void, Match &, MatchChatEventArgs const &>(fp, object)
        {
        }

        /**
         * Constructor that constructs an MatchChatMessageDelegate with the specified pointer to a free function.
         *
         * @param fp A pointer to a free function.
         */
        MatchChatMessageDelegate(void (*fp)(Match &, MatchChatEventArgs const &))
            : Delegate2<void, Match &, MatchChatEventArgs const &>(fp)
        {
        }
    };
}

#endif // BADUMNA_MATCH_DELEGATE_H