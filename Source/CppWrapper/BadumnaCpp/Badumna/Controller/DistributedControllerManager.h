//---------------------------------------------------------------------------------
// <copyright file="DistributedControllerManager.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_DISTRIBUTED_CONTROLLER_MANAGER_H
#define BADUMNA_DISTRIBUTED_CONTROLLER_MANAGER_H

#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Core/BadumnaMap.h"
#include "Badumna/Controller/CreateControllerDelegate.h"
#include "Badumna/Controller/DistributedSceneController.h"
#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/DataTypes/OutputStream.h"
#include "Badumna/DataTypes/InputStream.h"
#include "Badumna/Replication/ISpatialOriginal.h"
#include "Badumna/Replication/ISpatialReplica.h"
#include "Badumna/Replication/OriginalWrapper.h"
#include "Badumna/Replication/ReplicaWrapper.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class DistributedControllerManager
    {
    public:
        DistributedControllerManager();

        void RegisterCreateDelegate(String const &type_name, CreateControllerDelegate d);
        bool IsTypeRegistered(String const &type_name) const;
        void CreateController(DotNetObject object, String const &unique_name, String const &type_name);

        ISpatialOriginal *TakeControlOfEntity(
            String const &unique_name, 
            BadumnaId const &entity_id, 
            uint32_t entity_type);

        ISpatialReplica *InstantiateRemoteEntity(
            String const &unique_name, 
            BadumnaId const &entity_id, 
            uint32_t entity_type);
        
        void RemoveEntity(String const &unique_name, ISpatialReplica const &replica);
        
        void Checkpoint(String const &unique_name, OutputStream *stream);
        void Recover(String const &unique_name, InputStream *data);
        void Recover(String const &unique_name);
        
        void Process(String const &unique_name, int duration_in_ms);
        void Wake(String const &unique_name);
        void Sleep(String const &unique_name);
        void Shutdown(String const &unique_name);

        inline bool Contains(String const &unique_name) const
        {
            return controller_map_.Contains(unique_name);
        }

    private:
        typedef BadumnaMap<CreateControllerDelegate> CreateControllerDelegateMap;
        typedef BadumnaMap<DistributedSceneController*> ControllerMap;
        
        // controller creators
        CreateControllerDelegateMap create_controller_delegate_map_;
        // controllers
        ControllerMap controller_map_;

        DISALLOW_COPY_AND_ASSIGN(DistributedControllerManager);
    };
}

#endif // BADUMNA_DISTRIBUTED_CONTROLLER_MANAGER_H