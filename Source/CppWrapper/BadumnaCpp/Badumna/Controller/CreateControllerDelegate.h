//---------------------------------------------------------------------------------
// <copyright file="CreateControllerDelegate.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_CREATE_CONTROLLER_DELEGATE_H
#define BADUMNA_CREATE_CONTROLLER_DELEGATE_H

#include <algorithm>
#include "Badumna/Core/Delegate.h"
#include "Badumna/Controller/DistributedSceneController.h"
#include "Badumna/DataTypes/String.h"

namespace Badumna
{
    class NetworkFacade;

    /**
     * @class CreateControllerDelegate.
     * @brief The factory class for creating instances of registered controller types.
     *
     * The delegate has the following signature:\n
     * \n
     * <code>void function_name(NetworkFacade *facade, String const &unique_name);</code>\n
     * \n
     */
    class BADUMNA_API CreateControllerDelegate
    {
    public:
        /**
         * Constructor that constructs an CreateControllerDelegate with specified pointer to member function and object. 
         *
         * @tparam K Object type.
         * @param fp A pointer to a member function.
         * @param object The object. 
         * @param facade The network facade object to be injected into the created controller. 
         */
        template<typename K>
        CreateControllerDelegate(
            DistributedSceneController *(K::*fp)(NetworkFacade *, String const &), 
            K &object, 
            NetworkFacade *facade) 
            : callback_(fp, object),
            facade_(facade)
        {
        }

        /**
         * Constructor.
         *
         * @param fp A function pointer that creates the DistributedSceneController instance.
         * @param facade The network facade object to be injected into the created controller. 
         */
        CreateControllerDelegate(
            DistributedSceneController *(*fp)(NetworkFacade *, String const &), 
            NetworkFacade *facade) 
            : callback_(fp),
            facade_(facade)
        {
        }

#ifndef DOXYGEN_SHOULD_SKIP_THIS
        DistributedSceneController *operator()(String const &unique_name)
        {
            DistributedSceneController* controller = callback_(facade_, unique_name);
            return controller;
        }

        // to avoid the gcc -Weffc++ warning
        CreateControllerDelegate(CreateControllerDelegate const &other)
            :callback_(other.callback_),
            facade_(other.facade_)
        {
        }

        CreateControllerDelegate& operator=(CreateControllerDelegate const &rhs)
        {
            CreateControllerDelegate tmp(rhs);
            std::swap(callback_, tmp.callback_);
            std::swap(facade_, tmp.facade_);

            return *this;
        }
#endif // DOXYGEN_SHOULD_SKIP_THIS

    private:
        Delegate2<DistributedSceneController*, NetworkFacade *, String const &> callback_;
        NetworkFacade * facade_;
    };
}

#endif // BADUMNA_CREATE_CONTROLLER_DELEGATE_H