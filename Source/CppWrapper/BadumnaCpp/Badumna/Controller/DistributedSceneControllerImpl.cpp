//---------------------------------------------------------------------------------
// <copyright file="DistributedSceneControllerImpl.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include "Badumna/Controller/DistributedSceneControllerImpl.h"
#include "Badumna/Core/Arguments.h"

namespace Badumna
{
    DistributedSceneControllerImpl::DistributedSceneControllerImpl(
        MonoRuntime *runtime, 
        DotNetObject object, 
        String const &name)
        : ProxyObject(runtime, object, "Badumna.CppWrapperStub", "DistributedControllerStub"),
        unique_name_(name)
    {
    }

    void DistributedSceneControllerImpl::ConstructControlledEntity(uint32_t entity_type)
    {
        Arguments args;
        args.AddArgument(&entity_type);
        managed_object_.InvokeVirtualMethod("ConstructControlledEntity", args);
    }

    void DistributedSceneControllerImpl::Replicate()
    {
        managed_object_.InvokeVirtualMethod("Replicate");
    }
}