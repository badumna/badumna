//---------------------------------------------------------------------------------
// <copyright file="DistributedSceneControllerImpl.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_DISTRIBUTED_SCENE_CONTROLLER_IMPL_H
#define BADUMNA_DISTRIBUTED_SCENE_CONTROLLER_IMPL_H

#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Core/ProxyObject.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class DistributedSceneControllerImpl : public ProxyObject
    {
        friend class BadumnaRuntime;
    public:
        void ConstructControlledEntity(uint32_t entity_type);
        void Replicate();
        
    protected:
        String unique_name_;

    private:
        DistributedSceneControllerImpl(MonoRuntime *runtime, DotNetObject object, String const &name);
        DISALLOW_COPY_AND_ASSIGN(DistributedSceneControllerImpl);
    };
}

#endif // BADUMNA_DISTRIBUTED_SCENE_CONTROLLER_IMPL_H