//---------------------------------------------------------------------------------
// <copyright file="DistributedSceneController.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include "Badumna/Core/Arguments.h"
#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/Controller/DistributedSceneController.h"
#include "Badumna/Controller/DistributedSceneControllerImpl.h"

namespace Badumna
{
    DistributedSceneController::DistributedSceneController(NetworkFacade *facade, String const &)
        : facade_(facade), 
        impl_(NULL)
    {
        assert(facade != NULL);
    }

    DistributedSceneController::~DistributedSceneController()
    {
    }

    void DistributedSceneController::RegisterImpl(DistributedSceneControllerImpl *impl)
    {
        assert(impl != NULL);
        this->impl_.reset(impl);
    }

    void DistributedSceneController::ConstructControlledEntity(uint32_t entity_type)
    {
        impl_->ConstructControlledEntity(entity_type);
    }

    void DistributedSceneController::Replicate()
    {
        impl_->Replicate();
    }
}