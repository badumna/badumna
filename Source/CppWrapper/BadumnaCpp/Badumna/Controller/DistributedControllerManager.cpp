//---------------------------------------------------------------------------------
// <copyright file="DistributedControllerManager.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include <iostream>
#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/Controller/DistributedControllerManager.h"
#include "Badumna/Utils/Logger.h"

namespace Badumna
{
    DistributedControllerManager::DistributedControllerManager()
        : create_controller_delegate_map_(),
        controller_map_()
    {
    }

    void DistributedControllerManager::RegisterCreateDelegate(String const &type_name, CreateControllerDelegate d)
    {
        create_controller_delegate_map_.AddOrReplace(type_name, d);
    }

    bool DistributedControllerManager::IsTypeRegistered(String const &type_name) const
    {
        return create_controller_delegate_map_.Contains(type_name);
    }

    ISpatialOriginal *DistributedControllerManager::TakeControlOfEntity(
            String const &unique_name, 
            BadumnaId const &entity_id, 
            uint32_t entity_type)
    {
        DistributedSceneController *controller = controller_map_.GetValue(unique_name);
        ISpatialOriginal *original = controller->TakeControlOfEntity(entity_id, entity_type);
        assert(original != NULL);
        return original;
    }

    void DistributedControllerManager::CreateController(
        DotNetObject object, 
        String const &unique_name, 
        String const &type_name)
    {
        assert(object != NULL);

        if(create_controller_delegate_map_.Contains(type_name))
        {
            CreateControllerDelegate d = create_controller_delegate_map_.GetValue(type_name);

            DistributedSceneController *controller = d(unique_name);
            assert(controller != NULL);
            controller->RegisterImpl(BadumnaRuntime::Instance().CreateDistributedControllerImpl(object, unique_name));
            uint32_t entity_type = controller->GetEntityType();
            controller->ConstructControlledEntity(entity_type);

            controller_map_.AddOrReplace(unique_name, controller);
        }
        else
        {
            std::cerr << "Unknown controller name, couldn't create controller." << std::endl;
        }
    }

    void DistributedControllerManager::Shutdown(String const &unique_name)
    {
        DistributedSceneController *to_remove = controller_map_.GetValue(unique_name);
        controller_map_.Remove(unique_name);
        delete to_remove;
    }

    ISpatialReplica *DistributedControllerManager::InstantiateRemoteEntity(
        String const &unique_name, 
        BadumnaId const &entity_id, 
        uint32_t entity_type)
    {
        DistributedSceneController *controller = controller_map_.GetValue(unique_name);
        return controller->InstantiateRemoteEntity(entity_id, entity_type);
    }
        
    void DistributedControllerManager::RemoveEntity(
        String const &unique_name, 
        ISpatialReplica const &replica)
    {
        DistributedSceneController *controller = controller_map_.GetValue(unique_name);
        controller->RemoveEntity(replica);
    }
        
    void DistributedControllerManager::Checkpoint(String const &unique_name, OutputStream *stream)
    {
        assert(stream != NULL);

        DistributedSceneController *controller = controller_map_.GetValue(unique_name);
        controller->Checkpoint(stream);
    }
    
    void DistributedControllerManager::Recover(String const &unique_name, InputStream *stream)
    {
        assert(stream != NULL);

        DistributedSceneController *controller = controller_map_.GetValue(unique_name);
        controller->Recover(stream);
    }
    
    void DistributedControllerManager::Recover(String const &unique_name)
    {
        DistributedSceneController *controller = controller_map_.GetValue(unique_name);
        controller->Recover();
    }
        
    void DistributedControllerManager::Process(String const &unique_name, int duration_in_ms)
    {
        DistributedSceneController *controller = controller_map_.GetValue(unique_name);
        controller->Process(duration_in_ms);
    }
    
    void DistributedControllerManager::Wake(String const &unique_name)
    {
        DistributedSceneController *controller = controller_map_.GetValue(unique_name);
        controller->Wake();
    }
    
    void DistributedControllerManager::Sleep(String const &unique_name)
    {
        DistributedSceneController *controller = controller_map_.GetValue(unique_name);
        controller->Sleep();
    }
}