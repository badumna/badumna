//---------------------------------------------------------------------------------
// <copyright file="DistributedSceneController.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_DISTRIBUTED_SCENE_CONTROLLER_H
#define BADUMNA_DISTRIBUTED_SCENE_CONTROLLER_H

#include <memory>
#include "Badumna/DataTypes/String.h"
#include "Badumna/Replication/ISpatialOriginal.h"
#include "Badumna/Replication/ISpatialReplica.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class NetworkFacade;
    class DistributedSceneControllerImpl;

    /**
     * @class DistributedSceneController
     *
     * @brief A distributed controller for the control of entities within a scene.
     *
     * The DistributedSceneController class provides methods for the creation, removal and migration of entities within
     * a scene and is particularly useful for the controller of non-player characters. Because it is also a distributed 
     * controller the process of choosing when and how to migrate/replicate is handled transparently to the developer.
     *
     * When developing a distributed controller you must be mindful of the fact that you do not have any control over 
     * which peer the code will be run. This means that all operation of the code must be performed within the 
     * Wake/Sleep cycle described below and all interaction with the object should be done through message passing.
     *
     * After a controller has been registered the network will determine an appropriate peer for the code to be executed
     * on. This peer will create an instance of the derived distributed controller and call its Wake() method. This 
     * method idicates that the local instance of the controller is now the recipient of all messages sent to the 
     * controller and is now responsible for handling the processing. Likewise, if the network layer decides another 
     * peer is more appropriate to run the controller the local instance's Sleep() method will be called. 
     * 
     * When the Sleep() method is called any checkpointed data will be sent to the peer now responsible for the 
     * controller so that peer can call Recover() to restart from the checkpointed state. 
     *
     * The Replicate() method can be called at anytime the controller is awake. This will cause the network layer to 
     * call Checkpoint() and replicatesthe returned data to ensure that if the current controlling peer fails another 
     * peer will be able to continue from that point.
     */
    class BADUMNA_API DistributedSceneController
    {
    public:
        /**
         * Constructor. Each controller with the same uniqueName must have the same functionality. Exactly one 
         * DistributedSceneController instance for each unique name will be woken at any one time.
         *
         * @param facade The network facade object.
         * @param name The unique name of the controller.
         */
        DistributedSceneController(NetworkFacade *facade, String const &name);

        /**
         * Destructor.
         */
        virtual ~DistributedSceneController();

        /**
         * Register the internal DistributedSceneControllerImpl implementation object. This method should never be
         * called by application code. 
         */
        void RegisterImpl(DistributedSceneControllerImpl *impl);
        
        /**
         * Constructs the controlled entity. This method should never be called by application code. 
         */
        void ConstructControlledEntity(uint32_t entity_type);

        /**
         * Gets the entity type of the controlled entity.
         *
         * @return The entity type.
         */
        virtual uint32_t GetEntityType() const = 0;
        
        /**
         * This method provides notification that the current instance of the controller is now responsible for the 
         * given entity. 
         *
         * @param entity_id This id of the entity.
         * @param entity_type The type id associated with this entity.
         * @return A new ISpatialOriginal instance of the given remote entity.
         */
        virtual ISpatialOriginal *TakeControlOfEntity(BadumnaId const &entity_id, uint32_t entity_type) = 0;

        /**
         * InstantiateRemoteEntity is called when a new spatial entity arrives within the visible region of this 
         * distributed controller. May occur when the controller is asleep.
         *
         * @param entity_id The id of the remote entity.
         * @param entity_type The type id associated with this entity.
         * @return An instance of the spatial replica.
         */
        virtual ISpatialReplica *InstantiateRemoteEntity(BadumnaId const &entity_id, uint32_t entity_type) = 0;
        
        /**
         * RemoveEntity is called when an entity leaves the visible region of this distributed controller. May occur 
         * when the controller is asleep.
         *
         * @param replica The spatial replica object.
         */
        virtual void RemoveEntity(ISpatialReplica const &replica) = 0;
        
        /**
         * Capture any state the controller would need if it were to run on another machine. The data written to the 
         * given stream may be passed to the Recover() method, possibly on another machine. The application code should 
         * ensure that all information needed for the controller to run in a consistent manner is included.
         *
         * @param stream The stream to write saved state to. 
         */
        virtual void Checkpoint(OutputStream *stream) = 0;
        
        /**
         * Read from the given stream all the state that was previously written to it by the Checkpoint() method. This 
         * method is called after a controller has migrated to another peer.
         * 
         * @param stream The strema to read the checkpoint data from.
         */
        virtual void Recover(InputStream *stream) = 0;
        
        /**
         * This method is called after a controller has migrated to another peer while checkpointed data can not be 
         * located. This notifies the application that the controller's state should be restored to a reasonable
         * default one.
         */
        virtual void Recover() = 0;
        
        /**
         * This method is called periodically (once for every call to NetworkFacade::ProcessNetworkState). All 
         * application behaviors of the controller and its controlled entity should be executed inside this method.
         *
         * @param duration_in_ms The elapsed time in milliseconds since the last call to this method.
         */
        virtual void Process(int duration_in_ms) = 0;

        /**
         * Wake the controller. This method is called when the network layer decides that the local peer is now 
         * responsible for the controller.
         */
        virtual void Wake() = 0;

        /**
         * Put to sleep the controller. This method is called when the network layer decides that another peer is now 
         * responsible for this controller.
         */
        virtual void Sleep() = 0;

    protected:
        /**
         * Replicates any checkpointable state. Calling this method will cause the Checkpoint() method to be called. 
         * The data returned will be replicated on a number of peers to ensure that if the current running peer fails 
         * the next peer responsible for the controller can continue from this point.
         */
        void Replicate();

        /**
         * The facade object.
         */
        NetworkFacade * const facade_;

    private:
        std::auto_ptr<DistributedSceneControllerImpl> impl_;

        DISALLOW_COPY_AND_ASSIGN(DistributedSceneController);
    };
}

#endif // BADUMNA_DISTRIBUTED_SCENE_CONTROLLER_H