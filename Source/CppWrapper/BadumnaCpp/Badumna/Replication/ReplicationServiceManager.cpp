//---------------------------------------------------------------------------------
// <copyright file="ReplicationServiceManager.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------
#include <iostream>
#include <cassert>

#include "Badumna/Core/MonoRuntime.h"
#include "Badumna/Core/InternalUniqueId.h"
#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/Replication/ReplicationServiceManager.h"
#include "Badumna/Utils/Logger.h"

namespace Badumna
{
    ReplicationServiceManager::ReplicationServiceManager()
        : replica_delegate_map_()
    {
    }

    ReplicationServiceManager::~ReplicationServiceManager()
    {
#ifndef NDEBUG
        if(replica_delegate_map_.Size() > 0)
        {
            std::cerr << "There are still delegates in replication service manager." << std::endl;
        }
#endif // NDEBUG
    }

    void ReplicationServiceManager::AddReplicaDelegate(
        UniqueId const &id, 
        CreateSpatialReplicaDelegate create_delegate, 
        RemoveSpatialReplicaDelegate remove_delegate)
    {
        BADUMNA_LOG_INFO("RSM::AddReplicaDelegate, id: " << id.UTF8CStr());
        ReplicaDelegate replica_delegate(create_delegate, remove_delegate);
        replica_delegate_map_.TryAdd(id, replica_delegate);
    }

    void ReplicationServiceManager::RemoveReplicaDelegate(UniqueId const &id)
    {
        BADUMNA_LOG_INFO("RSM::RemoveReplicaDelegate, id: " << id.UTF8CStr());
        replica_delegate_map_.Remove(id);
    }

    CreateSpatialReplicaDelegate ReplicationServiceManager::GetCreateReplicaDelegate(UniqueId const &id) const
    {
        BADUMNA_LOG_INFO("RSM::GetCreateReplicaDelegate, id: " << id.UTF8CStr());
        ReplicaDelegate p = replica_delegate_map_.GetValue(id);
        return p.first;
    }
    
    RemoveSpatialReplicaDelegate ReplicationServiceManager::GetRemoveReplicaDelegate(UniqueId const &id) const
    {
        BADUMNA_LOG_INFO("RSM::GetRemoveReplicaDelegate, id: " << id.UTF8CStr());
        ReplicaDelegate p = replica_delegate_map_.GetValue(id);
        return p.second;
    }

    ISpatialReplica *ReplicationServiceManager::InvokeCreateSpatialReplicaDelegate(
        UniqueId const &unique_id, 
        NetworkScene const &scene, 
        BadumnaId const &entity_id, 
        uint32_t entity_type) 
    {
        BADUMNA_LOG_INFO("RSM::InvokeCreateSpatialReplicaDelegate, id: " << unique_id.UTF8CStr());
        // invoke the delegate to create the native replica 
        return GetCreateReplicaDelegate(unique_id)(scene, entity_id, entity_type);
    }

    void ReplicationServiceManager::InvokeRemoveSpatialReplicaDelegate(
        UniqueId const &delegate_unique_id, 
        NetworkScene const &scene, 
        ReplicaWrapper *replica_wrapper) 
    {
        BADUMNA_LOG_INFO("RSM::InvokeRemoveSpatialReplicaDelegate, id: " << delegate_unique_id.UTF8CStr());
        GetRemoveReplicaDelegate(delegate_unique_id)(scene, *(replica_wrapper->GetReplica()));
    }
}