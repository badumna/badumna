//---------------------------------------------------------------------------------
// <copyright file="ISpatialReplica.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_ISPATIAL_REPLICA_H
#define BADUMNA_ISPATIAL_REPLICA_H

#include "Badumna/DataTypes/InputStream.h"
#include "Badumna/DataTypes/BooleanArray.h"
#include "Badumna/Replication/ISpatialEntity.h"

namespace Badumna
{
    /**
     * @class ISpatialReplica
     * @brief An replica spatial entity. 
     */
    class BADUMNA_API ISpatialReplica : public ISpatialEntity
    {
    public:
        /**
         * Destructor.
         */
        virtual ~ISpatialReplica() {}

        /**
         * Deserialize the spatial entity state from the given stream.
         * The data in stream is the same as was written by the original spatial entity in ISpatialOriginal.Serialize. 
         * The included_part specifies which sections are included in the stream. State changes are guaranteed to be 
         * replicated eventually, however intermediate states may be skipped (i.e. if a particular piece of state 
         * changes twice quickly, the first state change may not make it to all (any) replicas).  Properties defined on 
         * ISpatialEntity (e.g. Position) are serialized/deserialized automatically and will be updated prior to this 
         * method being called (these properties still need to be flagged for update when they change).
         *
         * @param included_part List of parts included in the serialization.
         * @param stream The stream containing the serialized data.
         * @param estimated_milliseconds_since_departure Estimated time in milliseconds since serialization.
         */
        virtual void Deserialize(
            BooleanArray const &included_part, 
            InputStream *stream, 
            int estimated_milliseconds_since_departure) = 0;

        /**
         * Determines whether this is a DeadReckonable entity. Applications should never override this virtual method.
         * 
         * @return A boolean value indicating whether this is a dead reckonable entity. 
         */
        bool IsDeadReckonable() const
        {
            return false;
        }
    };
}

#endif // BADUMNA_ISPATIAL_REPLICA_H