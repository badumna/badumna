//---------------------------------------------------------------------------------
// <copyright file="ReplicaWrapper.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_REPLICA_WRAPPER_H
#define BADUMNA_REPLICA_WRAPPER_H

#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Core/ProxyObject.h"
#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/DataTypes/BooleanArray.h"
#include "Badumna/DataTypes/OutputStream.h"
#include "Badumna/Replication/ISpatialReplica.h"

namespace Badumna
{
    class ReplicaWrapper : public ProxyObject
    {
    public:
        ReplicaWrapper(MonoRuntime *runtime, DotNetObject object, ISpatialReplica *replica);

        void Deserialize(
            BooleanArray const &included_part, 
            InputStream *stream, 
            int estimated_milliseconds_since_departure);

        void SetReplicaProperty(int index, DotNetObject value);
        void SetReplicaProperty(int index, float x, float y, float z);

        // it is always bad to give away internals, but we don't actually own the replica object,
        // this is just a wrapper for the object. 
        ISpatialReplica *GetReplica() const
        {
            return spatial_replica_;
        }

        void HandleEvent(InputStream *stream);
        void SendCustomMessageToOriginal(OutputStream *stream) const;
        void AttemptMovement(Vector3 position);

    private:
        void SynchronizeReplicaPosition();
        ISpatialReplica * const spatial_replica_;

        DISALLOW_COPY_AND_ASSIGN(ReplicaWrapper);
    };
}

#endif // BADUMNA_REPLICA_WRAPPER_H