//---------------------------------------------------------------------------------
// <copyright file="NetworkScene.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_NETWORK_SCENE_H
#define BADUMNA_NETWORK_SCENE_H

#include <memory>

#include "Badumna/DataTypes/String.h"
#include "Badumna/Replication/ISpatialOriginal.h"
#include "Badumna/Utils/BasicTypes.h"


namespace Badumna
{
    class NetworkSceneImpl;

    /**
     * @class NetworkScene
     * @brief A network scene is a distinct space.
     */
    class BADUMNA_API NetworkScene
    {
        friend class NetworkFacadeImpl;
        friend class BadumnaRuntime;
    public:
        /**
         * Destructor.
         */
        ~NetworkScene();

        /**
         * Gets the unique name of the scene.
         * 
         * @return A string that represents the name of the network scene. 
         */
        String Name() const;

        /**
         * Gets a value indicating whether the scene is a mini scene.
         *
         * @return A value indicating whether the scene is a mini scene.
         */
        bool MiniScene() const;

        /**
         * Departs from the given scene, any local entities that were registered on the scene will be unregistered 
         * automatically. This method should never be called in any delegate. 
         */
        void Leave();

        /** 
         * Registers the given entity with the scene.
         * An entity must be registered on a scene in order to receive updates from other entities on that scene, and
         * to propagate its updates to other entities on that scene.  An entity can only be registered on a single
         * scene at a time.  If the entity is currently registered with a different scene calling this method will
         * automatically unregister it from that scene.  The first time an entity is registered on a scene its Guid must 
         * has be never been set. Badumna will generate the Guid for the entity.
         *
         * @param entity The entity.
         * @param entity_type Type of the entity.
         */
        void RegisterEntity(ISpatialOriginal *entity, uint32_t entity_type);

        /**
         * Unregisters the given entity with the scene.
         * Once the entity has been unregisted from the scene it will no longer receive updates from entities on that 
         * scene (unless they share another scene).  Equally, updats from the unregistered entity will not be propagated
         * to entities on that scene.  An entity removal event will be propagated peers that are interested in the 
         * entity at the time.
         *
         * @param entity The locally owned entity to remove from the current scene.
         */
        void UnregisterEntity(ISpatialOriginal const &entity);
    private:
        explicit NetworkScene(NetworkSceneImpl *impl);
        std::auto_ptr<NetworkSceneImpl> const impl_;

        DISALLOW_COPY_AND_ASSIGN(NetworkScene);
    };
}

#endif // BADUMNA_NETWORK_SCENE_H
