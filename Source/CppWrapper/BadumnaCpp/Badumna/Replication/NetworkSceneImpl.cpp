//---------------------------------------------------------------------------------
// <copyright file="NetworkSceneImpl.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>

#include "Badumna/Core/Arguments.h"
#include "Badumna/Core/MonoRuntime.h"
#include "Badumna/Core/TypeRegistry.h"
#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/Core/InternalUniqueId.h"
#include "Badumna/Replication/NetworkSceneImpl.h"
#include "Badumna/Replication/OriginalWrapper.h"

namespace Badumna
{
    REGISTER_MANAGED_MONO_TYPE(CppWrapperStub, SpatialEntityDelegates);
    NetworkSceneImpl::NetworkSceneImpl(
        MonoRuntime *runtime, 
        DotNetObject object,
        String const &unique_scene_id,
        EntityManager *entity_manager,
        ReplicationServiceManager *replication_manager,
        NetworkFacadeImpl *network_facade_impl) 
        : ProxyObject(runtime, object, "Badumna.CppWrapperStub", "SpatialEntityDelegates"),
        scene_id_(unique_scene_id),
        entity_manager_(entity_manager),
        replication_manager_(replication_manager),
        network_facade_(network_facade_impl)
    {
        assert(entity_manager != NULL);
        assert(replication_manager != NULL);
    }

    String NetworkSceneImpl::Name() const
    {
        return managed_object_.GetPropertyFromName<String>("Name");
    }

    bool NetworkSceneImpl::MiniScene() const
    {
        return managed_object_.GetPropertyFromName<bool>("MiniScene");
    }

    void NetworkSceneImpl::Leave()
    {
        // this method may trigger some remove spatial replica calls to be scheduled onto the application queue. 
        // need to get them executed before we remove the detegate.
        assert(network_facade_ != NULL);
        assert(!scene_id_.Empty());

        managed_object_.InvokeMethod("Leave");
        network_facade_->ProcessNetworkState();
        replication_manager_->RemoveReplicaDelegate(scene_id_);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, SpatialEntityDelegates, RegisterEntity, 2)
    void NetworkSceneImpl::RegisterEntity(ISpatialOriginal *entity, uint32_t entity_type)
    {
        assert(entity != NULL);

        // create the original wrapper
        OriginalWrapper *original_wrapper(BadumnaRuntime::Instance().CreateOriginalWrapper(entity));

        // synchronize the original properties with the stub.
        // this must be called before invoking the managed register entity method. 
        original_wrapper->SynchronizeOriginalProperty();

        // actually register the original stub with the scene object.
        Arguments args;
        args.AddArgument(original_wrapper->GetManagedMonoObject());
        args.AddArgument(&entity_type);

        // register the entity, keep the guid and set it to entity
        DotNetObject guid = managed_object_.InvokeMethod("RegisterEntity", args);

        // now the guid is ready. 
        BadumnaId original_entity_guid = BadumnaRuntime::Instance().CreateBadumnaId(guid);
        entity->SetGuid(original_entity_guid);

        // get the unique id string
        String original_id = InternalUniqueIdManager::GetOriginalId(*entity);
        original_wrapper->SetUniqueId(original_id);

        // register the entity with the manager.
        entity_manager_->AddOriginal(original_id, original_wrapper);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, SpatialEntityDelegates, UnregisterEntity, 1)
    void NetworkSceneImpl::UnregisterEntity(ISpatialOriginal const &entity)
    {
        if(!entity.Guid().IsValid())
        {
            // not registered
            return;
        }

        String original_id = InternalUniqueIdManager::GetOriginalId(entity);
        if(!entity_manager_->ContainsOriginal(original_id))
        {
            // not registered, will do nothing
            return;
        }

        OriginalWrapper *original = entity_manager_->GetOriginal(original_id);
        
        // unregister the stub
        Arguments args;
        args.AddArgument(original->GetManagedMonoObject());
        managed_object_.InvokeMethod("UnregisterEntity", args);

        // remove the original and its stub
        entity_manager_->RemoveOriginal(original_id);
    }
}
