//---------------------------------------------------------------------------------
// <copyright file="ISpatialEntity.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_ISPATIAL_ENTITY_H
#define BADUMNA_ISPATIAL_ENTITY_H

#include "Badumna/DataTypes/Vector3.h"
#include "Badumna/Replication/IEntity.h"

namespace Badumna
{
    /**
     * @class ISpatialEntity
     *
     * @brief ISpatialEntity is an interface class that represents a collection of properties common to spatial entities.
     */
    class BADUMNA_API ISpatialEntity : public IEntity
    {
    public:
        /**
         * Destructor.
         */
        virtual ~ISpatialEntity() { }

        /**
         * Gets the centroid of the entity.
         *
         * @return A Vector3 object that represents the centroid of the entity.
         */
        virtual Vector3 Position() const = 0;

        /**
         * Sets the centroid of the entity.
         *
         * @param position The position to set.
         */
        virtual void SetPosition(Vector3 const &position) = 0;

        /**
         * Gets the radius of the entity's bounding sphere.
         *
         * @return A float value that represents the radius of the entity. 
         */
        virtual float Radius() const = 0;

        /**
         * Sets the radius of the entity's bounding sphere.
         *
         * @param val The radius value to set. 
         */
        virtual void SetRadius(float val) = 0;

        /**
         * Gets the radius of the entity's sphere of interest.
         *
         * @return A float value that represents the radius of the entity. 
         */
        virtual float AreaOfInterestRadius() const = 0;

        /**
         * Sets the radius of the entity's sphere of interest.
         *
         * @param val The area of interest radius value to set.
         */
        virtual void SetAreaOfInterestRadius(float val) = 0;

        /**
         * Determines whether this is a DeadReckonable entity. Applications should never override this virtual method.
         * 
         * @return A boolean value indicating whether this is a dead reckonable entity. 
         */
        virtual bool IsDeadReckonable() const = 0;
    };
}

#endif // BADUMNA_ISPATIAL_ENTITY_H