//---------------------------------------------------------------------------------
// <copyright file="ISpatialOriginal.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_ISPATIAL_ORIGINAL_H
#define BADUMNA_ISPATIAL_ORIGINAL_H

#include "Badumna/DataTypes/OutputStream.h"
#include "Badumna/DataTypes/BooleanArray.h"
#include "Badumna/Replication/ISpatialEntity.h"

namespace Badumna
{
    /**
     * @class ISpatialOriginal
     * @brief An original spatial entity. Original entities are replicated to interested peers.
     */
    class BADUMNA_API ISpatialOriginal : public ISpatialEntity
    {
    public:
        /**
         * Destructor.
         */
        virtual ~ISpatialOriginal() {}
        
        /**
         * Serializes the required_parts of the entity on to the stream.
         *
         * @param required_parts Indicates which parts should be serialized. Exactly these parts must be written to the 
         * stream.
         * @param stream The stream to serialize to.
         */
        virtual void Serialize(BooleanArray const &required_parts, OutputStream *stream) = 0;

        /**
         * Determines whether this is a DeadReckonable entity. Applications should never override this virtual method.
         * 
         * @return A boolean value indicating whether this is a dead reckonable entity. 
         */
        bool IsDeadReckonable() const
        {
            return false;
        }
    };
}

#endif // BADUMNA_ISPATIAL_ORIGINAL_H