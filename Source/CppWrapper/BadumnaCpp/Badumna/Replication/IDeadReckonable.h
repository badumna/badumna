//---------------------------------------------------------------------------------
// <copyright file="IDeadReckonable.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_IDEADRECKONABLE_H
#define BADUMNA_IDEADRECKONABLE_H

#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/DataTypes/Vector3.h"

namespace Badumna
{
    /**
     * @class IDeadReckonable
     * @brief IDeadReckonable entity will have its position extrapolated and smoothed between updates.
     */
    class BADUMNA_API IDeadReckonable
    {
    public:
        /**
         * Destructor.
         */
        virtual ~IDeadReckonable() {}

        /**
         * Gets the velocity of the entity.
         *
         * @return A Vector3 object that represents the velocity of the entity.
         */
        virtual Vector3 Velocity() const = 0;

        /**
         * Sets entity's velocity.
         * 
         * @param velocity The velocity to set.
         */
        virtual void SetVelocity(Vector3 const &velocity) = 0;

        /**
         * This method is called on remote copies inside the NetworkFacade.ProcessNetworkState() method.
         * The reckoned_position is the current estimated position of the controlling entity. The implementation of this 
         * method should check that the new position is valid (i.e. check for collisions etc) and apply it if so. 
         *
         * @param reckoned_position The estimated position. 
         */
        virtual void AttemptMovement(Vector3 const &reckoned_position) = 0;
    };
}

#endif // BADUMNA_IDEADRECKONABLE_H