//---------------------------------------------------------------------------------
// <copyright file="IDeadReckonableSpatialReplica.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_IDEADRECKONABLE_SPATIAL_REPLICA_H
#define BADUMNA_IDEADRECKONABLE_SPATIAL_REPLICA_H

#include "Badumna/Replication/IDeadReckonable.h"
#include "Badumna/Replication/ISpatialReplica.h"

namespace Badumna
{
    /**
     * @class IDeadReckonableSpatialReplica
     * @brief DeadReckonable replica entity that will have its position extrapolated and smoothed between updates.
     */
    class BADUMNA_API IDeadReckonableSpatialReplica : public ISpatialReplica, public IDeadReckonable
    {
    public:
        /**
         * Determines whether this is a DeadReckonable entity. Applications should never override this virtual method.
         * 
         * @return A boolean value indicating whether this is a dead reckonable entity. 
         */
        bool IsDeadReckonable() const
        {
            return true;
        }
    };
}

#endif // BADUMNA_IDEADRECKONABLE_SPATIAL_REPLICA_H