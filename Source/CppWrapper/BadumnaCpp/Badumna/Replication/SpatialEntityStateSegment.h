//---------------------------------------------------------------------------------
// <copyright file="SpatialEntityStateSegment.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_SPATIAL_ENTITY_STATE_SEGEMENT_H
#define BADUMNA_SPATIAL_ENTITY_STATE_SEGEMENT_H

namespace Badumna
{
    /**
     * @enum SpatialEntityStateSegment
     *
     * Names for the enity state segment indexes used by the spatial entity system. When spatial entities are defined 
     * they should start numbering state segments from FirstUserSegment onwards (inclusive).
     */
    enum SpatialEntityStateSegment
    {
        StateSegment_Scene = 0,                 /**< Index for state segment holding the scene name */
        StateSegment_Position,                  /**< Index for state segment holding the position */
        StateSegment_Velocity,                  /**< Index for state segment holding the velocity (for IDeadReckonable entities) */
        StateSegment_Radius,                    /**< Index for state segment holding the radius */
        StateSegment_InterestRadius,            /**< Index for state segment holding the interest radius */
        StateSegment_Match,                     /**< Index for state segment holding the match id. */
        StateSegment_FirstAvailableSegment      /**< Index of the first unused state segment index. */
    };
}

#endif // BADUMNA_SPATIAL_ENTITY_STATE_SEGEMENT_H