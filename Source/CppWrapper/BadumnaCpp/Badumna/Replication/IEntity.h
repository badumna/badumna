//---------------------------------------------------------------------------------
// <copyright file="IEntity.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_IENTITY_H
#define BADUMNA_IENTITY_H

#include <vector>

#include "Badumna/DataTypes/InputStream.h"
#include "Badumna/DataTypes/BadumnaId.h"

namespace Badumna
{
    /**
     * @class IEntity
     * @brief IEntity is an interface class that represents a collection of properties common to entities.
     */
    class BADUMNA_API IEntity
    {
    public:
        /**
         * Destructor.
         */
        virtual ~IEntity() {}

        /**
         * Gets the BadumnaId which is a network wide identifier of the entity.
         *
         * @return The BadumnaId object.
         */
        virtual BadumnaId Guid() const = 0;

        /**
         * Sets the BadumnaId.
         *
         * @param id The BadumnaId to set.
         */
        virtual void SetGuid(BadumnaId const &id) = 0;

        /**
         * Processes an event sent to this entity by a call to 
         * <code>NetworkFacade.SendCustomMessageToRemoteCopies()</code>.
         *
         * @param stream A stream containing the serialized event data. 
         */
        virtual void HandleEvent(InputStream *stream) = 0;
    };
}

#endif // BADUMNA_IENTITY_H