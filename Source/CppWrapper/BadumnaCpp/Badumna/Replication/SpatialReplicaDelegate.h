//---------------------------------------------------------------------------------
// <copyright file="SpatialReplicaDelegate.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_SPATIAL_REPLICA_DELEGATE_H
#define BADUMNA_SPATIAL_REPLICA_DELEGATE_H

#include "Badumna/Core/Delegate.h"
#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/Replication/ISpatialReplica.h"
#include "Badumna/Replication/NetworkScene.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    /**
     * @class CreateSpatialReplicaDelegate
     * @brief The delegate invoked when a new replica needs to be created.
     *
     * A delegate called by the network layer when a new spatial entity arrives within the visible region for a 
     * given network scene. The delegate should return an instance of <code>ISpatialReplica</code> to which remote 
     * updates and custom messages will be applied. The delegate has the following signature:\n
     * \n
     * <code>ISpatialReplica * function_name(NetworkScene const &scene, BadumnaId const &entity_id, uint32_t entity_type);</code>\n
     * \n
     * It can be a member function or a free function. The application has the ownership of the returned replica object. 
     */
    class BADUMNA_API CreateSpatialReplicaDelegate 
        : public Delegate3<ISpatialReplica *, NetworkScene const &, BadumnaId const &, uint32_t>
    {
    public:
        /**
         * Constructor that constructs an CreateSpatialReplicaDelegate with the specified pointers to a member function and 
         * object. 
         *
         * @tparam K Object type.
         * @param fp A pointer to a member function.
         * @param object The object. 
         */
        template<typename K>
        CreateSpatialReplicaDelegate(ISpatialReplica *(K::*fp)(NetworkScene const &, BadumnaId const &, uint32_t),
            K &object) : Delegate3<ISpatialReplica *, NetworkScene const &, BadumnaId const &, uint32_t>(fp, object)
        {
        }

        /**
         * Constructor that constructs a CreateSpatialReplicaDelegate with the specified pointer to a free function.
         *
         * @param fp A pointer to a free function.
         */
        CreateSpatialReplicaDelegate(ISpatialReplica *(*fp)(NetworkScene const &, BadumnaId const &, uint32_t)) 
            : Delegate3<ISpatialReplica *, NetworkScene const &, BadumnaId const &, uint32_t>(fp)
        {
        }
    };

    /**
     * @class RemoveSpatialReplicaDelegate
     * @brief The delegate invoked when a replica needs to be removed. 
     *
     * A delegate called by the network layer when an entity leaves the visible region for a given network scene.
     * This delegate gives the application layer the opportunity to cleanup any references to the given replica. It 
     * indicates that no more updates or custom messages will arrive for this replica. The delegate has the following 
     * signature:\n
     * \n
     * <code>void function_name(NetworkScene const &scene, ISpatialReplica const &replica);</code>\n
     * \n
     * It can be a member function or a free function. 
     */
    class BADUMNA_API RemoveSpatialReplicaDelegate : public Delegate2<void, NetworkScene const &, ISpatialReplica const &>
    {
    public:
        /**
         * Constructor that constructs a RemoveSpatialReplicaDelegate with the specified pointers to a member function and 
         * object. 
         *
         * @tparam K Object type.
         * @param fp A pointer to a member function.
         * @param object The object. 
         */
        template<typename K>
        RemoveSpatialReplicaDelegate(void (K::*fp)(NetworkScene const &, ISpatialReplica const &), K &object)
            : Delegate2<void, NetworkScene const &, ISpatialReplica const &>(fp, object)
        {
        }

        /**
         * Constructor that constructs an RemoveSpatialReplicaDelegate with the specified pointer to a free function.
         *
         * @param fp A pointer to a free function.
         */
        RemoveSpatialReplicaDelegate(void (*fp)(NetworkScene const &, ISpatialReplica const &))
            : Delegate2<void, NetworkScene const &, ISpatialReplica const &>(fp)
        {
        }
    };
}

#endif // BADUMNA_SPATIAL_REPLICA_DELEGATE_H