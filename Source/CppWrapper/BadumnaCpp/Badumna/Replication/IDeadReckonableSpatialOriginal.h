//---------------------------------------------------------------------------------
// <copyright file="IDeadReckonableSpatialOriginal.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_IDEADRECKONABLE_SPATIAL_ORIGINAL_H
#define BADUMNA_IDEADRECKONABLE_SPATIAL_ORIGINAL_H

#include "Badumna/Replication/IDeadReckonable.h"
#include "Badumna/Replication/ISpatialOriginal.h"

namespace Badumna
{
    /**
     * @class IDeadReckonableSpatialOriginal
     * @brief DeadReckonable original entity that will have its position extrapolated and smoothed between updates.
     */
    class BADUMNA_API IDeadReckonableSpatialOriginal : public ISpatialOriginal, public IDeadReckonable
    {
    public:
        /**
         * Determines whether this is a DeadReckonable entity. Applications should never override this virtual method.
         * 
         * @return A boolean value indicating whether this is a dead reckonable entity. 
         */
        bool IsDeadReckonable() const
        {
            return true;
        }
    };
}

#endif // BADUMNA_IDEADRECKONABLE_SPATIAL_ORIGINAL_H