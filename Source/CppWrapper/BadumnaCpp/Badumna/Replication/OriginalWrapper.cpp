//---------------------------------------------------------------------------------
// <copyright file="OriginalWrapper.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>

#include "Badumna/Core/TypeRegistry.h"
#include "Badumna/Core/Arguments.h"
#include "Badumna/Replication/OriginalWrapper.h"
#include "Badumna/Replication/SpatialEntityStateSegment.h"
#include "Badumna/Replication/IDeadReckonableSpatialOriginal.h"

namespace Badumna
{
    REGISTER_MANAGED_MONO_TYPE(CppWrapperStub, SpatialOriginalStub);
    OriginalWrapper::OriginalWrapper(MonoRuntime *runtime, DotNetObject object, ISpatialOriginal *original)
        : ProxyObject(runtime, object), 
        spatial_original_(original)
    {
        assert(runtime != NULL && original != NULL && object != NULL);
    }

    void OriginalWrapper::SetUniqueId(String const &id)
    {
        Arguments args;
        DotNetString id_string = MonoRuntime::GetString(id);
        args.AddArgument(id_string);

        managed_object_.InvokeMethod("SetUniqueId", args);
    }

    void OriginalWrapper::Serialize(BooleanArray const &required_parts, OutputStream *stream)
    {
        assert(stream != NULL);
        spatial_original_->Serialize(required_parts, stream);
    }

    void OriginalWrapper::SynchronizeOriginalProperty()
    {
        SynchronizeOriginalProperty(StateSegment_Position);
        SynchronizeOriginalProperty(StateSegment_Radius);
        SynchronizeOriginalProperty(StateSegment_InterestRadius);

        if(spatial_original_->IsDeadReckonable())
        {
            SynchronizeOriginalProperty(StateSegment_Velocity);
        }
    }
    
    REGISTER_MANAGED_METHOD(CppWrapperStub, SpatialOriginalStub, SetPosition, 3)
    REGISTER_MANAGED_METHOD(CppWrapperStub, DeadReckonableOriginalStub, SetVelocity, 3)
    void OriginalWrapper::SynchronizeOriginalProperty(int index)
    {
        assert(index == StateSegment_Position || index == StateSegment_Radius || 
               index == StateSegment_InterestRadius || index == StateSegment_Velocity);

        if(index == StateSegment_Position)
        {
            Vector3 p = spatial_original_->Position();
            float x = p.GetX();
            float y = p.GetY();
            float z = p.GetZ();

            Arguments args;
            args.AddArgument(&x);
            args.AddArgument(&y);
            args.AddArgument(&z);
            managed_object_.InvokeMethod("SetPosition", args); 
        }
        else if(index == StateSegment_Radius)
        {
            managed_object_.SetPropertyValue("Radius", spatial_original_->Radius()); 
        }
        else if(index == StateSegment_InterestRadius)
        {
            managed_object_.SetPropertyValue("AreaOfInterestRadius", spatial_original_->AreaOfInterestRadius()); 
        }
        else if(index == StateSegment_Velocity)
        {
            assert(spatial_original_->IsDeadReckonable());

            IDeadReckonableSpatialOriginal *deadreckonable;
            deadreckonable = static_cast<IDeadReckonableSpatialOriginal *>(spatial_original_);

            Vector3 v = deadreckonable->Velocity();
            float x = v.GetX();
            float y = v.GetY();
            float z = v.GetZ();

            Arguments args;
            args.AddArgument(&x);
            args.AddArgument(&y);
            args.AddArgument(&z);

            managed_object_.InvokeMethod("SetVelocity", args);
        }
    }

    void OriginalWrapper::SynchronizeOriginalProperty(BooleanArray const &changed_parts)
    {
        if(changed_parts[StateSegment_InterestRadius])
        {
            SynchronizeOriginalProperty(StateSegment_InterestRadius);
        }
        
        if(changed_parts[StateSegment_Radius])
        {
            SynchronizeOriginalProperty(StateSegment_Radius);
        }

        if(changed_parts[StateSegment_Position])
        {
            SynchronizeOriginalProperty(StateSegment_Position);
        }
        
        if(changed_parts[StateSegment_Velocity])
        {
            SynchronizeOriginalProperty(StateSegment_Velocity);
        }
    }

    void OriginalWrapper::HandleEvent(InputStream *stream)
    {
        assert(stream != NULL);
        spatial_original_->HandleEvent(stream);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, SpatialOriginalStub, SendCustomMessageToRemoteCopies, 1)
    void OriginalWrapper::SendCustomMessageToRemoteCopies(OutputStream *stream) const
    {
        assert(stream != NULL);
        size_t buffer_len = stream->GetLength();
        if(buffer_len > 0)
        {
            scoped_array<char> buffer(new char[buffer_len]);
            stream->GetBuffer(buffer.get(), buffer_len);

            DotNetArray data_event = MonoRuntime::GetMonoByteArray(buffer.get(), buffer_len);
            Arguments args;
            args.AddArgument(data_event);

            managed_object_.InvokeMethod("SendCustomMessageToRemoteCopies", args);
        }
    }
}