//---------------------------------------------------------------------------------
// <copyright file="ReplicaWrapper.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>

#include "Badumna/Core/TypeRegistry.h"
#include "Badumna/Replication/IDeadReckonable.h"
#include "Badumna/Replication/ReplicaWrapper.h"
#include "Badumna/Replication/SpatialEntityStateSegment.h"
#include "Badumna/Replication/IDeadReckonableSpatialReplica.h"

namespace Badumna
{
    REGISTER_MANAGED_MONO_TYPE(CppWrapperStub, SpatialReplicaStub);
    ReplicaWrapper::ReplicaWrapper(MonoRuntime *runtime, DotNetObject object, ISpatialReplica *replica)
        : ProxyObject(runtime, object),
        spatial_replica_(replica)
    {
        assert(runtime != NULL && object != NULL && replica != NULL);
    }

    void ReplicaWrapper::Deserialize(
        BooleanArray const &included_part, 
        InputStream *stream, 
        int estimated_milliseconds_since_departure)
    {
        assert(stream != NULL);
        spatial_replica_->Deserialize(included_part, stream, estimated_milliseconds_since_departure);
    }

    void ReplicaWrapper::SetReplicaProperty(int index, DotNetObject value)
    {
        assert(value != NULL);
        assert(index == StateSegment_Radius || index == StateSegment_InterestRadius);

        if(index == StateSegment_Radius)
        {
            float native_value = MonoRuntime::UnboxingObject<float>(value);
            spatial_replica_->SetRadius(native_value);
        }
        else if(index == StateSegment_InterestRadius)
        {
            float native_value = MonoRuntime::UnboxingObject<float>(value);
            spatial_replica_->SetAreaOfInterestRadius(native_value);
        }
    }
    
    void ReplicaWrapper::SetReplicaProperty(int index, float x, float y, float z)
    {
        assert(index == StateSegment_Position || index == StateSegment_Velocity);

        if(index == StateSegment_Position)
        {
            Vector3 new_position(x, y, z);
            spatial_replica_->SetPosition(new_position);
        }
        else if(index == StateSegment_Velocity)
        {
            assert(spatial_replica_->IsDeadReckonable());

            Vector3 new_velocity(x, y, z);
            IDeadReckonableSpatialReplica *deadreckonable; 
            deadreckonable = static_cast<IDeadReckonableSpatialReplica *>(spatial_replica_);
            deadreckonable->SetVelocity(new_velocity);
        }
    }

    void ReplicaWrapper::HandleEvent(InputStream *stream)
    {
        assert(stream != NULL);
        spatial_replica_->HandleEvent(stream);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, SpatialReplicaStub, SendCustomMessageToOriginal, 1)
    void ReplicaWrapper::SendCustomMessageToOriginal(OutputStream *stream) const
    {
        assert(stream != NULL);
        size_t buffer_len = stream->GetLength();
        if(buffer_len > 0)
        {
            scoped_array<char> buffer(new char[buffer_len]);
            stream->GetBuffer(buffer.get(), buffer_len);

            DotNetArray data_event = MonoRuntime::GetMonoByteArray(buffer.get(), buffer_len);
            Arguments args;
            args.AddArgument(data_event);

            managed_object_.InvokeMethod("SendCustomMessageToOriginal", args);
        }
    }

    void ReplicaWrapper::AttemptMovement(Vector3 position)
    {
        assert(spatial_replica_->IsDeadReckonable());
        
        IDeadReckonableSpatialReplica *deadreckonable; 
        deadreckonable = static_cast<IDeadReckonableSpatialReplica *>(spatial_replica_);
        deadreckonable->AttemptMovement(position);

        // we don't know what the app code will do with the above specified estimated position. 
        // so have to synchronize the stub and the actual replica. 
        SynchronizeReplicaPosition();
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, SpatialReplicaStub, SynchronizePosition, 3)
    void ReplicaWrapper::SynchronizeReplicaPosition()
    {
        // this should only be called within AttemptMovement to synchronize the replica positions.
        float x = spatial_replica_->Position().GetX();
        float y = spatial_replica_->Position().GetY();
        float z = spatial_replica_->Position().GetZ();

        Arguments args;
        args.AddArgument(&x);
        args.AddArgument(&y);
        args.AddArgument(&z);       

        managed_object_.InvokeMethod("SynchronizePosition", args);
    }
}