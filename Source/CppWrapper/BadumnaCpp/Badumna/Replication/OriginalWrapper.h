//---------------------------------------------------------------------------------
// <copyright file="OriginalWrapper.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_ORIGINAL_WRAPPER_H
#define BADUMNA_ORIGINAL_WRAPPER_H

#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Core/ProxyObject.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/Replication/ISpatialOriginal.h"

namespace Badumna
{
    class OriginalWrapper : public ProxyObject
    {
    public:
        OriginalWrapper(MonoRuntime *runtime, DotNetObject object, ISpatialOriginal *original);

        void SetUniqueId(String const &id);

        void Serialize(BooleanArray const &required_parts, OutputStream *stream);
        
        // synchronize property values between the actual original entity and the stub. 
        void SynchronizeOriginalProperty();
        void SynchronizeOriginalProperty(int index);
        void SynchronizeOriginalProperty(BooleanArray const &changed_parts);

        void HandleEvent(InputStream *stream);
        void SendCustomMessageToRemoteCopies(OutputStream *stream) const;

        ISpatialOriginal *GetSpatialOriginal() const
        {
            return spatial_original_;
        }

    private:
        ISpatialOriginal * const spatial_original_;

        DISALLOW_COPY_AND_ASSIGN(OriginalWrapper);
    };
}

#endif // BADUMNA_ORIGINAL_WRAPPER_H
