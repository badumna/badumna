//---------------------------------------------------------------------------------
// <copyright file="NetworkSceneImpl.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_NETWORK_SCENE_IMPL_H
#define BADUMNA_NETWORK_SCENE_IMPL_H

#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Core/EntityManager.h"
#include "Badumna/Core/NetworkFacadeImpl.h"
#include "Badumna/Core/ProxyObject.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Replication/ISpatialOriginal.h"
#include "Badumna/Replication/ReplicationServiceManager.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class NetworkSceneImpl : public ProxyObject
    {
        friend class BadumnaRuntime;
    public:
        String Name() const;
        bool MiniScene() const;
        void Leave();
        void RegisterEntity(ISpatialOriginal *entity, uint32_t entity_type);
        void UnregisterEntity(ISpatialOriginal const &entity);
    private:
        NetworkSceneImpl(
            MonoRuntime *runtime, 
            DotNetObject object,
            String const &unique_scene_id,
            EntityManager *entity_manager, 
            ReplicationServiceManager *replication_manager,
            NetworkFacadeImpl *network_facade_impl);

        String scene_id_;

        // doesn't own the object
        EntityManager * const entity_manager_;
        ReplicationServiceManager * const replication_manager_;
        NetworkFacadeImpl *const network_facade_;

        DISALLOW_COPY_AND_ASSIGN(NetworkSceneImpl);
    };
}

#endif // BADUMNA_NETWORK_SCENE_IMPL_H
