//---------------------------------------------------------------------------------
// <copyright file="NetworkScene.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include "Badumna/Replication/NetworkScene.h"
#include "Badumna/Replication/NetworkSceneImpl.h"

namespace Badumna
{
    NetworkScene::NetworkScene(NetworkSceneImpl *p) 
        : impl_(p)
    {
        assert(p != NULL);
    }

    NetworkScene::~NetworkScene()
    {
    }

    String NetworkScene::Name() const
    {
        return impl_->Name();
    }

    bool NetworkScene::MiniScene() const
    {
        return impl_->MiniScene();
    }

    void NetworkScene::Leave()
    {
        impl_->Leave();
    }

    void NetworkScene::RegisterEntity(ISpatialOriginal *entity, uint32_t entity_type)
    {
        impl_->RegisterEntity(entity, entity_type);
    }

    void NetworkScene::UnregisterEntity(ISpatialOriginal const &entity)
    {
        impl_->UnregisterEntity(entity);
    }
}
