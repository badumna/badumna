//---------------------------------------------------------------------------------
// <copyright file="ReplicationServiceManager.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_REPLICATION_SERVICE_MANAGER_H
#define BADUMNA_REPLICATION_SERVICE_MANAGER_H

#include <utility>

#include "Badumna/Core/EntityManager.h"
#include "Badumna/Core/BadumnaMap.h"
#include "Badumna/DataTypes/Vector3.h"
#include "Badumna/DataTypes/BooleanArray.h"
#include "Badumna/DataTypes/InputStream.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Replication/OriginalWrapper.h"
#include "Badumna/Replication/ReplicaWrapper.h"
#include "Badumna/Replication/SpatialReplicaDelegate.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class OriginalWrapper;

    class ReplicationServiceManager
    {
    public:
        ReplicationServiceManager();
        ~ReplicationServiceManager();

        // delegates
        void AddReplicaDelegate(
            UniqueId const &id, 
            CreateSpatialReplicaDelegate create_delegate, 
            RemoveSpatialReplicaDelegate remove_delegate);

        void RemoveReplicaDelegate(UniqueId const &id);
    
        CreateSpatialReplicaDelegate GetCreateReplicaDelegate(UniqueId const &id) const;
        RemoveSpatialReplicaDelegate GetRemoveReplicaDelegate(UniqueId const &id) const;

        // invoke spatial replica callbacks
        ISpatialReplica *InvokeCreateSpatialReplicaDelegate(
            UniqueId const &delegate_unique_id, 
            NetworkScene const &scene, 
            BadumnaId const &entity_id, 
            uint32_t entity_type);
        
        void InvokeRemoveSpatialReplicaDelegate(
            UniqueId const &delegate_unique_id, 
            NetworkScene const &scene, 
            ReplicaWrapper *replica_wrapper);

    private:
        // key is the unique id of the scene, the unique id consists of the scene name and the address
        // of the delegate method.  
        typedef std::pair<CreateSpatialReplicaDelegate, RemoveSpatialReplicaDelegate> ReplicaDelegate;
        // key is the internal unique id.
        typedef BadumnaMap<ReplicaDelegate> ReplicaDelegateMap;
        
        // delegates
        ReplicaDelegateMap replica_delegate_map_;
        
        DISALLOW_COPY_AND_ASSIGN(ReplicationServiceManager);
    };
}

#endif // BADUMNA_REPLICATION_SERVICE_MANAGER_H