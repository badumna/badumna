//---------------------------------------------------------------------------------
// <copyright file="ProfilerExceptionDetails.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_PROFILER_EXCEPTION_DETAILS_H
#define BADUMNA_PROFILER_EXCEPTION_DETAILS_H

#include <vector>
#include <string>

namespace Badumna
{
    class ProfilerExceptionDetails
    {
    public:
        ProfilerExceptionDetails()
            : details_()
        {
        }

        void AddDetails(std::string const &d)
        {
            details_.push_back(d);
        }

        int Size() const
        {
            return details_.size();
        }

        int DumpToFile() const
        {
            int c = 0;
            std::ofstream output_file;
            output_file.open("bp_exception_details.txt");

            std::vector<std::string>::const_iterator iter = details_.begin();
            while(iter != details_.end())
            {
                output_file << *iter << std::endl;
                iter++;
                c++;
            }

            output_file.close();
            return c;
        }

    private:
        std::vector<std::string> details_;
    };
}

#endif // BADUMNA_PROFILER_EXCEPTION_DETAILS_H