//---------------------------------------------------------------------------------
// <copyright file="AllocationStats.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_ALLOCATION_STATS_H
#define BADUMNA_ALLOCATION_STATS_H

#include <map>
#include <string>
#include <fstream>

namespace Badumna
{
    class AllocationStats
    {
    public:
        AllocationStats()
            : allocated_objects_stats_()
        {
        }

        void RecordAllocationEvent(std::string const &type_name)
        {
            std::map<std::string, int>::iterator iter = allocated_objects_stats_.find(type_name);
            if(iter != allocated_objects_stats_.end())
            {
                int count = allocated_objects_stats_[type_name];
                allocated_objects_stats_[type_name] = count + 1;
            }
            else
            {
                allocated_objects_stats_.insert(std::make_pair(type_name, 1));
            }
        }

        int Size() const
        {
            return allocated_objects_stats_.size();
        }

        void DumpToFile() const
        {
            std::ofstream output_file;
            output_file.open("bp_allocation_details.txt");

            std::map<std::string, int>::const_iterator iter = allocated_objects_stats_.begin();
            while(iter != allocated_objects_stats_.end())
            {
                output_file << iter->first << "         " << iter->second << std::endl;
                iter++;
            }

            output_file.close();
        }

    private:
        std::map<std::string, int> allocated_objects_stats_;
    };
}

#endif // BADUMNA_ALLOCATION_STATS_H