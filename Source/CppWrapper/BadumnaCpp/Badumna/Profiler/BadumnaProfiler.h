//---------------------------------------------------------------------------------
// <copyright file="BadumnaProfiler.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_BADUMNA_PROFILER_H
#define BADUMNA_BADUMNA_PROFILER_H

void SetupBadumnaProfiler();

#endif // BADUMNA_BADUMNA_PROFILER_H