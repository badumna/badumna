//---------------------------------------------------------------------------------
// <copyright file="Assembly.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_ASSEMBLY_H
#define BADUMNA_ASSEMBLY_H

// suppress level 4 warnings in this external header file. 
#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable : 4127)
#pragma warning(disable : 4510)
#pragma warning(disable : 4512)
#pragma warning(disable : 4610)
#endif

// always included
#include <mono/metadata/assembly.h>

#ifdef _MSC_VER
#pragma warning(pop)
#endif

#endif // BADUMNA_ASSEMBLY_H