//---------------------------------------------------------------------------------
// <copyright file="Logger.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_LOGGER_H
#define BADUMNA_LOGGER_H

#ifdef BADUMNA_LOGGER_ENABLED
#include <string>
#include "Badumna/Utils/Singleton.h"

#ifdef _MSC_VER
#pragma warning(disable : 4127)
#endif

#include <log4cplus/logger.h>
#include <log4cplus/streams.h>

void BadumnaLogTrace(std::string const &msg);
void BadumnaLogDebug(std::string const &msg);
void BadumnaLogInfo(std::string const &msg);
void BadumnaLogWarn(std::string const &msg);
void BadumnaLog(std::string const &msg);
void BadumnaLogWarn(std::string const &msg);
void BadumnaLogFatal(std::string const &msg);
#elif defined(DEBUG)
#include <iostream>
#endif


namespace Badumna
{
#ifdef BADUMNA_LOGGER_ENABLED
    using std::string;

    class Logger : public Singleton<Logger>
    {
        friend class Singleton<Logger>;
    public:
        Logger();
        virtual void Initialize();

        void LogTrace(string const& msg);
        void LogDebug(string const& msg);
        void LogInfo(string const& msg);
        void LogWarn(string const& msg);
        void LogError(string const& msg);
        void LogFatal(string const& msg);

    private:
        static const int CHUNKS = 5;
        static const int UNIT_SIZE = 2048 * 1024;

        bool initialized;
        log4cplus::Logger logger;
    };
#endif // BADUMNA_LOGGER_ENABLED
}

#ifdef BADUMNA_LOGGER_ENABLED
#define BADUMNA_LOG_TRACE(logEvent)                                         \
    do {                                                                    \
        log4cplus::tostringstream _log4cplus_buf;                           \
        _log4cplus_buf << logEvent;                                         \
        ::BadumnaLogTrace(_log4cplus_buf.str());                            \
    }while(0);

#define BADUMNA_LOG_DEBUG(logEvent)                                         \
    do {                                                                    \
        log4cplus::tostringstream _log4cplus_buf;                           \
        _log4cplus_buf << logEvent;                                         \
        ::BadumnaLogDebug(_log4cplus_buf.str());                            \
    }while(0);

#define BADUMNA_LOG_INFO(logEvent)                                          \
    do {                                                                    \
        log4cplus::tostringstream _log4cplus_buf;                           \
        _log4cplus_buf << logEvent;                                         \
        ::BadumnaLogInfo(_log4cplus_buf.str());                             \
    }while(0);

#define BADUMNA_LOG_WARN(logEvent)                                          \
    do {                                                                    \
        log4cplus::tostringstream _log4cplus_buf;                           \
        _log4cplus_buf << logEvent;                                         \
        ::BadumnaLogWarn(_log4cplus_buf.str());                             \
    }while(0);

#define BADUMNA_LOG_ERROR(logEvent)                                         \
    do {                                                                    \
        log4cplus::tostringstream _log4cplus_buf;                           \
        _log4cplus_buf << logEvent;                                         \
        ::BadumnaLogError(_log4cplus_buf.str());                            \
    }while(0);

#define BADUMNA_LOG_FATAL(logEvent)                                         \
    do {                                                                    \
        log4cplus::tostringstream _log4cplus_buf;                           \
        _log4cplus_buf << logEvent;                                         \
        ::BadumnaLogFatal(_log4cplus_buf.str());                            \
    }while(0);

#elif defined(DEBUG)
#define BADUMNA_LOG_TRACE(logEvent)                                         
#define BADUMNA_LOG_DEBUG(logEvent)                                         \
    do {                                                                    \
        std::cerr << "[DEBUG] " << logEvent << std::endl;                   \
    }while(0);

#define BADUMNA_LOG_INFO(logEvent)                                          \
    do {                                                                    \
        std::cerr << "[INFO] " << logEvent << std::endl;                    \
    }while(0);

#define BADUMNA_LOG_WARN(logEvent)                                          \
    do {                                                                    \
        std::cerr << "[WARN] " << logEvent << std::endl;                    \
    }while(0);

#define BADUMNA_LOG_ERROR(logEvent)                                         \
    do {                                                                    \
        std::cerr << "[ERROR] " << logEvent << std::endl;                   \
    }while(0);

#define BADUMNA_LOG_FATAL(logEvent)                                         \
    do {                                                                    \
        std::cerr << "[FATAL] " << logEvent << std::endl;                   \
    }while(0);
#else
#define BADUMNA_LOG_TRACE(logEvent)                                         
#define BADUMNA_LOG_DEBUG(logEvent)                                         
#define BADUMNA_LOG_INFO(logEvent)                                          
#define BADUMNA_LOG_WARN(logEvent)                                          
#define BADUMNA_LOG_ERROR(logEvent)                                         
#define BADUMNA_LOG_FATAL(logEvent)                                         
#endif // BADUMNA_LOGGER_ENABLED

#endif // BADUMNA_LOGGER_H