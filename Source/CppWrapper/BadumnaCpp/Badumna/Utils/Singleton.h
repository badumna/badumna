//---------------------------------------------------------------------------------
// <copyright file="Singleton.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_SINGLETON_H
#define BADUMNA_SINGLETON_H

namespace Badumna
{
#ifndef DOXYGEN_SHOULD_SKIP_THIS
    /**
     * @class Singleton 
     *
     * @brief A Singleton template class. 
     * @tparam The singleton object type. 
     */
    template<class T> 
    class Singleton 
    {
    public:
        /**
         * Get the singleton instance. 
         *
         * @return A reference to the singleton object. 
         */
        static T& Instance() 
        {
            // singletons should not have dependency on each other during destruction
            static T instance;
            return instance;
        }

        /**
         * Destructor
         */
        virtual ~Singleton() {}

    protected:
        /**
         * Constructor
         */
        Singleton() {}

        /**
         * Copy constructor.
         *
         * @param other The other singleton object to copy from.
         */
        explicit Singleton(Singleton const& other);
    private:
        Singleton& operator=(Singleton const & rhs);
    };
#endif // DOXYGEN_SHOULD_SKIP_THIS
}

#endif // BADUMNA_SINGLETON_H