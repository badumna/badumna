//---------------------------------------------------------------------------------
// <copyright file="FileIO.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifdef WIN32
#include <windows.h>
#else
#include <unistd.h>
#include <sys/stat.h>
#include <cassert>
#endif

#include <cstdio>
#include "Badumna/Utils/FileIO.h"

namespace Badumna
{
#ifdef WIN32
    bool FileExists(String const &filename)
    {
        DWORD attrib = GetFileAttributes(filename.UTF8CStr());
        if(INVALID_FILE_ATTRIBUTES == attrib || attrib & FILE_ATTRIBUTE_DIRECTORY)
        {
            return false;
        }

        return true;
    }

    bool DirectoryExists(String const &dirname)
    {
        DWORD attrib = GetFileAttributes(dirname.UTF8CStr());
        if(INVALID_FILE_ATTRIBUTES == attrib)
        {
            return false;
        }

        if(attrib & FILE_ATTRIBUTE_DIRECTORY)
        {
            return true;
        }

        return false;
    }

    String GetCurrentDir()
    {
        char current_path[FILENAME_MAX];
        GetCurrentDirectory(FILENAME_MAX, current_path);

        return current_path;
    }
#else
    bool FileExists(String const &filename)
    {
        struct stat file_stat;
        if(stat(filename.UTF8CStr(), &file_stat) == -1)
        {
            return false;
        }

        if(S_ISDIR(file_stat.st_mode))
        {
            return false;
        }

        return true;
    }

    bool DirectoryExists(String const &dirname)
    {
        struct stat file_stat;
        if(stat(dirname.UTF8CStr(), &file_stat) == -1)
        {
            return false;
        }

        if(S_ISDIR(file_stat.st_mode))
        {
            return true;
        }

        return false;
    }

    String GetCurrentDir()
    {
        char current_path[FILENAME_MAX];
        assert(getcwd(current_path, FILENAME_MAX) != NULL);

        return current_path;
    }
#endif
}