//---------------------------------------------------------------------------------
// <copyright file="Logger.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifdef BADUMNA_LOGGER_ENABLED 
#include <log4cplus/logger.h>
#include <log4cplus/fileappender.h>
#include <log4cplus/layout.h>
#include <log4cplus/ndc.h>
#include <log4cplus/helpers/loglog.h>
#include <string>
#include "Badumna/Utils/Logger.h"

void BadumnaLogTrace(std::string const &msg)
{
    Badumna::Logger::Instance().LogTrace(msg);
}

void BadumnaLogDebug(std::string const &msg)
{
    Badumna::Logger::Instance().LogDebug(msg);
}

void BadumnaLogInfo(std::string const &msg)
{
    Badumna::Logger::Instance().LogInfo(msg);
}

void BadumnaLogWarn(std::string const &msg)
{
    Badumna::Logger::Instance().LogWarn(msg);
}

void BadumnaLogError(std::string const &msg)
{
    Badumna::Logger::Instance().LogError(msg);
}

void BadumnaLogFatal(std::string const &msg)
{
    Badumna::Logger::Instance().LogFatal(msg);
}

namespace Badumna
{
    using namespace log4cplus;
    using std::string;

    Logger::Logger()
        : initialized(false),
        logger()
    {
    }

    void Logger::Initialize()
    {
        std::cout << "logger is being initialized." << std::endl;
        SharedAppenderPtr append_1(new RollingFileAppender(LOG4CPLUS_TEXT("BadumnaCppWrapper.log"), UNIT_SIZE, CHUNKS));
        append_1->setName(LOG4CPLUS_TEXT("Default"));
        append_1->setLayout(std::auto_ptr<Layout>(new TTCCLayout()));
        log4cplus::Logger::getRoot().addAppender(append_1);

        logger = log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("Bdn"));
    }

    void Logger::LogTrace(string const& string)
    {
        if(!initialized)
        {
            Initialize();
            initialized = true;
        }

        LOG4CPLUS_TRACE(logger, string);
    }

    void Logger::LogDebug(string const& string)
    {
        if(!initialized)
        {
            Initialize();
            initialized = true;
        }

        LOG4CPLUS_DEBUG(logger, string);
    }

    void Logger::LogInfo(string const& string)
    {
        if(!initialized)
        {
            Initialize();
            initialized = true;
        }

        NDCContextCreator _context("");
        LOG4CPLUS_INFO(logger, string);
    }

    void Logger::LogWarn(string const& string)
    {
        if(!initialized)
        {
            Initialize();
            initialized = true;
        }

        LOG4CPLUS_WARN(logger, string);
    }

    void Logger::LogError(string const& string)
    {
        if(!initialized)
        {
            Initialize();
            initialized = true;
        }

        LOG4CPLUS_ERROR(logger, string);
    }

    void Logger::LogFatal(string const& string)
    {
        if(!initialized)
        {
            Initialize();
            initialized = true;
        }

        LOG4CPLUS_FATAL(logger, string);
    }
}

#endif //BADUMNA_LOGGER_ENABLED
