//---------------------------------------------------------------------------------
// <copyright file="FileIO.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_FILE_IO_H
#define BADUMNA_FILE_IO_H

#include "Badumna/DataTypes/String.h"

#ifndef DOXYGEN_SHOULD_SKIP_THIS

namespace Badumna
{
    // simple file io related methods. 
    bool FileExists(String const &filename);
    bool DirectoryExists(String const &dirname);
    String GetCurrentDir();
}

#endif // DOXYGEN_SHOULD_SKIP_THIS

#endif // BADUMNA_FILE_IO_H