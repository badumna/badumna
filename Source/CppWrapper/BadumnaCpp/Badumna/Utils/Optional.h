//-----------------------------------------------------------------------
// <copyright file="Optional.h" company="NICTA Pty Ltd">
//     Copyright (c) 2012 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef BADUMNA_OPTIONAL_H
#define BADUMNA_OPTIONAL_H

#include <assert.h>

namespace Badumna
{
    template<typename T>
    class Optional
    {
    public:
        Optional():
            is_set_(false),
            value_((T)0)
        {
        }

        Optional(T value):
            is_set_(true),
            value_(value)
        {
        }

        void Set(T value)
        {
            value_ = value;
            is_set_ = true;
        }

        bool IsSet()
        {
            return is_set_;
        }

        T Get()
        {
            assert(is_set_);
            return value_;
        }

    private:
        bool is_set_;
        T value_;
    };
}

#endif // BADUMNA_OPTIONAL_H
