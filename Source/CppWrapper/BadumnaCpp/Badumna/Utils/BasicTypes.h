//---------------------------------------------------------------------------------
// <copyright file="BasicTypes.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_BASIC_TYPES_H
#define BADUMNA_BASIC_TYPES_H

#ifdef __GNUG__
#include <stdint.h>
#elif defined(_MSC_VER)
/**
 * @typedef __int16 int16_t
 * Defines the 16bits integer.
 */
typedef __int16 int16_t;

/**
 * @typedef unsigned __int16 uint16_t
 * Defines the 16bits unsigned integer.
 */
typedef unsigned __int16 uint16_t;

/**
 * @typedef __int32 int32_t
 * Defines the 32bits integer.
 */
typedef __int32 int32_t;

/**
 * @typedef unsigned __int32 uint32_t
 * Defines the 32bits unsigned integer.
 */
typedef unsigned __int32 uint32_t;

/**
 * @typedef __int64 int64_t
 * Defines the 64bits integer.
 */
typedef __int64 int64_t;

/**
 * @typedef unsigned __int64 int64_t
 * Defines the 64bits unsigned integer.
 */
typedef unsigned __int64 uint64_t;
#endif // __GNUG__

/**
 * Disallow copy and assign. 
 */
#define DISALLOW_COPY_AND_ASSIGN(TypeName)  \
  TypeName(TypeName const &);               \
  void operator=(TypeName const &)

#define DISALLOW_HEAP_OBJECT_ALLOCATION \
    void *operator new(size_t);         \
    void operator delete(void*);        \
    void *operator new[](size_t);       \
    void operator delete[](void*)

// To build the DLL wrapper, BADUMNA_DLL and INSIDE_BADUMNA should be defined. 
// BADUMNA_DLL should be defined when using the DLL in applications. 
#ifdef WIN32
#   ifdef BADUMNA_DLL
#       ifdef INSIDE_BADUMNA
#           define BADUMNA_API __declspec(dllexport)
#       else 
#           define BADUMNA_API __declspec(dllimport)
#       endif
#   else
#       define BADUMNA_API
#   endif // BADUMNA_DLL
#else
#   define BADUMNA_API
#endif // WIN32

// suppress the VS 4251 warning.
#ifdef _MSC_VER
#pragma warning(disable:4251)
#pragma warning(disable:4661)
#endif // _MSC_VER

namespace Badumna
{
    // static assertion
    template<bool b>
    class StaticAssert{ };

    template<>
    class StaticAssert<true>
    {
    public:
        static void Valid() 
        {
        }
    };

    // the class template used to determine whether a specified type is a fundamental build in type.
    template<typename T>
    class IsFundamentalType
    {
    public:
        enum { IsTrue = 0, IsFalse = 1 };
    };

#define REGISTER_FUNDAMENTAL_TYPE(type)     \
    template<>                              \
    class IsFundamentalType<type>           \
    {                                       \
    public:                                 \
        enum { IsFalse = 0, IsTrue = 1 };   \
    };

    REGISTER_FUNDAMENTAL_TYPE(bool)
    REGISTER_FUNDAMENTAL_TYPE(char)
    REGISTER_FUNDAMENTAL_TYPE(wchar_t)
    REGISTER_FUNDAMENTAL_TYPE(unsigned char)
    REGISTER_FUNDAMENTAL_TYPE(short)
    REGISTER_FUNDAMENTAL_TYPE(unsigned short)
    REGISTER_FUNDAMENTAL_TYPE(int)
    REGISTER_FUNDAMENTAL_TYPE(unsigned int)
    REGISTER_FUNDAMENTAL_TYPE(long)
    REGISTER_FUNDAMENTAL_TYPE(unsigned long)
    REGISTER_FUNDAMENTAL_TYPE(float)
    REGISTER_FUNDAMENTAL_TYPE(double)
    REGISTER_FUNDAMENTAL_TYPE(long double)
}

// compile time assert
#define BADUMNA_STATIC_ASSERT(type) Badumna::StaticAssert<type>::Valid()

#endif // BADUMNA_BASIC_TYPES_H