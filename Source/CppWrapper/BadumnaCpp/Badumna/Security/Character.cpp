//---------------------------------------------------------------------------------
// <copyright file="Character.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <sstream>
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/Security/Character.h"

namespace Badumna
{
    using Badumna::String;

    const Character Character::None = Character(-1, String());

    Character::Character(Character const &other)
        : id_(other.Id()),
        name_(other.Name())
    {
    }

    Character::Character(long id, String name)
        : id_(id),
        name_(name)
    {
    }

    Character & Character::operator=(Character const &rhs)
    {
        if(this != &rhs)
        {
            id_ = rhs.Id();
            name_ = rhs.Name();
        }

        return *this;
    }

    String Character::Name() const
    {
        return name_;
    }

    long Character::Id() const
    {
        return id_;
    }

    String Character::ToString() const
    {
        std::ostringstream stream;
        stream << "<#Character " << Id() << ": " << Name().UTF8CStr() << ">";
        return String(stream.str().c_str());
    }

    bool Character::operator==(Character const &rhs) const
    {
        return name_ == rhs.name_
            && id_ == rhs.id_;
    }

    bool Character::operator!=(Character const &rhs) const
    {
        return !((*this) == rhs);
    }
}
