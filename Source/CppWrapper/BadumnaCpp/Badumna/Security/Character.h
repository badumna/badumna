//---------------------------------------------------------------------------------
// <copyright file="Character.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_CHARACTER_H
#define BADUMNA_CHARACTER_H

#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    /**
     * @class Character
     *
     * The in-game identity for a user.
     */
    class BADUMNA_API Character
    {
    public:
        /**
         * The null character, which can be used by servers and other peers
         * which do not need to use any character-related services.
         */
        const static Character None;

        /**
         * Copy constructor that constructs a Character object from another Character object.
         *
         * @param other Another Character object.
         */
        Character(Character const &other);

        /**
         * Construct a Character from an ID and character name.
         *
         * This constructor should rarely be needed, you should typically just use
         * Character objects received from e.g `IdentityProvider.Login()`
         */
        Character(long id, Badumna::String name);

        /**
         * Assignment operator.
         *
         * @param rhs The right hand side parameter of the assignment operation.
         * @return A reference to this Character object.
         */
        Character &operator=(Character const &rhs);

        /**
         * Gets this character's name.
         *
         * @return The character name.
         */
        Badumna::String Name() const;

        /**
         * Gets the error description.
         *
         * @return The character ID
         */
        long Id() const;

        /**
         * Renders a Character as a string for debugging purposes. No guarantee is made that the format
         * of this string will not change.
         *
         * @return A string representation of this Character. 
         */
        Badumna::String ToString() const;

        bool operator==(Character const &rhs) const;

        bool operator!=(Character const &rhs) const;

    private:

        long id_;
        Badumna::String name_;
    };
}

#endif // BADUMNA_CHARACTER_H
