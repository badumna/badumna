//---------------------------------------------------------------------------------
// <copyright file="MonoPInvokes.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include <memory>
#include <vector>
#include <utility>
#include <algorithm>

#include "Badumna/Core/MonoRuntime.h"
#include "Badumna/Core/MonoPInvokes.h"
#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/DataTypes/InputStream.h"
#include "Badumna/DataTypes/OutputStream.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Chat/ChatChannel.h"
#include "Badumna/Replication/NetworkScene.h"
#include "Badumna/Replication/SpatialEntityStateSegment.h"
#include "Badumna/Arbitration/ServiceConnectionResultType.h"
#include "Badumna/Match/MatchFacadeImpl.h"
#include "Badumna/Match/MatchImpl.h"

#include "Badumna/Utils/ScopedPtr.h"
#include "Badumna/Utils/Logger.h"

#ifndef DOXYGEN_SHOULD_SKIP_THIS

#define ADD_PINVOKE_CALL(name, func)                                                \
    {                                                                               \
        void const *function_pointer;                                               \
        function_pointer = reinterpret_cast<void const *>(func);                    \
        details->push_back(make_pair(name, function_pointer));                      \
    };

namespace Badumna
{
    using std::auto_ptr;
    using std::pair;
    using std::make_pair;
    using std::vector;

    void RegisterExternalCall(pair<String, void const *> detail)
    {
        if(detail.first.Find("Badumna.") != 0)
        {
            String full_name = "Badumna.CppWrapperStub.";
            full_name += detail.first;
            mono_add_internal_call(full_name.UTF8CStr(), detail.second);
        }
        else
        {
            mono_add_internal_call(detail.first.UTF8CStr(), detail.second);
        }
    }

    void MonoPInvokes::RegisterExternalCalls()
    {
        BADUMNA_LOG_DEBUG("RegisterExternalCalls is called.");
        
        vector<pair<String, void const *> > details;
        GetInternalCallDetails(&details);

        std::for_each(details.begin(), details.end(), RegisterExternalCall);
    }

    void MonoPInvokes::GetInternalCallDetails(vector<pair<String, void const *> > *details)
    {   
        // system
        ADD_PINVOKE_CALL(
            "Badumna.Controllers.ControllerInitializerHelper::CallGetCppWrapperVersion", 
            InvokeGetCppWrapperVersion);
        // replication related
        ADD_PINVOKE_CALL("SpatialEntityDelegates::CallCreateSpatialReplica", InvokeCreateSpatialReplicaDelegate);
        ADD_PINVOKE_CALL("SpatialEntityDelegates::CallRemoveSpatialReplica", InvokeRemoveSpatialReplicaDelegate);
        ADD_PINVOKE_CALL("SpatialReplicaStub::CallDeserialize", InvokeDeserialize);
        ADD_PINVOKE_CALL("SpatialOriginalStub::CallSerialize", InvokeSerialize);
        ADD_PINVOKE_CALL("SpatialReplicaStub::SetProperty", InvokeSetProperty);
        ADD_PINVOKE_CALL("SpatialReplicaStub::SetPositionProperty", InvokeSetPositionProperty);
        ADD_PINVOKE_CALL("SpatialOriginalStub::CallOriginalHandleEvent", InvokeHandleOriginalEvent);
        ADD_PINVOKE_CALL("SpatialReplicaStub::CallReplicaHandleEvent", InvokeHandleReplicaEvent);
        // deadreckoning related
        ADD_PINVOKE_CALL("DeadReckonableReplicaStub::CallAttemptMovement", InvokeAttemptMovement);
        ADD_PINVOKE_CALL("DeadReckonableReplicaStub::SetVelocityProperty", InvokeSetVelocityProperty);
        // chat related
        ADD_PINVOKE_CALL("ChatSessionStub::CallMessageHandler", InvokeChatMessageHandler);
        ADD_PINVOKE_CALL("ChatSessionStub::CallPresenceHandler", InvokeChatPresenceHandler);
        ADD_PINVOKE_CALL("ChatSessionStub::CallInvitationHandler", InvokeChatInvitationHandler);
        // arbitration related
        ADD_PINVOKE_CALL("ArbitrationClientStub::CallConnectionResultHandler", InvokeArbitrationConnectionDelegate);
        ADD_PINVOKE_CALL("ArbitrationClientStub::CallConnectionFailureHandler", InvokeArbitrationConnectionFailureDelegate);
        ADD_PINVOKE_CALL("ArbitrationClientStub::CallServerMessageHandler", InvokeArbitrationServerMessageDelegate);
        ADD_PINVOKE_CALL("ArbitrationServerStub::CallClientMessageHandler", InvokeArbitrationClientMessageDelegate);
        ADD_PINVOKE_CALL("ArbitrationServerStub::CallClientDisconnectHandler", InvokeArbitrationClientDisconnectDelegate);
        // network facade
        ADD_PINVOKE_CALL("NetworkFacadeStub::CallOnlineEventHandler", InvokeOnlineEventDelegate);
        ADD_PINVOKE_CALL("NetworkFacadeStub::CallOfflineEventHandler", InvokeOfflineEventDelegate);
        ADD_PINVOKE_CALL("NetworkFacadeStub::CallAddressChangedEventHandler", InvokeAddressChangedEventDelegate);
        // controller
        ADD_PINVOKE_CALL("DistributedControllerStub::CallCreateController", InvokeCreateController);
        ADD_PINVOKE_CALL("DistributedControllerStub::CallTakeControlOfDCEntity", InvokeTakeControlOfDCEntity);
        ADD_PINVOKE_CALL("DistributedControllerStub::CallCreateDCSpatialReplica", InvokeCreateDCSpatialReplica);
        ADD_PINVOKE_CALL("DistributedControllerStub::CallRemoveDCSpatialReplica", InvokeRemoveDCSpatialReplica);
        ADD_PINVOKE_CALL("DistributedControllerStub::CallDCCheckpoint", InvokeDCCheckpoint);
        ADD_PINVOKE_CALL("DistributedControllerStub::CallDCRecover", InvokeDCRecover);
        ADD_PINVOKE_CALL("DistributedControllerStub::CallDCRecover2", InvokeDCRecover2);
        ADD_PINVOKE_CALL("DistributedControllerStub::CallDCProcess", InvokeDCProcess);
        ADD_PINVOKE_CALL("DistributedControllerStub::CallDCWake", InvokeDCWake);
        ADD_PINVOKE_CALL("DistributedControllerStub::CallDCSleep", InvokeDCSleep);
        ADD_PINVOKE_CALL("DistributedControllerStub::CallDCShutdown", InvokeDCShutdown);
        ADD_PINVOKE_CALL(
            "Badumna.Controllers.ControllerInitializerHelper::CallIsRequiredControllerNameRegistered", 
            InvokeIsRequiredControllerNameRegistered);
        // streaming
        ADD_PINVOKE_CALL("StreamingManagerStub::CallStreamSendComplete", InvokeStreamSendComplete);
        ADD_PINVOKE_CALL("StreamingManagerStub::CallStreamRequestHandler", InvokeStreamRequestHandle);
        ADD_PINVOKE_CALL("StreamRequestStub::CallStreamReceiveComplete", InvokeStreamReceiveComplete);
        // match
        ADD_PINVOKE_CALL("MatchFacadeStub::InvokeFindMatchesResultsDelegate", MatchFacadeImpl_InvokeFindMatchesResultsDelegate);
        ADD_PINVOKE_CALL("MatchStub::InvokeMatchStateChangedDelegate", MatchImpl_InvokeMatchStateChangedDelegate);
        ADD_PINVOKE_CALL("MatchStub::InvokeMemberAddedDelegate", MatchImpl_InvokeMemberAddedDelegate);
        ADD_PINVOKE_CALL("MatchStub::InvokeMemberRemovedDelegate", MatchImpl_InvokeMemberRemovedDelegate);
        ADD_PINVOKE_CALL("MatchStub::InvokeChatMessageDelegate", MatchImpl_InvokeChatMessageDelegate);
        ADD_PINVOKE_CALL("MatchStub::InvokeMatchCreateReplicaDelegate", MatchImpl_InvokeMatchCreateReplicaDelegate);
        ADD_PINVOKE_CALL("MatchStub::InvokeMatchRemoveReplicaDelegate", MatchImpl_InvokeMatchRemoveReplicaDelegate);
        ADD_PINVOKE_CALL("MatchStub::InvokeMatchCreateHostedReplicaDelegate", MatchImpl_InvokeMatchCreateHostedReplicaDelegate);
        ADD_PINVOKE_CALL("MatchStub::InvokeMatchRemoveHostedReplicaDelegate", MatchImpl_InvokeMatchRemoveHostedReplicaDelegate);
        ADD_PINVOKE_CALL("MatchOriginalStub::CallSerialize", MatchImpl_Serialize);
        ADD_PINVOKE_CALL("MatchOriginalStub::CallHandleEvent", MatchImpl_OriginalHandleEvent);
        ADD_PINVOKE_CALL("MatchReplicaStub::CallDeserialize", MatchImpl_Deserialize);
        ADD_PINVOKE_CALL("MatchReplicaStub::CallHandleEvent", MatchImpl_ReplicaHandleEvent);
        ADD_PINVOKE_CALL("MatchHostedStub::CallSerialize", MatchImpl_HostedSerialize);
        ADD_PINVOKE_CALL("MatchHostedStub::CallDeserialize", MatchImpl_HostedDeserialize);
        ADD_PINVOKE_CALL("MatchHostedStub::CallHandleEvent", MatchImpl_HostedHandleEvent);
    }
    
    int InvokeGetCppWrapperVersion()
    {
        return 2;
    }

    // replication
    DotNetObject InvokeCreateSpatialReplicaDelegate(
        DotNetString unique_id, 
        DotNetObject scene, 
        DotNetString scene_id,
        DotNetObject entity_id, 
        uint32_t entity_type)
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokeCreateSpatialReplicaDelegate is called.");

        assert(unique_id != NULL && scene != NULL && scene_id != NULL && entity_id != NULL);
        String id = MonoRuntime::MonoStringToWString(unique_id);
        
        auto_ptr<NetworkScene> network_scene(
            BadumnaRuntime::Instance().CreateNetworkScene(
                scene, 
                MonoPInvokes::EntityService(), 
                MonoPInvokes::ReplicationManager()
            )
        );

        BadumnaId badumna_id = BadumnaRuntime::Instance().CreateBadumnaId(entity_id);
        ISpatialReplica *replica = MonoPInvokes::ReplicationManager()->InvokeCreateSpatialReplicaDelegate(
            id, *(network_scene.get()), badumna_id, entity_type);
        UniqueId replica_id = InternalUniqueIdManager::GetReplicaId(
            *replica, 
            MonoRuntime::MonoStringToWString(scene_id));
        ReplicaWrapper *wrapper = BadumnaRuntime::Instance().CreateReplicaWrapper(replica_id, replica);
        MonoPInvokes::EntityService()->AddReplica(replica_id, wrapper);

        return wrapper->GetManagedMonoObject();
    }

    void InvokeRemoveSpatialReplicaDelegate(
        DotNetString unique_id, 
        DotNetObject scene,
        DotNetString replica_unique_id)
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokeRemoveSpatialReplicaDelegate is called.");

        assert(unique_id != NULL && scene != NULL && replica_unique_id != NULL);
        String delegate_id = MonoRuntime::MonoStringToWString(unique_id);
        auto_ptr<NetworkScene> network_scene(
            BadumnaRuntime::Instance().CreateNetworkScene(
                scene, 
                MonoPInvokes::EntityService(), 
                MonoPInvokes::ReplicationManager()
            )
        );
        String replica_id = MonoRuntime::MonoStringToWString(replica_unique_id);

        if(MonoPInvokes::EntityService()->ContainsReplica(replica_id))
        {
            ReplicaWrapper *replica_wrapper = MonoPInvokes::EntityService()->GetReplica(replica_id);
            MonoPInvokes::ReplicationManager()->InvokeRemoveSpatialReplicaDelegate(delegate_id, 
                *(network_scene.get()), replica_wrapper);
            MonoPInvokes::EntityService()->RemoveReplica(replica_id);
        }
    }

    DotNetArray InvokeSerialize(DotNetString unique_id, DotNetObject required_parts)
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokeSerialize is called.");

        assert(unique_id != NULL && required_parts != NULL);
        OutputStream output_stream;
        String original_id = MonoRuntime::MonoStringToWString(unique_id);
        BooleanArray boolean_array = BadumnaRuntime::Instance().CreateBooleanArray(required_parts);

        if(MonoPInvokes::EntityService()->ContainsOriginal(original_id))
        {
            MonoPInvokes::EntityService()->InvokeSerialize(original_id, boolean_array, &output_stream);
        }

        return MonoRuntime::OutputStreamToMonoArray(&output_stream);
    }

    void InvokeDeserialize(
        DotNetString unique_id, 
        DotNetObject included_parts, 
        DotNetArray data, 
        int estimated_milliseconds_since_departure)
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokeDeserialize is called.");

        assert(unique_id != NULL && included_parts != NULL && data != NULL);
        String replica_id = MonoRuntime::MonoStringToWString(unique_id);
        std::pair<char *, size_t> array_data = MonoRuntime::GetArrayData(data);
        
        if(array_data.second > 0)
        {
            scoped_array<char> buffer(array_data.first);
            BooleanArray boolean_array = BadumnaRuntime::Instance().CreateBooleanArray(included_parts);
            InputStream stream(buffer.get(), array_data.second);

            if(MonoPInvokes::EntityService()->ContainsReplica(replica_id))
            {
                MonoPInvokes::EntityService()->InvokeDeserialize(
                    replica_id, 
                    boolean_array,
                    &stream,
                    estimated_milliseconds_since_departure);
            }
        }
    }

    void InvokeSetProperty(DotNetString unique_id, int property_index, DotNetObject value)
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokeSetProperty, pindex = " << property_index);

        assert(unique_id != NULL && value != NULL);
        String replica_id = MonoRuntime::MonoStringToWString(unique_id);
        if(MonoPInvokes::EntityService()->ContainsReplica(replica_id))
        {
            MonoPInvokes::EntityService()->InovkeSetReplicaProperty(replica_id, property_index, value);
        }
    }

    void InvokeSetPositionProperty(DotNetString unique_id, int property_index, float x, float y, float z)
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokeSetPositionProperty, pindex = " << property_index);
        assert(unique_id != NULL);

        String replica_id = MonoRuntime::MonoStringToWString(unique_id);
        if(MonoPInvokes::EntityService()->ContainsReplica(replica_id))
        {
            MonoPInvokes::EntityService()->InovkeSetReplicaProperty(replica_id, property_index, x, y, z);
        }
    }

    void InvokeSetVelocityProperty(DotNetString unique_id, float x, float y, float z)
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokeSetVelocityProperty");
        
        assert(unique_id != NULL);
        String replica_id = MonoRuntime::MonoStringToWString(unique_id);
        if(MonoPInvokes::EntityService()->ContainsReplica(replica_id))
        {
            MonoPInvokes::EntityService()->InovkeSetReplicaProperty(replica_id, StateSegment_Velocity, x, y, z);
        }
    }

    void InvokeAttemptMovement(DotNetString unique_id, float x, float y, float z)
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokeAttemptMovement");
        
        assert(unique_id != NULL);
        String replica_id = MonoRuntime::MonoStringToWString(unique_id);
        Vector3 position(x, y, z);
        if(MonoPInvokes::EntityService()->ContainsReplica(replica_id))
        {
            MonoPInvokes::EntityService()->InvokeAttemptMovement(replica_id, position);
        }
    }

    void InvokeHandleEvent(DotNetString unique_id, DotNetArray data, bool original)
    {
        BADUMNA_LOG_INFO("InvokeHandleEvent");

        assert(unique_id != NULL && data != NULL);
        String id = MonoRuntime::MonoStringToWString(unique_id);
        std::pair<char *, size_t> array_data = MonoRuntime::GetArrayData(data);
    
        if(array_data.second > 0)
        {
            scoped_array<char> buffer(array_data.first);
            InputStream input_stream(buffer.get(), array_data.second);

            if(original)
            {
                if(MonoPInvokes::EntityService()->ContainsOriginal(id))
                {
                    MonoPInvokes::EntityService()->InvokeHandleOriginalEvent(id, &input_stream);
                }
            }
            else
            {
                if(MonoPInvokes::EntityService()->ContainsReplica(id))
                {
                    MonoPInvokes::EntityService()->InvokeHandleReplicaEvent(id, &input_stream);
                }
            }
        }
    }

    void InvokeHandleOriginalEvent(DotNetString unique_id, DotNetArray data)
    {
        InvokeHandleEvent(unique_id, data, true);
    }

    void InvokeHandleReplicaEvent(DotNetString unique_id, DotNetArray data)
    {
        InvokeHandleEvent(unique_id, data, false);
    }

    // proximity chat
    void InvokeChatMessageHandler(
        DotNetString unique_id, 
        DotNetObject channel,
        DotNetObject user_id, 
        DotNetString message)
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokeChatMessageHandler");

        assert(unique_id != NULL && channel != NULL && user_id != NULL && message != NULL);
        String session_id = MonoRuntime::MonoStringToWString(unique_id);
        String chat_message = MonoRuntime::MonoStringToWString(message);
        ChatChannelId chat_channel_id = BadumnaRuntime::Instance().CreateChatChannelId(channel);
        BadumnaId sender_id = BadumnaRuntime::Instance().CreateBadumnaId(user_id);

        MonoPInvokes::ChatManager()->InvokeMessageHandler(session_id, chat_channel_id, sender_id, chat_message);
    }

    void InvokeChatPresenceHandler(
        DotNetString unique_id, 
        DotNetObject channel, 
        DotNetObject user_id, 
        DotNetString display_name, 
        int status)
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokePresenceHandler, name = " << display_name << ", status = " << status);

        assert(unique_id != NULL && channel != NULL && user_id != NULL && display_name != NULL);
        String session_id = MonoRuntime::MonoStringToWString(unique_id);
        ChatChannelId chat_channel_id = BadumnaRuntime::Instance().CreateChatChannelId(channel);
        BadumnaId sender_id = BadumnaRuntime::Instance().CreateBadumnaId(user_id);
        String username = MonoRuntime::MonoStringToWString(display_name);
        ChatStatus s = static_cast<ChatStatus>(status);

        MonoPInvokes::ChatManager()->InvokePresenceHandler(session_id, chat_channel_id, sender_id, username, s);
    }

    void InvokeChatInvitationHandler(
        DotNetString unique_id, 
        DotNetObject channel, 
        DotNetString username)
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokePrivateChatInvitationHandler, name = " << username);

        assert(unique_id != NULL && channel != NULL && username != NULL);
        String session_id = MonoRuntime::MonoStringToWString(unique_id);
        ChatChannelId channel_id = BadumnaRuntime::Instance().CreateChatChannelId(channel);
        String sender_name = MonoRuntime::MonoStringToWString(username);

        MonoPInvokes::ChatManager()->InvokeInvitationHandler(session_id, channel_id, sender_name);
    }

    // arbitration
    void InvokeArbitrationConnectionDelegate(DotNetString unique_id, int type)
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokeArbitrationConnectionDelegate");

        assert(unique_id != NULL);
        String id = MonoRuntime::MonoStringToWString(unique_id);
        ServiceConnectionResultType t = (ServiceConnectionResultType)type;
        MonoPInvokes::ArbitrationManager()->InvokeConnectionDelegate(id, t);
    }

    void InvokeArbitrationConnectionFailureDelegate(DotNetString unique_id)
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokeArbitrationConnectionFailureDelegate");

        assert(unique_id != NULL);
        String id = MonoRuntime::MonoStringToWString(unique_id);
        MonoPInvokes::ArbitrationManager()->InvokeConnectionFailureDelegate(id);
    }

    void InvokeArbitrationServerMessageDelegate(DotNetString unique_id, DotNetArray message)
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokeArbitrationServerMessageDelegate");

        assert(unique_id != NULL && message != NULL);
        String id = MonoRuntime::MonoStringToWString(unique_id);
        std::pair<char *, size_t> array_data = MonoRuntime::GetArrayData(message);

        if(array_data.second > 0)
        {
            scoped_array<char> buffer(array_data.first);
            InputStream input_stream(buffer.get(), array_data.second);
            MonoPInvokes::ArbitrationManager()->InvokeServerMessageDelegate(id, &input_stream);
        }
    }

    void InvokeArbitrationClientMessageDelegate(DotNetString unique_id, int session_id, DotNetArray  message)
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokeArbitrationClientMessageDelegate");

        assert(unique_id != NULL && message != NULL);
        String id = MonoRuntime::MonoStringToWString(unique_id);
        std::pair<char *, size_t> array_data = MonoRuntime::GetArrayData(message);
        if(array_data.second > 0)
        {
            scoped_array<char> buffer(array_data.first);
            InputStream input_stream(buffer.get(), array_data.second);
            MonoPInvokes::ArbitrationManager()->InvokeClientMessageDelegate(id, session_id, &input_stream);
        }
    }

    void InvokeArbitrationClientDisconnectDelegate(DotNetString unique_id, int session_id)
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokeArbitrationClientDisconnectDelegate");

        assert(unique_id != NULL);
        String id = MonoRuntime::MonoStringToWString(unique_id);
        MonoPInvokes::ArbitrationManager()->InvokeClientDisconnectDelegate(id, session_id);
    }

    // facade
    void InvokeOnlineEventDelegate()
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokeOnlineEventDelegate");
        MonoPInvokes::FacadeManager()->InvokeOnlineEventDelegate();
    }
    
    void InvokeOfflineEventDelegate()
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokeOfflineEventDelegate");
        MonoPInvokes::FacadeManager()->InvokeOfflineEventDelegate();
    }

    void InvokeAddressChangedEventDelegate()
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokeAddressChangedEventDelegate");
        MonoPInvokes::FacadeManager()->InvokeAddressChangedEventDelegate();
    }

    // distributed controller
    void InvokeCreateController(DotNetObject object, DotNetString unique_name, DotNetString type_name)
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokeCreateController");

        assert(object != NULL && unique_name != NULL && type_name != NULL);
        MonoPInvokes::ControllerManager()->CreateController(
            object, 
            MonoRuntime::MonoStringToWString(unique_name), 
            MonoRuntime::MonoStringToString(type_name));
    }

    DotNetObject InvokeTakeControlOfDCEntity(DotNetString unique_name, DotNetObject entity_id, uint32_t entity_type)
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokeTakeControlOfDCEntity");

        assert(unique_name != NULL && entity_id != NULL);

        if(MonoPInvokes::ControllerManager()->Contains(MonoRuntime::MonoStringToWString(unique_name)))
        {
            ISpatialOriginal *original = MonoPInvokes::ControllerManager()->TakeControlOfEntity(
                MonoRuntime::MonoStringToWString(unique_name),
                BadumnaRuntime::Instance().CreateBadumnaId(entity_id),
                entity_type);

            OriginalWrapper *original_wrapper(BadumnaRuntime::Instance().CreateOriginalWrapper(original));
            original_wrapper->SynchronizeOriginalProperty();

            String original_id = InternalUniqueIdManager::GetOriginalId(*original);
            original_wrapper->SetUniqueId(original_id);
    
            MonoPInvokes::EntityService()->AddOriginal(original_id, original_wrapper);
            return original_wrapper->GetManagedMonoObject();
        }
        else
        {
            // NOTE: this is probably an error.
            return NULL;
        }
    }

    DotNetObject InvokeCreateDCSpatialReplica(DotNetString unique_name, DotNetObject entity_id, uint32_t entity_type)
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokeCreateDCSpatialReplica");

        assert(unique_name != NULL && entity_id != NULL);
        ISpatialReplica *replica = MonoPInvokes::ControllerManager()->InstantiateRemoteEntity(
            MonoRuntime::MonoStringToWString(unique_name),
            BadumnaRuntime::Instance().CreateBadumnaId(entity_id),
            entity_type);

        UniqueId replica_id = InternalUniqueIdManager::GetReplicaId(
            *replica, 
            MonoRuntime::MonoStringToWString(unique_name));
        ReplicaWrapper *wrapper = BadumnaRuntime::Instance().CreateReplicaWrapper(replica_id, replica);
        MonoPInvokes::EntityService()->AddReplica(replica_id, wrapper);
        return wrapper->GetManagedMonoObject();
    }

    void InvokeRemoveDCSpatialReplica(DotNetString unique_name, DotNetString replica_id)
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokeRemoveDCSpatialReplica");

        assert(unique_name != NULL && replica_id != NULL);
        UniqueId id = MonoRuntime::MonoStringToWString(replica_id);

        if(MonoPInvokes::EntityService()->ContainsReplica(id))
        {
            ReplicaWrapper *wrapper = MonoPInvokes::EntityService()->GetReplica(id);
            MonoPInvokes::ControllerManager()->RemoveEntity(
                MonoRuntime::MonoStringToWString(unique_name),
                *(wrapper->GetReplica()));
            MonoPInvokes::EntityService()->RemoveReplica(id);
        }
    }

    DotNetArray InvokeDCCheckpoint(DotNetString unique_name)
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokeDCCheckpoint");

        assert(unique_name != NULL);
        OutputStream output_stream;

        String unique_name_str = MonoRuntime::MonoStringToWString(unique_name);
        if(MonoPInvokes::ControllerManager()->Contains(unique_name_str))
        {
            MonoPInvokes::ControllerManager()->Checkpoint(unique_name_str, &output_stream);
        }

        return MonoRuntime::OutputStreamToMonoArray(&output_stream);
    }

    void InvokeDCRecover(DotNetString unique_name, DotNetArray data)
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokeDCRecover");

        assert(unique_name != NULL && data != NULL);
        std::pair<char *, size_t> array_data = MonoRuntime::GetArrayData(data);
        if(array_data.second > 0)
        {
            scoped_array<char> buffer(array_data.first);
            InputStream input_stream(buffer.get(), array_data.second);

            String unique_name_str = MonoRuntime::MonoStringToWString(unique_name);
            if(MonoPInvokes::ControllerManager()->Contains(unique_name_str))
            {
                MonoPInvokes::ControllerManager()->Recover(unique_name_str, &input_stream);
            }
        }
    }

    void InvokeDCRecover2(DotNetString unique_name)
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokeDCRecover2");

        assert(unique_name != NULL);
        String unique_name_str = MonoRuntime::MonoStringToWString(unique_name);
        if(MonoPInvokes::ControllerManager()->Contains(unique_name_str))
        {
            MonoPInvokes::ControllerManager()->Recover(unique_name_str);
        }
    }

    void InvokeDCProcess(DotNetString unique_name, int duration_in_ms)
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokeDCProcess");

        assert(unique_name != NULL);
        String unique_name_str = MonoRuntime::MonoStringToWString(unique_name);
        if(MonoPInvokes::ControllerManager()->Contains(unique_name_str))
        {
            MonoPInvokes::ControllerManager()->Process(unique_name_str, duration_in_ms);
        }
    }

    void InvokeDCWake(DotNetString unique_name)
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokeDCWake");

        assert(unique_name != NULL);
        String unique_name_str = MonoRuntime::MonoStringToWString(unique_name);
        if(MonoPInvokes::ControllerManager()->Contains(unique_name_str))
        {
            MonoPInvokes::ControllerManager()->Wake(unique_name_str);
        }
    }
    
    void InvokeDCSleep(DotNetString unique_name, DotNetString original_id)
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokeDCSleep");

        assert(unique_name != NULL && original_id != NULL);
        UniqueId id = MonoRuntime::MonoStringToWString(original_id);

        String unique_name_str = MonoRuntime::MonoStringToWString(unique_name);
        if(MonoPInvokes::ControllerManager()->Contains(unique_name_str))
        {
            MonoPInvokes::ControllerManager()->Sleep(unique_name_str);
        }

        if(MonoPInvokes::EntityService()->ContainsOriginal(id))
        {
            MonoPInvokes::EntityService()->RemoveOriginal(id);
        }
    }

    void InvokeDCShutdown(DotNetString unique_name)
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokeDCShutdown");

        assert(unique_name != NULL);
        String unique_name_str = MonoRuntime::MonoStringToWString(unique_name);
        if(MonoPInvokes::ControllerManager()->Contains(unique_name_str))
        {
            MonoPInvokes::ControllerManager()->Shutdown(unique_name_str);
        }
    }

    int InvokeIsRequiredControllerNameRegistered(DotNetString name)
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokeIsRequiredControllerNameRegistered");

        assert(name != NULL);
        bool ret = MonoPInvokes::ControllerManager()->IsTypeRegistered(MonoRuntime::MonoStringToWString(name));
        if(ret)
        {
            return 1;
        }

        return 0;
    }

    // streaming
    void InvokeStreamSendComplete(DotNetString tag, DotNetString name, DotNetObject destination)
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokeStreamSendComplete");

        assert(tag != NULL && name != NULL && destination != NULL);
        String stream_tag = MonoRuntime::MonoStringToWString(tag);
        String stream_name = MonoRuntime::MonoStringToWString(name);
        BadumnaId destination_id = BadumnaRuntime::Instance().CreateBadumnaId(destination);
        UniqueId id = InternalUniqueIdManager::GetStreamingCompleteDelegateId(stream_tag, stream_name, destination_id);
        MonoPInvokes::StreamingManager()->InvokeSendCompleteDelegate(id, stream_tag, stream_name);
    }

    void InvokeStreamRequestHandle(DotNetObject args)
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokeStreamRequestHandle");

        assert(args != NULL);
        std::auto_ptr<StreamRequest> request(
            new StreamRequest(
                BadumnaRuntime::Instance().CreateStreamRequestImpl(args, MonoPInvokes::StreamingManager())
            )
        );
        UniqueId id = InternalUniqueIdManager::GetStreamingRequestDelegateId(request->GetStreamTag());
        MonoPInvokes::StreamingManager()->InvokeRequestDelegate(id, request.get());
    }

    void InvokeStreamReceiveComplete(DotNetString tag, DotNetString name, DotNetString username)
    {
        BADUMNA_LOG_INFO("MonoPInvokes::InvokeStreamReceiveComplete");

        assert(tag != NULL && name != NULL && username != NULL);
        String stream_tag = MonoRuntime::MonoStringToWString(tag);
        String stream_name = MonoRuntime::MonoStringToWString(name);
        String user_name = MonoRuntime::MonoStringToWString(username);
        UniqueId id = InternalUniqueIdManager::GetStreamingReceiveCompleteDelegateId(
            stream_tag, 
            stream_name, 
            user_name);
        MonoPInvokes::StreamingManager()->InvokeReceiveCompleteDelegate(id, stream_tag, stream_name);
    }
}

#endif // DOXYGEN_SHOULD_SKIP_THIS
