//---------------------------------------------------------------------------------
// <copyright file="InternalErrorHandler.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_INTERNAL_ERROR_HANDLER_H
#define BADUMNA_INTERNAL_ERROR_HANDLER_H

#include <cstdlib>
#include <iostream>
#include "Badumna/Core/InternalErrorCallback.h"
#include "Badumna/Utils/Singleton.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class InternalErrorHandler : public Singleton<InternalErrorHandler>
    {
        friend class Singleton<InternalErrorHandler>;
    public:
        void RegisterCallback(InternalErrorCallback callback)
        {
            callback_ = callback;
            callback_registered_ = true;
        }

        void Invoke(int error_code, String const &message)
        {
            if(callback_registered_)
            {
                callback_(error_code, message);
            }
            else
            {
                std::cerr << "An internal error has occured." << std::endl;
                std::cerr << "Error code : " << error_code << std::endl;
                std::cerr << "Message : " << message.UTF8CStr() << std::endl;

                ::abort();
            }
        }

    private:
        InternalErrorHandler()
            : callback_registered_(false),
            callback_(NULL)
        {
        }

        bool callback_registered_;
        InternalErrorCallback callback_;

        DISALLOW_COPY_AND_ASSIGN(InternalErrorHandler);
    };
}

#endif // BADUMNA_INTERNAL_ERROR_HANDLER_H