//---------------------------------------------------------------------------------
// <copyright file="AssemblyImageManager.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_ASSEMBLY_IMAGE_MANAGER_H
#define BADUMNA_ASSEMBLY_IMAGE_MANAGER_H

#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    /**
     * The AssemblyImageManager manages the Badumna and Dei images loaded by Mono. The images are used by the 
     * MonoRuntime class. 
     */ 
    class AssemblyImageManager
    {
    public:
        AssemblyImageManager();
    
        bool Initialize(String const &path);
        void Shutdown();

        DotNetImage GetImage(String const &name_space) const;

    private:
        bool LoadDeiAssembly() const;

        DotNetImage badumna_image_;
        MonoAssembly *badumna_assembly_;
        mutable DotNetImage dei_image_;
        mutable MonoAssembly *dei_assembly_;
        
        DISALLOW_COPY_AND_ASSIGN(AssemblyImageManager);
    };
}

#endif // BADUMNA_ASSEMBLY_IMAGE_MANAGER_H