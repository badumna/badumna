//---------------------------------------------------------------------------------
// <copyright file="MonoRuntime.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_MONO_RUNTIME_H
#define BADUMNA_MONO_RUNTIME_H

#include <memory>
#include <utility>

#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Core/Arguments.h"
#include "Badumna/Core/MonoRuntimeConfig.h"
#include "Badumna/Core/ExceptionDetails.h"
#include "Badumna/Core/AssemblyImageManager.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/Utils/Singleton.h"
#include "Badumna/Utils/Assembly.h"
#include "Badumna/Utils/ScopedPtr.h"

namespace Badumna
{
#ifndef DOXYGEN_SHOULD_SKIP_THIS
    typedef uint32_t MonoGCHandle;
    typedef char const * NameType;
#endif // DOXYGEN_SHOULD_SKIP_THIS

    class OutputStream;

    /**
     * The Mono Runtime class encapsaltes the objects (e.g. domain, images) required to interact with the Mono runtime 
     * system. It allows specified managed objects to be created and constructed. It also allows you to invoke methods
     * and properties, there are also a group of helper methods available to do type conversion (e.g. from C++ string 
     * to Mono Managed String), reference counting (GetObjectFromGCHandle/FreeObjectGCHandle) and etc. 
     *
     * The Mono Runtime object is essential when any C++ wrapper object needs to interact with the managed side of the 
     * world. It is thus injected into all ManagedObject objects created by the BadumnaRuntime singleton. 
     */
    class MonoRuntime
    {
    public:
        MonoRuntime();
        ~MonoRuntime();

        /**
         * Initialize the Mono runtime using the specified configuration. 
         */
        bool Initialize(MonoRuntimeConfig *config);

        /**
         * Shutdown the runtime.
         */ 
        void Shutdown();

        DotNetClass GetClass(NameType name_space, NameType name) const;
        DotNetClass GetClass2(NameType name_space, NameType name) const;

        DotNetObject CreateObject(DotNetClass klass, Arguments const &args, ExceptionDetails *exc_details) const;
        DotNetObject CreateObject(DotNetClass klass, ExceptionDetails *exc_details) const ;
        DotNetObject CloneObject(DotNetObject object) const;

        DotNetObject InvokeMethod(
            DotNetClass klass, 
            DotNetObject object, 
            NameType name, 
            Arguments const &args, 
            ExceptionDetails *exc_details) const;

        DotNetObject InvokeStaticMethod(
            NameType name_space, 
            NameType class_name, 
            NameType name, 
            Arguments const &args, 
            ExceptionDetails *exc_details) const;

        template<typename T>
        T InvokeStaticMethod(
            NameType name_space, 
            NameType class_name, 
            NameType name, 
            Arguments const &args, 
            ExceptionDetails *exc_details) const
        {
            DotNetClass k = GetClass(name_space, class_name);
            DotNetObject object = InvokeMethod(k, NULL, name, args, exc_details);
            return UnboxingObject<T>(object);
        }

        DotNetObject InvokeMethodFullName(
            DotNetClass klass, 
            DotNetObject object, 
            NameType name, 
            bool include_namespace, 
            Arguments const &args,
            ExceptionDetails *exc_details) const;

        DotNetObject InvokeVirtualMethod(
            DotNetClass klass, 
            DotNetObject object, 
            NameType name, 
            Arguments const &args, 
            ExceptionDetails *exc_details) const;

        DotNetObject InvokeVirtualMethodFullName(
            DotNetClass klass, 
            DotNetObject object, 
            NameType name, 
            bool include_namespace, 
            Arguments const &args, 
            ExceptionDetails *exc_details) const;

        DotNetObject GetStubObject(
            NameType name_space, 
            NameType name, 
            String const &unique_id,
            DotNetObject facade_object) const;

        DotNetObject GetStubObject(
            NameType name_space, 
            NameType name, 
            DotNetObject facade_object) const;

        DotNetObject GetPropertyFromName(
            NameType name_space, 
            NameType class_name, 
            DotNetObject object, 
            NameType name, 
            Arguments const &args, 
            ExceptionDetails *exc_details) const; 

        DotNetObject GetPropertyFromName(
            DotNetClass klass, 
            DotNetObject object, 
            NameType name, 
            Arguments const &args, 
            ExceptionDetails *exc_details) const;

        DotNetObject GetVirtualPropertyFromName(
            DotNetClass klass, 
            DotNetObject object, 
            NameType name, 
            Arguments const &args, 
            ExceptionDetails *exc_details) const;

        template<typename T>
        T GetPropertyFromName(
            NameType name_space, 
            NameType class_name, 
            DotNetObject object, 
            NameType name, 
            Arguments const &args, 
            ExceptionDetails *exc_details) const
        {
            DotNetObject ret = GetPropertyFromName(name_space, class_name, object, name, args, exc_details);
            return MonoRuntime::UnboxingObject<T>(ret);
        }
        
        void SetPropertyValue(
            DotNetClass klass, 
            DotNetObject object, 
            NameType name, 
            Arguments const &args, 
            ExceptionDetails *exc_details) const;

        DotNetObject InvokeGetPropertyFromName(
            NameType name_space, 
            NameType class_name, 
            NameType name, 
            ExceptionDetails *exc_details) const;

        DotNetObject GetStaticFieldValue(
            NameType name_space, 
            NameType class_name, 
            NameType name) const;

        bool MethodAvailable(
            NameType name_space, 
            NameType class_name, 
            NameType name,
            int count) const;

        bool PropertyAvailable(
            NameType name_space, 
            NameType class_name, 
            NameType name) const;

        DotNetThread ThreadAttach();
        void ThreadDetach(DotNetThread thread);

        template<typename T>
        static T UnboxingObject(DotNetObject object) 
        {
            return *(reinterpret_cast<T*>(mono_object_unbox(object)));
        }

        static DotNetArray OutputStreamToMonoArray(OutputStream *stream);
        static DotNetString GetString(char const *text);
        static DotNetString GetString(String const &str);
        static DotNetClass GetClass(DotNetObject object);
        static String GetObjectTypeNameFromObject(DotNetObject object);
        static DotNetObject GetObjectFromGCHandle(MonoGCHandle handle);
        static MonoGCHandle GetObjectGCHandle(DotNetObject object);
        static void FreeObjectGCHandle(MonoGCHandle handle);
        static std::pair<char *, size_t> GetArrayData(DotNetArray data);
        static DotNetArray GetMonoByteArray(char const *data, size_t length);
        static String MonoStringToString(DotNetString str);
        static String MonoStringToWString(DotNetString str);
        static DotNetObject GetManagedFloatValue(float value);

    private:
        void HandleException(NameType method_name, DotNetObject exception, ExceptionDetails *exc_details) const;

        DotNetDomain domain_;
        AssemblyImageManager image_manager_;
        std::auto_ptr<MonoRuntimeConfig> runtime_config_;

        DISALLOW_COPY_AND_ASSIGN(MonoRuntime);
    };

#ifndef DOXYGEN_SHOULD_SKIP_THIS
    // explicit specialization of the template member method 
    template<>
    DotNetString MonoRuntime::UnboxingObject<DotNetString>(DotNetObject object);
    
    template<>
    String MonoRuntime::UnboxingObject<String>(DotNetObject object);

    String MonoStringToWStringImpl(DotNetString str);
#endif // DOXYGEN_SHOULD_SKIP_THIS
}

#endif // BADUMNA_MONO_RUNTIME_H