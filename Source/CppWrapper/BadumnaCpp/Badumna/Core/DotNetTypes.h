//---------------------------------------------------------------------------------
// <copyright file="DotNetTypes.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_DOT_NET_TYPES_H
#define BADUMNA_DOT_NET_TYPES_H

#include <mono/jit/jit.h>

// These types defined and exported by Mono are used as Ids. That is we never
// use these pointer to access their members, we never free/delete them. In the
// wrapper, they are only used to identify internal object instances managed by 
// Mono. 
typedef MonoImage* DotNetImage;
typedef MonoDomain* DotNetDomain;

typedef MonoObject* DotNetObject;
typedef MonoClass* DotNetClass;
typedef MonoArray* DotNetArray;
typedef MonoString* DotNetString;
typedef MonoMethod* DotNetMethod;
typedef MonoProperty* DotNetProperty;

typedef MonoThread* DotNetThread;

#endif // BADUMNA_DOT_NET_TYPES_H