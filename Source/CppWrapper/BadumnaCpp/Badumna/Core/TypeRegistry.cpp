//---------------------------------------------------------------------------------
// <copyright file="TypeRegistry.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <vector>
#include "Badumna/Core/TypeRegistry.h"

namespace Badumna
{
    TypeRegistry::TypeRegistry()
        : class_details(),
        method_details(),
        property_details()
    {
    }

    void TypeRegistry::RegisterType(String const &name_space, String const &type_name)
    {
        class_details.push_back(ClassDetails(name_space, type_name));
    }

    std::vector<ClassDetails> TypeRegistry::GetClassDetails() const
    {
        return class_details;
    }

    void TypeRegistry::RegisterMethod(
        String const &name_space, 
        String const &type_name, 
        String const &method_name, 
        int count)
    {
        method_details.push_back(MethodDetails(name_space, type_name, method_name, count));
    }
    
    std::vector<MethodDetails> TypeRegistry::GetMethodDetails() const
    {
        return method_details;
    }

    void TypeRegistry::RegisterProperty(String const &name_space, String const &type_name, String const &pname)
    {
        property_details.push_back(PropertyDetails(name_space, type_name, pname));
    }

    std::vector<PropertyDetails> TypeRegistry::GetPropertyDetails() const
    {
        return property_details;
    }
}