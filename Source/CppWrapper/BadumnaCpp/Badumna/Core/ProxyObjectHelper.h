//---------------------------------------------------------------------------------
// <copyright file="ProxyObjectHelper.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_PROXY_OBJECT_HELPER_H
#define BADUMNA_PROXY_OBJECT_HELPER_H

#define DECLARE_COPY_AND_ASSIGN(TypeName)                               \
  TypeName(TypeName const &);                                           \
  TypeName& operator=(TypeName const &)

#define DEFINE_WRAPPER_CLASS_IMPL_COPYCTOR_ASSIGN_OPERATOR(TypeName)    \
    TypeName::TypeName(TypeName const &other)                           \
        : ProxyObject(other.managed_object_)                            \
    {                                                                   \
    }                                                                   \
                                                                        \
    TypeName& TypeName::operator=(TypeName const &rhs)                  \
    {                                                                   \
        if(this != &rhs)                                                \
        {                                                               \
            managed_object_ = rhs.managed_object_;                      \
        }                                                               \
                                                                        \
        return *this;                                                   \
    }                                                                   

#define DEFINE_WRAPPER_CLASS_COPYCTOR_ASSIGN_OPERATOR(TypeName, ImplTypeName, ImplName, VarName)            \
    TypeName::TypeName(TypeName const &VarName)                                                             \
    : ImplName(new ImplTypeName(*(VarName.ImplName)))                                                       \
    {                                                                                                       \
    }                                                                                                       \
                                                                                                            \
    TypeName& TypeName::operator=(TypeName const &VarName)                                                  \
    {                                                                                                       \
        if(this != &VarName)                                                                                \
        {                                                                                                   \
            *(ImplName) = *(VarName.ImplName);                                                              \
        }                                                                                                   \
                                                                                                            \
        return *this;                                                                                       \
    }

#endif // BADUMNA_PROXY_OBJECT_HELPER_H