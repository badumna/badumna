//---------------------------------------------------------------------------------
// <copyright file="RuntimeInitializer.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <iostream>
#include "Badumna/Core/RuntimeInitializer.h"
#include "Badumna/Core/MonoRuntimeConfig.h"
#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/Core/MonoPInvokes.h"
#include "Badumna/Core/TypeRegistry.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/FileIO.h"


namespace Badumna
{
    bool CheckMonoDistribution(MonoRuntimeConfig const &runtime_config)
    {
        String mono_lib_path = runtime_config.GetLibPath();
        String mono_etc_path = runtime_config.GetEtcPath();
        String mono_config_file_path = runtime_config.GetConfigPath();

        // check whether the etc and lib subdirectories are there
        if(!DirectoryExists(mono_lib_path))
        {
            std::cerr << "Mono lib path not found" << std::endl;
            return false;
        }
        if(!DirectoryExists(mono_etc_path))
        {
            std::cerr << "Mono etc path not found" << std::endl;
            return false;
        }

        // check whether the etc/config file is there
        if(!FileExists(mono_config_file_path))
        {
            std::cerr << "Mono config file path not found" << std::endl;
            return false;
        }

#ifdef WIN32
        String separator = "\\";
#else
        String separator = "/";
#endif

        // check whether the 2.0 runtime library is there 
        String mscorlib_file_path = 
            mono_lib_path + separator + "mono" + separator + "2.0" + separator + "mscorlib.dll";
        if(!FileExists(mscorlib_file_path))
        {
            std::cerr << "mscorlib file path not found: " << mscorlib_file_path.UTF8CStr() << std::endl;
            return false;
        }

        return true;
    }
    
    bool DoInitializeRuntime(MonoRuntimeConfig *config)
    {
        if(!CheckMonoDistribution(*config))
        {
            std::cerr << "Mono runtime not installed correctly" << std::endl;
            return false;
        }

        bool success = BadumnaRuntime::Instance().Initialize(config);
        if(!success)
        {
            std::cerr << "Failed to initialize the runtime (maybe Badumna.dll or Dei.dll is missing?)" << std::endl;
            return false;
        }

#ifndef NDEBUG
        success = BadumnaRuntime::Instance().Verify(TypeRegistry::Instance());
        if(!success)
        {
            std::cerr << "Managed type check failed." << std::endl;
            return false;
        }
#endif // NDEBUG

        MonoPInvokes::RegisterExternalCalls();
        return success;
    }

    void CheckInitializeRuntimeResult(bool succeed, MonoRuntimeConfig *config)
    {
        if(!succeed)
        {
            if(config->ExceptionCallbackRegistered())
            {
                config->GetExceptionCallback()("InitializeRuntime", "", "Failed to initialize the runtime.");
            }
            else
            {
                std::cerr << "Failed to initialize the runtime. " << std::endl;
                std::cerr << "Going to call abort()." << std::endl;
                ::abort();
            }
        }
    }

    void InitializeRuntime()
    {
        MonoRuntimeConfig *config = new MonoRuntimeConfig();
        CheckInitializeRuntimeResult(DoInitializeRuntime(config), config);
    }
    
    void InitializeRuntime(
        BadumnaExceptionCallback exception_callback, 
        InternalErrorCallback internal_error_callback)
    {
        MonoRuntimeConfig *config = new MonoRuntimeConfig();
        config->SetExceptionCallback(exception_callback);
        InternalErrorHandler::Instance().RegisterCallback(internal_error_callback);

        CheckInitializeRuntimeResult(DoInitializeRuntime(config), config);
    }

    void InitializeRuntime(String const &mono_path, String const &assembly_path)
    {
        MonoRuntimeConfig *config = new MonoRuntimeConfig();
        config->SetMonoPath(mono_path);
        config->SetAssemblyPath(assembly_path);

        CheckInitializeRuntimeResult(DoInitializeRuntime(config), config);
    }

    void InitializeRuntime(
        BadumnaExceptionCallback exception_callback, 
        InternalErrorCallback internal_error_callback, 
        String const &mono_path, 
        String const &assembly_path)
    {
        MonoRuntimeConfig *config = new MonoRuntimeConfig();
        config->SetMonoPath(mono_path);
        config->SetAssemblyPath(assembly_path);
        config->SetExceptionCallback(exception_callback);
        InternalErrorHandler::Instance().RegisterCallback(internal_error_callback);

        CheckInitializeRuntimeResult(DoInitializeRuntime(config), config);
    }

    void InitializeRuntimeFromConfigFile(String const &config_file_path, String const &assembly_path)
    {
        MonoRuntimeConfig *config = new MonoRuntimeConfig();
        config->ConfigFromFile(config_file_path);
        config->SetAssemblyPath(assembly_path);

        CheckInitializeRuntimeResult(DoInitializeRuntime(config), config);
    }

    void InitializeRuntimeFromConfigFile(
        BadumnaExceptionCallback exception_callback,
        InternalErrorCallback internal_error_callback,
        String const &config_file_path, 
        String const &assembly_path)
    {
        MonoRuntimeConfig *config = new MonoRuntimeConfig();
        config->ConfigFromFile(config_file_path);
        config->SetAssemblyPath(assembly_path);
        config->SetExceptionCallback(exception_callback);
        InternalErrorHandler::Instance().RegisterCallback(internal_error_callback);

        CheckInitializeRuntimeResult(DoInitializeRuntime(config), config);
    }

    void ShutdownRuntime()
    {
        BadumnaRuntime::Instance().Shutdown();
    }

    /**************************************************************************
    * BadumnaRuntimeInitializer RAII class
    **************************************************************************/
    BadumnaRuntimeInitializer::BadumnaRuntimeInitializer()
    {
        InitializeRuntime();
    }

    BadumnaRuntimeInitializer::BadumnaRuntimeInitializer(
        BadumnaExceptionCallback exception_callback, 
        InternalErrorCallback internal_error_callback)
    {
        InitializeRuntime(exception_callback, internal_error_callback);
    }

    BadumnaRuntimeInitializer::BadumnaRuntimeInitializer(
        String const &mono_path, 
        String const &assembly_path)
    {
        InitializeRuntime(mono_path, assembly_path);
    }

    
    BadumnaRuntimeInitializer::BadumnaRuntimeInitializer(
        BadumnaExceptionCallback exception_callback,
        InternalErrorCallback internal_error_callback,
        String const &mono_path, 
        String const &assembly_path)
    {
        InitializeRuntime(exception_callback, internal_error_callback, mono_path, assembly_path);
    }

    BadumnaRuntimeInitializer::~BadumnaRuntimeInitializer()
    {
        ShutdownRuntime();
    }

    /**************************************************************************
    * BadumnaRuntimeInitializer2 RAII class
    **************************************************************************/
    BadumnaRuntimeInitializer2::BadumnaRuntimeInitializer2(
        String const &config_file_path, 
        String const &assembly_path)
    {
        InitializeRuntimeFromConfigFile(config_file_path, assembly_path);
    }

    BadumnaRuntimeInitializer2::BadumnaRuntimeInitializer2(
        BadumnaExceptionCallback exception_callback,
        InternalErrorCallback internal_error_callback,
        String const &config_file_path, 
        String const &assembly_path)
    {
        InitializeRuntimeFromConfigFile(exception_callback, internal_error_callback, config_file_path, assembly_path);
    }

    BadumnaRuntimeInitializer2::~BadumnaRuntimeInitializer2()
    {
        ShutdownRuntime();
    }
}
