//---------------------------------------------------------------------------------
// <copyright file="NetworkFacadeImpl.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include <cstdlib>
#include <memory>

#include "Badumna/Chat/ChatSession.h"
#include "Badumna/Core/TypeRegistry.h"
#include "Badumna/Core/Arguments.h"
#include "Badumna/Core/InternalUniqueId.h"
#include "Badumna/Core/NetworkFacadeImpl.h"
#include "Badumna/Core/ManagedObjectGuard.h"
#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/Configuration/OptionsImpl.h"
#include "Badumna/Dei/IdentityProviderImpl.h"
#include "Badumna/DataTypes/BooleanArrayImpl.h"
#include "Badumna/Arbitration/ArbitrationClient.h"
#include "Badumna/Utils/Logger.h"

namespace Badumna
{
    using Dei::IdentityProviderImpl;
    using Dei::IdentityProvider;

    REGISTER_MANAGED_MONO_TYPE(CppWrapperStub, NetworkFacadeStub);
    NetworkFacadeImpl::NetworkFacadeImpl(MonoRuntime *runtime, BadumnaStack *stack, Options const *options, String const *identifier)
        : ProxyObject(runtime),
        badumna_stack_(stack),
        arbitration_server_impl_(NULL),
        chat_session_(NULL)
    {
        assert(runtime != NULL && stack != NULL && (options != NULL || identifier != NULL));

        Arguments args;
        if (options != NULL)
        {
            args.AddArgument(ManagedObjectGuard::GetManagedObject(*options));
            args.AddNullArgument();
        }
        else
        {
            args.AddNullArgument();
            args.AddArgument(MonoRuntime::GetString(*identifier));
        }

        DotNetClass klass = runtime->GetClass("Badumna.CppWrapperStub", "NetworkFacadeStub");
        DotNetObject facade_object = runtime->CreateObject(klass, args, NULL);
        if (facade_object == NULL)
        {
            BADUMNA_LOG_DEBUG("failed to get the facade object.");
            abort();
        }
    
        managed_object_.Initialize(facade_object, "Badumna.CppWrapperStub", "NetworkFacadeStub");
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, NetworkFacadeStub, AnnounceService, 1)
    void NetworkFacadeImpl::AnnounceService(int type) const
    {
        BADUMNA_LOG_DEBUG("NetworkFacadeImpl::AnnounceService");

        Arguments args;
        args.AddArgument(&type);
        managed_object_.InvokeVirtualMethod("AnnounceService", args);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, NetworkFacadeStub, Login, 0)
    bool NetworkFacadeImpl::Login()
    {
        BADUMNA_LOG_DEBUG("NetworkFacadeImpl::Login");

        return managed_object_.InvokeMethod<bool>("Login");
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, NetworkFacadeStub, LoginString, 1)
    bool NetworkFacadeImpl::Login(String const &character_name)
    {
        BADUMNA_LOG_DEBUG("NetworkFacadeImpl::LoginString");
        Arguments args;
        args.AddArgument(mono_runtime_->GetString(character_name));

        return managed_object_.InvokeMethod<bool>("LoginString", args);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, NetworkFacadeStub, LoginIdentityProvider, 1)
    bool NetworkFacadeImpl::Login(IdentityProvider const &identity_provider)
    {
        BADUMNA_LOG_INFO("NetworkFacadeImpl::LoginIdentityProvider");

        assert(identity_provider.impl_.get() != NULL);

        Arguments args;
        args.AddArgument(ManagedObjectGuard::GetManagedObject(identity_provider));

        return managed_object_.InvokeMethod<bool>("LoginIdentityProvider", args);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, NetworkFacadeStub, ProcessNetworkState, 0)
    void NetworkFacadeImpl::ProcessNetworkState()
    {
        managed_object_.InvokeMethod("ProcessNetworkState");
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, NetworkFacadeStub, Shutdown, 1)
    void NetworkFacadeImpl::Shutdown()
    {
        BADUMNA_LOG_INFO("NetworkFacadeImpl::Shutdown");

        // always call Shutdown(true)
        bool true_flag = true;
        Arguments args;
        args.AddArgument(&true_flag);
        managed_object_.InvokeVirtualMethod("Shutdown", args);
        chat_session_.reset(NULL);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, NetworkFacadeStub, GetNetworkStatus, 0)
    NetworkStatus NetworkFacadeImpl::GetNetworkStatus() const
    {
        BADUMNA_LOG_INFO("NetworkFacadeImpl::GetNetworkStatus");

        DotNetObject object = managed_object_.InvokeVirtualMethod("GetNetworkStatus");
        return NetworkStatus(BadumnaRuntime::Instance().CreateNetworkStatusImpl(object));
    }

    NetworkScene *NetworkFacadeImpl::JoinScene(
        String const &scene_name, 
        CreateSpatialReplicaDelegate create_delegate, 
        RemoveSpatialReplicaDelegate remove_delegate)
    {
        BADUMNA_LOG_INFO("NetworkFacadeImpl::JoinScene, scene name: " << scene_name.UTF8CStr());

        // unique id for the scene.
        UniqueId delegate_id = InternalUniqueIdManager::GetSceneId(scene_name, create_delegate, remove_delegate);
        // register delegates.
        ReplicationService()->AddReplicaDelegate(delegate_id, create_delegate, remove_delegate);

        // create the stub object
        DotNetObject stub;
        stub = mono_runtime_->GetStubObject(
            "Badumna.CppWrapperStub", 
            "SpatialEntityDelegates", 
            delegate_id, 
            managed_object_.InvokeMethod("GetFacade"));
        
        // associate the returned stub object with the network scene wrapper immediately, or it may be GCed. 
        NetworkSceneImpl *scene_impl = BadumnaRuntime::Instance().CreateNetworkSceneImpl(
            stub, 
            delegate_id, 
            EntityService(), 
            ReplicationService(), 
            this);
        NetworkScene *scene = new NetworkScene(scene_impl);

        Arguments args;
        args.AddArgument(MonoRuntime::GetString(scene_name));
        DotNetClass stub_class = mono_object_get_class(stub);
        mono_runtime_->InvokeMethod(stub_class, stub, "JoinScene", args, NULL);

        return scene;
    }

    NetworkScene *NetworkFacadeImpl::JoinMiniScene(
        String const &scene_name, 
        CreateSpatialReplicaDelegate create_delegate, 
        RemoveSpatialReplicaDelegate remove_delegate)
    {
        BADUMNA_LOG_INFO("NetworkFacadeImpl::JoinMiniScene, scene name: " << scene_name.UTF8CStr());

        // unique id for the scene.
        UniqueId delegate_id = InternalUniqueIdManager::GetSceneId(scene_name, create_delegate, remove_delegate);
        // register delegates.
        ReplicationService()->AddReplicaDelegate(delegate_id, create_delegate, remove_delegate);

        // create the stub object
        DotNetObject stub;
        stub = mono_runtime_->GetStubObject(
            "Badumna.CppWrapperStub", 
            "SpatialEntityDelegates", 
            delegate_id, 
            managed_object_.InvokeMethod("GetFacade"));
        
        // associate the returned stub object with the network scene wrapper immediately, or it may be GCed. 
        NetworkSceneImpl *scene_impl = BadumnaRuntime::Instance().CreateNetworkSceneImpl(
            stub, 
            delegate_id, 
            EntityService(), 
            ReplicationService(), 
            this);
        NetworkScene *scene = new NetworkScene(scene_impl);

        Arguments args;
        args.AddArgument(MonoRuntime::GetString(scene_name));
        DotNetClass stub_class = mono_object_get_class(stub);
        mono_runtime_->InvokeMethod(stub_class, stub, "JoinMiniScene", args, NULL);

        return scene;
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, NetworkFacadeStub, FlagForUpdate, 2)
    void NetworkFacadeImpl::FlagForUpdate(ISpatialOriginal const &local_entity, BooleanArray const &changed_parts) const
    {
        BADUMNA_LOG_INFO("NetworkFacadeImpl::FlagForUpdate, " << changed_parts.ToString().UTF8CStr());

        UniqueId original_id = InternalUniqueIdManager::GetOriginalId(local_entity);
        if(!EntityService()->ContainsOriginal(original_id))
        {
            return;
        }
        
        OriginalWrapper *original = EntityService()->GetOriginal(original_id);
        DotNetObject original_stub = original->GetManagedMonoObject();
        // synchronize ISpatialEntity property when necessary
        original->SynchronizeOriginalProperty(changed_parts);
        
        Arguments args;
        args.AddArgument(original_stub);
        args.AddArgument(ManagedObjectGuard::GetManagedObject(changed_parts));

        managed_object_.InvokeVirtualMethodFullName(
            "Badumna.CppWrapperStub.NetworkFacadeStub:FlagForUpdate(Badumna.SpatialEntities.ISpatialOriginal,Badumna.Utilities.BooleanArray)",
            true, args);
    }

    void NetworkFacadeImpl::FlagForUpdate(ISpatialOriginal const &local_entity, int changed_part_index) const
    {
        BADUMNA_LOG_INFO("NetworkFacadeImpl::FlagForUpdate2, " << changed_part_index);
        
        BooleanArray changed_parts;
        changed_parts.Set(changed_part_index, true);
        FlagForUpdate(local_entity, changed_parts);
    }

    void NetworkFacadeImpl::SendCustomMessageToRemoteCopies(
        ISpatialOriginal const &local_entity, 
        OutputStream *event_data) const
    {
        BADUMNA_LOG_INFO("NetworkFacadeImpl::SendCustomMessageToRemoteCopies");

        UniqueId original_id = InternalUniqueIdManager::GetOriginalId(local_entity);
        if(!EntityService()->ContainsOriginal(original_id))
        {
            return;
        }
        
        OriginalWrapper *original = EntityService()->GetOriginal(original_id);
        original->SendCustomMessageToRemoteCopies(event_data);
    }
    
    void NetworkFacadeImpl::SendCustomMessageToOriginal(
        ISpatialReplica const &remote_entity, 
        OutputStream *event_data) const
    {
        BADUMNA_LOG_INFO("NetworkFacadeImpl::SendCustomMessageToOriginal");

        assert(event_data != NULL);
        if(!EntityService()->ContainsReplica(remote_entity))
        {
            return;
        }
        
        ReplicaWrapper *replica = EntityService()->GetReplica(remote_entity);
        replica->SendCustomMessageToOriginal(event_data);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, NetworkFacadeStub, RegisterEntityDetailsStub, 2)
    void NetworkFacadeImpl::RegisterEntityDetails(
        float area_of_interest_radius, 
        float max_speed)
    {
        BADUMNA_LOG_INFO("NetworkFacadeImpl::RegisterEntityDetails");

        Arguments args;
        args.AddArgument(&area_of_interest_radius);
        args.AddArgument(&max_speed);

        managed_object_.InvokeMethod("RegisterEntityDetailsStub", args);
    }

    IChatSession *NetworkFacadeImpl::ChatSession()
    {
        if(chat_session_.get() == NULL)
        {
            BADUMNA_LOG_INFO("NetworkFacadeImpl::ChatSession creating new instance");

            UniqueId unique_id = InternalUniqueIdManager::GetChatSessionId();
            DotNetClass stub_class = mono_runtime_->GetClass("Badumna.CppWrapperStub", "ChatSessionStub");
            
            Arguments args;
            args.AddArgument(mono_runtime_->GetString(unique_id));
            args.AddArgument(managed_object_.InvokeMethod("GetFacade"));
            DotNetObject stub = mono_runtime_->CreateObject(stub_class, args, NULL);

            ChatSessionImpl * session_impl = BadumnaRuntime::Instance().CreateChatSessionImpl(badumna_stack_->ChatManager(), unique_id, stub);
            
            IChatSession *chat_session = new ::Badumna::ChatSession(session_impl);
            chat_session_.reset(chat_session);
        }
        return chat_session_.get();
    }

    StatisticsTracker *NetworkFacadeImpl::CreateTracker(
            String const &server_address, 
            int port,
            int interval_seconds, 
            String const &initial_payload) const
    {
        BADUMNA_LOG_INFO("NetworkFacadeImpl::CreateTracker");
        
        Arguments args;
        args.AddArgument(MonoRuntime::GetString(server_address));
        args.AddArgument(&port);
        args.AddArgument(&interval_seconds);
        args.AddArgument(MonoRuntime::GetString(initial_payload));

        DotNetObject managed_tracker_object = managed_object_.InvokeMethod("CreateTrackerStub", args);

        return new StatisticsTracker(BadumnaRuntime::Instance().CreateStatisticsTrackerImpl(managed_tracker_object));
    }

    IArbitrator *NetworkFacadeImpl::GetArbitrator(String const &name) const
    {
        BADUMNA_LOG_INFO("NetworkFacadeImpl::GetArbitrator, name : " << name.UTF8CStr());
        
        DotNetObject stub = mono_runtime_->GetStubObject(
            "Badumna.CppWrapperStub", 
            "ArbitrationClientStub", 
            name, 
            managed_object_.InvokeMethod("GetFacade"));

        return new ArbitrationClient(
            BadumnaRuntime::Instance().CreateArbitrationClientImpl(name, stub, ArbitrationService()));
    }

    void NetworkFacadeImpl::RegisterArbitrationHandler(
        ArbitrationClientMessageDelegate client_message_delegate, 
        int timeout_seconds, 
        ArbitrationClientDisconnectDelegate client_disconnect_delegate)
    {
        BADUMNA_LOG_INFO("NetworkFacadeImpl::RegisterArbitrationHandler, timeout : " << timeout_seconds);

        assert(timeout_seconds > 0);
        if(arbitration_server_impl_.get() == NULL)
        {
            // at this stage, we can only have one server per badumna process. 
            String unique_id = L"only_one_server_per_process";
            DotNetObject stub = mono_runtime_->GetStubObject(
                "Badumna.CppWrapperStub", 
                "ArbitrationServerStub", 
                unique_id,
                managed_object_.InvokeMethod("GetFacade"));

            arbitration_server_impl_.reset(
                BadumnaRuntime::Instance().CreateArbitrationServerImpl(unique_id, stub, ArbitrationService()));
        }

        arbitration_server_impl_->Register(client_message_delegate, client_disconnect_delegate, timeout_seconds);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, NetworkFacadeStub, SendServerArbitrationEvent, 2)
    void NetworkFacadeImpl::SendServerArbitrationEvent(int32_t destination_session_id, OutputStream *stream) const
    {
        BADUMNA_LOG_INFO("NetworkFacadeImpl::SendServerArbitrationEvent");

        assert(stream != NULL);

        Arguments args;
        DotNetArray data = MonoRuntime::OutputStreamToMonoArray(stream);
        args.AddArgument(&destination_session_id);
        args.AddArgument(data);

        managed_object_.InvokeVirtualMethod("SendServerArbitrationEvent", args);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, NetworkFacadeStub, GetUserIdForSession, 1)
    int64_t NetworkFacadeImpl::GetUserIdForSession(int32_t session_id) const
    {
        Arguments args;
        args.AddArgument(&session_id);

        return managed_object_.InvokeVirtualMethod<int64_t>("GetUserIdForSession", args);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, NetworkFacadeStub, GetCharacterForArbitrationSession, 1)
    std::auto_ptr<Character> NetworkFacadeImpl::GetCharacterForArbitrationSession(int32_t session_id) const
    {
        Arguments args;
        args.AddArgument(&session_id);
        DotNetObject character_dno = managed_object_.InvokeVirtualMethod("GetCharacterForArbitrationSession", args);
        return BadumnaRuntime::Instance().GetMaybeCharacter(character_dno);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, NetworkFacadeStub, GetBadumnaIdForArbitrationSession, 1)
    BadumnaId NetworkFacadeImpl::GetBadumnaIdForArbitrationSession(int32_t session_id) const
    {
        Arguments args;
        args.AddArgument(&session_id);

        DotNetObject badumnaId = managed_object_.InvokeVirtualMethod("GetBadumnaIdForArbitrationSession", args);
        return BadumnaRuntime::Instance().CreateBadumnaId(badumnaId);
    }

    REGISTER_PROPERTY(CppWrapperStub, NetworkFacadeStub, StreamingStub)
    StreamingManager *NetworkFacadeImpl::GetStreamingManager() const
    {
        DotNetObject object = managed_object_.GetPropertyFromName("StreamingStub");
        return new StreamingManager(
            BadumnaRuntime::Instance().CreateStreamingManagerImpl(object, StreamingService()));
    }

    REGISTER_PROPERTY(CppWrapperStub, NetworkFacadeStub, MatchStub)
    std::auto_ptr<MatchFacade> NetworkFacadeImpl::GetMatchFacade() const
    {
        DotNetObject object = managed_object_.GetPropertyFromName("MatchStub");
        return std::auto_ptr<MatchFacade>(new MatchFacade(
            BadumnaRuntime::Instance().CreateMatchFacadeImpl(object)));
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, NetworkFacadeStub, SetOnlineEventHandler, 0)
    void NetworkFacadeImpl::SetOnlineEventDelegate(ConnectivityStatusDelegate online_delegate)
    {
        badumna_stack_->FacadeService()->SetOnlineEventDelegate(online_delegate);
        managed_object_.InvokeMethod("SetOnlineEventHandler");
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, NetworkFacadeStub, SetOfflineEventHandler, 0)
    void NetworkFacadeImpl::SetOfflineEventDelegate(ConnectivityStatusDelegate offline_delegate)
    {
        badumna_stack_->FacadeService()->SetOfflineEventDelegate(offline_delegate);
        managed_object_.InvokeMethod("SetOfflineEventHandler");
    }
    
    REGISTER_MANAGED_METHOD(CppWrapperStub, NetworkFacadeStub, SetAddressChangedEventHandler, 0)
    void NetworkFacadeImpl::SetAddressChangedEventDelegate(ConnectivityStatusDelegate address_change_delegate)
    {
        badumna_stack_->FacadeService()->SetAddressChangedEventDelegate(address_change_delegate);
        managed_object_.InvokeMethod("SetAddressChangedEventHandler");
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, NetworkFacadeStub, GetDestinationStub, 1)
    Vector3 NetworkFacadeImpl::GetDestination(IDeadReckonableSpatialReplica const &remote_entity) const
    {
        BADUMNA_LOG_INFO("NetworkFacadeImpl::GetDestination");

        if(!ContainsReplica(remote_entity))
        {
            return Vector3();
        }

        DotNetObject replica_stub = GetManagedReplica(remote_entity);

        Arguments args;
        args.AddArgument(replica_stub);
        DotNetObject ret = managed_object_.InvokeMethod("GetDestinationStub", args);
        ManagedObject managed_returned_object(managed_object_.GetMonoRuntime(), ret);

        float x = managed_returned_object.GetPropertyFromName<float>("X");
        float y = managed_returned_object.GetPropertyFromName<float>("Y");
        float z = managed_returned_object.GetPropertyFromName<float>("Z");
    
        return Vector3(x, y, z);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, NetworkFacadeStub, SnapToDestination, 1)
    void NetworkFacadeImpl::SnapToDestination(IDeadReckonableSpatialReplica *remote_entity) const
    {
        BADUMNA_LOG_INFO("NetworkFacadeImpl::SnapToDestination");

        assert(remote_entity != NULL);
        if(!ContainsReplica(*remote_entity))
        {
            return;
        }

        DotNetObject replica_stub = GetManagedReplica(*remote_entity);

        Arguments args;
        args.AddArgument(replica_stub);

        managed_object_.InvokeVirtualMethod("SnapToDestination", args);
    }

    // controller related
    void NetworkFacadeImpl::RegisterController(String const &type_name, CreateControllerDelegate creator)
    {
        BADUMNA_LOG_INFO("NetworkFacadeImpl::RegisterController");

        badumna_stack_->ControllerService()->RegisterCreateDelegate(type_name, creator);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, NetworkFacadeStub, StartControllerStub, 3)
    String NetworkFacadeImpl::StartController(
        String const &scene_name, 
        String const &type_name,
        uint32_t max)
    {
        BADUMNA_LOG_INFO("NetworkFacadeImpl::StartController");

        Arguments args;
        args.AddArgument(MonoRuntime::GetString(scene_name));
        args.AddArgument(MonoRuntime::GetString(type_name));
        args.AddArgument(&max);
        return managed_object_.InvokeMethod<String>("StartControllerStub", args);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, NetworkFacadeStub, StopControllerStub, 1)
    void NetworkFacadeImpl::StopController(String const &unique_name)
    {
        BADUMNA_LOG_INFO("NetworkFacadeImpl::StopController");

        Arguments args;
        args.AddArgument(MonoRuntime::GetString(unique_name));
        managed_object_.InvokeMethod("StopControllerStub", args);
    }

    REGISTER_PROPERTY(CppWrapperStub, NetworkFacadeStub, IsTunnelled)
    bool NetworkFacadeImpl::IsTunnelled() const
    {
        return managed_object_.GetPropertyFromName<bool>("IsTunnelled");
    }

    REGISTER_PROPERTY(CppWrapperStub, NetworkFacadeStub, IsLoggedIn)
    bool NetworkFacadeImpl::IsLoggedIn() const
    {
        return managed_object_.GetPropertyFromName<bool>("IsLoggedIn");
    }

    REGISTER_PROPERTY(CppWrapperStub, NetworkFacadeStub, IsFullyConnected)
    bool NetworkFacadeImpl::IsFullyConnected() const
    {
        return managed_object_.GetPropertyFromName<bool>("IsFullyConnected");
    }

    REGISTER_PROPERTY(CppWrapperStub, NetworkFacadeStub, Character)
    std::auto_ptr<Character> NetworkFacadeImpl::GetCharacter() const
    {
        return BadumnaRuntime::Instance().GetMaybeCharacter(
            managed_object_.GetPropertyFromName("Character"));
    }

    REGISTER_PROPERTY(CppWrapperStub, NetworkFacadeStub, IsOffline)
    bool NetworkFacadeImpl::IsOffline() const
    {
        return managed_object_.GetPropertyFromName<bool>("IsOffline");
    }
    
    REGISTER_PROPERTY(CppWrapperStub, NetworkFacadeStub, IsOnline)
    bool NetworkFacadeImpl::IsOnline() const
    {
        return managed_object_.GetPropertyFromName<bool>("IsOnline");
    }

    REGISTER_PROPERTY(CppWrapperStub, NetworkFacadeStub, OutboundBytesPerSecond)
    double NetworkFacadeImpl::GetOutboundBytesPerSecond() const
    {
        return managed_object_.GetVirtualPropertyFromName<double>("OutboundBytesPerSecond");
    }

    REGISTER_PROPERTY(CppWrapperStub, NetworkFacadeStub, InboundBytesPerSecond)
    double NetworkFacadeImpl::GetInboundBytesPerSecond() const
    {
        return managed_object_.GetVirtualPropertyFromName<double>("InboundBytesPerSecond");
    }
    
    REGISTER_PROPERTY(CppWrapperStub, NetworkFacadeStub, MaximumPacketLossRate)
    double NetworkFacadeImpl::GetMaximumPacketLossRate() const
    {
        return managed_object_.GetVirtualPropertyFromName<double>("MaximumPacketLossRate");
    }

    REGISTER_PROPERTY(CppWrapperStub, NetworkFacadeStub, AveragePacketLossRate)
    double NetworkFacadeImpl::GetAveragePacketLossRate() const
    {
        return managed_object_.GetVirtualPropertyFromName<double>("AveragePacketLossRate");
    }

    REGISTER_PROPERTY(CppWrapperStub, NetworkFacadeStub, TotalSendLimitBytesPerSecond)
    double NetworkFacadeImpl::GetTotalSendLimitBytesPerSecond() const
    {
        return managed_object_.GetVirtualPropertyFromName<double>("TotalSendLimitBytesPerSecond");
    }

    REGISTER_PROPERTY(CppWrapperStub, NetworkFacadeStub, MaximumSendLimitBytesPerSecond)
    double NetworkFacadeImpl::GetMaximumSendLimitBytesPerSecond() const
    {
        return managed_object_.GetVirtualPropertyFromName<double>("MaximumSendLimitBytesPerSecond");
    }

    bool NetworkFacadeImpl::ContainsOriginal(ISpatialOriginal const &local_entity) const
    {
        if(!local_entity.Guid().IsValid())
        {
            return false;
        }

        UniqueId original_id = InternalUniqueIdManager::GetOriginalId(local_entity);
        if(!EntityService()->ContainsOriginal(original_id))
        {
            return false;
        }

        return true;
    }

    DotNetObject NetworkFacadeImpl::GetManagedOriginal(ISpatialOriginal const &local_entity) const
    {
        UniqueId original_id = InternalUniqueIdManager::GetOriginalId(local_entity);
        OriginalWrapper *original = EntityService()->GetOriginal(original_id);
        DotNetObject original_stub = original->GetManagedMonoObject();
        return original_stub;
    }

    bool NetworkFacadeImpl::ContainsReplica(ISpatialReplica const &remote_entity) const
    {
        if(!remote_entity.Guid().IsValid())
        {
            return false;
        }

        if(!EntityService()->ContainsReplica(remote_entity))
        {
            return false;
        }

        return true;
    }

    DotNetObject NetworkFacadeImpl::GetManagedReplica(ISpatialReplica const &remote_entity) const
    {
        ReplicaWrapper *replica = EntityService()->GetReplica(remote_entity);
        DotNetObject replica_stub = replica->GetManagedMonoObject();
        return replica_stub;
    }
}
