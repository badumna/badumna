//---------------------------------------------------------------------------------
// <copyright file="InternalUniqueId.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_INTERNAL_UNIQUE_ID_H
#define BADUMNA_INTERNAL_UNIQUE_ID_H

#include "Badumna/DataTypes/String.h"
#include "Badumna/Replication/ISpatialReplica.h"
#include "Badumna/Replication/SpatialReplicaDelegate.h"

namespace Badumna
{
#ifndef DOXYGEN_SHOULD_SKIP_THIS
    typedef String UniqueId;
#endif // DOXYGEN_SHOULD_SKIP_THIS

    class InternalUniqueIdManager
    {
    public:
        static UniqueId GetSceneId(
            String const &scene_name, 
            CreateSpatialReplicaDelegate const create_delegate, 
            RemoveSpatialReplicaDelegate const remove_delegate);

        static UniqueId GetReplicaId(ISpatialReplica const &replica, UniqueId const &sceneId);
        static UniqueId GetOriginalId(ISpatialOriginal const &original);
        
        static UniqueId GetChatSessionId();
        
        static UniqueId GetArbitratorId(String const &name);

        static UniqueId GetStreamingCompleteDelegateId(
            String const &tag_name, 
            String const &stream_name, 
            BadumnaId const &destination);

        static UniqueId GetStreamingReceiveCompleteDelegateId(
            String const &tag_name,
            String const &stream_name,
            String const &username);

        static UniqueId GetStreamingRequestDelegateId(String const &tag_name);
    private:
        static const char separator_char = '_';
        InternalUniqueIdManager();
        ~InternalUniqueIdManager();
    };
}

#endif // BADUMNA_INTERNAL_UNIQUE_ID_H