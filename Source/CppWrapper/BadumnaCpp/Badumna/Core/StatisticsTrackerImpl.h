//---------------------------------------------------------------------------------
// <copyright file="StatisticsTrackerImpl.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_STATISTICS_TRACKER_IMPL_H
#define BADUMNA_STATISTICS_TRACKER_IMPL_H

#include <memory>
#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Core/ProxyObject.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class StatisticsTrackerImpl : public ProxyObject
    {
        friend class BadumnaRuntime;
    public:
        void SetUserId(int64_t id);
        void SetPayload(String const &payload);
        void Start();
        void Stop();

    private:
        StatisticsTrackerImpl(MonoRuntime *runtime, DotNetObject object);

        DISALLOW_COPY_AND_ASSIGN(StatisticsTrackerImpl);
    };
}

#endif // BADUMNA_STATISTICS_TRACKER_IMPL_H