//---------------------------------------------------------------------------------
// <copyright file="BadumnaStack.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_BADUMNA_STACK_H
#define BADUMNA_BADUMNA_STACK_H

#include <memory>
#include "Badumna/Arbitration/ArbitrationServiceManager.h"
#include "Badumna/Core/EntityManager.h"
#include "Badumna/Core/FacadeServiceManager.h"
#include "Badumna/Chat/ChatSessionManager.h"
#include "Badumna/Controller/DistributedControllerManager.h"
#include "Badumna/Replication/ReplicationServiceManager.h"
#include "Badumna/Streaming/StreamingServiceManager.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    /**
     * The BadumnaStack class manages all involved manager objects. 
     */ 
    class BadumnaStack
    {
        friend class MonoPInvokes;
        friend class NetworkFacadeImpl;
    public:
        BadumnaStack();
        void Shutdown();
    private:
        ReplicationServiceManager *ReplicationService();
        ChatSessionManager *ChatManager();
        ArbitrationServiceManager *ArbitrationService();
        FacadeServiceManager *FacadeService();
        DistributedControllerManager *ControllerService();
        EntityManager *EntityService();
        StreamingServiceManager *StreamingService();

        std::auto_ptr<ChatSessionManager> chat_manager_;
        std::auto_ptr<ReplicationServiceManager> replication_service_;
        std::auto_ptr<ArbitrationServiceManager> arbitration_service_;
        std::auto_ptr<FacadeServiceManager> facade_service_;
        std::auto_ptr<DistributedControllerManager> controller_service_;
        std::auto_ptr<EntityManager> entity_service_;
        std::auto_ptr<StreamingServiceManager> streaming_service_;

        bool initialized_;

        DISALLOW_COPY_AND_ASSIGN(BadumnaStack);
    };
}

#endif // BADUMNA_BADUMNA_STACK_H