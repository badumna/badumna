//---------------------------------------------------------------------------------
// <copyright file="NetworkStatusImpl.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>

#include "Badumna/Core/NetworkStatusImpl.h"
#include "Badumna/Core/TypeRegistry.h"

namespace Badumna
{
    REGISTER_MANAGED_MONO_TYPE(Badumna, NetworkStatus);
    NetworkStatusImpl::NetworkStatusImpl(MonoRuntime *runtime) 
        : ProxyObject(runtime)
    {
        assert(runtime != NULL);
    }

    NetworkStatusImpl::NetworkStatusImpl(MonoRuntime *runtime, DotNetObject object) 
        : ProxyObject(runtime, object, "Badumna", "NetworkStatus")
    {
        assert(runtime != NULL && object != NULL);
    }

    String NetworkStatusImpl::ToString() const
    {
        return ProxyObject::ToString();
    }

    REGISTER_PROPERTY(Badumna, NetworkStatus, PublicAddress)
    String NetworkStatusImpl::GetPublicAddress() const
    {
        return managed_object_.GetPropertyFromName<String>("PublicAddress");
    }

    REGISTER_PROPERTY(Badumna, NetworkStatus, PrivateAddress)
    String NetworkStatusImpl::GetPrivateAddress() const
    {
        return managed_object_.GetPropertyFromName<String>("PrivateAddress");
    }

    REGISTER_PROPERTY(Badumna, NetworkStatus, ActiveConnectionCount)
    int32_t NetworkStatusImpl::GetActiveConnectionCount() const
    {
        return managed_object_.GetPropertyFromName<int32_t>("ActiveConnectionCount");
    }

    REGISTER_PROPERTY(Badumna, NetworkStatus, InitializingConnectionCount)
    int32_t NetworkStatusImpl::GetInitializingConnectionCount() const
    {
        return managed_object_.GetPropertyFromName<int32_t>("InitializingConnectionCount");
    }

    REGISTER_PROPERTY(Badumna, NetworkStatus, DiscoveryMethod)
    String NetworkStatusImpl::GetDiscoveryMethod() const
    {
        return managed_object_.GetPropertyFromName<String>("DiscoveryMethod");
    }

    REGISTER_PROPERTY(Badumna, NetworkStatus, PortForwardingEnabled)
    bool NetworkStatusImpl::GetPortForwardingEnabled() const
    {
        return managed_object_.GetPropertyFromName<bool>("PortForwardingEnabled");
    }

    REGISTER_PROPERTY(Badumna, NetworkStatus, PortForwardingSucceeded)
    bool NetworkStatusImpl::GetPortForwardingSucceeded() const
    {
        return managed_object_.GetPropertyFromName<bool>("PortForwardingSucceeded");
    }

    REGISTER_PROPERTY(Badumna, NetworkStatus, LocalObjectCount)
    int32_t NetworkStatusImpl::GetLocalObjectCount() const
    {
        return managed_object_.GetPropertyFromName<int32_t>("LocalObjectCount");
    }

    REGISTER_PROPERTY(Badumna, NetworkStatus, RemoteObjectCount)
    int32_t NetworkStatusImpl::GetRemoteObjectCount() const
    {
        return managed_object_.GetPropertyFromName<int32_t>("RemoteObjectCount");
    }
    
    REGISTER_PROPERTY(Badumna, NetworkStatus, TotalBytesSentPerSecond)
    int64_t NetworkStatusImpl::GetTotalBytesSentPerSecond() const
    {
        return managed_object_.GetPropertyFromName<int64_t>("TotalBytesSentPerSecond");
    }

    REGISTER_PROPERTY(Badumna, NetworkStatus, TotalBytesReceivedPerSecond)
    int64_t NetworkStatusImpl::GetTotalBytesReceivedPerSecond() const
    {
        return managed_object_.GetPropertyFromName<int64_t>("TotalBytesReceivedPerSecond");
    }
    
    DEFINE_WRAPPER_CLASS_IMPL_COPYCTOR_ASSIGN_OPERATOR(NetworkStatusImpl)
}