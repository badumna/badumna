//---------------------------------------------------------------------------------
// <copyright file="NetworkStatus.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_NETWORK_STATUS_H
#define BADUMNA_NETWORK_STATUS_H

#include <memory>
#include <iosfwd>

#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class NetworkStatusImpl;

    /**
     * @class NetworkStatus.
     * @brief The network status class provides information about the state of the local peer. 
     *
     * This class is intended to provide useful information for users/network operators to diagnose issues with 
     * connectivity to a Badumna network. An instance of the class should be retrieved via a call to 
     * NetworkFacade.GetNetworkStatus(). The values in this instance are valid at the time it is returned from 
     * GetNetworkStatus(), but will not be automatically updated. To refresh the status, another call to 
     * GetNetworkStatus() should be made.
     */
    class BADUMNA_API NetworkStatus
    {
        friend class NetworkFacadeImpl;
    public:
        /**
         * Copy constructor.
         *
         * @param status Another network status instance to copy from.
         */
        NetworkStatus(NetworkStatus const &status);

        /**
         * Destructor.
         */
        ~NetworkStatus();

        /**
         * Assignment operator.
         *
         * @param status The right hand side parameter of the assignment operation.
         * @return A reference to this object.
         */
        NetworkStatus& operator=(NetworkStatus const &status);
        
        /**
         * Returns a human readable representation of the network status object.
         *
         * @return A string object that contains the human readable representation of the network status object.
         */
        String ToString() const;
        
        /**
         * Gets the public address.
         * 
         * @return The public address.
         */
        String GetPublicAddress() const;

        /**
         * Gets the private address.
         * 
         * @return The private address.
         */
        String GetPrivateAddress() const;

        /**
         * Gets the number of established connections count.
         * 
         * @return The established connections count.
         */
        int32_t GetActiveConnectionCount() const;

        /**
         * Gets the initializing connection count.
         * 
         * @return The number of the initializing connection count.
         */
        int32_t GetInitializingConnectionCount() const;

        /**
         * Gets the discovery method.
         * 
         * @return The discovery method.
         */
        String GetDiscoveryMethod() const;

        /**
         * Gets a value indicating whether port forwarding is enabled.
         * 
         * @return Whether port forwarding is enabled.
         */
        bool GetPortForwardingEnabled() const;

        /**
         * Gets a value indicating whether port forwarding succeed.
         * 
         * @return Whether port forwarding succeed.
         */
        bool GetPortForwardingSucceeded() const;

        /**
         * Gets the number of registered local entities.
         * 
         * @return The local entity count.
         */
        int32_t GetLocalObjectCount() const;

        /**
         * Gets the number of replicas.
         * 
         * @return The replica count.
         */
        int32_t GetRemoteObjectCount() const;

        /**
         * Gets the total bytes sent per second.
         * 
         * @return The total bytes sent per second.
         */
        int64_t GetTotalBytesSentPerSecond() const;

        /**
         * Gets the total bytes received per second.
         * 
         * @return The total bytes received per second.
         */
        int64_t GetTotalBytesReceivedPerSecond() const;
    
    private:
        explicit NetworkStatus(NetworkStatusImpl *imp);
        std::auto_ptr<NetworkStatusImpl> const impl_;
    };

    /**
     * Insertion operator.
     * 
     * @param out The ostream reference. 
     * @param status The network status object. 
     * @return A reference to the specified ostream object.
     */
    BADUMNA_API std::wostream& operator<<(std::wostream& out, NetworkStatus const &status);
}

#endif // BADUMNA_NETWORK_STATUS_H