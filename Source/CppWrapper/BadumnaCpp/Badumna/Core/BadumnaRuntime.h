//---------------------------------------------------------------------------------
// <copyright file="BadumnaRuntime.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_BADUMNA_RUNTIME_H
#define BADUMNA_BADUMNA_RUNTIME_H

#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Core/MonoRuntime.h"
#include "Badumna/Core/BadumnaStack.h"
#include "Badumna/Core/TypeRegistry.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/Singleton.h"
#include "Badumna/Utils/Assembly.h"
#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/Security/Character.h"

namespace Dei
{
    class SessionImpl;
    class IdentityProviderImpl;
} // forward declaration only. 

namespace Badumna
{
    class BadumnaId;
    class ChatChannelId;
    class ChatChannelImpl;
    class Character;
    class BooleanArray;
    class EntityManager;
    class ChatSessionManager;
    class ReplicationServiceManager;
    class ArbitrationServiceManager;
    class OriginalWrapper;
    class ReplicaWrapper;
    class ISpatialOriginal;
    class ISpatialReplica;
    class NetworkScene;
    class Options;

    class BadumnaIdImpl;
    class ChatChannelIdImpl;
    class BooleanArrayImpl;
    class NetworkStatusImpl;
    class NetworkFacadeImpl;
    class NetworkSceneImpl;
    class ChatSessionImpl;
    class InvalidOperationExceptionImpl;
    class NotSupportedExceptionImpl;
    class ArgumentNullExceptionImpl;
    class ArgumentExceptionImpl;
    class ArgumentOutOfRangeExceptionImpl;
    class ArbitrationClientImpl;
    class ArbitrationServerImpl;
    class StatisticsTrackerImpl;
    class DistributedSceneControllerImpl;
    class LocalStreamControllerImpl; 
    class StreamingServiceManager;
    class StreamingManagerImpl;
    class StreamRequestImpl;
    class MatchFacadeImpl;
    class MatchmakingQueryResultImpl;
    class MatchmakingResultImpl;
    
    class OptionsImpl;
    class ConnectivityModuleImpl;
    class OverloadModuleImpl;
    class ArbitrationModuleImpl;
    class LoggerModuleImpl;
    class MatchmakingModuleImpl;
    
    class ThreadGuardImpl;

    /**
     * The Badumna Runtime class is basically the runtime context of the process. It owns a Mono Runtime object which 
     * runs the managed Badumna.dll code, the BadumnaStack is a stack of those Service Managers which manage all 
     * registered objects and callbacks. 
     *
     * The Badumna Runtime class is mainly used to create Proxy Object of managed objects, e.g. see CreateBadumnaIdImpl.
     * It creates the specified type and inject the Mono Runtime object. 
     */
    class BadumnaRuntime : public Singleton<BadumnaRuntime>
    {
        friend class Singleton<BadumnaRuntime>;
    public:
        // Initialize the proxy object factory by setting up the Mono runtime. 
        bool Initialize(MonoRuntimeConfig *config);
        
        // Shutdown the badumna runtime, this will clean up the mono runtime. 
        void Shutdown();

        // verify whether the mono runtime and assembly contains all types required. 
        bool Verify(TypeRegistry const &registry) const;

        // Get the Badumna stack
        BadumnaStack &GetBadumnaStack()
        {
            return badumna_stack_;
        }

        // BadumnaId
        BadumnaId CreateBadumnaId(DotNetObject object);
        BadumnaId CreateBadumnaId(String const &id);
        BadumnaIdImpl *CreateBadumnaIdImpl(DotNetObject object);
        BadumnaIdImpl *CreateBadumnaIdImpl(DotNetObject object, String const &name_space, String const &class_name);
        BadumnaIdImpl *CreateBadumnaIdImpl();

        // ChatChannelId
        ChatChannelId CreateChatChannelId(DotNetObject object);
        ChatChannelId CreateChatChannelId(ChatChannelType type, BadumnaId id);
        ChatChannelIdImpl *CreateChatChannelIdImpl(DotNetObject object);

        // BooleanArray
        BooleanArray CreateBooleanArray(DotNetObject object);
        BooleanArrayImpl *CreateBooleanArrayImpl();
        BooleanArrayImpl *CreateBooleanArrayImpl(DotNetObject object);
        BooleanArrayImpl *CreateBooleanArrayImpl(bool value);
        BooleanArrayImpl *CreateBooleanArrayImpl(int const *indexes_set_to_true, size_t length);
        BooleanArrayImpl *CreateBooleanArrayImpl(String const &bits);

        // NetworkStatus
        NetworkStatusImpl *CreateNetworkStatusImpl(DotNetObject object);

        // NetworkFacade
        NetworkFacadeImpl *CreateNetworkFacadeImpl(Options const &options);
        NetworkFacadeImpl *CreateNetworkFacadeImpl(Badumna::String const &identifier);

        // ChatSession
        ChatSessionImpl *CreateChatSessionImpl(
            ChatSessionManager * manager,
            String const &id,
            DotNetObject object);

        // ChatChannel
        ChatChannelImpl *CreateChatChannelImpl(
            DotNetObject object,
            ChatMessageHandler message_handler);

        ChatChannelImpl *CreateChatChannelImpl(
            DotNetObject object,
            ChatMessageHandler message_handler,
            ChatPresenceHandler presence_handler);

        // NetworkScene
        NetworkSceneImpl *CreateNetworkSceneImpl(
            DotNetObject object, 
            String const &unique_scene_id,
            EntityManager *entity_manager,
            ReplicationServiceManager* replication_manager,
            NetworkFacadeImpl *network_facade_impl);

        NetworkScene *CreateNetworkScene(
            DotNetObject object, 
            EntityManager *entity_manager, 
            ReplicationServiceManager *manager);

        // Replica and Original Wrappers
        ReplicaWrapper *CreateReplicaWrapper(String const &replica_id, ISpatialReplica *replica);
        OriginalWrapper *CreateOriginalWrapper(ISpatialOriginal *original);

        // ArbitrationClient
        ArbitrationClientImpl *CreateArbitrationClientImpl(
            String const &id, 
            DotNetObject object, 
            ArbitrationServiceManager *manager);

        ArbitrationServerImpl *CreateArbitrationServerImpl(
            String const &id, 
            DotNetObject object, 
            ArbitrationServiceManager *manager);

        // Dei
        Dei::SessionImpl *CreateSessionImpl(String const &server_host, int server_port, String const *string_xml);
        Dei::IdentityProviderImpl* CreateIdentityProviderImpl(DotNetObject object);
        Badumna::String GenerateKeyPair();
        
        // Character
        Badumna::Character GetCharacter(DotNetObject object);
        std::auto_ptr<Badumna::Character> GetMaybeCharacter(DotNetObject object);
        DotNetObject ConvertCharacter(Character character);

        // Options
        OptionsImpl *CreateOptionsImpl(String const &filename);
        OptionsImpl *CreateOptionsImpl();
        ConnectivityModuleImpl *CreateConnectivityModuleImpl(DotNetObject object);
        OverloadModuleImpl *CreateOverloadModuleImpl(DotNetObject object);
        ArbitrationModuleImpl *CreateArbitrationModuleImpl(DotNetObject object);
        LoggerModuleImpl *CreateLoggerModuleImpl(DotNetObject object);
        MatchmakingModuleImpl *CreateMatchmakingModuleImpl(DotNetObject object);

        // Thread guard
        ThreadGuardImpl *CreateMonoThreadGuardImpl();

        // Presence tracker
        StatisticsTrackerImpl *CreateStatisticsTrackerImpl(DotNetObject object);

        // Distributed controller
        DistributedSceneControllerImpl *CreateDistributedControllerImpl(DotNetObject object, String const &name);

        // Streaming
        StreamingManagerImpl *CreateStreamingManagerImpl(DotNetObject object, StreamingServiceManager *manager);
        LocalStreamControllerImpl *CreateLocalStreamControllerImpl(DotNetObject object, StreamingServiceManager *manager);
        StreamRequestImpl *CreateStreamRequestImpl(DotNetObject object, StreamingServiceManager *manager);

        // Match
        MatchFacadeImpl *CreateMatchFacadeImpl(DotNetObject object);
        MatchmakingQueryResultImpl *CreateMatchmakingQueryResultImpl(DotNetObject object);
        MatchmakingResultImpl *CreateMatchmakingResultImpl(DotNetObject object);

    private:
        BadumnaRuntime();

        bool initialized_;
        MonoRuntime mono_runtime_;
        BadumnaStack badumna_stack_;

        DotNetObject facade_object_;

        DISALLOW_COPY_AND_ASSIGN(BadumnaRuntime);
    };
}

#endif // BADUMNA_BadumnaRuntime_H