//---------------------------------------------------------------------------------
// <copyright file="MonoRuntime.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifdef WIN32
#include <windows.h>
#endif

#include <mono/metadata/object.h>
#include <mono/metadata/threads.h>
#include <mono/metadata/mono-config.h>
#include <mono/metadata/debug-helpers.h>
#include <mono/metadata/appdomain.h>

#include <cassert>
#include <cstdlib>
#include <iostream>
#include <utility>

#include "Badumna/Core/iOS.h"
#include "Badumna/Core/MonoRuntime.h"
#include "Badumna/Core/MonoRuntimeConfig.h"
#include "Badumna/Core/BadumnaExceptionCallback.h"
#include "Badumna/Core/InternalErrorHandler.h"
#include "Badumna/Core/InternalErrorType.h"
#include "Badumna/DataTypes/OutputStream.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/ScopedPtr.h"
#include "Badumna/Utils/Logger.h"
#include "Badumna/Profiler/BadumnaProfiler.h"

namespace
{
    using std::pair;
    using Badumna::scoped_array;
    using Badumna::String;
    using Badumna::InternalErrorHandler;
    using Badumna::NameType;

    void CheckReturnedMethod(DotNetMethod method, String const &name, String const &msg = "")
    {
        if(method == NULL)
        {
            BADUMNA_LOG_FATAL("Couldn't locate the method, name : " << name.UTF8CStr());
            BADUMNA_LOG_FATAL(msg.UTF8CStr());

            String message("Couldn't locate the method, name : ");
            message += name;
            message += ", " + msg;
            InternalErrorHandler::Instance().Invoke(Badumna::InternalErrorType_ManagedMethodIsMissing, message);
        }
    }

    // helper method to allocate the specified method or property, mono requires the caller to travsal the 
    // class inheritance hierarchy to locate the wanted method/property. 
    DotNetMethod GetMethodFromName(DotNetClass top_level_class, NameType name, int count)
    {
        DotNetMethod method = NULL;
        DotNetClass klass = top_level_class;

        while(klass != NULL) 
        {
            method = mono_class_get_method_from_name(klass, name, count);
            if(method == NULL)
            {
                klass = mono_class_get_parent(klass);
            }
            else
            {
                return method;
            }
        }

        // couldn't get the method
        BADUMNA_LOG_FATAL("Couldn't locate the method, name : " << name << ", parameter number : " << count);

        String msg("Couldn't locate the method, name : ");
        msg += name;
        InternalErrorHandler::Instance().Invoke(Badumna::InternalErrorType_ManagedMethodIsMissing, msg);            
        
        return NULL;
    }

    DotNetProperty GetPropertyFromName(DotNetClass top_level_class, NameType name)
    {
        DotNetProperty prop = NULL;
        DotNetClass klass = top_level_class;

        while(klass != NULL)
        {
            prop = mono_class_get_property_from_name(klass, name);
            if(prop == NULL)
            {
                klass = mono_class_get_parent(klass);
            }
            else
            {
                return prop;
            }
        }

        // couldn't get the property
        BADUMNA_LOG_FATAL("Couldn't locate the property, name : " << name);
        String msg("Couldn't locate the property, name : ");
        msg += name;
        InternalErrorHandler::Instance().Invoke(Badumna::InternalErrorType_ManagedPropertyIsMissing, msg);  

        return NULL;
    }

    void DisableCompiledXMLSerializer()
    {
#ifdef WIN32
        ::SetEnvironmentVariable("MONO_XMLSERIALIZER_THS", "no");
#else
        putenv((char *)"MONO_XMLSERIALIZER_THS=no");
#endif
    }
}

namespace Badumna
{
    // each char in MonoString is always 2bytes UTF16, while on Linux/MacOSX, each char is 4bytes/UTF32. 
    String MonoStringToWStringImpl(DotNetString str)
    {
        assert(str != NULL);

        mono_unichar2 *p = mono_string_to_utf16(str);
#ifdef WIN32
        BADUMNA_STATIC_ASSERT(sizeof(wchar_t) == 2);
        String result_string(reinterpret_cast<wchar_t *>(p));
        mono_free(p);
        return result_string;
#else
        BADUMNA_STATIC_ASSERT(sizeof(wchar_t) == 4);
        size_t len = mono_string_length(str);
        scoped_array<wchar_t> utf32str(new wchar_t[len + 1]);
        UTF16ToUTF32(p, utf32str.get(), len);
        String result_string(utf32str.get());
        mono_free(p);
        return result_string;
#endif
    }

    MonoRuntime::MonoRuntime()
        : domain_(NULL), 
        image_manager_(),
        runtime_config_(NULL)
    {
    }

    MonoRuntime::~MonoRuntime()
    {
        BADUMNA_LOG_DEBUG("MonoRuntime::~dtor is called.");
    }

    void MonoRuntime::Shutdown()
    {
        // This must be called after ALL references to DotNetObjects have been deleted/freed, otherwise
        // you'll get SIGSEGV and friends during mono cleanup.

        // Singleton references should all be deleted as part of the relevant object's Shutdown() method,
        // which will be called (via BadumnaStack) before MonoRuntime::Shutdown().

        BADUMNA_LOG_DEBUG("mono_jit_cleanup()");
        mono_jit_cleanup(domain_);
        BADUMNA_LOG_DEBUG("mono_jit_cleanup() complete");
    }

    // required for unit test. 
    bool MonoRuntime::Initialize(MonoRuntimeConfig *config)
    {
        BADUMNA_LOG_DEBUG("MonoRuntime::Initialize is called.");
        // initialize should never be called twice
        assert(runtime_config_.get() == NULL);

        // get the ownership of the runtime config object.
        runtime_config_.reset(config);
        // set the mono runtime related environment variables
        ::DisableCompiledXMLSerializer();
        // tell mono where is the dll mapping configuration file. 
        String config_path = runtime_config_->GetConfigPath();
        BADUMNA_LOG_DEBUG("Mono config file: " << config_path.UTF8CStr());
        mono_config_parse(config_path.UTF8CStr());

        // config the mono dirs. 
        String mono_lib_path = runtime_config_->GetLibPath();
        String mono_etc_path = runtime_config_->GetEtcPath();
        BADUMNA_LOG_DEBUG("Setting mono lib path : " << mono_lib_path.UTF8CStr() << ", " << mono_etc_path.UTF8CStr());
        mono_set_dirs(mono_lib_path.UTF8CStr(), mono_etc_path.UTF8CStr());

        String assembly_path = runtime_config_->GetAssemblyPath();
        BADUMNA_LOG_DEBUG("Using assembly : " << assembly_path.UTF8CStr());

#if IOS
        initialize_ios();
#endif

        domain_ = mono_jit_init("BadumnaCpp");
        if(domain_ == NULL)
        {
            BADUMNA_LOG_FATAL("mono_jit_init failed.");
            return false;   
        }

        ::SetupBadumnaProfiler();

        return image_manager_.Initialize(runtime_config_->GetAssemblyPath());
    }

    DotNetString MonoRuntime::GetString(char const *text)
    {
        assert(text != NULL);
        return mono_string_new(mono_domain_get(), text);
    }

    DotNetString MonoRuntime::GetString(String const &str)
    {
        wchar_t *wchar_buf = const_cast<wchar_t *>(str.CStr());

#ifdef WIN32
            return mono_string_from_utf16(reinterpret_cast<mono_unichar2 *>(wchar_buf));
#else
            size_t len = str.Length();
            scoped_array<mono_unichar2> utf16str(new mono_unichar2[len + 1]);
            UTF32ToUTF16(wchar_buf, utf16str.get(), len);
            return mono_string_from_utf16(utf16str.get());
#endif
    }

    DotNetObject MonoRuntime::CreateObject(
        DotNetClass klass, 
        Arguments const &args, 
        ExceptionDetails *exc_details) const
    {
        BADUMNA_LOG_DEBUG("CreateObject.");

        assert(klass != NULL);
        DotNetObject created_object = mono_object_new(domain_, klass);
        InvokeMethod(klass, created_object, ".ctor", args, exc_details);

        return created_object;
    }

    DotNetObject MonoRuntime::CreateObject(DotNetClass klass, ExceptionDetails *exc_details) const
    {
        BADUMNA_LOG_DEBUG("CreateObject_2");

        assert(klass != NULL);
        return CreateObject(klass, Arguments::None(), exc_details);
    }

    DotNetObject MonoRuntime::InvokeMethod(
        DotNetClass klass, 
        DotNetObject object, 
        NameType name, 
        Arguments const &args, 
        ExceptionDetails *exc_details) const
    {
        BADUMNA_LOG_DEBUG("InvokeMethod(), method:" << name);

        assert(klass != NULL);
        DotNetObject exception = NULL;
        DotNetMethod method = ::GetMethodFromName(klass, name, args.Count());
        DotNetObject ret = mono_runtime_invoke(method, object, args, &exception);
        HandleException(name, exception, exc_details);

        return ret;
    }

    DotNetObject MonoRuntime::InvokeStaticMethod(
        NameType name_space, 
        NameType class_name, 
        NameType name, 
        Arguments const &args,
        ExceptionDetails *exc_details) const
    {
        BADUMNA_LOG_DEBUG("InvokeStaticMethod(), method:" << name);

        DotNetClass k = GetClass(name_space, class_name);
        DotNetObject object = InvokeMethod(k, NULL, name, args, exc_details);
        return mono_object_isinst(object, k);
    }

    DotNetObject MonoRuntime::InvokeMethodFullName(
        DotNetClass klass, 
        DotNetObject object, 
        NameType name, 
        bool include_namespace, 
        Arguments const &args,
        ExceptionDetails *exc_details) const
    {
        BADUMNA_LOG_DEBUG("InvokeMethodFullName(), method:" << name);

        assert(klass != NULL && object != NULL);
        DotNetObject exception = NULL;
        MonoMethodDesc *desc = mono_method_desc_new(name, include_namespace);
        DotNetMethod method = mono_method_desc_search_in_class(desc, klass);
        ::CheckReturnedMethod(method, name);
        DotNetObject ret = mono_runtime_invoke(method, object, args, &exception);
        mono_method_desc_free(desc);
        HandleException(name, exception, exc_details);

        return ret;
    }

    DotNetObject MonoRuntime::InvokeVirtualMethod(
        DotNetClass klass, 
        DotNetObject object, 
        NameType name, 
        Arguments const &args,
        ExceptionDetails *exc_details) const
    {
        BADUMNA_LOG_DEBUG("InvokeVirtualMethod(), method:" << name);

        assert(klass != NULL && object != NULL);

        DotNetObject exception = NULL;
        DotNetMethod method = ::GetMethodFromName(klass, name, args.Count());
        ::CheckReturnedMethod(method, name);
        DotNetMethod vtmethod = mono_object_get_virtual_method(object, method);
        ::CheckReturnedMethod(vtmethod, name, "not a virtual method?");
        DotNetObject ret = mono_runtime_invoke(vtmethod, object, args, &exception);
        HandleException(name, exception, exc_details);

        return ret;
    }

    DotNetObject MonoRuntime::InvokeVirtualMethodFullName(
        DotNetClass klass, 
        DotNetObject object, 
        NameType name, 
        bool include_namespace, 
        Arguments const &args, 
        ExceptionDetails *exc_details) const
    {
        BADUMNA_LOG_DEBUG("InvokeVirtualMethodFullName(), method:" << name);

        assert(klass != NULL && object != NULL);
        DotNetObject exception = NULL;
        MonoMethodDesc *desc = mono_method_desc_new(name, include_namespace);
        DotNetMethod method = mono_method_desc_search_in_class(desc, klass);
        ::CheckReturnedMethod(method, name);
        DotNetMethod vtmethod = mono_object_get_virtual_method(object, method);
        ::CheckReturnedMethod(vtmethod, name, "not a virtual method?");
        DotNetObject result = mono_runtime_invoke(vtmethod, object, args, &exception);
        mono_method_desc_free(desc);
        HandleException(name, exception, exc_details);

        return result;
    }

    DotNetObject MonoRuntime::GetStubObject(
        NameType name_space, 
        NameType name, 
        String const &unique_id,
        DotNetObject facade_object) const
    {   
        BADUMNA_LOG_DEBUG("GetStubObject(), namespace:" << name_space << ", name = " << name);
        DotNetClass stub_class = GetClass(name_space, name);
        
        Arguments args;
        args.AddArgument(GetString(unique_id));
        args.AddArgument(facade_object);
        return CreateObject(stub_class, args, NULL);
    }

    DotNetObject MonoRuntime::GetStubObject(
        NameType name_space, 
        NameType name, 
        DotNetObject facade_object) const
    {   
        BADUMNA_LOG_DEBUG("GetStubObject2(), namespace:" << name_space << ", name = " << name);
        DotNetClass stub_class = GetClass(name_space, name);
        
        Arguments args;
        args.AddArgument(facade_object);
        return CreateObject(stub_class, args, NULL);
    }

    DotNetObject MonoRuntime::GetPropertyFromName(
        NameType name_space, 
        NameType class_name, 
        DotNetObject object,
        NameType name, 
        Arguments const &args, 
        ExceptionDetails *exc_details) const
    {
        BADUMNA_LOG_DEBUG("GetPropertyFromName(), name:" << name);

        DotNetClass klass = GetClass(name_space, class_name);
        return GetPropertyFromName(klass, object, name, args, exc_details);
    }

    DotNetObject MonoRuntime::GetPropertyFromName(
        DotNetClass klass, 
        DotNetObject object, 
        NameType name, 
        Arguments const &args,
        ExceptionDetails *exc_details) const
    {
        BADUMNA_LOG_DEBUG("GetPropertyFromName_2(), name:" << name);

        assert(klass != NULL);
        DotNetObject exception = NULL;
        DotNetProperty prop = ::GetPropertyFromName(klass, name);

        void *real_object;
        if (mono_class_is_valuetype(klass))
        {
            real_object = mono_object_unbox(object);
        }
        else
        {
            real_object = object;
        }

        DotNetObject result = mono_property_get_value(prop, real_object, args, &exception);
        HandleException(name, exception, exc_details);

        return result;
    }

    DotNetObject MonoRuntime::GetVirtualPropertyFromName(
        DotNetClass klass, 
        DotNetObject object, 
        NameType name, 
        Arguments const &args, 
        ExceptionDetails *exc_details) const
    {
        BADUMNA_LOG_DEBUG("GetVirtualPropertyFromName(), name:" << name);

        assert(klass != NULL);
        DotNetObject exception = NULL;
        DotNetProperty prop = ::GetPropertyFromName(klass, name);
        DotNetMethod method = mono_property_get_get_method(prop);
        ::CheckReturnedMethod(method, name);
        DotNetMethod vtmethod = mono_object_get_virtual_method (object, method);
        ::CheckReturnedMethod(vtmethod, name, "not a virtual method?");
        DotNetObject result = mono_runtime_invoke(vtmethod, object, args, &exception);
        HandleException(name, exception, exc_details);

        return result;
    }

    void MonoRuntime::SetPropertyValue(
        DotNetClass klass, 
        DotNetObject object, 
        NameType name, 
        Arguments const &args, 
        ExceptionDetails *exc_details) const
    {
        BADUMNA_LOG_DEBUG("SetPropertyValue(), name:" << name);

        assert(klass != NULL);
        DotNetObject exception = NULL;
        DotNetProperty prop = ::GetPropertyFromName(klass, name);
        mono_property_set_value(prop, object, args, &exception);
        HandleException(name, exception, exc_details);
    }

    DotNetObject MonoRuntime::InvokeGetPropertyFromName(
        NameType name_space, 
        NameType class_name, 
        NameType property_name, 
        ExceptionDetails *exc_details) const
    {
        BADUMNA_LOG_DEBUG("InvokeGetPropertyFromName, name:" << property_name);

        DotNetObject exception = NULL;
        DotNetClass klass = GetClass(name_space, class_name);
        DotNetProperty prop = ::GetPropertyFromName(klass, property_name);
        DotNetMethod method = mono_property_get_get_method(prop);
        ::CheckReturnedMethod(method, property_name);
        DotNetObject result = mono_runtime_invoke(method, NULL, NULL, &exception);
        HandleException(property_name, exception, exc_details);
    
        return result;
    }

    DotNetObject MonoRuntime::GetStaticFieldValue(
        NameType name_space, 
        NameType class_name, 
        NameType name) const
    {
        BADUMNA_LOG_DEBUG("GetStaticFieldValue, name:" << name);
        DotNetClass k = GetClass(name_space, class_name);
        MonoClassField *field = mono_class_get_field_from_name(k, name);
        return mono_field_get_value_object(domain_, field, NULL);
    }

    // mono_object_clone generates a shadow copy of the object, which is not useful. 
    DotNetObject MonoRuntime::CloneObject(DotNetObject object) const
    {
        BADUMNA_LOG_DEBUG("CloneObject()");
        assert(object != NULL);
        DotNetClass k = MonoRuntime::GetClass(object);
        String class_name = mono_class_get_name(k);
        String name_space = mono_class_get_namespace(k);
        String ctor_name = name_space + "." + class_name + ":.ctor";
        String ctor_full_name = ctor_name + "(" + name_space + "." + class_name + ")";

        Arguments args;
        args.AddArgument(object);
        DotNetObject created_object = mono_object_new(domain_, k);
        InvokeMethodFullName(k, created_object, ctor_full_name.UTF8CStr(), true, args, NULL);

        return created_object;
    }

    DotNetClass MonoRuntime::GetClass(NameType name_space, NameType name) const
    {
        DotNetImage image = image_manager_.GetImage(name_space);
        DotNetClass returned_class = mono_class_from_name(image, name_space, name);

        if(returned_class == NULL)
        {
            // couldn't get the class
            BADUMNA_LOG_FATAL("Couldn't locate the class, namespace : " << 
                name_space << ", name : " << name);
            String msg("Couldn't locate the class, namespace : ");
            msg += name_space;
            msg += ", name : ";
            msg += name;
            InternalErrorHandler::Instance().Invoke(InternalErrorType_ManagedClassIsNull, msg);
        }

        return returned_class;
    }

    DotNetClass MonoRuntime::GetClass2(NameType name_space, NameType name) const
    {
        DotNetImage image = image_manager_.GetImage(name_space);
        DotNetClass returned_class = mono_class_from_name(image, name_space, name);

        if(returned_class == NULL)
        {
            return NULL;
        }

        return returned_class;
    }

    void MonoRuntime::HandleException(
        NameType method_name, 
        DotNetObject exception, 
        ExceptionDetails *exc_details) const
    {
        if(exception == NULL)
        {
            // no exception reported by the managed code
            return;
        }

        BADUMNA_LOG_WARN("Badumna exception caught when invoking " << method_name);
        
        DotNetClass exp_class = GetClass(exception);
        String exception_type_name = GetObjectTypeNameFromObject(exception);
        DotNetString str = reinterpret_cast<DotNetString>(GetPropertyFromName(
                exp_class, 
                exception, 
                "Message", 
                Arguments::None(), 
                NULL));
        String message = MonoRuntime::MonoStringToWString(str);

        str = reinterpret_cast<DotNetString>(GetPropertyFromName(
                exp_class, 
                exception, 
                "StackTrace", 
                Arguments::None(), 
                NULL));
        String stack_trace = MonoRuntime::MonoStringToWString(str);

        if(exc_details == NULL)
        {
            // there should be any exception. the wrapper can not handle such exception. 
            // time to invoke the exception callback methods. 
            if(runtime_config_.get() != NULL && runtime_config_->ExceptionCallbackRegistered())
            {
                BadumnaExceptionCallback callback = runtime_config_->GetExceptionCallback();
                callback(stack_trace, exception_type_name, message);
            }
            else
            {
                while (exception != NULL)
                {
                    exp_class = GetClass(exception);
                    exception_type_name = GetObjectTypeNameFromObject(exception);
                    message = MonoRuntime::MonoStringToWString(reinterpret_cast<DotNetString>(GetPropertyFromName(
                            exp_class,
                            exception,
                            "Message",
                            Arguments::None(),
                            NULL)));

                    stack_trace = MonoRuntime::MonoStringToWString(reinterpret_cast<DotNetString>(GetPropertyFromName(
                            exp_class,
                            exception,
                            "StackTrace",
                            Arguments::None(),
                            NULL)));


                    std::cerr << "Exception caught. method: " << stack_trace.UTF8CStr() << std::endl;
                    std::cerr << "Exception type name: " << exception_type_name.UTF8CStr() << std::endl;
                    std::cerr << "Message: " << message.UTF8CStr() << std::endl;

                    exception = GetPropertyFromName(
                        exp_class,
                        exception,
                        "InnerException",
                        Arguments::None(),
                        NULL);
                }

                abort();
            }
        }
        else
        {
            exc_details->exception_caught = true;
            exc_details->method_name = method_name;
            exc_details->managed_exception_type_name = exception_type_name;
            exc_details->message = message;
        }
    }

    template<>
    DotNetString MonoRuntime::UnboxingObject<DotNetString>(DotNetObject object)
    {
        assert(object != NULL);
        return reinterpret_cast<DotNetString>(object);
    }

    template<>
    String MonoRuntime::UnboxingObject<String>(DotNetObject object) 
    {
        assert(object != NULL);
        DotNetString str = reinterpret_cast<DotNetString>(object);
        return MonoStringToWStringImpl(str);
    }

    DotNetThread MonoRuntime::ThreadAttach()
    {
        return mono_thread_attach(domain_);
    }
        
    void MonoRuntime::ThreadDetach(MonoThread* thread)
    {
        mono_thread_detach(thread);
    }

    bool MonoRuntime::MethodAvailable(
        NameType name_space, 
        NameType class_name, 
        NameType name,
        int count) const
    {
        DotNetClass klass = GetClass2(name_space, class_name);
        if(klass == NULL)
        {
            return false;
        }

        DotNetMethod method = NULL;
        while(klass != NULL) 
        {
            method = mono_class_get_method_from_name(klass, name, count);
            if(method == NULL)
            {
                klass = mono_class_get_parent(klass);
            }
            else
            {
                return true;
            }
        }

        return false;
    }

    bool MonoRuntime::PropertyAvailable(
        NameType name_space, 
        NameType class_name, 
        NameType name) const
    {
        DotNetClass klass = GetClass2(name_space, class_name);
        if(klass == NULL)
        {
            return false;
        }

        DotNetProperty prop = NULL;
        while(klass != NULL)
        {
            prop = mono_class_get_property_from_name(klass, name);
            if(prop == NULL)
            {
                klass = mono_class_get_parent(klass);
            }
            else
            {
                return true;
            }
        }
        
        return false;
    }

    //
    // static methods
    //
    DotNetClass MonoRuntime::GetClass(DotNetObject object)
    {
        BADUMNA_LOG_DEBUG("GetClass.");
        assert(object != NULL);
        
        DotNetClass returned_class = mono_object_get_class(object);
        if(returned_class == NULL)
        {
            // couldn't get the class
            String msg("Couldn't locate the class");
            InternalErrorHandler::Instance().Invoke(InternalErrorType_ManagedClassIsNull, msg);
        }

        return returned_class;
    }

    String MonoRuntime::GetObjectTypeNameFromObject(DotNetObject object)
    {
        BADUMNA_LOG_DEBUG("GetObjectTypeNameFromObject.");
        assert(object != NULL);
        DotNetClass klass = GetClass(object);
        return mono_class_get_name(klass);
    }

    DotNetObject MonoRuntime::GetObjectFromGCHandle(MonoGCHandle handle)
    {
        BADUMNA_LOG_DEBUG("GetObjectFromGCHandle, val = " << handle);
        return mono_gchandle_get_target(handle);
    }

    MonoGCHandle MonoRuntime::GetObjectGCHandle(DotNetObject object) 
    {
        BADUMNA_LOG_DEBUG("GetObjectGCHandle.");
        assert(object != NULL);
        // managed object should not be pinned, as pinned objects cause heap fragmentation 
        return mono_gchandle_new(object, false);
    }

    void MonoRuntime::FreeObjectGCHandle(MonoGCHandle handle) 
    {
        BADUMNA_LOG_DEBUG("FreeObjectGCHandle, val = " << handle);
        mono_gchandle_free(handle);
    }

    String MonoRuntime::MonoStringToString(DotNetString str) 
    {
        assert(str != NULL);
        char *p = mono_string_to_utf8(str);
        String result_string(p);
        mono_free(p);
        return result_string;
    }

    String MonoRuntime::MonoStringToWString(DotNetString str)
    {
        assert(str != NULL);
        return MonoStringToWStringImpl(str);
    }

    DotNetArray MonoRuntime::OutputStreamToMonoArray(OutputStream *stream)
    {
        int length = stream->GetLength();
        BADUMNA_LOG_DEBUG("OutputStreamToMonoArray, length = " << length);
        assert(stream != NULL);

        if(length > 0)
        {
            scoped_array<char> buffer(new char[length]);
            stream->GetBuffer(buffer.get(), length);
            return MonoRuntime::GetMonoByteArray(buffer.get(), length);
        }
        else
        {
            return NULL;
        }
    }

    pair<char *, size_t> MonoRuntime::GetArrayData(DotNetArray data)
    {
        size_t s = mono_array_length(data);
        BADUMNA_LOG_DEBUG("GetArrayData(), size:" << s);
        
        assert(data != NULL);
        if(s <= 0)
        {
            return std::make_pair(static_cast<char*>(NULL), s);
        }

        char *buffer = new char[s];
        for (size_t i = 0; i < s; i++)
        {
            buffer[i] = mono_array_get(data, char, i);
        }

        return std::make_pair(buffer, s);
    }

    DotNetArray MonoRuntime::GetMonoByteArray(char const *data, size_t length)
    {
        BADUMNA_LOG_DEBUG("GetMonoByteArray(), size:" << length);
        assert(data != NULL);
        DotNetArray mono_array = mono_array_new(mono_domain_get(), mono_get_byte_class(), length);
        for (size_t i = 0; i < length; i++)
        {
#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable : 4127)
#endif
            mono_array_set(mono_array, unsigned char, i, data[i]);
#ifdef _MSC_VER
#pragma warning(pop)
#endif
        }

        return mono_array;
    }

    DotNetObject MonoRuntime::GetManagedFloatValue(float value)
    {
        BADUMNA_LOG_DEBUG("GetManagedFloatValue(), val:" << value);
        DotNetDomain d = mono_domain_get();
        return mono_value_box(d, mono_get_single_class(), &value);
    }
}