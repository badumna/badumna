//---------------------------------------------------------------------------------
// <copyright file="MonoPInvokes.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_MONO_PINVOKES_H
#define BADUMNA_MONO_PINVOKES_H

#include <vector>
#include <utility>

#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/Core/NetworkFacadeImpl.h"
#include "Badumna/Core/FacadeServiceManager.h"
#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/Chat/ChatSessionManager.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Replication/ReplicationServiceManager.h"
#include "Badumna/Arbitration/ArbitrationServiceManager.h"
#include "Badumna/Streaming/StreamingServiceManager.h"

namespace Badumna
{
#ifndef DOXYGEN_SHOULD_SKIP_THIS
    // system related
    extern "C" int InvokeGetCppWrapperVersion();

    // replication related
    // replication related Mono callbacks   
    extern "C" DotNetObject InvokeCreateSpatialReplicaDelegate(
        DotNetString unique_id, 
        DotNetObject scene,
        DotNetString scene_id,
        DotNetObject entity_id, 
        uint32_t entity_type);

    extern "C" void InvokeRemoveSpatialReplicaDelegate(
        DotNetString unique_id, 
        DotNetObject scene, 
        DotNetString replica_unique_id);
    
    extern "C" void InvokeDeserialize(
        DotNetString unique_id, 
        DotNetObject included_parts, 
        DotNetArray data, 
        int estimated_milliseconds_since_departure);

    extern "C" DotNetArray InvokeSerialize(
        DotNetString unique_id, 
        DotNetObject required_parts);

    extern "C" void InvokeSetProperty(
        DotNetString unique_id, 
        int property_index, 
        DotNetObject value);
    
    extern "C" void InvokeSetPositionProperty(
        DotNetString unique_id, 
        int property_index, 
        float x, float y, float z);

    extern "C" void InvokeSetVelocityProperty(
        DotNetString unique_id, 
        float x, float y, float z);

    extern "C" void InvokeAttemptMovement(DotNetString unique_id, float x, float y, float z);

    extern "C" void InvokeHandleOriginalEvent(DotNetString unique_id, DotNetArray data);
    extern "C" void InvokeHandleReplicaEvent(DotNetString unique_id, DotNetArray data);
    
    // chat related
    extern "C" void InvokeChatMessageHandler(
        DotNetString uniqueId, 
        DotNetObject channel, 
        DotNetObject user_id, 
        DotNetString message);

    extern "C" void InvokeChatPresenceHandler(
        DotNetString unique_id, 
        DotNetObject channel, 
        DotNetObject user_id, 
        DotNetString display_name, 
        int status);

    extern "C" void InvokeChatInvitationHandler(
        DotNetString unique_id, 
        DotNetObject channel, 
        DotNetString username);

    // arbitration client related
    extern "C" void InvokeArbitrationConnectionDelegate(DotNetString unique_id, int type);
    extern "C" void InvokeArbitrationConnectionFailureDelegate(DotNetString unique_id);
    extern "C" void InvokeArbitrationServerMessageDelegate(DotNetString unique_id, DotNetArray message);

    // arbitration server related
    extern "C" void InvokeArbitrationClientMessageDelegate(DotNetString unique_id, int session_id, DotNetArray message);
    extern "C" void InvokeArbitrationClientDisconnectDelegate(DotNetString unique_id, int session_id);

    // distributed controller related
    extern "C" void InvokeCreateController(DotNetObject object, DotNetString unique_name, DotNetString type_name);
    
    extern "C" DotNetObject InvokeTakeControlOfDCEntity(
        DotNetString unique_name, 
        DotNetObject entity_id, 
        uint32_t entityType);
    
    extern "C" DotNetObject InvokeCreateDCSpatialReplica(
        DotNetString unique_name,
        DotNetObject entity_id,
        uint32_t entity_type);

    extern "C" void InvokeRemoveDCSpatialReplica(DotNetString unique_name, DotNetString replia_id);
    extern "C" DotNetArray InvokeDCCheckpoint(DotNetString unique_name);
    extern "C" void InvokeDCRecover(DotNetString unique_name, DotNetArray data);
    extern "C" void InvokeDCRecover2(DotNetString unique_name);
    extern "C" void InvokeDCProcess(DotNetString unique_name, int duration_in_ms);
    extern "C" void InvokeDCWake(DotNetString unique_name);
    extern "C" void InvokeDCSleep(DotNetString unique_name, DotNetString original_id);
    extern "C" void InvokeDCShutdown(DotNetString unique_name);

    extern "C" int InvokeIsRequiredControllerNameRegistered(DotNetString name);

    // streaming
    extern "C" void InvokeStreamSendComplete(DotNetString tag, DotNetString name, DotNetObject destination);
    extern "C" void InvokeStreamRequestHandle(DotNetObject args);
    extern "C" void InvokeStreamReceiveComplete(DotNetString tag, DotNetString name, DotNetString username);

    // facade related
    extern "C" void InvokeOnlineEventDelegate();
    extern "C" void InvokeOfflineEventDelegate();
    extern "C" void InvokeAddressChangedEventDelegate();

#endif // DOXYGEN_SHOULD_SKIP_THIS
    class MonoPInvokes
    {
    public:
        // setup all extern calls
        static void RegisterExternalCalls();

        static void GetInternalCallDetails(std::vector<std::pair<String, void const *> > *details);

        static ReplicationServiceManager *ReplicationManager()
        {
            return BadumnaRuntime::Instance().GetBadumnaStack().ReplicationService();
        }

        static ChatSessionManager *ChatManager()
        {
            return BadumnaRuntime::Instance().GetBadumnaStack().ChatManager();
        }

        static ArbitrationServiceManager *ArbitrationManager()
        {
            return BadumnaRuntime::Instance().GetBadumnaStack().ArbitrationService();
        }

        static FacadeServiceManager *FacadeManager()
        {
            return BadumnaRuntime::Instance().GetBadumnaStack().FacadeService();
        }

        static DistributedControllerManager* ControllerManager()
        {
            return BadumnaRuntime::Instance().GetBadumnaStack().ControllerService();
        }

        static EntityManager *EntityService()
        {
            return BadumnaRuntime::Instance().GetBadumnaStack().EntityService();
        }

        static StreamingServiceManager *StreamingManager()
        {
            return BadumnaRuntime::Instance().GetBadumnaStack().StreamingService();
        }

    private:
        MonoPInvokes();
        ~MonoPInvokes();

        DISALLOW_COPY_AND_ASSIGN(MonoPInvokes);
    };
}

#endif // BADUMNA_MONO_PINVOKES_H