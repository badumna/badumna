//---------------------------------------------------------------------------------
// <copyright file="ExceptionDetails.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_EXCEPTION_DETAILS_H
#define BADUMNA_EXCEPTION_DETAILS_H

#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    /**
     * The details of an exception.
     */ 
    struct ExceptionDetails
    {
    public:
        ExceptionDetails()
            : method_name(),
            managed_exception_type_name(),
            message(),
            exception_caught(false)
        {
        }

        /**
         * The name of the method that trigger the exception.
         */
        String method_name;
        
        /**
         * The type name of the managed exception class.
         */
        String managed_exception_type_name;

        /**
         * The message of the exception.
         */
        String message;

        /**
         * Whether there is exception caught.
         */
        bool exception_caught;
    };
}

#endif // BADUMNA_EXCEPTION_DETAILS_H