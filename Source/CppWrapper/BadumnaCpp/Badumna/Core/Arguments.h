//---------------------------------------------------------------------------------
// <copyright file="Arguments.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_ARGUMENTS_H
#define BADUMNA_ARGUMENTS_H

#include <vector>

#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Utils/ScopedPtr.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    /**
     * Mono takes an array of void* pointers (void **) as parameter when invoke a managed method. 
     * The Arguments class is a safe wrapper for void**. 
     */
    class Arguments
    {
    public:
        Arguments();

        static Arguments const& None()
        {
            static Arguments none;
            return none;
        }

        // these methods should not be replaced by a template function, as clearly we don't want 
        // int*** or MonoRuntime* to be acceptable.  
        void AddArgument(int *p);
        void AddArgument(float *p);
        void AddArgument(bool *p);
        void AddArgument(void *p);
        void AddArgument(size_t *p);
        void AddArgument(DotNetObject p);
        void AddArgument(DotNetString p);
        void AddArgument(DotNetArray p);

        void AddNullArgument();

        inline int Count() const
        {
            return index_;
        }

        operator void **() const;
    private:
        void FailIfFnalized();
        static const int MAX_ARGS_NUM = 8;

        void *args_[MAX_ARGS_NUM];
        int index_;
        mutable bool finalized_;

        DISALLOW_COPY_AND_ASSIGN(Arguments);
    };
}

#endif