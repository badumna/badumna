//---------------------------------------------------------------------------------
// <copyright file="ThreadGuardImpl.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_THREAD_GUARD_IMPL_H
#define BADUMNA_THREAD_GUARD_IMPL_H

#include <cassert>
#include "Badumna/Core/MonoRuntime.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class ThreadGuardImpl
    {
        friend class BadumnaRuntime;
    public:
        ~ThreadGuardImpl()
        {
            mono_runtime_->ThreadDetach(thread_);
        }

    private:
        explicit ThreadGuardImpl(MonoRuntime *runtime)
            : mono_runtime_(runtime),
            thread_(mono_runtime_->ThreadAttach())
        {
            assert(thread_ != NULL);
        }

        MonoRuntime *mono_runtime_;
        MonoThread *thread_;
        
        DISALLOW_COPY_AND_ASSIGN(ThreadGuardImpl);
    };
}

#endif // BADUMNA_THREAD_GUARD_IMPL_H