//---------------------------------------------------------------------------------
// <copyright file="BadumnaStack.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include "Badumna/Core/BadumnaStack.h"

namespace Badumna
{
    BadumnaStack::BadumnaStack()
        : chat_manager_(new ChatSessionManager),
        replication_service_(new ReplicationServiceManager),
        arbitration_service_(new ArbitrationServiceManager),
        facade_service_(new FacadeServiceManager),
        controller_service_(new DistributedControllerManager),
        entity_service_(new EntityManager),
        streaming_service_(new StreamingServiceManager),
        initialized_(true)
    {
    }

    void BadumnaStack::Shutdown()
    {
        assert(initialized_ && "Shutdown down runtime shouldn't be called twice.");
        initialized_ = false;
        chat_manager_.reset(NULL);
        replication_service_.reset(NULL);
        arbitration_service_.reset(NULL);
        facade_service_.reset(NULL);
        controller_service_.reset(NULL);
        entity_service_.reset(NULL);
        streaming_service_.reset(NULL);
    }

    ReplicationServiceManager *BadumnaStack::ReplicationService()
    {
        assert(initialized_);
        return replication_service_.get();
    }

    ChatSessionManager *BadumnaStack::ChatManager()
    {
        assert(initialized_);
        return chat_manager_.get();
    }

    ArbitrationServiceManager *BadumnaStack::ArbitrationService()
    {
        assert(initialized_);
        return arbitration_service_.get();
    }

    FacadeServiceManager *BadumnaStack::FacadeService()
    {
        assert(initialized_);
        return facade_service_.get();
    }

    DistributedControllerManager *BadumnaStack::ControllerService()
    {
        assert(initialized_);
        return controller_service_.get();
    }

    EntityManager *BadumnaStack::EntityService()
    {
        assert(initialized_);
        return entity_service_.get();
    }

    StreamingServiceManager *BadumnaStack::StreamingService()
    {
        assert(initialized_);
        return streaming_service_.get();
    }
}