//---------------------------------------------------------------------------------
// <copyright file="StatisticsTrackerImpl.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include "Badumna/Core/StatisticsTrackerImpl.h"
#include "Badumna/Core/TypeRegistry.h"

namespace Badumna
{
    REGISTER_MANAGED_MONO_TYPE(Utilities, StatisticsTracker);
    StatisticsTrackerImpl::StatisticsTrackerImpl(MonoRuntime *runtime, DotNetObject object)
        : ProxyObject(runtime, object, "Badumna.Utilities", "StatisticsTracker")
    {
    }

    REGISTER_MANAGED_METHOD(Utilities, StatisticsTracker, SetUserID, 1)
    void StatisticsTrackerImpl::SetUserId(int64_t id)
    {
        Arguments args;
        args.AddArgument(&id);
        managed_object_.InvokeMethod("SetUserID", args);
    }

    REGISTER_MANAGED_METHOD(Utilities, StatisticsTracker, SetPayload, 1)
    void StatisticsTrackerImpl::SetPayload(String const &payload)
    {
        Arguments args;
        args.AddArgument(MonoRuntime::GetString(payload));
        managed_object_.InvokeMethod("SetPayload", args);
    }

    REGISTER_MANAGED_METHOD(Utilities, StatisticsTracker, Start, 0)
    void StatisticsTrackerImpl::Start()
    {
        managed_object_.InvokeMethod("Start");
    }

    REGISTER_MANAGED_METHOD(Utilities, StatisticsTracker, Stop, 0)
    void StatisticsTrackerImpl::Stop()
    {
        managed_object_.InvokeMethod("Stop");
    }
}