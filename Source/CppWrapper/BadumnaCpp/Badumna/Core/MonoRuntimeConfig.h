//---------------------------------------------------------------------------------
// <copyright file="MonoRuntimeConfig.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_MONO_RUNTIME_CONFIG_H
#define BADUMNA_MONO_RUNTIME_CONFIG_H

#include "Badumna/Core/BadumnaExceptionCallback.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/Singleton.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    /**
     * The configuration for the Mono runtime, including the location of Badumna.dll/Dei.dll and the exception handlers
     * that will be registered with the runtime. 
     */
    class MonoRuntimeConfig
    {
    public:
        /**
         * Constructor.
         */
        MonoRuntimeConfig();

        /**
         * Set the path of the embedded mono.
         *
         * @param path The path of the embedded mono.
         */
        void SetMonoPath(String const &path);
        
        /**
         * Set the path of the Badumna.dll assembly.
         *
         * @param path The path of the Badumna.dll assembly.
         */
        void SetAssemblyPath(String const &path);

        /**
         * Config the runtime according to the configuration file specified by path.
         *
         * @param path The path of the configuration file.
         */
        bool ConfigFromFile(String const &path);

        /**
         * Register the callback method for exceptions. BadumnaCPPWrapper doesn't throw any exception from its native 
         * code and is compiled with C++ exceptions disabled by default. However, the Badumna engine itself implemented
         * in .Net use exception extensively for reporting logic errors, e.g. Login is called before Initialize is 
         * called, such logic errors reported by the Badumna engine (Badumna.dll) is passed to the C++ application code
         * through this exception callback method. Application code can assume the callback will never return, i.e. it 
         * is recommended to just call terminate() at the end of the callback. The application will call abort on 
         * exception when no callback is registered. 
         *
         * @param callback The callback method called on exception.
         */
        void SetExceptionCallback(BadumnaExceptionCallback callback)
        {
            exc_callback_ = callback;
            exc_callback_registered_ = true;
        }

        /**
         * Get the exception callback.
         *
         * @return The exception callback.
         */
        BadumnaExceptionCallback GetExceptionCallback() const
        {
            return exc_callback_;
        }

        /**
         * Determines whether exception callback has been registered.
         *
         * @return A boolean value indicating whether exception callback has been registered.
         */
        bool ExceptionCallbackRegistered() const
        {
            return exc_callback_registered_;
        }

        /**
         * Get the path of the mono/lib subdirectory.
         *
         * @return The path of mono/lib subdirectory.
         */
        String GetLibPath() const
        {
            return mono_lib_path_;
        }

        /**
         * Get the path of the mono/etc subdirectory.
         *
         * @return The path of mono/etc subdirectory.
         */
        String GetEtcPath() const
        {
            return mono_etc_path_;
        }

        /**
         * Get the path of the configuration file.
         *
         * @return The path of configuration file.
         */
        String GetConfigPath() const
        {
            return mono_config_file_path_;
        }

        /**
         * Get the path of the Badumna.dll assembly.
         *
         * @return The path of the Badumna.dll assembly.
         */
        String GetAssemblyPath() const
        {
            return assembly_path_;
        }
    
    private:
        String mono_lib_path_;
        String mono_etc_path_;
        String mono_config_file_path_;
        String assembly_path_;

        BadumnaExceptionCallback exc_callback_;
        bool exc_callback_registered_;

        DISALLOW_COPY_AND_ASSIGN(MonoRuntimeConfig);
    };
}

#endif // BADUMNA_MONO_RUNTIME_CONFIG_H