//---------------------------------------------------------------------------------
// <copyright file="StatisticsTracker.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include "Badumna/Core/StatisticsTrackerImpl.h"
#include "Badumna/Core/StatisticsTracker.h"

namespace Badumna
{
    StatisticsTracker::StatisticsTracker(StatisticsTrackerImpl *impl)
        : impl_(impl)
    {
    }

    StatisticsTracker::~StatisticsTracker()
    {
    }

    void StatisticsTracker::SetUserId(int64_t id)
    {
        impl_->SetUserId(id);
    }

    void StatisticsTracker::SetPayload(String const &payload)
    {
        impl_->SetPayload(payload);
    }

    void StatisticsTracker::Start()
    {
        impl_->Start();
    }

    void StatisticsTracker::Stop()
    {
        impl_->Stop();
    }
}