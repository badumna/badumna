//---------------------------------------------------------------------------------
// <copyright file="ProxyObject.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_PROXY_OBJECT_H
#define BADUMNA_PROXY_OBJECT_H

#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Core/ManagedObject.h"
#include "Badumna/Core/MonoRuntime.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    /**
     * ProxyObject is the base class for all wrapper object classes (BadumnaIdImpl, BooleanArrayImpl etc.). It is a
     * Proxy Object because all calls will be redirected to the actually managed_object_ object. 
     *
     * Concrete ProxyObject classes are usually those IMPL classes (e.g. NetworkFacadeImpl, NetworkStatusImpl). PIMPL
     * is extensively used as compiler firewall so the internal Mono details are completely transparent to the C++ app 
     * built on BadumnaCpp, e.g. app code doesn't need to include any Mono .h header to get compiled. 
     *
     * Concrete ProxyObject classes usually expose a concrete and high level interface, rather than general InvokeMethod, 
     * GetPropertyFromName, SetPropertyValue kind of low level interface. 
     */
    class ProxyObject
    {
    public:
        explicit ProxyObject(ManagedObject const &object);

        virtual ~ProxyObject()
        {
        }

        DotNetObject GetManagedMonoObject() const;
        DotNetClass GetManagedMonoClass() const;

        String ToString() const;
        int GetHashCode() const;
        bool Equals(DotNetObject other) const;
    
        MonoRuntime *GetMonoRuntime() const
        {
            return mono_runtime_;
        }

    protected:
        explicit ProxyObject(MonoRuntime *runtime);
        ProxyObject(MonoRuntime *runtime, DotNetObject object);
        ProxyObject(MonoRuntime *runtime, NameType name_space, NameType class_name);
        ProxyObject(MonoRuntime *runtime, DotNetObject object, NameType name_space, NameType class_name);

        MonoRuntime * const mono_runtime_;
        ManagedObject managed_object_;

        DISALLOW_COPY_AND_ASSIGN(ProxyObject);
    };
}

#endif // BADUMNA_PROXY_OBJECT_H