//---------------------------------------------------------------------------------
// <copyright file="InternalUniqueId.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include <sstream>

#include "Badumna/Core/InternalUniqueId.h"

namespace Badumna
{
    using std::wstring;
    using std::wstringstream;

    UniqueId InternalUniqueIdManager::GetSceneId(
        String const &scene_name, 
        CreateSpatialReplicaDelegate const /*create_delegate*/, 
        RemoveSpatialReplicaDelegate const /*remove_delegate*/)
    {
        wstring ws(scene_name.CStr());
        wstringstream skey;
        skey << L"scene_uid" << InternalUniqueIdManager::separator_char;
        skey << ws;

        return skey.str().c_str();
    }

    UniqueId InternalUniqueIdManager::GetReplicaId(ISpatialReplica const &replica, UniqueId const &sceneId)
    {
        wstringstream skey;
        BadumnaId id = replica.Guid();

        if(id.IsValid())
        {
            skey << L"replica_uid" << InternalUniqueIdManager::separator_char;
            skey << id.ToInvariantIDString().CStr() << InternalUniqueIdManager::separator_char;
            skey << sceneId.CStr();

            return skey.str().c_str();
        }
        else
        {
            return String();
        }
    }

    UniqueId InternalUniqueIdManager::GetOriginalId(ISpatialOriginal const &original)
    {
        wstringstream skey;
        BadumnaId id = original.Guid();

        if(id.IsValid())
        {
            skey << L"original_uid" << InternalUniqueIdManager::separator_char << id.ToInvariantIDString().CStr();
            return skey.str().c_str();
        }
        else
        {
            return String();
        }
    }

    UniqueId InternalUniqueIdManager::GetChatSessionId()
    {
        static int id = 0;
        wstringstream skey;
        skey << L"chat_session_uid" << InternalUniqueIdManager::separator_char;
        skey << id;
        id++;

        return skey.str().c_str();
    }

    UniqueId InternalUniqueIdManager::GetArbitratorId(String const &name)
    {
        wstringstream skey;
        skey << L"arbitrator_uid_" << name.CStr() << std::endl;

        return skey.str().c_str();
    }

    UniqueId InternalUniqueIdManager::GetStreamingCompleteDelegateId(
        String const &tag_name, 
        String const &stream_name, 
        BadumnaId const &destination)
    {
        wstringstream skey;

        skey << "streaming_cd_uid_" << tag_name.CStr() << InternalUniqueIdManager::separator_char;
        skey << stream_name.CStr() << InternalUniqueIdManager::separator_char;
        skey << destination.ToInvariantIDString().CStr() << std::endl;

        return skey.str().c_str();
    }

    UniqueId InternalUniqueIdManager::GetStreamingReceiveCompleteDelegateId(
        String const &tag_name,
        String const &stream_name,
        String const &username)
     {
         wstringstream skey;

         skey << "streaming_rcd_uid_" << tag_name.CStr() << InternalUniqueIdManager::separator_char;
         skey << stream_name.CStr() << InternalUniqueIdManager::separator_char;
         skey << username.CStr() << std::endl;

         return skey.str().c_str();
     }

    UniqueId InternalUniqueIdManager::GetStreamingRequestDelegateId(String const &tag_name)
    {
        wstringstream skey;
        
        skey << "streaming_rd_uid_" << tag_name.CStr() << std::endl;
        
        return skey.str().c_str(); 
    }
}
