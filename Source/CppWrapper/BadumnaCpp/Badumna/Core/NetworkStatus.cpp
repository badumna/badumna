//---------------------------------------------------------------------------------
// <copyright file="NetworkStatus.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include <iostream>
#include <sstream>

#include "Badumna/Core/NetworkStatus.h"
#include "Badumna/Core/NetworkStatusImpl.h"

namespace Badumna
{
    NetworkStatus::NetworkStatus(NetworkStatusImpl* impl) 
        : impl_(impl)
    {
        assert(impl != NULL);
    }

    NetworkStatus::~NetworkStatus()
    {
    }

    String NetworkStatus::ToString() const
    {
        return impl_->ToString();
    }

    String NetworkStatus::GetPublicAddress() const
    {
        return impl_->GetPublicAddress();
    }

    String NetworkStatus::GetPrivateAddress() const
    {
        return impl_->GetPrivateAddress();
    }

    int32_t NetworkStatus::GetActiveConnectionCount() const
    {
        return impl_->GetActiveConnectionCount();
    }

    int32_t NetworkStatus::GetInitializingConnectionCount() const
    {
        return impl_->GetInitializingConnectionCount();
    }

    String NetworkStatus::GetDiscoveryMethod() const
    {
        return impl_->GetDiscoveryMethod();
    }

    bool NetworkStatus::GetPortForwardingEnabled() const
    {
        return impl_->GetPortForwardingEnabled();
    }

    bool NetworkStatus::GetPortForwardingSucceeded() const
    {
        return impl_->GetPortForwardingSucceeded();
    }

    int32_t NetworkStatus::GetLocalObjectCount() const
    {
        return impl_->GetLocalObjectCount();
    }

    int32_t NetworkStatus::GetRemoteObjectCount() const
    {
        return impl_->GetRemoteObjectCount();
    }

    int64_t NetworkStatus::GetTotalBytesSentPerSecond() const
    {
        return impl_->GetTotalBytesSentPerSecond();
    }

    int64_t NetworkStatus::GetTotalBytesReceivedPerSecond() const
    {
        return impl_->GetTotalBytesReceivedPerSecond();
    }

    std::wostream& operator<<(std::wostream& out, NetworkStatus const& status)
    {
        std::wstring ws(status.ToString().CStr());
        out << ws;
        return out;
    }

    DEFINE_WRAPPER_CLASS_COPYCTOR_ASSIGN_OPERATOR(NetworkStatus,NetworkStatusImpl, impl_, other)
}