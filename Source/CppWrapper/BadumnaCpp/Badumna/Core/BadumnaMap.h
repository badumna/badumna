//---------------------------------------------------------------------------------
// <copyright file="BadumnaMap.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_BADUMNA_MAP_H
#define BADUMNA_BADUMNA_MAP_H

#include <cstdlib>
#include <iostream>
#include <map>

#include "Badumna/Core/InternalUniqueId.h"
#include "Badumna/Core/InternalErrorHandler.h"
#include "Badumna/Core/InternalErrorType.h"
#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/Utils/Optional.h"

namespace Badumna
{
    /**
     * BadumnaMap is a wrapper class for the std::map. It adds the feature to allow delegates added to the map to be 
     * invoked by name (i.e. the key). 
     */
    template<typename VT, typename KeyT = UniqueId>
    class BadumnaMap
    {
    public:
        typedef typename std::map<KeyT, VT>::iterator Iterator;
        typedef typename std::map<KeyT, VT>::size_type SizeType;
        typedef typename std::map<KeyT, VT>::const_iterator ConstIterator;

        BadumnaMap()
            : map_container_()
        { 
        }

        void TryAdd(KeyT const &key, VT value);
        void AddOrReplace(KeyT const &key, VT value);
        void TryRemove(KeyT const &key);
        bool Remove(KeyT const &key);
        bool Contains(KeyT const &key) const;
        VT GetValue(KeyT const &key) const;
        Optional<VT> TryGetValue(KeyT const &key) const;
        
        void InvokeDelegate(KeyT const &key) const;

        template<typename P1>
        void InvokeDelegate(KeyT const &key, P1 p1) const;

        template<typename P1, typename P2>
        void InvokeDelegate(KeyT const &key, P1 p1, P2 p2) const;

        template<typename P1, typename P2, typename P3>
        void InvokeDelegate(KeyT const &key, P1 p1, P2 p2, P3 p3) const;

        template<typename P1, typename P2, typename P3, typename P4>
        void InvokeDelegate(KeyT const &key, P1 p1, P2 p2, P3 p3, P4 p4) const;

        SizeType Size() const
        {
            return map_container_.size();
        }

        Iterator Begin()
        {
            return map_container_.begin();
        }

        ConstIterator Begin() const
        {
            return map_container_.begin();
        }

        Iterator End()
        {
            return map_container_.end();
        }

        ConstIterator End() const
        {
            return map_container_.end();
        }

    private:
        std::map<KeyT, VT> map_container_;

        DISALLOW_COPY_AND_ASSIGN(BadumnaMap);
    };

    template<typename VT, typename KeyT>
    void BadumnaMap<VT, KeyT>::TryAdd(KeyT const &key, VT value)
    {
        typename std::map<KeyT, VT>::iterator iter = map_container_.find(key);
        if(iter == map_container_.end())
        {
            map_container_.insert(std::make_pair(key, value));
        }
    }

    template<typename VT, typename KeyT>
    void BadumnaMap<VT, KeyT>::AddOrReplace(KeyT const &key, VT value)
    {
        typename std::map<KeyT, VT>::iterator iter = map_container_.find(key);
        if(iter != map_container_.end())
        {
            map_container_.erase(iter);
        }

        map_container_.insert(std::make_pair(key, value));
    }

    template<typename VT, typename KeyT>
    void BadumnaMap<VT, KeyT>::TryRemove(KeyT const &key)
    {
        typename std::map<KeyT, VT>::iterator iter = map_container_.find(key);
        if(iter != map_container_.end())
        {
            map_container_.erase(iter);
        }
    }

    template<typename VT, typename KeyT>
    bool BadumnaMap<VT, KeyT>::Remove(KeyT const &key)
    {
        typename std::map<KeyT, VT>::iterator iter = map_container_.find(key);
        if(iter != map_container_.end())
        {
            map_container_.erase(iter);
            return true;
        }

        return false;
    }
    
    template<typename VT, typename KeyT>
    bool BadumnaMap<VT, KeyT>::Contains(KeyT const &key) const
    {
        typename std::map<KeyT, VT>::const_iterator iter = map_container_.find(key);
        if(iter != map_container_.end())
        {
            return true;
        }

        return false;
    }

    template<typename VT, typename KeyT>
    VT BadumnaMap<VT, KeyT>::GetValue(KeyT const &key) const
    {
        typename std::map<KeyT, VT>::const_iterator iter = map_container_.find(key);
        if(iter == map_container_.end())
        {
            InternalErrorHandler::Instance().Invoke(
                InternalErrorType_BadumnaMapInconsistency, 
                L"BadumnaMap item couldn't be located.");
        }
    
        return iter->second;
    }

    template<typename VT, typename KeyT>
    Optional<VT> BadumnaMap<VT, KeyT>::TryGetValue(KeyT const &key) const
    {
        typename std::map<KeyT, VT>::const_iterator iter = map_container_.find(key);
        if(iter == map_container_.end())
        {
            return Optional<VT>();
        }
    
        return Optional<VT>(iter->second);
    }

    template<typename VT, typename KeyT>
    void BadumnaMap<VT, KeyT>::InvokeDelegate(KeyT const &key) const
    {
        VT target_delegate = GetValue(key);
        target_delegate();
    }

    template<typename VT, typename KeyT>
    template<typename P1>
    void BadumnaMap<VT, KeyT>::InvokeDelegate(KeyT const &key, P1 p1) const
    {
        VT target_delegate = GetValue(key);
        target_delegate(p1);
    }

    template<typename VT, typename KeyT>
    template<typename P1, typename P2>
    void BadumnaMap<VT, KeyT>::InvokeDelegate(KeyT const &key, P1 p1, P2 p2) const
    {
        VT target_delegate = GetValue(key);
        target_delegate(p1, p2);
    }

    template<typename VT, typename KeyT>
    template<typename P1, typename P2, typename P3>
    void BadumnaMap<VT, KeyT>::InvokeDelegate(KeyT const &key, P1 p1, P2 p2, P3 p3) const
    {
        VT target_delegate = GetValue(key);
        target_delegate(p1, p2, p3);
    }

    template<typename VT, typename KeyT>
    template<typename P1, typename P2, typename P3, typename P4>
    void BadumnaMap<VT, KeyT>::InvokeDelegate(KeyT const &key, P1 p1, P2 p2, P3 p3, P4 p4) const
    {
        VT target_delegate = GetValue(key);
        target_delegate(p1, p2, p3, p4);
    }
}

#endif // BADUMNA_BADUMNA_MAP_H