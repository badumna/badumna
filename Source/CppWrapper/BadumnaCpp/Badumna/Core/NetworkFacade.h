//---------------------------------------------------------------------------------
// <copyright file="NetworkFacade.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_NETWORK_FACADE_H
#define BADUMNA_NETWORK_FACADE_H

#include <memory>

#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/Core/NetworkStatus.h"
#include "Badumna/Core/StatisticsTracker.h"
#include "Badumna/Core/ConnectivityStatusDelegate.h"
#include "Badumna/Controller/CreateControllerDelegate.h"
#include "Badumna/Chat/IChatSession.h"
#include "Badumna/Security/Character.h"
#include "Badumna/Configuration/Options.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Dei/IdentityProvider.h"
#include "Badumna/Arbitration/IArbitrator.h"
#include "Badumna/Replication/SpatialReplicaDelegate.h"
#include "Badumna/Replication/IDeadReckonableSpatialOriginal.h"
#include "Badumna/Replication/IDeadReckonableSpatialReplica.h"
#include "Badumna/Streaming/StreamingManager.h"
#include "Badumna/Match/MatchFacade.h"

/**
 * @namespace Badumna
 *
 * @brief The Badumna namespace contains all Badumna Cpp classes.
 */
namespace Badumna
{
    class NetworkFacadeImpl;

    /**
     * @enum ServerType
     *
     * Server types.
     */
    enum ServerType
    {
        ServerType_ArbitrationServer = 1,           /**< Arbitration server */
        ServerType_OverloadServer,                  /**< Overload server */
    };

    /**
     * @class NetworkFacade
     * @brief A facade interface to the Badumna engine. 
     *
     * An interface to the Badumna engine.
     */
    class BADUMNA_API NetworkFacade
    {
    public:
        /**
         * Destructor
         */
        ~NetworkFacade();

        /**
         * Creates a NetworkFacade using the given options. Only one NetworkFacade can exist at any time. Before creating
         * a new NetworkFacade the previous one (if any) must be shutdown.
         * The contents of the options parameter must not be changed after calling this method. The effect of changing the 
         * options after creating the facade is undefined. The caller has the ownership of the returned network facade
         * object.
         *
         * @param options The options used to create the network facade object.
         * @return The new NetworkFacade instance.
         */
        static NetworkFacade *Create(Options const &options);

        /*
         * Creates a NetworkFacade configured for the given cloud identifier. Only one NetworkFacade can exist at any time.
         * Before creating a new NetworkFacade the previous one (if any) must be shutdown.
         * The caller has the ownership of the returned network facade object.
         *
         * @param identifier The identifier of the cloud network to connect to.
         * @return The new NetworkFacade instance.
         */
        static NetworkFacade *Create(Badumna::String const &identifier);

        /**
         * Announces the service specified description. This method should only be called by hosted services such as on 
         * Overload/Arbitration servers. Game client should never call this method.
         *
         * @param type The server type to announce.
         */ 
        void AnnounceService(ServerType type) const;

        /**
         * Performs login-time initializations, and indicates that no authorization system is being used.
         *
         * Note that this override does not take any character details, so the resulting session
         * will not be able to use character-based APIs (such as the Chat module).
         *
         * @return A boolean value indicating whether the login succeed or not.
         */
        bool Login();

        /**
         * Performs login-time initializations, and indicates that no authorization system is being used.
         *
         * Specifies a character name to be used by character-based APIs (e.g chat).
         * Note that you need to use the overload of this method that takes an IdentityProvider
         * if you wish to ensure that the character name is unique and belongs to the active user.
         *
         * @return A boolean value indicating whether the login succeed or not.
         */
        bool Login(Badumna::String const &character_name);

        /**
         * Logs in to the network, specifing a delegate that supplies authorization tokens.  
         * Specifies a supplier object for the system to retrieve authorization tokens and performs login-time 
         * initializations.
         * 
         * @param identity_provider An implementation of an identity provider that will return any required authorization 
         * tokens. Calls to the identity provider may be made from a different thread to that which called Login. The 
         * provider must be usable until Shutdown is called.
         * @return A boolean value indicating whether the login succeed or not.
         */
        bool Login(Dei::IdentityProvider const &identity_provider);
        
        /**
         * Shutdown. 
         */
        void Shutdown(); 

        /**
         * Performs any regular processing in Badumna that requires synchronisation with the application.
         * This function must be called regularly by the application so that network events requiring sychronisation can
         * be processed (e.g. it might be called once per frame rendered). As a rule Badumna will not call application 
         * code asynchronously. Particularly, all calls to methods or properties on Badumna interfaces implemented by 
         * the application will be made from within <code>ProcessNetworkState()</code>. Calls to delegates registered 
         * with Badumna will also only be made from ProcessNetworkState(). 
         */
        void ProcessNetworkState();

        /**
         * Gets the current network status.
         */
        NetworkStatus GetNetworkStatus() const;

        /** 
         * Joins a scene. The caller has the onwership of the returned network scene object. 
         * 
         * The create and remove entity delegates are called upon the arrival of a new entity and the departure of an 
         * old entity respectively.  Only entities which are in the same scene and in proximity to a locally registered 
         * entity will be passed to these delegates.
         *
         * @param scene_name The unique name identifying the scene.
         * @param create_delegate The delegate called when a new entity needs to be instantiated into the scene.
         * @param remove_delegate The delegate called when an entity in the scene departs.
         * @return A pointer to an instance of NetworkScene representing the scene. The caller has the onwership of the
         * returned object.
         */
        NetworkScene *JoinScene(
            String const &scene_name, 
            CreateSpatialReplicaDelegate create_delegate, 
            RemoveSpatialReplicaDelegate remove_delegate);

        /** 
         * Joins a mini scene. Only used this for mini/casual type of game
         * The caller has the onwership of the returned network scene object. 
         *
         * The create and remove entity delegates are called upon the arrival of a new entity and the departure of an 
         * old entity respectively.  Only entities which are in the same scene and in proximity to a locally registered 
         * entity will be passed to these delegates.
         *
         * @param scene_name The unique name identifying the scene.
         * @param create_delegate The delegate called when a new entity needs to be instantiated into the scene.
         * @param remove_delegate The delegate called when an entity in the scene departs.
         * @return A pointer to an instance of NetworkScene representing the scene. The caller has the onwership of the
         * returned object.
         */
        NetworkScene *JoinMiniScene(
            String const &scene_name, 
            CreateSpatialReplicaDelegate create_delegate, 
            RemoveSpatialReplicaDelegate remove_delegate);

        /**
         * Indicates that the given entity has state changes that need to be propagated to interested peers.
         *
         * @param local_entity The entity with changed state.
         * @param changed_parts A BooleanArray with bits set indicating which parts have changed.
         */
        void FlagForUpdate(ISpatialOriginal const &local_entity, BooleanArray const &changed_parts) const;
        
        /**
         * Indicates that the given entity has a state change that need to be propagated to interested peers.
         * 
         * @param local_entity The entity with changed state.
         * @param changed_part_index The index of the part of state that has changed.
         */
        void FlagForUpdate(ISpatialOriginal const &local_entity, int changed_part_index) const;

        /**
         * Sends a single message directly to all remote replicas of the specified entity.
         * Events are intended to be used for state changes on entities that are infrequent so they are sent reliably 
         * and are guaranteed to be applied in the same order as they are sent. When the message arrives the remote 
         * ISpatialReplica.HandleEvent() method will be called with the given arguments.
         *
         * @param local_entity The entity whose replicas should receive the event.
         * @param event_data The event specific data to send.
         */
        void SendCustomMessageToRemoteCopies(ISpatialOriginal const &local_entity, OutputStream *event_data) const;
        
        /**
         * Sends a single message directly to the controlling instance of this entity.
         * Events are intended to be used for state changes on entities that are infrequent so they are sent reliably 
         * and are guaranteed to be applied in the same order as they are sent. When the message arrives the remote 
         * ISpatialOriginal.HandleEvent() method will be called with the given arguments.
         * 
         * @param remote_entity The entity whose controlling instance should receive the event.
         * @param event_data The event specific data to send.
         */
        void SendCustomMessageToOriginal(ISpatialReplica const &remote_entity, OutputStream *event_data) const;

        /**
         * Registers limitations on replicable entities so Badumna can come up with optimized setting for the game environment.
         * Cannot be called after joining a scene.
         *
         * @remark Can be called multiple times, and system will use maximum values passed.
         *
         * @param area_of_interest_radius The largest area of interest radius of any replicable entity (or entity radius if that is larger).
         * @param max_speed >The maximum speed any replicable entity will move at.
         */
        void RegisterEntityDetails(float area_of_interest_radius, float max_speed);

        /** 
         * Gets the chat session instance. The network facade has ownership of the returned IChatSession object
         * - you should not delete it.
         * 
         * @return A pointer to an IChatSession instance.
         */ 
        IChatSession *ChatSession() const;

        /**
         * Gets the logged-in Character.
         *
         * @return An auto_ptr to a Character instance, or to null if no character is logged in.
         */
        std::auto_ptr<Character> GetCharacter() const;

        /**
         * Creates a StatisticsTracker which periodically sends packets to a tracking server to estimate the number of 
         * current users on the network. The packets sent are unreliable and may be dropped by firewalls, or due to 
         * congestion, or for a number of other reasons.  As such the tracking server can only provide an estimate of 
         * the active user count. The caller has the ownership of the returned object.
         *
         * @param server_address The address of the server.
         * @param server_port The server_port of the server.
         * @param interval_seconds The interval between sending packets in seconds.
         * @param initial_payload The The initial payload data.
         * @return A pointer to the created statistics tracker object. The caller has the ownership of the returned object.
         */
        StatisticsTracker *CreateTracker(
            String const &server_address, 
            int server_port,
            int interval_seconds, 
            String const &initial_payload) const;

        /**
         * Get an IArbitrator instance that can be used to send arbitration events to the aribtration server identified 
         * by the parameter name. Used by arbitration clients. The caller has the ownership of the returned IArbitrator 
         * object.
         *  
         * @param name The arbitration server name.
         * @return A pointer to an IArbitrator instance. The caller has the ownership of the returned object.
         */
        IArbitrator *GetArbitrator(String const &name) const;

        /**
         * Registers the handler that will be called when this peer receives an arbitration event from another peer. 
         * This is used by the arbitration server only.
         *
         * @param client_message_delegate The delegate called on receiving an arbitration message from the client. 
         * @param timeout_seconds A client session is considered as timeout after being idle for more than timeout_seconds
         * seconds.
         * @param client_disconnect_delegate The delegate called on client disconnection.
         */
        void RegisterArbitrationHandler(
            ArbitrationClientMessageDelegate client_message_delegate, 
            int timeout_seconds, 
            ArbitrationClientDisconnectDelegate client_disconnect_delegate);

        /**
         * Sends an event from the arbitration server to the client identified by destinationUserId. This user Id must 
         * already be known to the arbitration server (i.e. the client must always initiate any arbitration session). 
         * This method is used by arbitration server only.
         *
         * @param destination_session_id The destination session id.
         * @param stream The message to send.
         */
        void SendServerArbitrationEvent(int32_t destination_session_id, OutputStream *stream) const;

        /**
         * Gets the user Id associated with the given session_id. This is only valid on a arbitration server peer 
         * (arbitration clients only have one arbitration session per arbitration server). If the session_id is unknown 
         * this function returns -1.
         *
         * @param session_id The session id.
         * @return The user id or -1 on error. 
         */
        int64_t GetUserIdForSession(int32_t session_id) const;

        /**
         * Gets a BadumnaId identifying the peer associated with the given session_id.
         * This is only valid on an arbitration server peer (arbitration clients only have one arbitration session
         * per arbitration server).  If the session_id is unknown this function returns BadumnaId.None().
         *
         * @param session_id The session id, as passed to the registered ArbitrationClientMessageDelegate.
         * @return A BadumnaId identifying the peer, or BadumnaId.None() if the session was not found.
         */
        BadumnaId GetBadumnaIdForArbitrationSession(int32_t session_id) const;

        /**
         * Gets the current Character of the peer associated with the given session_id.
         * This is only valid on an arbitration server peer (arbitration clients only have one arbitration session
         * per arbitration server).  If the session_id is unknown or the peer does not have a character,
         * this function returns NULL.
         *
         * @param session_id The session id, as passed to the registered ArbitrationClientMessageDelegate.
         * @return The Character identifying the peer, or NULL if the session was not found.
         */
        std::auto_ptr<Badumna::Character> GetCharacterForArbitrationSession(int32_t session_id) const;

        /**
         * Gets the streaming manager that can be used to do data streaming. The caller has the ownership of the 
         * returned StreamManager object.
         *
         * @return A pointer to a StreamManager instance. The caller has the ownership of the returned object.
         */
        StreamingManager *GetStreamingManager() const;

        /**
         * Gets the Match facade.
         *
         * @return A MatchFacade instance.
         */
        std::auto_ptr<Badumna::MatchFacade> GetMatchFacade() const;

        /**
         * Gets a boolean value indicating whether the connection to the network is being tunnelled over HTTP.
         *
         * @return A boolean value indicating whether the connection to the network is being tunnelled over HTTP.
         */
        bool IsTunnelled() const;

        /**
         * Gets a boolean value indicating whether the local peer has logged in.
         *
         * @return A boolean value indicating whether logged in
         */
        bool IsLoggedIn() const;

        /**
         * Gets a boolean value indicating whether the local peer is fully connected to the network.
         *
         * @return A boolean value indicating whether fully connected to the network.
         */
        bool IsFullyConnected() const;

        /**
         * Gets a boolean value indicating whether the local peer if offline.
         *
         * @return A boolean value indicating whether offline.
         */
        bool IsOffline() const;

        /**
         * Gets a boolean value indicating whether the local peer is online.
         *
         * @return A boolean value indicating whether online.
         */
        bool IsOnline() const;

        /**
         * Gets the outbound bytes per seconds.
         * 
         * @return The outbound bytes per second.
         */
        double GetOutboundBytesPerSecond() const;

        /**
         * Gets the inbound bytes per seconds.
         * 
         * @return The inbound bytes per second.
         */
        double GetInboundBytesPerSecond() const;

        /**
         * Gets the maximum packet loss rate.
         * 
         * @return The maximum packet loss rate.
         */
        double GetMaximumPacketLossRate() const;

        /**
         * Gets the average packet loss rate.
         * 
         * @return The average packet loss rate.
         */
        double GetAveragePacketLossRate() const;

        /**
         * Gets the total send limit in bytes per second. 
         *
         * @return The total send limit in bytes per second.
         */ 
        double GetTotalSendLimitBytesPerSecond() const;

        /**
         * Gets the maximum send limit bytes per second.
         *
         * @return The maximum send limit bytes per second.
         */ 
        double GetMaximumSendLimitBytesPerSecond() const;

        /**
         * Registers the delegate that will be called when the peer becomes online.
         *
         * @param online_delegate The delegate to be registered.
         */
        void SetOnlineEventDelegate(ConnectivityStatusDelegate online_delegate);

        /**
         * Registers the delegate that will be called when the peer becomes offline.
         *
         * @param offline_delegate The delegate to be registered.
         */
        void SetOfflineEventDelegate(ConnectivityStatusDelegate offline_delegate);

        /**
         * Registers the delegate that will be called when the address of the peer changes.
         *
         * @param address_change_delegate The delegate to be registered.
         */
        void SetAddressChangedEventDelegate(ConnectivityStatusDelegate address_change_delegate);

        /**
         * Gets the destination position for an IDeadReckonable.
         *
         * This can be used in combination with the IDeadReckonable's current Position and Velocity to determine the 
         * state of the dead reckoning.  With reference to the velocity, if the current position is before the 
         * destination then the dead reckoner is interpolating (smoothing). If the current position is after the 
         * destination then the dead reckoner is extrapolating.
         * 
         * @param remote_entity The eneity.
         * @return The destination position. 
         */
        Vector3 GetDestination(IDeadReckonableSpatialReplica const &remote_entity) const;

        /**
         * Forces the IDeadReckonable's position and velocity to the current destination values and begins extrapolation.
         *
         * @param remote_entity The eneity
         */
        void SnapToDestination(IDeadReckonableSpatialReplica *remote_entity) const;

        /**
         * Registers a controller type. This method should be called before calling <code>Login()</code>. 
         * 
         * @param type_name The controller type name.
         * @param creator The delegate used to create instances of registered controller type. 
         */
        void RegisterController(String const &type_name, CreateControllerDelegate creator);

        /**
         * Starts controller. 
         * 
         * @param scene_name The name of the scene which the controller will join. 
         * @param type_name The type name of the controller. 
         * @param max The max number of instances across the netework. 
         * @return The unique name of the controller. 
         */
        String StartController(String const &scene_name, String const &type_name, uint32_t max);

        /**
         * Stops the controller. This will not immediately stop the controller, it only releases the controller from 
         * local peer's monitoring. 
         * 
         * @param unique_name The unique name of the controller. 
         */
        void StopController(String const &unique_name);

    private:
        NetworkFacade(Options const &options);
        NetworkFacade(Badumna::String const &identifier);
        std::auto_ptr<NetworkFacadeImpl> const impl_;

        DISALLOW_COPY_AND_ASSIGN(NetworkFacade);
    };
}

#endif // BADUMNA_NETWORK_FACADE_H
