//---------------------------------------------------------------------------------
// <copyright file="BadumnaRuntime.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include <iostream>
#include <vector>

#include "Badumna/Core/Arguments.h"
#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/Chat/ChatSessionImpl.h"
#include "Badumna/Chat/ChatChannelIdImpl.h"
#include "Badumna/Chat/ChatSessionManager.h"
#include "Badumna/Core/NetworkStatusImpl.h"
#include "Badumna/Core/NetworkFacadeImpl.h"
#include "Badumna/Core/ThreadGuardImpl.h"
#include "Badumna/Core/StatisticsTrackerImpl.h"
#include "Badumna/Core/TypeRegistry.h"
#include "Badumna/Configuration/OptionsImpl.h"
#include "Badumna/Configuration/OverloadModuleImpl.h"
#include "Badumna/Configuration/ConnectivityModuleImpl.h"
#include "Badumna/Configuration/ArbitrationModuleImpl.h"
#include "Badumna/Configuration/LoggerModuleImpl.h"
#include "Badumna/Configuration/MatchmakingModuleImpl.h"
#include "Badumna/Utils/Logger.h"
#include "Badumna/Controller/DistributedSceneControllerImpl.h"
#include "Badumna/DataTypes/BadumnaIdImpl.h"
#include "Badumna/DataTypes/BooleanArrayImpl.h"
#include "Badumna/Replication/NetworkSceneImpl.h"
#include "Badumna/Arbitration/ArbitrationClientImpl.h"
#include "Badumna/Arbitration/ArbitrationServerImpl.h"
#include "Badumna/Streaming/LocalStreamControllerImpl.h"
#include "Badumna/Streaming/StreamingServiceManager.h"
#include "Badumna/Streaming/StreamingManagerImpl.h"
#include "Badumna/Streaming/StreamRequestImpl.h"
#include "Badumna/Dei/IdentityProviderImpl.h"
#include "Badumna/Dei/SessionImpl.h"
#include "Badumna/Utils/Optional.h"
#include "Badumna/Match/MatchFacadeImpl.h"
#include "Badumna/Match/MatchmakingQueryResultImpl.h"
#include "Badumna/Match/MatchmakingResultImpl.h"

namespace 
{
    using Badumna::String;
    using Badumna::Optional;

    String GetFullNamespace(String const &name_space)
    {
        String full_namespace;
        if(name_space == "Badumna")
        {
            full_namespace = "Badumna";
        }
        else if(name_space == "Dei")
        {
            full_namespace = "Dei";
        }
        else
        {
            full_namespace = "Badumna." + name_space;
        }

        return full_namespace;
    }
}

namespace Badumna
{
    using std::vector;
    using Dei::IdentityProviderImpl;
    using Dei::SessionImpl;

    BadumnaRuntime::BadumnaRuntime()
        : initialized_(false),
        mono_runtime_(),
        badumna_stack_(),
        facade_object_(NULL)
    {
    }

    bool BadumnaRuntime::Initialize(MonoRuntimeConfig *config)
    {
        assert(!initialized_);

        bool ret = mono_runtime_.Initialize(config);
        initialized_ = true;
        return ret;
    }

    void BadumnaRuntime::Shutdown()
    {
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        BADUMNA_LOG_DEBUG("BadumnaRuntime::Shutdown()");
        badumna_stack_.Shutdown();
        mono_runtime_.Shutdown();
        initialized_ = false;
    }

    /*******************************************************************************
     * Verify
     *******************************************************************************/
    bool BadumnaRuntime::Verify(TypeRegistry const &registry) const
    {
        vector<ClassDetails> class_details = registry.GetClassDetails();
        // registered types
        for(vector<ClassDetails>::const_iterator iter = class_details.begin();
            iter != class_details.end(); ++iter)
        {
            String full_namespace = ::GetFullNamespace(iter->name_space);
            if(!mono_runtime_.GetClass2(full_namespace.UTF8CStr(), iter->type_name.UTF8CStr()))
            {
                std::cerr << "Type " << iter->type_name.UTF8CStr() << " is missing" << std::endl;
                return false;
            }
        }

        // registered methods
        vector<MethodDetails> method_details = registry.GetMethodDetails();
        for(vector<MethodDetails>::const_iterator iter = method_details.begin();
            iter != method_details.end(); ++iter)
        {
            String full_namespace = ::GetFullNamespace(iter->name_space);
            if(!mono_runtime_.MethodAvailable(
                    full_namespace.UTF8CStr(), 
                    iter->type_name.UTF8CStr(), 
                    iter->method_name.UTF8CStr(), 
                    iter->count))
            {
                std::cerr << "Method " << iter->method_name.UTF8CStr() << " is missing" << std::endl;
                return false;
            }
        }

        // registered properties
        vector<PropertyDetails> property_details = registry.GetPropertyDetails();
        for(vector<PropertyDetails>::const_iterator iter = property_details.begin();
            iter != property_details.end(); ++iter)
        {
            String full_namespace = ::GetFullNamespace(iter->name_space);
            if(!mono_runtime_.PropertyAvailable(
                    full_namespace.UTF8CStr(), 
                    iter->type_name.UTF8CStr(), 
                    iter->pname.UTF8CStr()))
            {
                std::cerr << "Property " << iter->pname.UTF8CStr() << " on " << iter->type_name.UTF8CStr() << " is missing" << std::endl;
                return false;
            }
        }

        return true;
    }

    /*******************************************************************************
     * BadumnaIdImpl
     *******************************************************************************/
    BadumnaId BadumnaRuntime::CreateBadumnaId(DotNetObject object)
    {
        assert(object != NULL);
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        return BadumnaId(CreateBadumnaIdImpl(object));
    }
    
    BadumnaId BadumnaRuntime::CreateBadumnaId(String const &id)
    {
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        DotNetString str = MonoRuntime::GetString(id);
        Arguments args;
        args.AddArgument(str);
        DotNetClass klass = mono_runtime_.GetClass("Badumna.DataTypes", "BadumnaId");
        DotNetObject object = mono_runtime_.InvokeMethod(klass, NULL, "TryParse", args, NULL);
        return BadumnaId(CreateBadumnaIdImpl(object));
    }

    BadumnaIdImpl *BadumnaRuntime::CreateBadumnaIdImpl(DotNetObject object)
    {
        assert(object != NULL);
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        return new BadumnaIdImpl(&mono_runtime_, object);
    }

    BadumnaIdImpl *BadumnaRuntime::CreateBadumnaIdImpl(
        DotNetObject object, 
        String const &name_space, 
        String const &class_name)
    {
        assert(object != NULL);
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        return new BadumnaIdImpl(&mono_runtime_, object, name_space.UTF8CStr(), class_name.UTF8CStr());
    }

    BadumnaIdImpl *BadumnaRuntime::CreateBadumnaIdImpl()
    {
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        DotNetObject object = mono_runtime_.GetPropertyFromName(
            "Badumna.DataTypes", 
            "BadumnaId", 
            NULL, 
            "None", 
            Arguments::None(), 
            NULL);
        
        assert(object != NULL);
        return CreateBadumnaIdImpl(object);
    }

    /*******************************************************************************
     * ChatChannelIdImpl
     *******************************************************************************/
    ChatChannelId BadumnaRuntime::CreateChatChannelId(DotNetObject object)
    {
        assert(object != NULL);
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        return ChatChannelId(CreateChatChannelIdImpl(object));
    }

    ChatChannelIdImpl *BadumnaRuntime::CreateChatChannelIdImpl(DotNetObject object)
    {
        assert(object != NULL);
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        return new ChatChannelIdImpl(&mono_runtime_, object);
    }

    // NOTE: only used for testing
    ChatChannelId BadumnaRuntime::CreateChatChannelId(ChatChannelType type, BadumnaId id)
    {
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");

        Arguments args;
        int *typep = new int(type);
        args.AddArgument(typep);
        args.AddArgument(id.impl_->GetManagedMonoObject());
        DotNetClass klass = mono_runtime_.GetClass("Badumna.CppWrapperStub", "ChatChannelIdStub");
        DotNetObject channelId = mono_runtime_.InvokeMethod(klass, NULL, "CreateChatChannelId", args, NULL);
        ChatChannelIdImpl *channelImpl = new ChatChannelIdImpl(&mono_runtime_, channelId);
        return ChatChannelId(channelImpl);
    }

    /*******************************************************************************
     * BooleanArrayImpl
     *******************************************************************************/
    BooleanArray BadumnaRuntime::CreateBooleanArray(DotNetObject object)
    {
        assert(object != NULL);
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        return BooleanArray(CreateBooleanArrayImpl(object));
    }

    BooleanArrayImpl *BadumnaRuntime::CreateBooleanArrayImpl()
    {
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        return new BooleanArrayImpl(&mono_runtime_);
    }
    
    BooleanArrayImpl *BadumnaRuntime::CreateBooleanArrayImpl(DotNetObject object)
    {
        assert(object != NULL);
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        return new BooleanArrayImpl(&mono_runtime_, object);
    }
    
    BooleanArrayImpl *BadumnaRuntime::CreateBooleanArrayImpl(bool value)
    {
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        return new BooleanArrayImpl(&mono_runtime_, value);
    }
    
    BooleanArrayImpl *BadumnaRuntime::CreateBooleanArrayImpl(int const *indexes_set_to_true, size_t length)
    {
        assert(indexes_set_to_true != NULL);
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        return new BooleanArrayImpl(&mono_runtime_, indexes_set_to_true, length);
    }

    BooleanArrayImpl *BadumnaRuntime::CreateBooleanArrayImpl(String const &bits)
    {
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        DotNetString str = MonoRuntime::GetString(bits);
        Arguments args;
        args.AddArgument(str);
        
        DotNetClass klass = mono_runtime_.GetClass("Badumna.Utilities", "BooleanArray");
        DotNetObject object = mono_runtime_.InvokeMethod(klass, NULL, "TryParse", args, NULL);
        return CreateBooleanArrayImpl(object);
    }

    /*******************************************************************************
     * NetworkStatusImpl
     *******************************************************************************/
    NetworkStatusImpl *BadumnaRuntime::CreateNetworkStatusImpl(DotNetObject object)
    {
        assert(object != NULL);
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        return new NetworkStatusImpl(&mono_runtime_, object);
    }

    /*******************************************************************************
     * NetworkFacadeImpl
     *******************************************************************************/
    NetworkFacadeImpl *BadumnaRuntime::CreateNetworkFacadeImpl(Options const &options)
    {
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        NetworkFacadeImpl* facade_impl = new NetworkFacadeImpl(&mono_runtime_, &badumna_stack_, &options, NULL);
        facade_object_ = facade_impl->managed_object_.InvokeMethod("GetFacade");

        return facade_impl;
    }

    NetworkFacadeImpl *BadumnaRuntime::CreateNetworkFacadeImpl(Badumna::String const &identifier)
    {
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        NetworkFacadeImpl* facade_impl = new NetworkFacadeImpl(&mono_runtime_, &badumna_stack_, NULL, &identifier);
        facade_object_ = facade_impl->managed_object_.InvokeMethod("GetFacade");

        return facade_impl;
    }

    /*******************************************************************************
     * NetworkSceneImpl
     *******************************************************************************/
    NetworkSceneImpl *BadumnaRuntime::CreateNetworkSceneImpl(
            DotNetObject object, 
            String const &unique_scene_id,
            EntityManager *entity_manager,
            ReplicationServiceManager *replication_manager,
            NetworkFacadeImpl *network_facade_impl)
    {
        assert(object != NULL);
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        return new NetworkSceneImpl(
            &mono_runtime_, 
            object, 
            unique_scene_id, 
            entity_manager, 
            replication_manager, 
            network_facade_impl);
    }

    NetworkScene *BadumnaRuntime::CreateNetworkScene(
        DotNetObject object, 
        EntityManager *entity_manager,
        ReplicationServiceManager *replication_manager)
    {
        assert(object != NULL);
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        // called in mono callback, we doesn't have information about the unique id and the facade object. 
        return new NetworkScene(CreateNetworkSceneImpl(object, String(), entity_manager, replication_manager, NULL));
    }

    /*******************************************************************************
     * ChatSessionImpl
     *******************************************************************************/
    ChatSessionImpl *BadumnaRuntime::CreateChatSessionImpl(
        ChatSessionManager * manager,
        String const &id, 
        DotNetObject object)
    {
        assert(object != NULL);
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        return new ChatSessionImpl(&mono_runtime_, manager, object, id);
    }

    /*******************************************************************************
     * ChatChannelImpl
     *******************************************************************************/
    ChatChannelImpl *BadumnaRuntime::CreateChatChannelImpl(
        DotNetObject object,
        ChatMessageHandler message_handler,
        ChatPresenceHandler presence_handler)
    {
        assert(object != NULL);
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        return new ChatChannelImpl(&mono_runtime_, object,
            Optional<ChatMessageHandler>(message_handler),
            Optional<ChatPresenceHandler>(presence_handler));
    }

    ChatChannelImpl *BadumnaRuntime::CreateChatChannelImpl(
        DotNetObject object,
        ChatMessageHandler message_handler)
    {
        assert(object != NULL);
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        return new ChatChannelImpl(&mono_runtime_, object,
            Optional<ChatMessageHandler>(message_handler),
            Optional<ChatPresenceHandler>());
    }

    /*******************************************************************************
     * ReplicaWrapper/OriginalWrapper
     *******************************************************************************/
    ReplicaWrapper *BadumnaRuntime::CreateReplicaWrapper(String const &replica_id, ISpatialReplica *replica)
    {
        assert(replica != NULL);
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        DotNetObject replica_stub;

        if(!replica->IsDeadReckonable())
        {       
            replica_stub = mono_runtime_.GetStubObject(
                "Badumna.CppWrapperStub", 
                "SpatialReplicaStub", 
                replica_id, 
                facade_object_);
        }
        else
        {
            replica_stub = mono_runtime_.GetStubObject(
                "Badumna.CppWrapperStub", 
                "DeadReckonableReplicaStub", 
                replica_id,
                facade_object_);
        }

        return new ReplicaWrapper(&mono_runtime_, replica_stub, replica);
    }

    OriginalWrapper *BadumnaRuntime::CreateOriginalWrapper(ISpatialOriginal *original)
    {
        assert(original != NULL);
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        DotNetObject original_stub;
        
        if(!original->IsDeadReckonable())
        {
            // regular spatial original
            original_stub = mono_runtime_.GetStubObject(
                "Badumna.CppWrapperStub", 
                "SpatialOriginalStub", 
                facade_object_);
        }
        else
        {
            // IDeadReckonable original
            original_stub = mono_runtime_.GetStubObject(
                "Badumna.CppWrapperStub", 
                "DeadReckonableOriginalStub", 
                facade_object_);
        }

        // synchronize the Guid
        if(original->Guid().IsValid())
        {
            // the entity has valid guid, this means it has been registered before. need to ensure the managed stub
            // will have the same guid. 
            Arguments args;
            args.AddArgument(original->Guid().impl_->GetManagedMonoObject());

            DotNetClass klass = mono_runtime_.GetClass("Badumna.CppWrapperStub", "SpatialOriginalStub");
            mono_runtime_.SetPropertyValue(klass, original_stub, "Guid", args, NULL);
        }

        return new OriginalWrapper(&mono_runtime_, original_stub, original);
    }

    /*******************************************************************************
     * ArbitrationClient
     *******************************************************************************/
    ArbitrationClientImpl *BadumnaRuntime::CreateArbitrationClientImpl(
        String const &id, 
        DotNetObject object, 
        ArbitrationServiceManager *manager)
    {
        assert(object != NULL);
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        return new ArbitrationClientImpl(&mono_runtime_, id, object, manager);
    }

    /*******************************************************************************
     * ArbitrationServer
     *******************************************************************************/
    ArbitrationServerImpl *BadumnaRuntime::CreateArbitrationServerImpl(
        String const &id, 
        DotNetObject object, 
        ArbitrationServiceManager *manager)
    {
        assert(object != NULL);
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        return new ArbitrationServerImpl(&mono_runtime_, id, object, manager);
    }

    /*******************************************************************************
     * Dei
     *******************************************************************************/
    SessionImpl* BadumnaRuntime::CreateSessionImpl(
        String const &server_host, 
        int server_port,
        String const *string_xml)
    {
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        return new SessionImpl(&mono_runtime_, server_host, server_port, string_xml);
    }

    IdentityProviderImpl* BadumnaRuntime::CreateIdentityProviderImpl(
        DotNetObject object)
    {
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        return new IdentityProviderImpl(&mono_runtime_, object);
    }

    String BadumnaRuntime::GenerateKeyPair()
    {
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        DotNetString str = mono_runtime_.InvokeStaticMethod<DotNetString>("Dei", "Session","GenerateKeyPair", Arguments::None(), NULL);
        return MonoRuntime::MonoStringToWString(str);
    }

    /*******************************************************************************
     * Character
     *******************************************************************************/
    Badumna::Character BadumnaRuntime::GetCharacter(DotNetObject character_dno)
    {
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        ManagedObject character_m(&mono_runtime_, character_dno);
        String characterName = character_m.GetPropertyFromName<String>("Name");
        long characterId = character_m.GetPropertyFromName<long>("Id");
        return Character(characterId, characterName);
    }

    std::auto_ptr<Badumna::Character> BadumnaRuntime::GetMaybeCharacter(DotNetObject character_dno)
    {
        if(character_dno == NULL)
        {
            return std::auto_ptr<Character>();
        }

        return std::auto_ptr<Character>(new Character(GetCharacter(character_dno)));
    }

    DotNetObject BadumnaRuntime::ConvertCharacter(Character character)
    {
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        Arguments args;
        uint64_t character_id = character.Id();
        args.AddArgument(&character_id);
        args.AddArgument(MonoRuntime::GetString(character.Name()));
        DotNetClass klass = mono_runtime_.GetClass("Badumna.Security", "Character");
        return mono_runtime_.CreateObject(klass, args, NULL);
    }

    /*******************************************************************************
     * StatisticsTracker
     *******************************************************************************/
    StatisticsTrackerImpl *BadumnaRuntime::CreateStatisticsTrackerImpl(DotNetObject object)
    {
        assert(object != NULL);
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        return new StatisticsTrackerImpl(&mono_runtime_, object);
    }

    /*******************************************************************************
     * Options
     *******************************************************************************/
    OptionsImpl *BadumnaRuntime::CreateOptionsImpl(String const &filename)
    {
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        return new OptionsImpl(&mono_runtime_, filename);
    }

    OptionsImpl *BadumnaRuntime::CreateOptionsImpl()
    {
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        return new OptionsImpl(&mono_runtime_);
    }

    ConnectivityModuleImpl *BadumnaRuntime::CreateConnectivityModuleImpl(DotNetObject object)
    {
        assert(object != NULL);
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        return new ConnectivityModuleImpl(&mono_runtime_, object);
    }
    
    OverloadModuleImpl *BadumnaRuntime::CreateOverloadModuleImpl(DotNetObject object)
    {
        assert(object != NULL);
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        return new OverloadModuleImpl(&mono_runtime_, object);
    }

    ArbitrationModuleImpl *BadumnaRuntime::CreateArbitrationModuleImpl(DotNetObject object)
    {
        assert(object != NULL);
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        return new ArbitrationModuleImpl(&mono_runtime_, object);
    }

    LoggerModuleImpl *BadumnaRuntime::CreateLoggerModuleImpl(DotNetObject object)
    {
        assert(object != NULL);
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        return new LoggerModuleImpl(&mono_runtime_, object);
    }
    
    MatchmakingModuleImpl *BadumnaRuntime::CreateMatchmakingModuleImpl(DotNetObject object)
    {
        assert(object != NULL);
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        return new MatchmakingModuleImpl(&mono_runtime_, object);
    }

    ThreadGuardImpl *BadumnaRuntime::CreateMonoThreadGuardImpl()
    {
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        return new ThreadGuardImpl(&mono_runtime_);
    }

    /*******************************************************************************
     * Distributed controller
     *******************************************************************************/
    DistributedSceneControllerImpl *BadumnaRuntime::CreateDistributedControllerImpl(
        DotNetObject object, 
        String const &name)
    {
        assert(object != NULL);
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");
        
        return new DistributedSceneControllerImpl(&mono_runtime_, object, name);
    }

    /*******************************************************************************
     * Streaming
     *******************************************************************************/
    LocalStreamControllerImpl *BadumnaRuntime::CreateLocalStreamControllerImpl(
        DotNetObject object,
        StreamingServiceManager *manager)
    {
        assert(object != NULL);
        assert(manager != NULL);
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");

        return new LocalStreamControllerImpl(&mono_runtime_, object, manager);
    }

    StreamingManagerImpl *BadumnaRuntime::CreateStreamingManagerImpl(
        DotNetObject object, 
        StreamingServiceManager *manager)
    {
        assert(object != NULL);
        assert(manager != NULL);
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");

        return new StreamingManagerImpl(&mono_runtime_, object, manager);
    }

    StreamRequestImpl *BadumnaRuntime::CreateStreamRequestImpl(DotNetObject object, StreamingServiceManager *manager)
    {
        assert(object != NULL);
        assert(manager != NULL);
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");

        return new StreamRequestImpl(&mono_runtime_, object, manager);
    }
    
    /*******************************************************************************
     * Match
     *******************************************************************************/
    MatchFacadeImpl *BadumnaRuntime::CreateMatchFacadeImpl(DotNetObject object)
    {
        assert(object != NULL);
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");

        return new MatchFacadeImpl(&mono_runtime_, object);
    }

    MatchmakingQueryResultImpl *BadumnaRuntime::CreateMatchmakingQueryResultImpl(DotNetObject object)
    {
        assert(object != NULL);
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");

        return new MatchmakingQueryResultImpl(&mono_runtime_, object);
    }

    MatchmakingResultImpl *BadumnaRuntime::CreateMatchmakingResultImpl(DotNetObject object)
    {
        assert(object != NULL);
        assert(initialized_ && "BadumnaRuntime must be initialized first (see InitializeRuntime(...) or BadumnaRuntimeInitializer in RuntimeInitializer.h)");

        return new MatchmakingResultImpl(&mono_runtime_, object);
    }
}