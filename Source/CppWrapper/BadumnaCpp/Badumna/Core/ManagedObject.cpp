//---------------------------------------------------------------------------------
// <copyright file="ManagedObject.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include <cstdlib>

#include "Badumna/Core/MonoRuntime.h"
#include "Badumna/Core/ManagedObject.h"
#include "Badumna/Core/InternalErrorHandler.h"
#include "Badumna/Core/InternalErrorType.h"

namespace Badumna
{
    ManagedObject::ManagedObject(MonoRuntime *runtime) 
        : mono_runtime_(runtime), 
        klass_(NULL),
        object_gchandle_(0),
        object_registered_(false)
    {
        assert(runtime != NULL);
    }

    ManagedObject::ManagedObject(MonoRuntime *runtime, DotNetObject object)
        : mono_runtime_(runtime),
        klass_(mono_runtime_->GetClass(object)),
        object_gchandle_(GetObjectGCHandle(object)),
        object_registered_(true)
    {
        assert(runtime != NULL && object != NULL);
        assert(klass_ != NULL);
    }

    ManagedObject::ManagedObject(MonoRuntime *runtime, NameType name_space, NameType class_name)
        : mono_runtime_(runtime),
        klass_(mono_runtime_->GetClass(name_space, class_name)),
        object_gchandle_(GetObjectGCHandle(mono_runtime_->CreateObject(klass_, NULL))),
        object_registered_(true)
    {
        assert(runtime != NULL);
        assert(klass_ != NULL);
    }

    ManagedObject::ManagedObject(
        MonoRuntime *runtime, 
        DotNetObject object, 
        NameType name_space, 
        NameType class_name)
        : mono_runtime_(runtime),
        klass_(mono_runtime_->GetClass(name_space, class_name)),
        object_gchandle_(GetObjectGCHandle(object)),
        object_registered_(true)
    {
        assert(runtime != NULL && object != NULL);
        assert(klass_ != NULL);
    }

    ManagedObject::ManagedObject(ManagedObject const &other)
        : mono_runtime_(other.GetMonoRuntime()),
        klass_(NULL),
        object_gchandle_(0),
        object_registered_(false)
    {
        if(!other.object_registered_)
        {
            object_registered_ = false;
            object_gchandle_ = 0;
            klass_= NULL;
        }
        else
        {
            DotNetObject cloned_object;
            cloned_object = mono_runtime_->CloneObject(other.GetManagedObject());
            klass_ = MonoRuntime::GetClass(cloned_object);
            object_registered_ = true;
            object_gchandle_ = GetObjectGCHandle(cloned_object);
        }
    }

    ManagedObject::~ManagedObject()
    {
        FreeObjectGCHandle();
    }

    ManagedObject &ManagedObject::operator=(ManagedObject const &rhs)
    {
        if(this != &rhs)
        {
            ManagedObject tmp(rhs);
            ManagedObject::Swap(*this, tmp);
        }

        return *this;
    }

    void ManagedObject::Swap(ManagedObject &lhs, ManagedObject &rhs)
    {
        bool object_registered = lhs.object_registered_;
        DotNetClass klass = lhs.klass_;
        MonoGCHandle object_gchandle = lhs.object_gchandle_;

        lhs.object_registered_ = rhs.object_registered_;
        lhs.klass_ = rhs.klass_;
        lhs.object_gchandle_ = rhs.object_gchandle_;

        rhs.object_registered_ = object_registered;
        rhs.klass_ = klass;
        rhs.object_gchandle_ = object_gchandle;
    }

    void ManagedObject::Initialize(DotNetObject object, NameType name_space, NameType class_name)
    {
        assert(object != NULL);

        object_gchandle_ = GetObjectGCHandle(object);
        klass_ = mono_runtime_->GetClass(name_space, class_name);
        object_registered_ = true;
    }

    DotNetObject ManagedObject::InvokeMethod(
        NameType name, 
        Arguments const &args, 
        ExceptionDetails *exc_details) const
    {
        CheckInternalObjects();
        return mono_runtime_->InvokeMethod(klass_, Object(), name, args, exc_details);
    }

    DotNetObject ManagedObject::InvokeVirtualMethod(
        NameType name, 
        Arguments const &args, 
        ExceptionDetails *exc_details) const
    {
        CheckInternalObjects();
        return mono_runtime_->InvokeVirtualMethod(klass_, Object(), name, args, exc_details);
    }
    
    DotNetObject ManagedObject::InvokeVirtualMethodFullName(
        NameType name, 
        bool include_namespace, 
        Arguments const &args, 
        ExceptionDetails *exc_details) const
    {
        CheckInternalObjects();
        return mono_runtime_->InvokeVirtualMethodFullName(klass_, Object(), name,  include_namespace, args, exc_details);
    }

    DotNetObject ManagedObject::GetPropertyFromName(NameType name, Arguments const &args, ExceptionDetails *exc_details) const
    {
        CheckInternalObjects();
        return mono_runtime_->GetPropertyFromName(klass_, Object(), name, args, exc_details);
    }

    void ManagedObject::SetPropertyValue(NameType name, Arguments const &args, ExceptionDetails *exc_details)
    {
        CheckInternalObjects();
        mono_runtime_->SetPropertyValue(klass_, Object(), name, args, exc_details);
    }

    void ManagedObject::SetPropertyValue(NameType name, bool value, ExceptionDetails *exc_details)
    {
        Arguments args;
        args.AddArgument(&value);
        SetPropertyValue(name, args, exc_details);
    }
    
    void ManagedObject::SetPropertyValue(NameType name, int value, ExceptionDetails *exc_details)
    {
        Arguments args;
        args.AddArgument(&value);
        SetPropertyValue(name, args, exc_details);
    }

    void ManagedObject::SetPropertyValue(NameType name, float value, ExceptionDetails *exc_details)
    {
        Arguments args;
        args.AddArgument(&value);
        SetPropertyValue(name, args, exc_details);
    }

    void ManagedObject::SetPropertyValue(NameType name, String const &value, ExceptionDetails *exc_details)
    {
        Arguments args;
        args.AddArgument(MonoRuntime::GetString(value));
        SetPropertyValue(name, args, exc_details);
    }

    DotNetObject ManagedObject::GetManagedObject() const
    {
        CheckInternalObjects();
        return Object();
    }

    DotNetClass ManagedObject::GetManagedClass() const
    {
        CheckInternalObjects();
        return klass_;
    }

    void ManagedObject::CheckInternalObjects() const
    {
#ifndef NDEBUG
        if(klass_ == NULL)
        {
            InternalErrorHandler::Instance().Invoke(InternalErrorType_ManagedClassIsNull, "Class is null");
        }

        if(!(object_registered_))
        {
            InternalErrorHandler::Instance().Invoke(InternalErrorType_ManagedObjectIsNull, "Object is null");
        }
#endif
    }
    
    void ManagedObject::FreeObjectGCHandle() const
    {
        if(object_registered_ && object_gchandle_ != 0)
        {
            MonoRuntime::FreeObjectGCHandle(object_gchandle_);
        }
    }
}