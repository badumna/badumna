//---------------------------------------------------------------------------------
// <copyright file="ManagedObject.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_MANAGED_OBJECT_H
#define BADUMNA_MANAGED_OBJECT_H

#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Core/Arguments.h"
#include "Badumna/Core/MonoRuntime.h"
#include "Badumna/Core/ExceptionDetails.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    /**
     * The ManagedObject class encapsaltes managed object created/managed by Mono and the Mono Runtime object itself 
     * required to manage those managed objects.
     * 
     * The ManagedObject class provides a low level interface allowing client code (usually concrete ProxyObject class) 
     * to InvokeMethod, GetPropertyFromName, SetPropertyValue and etc. It uses RAII to maintain reference counting for  
     * actual managed objects created in Mono. 
     */
    class ManagedObject
    {
    public:
        explicit ManagedObject(MonoRuntime *runtime);
        ManagedObject(MonoRuntime *runtime, DotNetObject object);
        ManagedObject(MonoRuntime *runtime, NameType name_space, NameType class_name);
        ManagedObject(MonoRuntime *runtime, DotNetObject object, NameType name_space, NameType class_name);
        explicit ManagedObject(ManagedObject const &other);

        ~ManagedObject();

        ManagedObject &operator=(ManagedObject const &rhs);

        void Initialize(DotNetObject object, NameType name_space, NameType class_name);

        // regular methods
        DotNetObject InvokeMethod(
            NameType name, 
            Arguments const &args = Arguments::None(), 
            ExceptionDetails *exc_details = NULL) const;

        template<typename T>
        T InvokeMethod(
            NameType name, 
            Arguments const &args = Arguments::None(), 
            ExceptionDetails *exc_details = NULL) const
        {
            CheckInternalObjects();
            DotNetObject result = mono_runtime_->InvokeMethod(klass_, Object(), name, args, exc_details);

            return MonoRuntime::UnboxingObject<T>(result);
        }

        // virtual methods
        DotNetObject InvokeVirtualMethod(
            NameType name, 
            Arguments const &args = Arguments::None(), 
            ExceptionDetails *exc_details = NULL) const;

        DotNetObject InvokeVirtualMethodFullName(
            NameType name, 
            bool include_namespace, 
            Arguments const &args = Arguments::None(), 
            ExceptionDetails *exc_details = NULL) const;

        template<typename T> 
        T InvokeVirtualMethod(
            NameType name, 
            Arguments const &args = Arguments::None(), 
            ExceptionDetails *exc_details = NULL) const
        {
            CheckInternalObjects();
            DotNetObject result = mono_runtime_->InvokeVirtualMethod(klass_, Object(), name, args, exc_details);

            return MonoRuntime::UnboxingObject<T>(result);
        }

        // property getter
        template<typename T>
        T GetPropertyFromName(
            NameType name, 
            Arguments const &args = Arguments::None(), 
            ExceptionDetails *exc_details = NULL) const
        {
            CheckInternalObjects();
            DotNetObject result = mono_runtime_->GetPropertyFromName(klass_, Object(), name, args, exc_details);

            return MonoRuntime::UnboxingObject<T>(result);
        }

        template<typename T>
        T GetVirtualPropertyFromName(
            NameType name, 
            Arguments const &args = Arguments::None(), 
            ExceptionDetails *exc_details = NULL) const
        {
            CheckInternalObjects();
            DotNetObject result = mono_runtime_->GetVirtualPropertyFromName(klass_, Object(), name, args, exc_details);

            return MonoRuntime::UnboxingObject<T>(result);
        }

        DotNetObject GetPropertyFromName(
            NameType name,
            Arguments const &args = Arguments::None(),
            ExceptionDetails *exc_details = NULL) const;

        // property setter
        // multiple parameters
        // this is required e.g. for index related property : BooleanArray[1] = true;
        void SetPropertyValue(NameType name, Arguments const &args, ExceptionDetails *exc_details = NULL);
        // single parameter
        void SetPropertyValue(NameType name, bool value, ExceptionDetails *exc_details = NULL);
        void SetPropertyValue(NameType name, int value, ExceptionDetails *exc_details = NULL);
        void SetPropertyValue(NameType name, float value, ExceptionDetails *exc_details = NULL);
        void SetPropertyValue(NameType name, String const &value, ExceptionDetails *exc_details = NULL);

        // access the internal object/klass
        DotNetObject GetManagedObject() const;
        DotNetClass GetManagedClass() const;

        MonoRuntime *GetMonoRuntime() const
        {
            return mono_runtime_;
        }

    private:
        inline DotNetObject Object() const
        {
            return MonoRuntime::GetObjectFromGCHandle(object_gchandle_);
        }

        inline MonoGCHandle GetObjectGCHandle(DotNetObject object) const
        {
            return MonoRuntime::GetObjectGCHandle(object);
        }

        void FreeObjectGCHandle() const;
        void CheckInternalObjects() const;
        static void Swap(ManagedObject &lhs, ManagedObject &rhs);

        MonoRuntime *mono_runtime_;
        DotNetClass klass_;
        MonoGCHandle object_gchandle_;
        bool object_registered_;
    };
}

#endif // BADUMNA_MANAGEDOBJECT_H