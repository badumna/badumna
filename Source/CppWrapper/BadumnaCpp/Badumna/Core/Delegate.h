//---------------------------------------------------------------------------------
// <copyright file="Delegate.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_DELEGATE_H
#define BADUMNA_DELEGATE_H

#include <cstdlib>
#include "Badumna/Utils/LinkedPtr.h"
#include "Badumna/Utils/BasicTypes.h"

#ifndef DOXYGEN_SHOULD_SKIP_THIS

namespace Badumna
{
    /*******************************************************************************************************************
    * Delegate0
    *******************************************************************************************************************/
    template<typename R>
    class GeneralMemFuncPointer0
    {
    public:
        virtual R operator()() = 0;
        virtual ~GeneralMemFuncPointer0() {}
    };

    template<typename R, typename T>
    class ConcreteMemFuncPointer0 : public GeneralMemFuncPointer0<R>
    {
    public:
        typedef R (T::*MemFunctionPointer)();

        ConcreteMemFuncPointer0(R (T::*f)(), T &object)
            : ref(object),
            function_pointer(f)
        {
        }

        R operator()()
        {
            return ((ref).*(function_pointer))();
        }

    private:
        T &ref;
        MemFunctionPointer function_pointer;

        DISALLOW_COPY_AND_ASSIGN(ConcreteMemFuncPointer0);
    };

    template<typename R, typename T>
    GeneralMemFuncPointer0<R> *GetGeneralMemFuncPointer0(
        typename ConcreteMemFuncPointer0<R, T>::MemFunctionPointer fp, 
        T &object)
    {
        return new ConcreteMemFuncPointer0<R, T>(fp, object);
    }

    /**
     * @class Delegate0
     * 
     * @tparam R The return type of the delegate.
     *
     * Delegate0 is a delegate without any parameter. The delegate object is copyable and assignable. 
     */
    template<typename R>
    class BADUMNA_API Delegate0
    {
    public:
        /**
         * Constructor. Constructs the delegate object using a class member function.
         *
         * @param fp The function pointer that points to a class member function. 
         * @param object The object associated with the pointer.
         * @tparam T The type of the class that the pointer points to. 
         */
        template<typename T>
        Delegate0(R (T::*fp)(), T &object)
            : use_mem_function(true), 
            gfp(GetGeneralMemFuncPointer0<R>(fp, object)),
            free_mem_function_pointer(NULL)
        {
        }

        /**
         * Constructor. Constructs the delegate object using a free function.
         *
         * @param fp The function pointer that points to a free function.
         */
        Delegate0(R (*fp)())
            : use_mem_function(false),
            gfp(NULL),
            free_mem_function_pointer(fp)
        {
        }

        /**
         * Destructor.
         */
        virtual ~Delegate0() { }

        /**
         * Invoke the delegate.
         *
         * @return The return value of the delegate.
         */
        R operator()() const
        {
            if(use_mem_function)
            {
                return (*gfp)();
            }
            else
            {
                return (*free_mem_function_pointer)();
            }
        }

    private:
        typedef R (*FreeMemFunctionPointer)();
        bool use_mem_function;
        linked_ptr<GeneralMemFuncPointer0<R> > gfp;
        FreeMemFunctionPointer free_mem_function_pointer;
    };

    /*******************************************************************************************************************
    * Delegate1
    *******************************************************************************************************************/
    template<typename R, typename A1>
    class GeneralMemFuncPointer1
    {
    public:
        virtual R operator()(A1 a1) = 0;
        virtual ~GeneralMemFuncPointer1() {}
    };

    template<typename R, typename A1, typename T>
    class ConcreteMemFuncPointer1 : public GeneralMemFuncPointer1<R, A1>
    {
    public:
        typedef R (T::*MemFunctionPointer)(A1);

        ConcreteMemFuncPointer1(R (T::*f)(A1), T &object)
            : ref(object),
            function_pointer(f)
        {
        }

        R operator()(A1 a)
        {
            return ((ref).*(function_pointer))(a);
        }

    private:
        T &ref;
        MemFunctionPointer function_pointer;

        DISALLOW_COPY_AND_ASSIGN(ConcreteMemFuncPointer1);
    };

    template<typename R, typename A1, typename T>
    GeneralMemFuncPointer1<R, A1> *GetGeneralMemFuncPointer1(
        typename ConcreteMemFuncPointer1<R, A1, T>::MemFunctionPointer fp, 
        T &object)
    {
        return new ConcreteMemFuncPointer1<R, A1, T>(fp, object);
    }

    /**
     * @class Delegate1
     * 
     * @tparam R The return type of the delegate.
     * @tparam A1 The type of the first parameter. 
     *
     * Delegate1 is a delegate with one parameter. The delegate object is copyable and assignable. 
     */
    template<typename R, typename A1>
    class BADUMNA_API Delegate1
    {
    public:
        /**
         * Constructor. Constructs the delegate object using a class member function.
         *
         * @param fp The function pointer that points to a class member function. 
         * @param object The object associated with the pointer.
         * @tparam T The type of the class that the pointer points to. 
         */
        template<typename T>
        Delegate1(R (T::*fp)(A1), T &object)
            : use_mem_function(true), 
            gfp(GetGeneralMemFuncPointer1<R, A1>(fp, object)),
            free_mem_function_pointer(NULL)
        {
        }

        /**
         * Constructor. Constructs the delegate object using a free function.
         *
         * @param fp The function pointer that points to a free function.
         */
        Delegate1(R (*fp)(A1))
            : use_mem_function(false),
            gfp(NULL),
            free_mem_function_pointer(fp)
        {
        }
        
        /**
         * Destructor.
         */
        virtual ~Delegate1() { }

        /**
         * Invoke the delegate.
         *
         * @param a1 The first parameter of the delegate.
         * @return The return value of the delegate.
         */
        R operator()(A1 a1) const
        {
            if(use_mem_function)
            {
                return (*gfp)(a1);
            }
            else
            {
                return (*free_mem_function_pointer)(a1);
            }
        }

    private:
        typedef R (*FreeMemFunctionPointer)(A1);
        bool use_mem_function;
        linked_ptr<GeneralMemFuncPointer1<R, A1> > gfp;
        FreeMemFunctionPointer free_mem_function_pointer;
    };

    /*******************************************************************************************************************
    * Delegate2
    *******************************************************************************************************************/
    template<typename R, typename A1, typename A2>
    class GeneralMemFuncPointer2
    {
    public:
        virtual R operator()(A1 a1, A2 a2) = 0;
        virtual ~GeneralMemFuncPointer2() {}
    };

    template<typename R, typename A1, typename A2, typename T>
    class ConcreteMemFuncPointer2 : public GeneralMemFuncPointer2<R, A1, A2>
    {
    public:
        typedef R (T::*MemFunctionPointer)(A1, A2);

        ConcreteMemFuncPointer2(R (T::*f)(A1, A2), T &object)
            : ref(object),
            function_pointer(f)
        {
        }

        R operator()(A1 a1, A2 a2)
        {
            return ((ref).*(function_pointer))(a1, a2);
        }

    private:
        T &ref;
        MemFunctionPointer function_pointer;

        DISALLOW_COPY_AND_ASSIGN(ConcreteMemFuncPointer2);
    };

    template<typename R, typename A1, typename A2, typename T>
    GeneralMemFuncPointer2<R, A1, A2> *GetGeneralMemFuncPointer2(
        typename ConcreteMemFuncPointer2<R, A1, A2, T>::MemFunctionPointer fp, 
        T &object)
    {
        return new ConcreteMemFuncPointer2<R, A1, A2, T>(fp, object);
    }

    /**
     * @class Delegate2
     * 
     * @tparam R The return type of the delegate.
     * @tparam A1 The type of the first parameter. 
     * @tparam A2 The type of the second parameter.
     *
     * Delegate2 is a delegate with two parameter. The delegate object is copyable and assignable. 
     */
    template<typename R, typename A1, typename A2>
    class BADUMNA_API Delegate2
    {
    public:
        /**
         * Constructor. Constructs the delegate object using a class member function.
         *
         * @param fp The function pointer that points to a class member function. 
         * @param object The object associated with the pointer.
         * @tparam T The type of the class that the pointer points to. 
         */
        template<typename T>
        Delegate2(R (T::*fp)(A1, A2), T &object)
            : use_mem_function(true), 
            gfp(GetGeneralMemFuncPointer2<R, A1, A2>(fp, object)),
            free_mem_function_pointer(NULL)
        {
        }

        /**
         * Constructor. Constructs the delegate object using a free function.
         *
         * @param fp The function pointer that points to a free function.
         */
        Delegate2(R (*fp)(A1, A2))
            : use_mem_function(false),
            gfp(NULL),
            free_mem_function_pointer(fp)
        {
        }
        
        /**
         * Desctructor.
         */
        virtual ~Delegate2() { }

        /**
         * Invoke the delegate.
         *
         * @param a1 The first parameter of the delegate.
         * @param a2 The second parameter of the delegate.
         * @return The return value of the delegate.
         */
        R operator()(A1 a1, A2 a2) const
        {
            if(use_mem_function)
            {
                return (*gfp)(a1, a2);
            }
            else
            {
                return (*free_mem_function_pointer)(a1, a2);
            }
        }

    private:
        typedef R (*FreeMemFunctionPointer)(A1, A2);
        bool use_mem_function;
        linked_ptr<GeneralMemFuncPointer2<R, A1, A2> > gfp;
        FreeMemFunctionPointer free_mem_function_pointer;
    };

    /*******************************************************************************************************************
    * Delegate3
    *******************************************************************************************************************/
    template<typename R, typename A1, typename A2, typename A3>
    class GeneralMemFuncPointer3
    {
    public:
        virtual R operator()(A1 a1, A2 a2, A3 a3) = 0;
        virtual ~GeneralMemFuncPointer3() {}
    };

    template<typename R, typename A1, typename A2, typename A3, typename T>
    class ConcreteMemFuncPointer3 : public GeneralMemFuncPointer3<R, A1, A2, A3>
    {
    public:
        typedef R (T::*MemFunctionPointer)(A1, A2, A3);

        ConcreteMemFuncPointer3(R (T::*f)(A1, A2, A3), T &object)
            : ref(object),
            function_pointer(f)
        {
        }

        R operator()(A1 a1, A2 a2, A3 a3)
        {
            return ((ref).*(function_pointer))(a1, a2, a3);
        }

    private:
        T &ref;
        MemFunctionPointer function_pointer;

        DISALLOW_COPY_AND_ASSIGN(ConcreteMemFuncPointer3);
    };

    template<typename R, typename A1, typename A2, typename A3, typename T>
    GeneralMemFuncPointer3<R, A1, A2, A3> *GetGeneralMemFuncPointer3(
        typename ConcreteMemFuncPointer3<R, A1, A2, A3, T>::MemFunctionPointer fp, 
        T &object)
    {
        return new ConcreteMemFuncPointer3<R, A1, A2, A3, T>(fp, object);
    }

    /**
     * @class Delegate3
     * 
     * @tparam R The return type of the delegate.
     * @tparam A1 The type of the first parameter. 
     * @tparam A2 The type of the second parameter.
     * @tparam A3 The type of the third parameter.
     *
     * Delegate3 is a delegate with three parameter. The delegate object is copyable and assignable. 
     */
    template<typename R, typename A1, typename A2, typename A3>
    class BADUMNA_API Delegate3
    {
    public:
        /**
         * Constructor. Constructs the delegate object using a class member function.
         *
         * @param fp The function pointer that points to a class member function. 
         * @param object The object associated with the pointer.
         * @tparam T The type of the class that the pointer points to. 
         */
        template<typename T>
        Delegate3(R (T::*fp)(A1, A2, A3), T &object)
            : use_mem_function(true), 
            gfp(GetGeneralMemFuncPointer3<R, A1, A2, A3>(fp, object)),
            free_mem_function_pointer(NULL)
        {
        }

        /**
         * Constructor. Constructs the delegate object using a free function.
         *
         * @param fp The function pointer that points to a free function.
         */
        Delegate3(R (*fp)(A1, A2, A3))
            : use_mem_function(false), 
            gfp(NULL),
            free_mem_function_pointer(fp)
        {
        }

        /**
         * Destructor.
         */
        virtual ~Delegate3() { }

        /**
         * Invoke the delegate.
         *
         * @param a1 The first parameter of the delegate.
         * @param a2 The second parameter of the delegate.
         * @param a3 The third parameter of the delegate.
         * @return The return value of the delegate.
         */
        R operator()(A1 a1, A2 a2, A3 a3) const
        {
            if(use_mem_function)
            {
                return (*gfp)(a1, a2, a3);
            }
            else
            {
                return (*free_mem_function_pointer)(a1, a2, a3);
            }
        }

    private:
        typedef R (*FreeMemFunctionPointer)(A1, A2, A3);
        bool use_mem_function;
        linked_ptr<GeneralMemFuncPointer3<R, A1, A2, A3> > gfp;
        FreeMemFunctionPointer free_mem_function_pointer;
    };

    /*******************************************************************************************************************
    * Delegate4
    *******************************************************************************************************************/
    template<typename R, typename A1, typename A2, typename A3, typename A4>
    class GeneralMemFuncPointer4
    {
    public:
        virtual R operator()(A1 a1, A2 a2, A3 a3, A4 a4) = 0;
        virtual ~GeneralMemFuncPointer4() {}
    };

    template<typename R, typename A1, typename A2, typename A3, typename A4, typename T>
    class ConcreteMemFuncPointer4 : public GeneralMemFuncPointer4<R, A1, A2, A3, A4>
    {
    public:
        typedef R (T::*MemFunctionPointer)(A1, A2, A3, A4);

        ConcreteMemFuncPointer4(R (T::*f)(A1, A2, A3, A4), T &object)
            : ref(object),
            function_pointer(f)
        {
        }

        R operator()(A1 a1, A2 a2, A3 a3, A4 a4)
        {
            return ((ref).*(function_pointer))(a1, a2, a3, a4);
        }

    private:
        T &ref;
        MemFunctionPointer function_pointer;

        DISALLOW_COPY_AND_ASSIGN(ConcreteMemFuncPointer4);
    };

    template<typename R, typename A1, typename A2, typename A3, typename A4, typename T>
    GeneralMemFuncPointer4<R, A1, A2, A3, A4> *GetGeneralMemFuncPointer4(
        typename ConcreteMemFuncPointer4<R, A1, A2, A3, A4, T>::MemFunctionPointer fp, 
        T &object)
    {
        return new ConcreteMemFuncPointer4<R, A1, A2, A3, A4, T>(fp, object);
    }

    /**
     * @class Delegate4
     * 
     * @tparam R The return type of the delegate.
     * @tparam A1 The type of the first parameter. 
     * @tparam A2 The type of the second parameter.
     * @tparam A3 The type of the third parameter.
     * @tparam A4 The type of the forth parameter.
     *
     * Delegate4 is a delegate with four parameter. The delegate object is copyable and assignable. 
     */
    template<typename R, typename A1, typename A2, typename A3, typename A4>
    class BADUMNA_API Delegate4
    {
    public:
        /**
         * Constructor. Constructs the delegate object using a class member function.
         *
         * @param fp The function pointer that points to a class member function. 
         * @param object The object associated with the pointer.
         * @tparam T The type of the class that the pointer points to. 
         */
        template<typename T>
        Delegate4(R (T::*fp)(A1, A2, A3, A4), T &object)
            : use_mem_function(true), 
            gfp(GetGeneralMemFuncPointer4<R, A1, A2, A3, A4>(fp, object)),
            free_mem_function_pointer(NULL)
        {
        }

        /**
         * Constructor. Constructs the delegate object using a free function.
         *
         * @param fp The function pointer that points to a free function.
         */
        Delegate4(R (*fp)(A1, A2, A3, A4))
            : use_mem_function(false),
            gfp(NULL),
            free_mem_function_pointer(fp)
        {
        }

        /**
         * Destructor.
         */
        virtual ~Delegate4() { }

        /**
         * Invoke the delegate.
         *
         * @param a1 The first parameter of the delegate.
         * @param a2 The second parameter of the delegate.
         * @param a3 The third parameter of the delegate.
         * @param a4 the forth parameter of the delegate.
         * @return The return value of the delegate.
         */
        R operator()(A1 a1, A2 a2, A3 a3, A4 a4) const
        {
            if(use_mem_function)
            {
                return (*gfp)(a1, a2, a3, a4);
            }
            else
            {
                return (*free_mem_function_pointer)(a1, a2, a3, a4);
            }
        }

    private:
        typedef R (*FreeMemFunctionPointer)(A1, A2, A3, A4);
        bool use_mem_function;
        linked_ptr<GeneralMemFuncPointer4<R, A1, A2, A3, A4> > gfp;
        FreeMemFunctionPointer free_mem_function_pointer;
    };
}

#endif // DOXYGEN_SHOULD_SKIP_THIS

#endif // BADUMNA_DELEGATE_H
