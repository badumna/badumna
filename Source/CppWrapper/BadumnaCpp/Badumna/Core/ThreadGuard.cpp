//---------------------------------------------------------------------------------
// <copyright file="ThreadGuard.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include "Badumna/Core/ThreadGuard.h"
#include "Badumna/Core/ThreadGuardImpl.h"
#include "Badumna/Core/BadumnaRuntime.h"

namespace Badumna
{
    ThreadGuard::ThreadGuard()
        : impl_(BadumnaRuntime::Instance().CreateMonoThreadGuardImpl())
    {
    }

    ThreadGuard::~ThreadGuard()
    {
    }
}
