//---------------------------------------------------------------------------------
// <copyright file="ProxyObject.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include "Badumna/Core/Arguments.h"
#include "Badumna/Core/ProxyObject.h"

namespace Badumna
{
    ProxyObject::ProxyObject(MonoRuntime *runtime) 
        : mono_runtime_(runtime),
        managed_object_(runtime)
    {
    }

    ProxyObject::ProxyObject(ManagedObject const &object)
        : mono_runtime_(object.GetMonoRuntime()),
        managed_object_(object)
    {
    }

    ProxyObject::ProxyObject(MonoRuntime *runtime, DotNetObject object)
        :  mono_runtime_(runtime),
        managed_object_(runtime, object)
    {
    }

    ProxyObject::ProxyObject(MonoRuntime *runtime, NameType name_space, NameType class_name) 
        : mono_runtime_(runtime), 
        managed_object_(runtime, name_space, class_name)
    {
    }

    ProxyObject::ProxyObject(MonoRuntime *runtime, 
        DotNetObject object, 
        NameType name_space, 
        NameType class_name) 
        : mono_runtime_(runtime), 
        managed_object_(runtime, object, name_space, class_name)
    {
    }

    DotNetObject ProxyObject::GetManagedMonoObject() const
    {
        return managed_object_.GetManagedObject();
    }

    DotNetClass ProxyObject::GetManagedMonoClass() const
    {
        return managed_object_.GetManagedClass();
    }

    String ProxyObject::ToString() const
    {
        DotNetString str = managed_object_.InvokeMethod<DotNetString>("ToString");  
        return MonoRuntime::MonoStringToWString(str);
    }

    int ProxyObject::GetHashCode() const
    {
        return managed_object_.InvokeMethod<int>("GetHashCode");
    }

    bool ProxyObject::Equals(DotNetObject other) const
    {
        Arguments args;
        args.AddArgument(other);
        return managed_object_.InvokeMethod<bool>("Equals", args);
    }
}