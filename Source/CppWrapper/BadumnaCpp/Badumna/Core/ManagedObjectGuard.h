//---------------------------------------------------------------------------------
// <copyright file="ManagedObjectGuard.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_MANAGED_OBJECT_GUARD_H
#define BADUMNA_MANAGED_OBJECT_GUARD_H

#include "Badumna/Core/DotNetTypes.h"

namespace Badumna
{
    class ManagedObjectGuard
    {
    public:
        template<typename T>
        static DotNetObject GetManagedObject(T const &object)
        {
            return object.impl_->GetManagedMonoObject();
        }
    };
}

#endif // BADUMNA_MANAGED_OBJECT_GUARD_H