//---------------------------------------------------------------------------------
// <copyright file="FacadeServiceManager.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_FACADE_SERVICE_MANAGER_H
#define BADUMNA_FACADE_SERVICE_MANAGER_H

#include "Badumna/Core/BadumnaMap.h"
#include "Badumna/Core/ConnectivityStatusDelegate.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class FacadeServiceManager
    {
    public:
        FacadeServiceManager();
    
        void SetOnlineEventDelegate(ConnectivityStatusDelegate online_delegate);
        void SetOfflineEventDelegate(ConnectivityStatusDelegate offline_delegate);
        void SetAddressChangedEventDelegate(ConnectivityStatusDelegate address_change_delegate);

        void InvokeOnlineEventDelegate() const;
        void InvokeOfflineEventDelegate() const;
        void InvokeAddressChangedEventDelegate() const;

    private:
        enum ConnectivityDelegateType
        {
            OnlineEventDelegateType,
            OfflineEventDelegateType,
            AddressChangedDelegateType
        };

        typedef BadumnaMap<ConnectivityStatusDelegate, ConnectivityDelegateType> DelegateMap;
        DelegateMap delegate_map_;

        void InvokeConnectivityDelegate(ConnectivityDelegateType type) const;

        DISALLOW_COPY_AND_ASSIGN(FacadeServiceManager);
    };
}

#endif // BADUMNA_FACADE_SERVICE_MANAGER_H