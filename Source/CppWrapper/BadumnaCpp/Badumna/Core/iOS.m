//---------------------------------------------------------------------------------
// <copyright file="iOS.m" company="National ICT Australia Ltd">
//     Copyright (c) 2013 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#import "iOS.h"

// Make sure to use the mono glib that's X11 licensed.
#include <glib.h>

#include <Foundation/Foundation.h>

extern gboolean mono_use_llvm;
void mono_aot_register_module (void *aot_info); 

#if defined(__arm__)
extern void *mono_aot_module_mscorlib_info;
extern void *mono_aot_module_Dei_Unity_iOS_info;
extern void *mono_aot_module_System_info;
extern void *mono_aot_module_System_Configuration_info;
extern void *mono_aot_module_System_Security_info;
extern void *mono_aot_module_Badumna_Unity_iOS_info;
extern void *mono_aot_module_Mono_Security_info;
extern void *mono_aot_module_System_Xml_info;
#endif

void initialize_ios()
{ 
	[[NSThread new] start];  // Need to spawn a dummy thread that just exits to let Cocoa know we're going to use native threads.
  
#if defined (__arm__)
	mono_aot_register_module (mono_aot_module_mscorlib_info);
	mono_aot_register_module (mono_aot_module_Dei_Unity_iOS_info);
	mono_aot_register_module (mono_aot_module_System_info);
	mono_aot_register_module (mono_aot_module_System_Configuration_info);
	mono_aot_register_module (mono_aot_module_System_Security_info);
	mono_aot_register_module (mono_aot_module_Badumna_Unity_iOS_info);
	mono_aot_register_module (mono_aot_module_Mono_Security_info);
	mono_aot_register_module (mono_aot_module_System_Xml_info);
    
	mono_jit_set_aot_only (TRUE);
	mono_use_llvm = FALSE;
#endif
}

char *get_bundle_path()
{
	return [[[NSBundle mainBundle] bundlePath] UTF8String];
}
