//---------------------------------------------------------------------------------
// <copyright file="NetworkStatusImpl.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_NETWORK_STATUS_IMPLE_H
#define BADUMNA_NETWORK_STATUS_IMPLE_H

#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Core/ProxyObject.h"
#include "Badumna/Core/ProxyObjectHelper.h"
#include "Badumna/DataTypes/String.h"

namespace Badumna
{
    class NetworkStatusImpl : public ProxyObject
    {
    public:
        explicit NetworkStatusImpl(MonoRuntime *runtime);
        NetworkStatusImpl(MonoRuntime *runtime, DotNetObject object);

        String ToString() const;
        String GetPublicAddress() const;
        String GetPrivateAddress() const;
        int32_t GetActiveConnectionCount() const;
        int32_t GetInitializingConnectionCount() const;
        String GetDiscoveryMethod() const;
        bool GetPortForwardingEnabled() const;
        bool GetPortForwardingSucceeded() const;

        int32_t GetLocalObjectCount() const;
        int32_t GetRemoteObjectCount() const;
        int64_t GetTotalBytesSentPerSecond() const;
        int64_t GetTotalBytesReceivedPerSecond() const;

        DECLARE_COPY_AND_ASSIGN(NetworkStatusImpl);
    };
}

#endif // BADUMNA_NETWORK_STATUS_IMPLE_H