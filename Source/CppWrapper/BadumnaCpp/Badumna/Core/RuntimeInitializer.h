//---------------------------------------------------------------------------------
// <copyright file="RuntimeInitializer.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_RUNTIME_INITIALIZER_H
#define BADUMNA_RUNTIME_INITIALIZER_H

#include "Badumna/Core/BadumnaExceptionCallback.h"
#include "Badumna/Core/InternalErrorCallback.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    /**
     * Initialize the runtime using default values. The default path of the mono library is "mono" under the current 
     * working directory. The default assembly path is the current working directory. (Note: on iOS the default paths
     * change to "BadumnaResources/mono" and "BadumnaResources" respectively). This method will fail and the
     * default <code>BadumnaExceptionCallback</code> will be called if the runtime can not be initilaized. 
     *
     * @return A boolean value indicating whether the runtime has been successfully initialized.
     */
    BADUMNA_API void InitializeRuntime();

    /**
     * Initialize the runtime using default values and register the error callback delegates to handle errors. The 
     * default path of the mono library is "mono" under the current working directory. The default assembly path is the 
     * current working directory. This method will fail and the default <code>BadumnaExceptionCallback</code> will be 
     * called if the runtime can not be initilaized.
     *
     * @param exception_callback The exception callback delegate that will be called on error.
     * @param internal_error_callback The internal error delegate that will be called on Badumna bug. 
     */
    BADUMNA_API void InitializeRuntime(
        BadumnaExceptionCallback exception_callback, 
        InternalErrorCallback internal_error_callback);

    /**
     * Initialize the runtime by using the specified mono library path and assembly path. This method will fail and the 
     * default <code>BadumnaExceptionCallback</code> will be called if the runtime can not be initilaized.  
     *
     * @param mono_path The path to the mono library.
     * @param assembly_path The path to the Badumna.dll assembly file. 
     */
    BADUMNA_API void InitializeRuntime(String const &mono_path, String const &assembly_path = "Badumna.dll");
    
    /**
     * Initialize the runtime by using the specified mono library path and assembly path, then register the error 
     * callback delegates for handling errors. This method will fail and the specified <code>exception_callback</code>
     * will be called if the runtime can not be initialized.
     *
     * @param exception_callback The exception callback delegate that will be called on error. 
     * @param internal_error_callback The internal error delegate that will be called on Badumna bug. 
     * @param mono_path The path to the mono library.
     * @param assembly_path The path to the Badumna.dll assembly file. 
     */
    BADUMNA_API void InitializeRuntime(
        BadumnaExceptionCallback exception_callback,
        InternalErrorCallback internal_error_callback,
        String const &mono_path, 
        String const &assembly_path = "Badumna.dll");
    
    /**
     * Shutdown the runtime.
     */
    BADUMNA_API void ShutdownRuntime();

    /**
     * @class BadumnaRuntimeInitializer
     *
     * @brief RAII class used to initialize and shutdown the runtime environment. 
     *
     * A RAII class used to initialize and shutdown the runtime environment. This RAII class calls 
     * <code>InitializeRuntime</code> in its constructor and then calls <code>ShutdownRuntime</code> in its destructor.
     * The constructor may fail if the runtime can not be initialized. The default <code>BadumnaExceptionCallback</code>
     * or the specified <code>exception_callback</code> will be called on failure. 
     */
    class BADUMNA_API BadumnaRuntimeInitializer
    {
    public:
        /**
         * Constructor.
         */
        BadumnaRuntimeInitializer();

        /**
         * Constructor.
         * 
         * @param exception_callback The exception callback delegate that will be called on error.
         * @param internal_error_callback The internal error delegate that will be called on Badumna bug. 
         */
        BadumnaRuntimeInitializer(
            BadumnaExceptionCallback exception_callback, 
            InternalErrorCallback internal_error_callback);

        /**
         * Constructor.
         * 
         * @param mono_path The path to the mono library.
         * @param assembly_path The path to the Badumna.dll assembly file. 
         */
        BadumnaRuntimeInitializer(
            String const &mono_path, 
            String const &assembly_path = "Badumna.dll");

        /**
         * @param exception_callback The exception callback delegate that will be called on error. 
         * @param internal_error_callback The internal error delegate that will be called on Badumna bug. 
         * @param mono_path The path to the mono library.
         * @param assembly_path The path to the Badumna.dll assembly file. 
         */
        BadumnaRuntimeInitializer(
            BadumnaExceptionCallback exception_callback,
            InternalErrorCallback internal_error_callback,
            String const &mono_path, 
            String const &assembly_path = "Badumna.dll");

        /**
         * Destrutor.
         */
        ~BadumnaRuntimeInitializer();

    private:
        DISALLOW_HEAP_OBJECT_ALLOCATION;
        DISALLOW_COPY_AND_ASSIGN(BadumnaRuntimeInitializer);
    };

#ifndef DOXYGEN_SHOULD_SKIP_THIS

    BADUMNA_API void InitializeRuntimeFromConfigFile(
        String const &config_file_path, 
        String const &assembly_path = "Badumna.dll");

    BADUMNA_API void InitializeRuntimeFromConfigFile(
        BadumnaExceptionCallback exception_callback,
        InternalErrorCallback internal_error_callback,
        String const &config_file_path, 
        String const &assembly_path = "Badumna.dll");

    /**
     * This is a undocumented feature that will be changed in future releases. 
     */
    class BADUMNA_API BadumnaRuntimeInitializer2
    {
    public:
        BadumnaRuntimeInitializer2(
            String const &config_file_path, 
            String const &assembly_path = "Badumna.dll");

        BadumnaRuntimeInitializer2(
            BadumnaExceptionCallback exception_callback,
            InternalErrorCallback internal_error_callback,
            String const &config_file_path, 
            String const &assembly_path = "Badumna.dll");

        ~BadumnaRuntimeInitializer2();
    private:
        DISALLOW_HEAP_OBJECT_ALLOCATION;
        DISALLOW_COPY_AND_ASSIGN(BadumnaRuntimeInitializer2);
    };
#endif // DOXYGEN_SHOULD_SKIP_THIS

}

#endif // BADUMNA_RUNTIME_INITIALIZER_H