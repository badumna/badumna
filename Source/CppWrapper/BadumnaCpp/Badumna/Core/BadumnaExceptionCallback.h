//---------------------------------------------------------------------------------
// <copyright file="BadumnaExceptionCallback.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_EXCEPTION_CALLBACK_H
#define BADUMNA_EXCEPTION_CALLBACK_H

#include "Badumna/Core/Delegate.h"
#include "Badumna/DataTypes/String.h"

namespace Badumna
{
    /**
     * @class BadumnaExceptionCallback
     * @brief The delegate invoked when there is logic error in user code. 
     *
     * The BadumnaExceptionCallback delegate is called when there is a logic error in user code. The Badumna CPP 
     * library is built without using C++ exceptions, the error handling is done by registering a BadumnaExceptionCallback
     * delegate to the network facade object. The delegate has the following signature:\n
     * \n
     * <code>void function_name(String const &stack_trace, String const &exception_type_name, String const &message);</code>\n
     * \n
     */ 
    class BADUMNA_API BadumnaExceptionCallback : public Delegate3<void, String const &, String const &, String const &>
    {
    public:
        /**
         * Constructor that constructs a BadumnaExceptionCallback with specified pointer to member function and 
         * object. 
         *
         * @tparam K Object type.
         * @param fp A pointer to a member function.
         * @param object The object. 
         */
        template<typename K>
        BadumnaExceptionCallback(void (K::*fp)(String const &, String const &, String const &), K &object) 
            : Delegate3<void, String const &, String const &, String const &>(fp, object)
        {
        }

        /**
         * Constructor that constructs a BadumnaExceptionCallback with specified pointer to free function.
         *
         * @param fp A pointer to a free function.
         */
        BadumnaExceptionCallback(void (*fp)(String const &, String const &, String const &)) 
            : Delegate3<void, String const &, String const &, String const &>(fp)
        {
        }
    };
}

#endif // BADUMNA_EXCEPTION_CALLBACK_H