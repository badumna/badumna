//---------------------------------------------------------------------------------
// <copyright file="EntityManager.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include "Badumna/Core/EntityManager.h"
#include "Badumna/Utils/Logger.h"

namespace Badumna
{
    EntityManager::EntityManager()
        : original_map_(),
        replica_map_()
    {
    }
    
    EntityManager::~EntityManager()
    {
        SpitialOriginalMap::Iterator iter = original_map_.Begin();
        while(iter != original_map_.End())
        {
            delete iter->second;
            iter++;
        }

        SpatialReplicaMap::Iterator iter2 = replica_map_.Begin();
        while(iter2 != replica_map_.End())
        {
            delete iter2->second;
            iter2++;
        }
    }

    void EntityManager::InvokeDeserialize(
        UniqueId const &replica_id, 
        BooleanArray const &included_parts, 
        InputStream *stream, 
        int estimated_milliseconds_since_departure) const
    {
        BADUMNA_LOG_DEBUG("EntityManager::InvokeDeserialize, id: " << replica_id.UTF8CStr());
        assert(stream != NULL);

        ReplicaWrapper* replica_ptr = replica_map_.GetValue(replica_id);
        replica_ptr->Deserialize(included_parts, stream, estimated_milliseconds_since_departure);
    }

    void EntityManager::InvokeSerialize(
        UniqueId const &unique_id, 
        BooleanArray const &included_parts, 
        OutputStream *stream) const
    {
        BADUMNA_LOG_DEBUG("EntityManager::InvokeSerialize, id: " << unique_id.UTF8CStr());
        assert(stream != NULL);

        OriginalWrapper* original = original_map_.GetValue(unique_id);
        original->Serialize(included_parts, stream);
    }

    void EntityManager::InovkeSetReplicaProperty(
        UniqueId const &replica_id, 
        int index, 
        DotNetObject value) const
    {
        ReplicaWrapper *replica = replica_map_.GetValue(replica_id);
        replica->SetReplicaProperty(index, value);
    }

    void EntityManager::InovkeSetReplicaProperty(
        UniqueId const &replica_id, 
        int index, 
        float x, 
        float y, 
        float z) const
    {
        ReplicaWrapper *replica = replica_map_.GetValue(replica_id);
        replica->SetReplicaProperty(index, x, y, z);
    }

    // original
    void EntityManager::AddOriginal(UniqueId const &id, OriginalWrapper *original)
    {
        BADUMNA_LOG_INFO("EntityManager::AddOriginal, id: " << id.UTF8CStr());
        assert(original != NULL);
        original_map_.TryAdd(id, original);
    }

    OriginalWrapper *EntityManager::GetOriginal(UniqueId const &id) const
    {
        BADUMNA_LOG_INFO("EntityManager::GetOriginal, id: " << id.UTF8CStr());
        return original_map_.GetValue(id);
    }

    bool EntityManager::ContainsOriginal(UniqueId const &id) const
    {
        BADUMNA_LOG_INFO("EntityManager::ContainsOriginal, id: " << id.UTF8CStr());
        return original_map_.Contains(id);
    }

    bool EntityManager::ContainsOriginal(ISpatialOriginal const &original) const
    {
        SpitialOriginalMap::ConstIterator iter = original_map_.Begin();

        while(iter != original_map_.End())
        {
            if(iter->second->GetSpatialOriginal() == &original)
            {
                return true;
            }

            iter++;
        }

        return false;
    }

    void EntityManager::RemoveOriginal(UniqueId const &id)
    {
        BADUMNA_LOG_INFO("EntityManager::RemoveOriginal, id: " << id.UTF8CStr());
        OriginalWrapper *wrapper = original_map_.GetValue(id);
        original_map_.TryRemove(id);
        delete wrapper;
    }

    // replica
    void EntityManager::AddReplica(UniqueId const &id, ReplicaWrapper *replica)
    {
        BADUMNA_LOG_INFO("EntityManager::AddReplica, id: " << id.UTF8CStr());
        assert(replica != NULL);
        replica_map_.TryAdd(id, replica);
    }

    void EntityManager::RemoveReplica(UniqueId const &id)
    {
        BADUMNA_LOG_INFO("EntityManager::RemoveReplica, id: " << id.UTF8CStr());
        ReplicaWrapper* wrapper = replica_map_.GetValue(id);
        replica_map_.TryRemove(id);
        delete wrapper;
    }

    ReplicaWrapper *EntityManager::GetReplica(UniqueId const &id) const
    {
        return replica_map_.GetValue(id);
    }
    
    bool EntityManager::ContainsReplica(UniqueId const &id) const
    {
        return replica_map_.Contains(id);
    }

    bool EntityManager::ContainsReplica(ISpatialReplica const &replica) const
    {
        SpatialReplicaMap::ConstIterator iter = replica_map_.Begin();

        while(iter != replica_map_.End())
        {
            if(iter->second->GetReplica() == &replica)
            {
                return true;
            }

            iter++;
        }

        return false;
    }

    ReplicaWrapper *EntityManager::GetReplica(ISpatialReplica const &replica) const
    {
        SpatialReplicaMap::ConstIterator iter = replica_map_.Begin();

        while(iter != replica_map_.End())
        {
            if(iter->second->GetReplica() == &replica)
            {
                return iter->second;
            }

            iter++;
        }

        return NULL;
    }

    void EntityManager::InvokeHandleOriginalEvent(UniqueId const &original_id, InputStream *stream) const
    {
        BADUMNA_LOG_DEBUG("EntityManager::InvokeHandleOriginalEvent, id: " << original_id.UTF8CStr());
        assert(stream != NULL);

        OriginalWrapper *original = original_map_.GetValue(original_id);
        original->HandleEvent(stream);
    }
    
    void EntityManager::InvokeHandleReplicaEvent(UniqueId const &replica_id, InputStream *stream) const
    {
        BADUMNA_LOG_DEBUG("EntityManager::InvokeHandleReplicaEvent, id: " << replica_id.UTF8CStr());
        assert(stream != NULL);
        ReplicaWrapper *replica = replica_map_.GetValue(replica_id);
        replica->HandleEvent(stream);
    }

    void EntityManager::InvokeAttemptMovement(UniqueId const &replica_id, Vector3 position) const
    {
        BADUMNA_LOG_DEBUG("EntityManager::InvokeAttemptMovement, id: " << replica_id.UTF8CStr());
        ReplicaWrapper *replica = replica_map_.GetValue(replica_id);
        replica->AttemptMovement(position);
    }
}