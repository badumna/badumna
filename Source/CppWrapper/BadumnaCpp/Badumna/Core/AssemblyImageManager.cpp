//---------------------------------------------------------------------------------
// <copyright file="AssemblyImageManager.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <iostream>
#include <cassert>
#include <cstdlib>

#include "Badumna/Core/iOS.h"
#include "Badumna/Core/AssemblyImageManager.h"
#include "Badumna/Core/InternalErrorHandler.h"
#include "Badumna/Core/InternalErrorType.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/Assembly.h"
#include "Badumna/Utils/Logger.h"

namespace
{
    bool NameSpaceMatches(Badumna::String const &name_space, Badumna::String const &expected)
    {
        if (!name_space.Compare(0, expected.Length(), expected))
        {
            return true;
        }

        return false;
    }
}

namespace Badumna
{
    AssemblyImageManager::AssemblyImageManager()
        : badumna_image_(NULL),
        badumna_assembly_(NULL),
        dei_image_(NULL),
        dei_assembly_(NULL)
    {
    }

    bool AssemblyImageManager::Initialize(String const &path)
    {
        // should never be called twice.
        assert(badumna_image_ == NULL);

        badumna_assembly_ = mono_domain_assembly_open(mono_domain_get(), path.UTF8CStr());
        if(badumna_assembly_ == NULL)
        {
            BADUMNA_LOG_FATAL("failed to open the assembly.");
            return false;
        }

        badumna_image_ = mono_assembly_get_image(badumna_assembly_);
        if(badumna_image_ == NULL)
        {
            BADUMNA_LOG_FATAL("failed to get the image");       
            return false;
        }

        return true;
    }

    void AssemblyImageManager::Shutdown()
    {
    }

    MonoImage* AssemblyImageManager::GetImage(String const &name_space) const
    {
        if(::NameSpaceMatches(name_space, "Badumna") || ::NameSpaceMatches(name_space, "CppWrapperTestStub"))
        {
            return badumna_image_;
        }

        if(::NameSpaceMatches(name_space, "Dei"))
        {
            if (dei_image_ == NULL && !LoadDeiAssembly())
            {
                InternalErrorHandler::Instance().Invoke(InternalErrorType_UnknownNameSpace, "Dei.dll not found");
            }

            return dei_image_;
        }

        BADUMNA_LOG_FATAL("unknown namespace : " << name_space.UTF8CStr());
        InternalErrorHandler::Instance().Invoke(InternalErrorType_UnknownNameSpace, "Class is null");

        // will never reach here
        return NULL;
    }

    bool AssemblyImageManager::LoadDeiAssembly() const
    {
#if IOS
        String assembly_path_ = String(get_bundle_path()) + "/BadumnaResources/Dei.Unity.iOS.dll";
        dei_assembly_ = mono_domain_assembly_open(mono_domain_get(), assembly_path_.UTF8CStr());
#else
        dei_assembly_ = mono_domain_assembly_open(mono_domain_get(), "Dei.dll");
#endif

        if(dei_assembly_ == NULL)
        {
            BADUMNA_LOG_FATAL("failed to open the Dei assembly.");
            return false;
        }

        dei_image_ = mono_assembly_get_image(dei_assembly_);
        if(dei_image_ == NULL)
        {
            BADUMNA_LOG_FATAL("failed to open the Dei image.");
            return false;
        }

        return true;
    }
}