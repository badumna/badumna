//---------------------------------------------------------------------------------
// <copyright file="StatisticsTracker.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_STATISTICS_TRACKER_H
#define BADUMNA_STATISTICS_TRACKER_H

#include <memory>
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class StatisticsTrackerImpl;

    /**
     * @class StatisticsTracker
     * @brief A tracker for sending usage data to a specified server.
     * 
     * Regularly sends packets to a server to estimate usage statistics.
     */
    class BADUMNA_API StatisticsTracker
    {
        friend class NetworkFacadeImpl;
    public:
        /**
         * Destructor
         */
        ~StatisticsTracker();

        /**
         * Sets the user ID in the data packet.
         *
         * @param id The new user id.
         */
        void SetUserId(int64_t id);

        /**
         * Sets the payload string in the data packet. Tracking packets with matching payload strings are grouped 
         * together in the statistics on the tracking server. A typical use would be to set the payload to the name of 
         * the current scene.
         *
         * @param payload The new payload.
         */
        void SetPayload(String const &payload);
        
        /**
         * Starts the tracker.
         */
        void Start();

        /**
         * Stops the tracker.
         */
        void Stop();

    private:
        explicit StatisticsTracker(StatisticsTrackerImpl *imp);
        std::auto_ptr<StatisticsTrackerImpl> const impl_;

        DISALLOW_COPY_AND_ASSIGN(StatisticsTracker);
    };
}

#endif // BADUMNA_PRESENCE_TRACKER_H