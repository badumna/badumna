//---------------------------------------------------------------------------------
// <copyright file="NetworkFacade.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/Core/NetworkFacadeImpl.h"
#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/Security/Character.h"

namespace Badumna
{
    using Dei::IdentityProvider;

    NetworkFacade::NetworkFacade(Options const &options) 
        : impl_(BadumnaRuntime::Instance().CreateNetworkFacadeImpl(options))
    {
    }

    NetworkFacade::NetworkFacade(Badumna::String const &identifier) 
        : impl_(BadumnaRuntime::Instance().CreateNetworkFacadeImpl(identifier))
    {
    }

    NetworkFacade::~NetworkFacade()
    {
    }

    NetworkFacade *NetworkFacade::Create(Options const &options)
    {
        return new NetworkFacade(options);
    }

    NetworkFacade *NetworkFacade::Create(Badumna::String const &identifier)
    {
        return new NetworkFacade(identifier);
    }

    void NetworkFacade::AnnounceService(ServerType type) const
    {
        impl_->AnnounceService(static_cast<int>(type));
    }

    bool NetworkFacade::Login()
    {
        return impl_->Login();
    }

    bool NetworkFacade::Login(String const &character_name)
    {
        return impl_->Login(character_name);
    }

    bool NetworkFacade::Login(IdentityProvider const &identity_provider)
    {
        return impl_->Login(identity_provider);
    }

    void NetworkFacade::Shutdown()
    {
        return impl_->Shutdown();
    }

    void NetworkFacade::ProcessNetworkState()
    {
        return impl_->ProcessNetworkState();
    }

    NetworkStatus NetworkFacade::GetNetworkStatus() const
    {
        return impl_->GetNetworkStatus();
    }

    NetworkScene *NetworkFacade::JoinScene(
        String const &scene_name, 
        CreateSpatialReplicaDelegate create_delegate, 
        RemoveSpatialReplicaDelegate remove_delegate)
    {
        return impl_->JoinScene(scene_name, create_delegate, remove_delegate);
    }

    NetworkScene *NetworkFacade::JoinMiniScene(
        String const &scene_name, 
        CreateSpatialReplicaDelegate create_delegate, 
        RemoveSpatialReplicaDelegate remove_delegate)
    {
        return impl_->JoinMiniScene(scene_name, create_delegate, remove_delegate);
    }

    void NetworkFacade::FlagForUpdate(ISpatialOriginal const &local_entity, BooleanArray const &changed_parts) const
    {
        impl_->FlagForUpdate(local_entity, changed_parts);
    }

    void NetworkFacade::FlagForUpdate(ISpatialOriginal const &local_entity, int changed_part_index) const
    {
        impl_->FlagForUpdate(local_entity, changed_part_index);
    }

    void NetworkFacade::SendCustomMessageToRemoteCopies(
        ISpatialOriginal const &local_entity, 
        OutputStream *event_data) const
    {
        impl_->SendCustomMessageToRemoteCopies(local_entity, event_data);
    }
    
    void NetworkFacade::SendCustomMessageToOriginal(
        ISpatialReplica const &remote_entity, 
        OutputStream *event_data) const
    {
        impl_->SendCustomMessageToOriginal(remote_entity, event_data);
    }

    void NetworkFacade::RegisterEntityDetails(
        float area_of_interest_radius, 
        float max_speed)
    {
        impl_->RegisterEntityDetails(area_of_interest_radius, max_speed);
    }

    IChatSession *NetworkFacade::ChatSession() const
    {
        return impl_->ChatSession();
    }

    std::auto_ptr<Character> NetworkFacade::GetCharacter() const
    {
        return impl_->GetCharacter();
    }

    StatisticsTracker *NetworkFacade::CreateTracker(
            String const &server_address, 
            int port,
            int interval_seconds, 
            String const &initial_payload) const
    {
        return impl_->CreateTracker(server_address, port, interval_seconds, initial_payload);
    }

    IArbitrator *NetworkFacade::GetArbitrator(String const &name) const
    {
        return impl_->GetArbitrator(name);
    }

    void NetworkFacade::RegisterArbitrationHandler(
        ArbitrationClientMessageDelegate client_message_delegate, 
        int timeout_seconds, 
        ArbitrationClientDisconnectDelegate client_disconnect_delegate)
    {
        impl_->RegisterArbitrationHandler(client_message_delegate, timeout_seconds, client_disconnect_delegate);
    }

    void NetworkFacade::SendServerArbitrationEvent(int32_t destination_session_id, OutputStream *stream) const
    {
        impl_->SendServerArbitrationEvent(destination_session_id, stream);
    }

    int64_t NetworkFacade::GetUserIdForSession(int32_t session_id) const
    {
        return impl_->GetUserIdForSession(session_id);
    }

    std::auto_ptr<Badumna::Character> NetworkFacade::GetCharacterForArbitrationSession(int32_t session_id) const
    {
        return impl_->GetCharacterForArbitrationSession(session_id);
    }

    BadumnaId NetworkFacade::GetBadumnaIdForArbitrationSession(int32_t session_id) const
    {
        return impl_->GetBadumnaIdForArbitrationSession(session_id);
    }
    
    StreamingManager *NetworkFacade::GetStreamingManager() const
    {
        return impl_->GetStreamingManager();
    }

    std::auto_ptr<Badumna::MatchFacade> NetworkFacade::GetMatchFacade() const
    {
        return impl_->GetMatchFacade();
    }

    bool NetworkFacade::IsTunnelled() const
    {
        return impl_->IsTunnelled();
    }
    
    bool NetworkFacade::IsLoggedIn() const
    {
        return impl_->IsLoggedIn();
    }

    bool NetworkFacade::IsFullyConnected() const
    {
        return impl_->IsFullyConnected();
    }
    
    bool NetworkFacade::IsOffline() const
    {
        return impl_->IsOffline();
    }
    
    bool NetworkFacade::IsOnline() const
    {
        return impl_->IsOnline();
    }

    double NetworkFacade::GetOutboundBytesPerSecond() const
    {
        return impl_->GetOutboundBytesPerSecond();
    }

    double NetworkFacade::GetInboundBytesPerSecond() const
    {
        return impl_->GetInboundBytesPerSecond();
    }
    
    double NetworkFacade::GetMaximumPacketLossRate() const
    {
        return impl_->GetMaximumPacketLossRate();
    }

    double NetworkFacade::GetAveragePacketLossRate() const
    {
        return impl_->GetAveragePacketLossRate();
    }

    double NetworkFacade::GetTotalSendLimitBytesPerSecond() const
    {
        return impl_->GetTotalSendLimitBytesPerSecond();
    }

    double NetworkFacade::GetMaximumSendLimitBytesPerSecond() const
    {
        return impl_->GetMaximumSendLimitBytesPerSecond();
    }

    void NetworkFacade::SetOnlineEventDelegate(ConnectivityStatusDelegate online_delegate)
    {
        impl_->SetOnlineEventDelegate(online_delegate);
    }
    
    void NetworkFacade::SetOfflineEventDelegate(ConnectivityStatusDelegate offline_delegate)
    {
        impl_->SetOfflineEventDelegate(offline_delegate);
    }

    void NetworkFacade::SetAddressChangedEventDelegate(ConnectivityStatusDelegate address_change_delegate)
    {
        impl_->SetAddressChangedEventDelegate(address_change_delegate);
    }

    Vector3 NetworkFacade::GetDestination(IDeadReckonableSpatialReplica const &remote_entity) const
    {
        return impl_->GetDestination(remote_entity);
    }
    
    void NetworkFacade::SnapToDestination(IDeadReckonableSpatialReplica *remote_entity) const
    {
        impl_->SnapToDestination(remote_entity);
    }

    void NetworkFacade::RegisterController(String const &type_name, CreateControllerDelegate creator)
    {
        impl_->RegisterController(type_name, creator);
    }
    
    String NetworkFacade::StartController(String const &scene_name, String const &type_name, uint32_t max)
    {
        return impl_->StartController(scene_name, type_name, max);
    }

    void NetworkFacade::StopController(String const &unique_name)
    {
        impl_->StopController(unique_name);
    }
}
