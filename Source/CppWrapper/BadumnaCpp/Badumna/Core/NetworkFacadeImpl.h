//---------------------------------------------------------------------------------
// <copyright file="NetworkFacadeImpl.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_NETWORK_FACADE_IMPL_H
#define BADUMNA_NETWORK_FACADE_IMPL_H

#include <memory>

#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Core/BadumnaStack.h"
#include "Badumna/Core/ProxyObject.h"
#include "Badumna/Core/NetworkStatus.h"
#include "Badumna/Core/ConnectivityStatusDelegate.h"
#include "Badumna/Core/StatisticsTracker.h"
#include "Badumna/Core/EntityManager.h"
#include "Badumna/Chat/IChatSession.h"
#include "Badumna/Security/Character.h"
#include "Badumna/Configuration/Options.h"
#include "Badumna/Controller/CreateControllerDelegate.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Dei/IdentityProvider.h"
#include "Badumna/Replication/SpatialReplicaDelegate.h"
#include "Badumna/Replication/IDeadReckonableSpatialOriginal.h"
#include "Badumna/Replication/IDeadReckonableSpatialReplica.h"
#include "Badumna/Arbitration/IArbitrator.h"
#include "Badumna/Arbitration/ArbitrationServerImpl.h"
#include "Badumna/Arbitration/ArbitrationDelegates.h"
#include "Badumna/Streaming/StreamingManager.h"
#include "Badumna/Streaming/StreamingServiceManager.h"
#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/Match/MatchFacade.h"

namespace Badumna
{
    class NetworkFacadeImpl : public ProxyObject    
    {
        friend class BadumnaRuntime;
    public:
        void AnnounceService(int type) const;

        bool Login();
        bool Login(String const &character_name);
        bool Login(Dei::IdentityProvider const &identity_provider);

        void Shutdown(); 

        void ProcessNetworkState();

        NetworkStatus GetNetworkStatus() const;

        NetworkScene *JoinScene(
            String const &scene_name, 
            CreateSpatialReplicaDelegate create_delegate, 
            RemoveSpatialReplicaDelegate remove_delegate);

        NetworkScene *JoinMiniScene(
            String const &scene_name, 
            CreateSpatialReplicaDelegate create_delegate, 
            RemoveSpatialReplicaDelegate remove_delegate);

        void FlagForUpdate(ISpatialOriginal const &local_entity, BooleanArray const &changed_parts) const;
        void FlagForUpdate(ISpatialOriginal const &local_entity, int changed_part_index) const;

        void SendCustomMessageToRemoteCopies(ISpatialOriginal const &local_entity, OutputStream *event_data) const;
        void SendCustomMessageToOriginal(ISpatialReplica const &remote_entity, OutputStream *event_data) const;

        void RegisterEntityDetails(float area_of_interest_radius, float max_speed);

        IChatSession *ChatSession();
        std::auto_ptr<Character> GetCharacter() const;

        IArbitrator *GetArbitrator(String const &name) const;

        void RegisterArbitrationHandler(
            ArbitrationClientMessageDelegate client_message_delegate, 
            int timeout_seconds, 
            ArbitrationClientDisconnectDelegate client_disconnect_delegate);

        void SendServerArbitrationEvent(int32_t destination_session_id, OutputStream *stream) const;
        int64_t GetUserIdForSession(int32_t session_id) const;
        std::auto_ptr<Character> GetCharacterForArbitrationSession(int32_t session_id) const;
        BadumnaId GetBadumnaIdForArbitrationSession(int32_t session_id) const;
        
        StatisticsTracker* CreateTracker(
            String const &server_address, 
            int port,
            int interval_seconds, 
            String const &initial_payload) const;

        void RegisterController(String const &type_name, CreateControllerDelegate creator);
        String StartController(String const &scene_name, String const &type_name, uint32_t max);
        void StopController(String const &unique_name);

        StreamingManager *GetStreamingManager() const;

        std::auto_ptr<MatchFacade> GetMatchFacade() const;

        // session status
        bool IsTunnelled() const;
        bool IsLoggedIn() const;
        bool IsFullyConnected() const;
        bool IsOffline() const;
        bool IsOnline() const;

        // traffic stats and limits
        double GetOutboundBytesPerSecond() const;
        double GetInboundBytesPerSecond() const;
        double GetMaximumPacketLossRate() const;
        double GetAveragePacketLossRate() const;
        double GetTotalSendLimitBytesPerSecond() const;
        double GetMaximumSendLimitBytesPerSecond() const;

        // connectivity status delegates
        void SetOnlineEventDelegate(ConnectivityStatusDelegate online_delegate);
        void SetOfflineEventDelegate(ConnectivityStatusDelegate offline_delegate);
        void SetAddressChangedEventDelegate(ConnectivityStatusDelegate address_change_delegate);

        Vector3 GetDestination(IDeadReckonableSpatialReplica const &remote_entity) const;
        void SnapToDestination(IDeadReckonableSpatialReplica *remote_entity) const;

    private:
        NetworkFacadeImpl(MonoRuntime *mono_runtime, BadumnaStack *stack, Options const *options, String const *identifier);

        DistributedControllerManager *ControllerService() const
        {
            return badumna_stack_->ControllerService();
        }

        ReplicationServiceManager *ReplicationService() const
        {
            return badumna_stack_->ReplicationService();
        }

        ArbitrationServiceManager *ArbitrationService() const
        {
            return badumna_stack_->ArbitrationService();
        }

        EntityManager *EntityService() const
        {
            return badumna_stack_->EntityService();
        }

        StreamingServiceManager *StreamingService() const
        {
            return badumna_stack_->StreamingService();
        }

        bool ContainsOriginal(ISpatialOriginal const &local_entity) const;
        DotNetObject GetManagedOriginal(ISpatialOriginal const &local_entity) const;

        bool ContainsReplica(ISpatialReplica const &remote_entity) const;
        DotNetObject GetManagedReplica(ISpatialReplica const &remote_entity) const;

        // doesn't own this object
        BadumnaStack * const badumna_stack_;

        std::auto_ptr<ArbitrationServerImpl> arbitration_server_impl_;
        std::auto_ptr<IChatSession> chat_session_;

        DISALLOW_COPY_AND_ASSIGN(NetworkFacadeImpl);
    };
}

#endif // BADUMNA_NETWORK_FACADE_IMPL_H
