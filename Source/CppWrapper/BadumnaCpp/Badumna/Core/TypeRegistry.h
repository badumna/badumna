//---------------------------------------------------------------------------------
// <copyright file="TypeRegistry.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_TYPE_REGISTRY_H
#define BADUMNA_TYPE_REGISTRY_H

#include <vector>
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/Singleton.h"

namespace Badumna
{
    // The type registry module is used to check whether the specified badumna.dll assembly contains all required 
    // types, methods, properties. In other words, it checks whether the C++ runtime and the assembly is compatible.
    //
    // Types get registered during static variable initialization phase, the type/method/property availability check is
    // performed when InitializeRuntime is called during run-time. 
    // 
    // The current implementation couldn't check whether all classes/methods/properties exposed from Badumna.dll are
    // covered in the wrapper. I think it will be helpful to make sure the entire badumna.dll interface is covered
    // in ApiExamples and then port all these examples to C++. 
    struct ClassDetails
    {
        ClassDetails(String const &name_space, String const &type_name)
            : name_space(name_space),
            type_name(type_name)
        {
        }

        String name_space;
        String type_name;
    };

    struct MethodDetails
    {
        MethodDetails(String const &name_space, String const &type_name, String const &method_name, int count)
            : name_space(name_space),
            type_name(type_name),
            method_name(method_name),
            count(count)
        {
        }

        String name_space;
        String type_name;
        String method_name;
        int count;
    };

    struct PropertyDetails
    {
        PropertyDetails(String const &name_space, String const &type_name, String const &pname)
            : name_space(name_space),
            type_name(type_name),
            pname(pname)
        {
        }

        String name_space;
        String type_name;
        String pname;
    };

    // all required types, methods and properties will be registered here
    class TypeRegistry : public Singleton<TypeRegistry>
    {
        friend class Singleton<TypeRegistry>;
    public:
        TypeRegistry();

        void RegisterType(String const &name_space, String const &type_name);
        std::vector<ClassDetails> GetClassDetails() const;
        
        void RegisterMethod(String const &name_space, String const &type_name, String const &method_name, int count);
        std::vector<MethodDetails> GetMethodDetails() const;

        void RegisterProperty(String const &name_space, String const &type_name, String const &pname);
        std::vector<PropertyDetails> GetPropertyDetails() const;

    private:
        std::vector<ClassDetails> class_details;
        std::vector<MethodDetails> method_details;
        std::vector<PropertyDetails> property_details;
    };

    struct TypeRegisteryClassTypeHelperObject 
    {
        TypeRegisteryClassTypeHelperObject(String const &name_space, String const &type_name)
        {
            TypeRegistry::Instance().RegisterType(name_space, type_name);
        }
    };

    struct TypeRegisteryMethodHelperObject
    {
        TypeRegisteryMethodHelperObject(
            String const &name_space, 
            String const &type_name, 
            String const &method_name, 
            int count)
        {
            TypeRegistry::Instance().RegisterMethod(name_space, type_name, method_name, count);
        }
    };

    struct TypeRegisteryPropertyHelperObject
    {
        TypeRegisteryPropertyHelperObject(
            String const &name_space, 
            String const &type_name, 
            String const &pname)
        {
            TypeRegistry::Instance().RegisterProperty(name_space, type_name, pname);
        }
    };
}

#ifndef NDEBUG
// Register managed types. 
#define REGISTER_MANAGED_MONO_TYPE(NAMESPACE, TYPENAME)                                                             \
namespace Badumna {                                                                                                 \
struct ClassTypeRegisteryHelper ## NAMESPACE ## TYPENAME                                                            \
    {                                                                                                               \
        static struct TypeRegisteryClassTypeHelperObject object;                                                    \
    };                                                                                                              \
struct TypeRegisteryClassTypeHelperObject                                                                           \
    ClassTypeRegisteryHelper ## NAMESPACE ## TYPENAME::object(#NAMESPACE, #TYPENAME);                               \
}

// Register member methods
#define REGISTER_MANAGED_METHOD(NAMESPACE, TYPENAME, METHODNAME, COUNT)                                             \
namespace Badumna {                                                                                                 \
struct MethodTypeRegisteryHelper ## NAMESPACE ## TYPENAME ## METHODNAME ## COUNT                                    \
    {                                                                                                               \
        static struct TypeRegisteryMethodHelperObject object;                                                       \
    };                                                                                                              \
struct TypeRegisteryMethodHelperObject                                                                              \
    MethodTypeRegisteryHelper ## NAMESPACE ## TYPENAME ## METHODNAME ## COUNT::object(#NAMESPACE, #TYPENAME, #METHODNAME, COUNT); \
}

// Register properties
#define REGISTER_PROPERTY(NAMESPACE, TYPENAME, PNAME)                                                               \
namespace Badumna {                                                                                                 \
struct PropertyTypeRegisteryHelper ## NAMESPACE ## TYPENAME ## PNAME                                                \
    {                                                                                                               \
        static struct TypeRegisteryPropertyHelperObject object;                                                     \
    };                                                                                                              \
struct TypeRegisteryPropertyHelperObject                                                                            \
    PropertyTypeRegisteryHelper ## NAMESPACE ## TYPENAME ## PNAME::object(#NAMESPACE, #TYPENAME, #PNAME);           \
}
#else
#define REGISTER_MANAGED_MONO_TYPE(NAMESPACE, TYPENAME)
#define REGISTER_MANAGED_METHOD(NAMESPACE, TYPENAME, METHODNAME, COUNT)
#define REGISTER_PROPERTY(NAMESPACE, TYPENAME, PNAME)
#endif // NDEBUG

#endif // BADUMNA_TYPE_REGISTRY_H