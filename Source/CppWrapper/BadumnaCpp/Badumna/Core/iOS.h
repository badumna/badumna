//---------------------------------------------------------------------------------
// <copyright file="iOS.h" company="National ICT Australia Ltd">
//     Copyright (c) 2013 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifdef __cplusplus
extern "C" {
#endif

void initialize_ios();
char *get_bundle_path();

#ifdef __cplusplus
}
#endif
