//---------------------------------------------------------------------------------
// <copyright file="ConnectivityStatusDelegate.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_CONNECTIVITY_STATUS_DELEGATE_H
#define BADUMNA_CONNECTIVITY_STATUS_DELEGATE_H

#include "Badumna/Core/Delegate.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    /**
     * @class ConnectivityStatusDelegate
     * @brief The delegate invoked on recieving notification of connectivity status changes. 
     *
     * The delegate get called on different connectivity related events. The delegate has the following signature:\n
     * \n
     * <code>void function_name();</code>\n
     * \n
     * It can be a member function or a free function. 
     */
    class BADUMNA_API ConnectivityStatusDelegate : public Delegate0<void>
    {
    public:
        /**
         * Constructor that constructs an ChatInvitationHandler with specified pointer to member function and object. 
         *
         * @tparam K Object type.
         * @param fp A pointer to a member function.
         * @param object The object. 
         */
        template<typename K>
        ConnectivityStatusDelegate(void (K::*fp)(), K &object) 
            : Delegate0<void>(fp, object)
        {
        }

        /**
         * Constructor that constructs an ChatInvitationHandler with specified pointer to free function.
         *
         * @param fp A pointer to a free function.
         */
        ConnectivityStatusDelegate(void (*fp)()) 
            : Delegate0<void>(fp)
        {
        }
    };
}

#endif // BADUMNA_CONNECTIVITY_STATUS_DELEGATE_H