//---------------------------------------------------------------------------------
// <copyright file="InternalErrorType.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_INTERNAL_ERROR_TYPE_H
#define BADUMNA_INTERNAL_ERROR_TYPE_H

namespace Badumna
{
    /**
     * @enum InternalErrorType
     *
     * Internal error is usually caused by Badumna bugs or when trying to use unsupported Badumna.dll assembly, e.g.
     * Badumna version 1.4.  
     * 
     */
    enum InternalErrorType
    {
        InternalErrorType_BadumnaMapInconsistency = 0,      /**< Inconsistency in BadumnaMap. */
        InternalErrorType_UnknownNameSpace,                 /**< Unknown namespace. */
        InternalErrorType_ManagedClassIsNull,               /**< NULL class. */
        InternalErrorType_ManagedObjectIsNull,              /**< NULL object. */
        InternalErrorType_ManagedMethodIsMissing,           /**< Method missing. */
        InternalErrorType_ManagedPropertyIsMissing          /**< Property missing. */
    };
}

#endif // BADUMNA_INTERNAL_ERROR_TYPE_H