//---------------------------------------------------------------------------------
// <copyright file="MonoRuntimeConfig.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <fstream>
#include <string>
#include "Badumna/Core/iOS.h"
#include "Badumna/Core/MonoRuntimeConfig.h"
#include "Badumna/Utils/Logger.h"
#include "Badumna/Utils/FileIO.h"

namespace Badumna
{
    using std::ifstream;
    using std::getline;

    MonoRuntimeConfig::MonoRuntimeConfig() :
        mono_lib_path_(),
        mono_etc_path_(),
        mono_config_file_path_(),
        assembly_path_(),
        exc_callback_(NULL),
        exc_callback_registered_(false)
    {
#ifdef IOS
        String bundle_path = get_bundle_path();
        mono_lib_path_ = bundle_path + "/BadumnaResources/mono/lib";
        mono_etc_path_ = bundle_path + "/BadumnaResources/mono/etc";
        mono_config_file_path_ = bundle_path + "/BadumnaResources/mono/etc/mono/config";
        assembly_path_ = bundle_path + "/BadumnaResources/Badumna.Unity.iOS.dll";
#else
        String current_dir = GetCurrentDir();
        mono_lib_path_ = current_dir + "\\mono\\lib";
        mono_etc_path_ = current_dir + "\\mono\\etc";
        mono_config_file_path_ = current_dir + "\\mono\\etc\\mono\\config";
        assembly_path_ = current_dir + "\\Badumna.dll";
#endif
    }

    void MonoRuntimeConfig::SetMonoPath(String const &path)
    {
        BADUMNA_LOG_DEBUG("SetMonoPath is called, path = " << path.UTF8CStr());

        String lib_path = "lib";
        String etc_path = "etc";

#ifdef WIN32
        String separator = "\\";
#else
        String separator = "/";
#endif

        mono_lib_path_ = path + separator + lib_path;
        mono_etc_path_ = path + separator + etc_path;
        mono_config_file_path_ = mono_etc_path_ + separator + "mono" +  separator + "config";
    }
    
    void MonoRuntimeConfig::SetAssemblyPath(String const &path)
    {
        assembly_path_ = path;
    }

    bool MonoRuntimeConfig::ConfigFromFile(String const &path)
    {
        std::string line;
        ifstream config_file(path.UTF8CStr());
        if(config_file.is_open())
        {
            while(config_file.good())
            {
                getline(config_file, line);
                
                if(line.length() > 0 && line.at(0) != '#')
                {
                    BADUMNA_LOG_DEBUG("Read the configuration from the mono config file: " << line);
                    SetMonoPath(line.c_str());
            
                    return true;
                }
            }
        }

        return false;
    }
}