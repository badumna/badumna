//---------------------------------------------------------------------------------
// <copyright file="EntityManager.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_ENTITY_MANAGER_H
#define BADUMNA_ENTITY_MANAGER_H

#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Core/BadumnaMap.h"
#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/DataTypes/OutputStream.h"
#include "Badumna/DataTypes/InputStream.h"
#include "Badumna/Replication/ISpatialReplica.h"
#include "Badumna/Replication/OriginalWrapper.h"
#include "Badumna/Replication/ReplicaWrapper.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class EntityManager
    {
    public:
        EntityManager();
        ~EntityManager();

        void AddOriginal(UniqueId const &id, OriginalWrapper *original);
        void RemoveOriginal(UniqueId const &id);
        bool ContainsOriginal(UniqueId const &id) const;
        bool ContainsOriginal(ISpatialOriginal const &original) const;
        OriginalWrapper *GetOriginal(UniqueId const &id) const;
        
        void AddReplica(UniqueId const &id, ReplicaWrapper *replica);
        void RemoveReplica(UniqueId const &id);

        ReplicaWrapper *GetReplica(ISpatialReplica const &replica) const;
        ReplicaWrapper *GetReplica(UniqueId const &id) const;

        bool ContainsReplica(ISpatialReplica const &replica) const;
        bool ContainsReplica(UniqueId const &id) const;

        void InvokeDeserialize(
            UniqueId const &replica_id, 
            BooleanArray const &included_parts, 
            InputStream *stream, 
            int estimated_milliseconds_since_departure) const;

        void InvokeSerialize(
            UniqueId const &unique_id, 
            BooleanArray const &included_parts, 
            OutputStream *stream) const;
        
        void InovkeSetReplicaProperty(UniqueId const &replica_id, int index, DotNetObject value) const;
        void InovkeSetReplicaProperty(UniqueId const &replica_id, int index, float x, float y, float z) const;

        void InvokeAttemptMovement(UniqueId const &unique_id, Vector3 position) const;

        void InvokeHandleOriginalEvent(UniqueId const &original_id, InputStream *stream) const;
        void InvokeHandleReplicaEvent(UniqueId const &replica_id, InputStream *stream) const;

    protected:
        typedef BadumnaMap<ReplicaWrapper*> SpatialReplicaMap;
        typedef BadumnaMap<OriginalWrapper*> SpitialOriginalMap;

        // originals
        SpitialOriginalMap original_map_;
        // replicas
        SpatialReplicaMap replica_map_;
        
    private:
        DISALLOW_COPY_AND_ASSIGN(EntityManager);
    };
}

#endif // BADUMNA_ENTITY_MANAGER_H