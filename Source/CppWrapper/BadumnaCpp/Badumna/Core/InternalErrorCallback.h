//---------------------------------------------------------------------------------
// <copyright file="InternalErrorCallback.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_INTERNAL_ERROR_CALLBACK_H
#define BADUMNA_INTERNAL_ERROR_CALLBACK_H

#include "Badumna/Core/Delegate.h"
#include "Badumna/DataTypes/String.h"

namespace Badumna
{
    /**
     * @class InternalErrorCallback 
     * @brief The delegate invoked when there is internal bug. 
     *
     * The InternalErrorCallback delegate is called when there is internal bug in Badumna. The program will be in 
     * undefined state after this callback method is invoke, thus application code can assume that the callback method 
     * will never return. The delegate has the following signature:\n
     * \n
     * <code>void function_name(int32_t error_code, String const &message);</code>\n
     * \n
     */ 
    class BADUMNA_API InternalErrorCallback : public Delegate2<void, int32_t, String const &>
    {
    public:
        /**
         * Constructor that constructs a InternalErrorCallback instance with specified pointer to member function and 
         * object. 
         *
         * @tparam K Object type.
         * @param fp A pointer to a member function.
         * @param object The object. 
         */
        template<typename K>
        InternalErrorCallback(void (K::*fp)(int32_t, String const &), K &object) 
            : Delegate2<void, int32_t, String const &>(fp, object)
        {
        }

        /**
         * Constructor that constructs a InternalErrorCallback instance with specified pointer to free function.
         *
         * @param fp A pointer to a free function.
         */
        InternalErrorCallback(void (*fp)(int32_t, String const &)) 
            : Delegate2<void, int32_t, String const &>(fp)
        {
        }
    };
}

#endif // BADUMNA_INTERNAL_ERROR_CALLBACK_H