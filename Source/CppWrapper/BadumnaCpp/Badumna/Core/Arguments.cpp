//---------------------------------------------------------------------------------
// <copyright file="Arguments.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include "Badumna/Core/Arguments.h"

namespace Badumna
{
    Arguments::Arguments()
        : index_(0),
        finalized_(false)
    {
        for(int i = 0; i < MAX_ARGS_NUM; i++)
        {
            args_[i] = NULL;
        }
    }

    void Arguments::AddArgument(void *p)
    {
        assert(!finalized_);
        assert(index_ < MAX_ARGS_NUM);

        args_[index_++] = p;
    }

    void Arguments::AddArgument(int *p)
    {
        AddArgument(static_cast<void *>(p));
    }

    void Arguments::AddArgument(float *p)
    {
        AddArgument(static_cast<void *>(p));
    }

    void Arguments::AddArgument(bool *p)
    {
        AddArgument(static_cast<void *>(p));
    }

    void Arguments::AddArgument(DotNetObject p)
    {
        AddArgument(static_cast<void *>(p));
    }
    
    void Arguments::AddArgument(DotNetString p)
    {
        AddArgument(static_cast<void *>(p));
    }

    void Arguments::AddArgument(DotNetArray p)
    {
        AddArgument(static_cast<void *>(p));
    }

    void Arguments::AddArgument(size_t *p)
    {
        AddArgument(static_cast<void *>(p));
    }

    void Arguments::AddNullArgument()
    {
        ++index_;
    }

    Arguments::operator void **() const
    {
        if(!finalized_)
        {
            finalized_ = true;
        }
    
        if(Count() > 0)
        {
            return const_cast<void **>(args_);
        }
        else
        {
            return NULL;
        }
    }
}