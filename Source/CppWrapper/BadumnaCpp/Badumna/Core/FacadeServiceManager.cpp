//---------------------------------------------------------------------------------
// <copyright file="FacadeServiceManager.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include "Badumna/Core/FacadeServiceManager.h"

namespace Badumna
{
    FacadeServiceManager::FacadeServiceManager()
        : delegate_map_()
    {
    }

    void FacadeServiceManager::SetOnlineEventDelegate(ConnectivityStatusDelegate online_delegate)
    {
        delegate_map_.AddOrReplace(OnlineEventDelegateType, online_delegate);
    }
    
    void FacadeServiceManager::SetOfflineEventDelegate(ConnectivityStatusDelegate offline_delegate)
    {
        delegate_map_.AddOrReplace(OfflineEventDelegateType, offline_delegate);
    }
    
    void FacadeServiceManager::SetAddressChangedEventDelegate(ConnectivityStatusDelegate address_change_delegate)
    {
        delegate_map_.AddOrReplace(AddressChangedDelegateType, address_change_delegate);
    }

    void FacadeServiceManager::InvokeOnlineEventDelegate() const
    {
        InvokeConnectivityDelegate(OnlineEventDelegateType);
    }
    
    void FacadeServiceManager::InvokeOfflineEventDelegate() const
    {
        InvokeConnectivityDelegate(OfflineEventDelegateType);
    }
    
    void FacadeServiceManager::InvokeAddressChangedEventDelegate() const
    {
        InvokeConnectivityDelegate(AddressChangedDelegateType);
    }

    void FacadeServiceManager::InvokeConnectivityDelegate(ConnectivityDelegateType type) const
    {
        ConnectivityStatusDelegate d = delegate_map_.GetValue(type);
        d();
    }
}