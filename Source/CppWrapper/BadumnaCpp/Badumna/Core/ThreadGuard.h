//---------------------------------------------------------------------------------
// <copyright file="ThreadGuard.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_THREAD_GUARD_H
#define BADUMNA_THREAD_GUARD_H

#include <memory>
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class ThreadGuardImpl;

    /**
     * @class ThreadGuard.
     * @brief Helper RAII class to attach/detach threads to the Mono runtime. 
     *
     * This is a helper RAII class used to attach/detach the calling thread to the Mono runtime. Each native thread 
     * that needs to access the Badumna library must be attached to the underlying Mono runtime and be detached before 
     * the thread joins. This is a required by the Mono runtime. ThreadGuard is a RAII helper class to do this.
     * The initial thread that initialized the badumna runtime doesn't need to use this helper class to register itself.
     */ 
    class BADUMNA_API ThreadGuard
    {
    public:
        /**
         * Constructor.
         */
        ThreadGuard();

        /**
         * Destructor
         */
        ~ThreadGuard();

    private:
        std::auto_ptr<ThreadGuardImpl> const impl_;

        DISALLOW_COPY_AND_ASSIGN(ThreadGuard);
    };
}

#endif // BADUMNA_THREAD_GUARD_H