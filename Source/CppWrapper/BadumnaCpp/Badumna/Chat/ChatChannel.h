//---------------------------------------------------------------------------------
// <copyright file="ChatChannel.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_CHAT_CHANNEL_H
#define BADUMNA_CHAT_CHANNEL_H

#include <memory>

#include "Badumna/Chat/IChatChannel.h"
#include "Badumna/Chat/ChatDelegates.h"
#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class ChatChannelImpl;
    class ChatSessionImpl;

    class BADUMNA_API ChatChannel : public IChatChannel
    {
        friend class ChatSessionManager;
        friend class ChatSessionImpl;

    public:
        ~ChatChannel();

        ChatChannelId Id();

        void SendMessage(String message);

        void Unsubscribe();

    private:
        explicit ChatChannel(ChatChannelImpl *impl, ChatSessionImpl *session_impl);

        std::auto_ptr<ChatChannelImpl> const impl_;
        ChatSessionImpl * const session_impl_;

        DISALLOW_COPY_AND_ASSIGN(ChatChannel);

        void InvokePresenceHandler(ChatChannel *self, BadumnaId const &user_id, String const &display_name, ChatStatus status);

        void InvokeMessageHandler(ChatChannel *self, BadumnaId const &sender, String const &message);
    };
}

#endif // BADUMNA_CHAT_CHANNEL_H