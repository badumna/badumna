//---------------------------------------------------------------------------------
// <copyright file="ChatChannel.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include "Badumna/Chat/IChatChannel.h"
#include "Badumna/Chat/ChatChannel.h"
#include "Badumna/Chat/ChatChannelImpl.h"
#include "Badumna/Chat/ChatSessionImpl.h"

namespace Badumna
{
    ChatChannel::ChatChannel(ChatChannelImpl *pimpl, ChatSessionImpl *session_impl)
        : impl_(pimpl),
        session_impl_(session_impl)
    {
        assert(pimpl != NULL);
    }

    ChatChannel::~ChatChannel()
    {
    }

    ChatChannelId ChatChannel::Id()
    {
        return impl_->Id();
    }

    void ChatChannel::SendMessage(String message)
    {
        impl_->SendMessage(message);
    }

    void ChatChannel::Unsubscribe()
    {
        session_impl_->RemoveChannel(this->Id());
        impl_->Unsubscribe();
        // We know that we are only ever created in a new() call inside ChatSessionImpl, and that
        // nobody should ever use a channel after it has been Unsubscribe()d, so this is OK:
        delete this;
    }

    void ChatChannel::InvokePresenceHandler(ChatChannel *self, BadumnaId const &user_id, String const &display_name, ChatStatus status)
    {
        impl_->InvokePresenceHandler(self, user_id, display_name, status);
    }

    void ChatChannel::InvokeMessageHandler(ChatChannel *self, BadumnaId const &sender, String const &message)
    {
        impl_->InvokeMessageHandler(self, sender, message);
    }
}