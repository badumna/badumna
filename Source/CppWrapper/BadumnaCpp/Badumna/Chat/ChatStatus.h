//---------------------------------------------------------------------------------
// <copyright file="ChatStatus.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_CHAT_STATUS_H
#define BADUMNA_CHAT_STATUS_H

namespace Badumna
{
    /**
     * @enum ChatStatus
     *
     * The status of a particular user.   
     */
    enum ChatStatus
    {
        ChatStatus_Online = 1,          /**< The user is online and available to chat. */
        ChatStatus_Offline = 2,         /**< The user is unavailable. Please note, application should not call 
                                        ChangePresence to set the presence status to offline.  */
        ChatStatus_Away = 3,            /**< The user is online but not present. */
        ChatStatus_Chat = 4,            /**< The user is online and actively wants to chat. */
        ChatStatus_DoNotDisturb = 5,    /**< The user is online but does not want to be disturbed. */
        ChatStatus_ExtendedAway = 6     /**< The user is online but in extended away status. */
    };
}

#endif // BADUMNA_CHAT_STATUS_H
