//---------------------------------------------------------------------------------
// <copyright file="ChatChannelIdImpl.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_CHAT_CHANNEL_ID_IMPL_H
#define BADUMNA_CHAT_CHANNEL_ID_IMPL_H

#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Chat/ChatChannelType.h"
#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/DataTypes/BadumnaIdImpl.h"

namespace Badumna
{
    class ChatChannelIdImpl : public ProxyObject
    {
        friend class BadumnaRuntime;
        friend class ChatChannelId;
    public:
        ~ChatChannelIdImpl() {}
        ChatChannelIdImpl& operator=(ChatChannelIdImpl const &rhs);
        
        ChatChannelType Type() const;
        String Name() const;
        void SetName(String const &val);

        bool operator ==(ChatChannelIdImpl const &rhs) const;
        bool operator !=(ChatChannelIdImpl const &rhs) const;
        bool operator < (ChatChannelIdImpl const &rhs) const;

    protected:
        ChatChannelIdImpl(MonoRuntime *runtime, DotNetObject object);
        
        explicit ChatChannelIdImpl(ChatChannelIdImpl const &other);
    };
}

#endif // BADUMNA_CHAT_CHANNEL_ID_IMPL_H