//---------------------------------------------------------------------------------
// <copyright file="ChatSessionImpl.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_CHAT_SESSION_IMPL_H
#define BADUMNA_CHAT_SESSION_IMPL_H

#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Core/ProxyObject.h"
#include "Badumna/Core/MonoRuntime.h"
#include "Badumna/Core/BadumnaMap.h"
#include "Badumna/Chat/ChatDelegates.h"
#include "Badumna/Chat/ChatChannelImpl.h"
#include "Badumna/Chat/IChatChannel.h"
#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/Utils/Optional.h"

namespace Badumna
{
    class ChatSessionManager;

    class ChatSessionImpl : public ProxyObject
    {
        friend class BadumnaRuntime;
    public:
        ~ChatSessionImpl();

        String Id() const;

        // proximity chat 
        IChatChannel * SubscribeToProximityChannel(
            BadumnaId const &entity_id, 
            ChatMessageHandler message_handler);
        
        // private chat
        void OpenPrivateChannels(ChatInvitationHandler invitation_handler);

        void InviteUserToPrivateChannel(String const &username);

        IChatChannel * AcceptInvitation(
            ChatChannelId const &channel, 
            ChatMessageHandler messageHandler, 
            ChatPresenceHandler presenceHandler);

        void ChangePresence(ChatStatus status);

        Optional<ChatChannel *> GetChannel(ChatChannelId const &channel_id);

        void RemoveChannel(ChatChannelId const &channel);

        void InvokeInvitationHandler(ChatChannelId const &channel_id, String const &username);

    private:
        ChatSessionImpl(
            MonoRuntime *runtime,
            ChatSessionManager *manager,
            DotNetObject object,
            String const &id);

        IChatChannel * StoreChannel(ChatChannelImpl * impl);

        ChatSessionManager * manager_;

        String unique_id_;

        BadumnaMap<ChatChannel*, ChatChannelId> channels_;

        Optional<ChatInvitationHandler> invitation_handler_;

        DISALLOW_COPY_AND_ASSIGN(ChatSessionImpl);
    };
}

#endif // BADUMNA_CHAT_SESSION_IMPL_H