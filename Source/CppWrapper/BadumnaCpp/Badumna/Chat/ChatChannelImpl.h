//---------------------------------------------------------------------------------
// <copyright file="ChatChannelImpl.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_CHAT_CHANNEL_IMPL_H
#define BADUMNA_CHAT_CHANNEL_IMPL_H

#include "Badumna/Core/DotNetTypes.h"
#include "Badumna/Core/ProxyObject.h"
#include "Badumna/Core/MonoRuntime.h"
#include "Badumna/Chat/ChatDelegates.h"
#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/Utils/Optional.h"

namespace Badumna
{
    class ChatChannel;

    class ChatChannelImpl : public ProxyObject
    {
        friend class BadumnaRuntime;
    public:
        ChatChannelId Id() const;

        void SendMessage(String message);
        
        void Unsubscribe();

        void InvokePresenceHandler(ChatChannel *channel, BadumnaId const &user_id, String const &display_name, ChatStatus status);

        void InvokeMessageHandler(ChatChannel *channel, BadumnaId const &sender, String const &message);

    private:
        ChatChannelImpl(
            MonoRuntime *runtime,
            DotNetObject object,
            Optional<ChatMessageHandler> message_handler,
            Optional<ChatPresenceHandler> presence_handler);

        Optional<ChatMessageHandler> message_handler_;
        Optional<ChatPresenceHandler> presence_handler_;

        DISALLOW_COPY_AND_ASSIGN(ChatChannelImpl);
    };
}

#endif // BADUMNA_CHAT_CHANNEL_IMPL_H