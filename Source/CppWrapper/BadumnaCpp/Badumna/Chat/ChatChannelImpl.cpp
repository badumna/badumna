//---------------------------------------------------------------------------------
// <copyright file="ChatChannelImpl.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>

#include "Badumna/Core/TypeRegistry.h"
#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/Chat/ChatChannelImpl.h"
#include "Badumna/Core/ManagedObjectGuard.h"
#include "Badumna/DataTypes/BadumnaIdImpl.h"
#include "Badumna/Utils/Optional.h"

namespace Badumna
{
    ChatChannelImpl::ChatChannelImpl(
        MonoRuntime *runtime, 
        DotNetObject object,
        Optional<ChatMessageHandler> message_handler,
        Optional<ChatPresenceHandler> presence_handler)
        : ProxyObject(runtime, object, "Badumna.Chat", "ChatChannel"),
        message_handler_(message_handler),
        presence_handler_(presence_handler)
    {
    }

    ChatChannelId ChatChannelImpl::Id() const
    {
      return BadumnaRuntime::Instance().CreateChatChannelId(managed_object_.GetPropertyFromName("Id"));
    }

    void ChatChannelImpl::InvokePresenceHandler(ChatChannel *channel, BadumnaId const &user_id, String const &display_name, ChatStatus status)
    {
        if(presence_handler_.IsSet())
        {
            presence_handler_.Get()((IChatChannel *)channel, user_id, display_name, status);
        }
    }

    void ChatChannelImpl::InvokeMessageHandler(ChatChannel *channel, BadumnaId const &sender, String const &message)
    {
        if(message_handler_.IsSet())
        {
            message_handler_.Get()((IChatChannel *)channel ,sender, message);
        }
    }

    REGISTER_MANAGED_METHOD(Chat, ChatChannel, SendMessage, 1)
    void ChatChannelImpl::SendMessage(String message)
    {
        Arguments args;
        args.AddArgument(MonoRuntime::GetString(message));
        managed_object_.InvokeMethod("SendMessage", args);
    }
    
    REGISTER_MANAGED_METHOD(Chat, ChatChannel, Unsubscribe, 0)
    void ChatChannelImpl::Unsubscribe()
    {
        managed_object_.InvokeMethod("Unsubscribe");
    }
}