//---------------------------------------------------------------------------------
// <copyright file="ChatChannelIdImpl.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include "Badumna/Chat/ChatChannelIdImpl.h"
#include "Badumna/Core/TypeRegistry.h"

namespace Badumna
{
    REGISTER_MANAGED_MONO_TYPE(Chat, ChatChannelId);
    ChatChannelIdImpl::ChatChannelIdImpl(MonoRuntime *runtime, DotNetObject object)
        : ProxyObject(runtime, object, "Badumna.Chat", "ChatChannelId")
    {
    }

    ChatChannelType ChatChannelIdImpl::Type() const
    {
        return static_cast<ChatChannelType>(managed_object_.GetPropertyFromName<int>("Type"));
    }

    REGISTER_PROPERTY(Chat, ChatChannelId, Name)
    String ChatChannelIdImpl::Name() const
    {   
        return managed_object_.GetPropertyFromName<String>("Name");
    }

    void ChatChannelIdImpl::SetName(String const &val)
    {
        managed_object_.SetPropertyValue("Name", val);
    }

    REGISTER_MANAGED_METHOD(Chat, ChatChannelId, Equals, 1)
    bool ChatChannelIdImpl::operator==(ChatChannelIdImpl const &rhs) const
    {
        Arguments args;
        args.AddArgument(rhs.managed_object_.GetManagedObject());
        
        bool result = managed_object_.InvokeMethod<bool>("Equals", args);
        return result;
    }

    bool ChatChannelIdImpl::operator !=(ChatChannelIdImpl const &rhs) const
    {
        return !ChatChannelIdImpl::operator ==(rhs);
    }

    REGISTER_MANAGED_METHOD(Chat, ChatChannelId, CompareTo, 1)
    bool ChatChannelIdImpl::operator<(ChatChannelIdImpl const &rhs) const
    {
        Arguments args;
        args.AddArgument(rhs.managed_object_.GetManagedObject());
        
        int result = managed_object_.InvokeMethod<int>("CompareTo", args);
        return result < 0;
    }

    DEFINE_WRAPPER_CLASS_IMPL_COPYCTOR_ASSIGN_OPERATOR(ChatChannelIdImpl)
}