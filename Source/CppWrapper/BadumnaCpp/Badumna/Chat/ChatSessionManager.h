//---------------------------------------------------------------------------------
// <copyright file="ChatSessionManager.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_CHAT_SESSION_MANAGER_H
#define BADUMNA_CHAT_SESSION_MANAGER_H

#include "Badumna/Core/BadumnaMap.h"
#include "Badumna/Chat/ChatDelegates.h"
#include "Badumna/Chat/ChatChannelId.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class ChatChannel;

    class ChatSessionImpl;

    class ChatSessionManager
    {
    public:
        ChatSessionManager();
        
        ~ChatSessionManager();

        void AddSession(ChatSessionImpl* session);

        void RemoveSession(ChatSessionImpl* session);

        void InvokePresenceHandler(UniqueId const &session_id, ChatChannelId const &channel_id, BadumnaId const &user_id, String const &display_name, ChatStatus status);

        void InvokeMessageHandler(UniqueId const &session_id, ChatChannelId const &channel_id, BadumnaId const &sender, String const &message);

        void InvokeInvitationHandler(UniqueId const &session_id, ChatChannelId const &channel_id, String const &sender_name);

    private:
        Optional<ChatSessionImpl *> GetSession(UniqueId const &id);

        Optional<ChatChannel *> GetChannel(UniqueId const &session_id, ChatChannelId const &channel_id);

        BadumnaMap<ChatSessionImpl*> session_map_;

        DISALLOW_COPY_AND_ASSIGN(ChatSessionManager);
    };
}

#endif // BADUMNA_CHAT_SESSION_MANAGER_H