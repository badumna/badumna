//---------------------------------------------------------------------------------
// <copyright file="ChatDelegates.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_CHAT_DELEGATES_H
#define BADUMNA_CHAT_DELEGATES_H

#include "Badumna/Core/Delegate.h"
#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Chat/ChatChannelId.h"
#include "Badumna/Chat/ChatStatus.h"

namespace Badumna
{
    class IChatChannel;

    /**
     * @class ChatMessageHandler
     * @brief The delegate invoked on received chat messages.
     *
     * A delegate for notifying the arrival of a chat message. The delegate has the following signature:\n
     * \n
     * <code>void function_name(IChatChannel *channel, BadumnaId const& sender_id, String const &message);</code>\n
     * \n
     * It can be a member function or a free function. 
     */
    class BADUMNA_API ChatMessageHandler : public Delegate3<void, IChatChannel *, BadumnaId const &, String const &>
    {
    public:
        /**
         * Constructor that constructs an ChatMessageHandler with specified pointer to member function and object. 
         *
         * @tparam K Object type.
         * @param fp A pointer to a member function.
         * @param object The object. 
         */
        template<typename K>
        ChatMessageHandler(void (K::*fp)(IChatChannel *, BadumnaId const &, String const &), K &object) 
            : Delegate3<void, IChatChannel *, BadumnaId const &, String const &>(fp, object)
        {
        }

        /**
         * Constructor that constructs an ChatMessageHandler with specified pointer to free function.
         *
         * @param fp A pointer to a free function.
         */
        ChatMessageHandler(void (*fp)(IChatChannel *, BadumnaId const &, String const &)) 
            : Delegate3<void, IChatChannel *, BadumnaId const &, String const &>(fp)
        {
        }
    };

    /**
     * @class ChatPresenceHandler
     * @brief The delegate invoked on received presence update.
     *
     * A delegate for notification of a presence event. The delegate has the following signature:\n
     * \n
     * <code>void function_name(ChatChannelId const &channel_id, BadumnaId const& sender_id, String const &username, 
     * ChatStatus status);</code>\n
     * \n
     * It can be a member function or a free function. 
     */
    class BADUMNA_API ChatPresenceHandler 
        : public Delegate4<void, IChatChannel *, BadumnaId const &, String const &, ChatStatus>
    {
    public:
        /**
         * Constructor that constructs an ChatPresenceHandler with specified pointer to member function and object. 
         *
         * @tparam K Object type.
         * @param fp A pointer to a member function.
         * @param object The object. 
         */
        template<typename K>
        ChatPresenceHandler(void (K::*fp)(IChatChannel *, BadumnaId const &, String const &, ChatStatus), K &object)
            : Delegate4<void, IChatChannel *, BadumnaId const &, String const &, ChatStatus>(fp, object)
        {
        }

        /**
         * Constructor that constructs an ChatPresenceHandler with specified pointer to free function.
         *
         * @param fp A pointer to a free function.
         */
        ChatPresenceHandler(void (*fp)(IChatChannel *, BadumnaId const &, String const &, ChatStatus))
            : Delegate4<void, IChatChannel *, BadumnaId const &, String const &, ChatStatus>(fp)
        {
        }
    };

    /**
     * @class ChatInvitationHandler
     * @brief The delegate invoked on received chat invitation. 
     *
     * A delegate for notification of a request by the given user for a private channel subscription. A call to 
     * <code>AcceptInvitation()</code> should be made with the given channel id to accept the invitation, otherwise the 
     * invitation is implicitly rejected. The delegate has the following signature:\n
     * \n
     * <code>void function_name(ChatChannelId const &channel_id, String const &username);</code>\n
     * \n
     * It can be a member function or a free function. 
     */
    class BADUMNA_API ChatInvitationHandler : public Delegate2<void, ChatChannelId const &, String const &>
    {
    public:
        /**
         * Constructor that constructs an ChatInvitationHandler with specified pointer to member function and object. 
         *
         * @tparam K Object type.
         * @param fp A pointer to a member function.
         * @param object The object. 
         */
        template<typename K>
        ChatInvitationHandler(void (K::*fp)(ChatChannelId const &, String const &), K &object)
            : Delegate2<void, ChatChannelId const &, String const &>(fp, object)
        {
        }

        /**
         * Constructor that constructs an ChatInvitationHandler with specified pointer to free function.
         *
         * @param fp A pointer to a free function.
         */
        ChatInvitationHandler(void (*fp)(ChatChannelId const &, String const &))
            : Delegate2<void, ChatChannelId const &, String const &>(fp)
        {
        }
    };
}

#endif // BADUMNA_CHAT_DELEGATES_H