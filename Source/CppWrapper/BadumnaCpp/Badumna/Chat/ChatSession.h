//---------------------------------------------------------------------------------
// <copyright file="ChatSession.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_CHAT_SESSION_H
#define BADUMNA_CHAT_SESSION_H

#include <memory>

#include "Badumna/Chat/IChatSession.h"
#include "Badumna/Chat/ChatDelegates.h"
#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

namespace Badumna
{
    class ChatSessionImpl;

    class BADUMNA_API ChatSession : public IChatSession
    {
        friend class NetworkFacadeImpl;
    public:
        ~ChatSession();

        IChatChannel * SubscribeToProximityChannel(
            BadumnaId const &entity_id, 
            ChatMessageHandler message_handler);

        void OpenPrivateChannels(ChatInvitationHandler invitationHandler);

        void InviteUserToPrivateChannel(String const &username);

        IChatChannel * AcceptInvitation(
            ChatChannelId const &channel, 
            ChatMessageHandler messageHandler, 
            ChatPresenceHandler presenceHandler);

        void ChangePresence(ChatStatus status);

    private:
        explicit ChatSession(ChatSessionImpl *impl);

        std::auto_ptr<ChatSessionImpl> const impl_;
        DISALLOW_COPY_AND_ASSIGN(ChatSession);
    };
}

#endif // BADUMNA_CHAT_SESSION_H