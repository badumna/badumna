//---------------------------------------------------------------------------------
// <copyright file="ChatChannelType.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_CHAT_CHANNEL_TYPE_H
#define BADUMNA_CHAT_CHANNEL_TYPE_H

namespace Badumna
{
    /**
     * @enum ChatChannelType
     *
     * Chat channel types.  
     */
    enum ChatChannelType
    {
        PrivateType = 1,        /**< Private chat channel type. */
        ProximityType = 2       /**< Proximity chat channel type. */
    };
}

#endif // BADUMNA_CHAT_CHANNEL_TYPE_H