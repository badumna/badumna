//---------------------------------------------------------------------------------
// <copyright file="ChatChannelId.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_CHAT_CHANNEL_ID_H
#define BADUMNA_CHAT_CHANNEL_ID_H

#include "Badumna/Chat/ChatChannelType.h"
#include "Badumna/DataTypes/BadumnaId.h"

namespace Badumna
{
    class ChatChannelIdImpl;

    /**
     * @class ChatChannelId.
     *
     * @brief A handle for chat channels.
     */
    class BADUMNA_API ChatChannelId
    {
        friend class BadumnaRuntime;
        friend class ManagedObjectGuard;
    public:
        /**
         * Destructor.
         */
        ~ChatChannelId();
        
        /**
         * Copy constructor that constructs a ChatChannelId object from the specified ChatChannelId object.
         *
         * @param other The other ChatChannelId object.
         */
        ChatChannelId(ChatChannelId const &other);

        /**
         * Assignment operator. 
         * 
         * @param rhs The right hand side parameter of the assignment operation.
         * @return A reference to this ChatChannelId object.
         */
        ChatChannelId& operator=(ChatChannelId const &rhs);

        /**
         * Equal to operator.
         * 
         * @param rhs The right hand side parameter of the equal to operation.
         * @return A boolean value indicating whether this object equals to the specified object.
         */
        bool operator==(ChatChannelId const &rhs) const;

        /**
         * Not equal to operator.
         * 
         * @param rhs The right hand side parameter of the not equal to operation.
         * @return A boolean value indicating whether this object not equals to the specified object.
         */
        bool operator!=(ChatChannelId const &rhs) const;

        /**
         * Less than operator.
         * 
         * @param rhs The right hand side parameter of the less than operation.
         * @return A boolean value indicating whether this object is less than the specified object.
         */
        bool operator<(ChatChannelId const &rhs) const;

        /**
         * Gets the type of the channel (Private or Proximity).
         * 
         * @return The ChatChannelType. 
         */
        ChatChannelType Type() const;

        /**
         * Renders a ChatChannelId as a string for debugging purposes.  No guarantee is made that the format
         * of this string will not change. ChatChannelId should be treated as an opaque identifier.
         * 
         * @return A string object that contains the string representation of the ChatChannelId object. 
         */
        String ToString() const;

        /**
         * Sets a friendly name for the id.
         *
         * @param val The friendly name to use for this ChatChannelId object.
         */
        void SetName(String const &val);

        /**
         * Gets the friendly name for the id.
         *
         * @return A string object that contains the friendly name of this id. 
         */
        String Name() const;

    private:
        // should not use auto_ptr or scoped_ptr here
        ChatChannelIdImpl * impl_;

        ChatChannelId();
        explicit ChatChannelId(ChatChannelIdImpl *impl);

        void CopyFrom(ChatChannelId const &other);
    };
}

#endif // BADUMNA_CHAT_CHANNEL_ID_H