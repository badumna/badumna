//---------------------------------------------------------------------------------
// <copyright file="ChatSessionManager.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include "Badumna/Chat/ChatSessionManager.h"
#include "Badumna/Utils/Logger.h"
#include "Badumna/Chat/ChatSessionImpl.h"
#include "Badumna/Chat/ChatChannel.h"

namespace Badumna
{
    ChatSessionManager::ChatSessionManager()
        : session_map_()
    {
    }

    ChatSessionManager::~ChatSessionManager()
    {
    }

    void ChatSessionManager::InvokePresenceHandler(UniqueId const &session_id, ChatChannelId const &channel_id, BadumnaId const &user_id, String const &display_name, ChatStatus status)
    {
        Optional<ChatChannel *> channel = GetChannel(session_id, channel_id);
        if(channel.IsSet())
        {
            channel.Get()->InvokePresenceHandler(channel.Get(), user_id, display_name, status);
        }
    }

    void ChatSessionManager::InvokeMessageHandler(UniqueId const &session_id, ChatChannelId const &channel_id, BadumnaId const &sender, String const &message)
    {
        Optional<ChatChannel *> channel = GetChannel(session_id, channel_id);
        if(channel.IsSet())
        {
            channel.Get()->InvokeMessageHandler(channel.Get(), sender, message);
        }
    }

    void ChatSessionManager::InvokeInvitationHandler(UniqueId const &session_id, ChatChannelId const &channel_id, String const &sender_name)
    {
        Optional<ChatSessionImpl *> session = GetSession(session_id);
        if(session.IsSet())
        {
            session.Get()->InvokeInvitationHandler(channel_id, sender_name);
        }
    }

    void ChatSessionManager::AddSession(ChatSessionImpl * session)
    {
        BADUMNA_LOG_DEBUG("Chat session manager adding: " << session->Id().UTF8CStr());
        session_map_.AddOrReplace(session->Id(), session);
    }

    void ChatSessionManager::RemoveSession(ChatSessionImpl * session)
    {
        // Note that this does not delete the impl - that will be done by its owner's destructor (~ChatSession())
        BADUMNA_LOG_DEBUG("Chat session manager removing: " << session->Id().UTF8CStr());
        if (!session_map_.Remove(session->Id()))
        {
            BADUMNA_LOG_DEBUG("(session not present, ignoring)");
        }
    }

    Optional<ChatSessionImpl *> ChatSessionManager::GetSession(UniqueId const &id)
    {
        BADUMNA_LOG_DEBUG("Chat session manager getting session " << id.UTF8CStr() << " from session mapping");
        Optional<ChatSessionImpl *> session = session_map_.TryGetValue(id);
        if(!session.IsSet())
        {
            BADUMNA_LOG_WARN("Chat session manager has no session " << id.UTF8CStr());
        }
        return session;
    }

    Optional<ChatChannel *> ChatSessionManager::GetChannel(UniqueId const &session_id, ChatChannelId const &channel_id)
    {
        Optional<ChatSessionImpl *> session = GetSession(session_id);
        if(session.IsSet())
        {
            return session.Get()->GetChannel(channel_id);
        }
        return Optional<ChatChannel *>();
    }
}