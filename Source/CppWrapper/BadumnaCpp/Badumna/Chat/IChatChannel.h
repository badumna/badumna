//---------------------------------------------------------------------------------
// <copyright file="IChatChannel.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_ICHATCHANNEL_H
#define BADUMNA_ICHATCHANNEL_H

#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Chat/ChatChannelId.h"
#include "Badumna/Chat/ChatDelegates.h"

namespace Badumna
{
    /**
     * @class IChatChannel.
     * @brief An interface which provides methods for interacting with an individual chat channel.
     */
    class BADUMNA_API IChatChannel
    {
    public:
        /**
         * Destructor.
         */
        virtual ~IChatChannel() {}

        /**
         * Gets this channel's ID. Note that this ID is only unique within the
         * scope of a ChatSession.
         * @returns A ChatChannelId
         */
        virtual ChatChannelId Id() = 0;

        /**
         * Send a message on this channel
         */
        virtual void SendMessage(String message) = 0;

        /**
         * Unsubscribe (remove the user) from this channel.
         * After calling this method, this object is deleted and
         * cannot be used further.
         */
        virtual void Unsubscribe() = 0;
    };
}

#endif // BADUMNA_ICHATCHANNEL_H