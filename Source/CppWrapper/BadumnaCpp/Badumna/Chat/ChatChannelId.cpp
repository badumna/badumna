//---------------------------------------------------------------------------------
// <copyright file="ChatChannelId.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include "Badumna/Chat/ChatChannelType.h"
#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/Chat/ChatChannelId.h"
#include "Badumna/Chat/ChatChannelIdImpl.h"

namespace Badumna
{
    ChatChannelId::ChatChannelId(ChatChannelIdImpl *impl)
        : impl_(impl)
    {
        assert(impl != NULL);
    }

    ChatChannelId::~ChatChannelId()
    {
    }

    ChatChannelType ChatChannelId::Type() const
    {
        return impl_->Type();
    }

    ChatChannelId::ChatChannelId(ChatChannelId const &other)
        : impl_(NULL)
    {
        ChatChannelIdImpl *p = new ChatChannelIdImpl(*(other.impl_));
        impl_ = p;
    }

    String ChatChannelId::ToString() const
    {
        return impl_->ToString();
    }

    void ChatChannelId::SetName(String const &val)
    {
        return impl_->SetName(val);
    }

    String ChatChannelId::Name() const
    {
        return impl_->Name();
    }

    ChatChannelId &ChatChannelId::operator=(ChatChannelId const &rhs)
    {
        if(this != &rhs)
        {
            *(impl_) = *(rhs.impl_);
        }

        return *this;
    }

    bool ChatChannelId::operator==(ChatChannelId const &rhs) const
    {
        return impl_->operator==(*(rhs.impl_));
    }

    bool ChatChannelId::operator!=(ChatChannelId const &rhs) const
    {
        return !ChatChannelId::operator==(rhs);
    }

    bool ChatChannelId::operator<(ChatChannelId const &rhs) const
    {
        return impl_->operator<(*(rhs.impl_));
    }
}