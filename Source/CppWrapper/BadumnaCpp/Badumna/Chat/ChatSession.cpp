//---------------------------------------------------------------------------------
// <copyright file="ChatSession.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include "Badumna/Chat/IChatChannel.h"
#include "Badumna/Chat/ChatSession.h"
#include "Badumna/Chat/ChatSessionImpl.h"
#include "Badumna/Utils/Logger.h"

namespace Badumna
{
    ChatSession::ChatSession(ChatSessionImpl *pimpl)
        : impl_(pimpl)
    {
        assert(pimpl != NULL);
    }

    ChatSession::~ChatSession()
    {
    }

    IChatChannel * ChatSession::SubscribeToProximityChannel(
        BadumnaId const &entity_id, 
        ChatMessageHandler message_handler)
    {
        return impl_->SubscribeToProximityChannel(entity_id, message_handler);
    }

    void ChatSession::OpenPrivateChannels(ChatInvitationHandler invitationHandler)
    {
        impl_->OpenPrivateChannels(invitationHandler);
    }

    void ChatSession::InviteUserToPrivateChannel(String const &username)
    {
        impl_->InviteUserToPrivateChannel(username);
    }

    IChatChannel * ChatSession::AcceptInvitation(
        ChatChannelId const &channel, 
        ChatMessageHandler messageHandler, 
        ChatPresenceHandler presenceHandler)
    {
        return impl_->AcceptInvitation(channel, messageHandler, presenceHandler);
    }

    void ChatSession::ChangePresence(ChatStatus status)
    {
        impl_->ChangePresence(status);
    }
}