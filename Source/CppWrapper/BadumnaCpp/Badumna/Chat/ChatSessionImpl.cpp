//---------------------------------------------------------------------------------
// <copyright file="ChatSessionImpl.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>

#include "Badumna/Utils/Logger.h"
#include "Badumna/Core/TypeRegistry.h"
#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/Core/ManagedObjectGuard.h"
#include "Badumna/Chat/ChatSessionImpl.h"
#include "Badumna/Chat/ChatChannelIdImpl.h"
#include "Badumna/Chat/ChatChannel.h"
#include "Badumna/DataTypes/BadumnaIdImpl.h"
#include "Badumna/Chat/ChatSessionManager.h"

namespace Badumna
{
    REGISTER_MANAGED_MONO_TYPE(CppWrapperStub, ChatSessionStub);
    ChatSessionImpl::ChatSessionImpl(
        MonoRuntime *runtime, 
        ChatSessionManager *manager,
        DotNetObject object, 
        String const &id)
        : ProxyObject(runtime, object, "Badumna.CppWrapperStub", "ChatSessionStub"),
        manager_(manager),
        unique_id_(id),
        channels_(),
        invitation_handler_()
    {
        assert(runtime != NULL && object != NULL);
        manager_->AddSession(this);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, ChatSessionStub, SubscribeToProximityChannel, 1)
    IChatChannel * ChatSessionImpl::SubscribeToProximityChannel(
        BadumnaId const &entity_id, 
        ChatMessageHandler message_handler)
    {
        Arguments args;
        args.AddArgument(ManagedObjectGuard::GetManagedObject(entity_id));
        DotNetObject returned_channel = managed_object_.InvokeMethod("SubscribeToProximityChannel", args);
        assert(returned_channel != NULL);

        ChatChannelImpl *channel_impl = BadumnaRuntime::Instance().CreateChatChannelImpl(returned_channel, message_handler);
        return StoreChannel(channel_impl);
    }

    Optional<ChatChannel *> ChatSessionImpl::GetChannel(ChatChannelId const &channel_id)
    {
        BADUMNA_LOG_DEBUG("Chat session impl getting channel " << channel_id.ToString().UTF8CStr());
        Optional<ChatChannel *> channel = channels_.TryGetValue(channel_id);
        if(!channel.IsSet())
        {
            BADUMNA_LOG_WARN("Chat session has no channel " << channel_id.ToString().UTF8CStr());
        }
        return channel;
    }

    String ChatSessionImpl::Id() const
    {
        return unique_id_;
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, ChatSessionStub, ChangePresence, 1)
    void ChatSessionImpl::ChangePresence(ChatStatus status)
    {
        int status_value = static_cast<int>(status);
        Arguments args;
        args.AddArgument(&status_value);
        managed_object_.InvokeMethod("ChangePresence", args);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, ChatSessionStub, OpenPrivateChannels, 0)
    void ChatSessionImpl::OpenPrivateChannels(ChatInvitationHandler invitation_handler)
    {
        assert(!invitation_handler_.IsSet());
        managed_object_.InvokeMethod("OpenPrivateChannels");
        invitation_handler_= Optional<ChatInvitationHandler>(invitation_handler);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, ChatSessionStub, InviteUserToPrivateChannel, 1)
    void ChatSessionImpl::InviteUserToPrivateChannel(String const &username)
    {
        Arguments args;
        args.AddArgument(MonoRuntime::GetString(username));
        managed_object_.InvokeMethod("InviteUserToPrivateChannel", args);
    }

    REGISTER_MANAGED_METHOD(CppWrapperStub, ChatSessionStub, AcceptInvitation, 1)
    IChatChannel * ChatSessionImpl::AcceptInvitation(
        ChatChannelId const &channel, 
        ChatMessageHandler message_handler, 
        ChatPresenceHandler presence_handler)
    {
        Arguments args;
        args.AddArgument(ManagedObjectGuard::GetManagedObject(channel));
        DotNetObject managed_channel = managed_object_.InvokeMethod("AcceptInvitation", args);
        ChatChannelImpl *channel_impl = BadumnaRuntime::Instance().CreateChatChannelImpl(managed_channel, message_handler, presence_handler);
        return StoreChannel(channel_impl);
    }

    void ChatSessionImpl::InvokeInvitationHandler(
        ChatChannelId const &channel_id,
        String const &username)
    {
        if(invitation_handler_.IsSet())
        {
            invitation_handler_.Get()(channel_id, username);
        }
    }

    void ChatSessionImpl::RemoveChannel(ChatChannelId const &channel)
    {
        channels_.Remove(channel);
    }

    ChatSessionImpl::~ChatSessionImpl()
    {
        BADUMNA_LOG_DEBUG("~ChatSessionImpl()");
        // channel objects are pointers owned by the map, so we must make
        // sure we delete all remaining channels before discarding the map
        BadumnaMap<ChatChannel*, ChatChannelId>::Iterator iter;
        for(iter = channels_.Begin(); iter != channels_.End(); iter++)
        {
            delete iter->second;
        }
    }

    IChatChannel * ChatSessionImpl::StoreChannel(ChatChannelImpl * impl)
    {
        ChatChannelId channel_key = impl->Id();
        BADUMNA_LOG_DEBUG("Chat session impl adding channel: " << channel_key.ToString().UTF8CStr());
        assert(!channels_.Contains(channel_key));
        ChatChannel * channel = new ChatChannel(impl, this);
        channels_.TryAdd(channel_key, channel);
        return channel;
    }
}