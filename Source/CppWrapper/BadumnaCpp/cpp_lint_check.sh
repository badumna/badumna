#!/bin/bash

find . \( -name "*.cpp" -o -name "*.h" \) | xargs python cpplint.py --filter=-whitespace,-build/header_guard,-readability/stream
