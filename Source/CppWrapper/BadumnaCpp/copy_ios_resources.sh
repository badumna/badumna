#!/bin/bash

FRAMEWORK_DIR="$(dirname "$0")"

OUTPUT_DIR="$TARGET_BUILD_DIR/$CONTENTS_FOLDER_PATH/BadumnaResources"
MONO_LIB_DIR="$OUTPUT_DIR/mono/lib/mono/2.0"
MONO_ETC_DIR="$OUTPUT_DIR/mono/etc/mono"
mkdir -p "$OUTPUT_DIR" "$MONO_LIB_DIR" "$MONO_ETC_DIR"

if [ "$PLATFORM_NAME" == "iphoneos" ]; then
   cp "$FRAMEWORK_DIR/Assemblies/aot"/* "$OUTPUT_DIR"
else
   cp "$FRAMEWORK_DIR/Assemblies/jit"/* "$OUTPUT_DIR"
fi

cat > "$MONO_ETC_DIR/config" <<EOF
<configuration>
</configuration>
EOF

mv "$OUTPUT_DIR/mscorlib.dll" "$MONO_LIB_DIR"
