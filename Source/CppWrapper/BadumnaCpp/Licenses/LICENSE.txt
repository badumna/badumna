The Badumna CPP Wrapper contains the following components licensed from 3rd party sources:

- Utils/LinkedPtr and Utils/ScopedPtr modules are licensed from the Chromium project. They are licensed under the BSD
license. The full text of the license is available in LICENSE.Chromium. 

- Mono embedded runtime module is licensed from Novell under the LGPL (v2.0) license. The full text of the license is 
available in LICENSE.MonoRuntime.