#!/bin/bash

set -e

if [ $# -ne 2 ]; then
    echo "Usage: $0 <assembly_dir> <mono_for_iphone_dir>"
    exit 1
fi

ASSEMBLY_DIR="$1"
MONO_ROOT="$2"

cd "$(dirname "$0")"
HERE="$(pwd)"
cd - > /dev/null


if [ ! -e "$MONO_ROOT" ]; then
    echo "Building Mono for iPhone..."
    mkdir -p "$MONO_ROOT"
    git clone https://github.com/mono/mono.git "$MONO_ROOT/source"
    cd "$MONO_ROOT/source"
    git checkout 2cde8a7269
    git apply "$HERE/0001-Changes-to-build-the-runtime-for-iOS.patch"
    ./build_ios_runtime.sh "$MONO_ROOT"
    cd - > /dev/null
fi


MONO_FRAMEWORK="$MONO_ROOT/classes/lib/mono/4.0"
MONO_LIB_SIMULATOR="$MONO_ROOT/simulator"
MONO_LIB_DEVICE="$MONO_ROOT/device"
MONO_ARM="$MONO_ROOT/compiler/bin/arm-darwin-mono"
CIL_STRIP="$MONO_ROOT/classes/bin/mono-cil-strip"

if [ ! -d "$MONO_FRAMEWORK" -o \
     ! -f "$MONO_LIB_SIMULATOR/lib/libmono-2.0.a" -o \
     ! -f "$MONO_LIB_DEVICE/lib/libmono-2.0.a" -o \
     ! -f "$MONO_ARM" -o \
     ! -f "$CIL_STRIP" ]; then
     echo "Mono for iPhone seems to be broken, aborting."
     echo "Check the correct location was specified.  To build Mono for iPhone, specify the desired build directory (which must not exist)."
     exit 1
fi


BADUMNA_ASSEMBLY="$ASSEMBLY_DIR/Badumna.Unity.iOS.dll"
DEI_ASSEMBLY="$ASSEMBLY_DIR/Dei.Unity.iOS.dll"

if [ ! -f "$BADUMNA_ASSEMBLY" -o ! -f "$DEI_ASSEMBLY" ]; then
    echo 'Must build Badumna.Unity.iOS.dll and Dei.Unity.iOS.dll first.'
    echo 'Run "build build:Badumna[unity-ios,release] build:Dei[unity-ios,release]" from the top of the working copy.'
    exit 1
fi


export IOS_BASE_SDK=$(xcodebuild -version -sdk iphoneos | grep SDKVersion | cut "-d " -f 2)
export IOS_DEPLOY_TGT=4.2.1


# Create a fresh tmpdir for our build results.
if [ -z "$TMPDIR" ]; then
    TMPDIR=/tmp
fi
BADUMNA_TMP="$TMPDIR/badumna-ios-build-tmp"
rm -rf "$BADUMNA_TMP"
mkdir -p "$BADUMNA_TMP"

AOT_OPTIONS="mtriple=armv7-darwin,ntrampolines=4096,nimt-trampolines=512,full,static,asmonly,direct-icalls,nodebug,print-skipped"
IPHONE_GCC="/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer/usr/bin/gcc"
GCC_OPTIONS="-miphoneos-version-min=$IOS_DEPLOY_TGT -arch armv7 -std=c99 -isysroot /Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer/SDKs/iPhoneOS$IOS_BASE_SDK.sdk"


OUTDIR="$HERE/Build/iOS"
rm -rf "$OUTDIR"

export AOT_OBJDIR="$OUTDIR/aot-obj"
AOTDIR="$OUTDIR/Badumna.framework/Assemblies/aot"
JITDIR="$OUTDIR/Badumna.framework/Assemblies/jit"

mkdir -p "$AOT_OBJDIR" "$AOTDIR" "$JITDIR"

build_aot()
{
    ASSEMBLY_NAME=$1

    MONO_PATH="$JITDIR" "$MONO_ARM" "--aot=$AOT_OPTIONS,outfile=$BADUMNA_TMP/$ASSEMBLY_NAME.7.s" "$JITDIR/$ASSEMBLY_NAME"
    "$IPHONE_GCC" $GCC_OPTIONS -c "$BADUMNA_TMP/$ASSEMBLY_NAME.7.s" -o "$AOT_OBJDIR/$ASSEMBLY_NAME.7.o"

    "$CIL_STRIP" "$JITDIR/$ASSEMBLY_NAME" "$AOTDIR/$ASSEMBLY_NAME"
}

# Possibly don't need System.Configuration.dll if CONFIGURATION_DEP is not defined when building Mono
ASSEMBLIES=(\
    "$MONO_FRAMEWORK/mscorlib.dll" \
    "$MONO_FRAMEWORK/System.Configuration.dll" \
    "$MONO_FRAMEWORK/System.dll" \
    "$MONO_FRAMEWORK/System.Xml.dll" \
    "$MONO_FRAMEWORK/System.Security.dll" \
    "$MONO_FRAMEWORK/Mono.Security.dll" \
    "$BADUMNA_ASSEMBLY" \
    "$DEI_ASSEMBLY")

cp "${ASSEMBLIES[@]}" "$JITDIR"

for i in ${ASSEMBLIES[@]}; do
    build_aot "$(basename "$i")"
done

build_badumna_cpp()
{
    ARCH=$1
    ARCH_OUTDIR="$OUTDIR/$ARCH"

    if [ $ARCH == i386 ]; then
        export MONO_2_PATH="$MONO_LIB_SIMULATOR"
    else
        export MONO_2_PATH="$MONO_LIB_DEVICE"
    fi

    cd "$HERE"
    make IOS_ARCH=$ARCH release=1 verbose=true
    cd - > /dev/null
}

build_badumna_cpp i386
build_badumna_cpp armv7

LIBBADUMNA="libbadumna-cpp.a"
lipo -arch armv7 "$OUTDIR/../RELEASE/armv7/$LIBBADUMNA" -arch i386 "$OUTDIR/../RELEASE/i386/$LIBBADUMNA" -create -output "$OUTDIR/$LIBBADUMNA"
strip -S "$OUTDIR/$LIBBADUMNA"  # strip debugging symbols to decrease the filesize

mkdir -p "$OUTDIR/Badumna.framework/Versions/A/Headers"
cp "$OUTDIR/$LIBBADUMNA" "$OUTDIR/Badumna.framework/Versions/A/Badumna"
cp -R "$HERE/Badumna/"* "$OUTDIR/Badumna.framework/Versions/A/Headers"
cp "$HERE/copy_ios_resources.sh" "$OUTDIR/Badumna.framework"
find "$OUTDIR/Badumna.framework/Versions/A/Headers" ! -name \*.h -delete || true
ln -s A "$OUTDIR/Badumna.framework/Versions/Current"
ln -s Versions/Current/Badumna "$OUTDIR/Badumna.framework/Badumna"
ln -s Versions/Current/Headers "$OUTDIR/Badumna.framework/Headers"
