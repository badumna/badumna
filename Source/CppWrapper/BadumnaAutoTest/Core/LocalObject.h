//---------------------------------------------------------------------------------
// <copyright file="LocalObject.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_LOCALOBJECT_H
#define BADUMNA_AUTO_TEST_LOCALOBJECT_H

#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/DataTypes/Vector3.h"
#include "Badumna/Replication/IDeadReckonableSpatialOriginal.h"
#include "Badumna/Replication/SpatialEntityStateSegment.h"
#include "Badumna/Utils/BasicTypes.h"

#include "EventLogger/EventLog.h"

namespace BadumnaTestApp
{
    enum StateSegment
    {
        Counter = Badumna::StateSegment_FirstAvailableSegment
    };

    class LocalObject : public Badumna::IDeadReckonableSpatialOriginal
    {
    public:
        LocalObject(Badumna::NetworkFacade *network_facade, EventLog *log);

        //
        // IEntity properties
        // 
        Badumna::BadumnaId Guid() const;
        void SetGuid(Badumna::BadumnaId const &id);
        void HandleEvent(Badumna::InputStream *stream);

        //
        // ISpatialEntity properties
        //
        Badumna::Vector3 Position() const;
        void SetPosition(Badumna::Vector3 const &position);
        float Radius() const;
        void SetRadius(float val);
        float AreaOfInterestRadius() const;
        void SetAreaOfInterestRadius(float val);

        //
        // ISpatialOriginal properties
        //
        void Serialize(Badumna::BooleanArray const &required_parts, Badumna::OutputStream *stream);

        // Deadreckoning
        Badumna::Vector3 Velocity() const;
        void SetVelocity(Badumna::Vector3 const &velocity);
        void AttemptMovement(Badumna::Vector3 const &reckoned_position);

        //
        // app specific.
        // 
        int GetCounter()
        {
            return counter_;
        }

        void SetCounter(int value);
        
    private:
        Badumna::NetworkFacade *facade_;
        Badumna::BadumnaId guid_;
        
        EventLog *log_;
        Badumna::Vector3 velocity_;
        Badumna::Vector3 position_;
        float radius_;
        float area_of_interest_radius_;

        int counter_; 

        DISALLOW_COPY_AND_ASSIGN(LocalObject);
    };
}

#endif // BADUMNA_AUTO_TEST_LOCALOBJECT_H