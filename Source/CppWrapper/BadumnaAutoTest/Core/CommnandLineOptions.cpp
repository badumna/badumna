//---------------------------------------------------------------------------------
// <copyright file="CommandLineOptions.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <stdexcept>
#include <iostream>
#include <string>
#include <cstdio>

#include <boost/program_options.hpp>
#include "Core/CommandLineOptions.h"

#define PROCESS_OPTION(name, mode)                                                \
    {                                                                             \
        if(vm.count(name))                                                        \
        {                                                                         \
            std::cout << "setting to " << name << " mode." << std::endl;          \
            config->mode_ = mode;                                                 \
            return 0;                                                             \
        }                                                                         \
    };

#define PROCESS_CHAT_OPTION(name, mode)                                           \
    {                                                                             \
        if(vm.count(name))                                                        \
        {                                                                         \
            std::cout << "setting to " << name << " mode." << std::endl;          \
            config->mode_ = mode;                                                 \
            config->friend_list_file_ = vm[name].as<string>().c_str();            \
            return 0;                                                             \
        }                                                                         \
    };

namespace BadumnaTestApp
{
    using std::string;
    namespace po = boost::program_options;

    int parse_commandline_options(int argc, char **argv, ProgramConfig *config)
    {
        po::options_description desc("Allowed options");
        desc.add_options()
            ("help", "produce help message")
            ("attach", "wait for keyboard input before continuing (giving you time to attach a debugger)")
            ("appname", po::value<string>(), "set the application name")
            ("tunnel", "run through tunnel")
            ("minimum_test", "set to minimum test mode")
            ("replication_test", "set to replication test mode")
            ("proximity_chat_test", "set to proximity chat test mode")
            ("custom_event_test", "set to custom event test mode")
            ("private_chat_test", po::value<string>(), "set to private chat test mode")
            ("private_chat_relogin_test", po::value<string>(), "set to private chat re-login test mode")
            ("arbitration_client", "set to arbitration client test mode")
            ("arbitration_server", "set to arbitration server test mode")
            ("overload_test", "set to overload test mode")
            ("overload_server", "set to overload server mode")
            ("dei_test", "set to Dei test mode")
            ("change_scene_sender", "set to change scene sender mode")
            ("change_scene_receiver", "set to change scene receiver mode")
            ("multiple_originals_test", "set to multiple originals test mode")
            ("multiple_peers_test", "set to multiple peers replication test mode")
            ("meet_and_apart_test", "set to meet and apart test mode")
            ("meet_and_apart_walker", "set to meet and apart walker mode")
            ("pc_meet_and_apart_test", po::value<string>(), "set to private chat meet and apart test mode")
            ("do_relogin", "this peer should be the one to re-login")
            ("login_sequence_test", "set to login sequence test mode")
            ("chat_relogin_test", "set to chat re-login test mode")
            ("character_management_test", "set to character management test mode")
            ("create_facade_test", "set to create facade test mode")
            ("single_peer_dc_test", "set to single peer controller test mode")
            ("long_running_replication_test", "set to long running replication test mode")
            ("streaming_sender_test", "set to streaming sender test mode")
            ("streaming_receiver_test", "set to streaming receiver test mode")
            ("announce_permission_test", "set to announce permission test mode")
            ("service_discovery_client_test", "set to service discovery client test mode")
            ("service_discovery_server_test", "set to service discovery server test mode")
			("mini_network_scene_test", "set to mini network scene test mode");

        po::variables_map vm;

        try
        {
            po::store(po::parse_command_line(argc, argv, desc), vm);
            po::notify(vm);
        }
        catch(std::logic_error &e)
        {
            std::cerr << "Failed to parse the command line options : " << e.what() << std::endl;
            throw;
        }

        if(vm.count("help"))
        {   
            std::cout << desc << std::endl;
            config->mode_ = BadumnaTestApp::HelpAndExit;
            return 0;
        }

        if(vm.count("attach"))
        {   
            std::cout << "Press `return` to continue..." << std::endl;
            getchar();
        }

        if(vm.count("appname"))
        {
            config->app_name_salt_ = vm["appname"].as<string>().c_str();
        }

        if (vm.count("tunnel"))
        {
            config->use_tunnel_ = true;
        }

        if(vm.count("do_relogin"))
        {
            std::cout << "setting do_relogin = true." << std::endl;
            config->do_relogin_ = true;
        }


        PROCESS_OPTION("minimum_test", BadumnaTestApp::MinimumTest);
        PROCESS_OPTION("replication_test", BadumnaTestApp::ReplicationTest);
        PROCESS_OPTION("proximity_chat_test", BadumnaTestApp::ProximityChatTest;);
        PROCESS_OPTION("custom_event_test", BadumnaTestApp::CustomEventTest);
        PROCESS_OPTION("arbitration_client", BadumnaTestApp::ArbitrationClientTest);
        PROCESS_OPTION("arbitration_server", BadumnaTestApp::ArbitrationServerTest);
        PROCESS_OPTION("overload_test", BadumnaTestApp::OverloadTest);
        PROCESS_OPTION("overload_server", BadumnaTestApp::OverloadServer);
        PROCESS_OPTION("dei_test", BadumnaTestApp::DeiReplicationTest);
        PROCESS_OPTION("change_scene_sender", BadumnaTestApp::ChangeSceneSender);
        PROCESS_OPTION("change_scene_receiver", BadumnaTestApp::ChangeSceneReceiver);
        PROCESS_OPTION("multiple_originals_test", BadumnaTestApp::MultiOriginalTest);
        PROCESS_OPTION("multiple_peers_test", BadumnaTestApp::MultiPeersReplicationTest);
        PROCESS_OPTION("meet_and_apart_test", BadumnaTestApp::MeetAndApartTest);
        PROCESS_OPTION("meet_and_apart_walker", BadumnaTestApp::MeetAndApartWalker);
        PROCESS_OPTION("login_sequence_test", BadumnaTestApp::SimpleLoginSequenceTest);
        PROCESS_OPTION("character_management_test", BadumnaTestApp::SimpleCharacterManagementTest);
        PROCESS_OPTION("create_facade_test", BadumnaTestApp::SimpleCreateFacadeTest);
        PROCESS_OPTION("single_peer_dc_test", BadumnaTestApp::SinglePeerDCTest);
        PROCESS_OPTION("long_running_replication_test", BadumnaTestApp::LRRTest);
        PROCESS_OPTION("streaming_sender_test", BadumnaTestApp::StreamingSenderTest);
        PROCESS_OPTION("streaming_receiver_test", BadumnaTestApp::StreamingReceiverTest);
        PROCESS_OPTION("announce_permission_test", BadumnaTestApp::SimpleAnnouncePermissionTest);
        PROCESS_OPTION("service_discovery_client_test", BadumnaTestApp::ServiceDiscoveryClientTest);
        PROCESS_OPTION("service_discovery_server_test", BadumnaTestApp::ServiceDiscoveryServerTest);
        PROCESS_OPTION("mini_network_scene_test", BadumnaTestApp::SimpleMiniNetworkSceneTest);

        PROCESS_CHAT_OPTION("private_chat_test", BadumnaTestApp::PrivateChatTest);
        PROCESS_CHAT_OPTION("private_chat_relogin_test", BadumnaTestApp::PCReLoginTest);
        PROCESS_CHAT_OPTION("pc_meet_and_apart_test", BadumnaTestApp::PCMeetAndApartTest);

        throw std::invalid_argument("No test option specified.");
    }
}