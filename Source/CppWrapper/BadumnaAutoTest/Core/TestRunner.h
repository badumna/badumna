//---------------------------------------------------------------------------------
// <copyright file="TestRunner.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_TEST_RUNNER_H
#define BADUMNA_AUTO_TEST_TEST_RUNNER_H

#include "App/AppConfig.h"

namespace BadumnaTestApp
{
    /**
     * TestRunner is used to run a test.
     */
    class TestRunner
    {
    public:
        TestRunner(ProgramConfig const &config);
        virtual ~TestRunner();

        int RunTest()
        {
            return DoRunTest();
        }
    protected:
        virtual int DoRunTest() = 0;
        ProgramConfig config;
    };

    class AutoTestRunner : public TestRunner
    {
    public:
        AutoTestRunner(ProgramConfig const &config);
        ~AutoTestRunner();

    protected:
        int DoRunTest();
    };

    /**
     * The runner class used for running simple tests.
     */
    class SimpleTestRunner : public TestRunner
    {
    public:
        SimpleTestRunner(ProgramConfig const &config);
        ~SimpleTestRunner();

    protected:
        int DoRunTest();
    };
}

#endif // BADUMNA_AUTO_TEST_TEST_RUNNER_H