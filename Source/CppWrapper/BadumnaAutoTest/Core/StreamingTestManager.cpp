//---------------------------------------------------------------------------------
// <copyright file="StreamingTestManager.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifdef WIN32
#include <direct.h>
#define GetCurrentDir _getcwd
#else
#include <unistd.h>
#define GetCurrentDir getcwd
#endif 

#include <iostream>
#include <fstream>
#include "Badumna/DataTypes/String.h"
#include "Core/StreamingTestManager.h"

namespace
{
    Badumna::String GetCurDir()
    {
        char cCurrentPath[FILENAME_MAX];
        GetCurrentDir(cCurrentPath, sizeof(cCurrentPath));
        Badumna::String dir = cCurrentPath;
        return dir;
    }
}

namespace BadumnaTestApp
{
    using Badumna::BadumnaId;

    StreamingTestManager::StreamingTestManager(Badumna::StreamingManager *manager, EventLog *log, bool sender)
        : sending_(false),
        receiving_(false),
        is_sender_(sender),
        manager_(manager),
        log_(log),
        send_controller_(NULL),
        receive_controller_(NULL)
    {
        if(!sender)
        {
            SubscribeToStream();
        }
    }

    void StreamingTestManager::SendFile(BadumnaId const &destination)
    {
        assert(!sending_);

        std::cout << "send file is called, sending to : " << destination.ToString().UTF8CStr() << std::endl;

        GenerateFile();
        Badumna::StreamingCompleteDelegate d(&StreamingTestManager::SendComplete, *this);
        send_controller_.reset(
            manager_->BeginSendReliableFile(
            "test_stream", 
            ::GetCurDir() + "/stream_test.data", 
            destination, 
            "sender_username", 
            d));

        sending_ = true;
    }

    void StreamingTestManager::SendComplete(Badumna::String const &tag, Badumna::String const &name)
    {
        assert(sending_);
        sending_ = false;

        log_->RecordCompletedStreamOperation();
        std::cout << "send complete is called, tag : " << tag.UTF8CStr() << ", name = " << name.UTF8CStr() << std::endl;
    }
    
    void StreamingTestManager::ReceiveComplete(Badumna::String const &tag, Badumna::String const &name)
    {
        assert(receiving_);
        receiving_ = false;

        log_->RecordCompletedStreamOperation();
        std::cout << "receive complete is called, tag : " << tag.UTF8CStr() << ", name = " << name.UTF8CStr() << std::endl;
    }
    
    void StreamingTestManager::RequestReceived(Badumna::StreamRequest *request)
    {
        std::cout << "request received." << std::endl;
        std::cout << "tag = " << request->GetStreamTag().UTF8CStr();
        std::cout << ", name = " << request->GetStreamName().UTF8CStr();
        std::cout << ", uesrname = " << request->GetUserName().UTF8CStr();
        std::cout << ", size = " << request->GetSizeKiloBytes() << std::endl;

        log_->RecordReceivedStreamRequest();
        Badumna::StreamingCompleteDelegate d(&StreamingTestManager::ReceiveComplete, *this);
        receiving_ = true;

        receive_controller_.reset(request->AcceptFile(::GetCurDir() + "/received_stream_test.data", d));
    }

    void StreamingTestManager::SubscribeToStream()
    {
        assert(!is_sender_);
        std::cout << "going to subscribe to the stream" << std::endl;

        Badumna::ReceiveStreamRequestDelegate d(&StreamingTestManager::RequestReceived, *this);
        manager_->SubscribeToReceiveStreamRequests("test_stream", d);
    }

    bool StreamingTestManager::IsWorking() const
    {
        if(is_sender_)
        {
            return sending_;
        }
        else
        {
            return receiving_;
        }
    }
    
    void StreamingTestManager::GenerateFile() const
    {
        std::ofstream data_file("stream_test.data");
        if(data_file.good())
        {
            for(int i = 0; i < 10000; i++)
            {
                data_file << "this is some non sense text" << std::endl;
            }
        }
        data_file.flush();
        data_file.close();
    }
}