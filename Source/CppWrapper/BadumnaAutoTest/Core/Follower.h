//---------------------------------------------------------------------------------
// <copyright file="Fellower.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_FOLLOWER_H
#define BADUMNA_AUTO_TEST_FOLLOWER_H

#include "Badumna/Replication/ISpatialOriginal.h"
#include "Badumna/Replication/ISpatialReplica.h"
#include "Badumna/DataTypes/Vector3.h"
#include "Badumna/Utils/BasicTypes.h"

namespace BadumnaTestApp
{
    class Follower
    {
    public:
        explicit Follower(Badumna::ISpatialOriginal *original);

        void SetPossibleTargets(std::vector<Badumna::ISpatialReplica *> *replicas);
        void Update();

        bool IsTargetting() const
        {
            return is_targetting_;
        }

    private:
        bool is_targetting_;
        Badumna::ISpatialOriginal *original_;
        Badumna::Vector3 target_;

        int speed_;

        DISALLOW_COPY_AND_ASSIGN(Follower);
    };
}

#endif // BADUMNA_AUTO_TEST_FOLLOWER_H