//---------------------------------------------------------------------------------
// <copyright file="FriendsList.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <stdexcept>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "FriendsList.h"

namespace BadumnaTestApp
{
    using std::runtime_error;
    using std::cerr;
    using std::ifstream;
    using std::string;
    using Badumna::String;
    using Badumna::ChatStatus;
    using Badumna::ChatChannelId;
    using Badumna::IChatChannel;
    
    bool FriendsList::LoadFriendsList(String const &path)
    {
        ifstream friends_list(path.UTF8CStr());
        if(friends_list.is_open())
        {
            string line;
            while(friends_list.good())
            {
                getline(friends_list, line);
                if(line.size() > 0)
                {
                    Friend f(line.c_str());
                    AddFriend(f);
                }
            }

            friends_list.close();
            return true;
        }
        else
        {
            std::cout << "can't open the friend list : " << path.UTF8CStr() << std::endl;
            throw runtime_error("friend list file can't be opened.");
        }
    }

    bool FriendsList::IsFriend(String const &name) const
    {
        FriendsMap::const_iterator iter = friends_map_.find(name);
        if(iter != friends_map_.end())
        {
            return true;
        }

        return false;
    }

    void FriendsList::SetFriendChatStatus(String const &name, ChatStatus status)
    {
        FriendsMap::iterator iter = friends_map_.find(name);
        if(iter != friends_map_.end())
        {
            Friend f = friends_map_[name];
            f.SetStatus(status);
            friends_map_[name] = f;
        }
    }

    void FriendsList::SetFriendChatChannel(String const &name, IChatChannel *channel)
    {
        FriendsMap::iterator iter = friends_map_.find(name);
        if(iter != friends_map_.end())
        {
            Friend f = friends_map_[name];
            f.SetChannel(channel);
            friends_map_[name] = f;
        }
    }

    Friend FriendsList::GetFriend(String const &name)
    {
        FriendsMap::iterator iter = friends_map_.find(name);
        if(iter != friends_map_.end())
        {
            Friend f = friends_map_[name];

            return f;
        }

        return Friend();
    }

    String FriendsList::GetFriendNameByChannel(IChatChannel *channel)
    {
        if(channel != NULL)
        {
            for(FriendsMap::iterator iter = friends_map_.begin(); iter != friends_map_.end(); iter++)
            {
                if(iter->second.IsValid() && iter->second.Channel() == channel)
                {
                    return iter->first;
                }
            }
        }

        throw runtime_error("unknown friend.");
    }

    void FriendsList::AddFriend(Friend f)
    {
        FriendsMap::iterator iter = friends_map_.find(f.Name());
        if(iter == friends_map_.end())
        {
            friends_map_[f.Name()] = f;
        }
    }

    void FriendsList::RemoveFriend(String const &name)
    {
        FriendsMap::iterator iter = friends_map_.find(name);
        if(iter != friends_map_.end())
        {
            friends_map_.erase(iter);
        }
    }

    std::vector<String> FriendsList::GetFriendNames() const
    {
        std::vector<String> vec;
        
        for(FriendsMap::const_iterator iter = friends_map_.begin(); iter != friends_map_.end(); iter++)
        {
            vec.push_back(iter->first);
        }

        return vec;
    }
}