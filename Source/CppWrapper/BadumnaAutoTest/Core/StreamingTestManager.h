//---------------------------------------------------------------------------------
// <copyright file="StreamingTestManager.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_STREAMING_TEST_MANAGER_H
#define BADUMNA_AUTO_TEST_STREAMING_TEST_MANAGER_H

#include <memory>
#include "Badumna/DataTypes/String.h"
#include "Badumna/Streaming/StreamRequest.h"
#include "Badumna/Streaming/StreamingManager.h"
#include "Badumna/Streaming/StreamingCompleteDelegate.h"
#include "Badumna/Streaming/ReceiveStreamRequestDelegate.h"
#include "Badumna/Utils/BasicTypes.h"

#include "EventLogger/EventLog.h"

namespace BadumnaTestApp
{
    class StreamingTestManager
    {
    public:
        StreamingTestManager(Badumna::StreamingManager *manager, EventLog *log, bool sender);
        
        void SendFile(Badumna::BadumnaId const &destination);

        void SendComplete(Badumna::String const &tag, Badumna::String const &name);
        void ReceiveComplete(Badumna::String const &tag, Badumna::String const &name);
        void RequestReceived(Badumna::StreamRequest *request);

    private:
        void SubscribeToStream();
        bool IsWorking() const;
        void GenerateFile() const;

        bool sending_;
        bool receiving_;
        bool is_sender_;

        std::auto_ptr<Badumna::StreamingManager> manager_;
        EventLog *log_;
        std::auto_ptr<Badumna::IStreamController> send_controller_;
        std::auto_ptr<Badumna::IStreamController> receive_controller_;

        DISALLOW_COPY_AND_ASSIGN(StreamingTestManager);
    };
}

#endif // BADUMNA_AUTO_TEST_STREAMING_TEST_MANAGER_H