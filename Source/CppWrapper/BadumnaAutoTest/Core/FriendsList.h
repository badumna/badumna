//---------------------------------------------------------------------------------
// <copyright file="FriendsList.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_FRIENDSLIST_H
#define BADUMNA_AUTO_TEST_FRIENDSLIST_H

#include <vector>
#include <map>

#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Chat/IChatChannel.h"
#include "Friend.h"

namespace BadumnaTestApp
{
    class FriendsList
    {
    public:
        FriendsList()
            : friends_map_()
        {
        }

        bool LoadFriendsList(Badumna::String const &path);
        bool IsFriend(Badumna::String const &name) const;
        void SetFriendChatStatus(Badumna::String const &name, Badumna::ChatStatus status);
        void SetFriendChatChannel(Badumna::String const &name, Badumna::IChatChannel *channel);
        void RemoveFriend(Badumna::String const &name);
        Friend GetFriend(Badumna::String const &name);
        Badumna::String GetFriendNameByChannel(Badumna::IChatChannel *channel);
        std::vector<Badumna::String> GetFriendNames() const;

    private:
        void AddFriend(Friend f);

        typedef std::map<Badumna::String, Friend> FriendsMap;
        FriendsMap friends_map_;

        DISALLOW_COPY_AND_ASSIGN(FriendsList);
    };
}

#endif // BADUMNA_AUTO_TEST_FRIENDSLIST_H