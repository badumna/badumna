//---------------------------------------------------------------------------------
// <copyright file="SinglePeerDCTestNPC.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include <vector>
#include <algorithm>
#include <functional>

#include "Badumna/Core/NetworkFacade.h"
#include "EventLogger/NullEventLog.h"
#include "Core/SinglePeerDCTestNPC.h"
#include "Core/LocalObject.h"

namespace BadumnaTestApp
{
    using std::string;
    using namespace Badumna;

    struct GuidEqualityComparable2 : public std::binary_function<ISpatialReplica *, BadumnaId, bool>
    {
        bool operator()(ISpatialReplica *replica, BadumnaId const &id) const
        {
            return replica->Guid() == id;
        }
    };

    SinglePeerDCTestNPC::SinglePeerDCTestNPC(NetworkFacade *facade, String const &name)
        : DistributedSceneController(facade, name), 
        original_(NULL), 
        replica_objects_(), 
        follower_(NULL),
        logger_(new NullEventLog()),
        process_called_(0)
    {
    }

    uint32_t SinglePeerDCTestNPC::GetEntityType() const
    {
        return 105;
    }

    ISpatialOriginal* SinglePeerDCTestNPC::TakeControlOfEntity(BadumnaId const &entity_id, uint32_t)
    {
        if(original_ == NULL)
        {
            original_ = new LocalObject(facade_, logger_.get());
            original_->SetGuid(entity_id);
            Vector3 initial_position(20.0f, 20.0f, 0.0f);
            original_->SetPosition(initial_position);
            original_->SetRadius(10.0f);
            original_->SetAreaOfInterestRadius(50.0f);

            follower_ = new Follower(original_);

            Replicate();
        }

        return original_;
    }
    
    ISpatialReplica* SinglePeerDCTestNPC::InstantiateRemoteEntity(BadumnaId const &entity_id, uint32_t)
    {
        ReplicaObject *replica = new ReplicaObject(logger_.get());
        replica->SetGuid(entity_id);
        replica_objects_.push_back(replica);
        return replica;
    }
    
    void SinglePeerDCTestNPC::RemoveEntity(ISpatialReplica const &replica)
    {
        std::vector<ISpatialReplica*>::iterator iter;
        iter = std::find_if(
            replica_objects_.begin(), 
            replica_objects_.end(), 
            std::bind2nd(GuidEqualityComparable2(), replica.Guid()));
        if(iter != replica_objects_.end())
        {
            ISpatialReplica* to_remove = *iter;
            replica_objects_.erase(iter);
            delete to_remove;
        }
    }

    void SinglePeerDCTestNPC::Checkpoint(OutputStream *stream)
    {
        float x = original_->Position().GetX();
        float y = original_->Position().GetY();

        (*stream) << x;
        (*stream) << y;
    }

    void SinglePeerDCTestNPC::Recover(InputStream *stream)
    {
        float x;
        float y;

        (*stream) >> x;
        (*stream) >> y;

        Vector3 v(x, y, 0);
        original_->SetPosition(v);
    }

    void SinglePeerDCTestNPC::Recover()
    {
        Vector3 v(20, 20, 0);
        original_->SetPosition(v);
    }
        
    void SinglePeerDCTestNPC::Process(int)
    {
        process_called_++;
        
        if(process_called_ % 6 == 0)
        { 
            if(follower_ != NULL && !follower_->IsTargetting() && replica_objects_.size() > 0)
            {
                follower_->SetPossibleTargets(&replica_objects_);
            }

            if(follower_ != NULL && follower_->IsTargetting())
            {
                follower_->Update();
            }
        }

        if(process_called_ % 200 == 0)
        {
            Replicate();
            original_->SetCounter(original_->GetCounter() + 1);
        }
    }
    
    void SinglePeerDCTestNPC::Wake()
    {
    }
    
    void SinglePeerDCTestNPC::Sleep()
    {
        if(original_ != NULL)
        {
            delete original_;
            original_ = NULL;
        }

        if(follower_ != NULL)
        {
            delete follower_;
            follower_ = NULL;
        }
    }
}