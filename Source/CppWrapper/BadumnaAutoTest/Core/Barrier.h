//---------------------------------------------------------------------------------
// <copyright file="Barrier.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_BARRIER_H
#define BADUMNA_AUTO_TEST_BARRIER_H

#include <memory>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/sync/interprocess_mutex.hpp>
#include <boost/interprocess/sync/named_semaphore.hpp>
#include "Badumna/DataTypes/String.h"

namespace BadumnaTestApp
{
    // FIXME(lni): this barrier class is pretty ugly. need to rewrite it. 

    class SharedMemoryRemover
    {
    public:
        SharedMemoryRemover(Badumna::String const &n)
            : name_(n),
            mutex_name_(n + "_mutex"),
            sem_name_(n + "_sem")
        {
        }

        ~SharedMemoryRemover()
        {
            boost::interprocess::shared_memory_object::remove(mutex_name_.UTF8CStr());
            boost::interprocess::shared_memory_object::remove(name_.UTF8CStr());
            boost::interprocess::named_semaphore::remove(sem_name_.UTF8CStr());
        }

    private:
        Badumna::String name_;
        Badumna::String mutex_name_;
        Badumna::String sem_name_;
    };

    class Barrier
    {
    public:
        Barrier(size_t number_of_process, Badumna::String const &name);

        void SetAsReady();
        bool Ready();
    
    private:
        void Initialize();

        bool set_;
        int nop_;
        Badumna::String name_;
        SharedMemoryRemover remover_;

        // used to store the counter
        std::auto_ptr<boost::interprocess::shared_memory_object> shm_;
        std::auto_ptr<boost::interprocess::mapped_region> region_;

        // used to store the interprocess mutex
        std::auto_ptr<boost::interprocess::shared_memory_object> mutex_shm_;
        std::auto_ptr<boost::interprocess::mapped_region> mutex_region_;
        boost::interprocess::interprocess_mutex *mutex_;

        // semaphore used to make sure only one process will initialize the shared memory value
        std::auto_ptr<boost::interprocess::named_semaphore> named_sem_;

        // address of the counter
        int *address_;

        static const int shared_memory_size = sizeof(int); 

        DISALLOW_COPY_AND_ASSIGN(Barrier);
    };
}

#endif // BADUMNA_AUTO_TEST_BARRIER_H