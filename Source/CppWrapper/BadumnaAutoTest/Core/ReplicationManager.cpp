//---------------------------------------------------------------------------------
// <copyright file="ReplicationManager.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include <iostream>

#include <boost/date_time/local_time/local_time.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "Badumna/DataTypes/Vector3.h"
#include "Badumna/Utils/BasicTypes.h"

#include "Console/IConsole.h"
#include "Core/ReplicationManager.h"

namespace BadumnaTestApp
{
    using std::map;
    using Badumna::Vector3;
    using Badumna::NetworkFacade;
    using Badumna::IChatSession;
    using Badumna::ChatChannelId;
    using Badumna::IChatChannel;
    using Badumna::ISpatialReplica;
    using Badumna::NetworkScene;
    using Badumna::BadumnaId;
    using Badumna::String;
    using Badumna::OutputStream;

    const float ReplicationManager::delta_x = 10.0f;
    const float ReplicationManager::delta_y = 8.0f;
    const float ReplicationManager::delta_z = 0.0f;

    ReplicationManager::ReplicationManager(NetworkFacade *network_facade, IChatSession *service, EventLog* event_log)
        : chat_session_(service),
        facade_(network_facade),
        log_(event_log),
        local_object_(new LocalObject(network_facade, event_log)),
        replica_objects_(),
        scene_(NULL),
        current_scene_name_(),
        ready_(false)
    {
        Initialize();
    }

    void ReplicationManager::Shutdown()
    {
        if(scene_.get() != NULL)
        {
            if(local_object_.get() != NULL)
            {
                std::cout << "going to call unregister" << std::endl;
                scene_->UnregisterEntity(*(local_object_.get()));
            }

            std::cout << "going to call leave" << std::endl;
            scene_->Leave();
        }
    }

    void ReplicationManager::Initialize()
    {
        facade_->RegisterEntityDetails(100, 10);
        // join scene
        scene_.reset(facade_->JoinScene(
            L"test_app_scene_1",
            Badumna::CreateSpatialReplicaDelegate(&ReplicationManager::CreateSpatialReplica, *this),
            Badumna::RemoveSpatialReplicaDelegate(&ReplicationManager::RemoveSpatialReplica, *this)));

        // initialize the local avatar
        Vector3 initial_position(0.0f, 0.0f, 0.0f);
        Vector3 initial_velocity(0.0f, 0.0f, 0.0f);

        local_object_->SetPosition(initial_position);
        local_object_->SetVelocity(initial_velocity);
        local_object_->SetRadius(100.0f);
        local_object_->SetAreaOfInterestRadius(100.0f);

        // register the local avatar to the above scene
        scene_->RegisterEntity(local_object_.get(), 100);
        local_object_->SetPosition(initial_position);
        current_scene_name_ = L"test_app_scene_1";

        // subscribe to the proximity chat channel. 
        chat_channel_ = chat_session_->SubscribeToProximityChannel(
            local_object_->Guid(), 
            Badumna::ChatMessageHandler(&ReplicationManager::ProximityChatMessageHandler, *this));
    }

    ISpatialReplica *ReplicationManager::CreateSpatialReplica(
        NetworkScene const & /*scene*/, 
        BadumnaId const &id, 
        uint32_t /*type*/)
    {
        std::ostringstream msg;
        const boost::posix_time::ptime now = boost::posix_time::second_clock::local_time();
        boost::posix_time::time_facet*const f = new boost::posix_time::time_facet("%H:%M:%S");
        msg.imbue(std::locale(msg.getloc(),f));
        msg << now;

        ready_ = true;
        std::cout << "%%% create replica is called, t : " << msg.str() << std::endl;

        String key = id.ToString();
        log_->RecordCreateReplicaEvent();

        map<String, ReplicaObject *>::iterator iter = replica_objects_.find(key);
        if(iter == replica_objects_.end())
        {
            ReplicaObject *r = new ReplicaObject(log_);
            r->SetGuid(id);
            replica_objects_.insert(std::make_pair(key, r));
        
            Vector3 dest = facade_->GetDestination(*r);
            std::wcout << L"dest: " << dest << std::endl;

            return r;
        }
        else
        {
            return iter->second;
        }
    }
    
    void ReplicationManager::RemoveSpatialReplica(NetworkScene const &/*scene*/, ISpatialReplica const &replica)
    {
        std::cout << "%%% remove replica is called" << std::endl;

        String key = replica.Guid().ToString();
        map<String, ReplicaObject *>::iterator iter = replica_objects_.find(key);
        if(iter != replica_objects_.end())
        {
            ReplicaObject *obj = iter->second;
            replica_objects_.erase(iter);
            delete obj;
        }

        log_->RecordRemoveReplicaEvent();
    }

    BadumnaId ReplicationManager::GetFirstReplica() const
    {
        assert(replica_objects_.size() > 0);

        return replica_objects_.begin()->second->Guid();
    }

    void ReplicationManager::ProximityChatMessageHandler(
        IChatChannel * /*channel_id*/, 
        BadumnaId const & /*user_id*/, 
        String const &message)
    {
        std::wcout << L"[Proximity chat]: " << message.CStr() << std::endl;

        log_->RecordProximityMessageEvent();
    }

    void ReplicationManager::MoveLocalObject(int c)
    {
        assert(local_object_->IsDeadReckonable());
        assert(c == MOVE_UP_COMMAND || c == MOVE_DOWN_COMMAND || 
               c == MOVE_RIGHT_COMMAND || c == MOVE_LEFT_COMMAND);

        if(c == MOVE_UP_COMMAND)
        {
            Vector3 v = local_object_->Velocity();
            v.SetY(v.GetY() + 1.0f);
            local_object_->SetVelocity(v);
        }
        else if(c == MOVE_DOWN_COMMAND)
        {
            Vector3 v = local_object_->Velocity();
            v.SetY(v.GetY() - 1.0f);
            local_object_->SetVelocity(v);
        }
        else if(c == MOVE_RIGHT_COMMAND)
        {
            Vector3 v = local_object_->Velocity();
            v.SetX(v.GetX() + 1.0f);
            local_object_->SetVelocity(v);
        }
        else if(c == MOVE_LEFT_COMMAND)
        {
            Vector3 v = local_object_->Velocity();
            v.SetX(v.GetX() - 1.0f);
            local_object_->SetVelocity(v);
        }
    }

    void ReplicationManager::RandomMove()
    {
        std::cout << "random move is called. " << std::endl;
        Vector3 new_position = local_object_->Position();
        new_position.SetX(new_position.GetX() + delta_x);
        new_position.SetY(new_position.GetY() + delta_y);
    
        local_object_->SetPosition(new_position);
    }

    void ReplicationManager::MoveLeftFixedDistance()
    {
        Vector3 new_position = local_object_->Position();
        new_position.SetX(new_position.GetX() - 80);

        local_object_->SetPosition(new_position);
    }

    void ReplicationManager::MoveRightFixedDistance()
    {
        Vector3 new_position = local_object_->Position();
        new_position.SetX(new_position.GetX() + 80);

        local_object_->SetPosition(new_position);
    }

    void ReplicationManager::ConsistencyCheck()
    {
        assert(replica_objects_.size() == 1 && "replica_objects size error");

        map<String, ReplicaObject *>::iterator iter = replica_objects_.begin();
        Vector3 p = iter->second->Position();

        std::cout << "ConsistencyCheck, replica position is " << p.ToString().UTF8CStr() << std::endl;

        if(::fabs(p.GetX() - delta_x) > 0.1f)
        {
            std::cout << "ConsistencyCheck() x failed, p.x : " << p.GetX() << std::endl;
            log_->SetConsistencyCheckFailed();
        }

        if(::fabs(p.GetY() - delta_y) > 0.1f)
        {
            std::cout << "ConsistencyCheck() y failed, p.y : " << p.GetY() << std::endl;
            log_->SetConsistencyCheckFailed();
        }

        std::cout << "ConsistencyCheck() done." << std::endl;
    }   

    void ReplicationManager::ChangeScene()
    {
        String new_scene_name;
        if(current_scene_name_ == L"test_app_scene_1")
        {
            new_scene_name = L"test_app_scene_2";
        }
        else
        {
            new_scene_name = L"test_app_scene_1";
        }

        if(scene_.get() != NULL)
        {
            if(local_object_.get() != NULL)
            {
                scene_->UnregisterEntity(*(local_object_.get()));
            }

            scene_->Leave();
        }

        std::cout << "entity is now no longer registered to the scene : " << current_scene_name_.UTF8CStr() << std::endl;

        scene_.reset(facade_->JoinScene(
            new_scene_name,
            Badumna::CreateSpatialReplicaDelegate(&ReplicationManager::CreateSpatialReplica, *this),
            Badumna::RemoveSpatialReplicaDelegate(&ReplicationManager::RemoveSpatialReplica, *this)));

        scene_->RegisterEntity(local_object_.get(), 100);
        current_scene_name_ = new_scene_name;

        std::cout << "entity is now registered to the new scene : " << current_scene_name_.UTF8CStr() << std::endl;
    }

    void ReplicationManager::IncreaseCounter()
    {
        int c = local_object_->GetCounter();
        local_object_->SetCounter(++c);
    }

    void ReplicationManager::SendProximityChatMessage()
    {
        chat_channel_->SendMessage(L"Proximity chat message - hello!");
    }

    void ReplicationManager::SendEventToRemoteCopies()
    {
        OutputStream stream;

        stream << 't';
        stream << 'e';
        stream << 's';
        stream << 't';

        facade_->SendCustomMessageToRemoteCopies(*local_object_, &stream);
    }

    void ReplicationManager::SendEventToOriginal()
    {
        if(replica_objects_.size() > 0)
        {
            OutputStream stream;
    
            stream << 't';
            stream << 'e';
            stream << 's';
            stream << 't';

            map<String, ReplicaObject *>::iterator iter = replica_objects_.begin();
            facade_->SendCustomMessageToOriginal(*(iter->second), &stream);
        }
    }

    bool ReplicationManager::Ready() const
    {
        return ready_;
    }
}