//---------------------------------------------------------------------------------
// <copyright file="Barrier.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include "Core/Barrier.h"
#include <boost/interprocess/sync/scoped_lock.hpp>

namespace BadumnaTestApp
{
    using Badumna::String;
    using namespace boost::interprocess;

    Barrier::Barrier(size_t number_of_process, String const &name)
        : set_(false), 
        nop_(number_of_process),
        name_(name),
        remover_(name),
        shm_(NULL),
        region_(NULL),
        mutex_shm_(NULL),
        mutex_region_(NULL),
        mutex_(NULL),
        named_sem_(NULL),
        address_(NULL)
    {
        Initialize();
    }

    void Barrier::Initialize()
    {
        // initialize the counter in shared memory
        shm_.reset(new shared_memory_object(
            open_or_create, 
            name_.UTF8CStr(), 
            read_write));
        shm_->truncate(shared_memory_size);

        region_.reset(new mapped_region(*(shm_.get()), read_write));
        address_ = (int *)region_->get_address();

        String sem_name = name_ + "_sem";
        named_sem_.reset(new named_semaphore(
            open_or_create, 
            sem_name.UTF8CStr(),
            1));

        // initialize the interprocess mutex stored in the shared memory
        String mutex_name = name_ + "_mutex";
        mutex_shm_.reset(new shared_memory_object(
            open_or_create, 
            mutex_name.UTF8CStr(), 
            read_write));
        mutex_shm_->truncate(sizeof(interprocess_mutex));
        
        mutex_region_.reset(new mapped_region(*(mutex_shm_.get()), read_write));
        mutex_ = new (mutex_region_->get_address()) interprocess_mutex;
    
        scoped_lock<interprocess_mutex> lock(*mutex_);
        if(named_sem_->try_wait())
        {
            // initialize the counter to 0
            (*address_) = 0;
        }
    }

    void Barrier::SetAsReady()
    {
        if(set_)
        {
            return;
        }

        scoped_lock<interprocess_mutex> lock(*mutex_);

        (*address_) = (*address_) + 1;
        set_ = true;
    }

    bool Barrier::Ready()
    {
        if(!set_)
        {
            return false;
        }

        scoped_lock<interprocess_mutex> lock(*mutex_);
        bool ready = ((*address_) >= nop_);

        return ready;
    }
}