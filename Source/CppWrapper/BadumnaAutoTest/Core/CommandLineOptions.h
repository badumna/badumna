//---------------------------------------------------------------------------------
// <copyright file="CommandLineOptions.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_COMMANDLINE_OPTIONS_H
#define BADUMNA_AUTO_TEST_COMMANDLINE_OPTIONS_H

#include "App/AppConfig.h"

namespace BadumnaTestApp
{
    int parse_commandline_options(int argc, char **argv, ProgramConfig *config);
}

#endif // BADUMNA_AUTO_TEST_COMMANDLINE_OPTIONS_H