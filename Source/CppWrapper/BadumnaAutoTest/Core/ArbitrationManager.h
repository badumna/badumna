//---------------------------------------------------------------------------------
// <copyright file="ArbitrationManager.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_ARBITRATION_MANAGER_H
#define BADUMNA_AUTO_TEST_ARBITRATION_MANAGER_H

#include <memory>

#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/DataTypes/InputStream.h"
#include "Badumna/Arbitration/IArbitrator.h"
#include "Badumna/Arbitration/ServiceConnectionResultType.h"
#include "Badumna/Utils/BasicTypes.h"

#include "EventLogger/EventLog.h"

namespace BadumnaTestApp
{
    class ArbitrationManager
    {
    public:
        ArbitrationManager(Badumna::NetworkFacade *network_facade, bool server_mode, EventLog *log);

        // client mode
        void ArbitrationConnectionResultHandler(Badumna::ServiceConnectionResultType type);
        void ArbitrationConnectionFailureHandler();
        void ArbitrationServerMessageHandler(Badumna::InputStream *stream);
        void SendArbitrationEvent();

        // server mode
        void ArbitrationClientMessageHandler(int32_t session_id, Badumna::InputStream *stream);
        void ArbitrationClientDisconnectHandler(int32_t session_id);
        void SendArbitrationEventToClient(int32_t destination_session_id);

        bool Ready() const
        {
            return ready_;
        }

    private:
    std::string GetStringFromMessage(Badumna::InputStream *stream);
    void SendStringMessage(std::string const &str, Badumna::OutputStream *stream);

        Badumna::NetworkFacade *facade_;
        EventLog *log_;

        std::auto_ptr<Badumna::IArbitrator> arbitrator_;
        bool arbitration_server_connected_;
        bool ready_;
        int events_sent_;

        DISALLOW_COPY_AND_ASSIGN(ArbitrationManager);
    };
}

#endif // BADUMNA_AUTO_TEST_ARBITRATION_MANAGER_H