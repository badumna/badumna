//---------------------------------------------------------------------------------
// <copyright file="ReplicationManager.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_REPLICATION_MANAGER_H
#define BADUMNA_AUTO_TEST_REPLICATION_MANAGER_H

#include <string>
#include <memory>
#include <map>

#include "Badumna/Chat/IChatSession.h"
#include "Badumna/Chat/IChatChannel.h"
#include "Badumna/Chat/ChatChannelId.h"
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Replication/NetworkScene.h"
#include "Badumna/Replication/ISpatialReplica.h"
#include "Badumna/Utils/BasicTypes.h"

#include "Core/ReplicaObject.h"
#include "Core/LocalObject.h"
#include "EventLogger/EventLog.h"

namespace BadumnaTestApp
{
    class ReplicationManager
    {
    public:
        ReplicationManager(Badumna::NetworkFacade *facade, Badumna::IChatSession *service, EventLog *event_log);
        virtual ~ReplicationManager() {}

        void Shutdown();

        // replication delegates
        Badumna::ISpatialReplica *CreateSpatialReplica(
            Badumna::NetworkScene const &scene, 
            Badumna::BadumnaId const &id, 
            uint32_t type);
        
        void RemoveSpatialReplica(Badumna::NetworkScene const &scene, Badumna::ISpatialReplica const &replica);
    
        Badumna::BadumnaId GetFirstReplica() const;

        // proximity chat
        void ProximityChatMessageHandler(
            Badumna::IChatChannel *channel_id, 
            Badumna::BadumnaId const &user_id, 
            Badumna::String const &message);

        void SendProximityChatMessage();

        // move local objects
        void MoveLocalObject(int command);
        void RandomMove();

        void MoveLeftFixedDistance();
        void MoveRightFixedDistance();

        // other behaviors
        void ConsistencyCheck();
        void ChangeScene();
        void IncreaseCounter();

        // custom events
        void SendEventToRemoteCopies();
        void SendEventToOriginal();

        bool Ready() const;

    protected:
        void Initialize();

        Badumna::IChatSession * chat_session_;
        Badumna::IChatChannel * chat_channel_;
        Badumna::NetworkFacade *facade_;
        EventLog *log_;

        std::auto_ptr<LocalObject> local_object_;
        std::map<Badumna::String, ReplicaObject*> replica_objects_;
        std::auto_ptr<Badumna::NetworkScene> scene_;

        Badumna::String current_scene_name_;

        bool ready_;

        static const float delta_x;
        static const float delta_y;
        static const float delta_z;

    private:
        DISALLOW_COPY_AND_ASSIGN(ReplicationManager);
    };
}

#endif // BADUMNA_AUTO_TEST_REPLICATION_MANAGER_H