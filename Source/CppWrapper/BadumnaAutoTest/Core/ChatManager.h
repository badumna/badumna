//---------------------------------------------------------------------------------
// <copyright file="ChatManager.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_CHATMANAGER_H
#define BADUMNA_AUTO_TEST_CHATMANAGER_H

#include <boost/shared_ptr.hpp>

#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/Chat/IChatSession.h"
#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

#include "Core/FriendsList.h"
#include "EventLogger/EventLog.h"

namespace BadumnaTestApp
{
    class ChatManager
    {
    public:
        ChatManager(boost::shared_ptr<Badumna::NetworkFacade> facade, EventLog *log);
        Badumna::String Initialize(Badumna::String const &friends_list_file);
        bool InviteFriends();

        void SetPresenceStatus(Badumna::ChatStatus status);
        Badumna::ChatStatus GetNextChatStatus() const;

        void SendChatMessage();

        void HandleChannelInvitation(
            Badumna::ChatChannelId const &channel,
            Badumna::String const &username);

        void HandlePrivateMessage(
            Badumna::IChatChannel *channel,
            Badumna::BadumnaId const &user_id, 
            Badumna::String const &message);

        void HandlePresence(
            Badumna::IChatChannel *channel,
            Badumna::BadumnaId const &user_id, 
            Badumna::String const &username, 
            Badumna::ChatStatus status);

        void UnsubscribeFromPrivateChat()
        {
            unsubscribe_after_first_message_ = true;
        }

        bool Ready() const
        {
            return ready_;
        }

    private:
        void SendChatMessage(Badumna::String const &name, Badumna::String const &msg);
        Badumna::String ReadUsername(Badumna::String const &path);

        void RegularProcessing();

        EventLog *log_;
        boost::shared_ptr<Badumna::NetworkFacade> facade_;
        Badumna::IChatSession *session_;
        Badumna::ChatStatus current_status_;
        FriendsList friend_list_;
        Badumna::String my_name_;

        int counter_;
        bool unsubscribe_after_first_message_;
        bool ready_;

        DISALLOW_COPY_AND_ASSIGN(ChatManager);
    };
}

#endif // BADUMNA_AUTO_TEST_CHATMANAGER_H