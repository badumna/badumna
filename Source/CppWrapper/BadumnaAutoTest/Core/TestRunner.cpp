//---------------------------------------------------------------------------------
// <copyright file="TestRunner.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <iostream>
#include "App/AppConfig.h"
#include "App/ConsoleApp.h"
#include "AppBuilder/AppDirector.h"
#include "SimpleTests/SimpleTest.h"
#include "SimpleTests/SimpleTestsFactory.h"
#include "Core/TestRunner.h"

#include "Badumna/Core/RuntimeInitializer.h"
#include "Badumna/Core/BadumnaExceptionCallback.h"
#include "Badumna/Core/InternalErrorCallback.h"
#include "Badumna/DataTypes/String.h"

namespace BadumnaTestApp
{
    using std::exception;
    using std::runtime_error;
    using std::logic_error;
    using std::auto_ptr;
    using std::string;

    using Badumna::String;
    using Badumna::BadumnaExceptionCallback;
    using Badumna::InternalErrorCallback;
    using BadumnaTestApp::ProgramMode;
    using BadumnaTestApp::ConsoleApp;
    using BadumnaTestApp::AppDirector;
    using BadumnaTestApp::ProgramConfig;
    using BadumnaTestApp::SimpleTest;
    using BadumnaTestApp::SimpleTestFactory;

    void exception_callback(String const &stack_trace, String const &exception_type_name, String const &message)
    {
        std::cout << "Unhandled " << exception_type_name.UTF8CStr() << ": " << message.UTF8CStr() << std::endl;
        std::cout << stack_trace.UTF8CStr() << std::endl << std::endl;
    }

    void error_handler(int32_t code, String const &msg)
    {
        std::cout << "error handler is called" << std::endl;
        std::cout << "code : " << code << std::endl;
        std::cout << "msg : " << msg.UTF8CStr() << std::endl;
    }

    /**************************************************************************
     * TestRunner
     *************************************************************************/
    TestRunner::TestRunner(ProgramConfig const &config)
        : config(config)
    {
    }
    
    TestRunner::~TestRunner()
    {
    }

    /**************************************************************************
     * AutoTestRunner
     *************************************************************************/
    AutoTestRunner::AutoTestRunner(ProgramConfig const &config)
        : TestRunner(config)
    {
    }

    AutoTestRunner::~AutoTestRunner()
    {
    }

    /**
     * To run a regular auto test, the steps below are followed:
     *  - Badumna Runtime get initialized. 
     *  - Full app name is constructed using the data passed in from command line options. This helps to make sure that
     *    peers from different tests won't get connected together. Processes in the same test should connect. 
     *  - The AppDirector class builds the app and all its required components based on the specified test mode. 
     *  - The test app get executed. 
     *  - The result is checked/verified. 
     */
    int AutoTestRunner::DoRunTest()
    {
        // regular tests
        BadumnaExceptionCallback callback(exception_callback);
        InternalErrorCallback error_callback(error_handler);

#ifdef WIN32
        Badumna::BadumnaRuntimeInitializer initializer(callback, error_callback);
#else
        Badumna::BadumnaRuntimeInitializer2 initializer(callback, error_callback, "mono_config_file");
#endif  

        Badumna::String app_name = L"badumna_auto_test_app_name_";
        if(config.app_name_salt_.Length() > 0)
        {
            app_name += config.app_name_salt_;
            std::cout << "App name is set to " << app_name.UTF8CStr() << std::endl;
        }
        else
        {
            std::cout << "Warning: app_name_salt is empty..." << std::endl; 
        }

        boost::asio::io_service io;
        AppDirector director(config.mode_);

        auto_ptr<ConsoleApp> app(director.CreateApp(config, io)); 
        bool ret = app->Run();
        if(!ret)
        {
            std::cerr << "app->Run returned -1" << std::endl;
            return -1;
        }

        if(app->Verify())
        {
            return 0;
        }   

        std::cerr << "event log check failed" << std::endl;
        return -1;
    }

    /**************************************************************************
     * SimpleTestRunner
     *************************************************************************/
    SimpleTestRunner::SimpleTestRunner(ProgramConfig const &config)
        : TestRunner(config)
    {
    }

    SimpleTestRunner::~SimpleTestRunner()
    {
    }

    int SimpleTestRunner::DoRunTest()
    {
        // simple test
        auto_ptr<SimpleTest> simple_test(SimpleTestFactory::Create(config));
        simple_test->Initialize();

        int ret = simple_test->RunTest();
        if(ret != 0)
        {
            std::cerr << "simple test run test returned error. " << std::endl;
            return -1;
        }

        simple_test->Shutdown();
        return 0;
    }
}