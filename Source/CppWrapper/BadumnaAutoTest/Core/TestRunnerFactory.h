//---------------------------------------------------------------------------------
// <copyright file="TestRunnerFactory.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_TEST_RUNNER_FACTORY_H
#define BADUMNA_AUTO_TEST_TEST_RUNNER_FACTORY_H

#include "App/AppConfig.h"
#include "Core/TestRunner.h"

namespace BadumnaTestApp
{
    /**
     * The factory class used to create a test runner object. 
     */
    class TestRunnerFactory
    {
    public:
        static TestRunner *Create(ProgramConfig const &config)
        {
            if(config.IsSimpleTest())
            {
                return new SimpleTestRunner(config);
            }
            else
            {
                return new AutoTestRunner(config);
            }
        }
    };
}

#endif // BADUMNA_AUTO_TEST_TEST_RUNNER_FACTORY_H