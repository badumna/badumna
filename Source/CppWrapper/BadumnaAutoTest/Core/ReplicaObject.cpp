//---------------------------------------------------------------------------------
// <copyright file="ReplicaObject.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <iostream>

#include "LocalObject.h"
#include "ReplicaObject.h"

namespace BadumnaTestApp
{
    using Badumna::Vector3;
    using Badumna::BadumnaId;
    using Badumna::InputStream;
    using Badumna::BooleanArray;

    ReplicaObject::ReplicaObject(EventLog *event_log) 
        : log_(event_log),
        position_(0.0f, 0.0f, 0.0f), 
        guid_(Badumna::BadumnaId::None()),
        velocity_(0.0f, 0.0f, 0.0f), 
        radius_(0.0f),
        area_of_interest_radius_(0.0f),
        counter_(0)
    {
    }

    Vector3 ReplicaObject::Velocity() const
    {
        return velocity_;
    }

    void ReplicaObject::SetVelocity(Vector3 const &v)
    {
        velocity_ = v;
    }

    void ReplicaObject::AttemptMovement(Vector3 const &reckoned_position)
    {
        position_ = reckoned_position;
        log_->RecordAttemptMoveEvent();
    }

    BadumnaId ReplicaObject::Guid() const
    {
        return guid_;
    }

    void ReplicaObject::SetGuid(BadumnaId const &id)
    {
        guid_ = id;
    }

    void ReplicaObject::HandleEvent(InputStream *)
    {
        std::cout << "ReplicaObject::HandleEvent called" << std::endl;
        log_->RecordReplicaEvent();
    }

    Vector3 ReplicaObject::Position() const
    {
        return position_;
    }

    void ReplicaObject::SetPosition(Vector3 const &value)
    {
        position_ = value;
    }

    float ReplicaObject::Radius() const
    {
        return radius_;
    }

    void ReplicaObject::SetRadius(float value)
    {
        radius_ = value;
    }

    float ReplicaObject::AreaOfInterestRadius() const
    {
        return area_of_interest_radius_;
    }

    void ReplicaObject::SetAreaOfInterestRadius(float value)
    {
        area_of_interest_radius_ = value;
    }

    void ReplicaObject::Deserialize(
        BooleanArray const &included_parts, 
        InputStream *stream, 
        int /*estimated_milliseconds_since_departure*/)
    {
        if(included_parts[Counter] == true)
        {
            (*stream) >> counter_;
            std::wcout << L"deserialize is called, counter = " << counter_ << L", p = " << position_ << std::endl;

            log_->RecordCounterUpdateEvent();
        }

        log_->RecordDeserializeEvent();
    }
}