//---------------------------------------------------------------------------------
// <copyright file="MultiOriginalReplicationManager.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_MULTI_ORIGINAL_REPLICATION_MANAGER_H
#define BADUMNA_AUTO_TEST_MULTI_ORIGINAL_REPLICATION_MANAGER_H

#include <string>
#include <memory>

#include "Badumna/Chat/IChatSession.h"
#include "Badumna/Chat/ChatChannelId.h"
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Replication/NetworkScene.h"
#include "Badumna/Replication/ISpatialReplica.h"
#include "Badumna/Utils/BasicTypes.h"

#include "Core/ReplicaObject.h"
#include "Core/LocalObject.h"
#include "EventLogger/EventLog.h"

namespace BadumnaTestApp
{
    class MultiOriginalReplicationManager
    {
    public:
        MultiOriginalReplicationManager(Badumna::NetworkFacade *facade, EventLog *event_log);

        void Shutdown();

        // replication delegates
        Badumna::ISpatialReplica *CreateSpatialReplica(
            Badumna::NetworkScene const &scene, 
            Badumna::BadumnaId const &id, 
            uint32_t type);
        
        void RemoveSpatialReplica(Badumna::NetworkScene const &scene, Badumna::ISpatialReplica const &replica);
    
        // proximity chat
        void ProximityChatMessageHandler(
            Badumna::IChatChannel *channel, 
            Badumna::BadumnaId const &user_id, 
            Badumna::String const &message);

        void SendProximityChatMessage();
        void IncreaseCounter();
        void SendEventToRemoteCopies();
        void SendEventToOriginal();

        bool Ready() const
        {
            return num_of_replica_ == 3;
        }

    private:
        void Initialize();

        Badumna::IChatSession * chat_session_;

        Badumna::NetworkFacade *facade_;
        EventLog *log_;

        std::auto_ptr<LocalObject> local_object_1_;
        std::auto_ptr<LocalObject> local_object_2_;
        std::auto_ptr<LocalObject> local_object_3_;

        std::auto_ptr<ReplicaObject> replica_object_1_;
        std::auto_ptr<ReplicaObject> replica_object_2_;
        std::auto_ptr<ReplicaObject> replica_object_3_;

        std::auto_ptr<Badumna::NetworkScene> scene_1_;
        std::auto_ptr<Badumna::NetworkScene> scene_2_;
        std::auto_ptr<Badumna::NetworkScene> scene_3_;

        Badumna::IChatChannel *channel_1_;
        Badumna::IChatChannel *channel_2_;
        Badumna::IChatChannel *channel_3_;

        int num_of_replica_;

        DISALLOW_COPY_AND_ASSIGN(MultiOriginalReplicationManager);
    };
}

#endif // BADUMNA_AUTO_TEST_MULTI_ORIGINAL_REPLICATION_MANAGER_H