//---------------------------------------------------------------------------------
// <copyright file="ArbitrationManager.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <iostream>
#include <string>
#include "Badumna/DataTypes/OutputStream.h"
#include "Badumna/Security/Character.h"
#include "Badumna/Arbitration/ArbitrationDelegates.h"
#include "ArbitrationManager.h"

namespace BadumnaTestApp
{
    using Badumna::ServiceConnectionResultType;
    using Badumna::NetworkFacade;
    using Badumna::String;
    using Badumna::InputStream;
    using Badumna::OutputStream;
    using Badumna::Character;
    using Badumna::ArbitrationConnectionDelegate;
    using Badumna::ArbitrationConnectionFailureDelegate;
    using Badumna::ArbitrationServerMessageDelegate;
    using Badumna::ArbitrationClientMessageDelegate;
    using Badumna::ArbitrationClientDisconnectDelegate;

    ArbitrationManager::ArbitrationManager(NetworkFacade *network_facade, bool server_mode, EventLog *event_log)
        : facade_(network_facade), 
        log_(event_log), 
        arbitrator_(NULL),
        arbitration_server_connected_(false),
        ready_(false),
        events_sent_(0)
    {
        if(!server_mode)
        {
            // client
            arbitrator_.reset(facade_->GetArbitrator(L"arb_test"));
            arbitrator_->Connect(
                ArbitrationConnectionDelegate(&ArbitrationManager::ArbitrationConnectionResultHandler, *this),
                ArbitrationConnectionFailureDelegate(&ArbitrationManager::ArbitrationConnectionFailureHandler, *this),
                ArbitrationServerMessageDelegate(&ArbitrationManager::ArbitrationServerMessageHandler, *this));
        }
        else
        {
            facade_->RegisterArbitrationHandler(
                ArbitrationClientMessageDelegate(&ArbitrationManager::ArbitrationClientMessageHandler, *this),
                60, // timeout in seconds
                ArbitrationClientDisconnectDelegate(&ArbitrationManager::ArbitrationClientDisconnectHandler, *this));

            facade_->AnnounceService(Badumna::ServerType_ArbitrationServer);
        }
    }
    
    void ArbitrationManager::ArbitrationConnectionResultHandler(ServiceConnectionResultType type)
    {
        std::cout << "ArbitrationConnectionResultHandler is called, type: " << type << std::endl;

        if(type == Badumna::ServiceConnection_Success)
        {
            std::cout << "Successfully connected to the arbitration server." << std::endl;
            arbitration_server_connected_ = true;
            ready_ = true;
        }
        else
        {
            std::cout << "ServiceConnectionResultType recieved : " << type << std::endl;
        }
    }

    void ArbitrationManager::ArbitrationConnectionFailureHandler()
    {
        std::cout << "ArbitrationConnectionFailureHandler is called" << std::endl;

        if(arbitration_server_connected_)
        {
            std::cout << "can no longer send arbitration event to the server" << std::endl;
            arbitration_server_connected_ = false;
        }
    }

    void ArbitrationManager::ArbitrationServerMessageHandler(InputStream *)
    {
        std::cout << "Received a server arbitration message." << std::endl;

        log_->RecordServerMessageEvent();
    }

    std::string ArbitrationManager::GetStringFromMessage(InputStream *stream)
    {
        std::string::size_type len;
        std::string result;
        *stream >> len;
        for (std::string::size_type i = 0; i < len; ++i)
        {
            char ch;
            *stream >> ch;
            result += ch;
        }
        return result;
    }

    void ArbitrationManager::SendStringMessage(std::string const &str, OutputStream *stream)
    {
        std::string::const_iterator iter;
        *stream << str.length();
        iter = str.begin();
        for (; iter != str.end(); ++iter)
        {
            *stream << *iter;
        }
    }

    // to server
    void ArbitrationManager::SendArbitrationEvent()
    {
        if(arbitrator_.get() == NULL)
        {
            return;
        }

        std::cout << "Sending an arbitration event to the server." << std::endl;

        OutputStream s;
        
        switch (events_sent_)
        {
            case 0:
                {
                    std::string public_address(facade_->GetNetworkStatus().GetPublicAddress().UTF8CStr());
                    std::cout << "Sending public address: " << public_address << std::endl;
                    SendStringMessage(public_address, &s);
                }
                break;
            case 1:
                {
                    std::auto_ptr<Character> myCharacter = facade_->GetCharacter();
                    assert(myCharacter.get() != NULL && "GetCharacter returned NULL");
                    std::string response = myCharacter->Name().UTF8CStr();
                    std::cout << "Sending character: " << response << std::endl;
                    SendStringMessage(response, &s);
                }
                break;
            default:
                s << 't' << 'e' << 's' << 't';
                break;
        }

        arbitrator_->SendEvent(&s);
        ++events_sent_;
    }

    void ArbitrationManager::ArbitrationClientMessageHandler(int32_t session_id, InputStream *stream)
    {
        std::cout << "Received a client arbitration message, session id is " << session_id << std::endl;
        ready_ = true;

        switch (events_sent_)
        {
            case 0:
                {
                    std::string result = GetStringFromMessage(stream);
                    std::cout << "Received public address: " << result << std::endl;

                    std::string clientBadumnaId = facade_->GetBadumnaIdForArbitrationSession(session_id).ToString().UTF8CStr();
                    assert(clientBadumnaId.find(result) == 0 && "GetBadumnaIdForArbitrationSession result doesn't match public address sent by peer");
                }
                break;
            case 1:
                {
                    std::string result = GetStringFromMessage(stream);
                    std::cout << "Received character: " << result << std::endl;

                    std::auto_ptr<Character> clientCharacter = facade_->GetCharacterForArbitrationSession(session_id);
                    assert(clientCharacter.get() != NULL && "GetCharacterForArbitrationSession returned NULL");
                
                    std::string expected = clientCharacter->Name().UTF8CStr();

                    assert(expected.find(result) == 0 && "Character name doesn't match the one sent by peer");
                }
                break;
            default:
                char ch1, ch2, ch3, ch4;
                *stream >> ch1;
                *stream >> ch2;
                *stream >> ch3;
                *stream >> ch4;
                break;
        }

        SendArbitrationEventToClient(session_id);
    }
    
    void ArbitrationManager::ArbitrationClientDisconnectHandler(int32_t session_id)
    {
        std::cout << "Arbitration Client disconnection detected, session_id is " << session_id << std::endl;
    }

    void ArbitrationManager::SendArbitrationEventToClient(int32_t destination_session_id)
    {
        OutputStream s;
        s << 'a' << 'c' << 'k';
        facade_->SendServerArbitrationEvent(destination_session_id, &s);
        ++events_sent_;
    }
}