//---------------------------------------------------------------------------------
// <copyright file="LocalObject.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <stdexcept>
#include <iostream>
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/Replication/SpatialEntityStateSegment.h"

#include "LocalObject.h"

namespace BadumnaTestApp
{
    using std::runtime_error;
    using Badumna::NetworkFacade;
    using Badumna::Vector3;
    using Badumna::BadumnaId;
    using Badumna::InputStream;
    using Badumna::OutputStream;
    using Badumna::BooleanArray;

    LocalObject::LocalObject(NetworkFacade *network_facade, EventLog *event_log)
        : facade_(network_facade), 
        guid_(Badumna::BadumnaId::None()),
        log_(event_log),
        velocity_(0.0f, 0.0f, 0.0f), 
        position_(0.0f, 0.0f, 0.0f), 
        radius_(0.0f),
        area_of_interest_radius_(0.0f),
        counter_(0)
    {
    }

    Vector3 LocalObject::Velocity() const
    {
        return velocity_;
    }

    void LocalObject::SetVelocity(Vector3 const &v)
    {
        velocity_ = v;
        facade_->FlagForUpdate(*this, Badumna::StateSegment_Velocity);
    }

    void LocalObject::AttemptMovement(Vector3 const &/*reckoned_position*/)
    {
        throw runtime_error("AttemptMovement is being called on original entity - badumna/wrapper bug.");
    }

    BadumnaId LocalObject::Guid() const
    {
        return guid_;
    }

    void LocalObject::SetGuid(BadumnaId const &id)
    {
        guid_ = id;
    }

    void LocalObject::HandleEvent(InputStream *)
    {
        std::cout << "LocalObject::HandleEvent is called" << std::endl;
        log_->RecordOriginalEvent();
    }

    Vector3 LocalObject::Position() const
    {
        return position_;
    }

    void LocalObject::SetPosition(Vector3 const &value)
    {
        position_ = value;
        facade_->FlagForUpdate(*this, Badumna::StateSegment_Position);
    }

    float LocalObject::Radius() const
    {
        return radius_;
    }

    void LocalObject::SetRadius(float value)
    {
        radius_ = value;
        facade_->FlagForUpdate(*this, Badumna::StateSegment_Radius);
    }

    float LocalObject::AreaOfInterestRadius() const
    {
        return area_of_interest_radius_;
    }

    void LocalObject::SetAreaOfInterestRadius(float value)
    {
        area_of_interest_radius_ = value;
        facade_->FlagForUpdate(*this, Badumna::StateSegment_InterestRadius);
    }

    void LocalObject::Serialize(BooleanArray const &required_parts, OutputStream *stream)
    {
        if(required_parts[Counter])
        {
            (*stream) << counter_;
        }

        log_->RecordSerializeEvent();
    }

    void LocalObject::SetCounter(int value)
    {
        counter_ = value;
        facade_->FlagForUpdate(*this, Counter);
    }
}