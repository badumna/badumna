//---------------------------------------------------------------------------------
// <copyright file="MultiOriginalReplicationManager.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include <stdexcept>
#include <iostream>
#include "Badumna/DataTypes/Vector3.h"
#include "Badumna/Utils/BasicTypes.h"

#include <boost/date_time/local_time/local_time.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "Console/IConsole.h"
#include "Core/MultiOriginalReplicationManager.h"

namespace BadumnaTestApp
{
    using Badumna::Vector3;
    using Badumna::NetworkFacade;
    using Badumna::IChatSession;
    using Badumna::ChatChannelId;
    using Badumna::IChatChannel;
    using Badumna::ISpatialReplica;
    using Badumna::NetworkScene;
    using Badumna::BadumnaId;
    using Badumna::String;
    using Badumna::OutputStream;

    MultiOriginalReplicationManager::MultiOriginalReplicationManager(
        NetworkFacade *network_facade, 
        EventLog* event_log)
        : chat_session_(network_facade->ChatSession()),
        facade_(network_facade),
        log_(event_log),
        local_object_1_(new LocalObject(network_facade, event_log)),
        local_object_2_(new LocalObject(network_facade, event_log)),
        local_object_3_(new LocalObject(network_facade, event_log)),
        replica_object_1_(NULL),
        replica_object_2_(NULL),
        replica_object_3_(NULL),
        scene_1_(NULL),
        scene_2_(NULL),
        scene_3_(NULL),
        channel_1_(NULL),
        channel_2_(NULL),
        channel_3_(NULL),
        num_of_replica_(0)
    {
        Initialize();
    }

    void MultiOriginalReplicationManager::Shutdown()
    {
        if(scene_1_.get() != NULL)
        {
            if(local_object_1_.get() != NULL)
            {
                std::cout << "going to call unregister" << std::endl;
                scene_1_->UnregisterEntity(*(local_object_1_.get()));
            }

            std::cout << "going to call leave" << std::endl;
            scene_1_->Leave();
        }

        if(scene_2_.get() != NULL)
        {
            if(local_object_2_.get() != NULL)
            {
                std::cout << "going to call unregister" << std::endl;
                scene_2_->UnregisterEntity(*(local_object_2_.get()));
            }

            std::cout << "going to call leave" << std::endl;
            scene_2_->Leave();
        }

        if(scene_3_.get() != NULL)
        {
            if(local_object_3_.get() != NULL)
            {
                std::cout << "going to call unregister" << std::endl;
                scene_3_->UnregisterEntity(*(local_object_3_.get()));
            }

            std::cout << "going to call leave" << std::endl;
            scene_3_->Leave();
        }
    }

    void MultiOriginalReplicationManager::Initialize()
    {
        facade_->RegisterEntityDetails(100.0f, 1.0f);

        // join scene
        scene_1_.reset(facade_->JoinScene(
            L"test_app_scene_1",
            Badumna::CreateSpatialReplicaDelegate(&MultiOriginalReplicationManager::CreateSpatialReplica, *this),
            Badumna::RemoveSpatialReplicaDelegate(&MultiOriginalReplicationManager::RemoveSpatialReplica, *this)));

        scene_2_.reset(facade_->JoinScene(
            L"test_app_scene_2",
            Badumna::CreateSpatialReplicaDelegate(&MultiOriginalReplicationManager::CreateSpatialReplica, *this),
            Badumna::RemoveSpatialReplicaDelegate(&MultiOriginalReplicationManager::RemoveSpatialReplica, *this)));

        scene_3_.reset(facade_->JoinScene(
            L"test_app_scene_3",
            Badumna::CreateSpatialReplicaDelegate(&MultiOriginalReplicationManager::CreateSpatialReplica, *this),
            Badumna::RemoveSpatialReplicaDelegate(&MultiOriginalReplicationManager::RemoveSpatialReplica, *this)));

        // initialize the local avatar
        Vector3 initial_position(0.0f, 0.0f, 0.0f);
        Vector3 initial_velocity(0.0f, 0.0f, 0.0f);

        // local object 1
        local_object_1_->SetPosition(initial_position);
        local_object_1_->SetVelocity(initial_velocity);
        local_object_1_->SetRadius(100.0f);
        local_object_1_->SetAreaOfInterestRadius(100.0f);

        // register the local avatar to the above scene
        scene_1_->RegisterEntity(local_object_1_.get(), 100);
        local_object_1_->SetPosition(initial_position);

        channel_1_ = chat_session_->SubscribeToProximityChannel(
            local_object_1_->Guid(), 
            Badumna::ChatMessageHandler(&MultiOriginalReplicationManager::ProximityChatMessageHandler, *this));

        // local object 2
        local_object_2_->SetPosition(initial_position);
        local_object_2_->SetVelocity(initial_velocity);
        local_object_2_->SetRadius(100.0f);
        local_object_2_->SetAreaOfInterestRadius(100.0f);

        // register the local avatar to the above scene
        scene_2_->RegisterEntity(local_object_2_.get(), 100);
        local_object_2_->SetPosition(initial_position);

        channel_2_ = chat_session_->SubscribeToProximityChannel(
            local_object_2_->Guid(), 
            Badumna::ChatMessageHandler(&MultiOriginalReplicationManager::ProximityChatMessageHandler, *this));

        // local object 3
        local_object_3_->SetPosition(initial_position);
        local_object_3_->SetVelocity(initial_velocity);
        local_object_3_->SetRadius(100.0f);
        local_object_3_->SetAreaOfInterestRadius(100.0f);

        // register the local avatar to the above scene
        scene_3_->RegisterEntity(local_object_3_.get(), 100);
        local_object_3_->SetPosition(initial_position);

        channel_3_ = chat_session_->SubscribeToProximityChannel(
            local_object_3_->Guid(), 
            Badumna::ChatMessageHandler(&MultiOriginalReplicationManager::ProximityChatMessageHandler, *this));

        std::cout << "object_1 = " << local_object_1_->Guid().ToString().UTF8CStr() << std::endl;
        std::cout << "object_2 = " << local_object_2_->Guid().ToString().UTF8CStr() << std::endl;
        std::cout << "object_3 = " << local_object_3_->Guid().ToString().UTF8CStr() << std::endl;
    }

    ISpatialReplica *MultiOriginalReplicationManager::CreateSpatialReplica(
        NetworkScene const & scene, 
        BadumnaId const &id, 
        uint32_t /*type*/)
    {
        std::ostringstream msg;
        const boost::posix_time::ptime now = boost::posix_time::second_clock::local_time();
        boost::posix_time::time_facet*const f = new boost::posix_time::time_facet("%H:%M:%S");
        msg.imbue(std::locale(msg.getloc(),f));
        msg << now;

        num_of_replica_++;

        std::cout << "%%% (t: " <<  msg.str() << ") create replica delegate is called, id = ";
        std::cout << id.ToString().UTF8CStr();
        std::cout << ", scene = " << scene.Name().UTF8CStr() << std::endl; 
        log_->RecordCreateReplicaEvent();

        if(scene.Name() == L"test_app_scene_1")
        {
            if(replica_object_1_.get() == NULL)
            {
                replica_object_1_.reset(new ReplicaObject(log_));
                replica_object_1_->SetGuid(id);
            }

            return replica_object_1_.get();
        }
        else if(scene.Name() == L"test_app_scene_2")
        {
            if(replica_object_2_.get() == NULL)
            {
                replica_object_2_.reset(new ReplicaObject(log_));
                replica_object_2_->SetGuid(id);
            }

            return replica_object_2_.get();
        }
        else if(scene.Name() == L"test_app_scene_3")
        {
            if(replica_object_3_.get() == NULL)
            {
                replica_object_3_.reset(new ReplicaObject(log_));
                replica_object_3_->SetGuid(id);
            }

            return replica_object_3_.get();
        }
        else
        {
            throw std::runtime_error("unknown scene (create replica).");
        }
    }
    
    void MultiOriginalReplicationManager::RemoveSpatialReplica(
        NetworkScene const &scene, 
        ISpatialReplica const &/*replica*/)
    {
        std::cout << "%%% remove replica is called, scene name = " << scene.Name().UTF8CStr() << std::endl;
        log_->RecordRemoveReplicaEvent();

        if(scene.Name() == L"test_app_scene_1")
        {
            if(replica_object_1_.get() != NULL)
            {
                ReplicaObject *obj = replica_object_1_.release();
                delete obj;
            }
        }
        else if(scene.Name() == L"test_app_scene_2")
        {
            if(replica_object_2_.get() != NULL)
            {
                ReplicaObject *obj = replica_object_2_.release();
                delete obj;
            }
        }
        else if(scene.Name() == L"test_app_scene_3")
        {
            if(replica_object_3_.get() != NULL)
            {
                ReplicaObject *obj = replica_object_3_.release();
                delete obj;
            }
        }
        else
        {
            throw std::runtime_error("unknown scene. (remove replica)");
        }
    }

    void MultiOriginalReplicationManager::ProximityChatMessageHandler(
        IChatChannel *channel,
        BadumnaId const &sender_id, 
        String const &message)
    {
        std::wcout << L"[Proximity chat]: " << message.CStr() << " on channel " << channel->Id().ToString().UTF8CStr() << " from entity " << sender_id.ToString().UTF8CStr() << std::endl;
        
        //TODO: id is not exposed, need another way to compare for self-messages
        /*if(sender_id.operator==(channel->Id()))
        {
            std::cout << "Received a loopback proximity chat message" << std::endl;
            throw new std::runtime_error("Received a loopback proximity chat message");
        }*/
        log_->RecordProximityMessageEvent();
    }
    
    void MultiOriginalReplicationManager::IncreaseCounter()
    {
        int c1 = local_object_1_->GetCounter();
        local_object_1_->SetCounter(++c1);

        int c2 = local_object_2_->GetCounter();
        local_object_2_->SetCounter(++c2);

        int c3 = local_object_3_->GetCounter();
        local_object_3_->SetCounter(++c3);
    }

    void MultiOriginalReplicationManager::SendProximityChatMessage()
    {
        channel_1_->SendMessage(L"Proximity chat message - hello!");
        channel_3_->SendMessage(L"Proximity chat message - hello!");
        channel_3_->SendMessage(L"Proximity chat message - hello!");
    }

    void MultiOriginalReplicationManager::SendEventToRemoteCopies()
    {
        OutputStream stream1;
        stream1 << 't';
        facade_->SendCustomMessageToRemoteCopies(*local_object_1_, &stream1);

        OutputStream stream2;
        stream2 << 't';
        facade_->SendCustomMessageToRemoteCopies(*local_object_2_, &stream2);

        OutputStream stream3;
        stream3 << 't';
        facade_->SendCustomMessageToRemoteCopies(*local_object_3_, &stream3);
    }

    void MultiOriginalReplicationManager::SendEventToOriginal()
    {
        if(replica_object_1_.get() != NULL)
        {
            OutputStream stream1;
            stream1 << 't';
            facade_->SendCustomMessageToOriginal(*replica_object_1_, &stream1);
        }

        if(replica_object_2_.get() != NULL)
        {
            OutputStream stream2;
            stream2 << 't';
            facade_->SendCustomMessageToOriginal(*replica_object_2_, &stream2);
        }

        if(replica_object_3_.get() != NULL)
        {
            OutputStream stream3;
            stream3 << 't';
            facade_->SendCustomMessageToOriginal(*replica_object_3_, &stream3);
        }
    }
}