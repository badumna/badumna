//---------------------------------------------------------------------------------
// <copyright file="SinglePeerDCTestNPC.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_SINGLE_PEER_DC_TEST_NPC_H
#define BADUMNA_AUTO_TEST_SINGLE_PEER_DC_TEST_NPC_H

#include <vector>
#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/Controller/DistributedSceneController.h"

#include "EventLogger/EventLog.h"
#include "LocalObject.h"
#include "ReplicaObject.h"
#include "Follower.h"

namespace BadumnaTestApp
{
    class SinglePeerDCTestNPC : public Badumna::DistributedSceneController
    {
    public:
        SinglePeerDCTestNPC(Badumna::NetworkFacade *facade, Badumna::String const &name);

        virtual uint32_t GetEntityType() const;

        virtual Badumna::ISpatialOriginal* TakeControlOfEntity(
            Badumna::BadumnaId const &entity_id, 
            uint32_t entity_type);

        virtual Badumna::ISpatialReplica* InstantiateRemoteEntity(
            Badumna::BadumnaId const &entity_id, 
            uint32_t entity_type);

        virtual void RemoveEntity(Badumna::ISpatialReplica const &replica);
        
        virtual void Checkpoint(Badumna::OutputStream *stream);
        virtual void Recover(Badumna::InputStream *stream);
        virtual void Recover();
        
        virtual void Process(int duration_in_ms);
        virtual void Wake();
        virtual void Sleep();

    private:
        LocalObject *original_;
        std::vector<Badumna::ISpatialReplica*> replica_objects_;
        Follower *follower_;

        std::auto_ptr<EventLog> logger_;

        int process_called_;

        DISALLOW_COPY_AND_ASSIGN(SinglePeerDCTestNPC);
    };
}

#endif // BADUMNA_AUTO_TEST_SINGLE_PEER_DC_TEST_NPC_H