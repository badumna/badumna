//---------------------------------------------------------------------------------
// <copyright file="ChatManager.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include <stdexcept>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>

#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/Chat/IChatChannel.h"
#include "Badumna/Chat/ChatStatus.h"
#include "Badumna/Chat/ChatDelegates.h"
#include "Badumna/DataTypes/String.h"

#include "ChatManager.h"

namespace BadumnaTestApp
{
    using std::range_error;
    using std::runtime_error;
    using std::vector;
    using std::ifstream;
    using std::cout;
    using std::string;
    using Badumna::BadumnaId;
    using Badumna::IChatSession;
    using Badumna::ChatStatus;
    using Badumna::ChatChannelId;
    using Badumna::IChatChannel;
    using Badumna::ChatInvitationHandler;
    using Badumna::ChatMessageHandler;
    using Badumna::ChatPresenceHandler;
    using Badumna::String;

    ChatManager::ChatManager(boost::shared_ptr<Badumna::NetworkFacade> facade, EventLog *event_log)
        : log_(event_log), 
        facade_(facade),
        current_status_(Badumna::ChatStatus_Online),
        session_(),
        friend_list_(),
        my_name_(),
        counter_(0),
        unsubscribe_after_first_message_(false),
        ready_(false)
    {
    }

    String ChatManager::Initialize(String const &friends_list_file)
    {
        std::cout << "ChatManager initializing..." << std::endl;
        my_name_ = ReadUsername(friends_list_file);

        friend_list_.LoadFriendsList(friends_list_file);
        friend_list_.RemoveFriend(my_name_); // Talking to yourself is a sign of madness...
        
        return my_name_;
    }

    bool ChatManager::InviteFriends()
    {
        session_ = facade_->ChatSession();
        std::cout << "Opening private channels..." << std::endl;
        session_->OpenPrivateChannels(ChatInvitationHandler(&ChatManager::HandleChannelInvitation, *this));
        std::cout << "Going online..." << std::endl;
        session_->ChangePresence(Badumna::ChatStatus_Online);

        vector<String> vec = friend_list_.GetFriendNames();
        for(vector<String>::iterator iter = vec.begin(); iter != vec.end(); iter++)
        {
            std::cout << "Inviting friend to chat: " << iter->UTF8CStr() << std::endl;
            session_->InviteUserToPrivateChannel(*iter);
        }

        return true;
    }

    ChatStatus ChatManager::GetNextChatStatus() const
    {
        if(current_status_ == Badumna::ChatStatus_Online)
        {
            return Badumna::ChatStatus_Away;
        }

        if(current_status_ == Badumna::ChatStatus_Away)
        {
            return Badumna::ChatStatus_Chat;
        }

        if(current_status_ == Badumna::ChatStatus_Chat)
        {
            return Badumna::ChatStatus_Online;
        }

        throw range_error("invalid current chat presence status");
    }

    void ChatManager::SetPresenceStatus(ChatStatus status)
    {
        std::cout << "Setting presence to: " << status << std::endl;
        session_->ChangePresence(status);
        current_status_ = status;
    }

    String ChatManager::ReadUsername(String const &path)
    {
        string line;
        String name;
        ifstream list_file(path.UTF8CStr());
    
        if(list_file.is_open())
        {
            if(list_file.good())
            {
                getline(list_file, line);
                name = line.c_str();
                list_file.close();
            }
            else
            {
                list_file.close();
                throw runtime_error("can't read from the friend list.");
            }
        }
        else
        {
            throw runtime_error("can't open the friend list.");
        }

        return name;
    }

    void ChatManager::HandleChannelInvitation(ChatChannelId const &channel_id, String const &username)
    {
        ready_ = true;
        if(friend_list_.IsFriend(username))
        {
            std::cout << "Got invitation from friend: " << username.UTF8CStr() << std::endl;
            Friend f = friend_list_.GetFriend(username);
            if(f.IsValid())
            {
                if(f.Channel()->Id() == channel_id)
                {
                    // we already have this channel
                    return;
                }
                f.Channel()->Unsubscribe();
            }
            ChatMessageHandler messageHandler = ChatMessageHandler(&ChatManager::HandlePrivateMessage, *this);
            ChatPresenceHandler presenceHandler = ChatPresenceHandler(&ChatManager::HandlePresence, *this);
            Badumna::IChatChannel* channel = session_->AcceptInvitation(channel_id, messageHandler, presenceHandler);
            friend_list_.SetFriendChatChannel(username, channel);
        }
        else
        {
            throw runtime_error("unknown friend.");
        }
    }
    
    void ChatManager::HandlePrivateMessage(
        IChatChannel *channel,
        BadumnaId const &/*user_id*/, 
        String const &message)
    {
        String name = friend_list_.GetFriendNameByChannel(channel);
        std::wcout << L"[" << name.CStr() << L" message] : " << message.CStr() << std::endl;
        log_->RecordPrivateChatMessageEvent();

        if(unsubscribe_after_first_message_)
        {
            std::wcout << L"(now unsubscribing from this channel)" << std::endl;
            friend_list_.SetFriendChatChannel(name, NULL);
            channel->Unsubscribe();
            unsubscribe_after_first_message_ = false;
        }
    }
    
    void ChatManager::HandlePresence(
        IChatChannel * /*channel*/,
        BadumnaId const & /*user_id*/, 
        String const &username, 
        ChatStatus status)
    {
        std::wcout << L"[" << username.CStr() << L" status] : " << status << std::endl;

        ChatStatus oldStatus = friend_list_.GetFriend(username).Status();

        friend_list_.SetFriendChatStatus(username, status);
        if(status == Badumna::ChatStatus_Offline)
        {
            Friend f = friend_list_.GetFriend(username);
            if(f.IsValid())
            {
                f.SetToInvalid();
            }

            // don't record offline events, as they are inconsistent between peers
            // (whoever goes offline first gets one less event)
            return;
        }

        if(status == Badumna::ChatStatus_Online && oldStatus == Badumna::ChatStatus_Offline)
        {
            std::cout << "(friend came online)" << std::endl;
            log_->RecordFriendCameOnlineEvent();
        }
        else
        {
            log_->RecordPrivateChatPresenceEvent();
        }
    }

    void ChatManager::SendChatMessage()
    {
        String name;
        if(my_name_ == L"john")
        {
            name = L"mary";
        }
        else
        {
            name = L"john";
        }

        std::wstringstream ss;
        ss << L"private chat msg, c = " << counter_++ << std::endl;
        
        Friend f = friend_list_.GetFriend(name);
        if(f.IsValid() && f.Name() == name)
        {
            String msg = ss.str().c_str();
            IChatChannel* channel = f.Channel();
            if(channel != NULL)
            {
                std::cout << "going to call send channel message on channel: " << channel << std::endl;
                channel->SendMessage(msg);
            }
            else
            {
                std::cout << "Friend's channel has been unsubscribed - ignoring message send" << std::endl;
            }
        }
        else
        {
            std::cout << "Invalid friend: " << name.UTF8CStr() << std::endl;
            throw runtime_error("invalid friend!");
        }
    }
}