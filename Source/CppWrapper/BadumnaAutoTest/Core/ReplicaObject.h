//---------------------------------------------------------------------------------
// <copyright file="ReplicaObject.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_REPLICAOBJECT_H
#define BADUMNA_AUTO_TEST_REPLICAOBJECT_H

#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/DataTypes/Vector3.h"
#include "Badumna/Replication/IDeadReckonableSpatialReplica.h"
#include "Badumna/Utils/BasicTypes.h"

#include "EventLogger/EventLog.h"

namespace BadumnaTestApp
{
    class ReplicaObject : public Badumna::IDeadReckonableSpatialReplica
    {
    public:
        ReplicaObject(EventLog *log);

        //
        // IEntity properties
        // 
        Badumna::BadumnaId Guid() const;
        void SetGuid(Badumna::BadumnaId const &id);
        void HandleEvent(Badumna::InputStream *stream);

        //
        // ISpatialEntity properties
        //
        Badumna::Vector3 Position() const;
        void SetPosition(Badumna::Vector3 const &position);
        float Radius() const;
        void SetRadius(float val);
        float AreaOfInterestRadius() const;
        void SetAreaOfInterestRadius(float val);

        void Deserialize(
            Badumna::BooleanArray const &included_part, 
            Badumna::InputStream *stream, 
            int estimated_milliseconds_since_departure);

        // Deadreckoning
        Badumna::Vector3 Velocity() const;
        void SetVelocity(Badumna::Vector3 const &velocity);
        void AttemptMovement(Badumna::Vector3 const &reckoned_position);

        //
        // app specific.
        // 
        inline int GetCounter()
        {
            return counter_;
        }

        inline void SetCounter(int value)
        {
            counter_ = value;
        }

    private:
        EventLog *log_;
        Badumna::Vector3 position_;
        Badumna::BadumnaId guid_;
        Badumna::Vector3 velocity_;
        float radius_;
        float area_of_interest_radius_;

        int counter_; 

        DISALLOW_COPY_AND_ASSIGN(ReplicaObject);
    };
}

#endif // BADUMNA_AUTO_TEST_REPLICAOBJECT_H