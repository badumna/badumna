//---------------------------------------------------------------------------------
// <copyright file="Friend.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_FRIEND_H
#define BADUMNA_AUTO_TEST_FRIEND_H

#include <memory>
#include "Badumna/Chat/IChatChannel.h"
#include "Badumna/Chat/ChatStatus.h"
#include "Badumna/Chat/ChatChannelId.h"
#include "Badumna/DataTypes/String.h"

namespace BadumnaTestApp
{
    class Friend
    {
    public:
        Friend()
            : valid_(false),
            username_(),
            status_(Badumna::ChatStatus_Offline),
            channel_()
        {
        }

        Friend(Badumna::String const &name)
            : valid_(false),
            username_(name),
            status_(Badumna::ChatStatus_Offline),
            channel_()
        {
        }

        Friend(Friend const &other)
            : valid_(other.valid_),
            username_(other.username_),
            status_(other.status_),
            channel_()
        {
            if(valid_)
            {
                channel_ = other.channel_;
            }
        }

        Friend& operator=(Friend const &rhs)
        {
            if(this != &rhs)
            {
                valid_ = rhs.valid_;
                username_ = rhs.username_;
                status_ = rhs.status_;
                
                if(valid_)
                {
                    channel_ = rhs.channel_;
                }
            }

            return *this;
        }

        Badumna::String Name() const 
        {
            return username_;
        }

        void SetName(Badumna::String const &name)
        {
            username_ = name;
        }

        Badumna::ChatStatus Status() const
        {
            return status_;
        }

        void SetStatus(Badumna::ChatStatus s)
        {
            status_ = s;
        }

        Badumna::IChatChannel *Channel() const
        {
            return channel_;
        }

        void SetChannel(Badumna::IChatChannel *channel)
        {
            channel_ = channel;
            valid_ = true;
        }

        bool IsValid() const
        {
            return valid_;
        }

        void SetToInvalid()
        {
            valid_ = false;
        }

    private:
        bool valid_;
        Badumna::String username_;
        Badumna::ChatStatus status_;
        Badumna::IChatChannel *channel_;
    };
}

#endif // BADUMNA_EXAMPLE_FRIEND_H