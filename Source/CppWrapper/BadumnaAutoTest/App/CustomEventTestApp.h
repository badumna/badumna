//---------------------------------------------------------------------------------
// <copyright file="CustomEventTestApp.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_CUSTOM_EVENT_TEST_APP_H
#define BADUMNA_AUTO_TEST_CUSTOM_EVENT_TEST_APP_H

#include <memory>
#include <boost/asio.hpp>
#include "App/ReplicationTestApp.h"
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/Utils/BasicTypes.h"

namespace BadumnaTestApp
{
    class CustomEventTestApp : public ReplicationTestApp
    {
    public:
        CustomEventTestApp(
            ProgramConfig config, 
            Badumna::NetworkFacade *facade, 
            boost::asio::io_service &io, 
            IConsole *console, EventLog *log)
            : ReplicationTestApp(config, facade, io, console, log)
        {
        }

    protected:
        virtual void DoHandleCommand(int c)
        {
            if(c == SEND_EVENT_TO_REMOTE_COPIES)
            {
                replication_manager_->SendEventToRemoteCopies();
            }
            else if (c == SEND_EVENT_TO_ORIGINAL)
            {
                replication_manager_->SendEventToOriginal();
            }
            else
            {
                ReplicationTestApp::DoHandleCommand(c);
            }
        }

        virtual void PrintTips() const
        {
            std::cout << "This is the CustomEventTestApp." << std::endl;
        }

        DISALLOW_COPY_AND_ASSIGN(CustomEventTestApp);
    };
}

#endif // BADUMNA_AUTO_TEST_PROXIMITY_CHAT_TEST_APP_H