//---------------------------------------------------------------------------------
// <copyright file="MultiOriginalReplicationTestApp.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_MULTI_ORIGINAL_REPLICATION_TEST_APP_H
#define BADUMNA_AUTO_TEST_MULTI_ORIGINAL_REPLICATION_TEST_APP_H

#include <memory>
#include <boost/asio.hpp>
#include "App/ConsoleApp.h"
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/Utils/BasicTypes.h"

#include "Core/MultiOriginalReplicationManager.h"

namespace BadumnaTestApp
{
    class MultiOriginalReplicationTestApp : public ConsoleApp
    {
    public:
        MultiOriginalReplicationTestApp(
            ProgramConfig config, 
            Badumna::NetworkFacade *facade, 
            boost::asio::io_service &io, 
            IConsole *console, 
            EventLog *log)
            : ConsoleApp(config, facade, io, console, log),
            replication_manager_(NULL)
        {
        }

    protected:
        virtual void DoInitialize()
        {
            replication_manager_.reset(new MultiOriginalReplicationManager(facade_.get(), log_.get()));
        }

        virtual void DoHandleCommand(int c)
        {
            if(c == INCREASE_COUNTER_COMMAND)
            {
                replication_manager_->IncreaseCounter();
            }
            else if(c == SEND_EVENT_TO_REMOTE_COPIES)
            {
                replication_manager_->SendEventToRemoteCopies();
            }
            else if(c == SEND_EVENT_TO_ORIGINAL)
            {
                replication_manager_->SendEventToOriginal();
            }
            else if(c == SEND_PROXIMITY_CHAT_COMMAND)
            {
                replication_manager_->SendProximityChatMessage();
            }
            else
            {
                ConsoleApp::DoHandleCommand(c);
            }
        }

        virtual void DoShutdown()
        {
            replication_manager_->Shutdown();
        }

        virtual bool IsReady() const
        {
            return replication_manager_->Ready();
        }

        virtual void PrintTips() const
        {
            std::cout << "This is the MultiOriginalReplicationTestApp." << std::endl;
        }

        std::auto_ptr<MultiOriginalReplicationManager> replication_manager_;
        DISALLOW_COPY_AND_ASSIGN(MultiOriginalReplicationTestApp);
    };
}

#endif // BADUMNA_AUTO_TEST_MULTI_ORIGINAL_REPLICATION_TEST_APP_H