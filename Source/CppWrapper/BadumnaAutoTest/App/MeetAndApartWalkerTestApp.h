//---------------------------------------------------------------------------------
// <copyright file="MeetAndApartWalkerTestApp.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_MEET_AND_APART_WALKER_TEST_APP_H
#define BADUMNA_AUTO_TEST_MEET_AND_APART_WALKER_TEST_APP_H

#include <memory>
#include <boost/asio.hpp>
#include "App/ConsoleApp.h"
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/Utils/BasicTypes.h"

#include "Core/ReplicationManager.h"

namespace BadumnaTestApp
{
    class MeetAndApartWalkerTestApp : public ConsoleApp
    {
    public:
        MeetAndApartWalkerTestApp(
            ProgramConfig config, 
            Badumna::NetworkFacade *facade, 
            boost::asio::io_service &io, 
            IConsole *console, 
            EventLog *log)
            : ConsoleApp(config, facade, io, console, log),
            replication_manager_(NULL)
        {
        }

    protected:
        virtual void DoInitialize()
        {
            replication_manager_.reset(
                new ReplicationManager(facade_.get(), facade_->ChatSession(), log_.get()));
        }

        virtual void DoHandleCommand(int c)
        {
            if(c == MOVE_RIGHT_FIX_DISTANCE)
            {
                replication_manager_->MoveRightFixedDistance();
            }
            else if (c == MOVE_LEFT_FIX_DISTANCE)
            {
                replication_manager_->MoveLeftFixedDistance();
            }
            else
            {
                ConsoleApp::DoHandleCommand(c);
            }
        }

        virtual void DoShutdown()
        {
            replication_manager_->Shutdown();
        }

        virtual bool IsReady() const
        {
            return replication_manager_->Ready();
        }

        virtual void PrintTips() const
        {
            std::cout << "This is the MeetAndApartWalkerTestApp." << std::endl;
        }

        std::auto_ptr<ReplicationManager> replication_manager_;
        DISALLOW_COPY_AND_ASSIGN(MeetAndApartWalkerTestApp);
    };
}

#endif // BADUMNA_AUTO_TEST_MEET_AND_APART_WALKER_TEST_APP_H