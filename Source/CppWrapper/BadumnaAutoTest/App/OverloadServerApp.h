//---------------------------------------------------------------------------------
// <copyright file="OverloadServerApp.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_OVERLOAD_SERVER_APP_H
#define BADUMNA_AUTO_TEST_OVERLOAD_SERVER_APP_H

#include <cstring>
#include <string>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <vector>
#include <boost/asio.hpp>
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/Utils/BasicTypes.h"

#include "App/ConsoleApp.h"

namespace 
{
    bool str2int (int &i, char const *s, int base = 10)
    {
        char *end;
        long  l;
        errno = 0;
        l = strtol(s, &end, base);
        if ((errno == ERANGE && l == LONG_MAX) || l > INT_MAX) {
            return false;
        }
        if ((errno == ERANGE && l == LONG_MIN) || l < INT_MIN) {
            return false;
        }
        if (*s == '\0' || *end != '\0') {
            return false;
        }
        i = l;
        return true;
    }
}

namespace BadumnaTestApp
{
    class OverloadServerApp : public ConsoleApp
    {
    public:
        OverloadServerApp(
            ProgramConfig config, 
            Badumna::NetworkFacade *facade, 
            boost::asio::io_service &io, 
            IConsole *console, 
            EventLog *log)
            : ConsoleApp(config, facade, io, console, log),
            last_report_time_(0)
        {
        }

    protected:
        virtual void OnTimerHandler(int time)
        {
            if(time - last_report_time_ > 5000)
            {
                std::wcout << facade_->GetNetworkStatus() << std::endl;
                RecordOverloadClient();
                last_report_time_ = time;
            }
        }

        virtual void DoInitialize()
        {
            facade_->AnnounceService(Badumna::ServerType_OverloadServer);
        }

        virtual bool IsReady() const
        {
            return true;
        }

        virtual void PrintTips() const
        {
            std::cout << "This is the OverloadServerApp." << std::endl;
        }

    private:
        void RecordOverloadClient()
        {
            std::stringstream ss(facade_->GetNetworkStatus().ToString().UTF8CStr());

            // parse the network status string
            std::string cur_line;
            while(getline(ss, cur_line))
            {
                if(cur_line.length() > 0)
                {
                    std::vector<std::string> tokens;
                    std::istringstream iss(cur_line);
                    std::copy(
                        std::istream_iterator<std::string>(iss),
                        std::istream_iterator<std::string>(),
                        std::back_inserter<std::vector<std::string> >(tokens));
                    
                    if(tokens.size() == 3)
                    {
                        if(tokens[0] == "Overload" && tokens[1] == "clients:")
                        {
                            int num = 0;
                            if(::str2int(num, tokens[2].c_str()))
                            {
                                log_->SetOverloadClients(num);
                            }
                        }
                    }
                }
            }
        }

        int last_report_time_;
        DISALLOW_COPY_AND_ASSIGN(OverloadServerApp);
    };
}

#endif // BADUMNA_AUTO_TEST_OVERLOAD_SERVER_APP_H