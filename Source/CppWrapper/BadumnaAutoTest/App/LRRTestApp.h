//---------------------------------------------------------------------------------
// <copyright file="LRRTestApp.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_LRR_TEST_APP_H
#define BADUMNA_AUTO_TEST_LRR_TEST_APP_H

#include <boost/asio.hpp>
#include "App/ConsoleApp.h"
#include "Core/Barrier.h"
#include "Badumna/Utils/BasicTypes.h"

namespace BadumnaTestApp
{
    class LRRTestApp : public ReplicationTestApp
    {
    public:
        LRRTestApp(
            ProgramConfig config, 
            Badumna::NetworkFacade *facade, 
            boost::asio::io_service &io, 
            IConsole *console, 
            EventLog *log)
            : ReplicationTestApp(config, facade, io, console, log)
        {
        }

    protected:
        virtual void DoHandleCommand(int c)
        {
            if(c == MOVE_LEFT_COMMAND || c == MOVE_RIGHT_COMMAND || 
               c == MOVE_UP_COMMAND || c == MOVE_DOWN_COMMAND)
            {
                replication_manager_->MoveLocalObject(c);
            }
            if(c == SEND_EVENT_TO_REMOTE_COPIES)
            {
                replication_manager_->SendEventToRemoteCopies();
            }
            else if (c == SEND_EVENT_TO_ORIGINAL)
            {
                replication_manager_->SendEventToOriginal();
            }
            if(c == SEND_PROXIMITY_CHAT_COMMAND)
            {
                replication_manager_->SendProximityChatMessage();
            }
            else
            {
                ReplicationTestApp::DoHandleCommand(c);
            }
        }

        virtual int GetMaxRuntime() const
        {
            return 60 * 60 * 24 * 10 * 1000;
        }
        
        virtual void PrintTips() const
        {
            std::cout << "This is the LRRTestApp." << std::endl;
        }

        DISALLOW_COPY_AND_ASSIGN(LRRTestApp);
    };
}

#endif // BADUMNA_AUTO_TEST_LRR_TEST_APP_H