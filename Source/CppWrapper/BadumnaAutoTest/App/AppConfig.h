//---------------------------------------------------------------------------------
// <copyright file="AppConfig.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_APP_CONFIG_H
#define BADUMNA_AUTO_TEST_APP_CONFIG_H

#include "Badumna/DataTypes/String.h"

namespace BadumnaTestApp
{
    enum ProgramMode
    {
        // help mode
        HelpAndExit,
        // regular tests
        ReplicationTest,
        DeadReckoningReplicationTest,
        DeiReplicationTest,
        ProximityChatTest,
        CustomEventTest,
        ArbitrationClientTest,
        ArbitrationServerTest,
        PrivateChatTest,
        PCReLoginTest,
        PCMeetAndApartTest,
        ArbitrationServer,
        ChangeSceneSender,
        ChangeSceneReceiver,
        MultiOriginalTest,
        MultiPeersReplicationTest,
        MeetAndApartTest,
        MeetAndApartWalker,
        OverloadTest,
        OverloadServer,
        SinglePeerDCTest,
        StreamingSenderTest,
        StreamingReceiverTest,
        ServiceDiscoveryClientTest,
        ServiceDiscoveryServerTest,
        // long running tests
        LRRTest, // long running replication test
        // SimpleTests
        MinimumTest,
        SimpleLoginSequenceTest,
        SimpleCharacterManagementTest,
        SimpleCreateFacadeTest,
        SimpleAnnouncePermissionTest,
		SimpleMiniNetworkSceneTest
    };

    struct ProgramConfig
    {
        ProgramConfig()
            : mode_(BadumnaTestApp::MinimumTest), 
            friend_list_file_(), 
            app_name_salt_(),
            use_tunnel_(false),
            do_relogin_(false)
        {
        }

        bool IsSimpleTest() const
        {
            if(mode_ == MinimumTest || mode_ == SimpleLoginSequenceTest || mode_ == SimpleCreateFacadeTest ||
               mode_ == SimpleAnnouncePermissionTest || mode_ == SimpleCharacterManagementTest || mode_ == SimpleMiniNetworkSceneTest)
            {
                return true;
            }

            return false;
        }

        ProgramMode mode_;
        Badumna::String friend_list_file_;
        Badumna::String app_name_salt_;
        bool use_tunnel_;
        bool do_relogin_;
    };
}

#endif // BADUMNA_AUTO_TEST_APP_CONFIG_H