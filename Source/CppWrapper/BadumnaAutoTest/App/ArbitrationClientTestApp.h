//---------------------------------------------------------------------------------
// <copyright file="ArbitrationClientTestApp.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_ARBITRATION_CLIENT_TEST_APP_H
#define BADUMNA_AUTO_TEST_ARBITRATION_CLIENT_TEST_APP_H

#include <memory>
#include <cassert>
#include <boost/asio.hpp>
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/Utils/BasicTypes.h"

#include "App/ConsoleApp.h"
#include "Core/ArbitrationManager.h"

namespace BadumnaTestApp
{
    class ArbitrationClientTestApp : public ConsoleApp
    {
    public:
        ArbitrationClientTestApp(
            ProgramConfig config, 
            Badumna::NetworkFacade *facade,
            boost::asio::io_service &io,
            IConsole *console,
            EventLog *log)
            : ConsoleApp(config, facade, io, console, log, 2),
            arbitration_manager_(NULL),
            character_name_("arbitration client")
        {
        }

    protected:
        virtual void DoInitialize()
        {
            arbitration_manager_.reset(new ArbitrationManager(facade_.get(), false, log_.get()));
        }

        virtual void DoLogin()
        {
            LoginAs(character_name_);
            assert(facade_->GetCharacter()->Name().Find(character_name_) == 0 && "facade->GetCharacter() returned an incorrect result");
        }


        virtual void DoHandleCommand(int c)
        {
            if(c == SEND_ARBITRATION_EVENT_COMMAND)
            {
                arbitration_manager_->SendArbitrationEvent();
            }
            else
            {
                ConsoleApp::DoHandleCommand(c);
            }
        }

        virtual bool IsReady() const
        {
            return arbitration_manager_->Ready();
        }

        virtual void PrintTips() const
        {
            std::cout << "This is the ArbitrationClientTestApp." << std::endl;
        }

        std::auto_ptr<ArbitrationManager> arbitration_manager_;
        Badumna::String character_name_;

        DISALLOW_COPY_AND_ASSIGN(ArbitrationClientTestApp);
    };
}

#endif // BADUMNA_AUTO_TEST_ARBITRATION_CLIENT_TEST_APP_H