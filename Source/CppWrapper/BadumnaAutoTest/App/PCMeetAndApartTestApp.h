//---------------------------------------------------------------------------------
// <copyright file="PCMeetAndApartTestApp.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_PC_MEET_AND_APART_TEST_APP_H
#define BADUMNA_AUTO_TEST_PC_MEET_AND_APART_TEST_APP_H

#include <memory>
#include <boost/asio.hpp>
#include "App/PrivateChatTestApp.h"
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/Utils/BasicTypes.h"

#include "Core/ChatManager.h"

namespace BadumnaTestApp
{
    class PCMeetAndApartTestApp : public PrivateChatTestApp
    {
    public:
        PCMeetAndApartTestApp(
            ProgramConfig config, 
            Badumna::NetworkFacade *facade, 
            boost::asio::io_service &io, 
            IConsole *console, 
            EventLog *log)
            : PrivateChatTestApp(config, facade, io, console, log)
        {
        }

    protected:
        virtual void DoInitialize()
        {
            PrivateChatTestApp::DoInitialize();
            chat_manager_->UnsubscribeFromPrivateChat();
        }

        virtual void PrintTips() const
        {
            std::cout << "This is the PCMeetAndApartTestApp." << std::endl;
        }

        DISALLOW_COPY_AND_ASSIGN(PCMeetAndApartTestApp);
    };
}

#endif // BADUMNA_AUTO_TEST_PC_MEET_AND_APART_TEST_APP_H