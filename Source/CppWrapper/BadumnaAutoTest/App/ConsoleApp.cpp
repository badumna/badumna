//---------------------------------------------------------------------------------
// <copyright file="ConsoleApp.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <stdexcept>
#include <cstdlib>
#include <signal.h>
#include <csignal>

#include <boost/bind.hpp>
#include <boost/date_time/local_time/local_time.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "Badumna/Core/ConnectivityStatusDelegate.h"
#include "Badumna/Core/NetworkStatus.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Replication/SpatialReplicaDelegate.h"
#include "Badumna/Arbitration/IArbitrator.h"
#include "Badumna/Dei/LoginResult.h"

#include "App/ConsoleApp.h"

namespace
{
    void OnlineEventHandler()
    {
        std::cout << "online event......" << std::endl;
    }

    void OfflineEventHandler()
    {
        std::cout << "offline event......" << std::endl;
    }

    void AddressChangedEventHandler()
    {
        std::cout << "address changed...." << std::endl;
    }
}

namespace BadumnaTestApp
{
    using std::runtime_error;
    using Dei::IdentityProvider;
    using Dei::LoginResult;

    using Badumna::String;
    using Badumna::NetworkFacade;

    ConsoleApp::ConsoleApp(
        ProgramConfig config, 
        NetworkFacade *facade, 
        boost::asio::io_service &io, 
        IConsole *console, 
        EventLog *event_log, int nop) 
        : barrier_(nop, config.app_name_salt_ + "_barrier_name"), 
        config_(config), 
        time_to_exit_(false),
        facade_(facade), 
        log_(event_log),
        console_(console),
        dtimer_(io), 
        timer_counter_(0),
        is_fully_connected_(false),
        reported_end_of_commands_(false),
        tracker_(NULL)
    {
    }

    ConsoleApp::~ConsoleApp()
    {
        Shutdown();
    }

    bool ConsoleApp::Run()
    {
        Badumna::ConnectivityStatusDelegate d1(::OnlineEventHandler);
        Badumna::ConnectivityStatusDelegate d2(::OfflineEventHandler);
        Badumna::ConnectivityStatusDelegate d3(::AddressChangedEventHandler);

        facade_->SetOnlineEventDelegate(d1);
        facade_->SetOfflineEventDelegate(d2);
        facade_->SetAddressChangedEventDelegate(d3);

        Authenticate();
        Login();

        std::cout << "login returned" << std::endl;

        if (!config_.use_tunnel_)  // Tracker is not supported in tunnel mode.
        {
            String payload = L"testing_payload";
            tracker_.reset(facade_->CreateTracker("127.0.0.1", 32325, 2, payload));
            tracker_->SetUserId(12345);
            tracker_->Start();
        }

        if (!config_.use_tunnel_) // GetNetworkStatus is not supported in tunnel mode yet.
        {
            // check and print out the network status
            std::wostringstream status_ostream;
            status_ostream << facade_->GetNetworkStatus() << std::endl;
        }

        // initialize the app, e.g. setup the replication manager, 
        // arbitration manager when required
        std::cout << "Initialize() start" << std::endl;
        Initialize();
        std::cout << "Initialize() done" << std::endl;

        std::ostringstream msg;
        const boost::posix_time::ptime now = boost::posix_time::second_clock::local_time();
        boost::posix_time::time_facet*const f = new boost::posix_time::time_facet("%H:%M:%S");
        msg.imbue(std::locale(msg.getloc(),f));
        msg << now;

        std::cout << "current time : " << msg.str() << std::endl;

        // print out some tips.
        PrintTips();

        // setup the async timer and keep executing the event queue. 
        dtimer_.expires_from_now(boost::posix_time::milliseconds(TIMER_INTERVAL_MS));
        dtimer_.async_wait(boost::bind(&ConsoleApp::TimerHandler, this, boost::asio::placeholders::error));
        dtimer_.get_io_service().run();

        // shutdown the app, need to shutdown the app components, 
        // e.g. replication manager shutdown. 
        Shutdown();

        if (tracker_.get() != NULL)
        {
            tracker_->Stop();
        }

        return true;
    }

    ///////////////////////////////////////////////////////////////////////////
    // login
    ///////////////////////////////////////////////////////////////////////////
    void ConsoleApp::Login()
    {
        DoLogin();
    }

    void ConsoleApp::DoLogin()
    {
        LoginAs("defaultCharacter");
    }

    void ConsoleApp::LoginAs(String const &characterName)
    {
        std::cout << "Logging in as character: " << characterName.UTF8CStr() << std::endl;
        if(!facade_->Login(characterName))
        {
            throw std::runtime_error("Login to badumna facade failed.");
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // authentication
    ///////////////////////////////////////////////////////////////////////////
    void ConsoleApp::Authenticate()
    {
        DoAuthentication();
    }

    void ConsoleApp::DoAuthentication()
    {
    }

    ///////////////////////////////////////////////////////////////////////////
    // Initialize
    ///////////////////////////////////////////////////////////////////////////
    void ConsoleApp::Initialize()
    {
        DoInitialize();
    }

    void ConsoleApp::DoInitialize()
    {
    }

    ///////////////////////////////////////////////////////////////////////////
    // handle command
    ///////////////////////////////////////////////////////////////////////////
    void ConsoleApp::HandleCommand(int c)
    {
        if(c == NO_MORE_COMMAND)
        {
            if(!reported_end_of_commands_)
            {
                reported_end_of_commands_ = true;
                std::cout << "end of commands (awaiting exit)." << std::endl;
            }
        }
        else
        {
            std::cout << "handling command: " << c << std::endl;
        }
        DoHandleCommand(c);
    }

    void ConsoleApp::DoHandleCommand(int c)
    {
        switch(c)
        {
            case EXIT_COMMAND:
                time_to_exit_ = true;
                break;
            case NO_MORE_COMMAND:
                break;
            default:
                std::cout << "WARNING: unknown command: " << c << std::endl;
                break;
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // shutdown
    ///////////////////////////////////////////////////////////////////////////
    void ConsoleApp::Shutdown()
    {
        DoShutdown();
    }

    void ConsoleApp::DoShutdown()
    {
        facade_->Shutdown();
    }

    ///////////////////////////////////////////////////////////////////////////
    // verify
    ///////////////////////////////////////////////////////////////////////////
    bool ConsoleApp::Verify()
    {
        return log_->Check();
    }

    ///////////////////////////////////////////////////////////////////////////
    // timer handler
    ///////////////////////////////////////////////////////////////////////////
    void ConsoleApp::TimerHandler(boost::system::error_code const &/*error_code*/)
    {
        timer_counter_++;
        int current_time = timer_counter_ * TIMER_INTERVAL_MS;

        if(current_time > GetMaxRuntime())
        {
            std::cerr << "runtime is longer than " << MAX_RUNTIME << " seconds. stopping the current app." << std::endl;
            throw std::runtime_error("runtime is longer than the max allowed.");
        }

        // called about 60 times per second 
        facade_->ProcessNetworkState();

        // check whether we are considered as ready to accept inputs from the console
        if(!is_fully_connected_)
        {
            if(IsFullyConnected())
            {
                is_fully_connected_ = true;
            }
        }

        if(IsReady() && is_fully_connected_)
        {
            barrier_.SetAsReady();  
        }

        if(BarrierReady())
        {
            SetConsoleAsReady(current_time);
        }

        // excute some code regularly.
        OnTimerHandler(current_time);

        int c = console_->GetCommand(current_time);
        HandleCommand(c);

        if(!time_to_exit_)
        {
            dtimer_.expires_from_now(boost::posix_time::milliseconds(16));
            dtimer_.async_wait(boost::bind(&ConsoleApp::TimerHandler, this, _1));
        }
    }

    void ConsoleApp::OnTimerHandler(int /*time*/)
    {
        GetNetworkFacadeStats();
    }

    void ConsoleApp::GetNetworkFacadeStats()
    {
        // connectivity status. 
        bool tunnelled = facade_->IsTunnelled();
        bool logged_in = facade_->IsLoggedIn();
        bool is_fully_connected = facade_->IsFullyConnected();
        bool is_offline = facade_->IsOffline();
        bool is_online = facade_->IsOnline();

        // to avoid unused variable warnings. 
        if(false)
        {
            std::cout << tunnelled << logged_in << is_fully_connected << is_offline << is_online << std::endl;
        }

        // traffic stats and limits
        double out = facade_->GetOutboundBytesPerSecond();
        double in = facade_->GetInboundBytesPerSecond();
        double mlost = facade_->GetMaximumPacketLossRate();
        double alost = facade_->GetAveragePacketLossRate();
        double sl = facade_->GetTotalSendLimitBytesPerSecond();
        double msl = facade_->GetMaximumSendLimitBytesPerSecond();

        if(false)
        {
            std::cout << out << in << mlost << alost << sl << msl << std::endl;
        }

        Badumna::NetworkStatus status = facade_->GetNetworkStatus();
        int32_t active_connection_count = status.GetActiveConnectionCount();
        int32_t initializing_connection_count = status.GetInitializingConnectionCount();
        int32_t local_object_count = status.GetLocalObjectCount();
        int32_t replica_object_count = status.GetRemoteObjectCount();

        if(false)
        {
            std::cout << active_connection_count << initializing_connection_count << local_object_count << 
                replica_object_count << std::endl;
        }

        bool port_forwarding_enabled = status.GetPortForwardingEnabled();
        bool port_forwarding_succeed = status.GetPortForwardingSucceeded();

        if(false)
        {
            std::cout << port_forwarding_enabled << port_forwarding_succeed << std::endl;
        }

        String discovery_method = status.GetDiscoveryMethod();
        String public_address = status.GetPublicAddress();
        String private_address = status.GetPrivateAddress();

        if(false)
        {
            std::wcout << discovery_method.CStr() << public_address.CStr() << private_address.CStr() << std::endl;
        }

        int64_t total_received = status.GetTotalBytesReceivedPerSecond();
        int64_t total_sent = status.GetTotalBytesSentPerSecond();
        String network_status_string = status.ToString();

        if(false)
        {
            std::wcout << total_received << total_sent << network_status_string.CStr() << std::endl;
        }
    }
    
    ///////////////////////////////////////////////////////////////////////////
    // print tips
    ///////////////////////////////////////////////////////////////////////////
    void ConsoleApp::PrintTips() const
    {
        std::cout << "Running in auto mode. Press Ctrl+C to terminate the program." << std::endl;
    }

    void ConsoleApp::SetConsoleAsReady(int time)
    {
        assert(is_fully_connected_);
        console_->SetAsReady(time);
    }
}