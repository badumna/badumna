//---------------------------------------------------------------------------------
// <copyright file="StreamingReceiverTestApp.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_STREAMING_RECEIVER_TEST_APP_H
#define BADUMNA_AUTO_TEST_STREAMING_RECEIVER_TEST_APP_H

#include <memory>
#include <boost/asio.hpp>

#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/Utils/BasicTypes.h"
#include "App/ConsoleApp.h"
#include "Core/Barrier.h"
#include "Core/ReplicationManager.h"
#include "Core/StreamingTestManager.h"

namespace BadumnaTestApp
{
    class StreamingReceiverTestApp : public ConsoleApp
    {
    public:
        StreamingReceiverTestApp(
            ProgramConfig config, 
            Badumna::NetworkFacade *facade, 
            boost::asio::io_service &io, 
            IConsole *console, 
            EventLog *log)
            : ConsoleApp(config, facade, io, console, log),
            replication_manager_(NULL),
            streaming_manager_(NULL)
        {
        }

    protected:
        virtual void DoInitialize()
        {
            replication_manager_.reset(
                new ReplicationManager(facade_.get(), facade_->ChatSession(), log_.get()));
            streaming_manager_.reset(
                new StreamingTestManager(facade_->GetStreamingManager(), log_.get(), false));
        }

        virtual void DoHandleCommand(int c)
        {
            ConsoleApp::DoHandleCommand(c);
        }

        virtual void DoShutdown()
        {
            replication_manager_->Shutdown();
        }

        virtual bool IsReady() const
        {
            return replication_manager_->Ready();
        }

        virtual void PrintTips() const
        {
            std::cout << "This is the StreamingReceiverTestApp." << std::endl;
        }

        std::auto_ptr<ReplicationManager> replication_manager_;
        std::auto_ptr<StreamingTestManager> streaming_manager_;

        DISALLOW_COPY_AND_ASSIGN(StreamingReceiverTestApp);
    };
}

#endif // BADUMNA_AUTO_TEST_STREAMING_RECEIVER_TEST_APP_H