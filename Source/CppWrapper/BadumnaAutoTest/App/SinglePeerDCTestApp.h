//---------------------------------------------------------------------------------
// <copyright file="SinglePeerDCTestApp.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_SINGLE_PEER_DC_TEST_APP_H
#define BADUMNA_AUTO_TEST_SINGLE_PEER_DC_TEST_APP_H

#include <memory>
#include <boost/asio.hpp>

#include "App/ConsoleApp.h"
#include "Core/SinglePeerDCTestNPC.h"
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/Utils/BasicTypes.h"

namespace BadumnaTestApp
{
    Badumna::DistributedSceneController *CreateDistributedNPC(
        Badumna::NetworkFacade *facade, 
        Badumna::String const &name)
    {
        Badumna::DistributedSceneController *controller = new SinglePeerDCTestNPC(facade, name);
        return controller;
    }

    class SinglePeerDCTestApp : public ConsoleApp
    {
    public:
        SinglePeerDCTestApp(
            ProgramConfig config, 
            Badumna::NetworkFacade *facade, 
            boost::asio::io_service &io, 
            IConsole *console, 
            EventLog *log)
            : ConsoleApp(config, facade, io, console, log),
            unique_dc_name_(),
            replication_manager_(NULL)
        {
        }

    protected:
        virtual void DoInitialize()
        {
            replication_manager_.reset(
                new ReplicationManager(facade_.get(), facade_->ChatSession(), log_.get()));

            unique_dc_name_ = facade_->StartController(L"test_app_scene_1", L"spdctestnpc", 16);
        }

        virtual void DoLogin()
        {
            Badumna::CreateControllerDelegate c(CreateDistributedNPC, facade_.get());
            facade_->RegisterController(L"spdctestnpc", c);
            
            ConsoleApp::DoLogin();
        }

        virtual void DoShutdown()
        {
            replication_manager_->Shutdown();
            facade_->StopController(unique_dc_name_);
        }

        virtual bool IsReady() const
        {
            return true;
        }

        virtual bool IsFullyConnected()
        {
            return true;
        }

        virtual bool BarrierReady()
        {
            return true;
        }

        virtual void PrintTips() const
        {
            std::cout << "This is the SinglePeerDCTestApp." << std::endl;
        }

        Badumna::String unique_dc_name_;
        std::auto_ptr<ReplicationManager> replication_manager_;

        DISALLOW_COPY_AND_ASSIGN(SinglePeerDCTestApp);
    };
}

#endif // BADUMNA_AUTO_TEST_SINGLE_PEER_DC_TEST_APP_H