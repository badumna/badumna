//---------------------------------------------------------------------------------
// <copyright file="DeiTestApp.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_DEI_TEST_APP_H
#define BADUMNA_AUTO_TEST_DEI_TEST_APP_H

#include <memory>
#include <boost/asio.hpp>
#include "App/ConsoleApp.h"
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/Dei/IdentityProvider.h"
#include "Badumna/Dei/Session.h"
#include "Badumna/Dei/LoginResult.h"
#include "Badumna/Utils/BasicTypes.h"

#include "Core/ReplicationManager.h"

namespace BadumnaTestApp
{
    using Badumna::Character;

    class DeiTestApp : public ConsoleApp
    {
    public:
        DeiTestApp(
            ProgramConfig config, 
            Badumna::NetworkFacade *facade, 
            boost::asio::io_service &io, 
            IConsole *console, 
            EventLog *log)
            : ConsoleApp(config, facade, io, console, log),
            replication_manager_(NULL),
            identity_provider_()
        {
        }

    protected:
        virtual void DoAuthentication()
        {
            Dei::Session session("127.0.0.1", 21248);
            session.SetUseSslConnection(false);
            Dei::LoginResult r = session.Authenticate("user", "user");
            if(r.WasSuccessful())
            {
                if(!r.Characters().empty())
                {
                    r = session.SelectIdentity(r.Characters()[0], identity_provider_);
                    if(r.WasSuccessful())
                    {
                        selected_character_.reset(new Character(r.Characters()[0]));
                        return;
                    } else std::cerr << "SelectIdentity() failed" << std::endl;
                } else std::cerr << "Empty set of characters returned" << std::endl;
            } else std::cerr << "unsuccessful login attempt: " << r.GetErrorDescription().UTF8CStr() << std::endl;


            std::cerr << "Failed to login to dei server." << std::endl;
            throw std::runtime_error("failed to login to dei server.");
        }

        virtual void DoLogin()
        {
            assert(facade_->GetCharacter().get() == NULL && "GetCharacter should return NULL before login");

            if(!facade_->Login(identity_provider_))
            {
                std::cerr << "Failed to login to badumna network." << std::endl;
                throw std::runtime_error("failed to login to badumna network.");
            }

            std::auto_ptr<Character> loggedInCharacter = facade_->GetCharacter();
            assert(loggedInCharacter.get() != NULL && "loggedInCharacter should not be NULL");
            std::cout << "successfully logged in as character: " << loggedInCharacter->ToString().UTF8CStr() << std::endl;
            assert(*loggedInCharacter == *selected_character_ && "GetCharacter does not match the character we selected!");
        }

        virtual void DoInitialize()
        {
            replication_manager_.reset(new ReplicationManager(facade_.get(), facade_->ChatSession(), log_.get()));
        }

        virtual void DoHandleCommand(int c)
        {
            if(c == INCREASE_COUNTER_COMMAND)
            {
                replication_manager_->IncreaseCounter();
            }

            ConsoleApp::DoHandleCommand(c);
        }

        virtual void DoShutdown()
        {
            replication_manager_->Shutdown();
        }

        virtual bool IsReady() const
        {
            return replication_manager_->Ready();
        }

        virtual void PrintTips() const
        {
            std::cout << "This is the DeiTestApp." << std::endl;
        }

        std::auto_ptr<ReplicationManager> replication_manager_;
        Dei::IdentityProvider identity_provider_;
        std::auto_ptr<Character> selected_character_;

        DISALLOW_COPY_AND_ASSIGN(DeiTestApp);
    };
}

#endif // BADUMNA_AUTO_TEST_DEI_TEST_APP_H
