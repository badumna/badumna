//---------------------------------------------------------------------------------
// <copyright file="ConsoleApp.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_CONSOLE_APP_H
#define BADUMNA_AUTO_TEST_CONSOLE_APP_H

#include <memory>
#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>

#include "Badumna/Core/NetworkFacade.h"
#include "App/AppConfig.h"
#include "Core/Barrier.h"
#include "Console/IConsole.h"
#include "EventLogger/EventLog.h"

extern "C" void SignalHandler(int sig);

namespace BadumnaTestApp
{
    /**
     * ConsoleApp is the base class for all App classes. A typical App class handles the input console command and 
     * implements the app logic, e.g. in PrivateChatTestApp, a private chat message will be sent on receving a 
     * SEND_PRIVATE_CHAT_COMMAND command from the console object in a private chat test app.  
     */
    class ConsoleApp
    {
    public:
        ConsoleApp(
            ProgramConfig config, 
            Badumna::NetworkFacade *facade, 
            boost::asio::io_service &io, 
            IConsole *console, 
            EventLog *log,
            int nop = 2);

        virtual ~ConsoleApp();

        /**
         * Runs this app.
         */
        bool Run();
        
        /**
         * Verifies the result.
         */
        bool Verify();

    protected:
        static const int TIMER_INTERVAL_MS = 16;
        static const int MAX_RUNTIME = 360000;

        /**
         * Print a human readable description about this test.
         */
        virtual void PrintTips() const;
        
        /**
         * The method get called on every timer event. This method called GetNetworkStatus by default. 
         */
        virtual void OnTimerHandler(int time);

        /**
         * Perform authentication.
         */
        virtual void DoAuthentication();
        
        /**
         * Perform login.
         */
        virtual void DoLogin();

        /**
         * Login as the given character.
         */
        virtual void LoginAs(Badumna::String const &name);

        /**
         * Called during initialization.
         */
        virtual void DoInitialize();
        
        /**
         * Called when handling a console command. 
         */
        virtual void DoHandleCommand(int command);

        /**
         * Called when the app is being shutdown. 
         */
        virtual void DoShutdown();

        /**
         * Whether the local process is fully connected and synchronized with other peers. 
         */
        virtual bool IsReady() const = 0;

        /**
         * Whether the local process is fully connected.
         */
        virtual bool IsFullyConnected()
        {
            return facade_->IsFullyConnected();
        }

        /**
         * Whether the local process is synchronized with other peers.
         */
        virtual bool BarrierReady()
        {
            return barrier_.Ready();
        }

        /**
         * Gets the maximum allowed run time.
         */
        virtual int GetMaxRuntime() const
        {
            return MAX_RUNTIME;
        }

        /**
         * Sets as ready.
         */
        void SetConsoleAsReady(int time);
        
        Barrier barrier_;
        ProgramConfig config_;
        volatile bool time_to_exit_;
        bool reported_end_of_commands_;

        boost::shared_ptr<Badumna::NetworkFacade> facade_;
        std::auto_ptr<EventLog> log_;
        std::auto_ptr<IConsole> console_;
        boost::asio::deadline_timer dtimer_;
        
        int timer_counter_;
        bool is_fully_connected_;
        std::auto_ptr<Badumna::StatisticsTracker> tracker_;

    protected:
        void Authenticate();
        void Login();
        void Initialize();

        void HandleCommand(int command);

        void Shutdown();
        void TimerHandler(boost::system::error_code const &error_code);
        
        void GetNetworkFacadeStats();

        DISALLOW_COPY_AND_ASSIGN(ConsoleApp);
    };
}

#endif // BADUMNA_EXAMPLE_CONSOLE_APP_H
