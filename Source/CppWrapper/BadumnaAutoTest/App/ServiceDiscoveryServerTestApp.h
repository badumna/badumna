//---------------------------------------------------------------------------------
// <copyright file="ServiceDiscoveryServerTestApp.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_SERVICE_DISCOVERY_SERVER_TEST_APP_H
#define BADUMNA_AUTO_TEST_SERVICE_DISCOVERY_SERVER_TEST_APP_H

#include <memory>
#include <boost/asio.hpp>
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/Utils/BasicTypes.h"

#include "App/ConsoleApp.h"
#include "Core/ArbitrationManager.h"

namespace BadumnaTestApp
{
    class ServiceDiscoveryServerTestApp : public ConsoleApp
    {
    public:
        ServiceDiscoveryServerTestApp(
            ProgramConfig config, 
            Badumna::NetworkFacade *facade, 
            boost::asio::io_service &io, 
            IConsole *console, 
            EventLog *log)
            : ConsoleApp(config, facade, io, console, log),
            arbitration_manager_(NULL),
            identity_provider_()
        {
        }

    protected:
        virtual void DoAuthentication()
        {
            Dei::Session session("127.0.0.1", 21248);
            session.SetUseSslConnection(false);

            // the account is created in a sqlite db found the in the top level of the auto test project directory. 
            Dei::LoginResult r = session.Authenticate("arbitrationserver", "arbitrationserver");
            if(r.WasSuccessful())
            {
                r = session.SelectIdentity(Badumna::Character::None, identity_provider_);
            }

            if(!r.WasSuccessful())
            {
                std::cerr << "Failed to login to dei server." << std::endl;
                throw std::runtime_error("failed to login to dei server.");
            }
        }

        virtual void DoLogin()
        {
            if(!facade_->Login(identity_provider_))
            {
                std::cerr << "Failed to login to badumna network." << std::endl;
                throw std::runtime_error("failed to login to badumna network.");
            }
        }

        virtual void DoInitialize()
        {
            arbitration_manager_.reset(new ArbitrationManager(facade_.get(), true, log_.get()));
        }

        virtual bool IsReady() const
        {
            return true;
        }

        virtual void PrintTips() const
        {
            std::cout << "This is the ServiceDiscoveryServerTestApp." << std::endl;
        }

        std::auto_ptr<ArbitrationManager> arbitration_manager_;
        Dei::IdentityProvider identity_provider_;
        DISALLOW_COPY_AND_ASSIGN(ServiceDiscoveryServerTestApp);
    };
}

#endif // BADUMNA_AUTO_TEST_SERVICE_DISCOVERY_SERVER_TEST_APP_H
