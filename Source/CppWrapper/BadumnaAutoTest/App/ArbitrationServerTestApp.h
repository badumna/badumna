//---------------------------------------------------------------------------------
// <copyright file="ArbitrationServerTestApp.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_ARBITRATION_SERVER_TEST_APP_H
#define BADUMNA_AUTO_TEST_ARBITRATION_SERVER_TEST_APP_H

#include <memory>
#include <boost/asio.hpp>
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/Utils/BasicTypes.h"

#include "App/ConsoleApp.h"
#include "Core/ArbitrationManager.h"

namespace BadumnaTestApp
{
    class ArbitrationServerTestApp : public ConsoleApp
    {
    public:
        ArbitrationServerTestApp(
            ProgramConfig config, 
            Badumna::NetworkFacade *facade, 
            boost::asio::io_service &io, 
            IConsole *console, 
            EventLog *log)
            : ConsoleApp(config, facade, io, console, log),
            arbitration_manager_(NULL)
        {
        }

    protected:
        virtual void DoInitialize()
        {
            arbitration_manager_.reset(new ArbitrationManager(facade_.get(), true, log_.get()));
        }

        virtual bool IsReady() const
        {
            return true;
        }

        virtual void PrintTips() const
        {
            std::cout << "This is the ArbitrationServerTestApp." << std::endl;
        }

        std::auto_ptr<ArbitrationManager> arbitration_manager_;
        DISALLOW_COPY_AND_ASSIGN(ArbitrationServerTestApp);
    };
}

#endif // BADUMNA_AUTO_TEST_ARBITRATION_SERVER_TEST_APP_H