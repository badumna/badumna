//---------------------------------------------------------------------------------
// <copyright file="PrivateChatTestApp.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_PRIVATE_CHAT_TEST_APP_H
#define BADUMNA_AUTO_TEST_PRIVATE_CHAT_TEST_APP_H

#include <memory>
#include <boost/asio.hpp>
#include "App/ConsoleApp.h"
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/Utils/BasicTypes.h"

#include "Core/ChatManager.h"

namespace BadumnaTestApp
{
    class PrivateChatTestApp : public ConsoleApp
    {
    public:
        PrivateChatTestApp(
            ProgramConfig config,
            Badumna::Options *options,
            Badumna::NetworkFacade *facade, 
            boost::asio::io_service &io, 
            IConsole *console, 
            EventLog *log)
            : ConsoleApp(config, facade, io, console, log),
            chat_manager_(NULL),
            options_(options)
        {
        }

        PrivateChatTestApp(
            ProgramConfig config,
            Badumna::NetworkFacade *facade, 
            boost::asio::io_service &io, 
            IConsole *console, 
            EventLog *log)
            : ConsoleApp(config, facade, io, console, log),
            chat_manager_(NULL),
            options_(NULL)
        {
        }

        virtual ~PrivateChatTestApp()
        {
        }

    protected:
        virtual void DoLogin()
        {
            chat_manager_.reset(new ChatManager(facade_, log_.get()));
            Badumna::String my_name(chat_manager_->Initialize(config_.friend_list_file_));
            LoginAs(my_name);
        }

        virtual void DoInitialize()
        {
            std::cout << "Inviting friends to chat" << std::endl;
            chat_manager_->InviteFriends();
        }

        virtual void DoHandleCommand(int c)
        {
            if(c == SEND_PRIVATE_CHAT_COMMAND)
            {
                chat_manager_->SendChatMessage();
            }
            else if(c == CHANGE_PRESENCE_COMMAND)
            {
                Badumna::ChatStatus s = chat_manager_->GetNextChatStatus();
                chat_manager_->SetPresenceStatus(s);
            }
            else if(c == RE_LOGIN)
            {
                std::cout << "Logging out ..." << std::endl;
                facade_->Shutdown();

                std::cout << "Logging back in ..." << std::endl;
                facade_.reset(Badumna::NetworkFacade::Create(*options_));
                Authenticate();
                Login();
                Initialize();
                std::cout << "And we're back!" << std::endl;
            }
            else
            {
                ConsoleApp::DoHandleCommand(c);
            }
        }

        virtual bool IsReady() const
        {
            return chat_manager_->Ready();
        }

        virtual void PrintTips() const
        {
            std::cout << "This is the PrivateChatTestApp." << std::endl;
        }

        std::auto_ptr<ChatManager> chat_manager_;
        Badumna::Options *options_;

        DISALLOW_COPY_AND_ASSIGN(PrivateChatTestApp);
    };
}

#endif // BADUMNA_AUTO_TEST_PRIVATE_CHAT_TEST_APP_H