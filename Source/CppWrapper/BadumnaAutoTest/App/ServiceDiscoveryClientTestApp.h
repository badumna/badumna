//---------------------------------------------------------------------------------
// <copyright file="ServiceDiscoveryClientTestApp.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_SERVICE_DISCOVERY_CLIENT_TEST_APP_H
#define BADUMNA_AUTO_TEST_SERVICE_DISCOVERY_CLIENT_TEST_APP_H

#include <memory>
#include <boost/asio.hpp>
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/Dei/Session.h"
#include "Badumna/Dei/IdentityProvider.h"

#include "App/ConsoleApp.h"
#include "Core/ArbitrationManager.h"

namespace BadumnaTestApp
{
    class ServiceDiscoveryClientTestApp : public ConsoleApp
    {
    public:
        ServiceDiscoveryClientTestApp(
            ProgramConfig config, 
            Badumna::NetworkFacade *facade, 
            boost::asio::io_service &io, 
            IConsole *console, 
            EventLog *log)
            : ConsoleApp(config, facade, io, console, log, 2),
            arbitration_manager_(NULL),
            identity_provider_()
        {
        }

    protected:
        virtual void DoAuthentication()
        {
            Dei::Session session("127.0.0.1", 21248);
            session.SetUseSslConnection(false);

            Dei::LoginResult r = session.Authenticate("user", "user");
            if(r.WasSuccessful())
            {
                r = session.SelectIdentity(r.Characters()[0], identity_provider_);
            }

            if(!r.WasSuccessful())
            {
                std::cerr << "Failed to login to dei server." << std::endl;
                throw std::runtime_error("failed to login to dei server.");
            }
        }

        virtual void DoLogin()
        {
            if(!facade_->Login(identity_provider_))
            {
                std::cerr << "Failed to login to badumna network." << std::endl;
                throw std::runtime_error("failed to login to badumna network.");
            }
        }

        virtual void DoInitialize()
        {
            arbitration_manager_.reset(new ArbitrationManager(facade_.get(), false, log_.get()));
        }

        virtual void DoHandleCommand(int c)
        {
            if(c == SEND_ARBITRATION_EVENT_COMMAND)
            {
                arbitration_manager_->SendArbitrationEvent();
            }
            else
            {
                ConsoleApp::DoHandleCommand(c);
            }
        }

        virtual bool IsReady() const
        {
            return arbitration_manager_->Ready();
        }

        virtual void PrintTips() const
        {
            std::cout << "This is the ServiceDiscoveryClientTestApp." << std::endl;
        }

        std::auto_ptr<ArbitrationManager> arbitration_manager_;
        Dei::IdentityProvider identity_provider_;
        DISALLOW_COPY_AND_ASSIGN(ServiceDiscoveryClientTestApp);
    };
}

#endif // BADUMNA_AUTO_TEST_SERVICE_DISCOVERY_CLIENT_TEST_APP_H
