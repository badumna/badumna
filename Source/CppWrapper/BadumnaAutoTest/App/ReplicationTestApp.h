//---------------------------------------------------------------------------------
// <copyright file="ReplicationTestApp.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_REPLICATION_TEST_APP_H
#define BADUMNA_AUTO_TEST_REPLICATION_TEST_APP_H

#include <memory>
#include <iostream>
#include <boost/asio.hpp>

#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/Utils/BasicTypes.h"
#include "App/ConsoleApp.h"
#include "Core/Barrier.h"
#include "Core/ReplicationManager.h"

namespace BadumnaTestApp
{
    class ReplicationTestApp : public ConsoleApp
    {
    public:
        ReplicationTestApp(
            ProgramConfig config, 
            Badumna::NetworkFacade *facade, 
            boost::asio::io_service &io, 
            IConsole *console, 
            EventLog *log)
            : ConsoleApp(config, facade, io, console, log),
            replication_manager_(NULL)
        {
        }

        virtual ~ReplicationTestApp()
        {
        }

    protected:
        virtual void DoInitialize()
        {
            replication_manager_.reset(
                new ReplicationManager(facade_.get(), facade_->ChatSession(), log_.get()));
        }

        virtual void DoHandleCommand(int c)
        {
            if(c == INCREASE_COUNTER_COMMAND)
            {
                replication_manager_->IncreaseCounter();
            }
            else if (c == RANDOM_MOVE_COMMAND)
            {
                replication_manager_->RandomMove();
            }
            else if(c == CONSISTENCY_CHECK_COMMAND)
            {
                replication_manager_->ConsistencyCheck();
            }
            else if(c == MOVE_LEFT_COMMAND || c == MOVE_RIGHT_COMMAND || 
                    c == MOVE_UP_COMMAND || c == MOVE_DOWN_COMMAND)
            {
                replication_manager_->MoveLocalObject(c);
            }
            else
            {
                ConsoleApp::DoHandleCommand(c);
            }
        }

        virtual void DoShutdown()
        {
            replication_manager_->Shutdown();
        }

        virtual bool IsReady() const
        {
            return replication_manager_->Ready();
        }

        virtual void PrintTips() const
        {
            std::cout << "This is the ReplicationTestApp." << std::endl;
        }

        std::auto_ptr<ReplicationManager> replication_manager_;

        DISALLOW_COPY_AND_ASSIGN(ReplicationTestApp);
    };
}

#endif // BADUMNA_AUTO_TEST_REPLICATION_TEST_APP_H