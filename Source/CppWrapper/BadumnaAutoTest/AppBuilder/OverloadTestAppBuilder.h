//---------------------------------------------------------------------------------
// <copyright file="OverloadTestAppBuilder.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_OVERLOAD_TEST_APP_BUILDER_H
#define BADUMNA_AUTO_TEST_OVERLOAD_TEST_APP_BUILDER_H

#include "Badumna/Utils/BasicTypes.h"

#include "AppBuilder/AppBuilder.h"
#include "App/OverloadTestApp.h"
#include "EventLogger/OverloadTestEventLog.h"
#include "Console/OverloadTestAutoConsole.h"
#include "Option/OverloadConfigOptions.h"

namespace BadumnaTestApp
{
    class OverloadTestAppBuilder : public AppBuilder
    {
    public:
        OverloadTestAppBuilder()
            : AppBuilder()
        {
        }

    protected:
        void DoBuildOptions(ProgramConfig config)
        {
            options_.reset(new OverloadConfigOptions());
            options_->SetOptions(config);
        }

        void DoBuildConsole()
        {
            console_ = new OverloadTestAutoConsole();
            console_->Start();
        }

        void DoBuildEventLog()
        {
            event_log_ = new OverloadTestEventLog();
        }

        void DoBuildApp(ProgramConfig config, Badumna::NetworkFacade *facade, boost::asio::io_service &io)
        {
            console_app_ = new OverloadTestApp(config, facade, io, console_, event_log_);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(OverloadTestAppBuilder);
    };
}

#endif // BADUMNA_AUTO_TEST_OVERLOAD_TEST_APP_BUILDER_H