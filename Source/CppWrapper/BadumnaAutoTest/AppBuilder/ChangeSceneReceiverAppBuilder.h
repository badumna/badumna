//---------------------------------------------------------------------------------
// <copyright file="ChangeSceneReceiverAppBuilder.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_CHANGE_SCENE_RECEIVER_APP_BUILDER_H
#define BADUMNA_AUTO_TEST_CHANGE_SCENE_RECEIVER_APP_BUILDER_H

#include "Badumna/Utils/BasicTypes.h"

#include "App/ChangeSceneTestApp.h"
#include "AppBuilder/AppBuilder.h"
#include "EventLogger/ChangeSceneTestEventLog.h"
#include "Console/ChangeSceneReceiverAutoConsole.h"

namespace BadumnaTestApp
{
    class ChangeSceneReceiverAppBuilder : public AppBuilder
    {
    public:
        ChangeSceneReceiverAppBuilder()
            : AppBuilder()
        {
        }

    protected:
        void DoBuildConsole()
        {
            console_ = new ChangeSceneReceiverAutoConsole();
            console_->Start();
        }

        void DoBuildEventLog()
        {
            event_log_ = new ChangeSceneTestEventLog();
        }

        void DoBuildApp(ProgramConfig config, Badumna::NetworkFacade *facade, boost::asio::io_service &io)
        {
            console_app_ = new ChangeSceneTestApp(config, facade, io, console_, event_log_);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(ChangeSceneReceiverAppBuilder);
    };
}

#endif // BADUMNA_AUTO_TEST_CHANGE_SCENE_RECEIVER_APP_BUILDER_H