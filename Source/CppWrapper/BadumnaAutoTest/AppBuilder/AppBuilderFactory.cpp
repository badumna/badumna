//---------------------------------------------------------------------------------
// <copyright file="AppBuilderFactory.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include "AppBuilder/AppBuilderFactory.h"
#include "AppBuilder/ReplicationTestAppBuilder.h"
#include "AppBuilder/ProximityChatTestAppBuilder.h"
#include "AppBuilder/CustomEventTestAppBuilder.h"
#include "AppBuilder/PrivateChatTestAppBuilder.h"
#include "AppBuilder/PCReLoginTestAppBuilder.h"
#include "AppBuilder/ArbitrationClientTestAppBuilder.h"
#include "AppBuilder/ArbitrationServerTestAppBuilder.h"
#include "AppBuilder/DeiTestAppBuilder.h"
#include "AppBuilder/ChangeSceneSenderAppBuilder.h"
#include "AppBuilder/ChangeSceneReceiverAppBuilder.h"
#include "AppBuilder/MultiOriginalReplicationTestAppBuilder.h"
#include "AppBuilder/MultiPeersReplicationTestAppBuilder.h"
#include "AppBuilder/MeetAndApartTestAppBuilder.h"
#include "AppBuilder/MeetAndApartWalkerAppBuilder.h"
#include "AppBuilder/PCMeetAndApartTestAppBuilder.h"
#include "AppBuilder/OverloadTestAppBuilder.h"
#include "AppBuilder/OverloadServerAppBuilder.h"
#include "AppBuilder/SinglePeerDCTestAppBuilder.h"
#include "AppBuilder/LRRTestAppBuilder.h"
#include "AppBuilder/StreamingSenderTestAppBuilder.h"
#include "AppBuilder/StreamingReceiverTestAppBuilder.h"
#include "AppBuilder/ServiceDiscoveryClientTestAppBuilder.h"
#include "AppBuilder/ServiceDiscoveryServerTestAppBuilder.h"

namespace BadumnaTestApp
{
    AppBuilder* AppBuilderFactory::Create(ProgramMode mode)
    {
        if(mode == ReplicationTest)
        {
            return new ReplicationTestAppBuilder();
        }
        else if(mode == ProximityChatTest)
        {
            return new ProximityChatTestAppBuilder();
        }
        else if(mode == CustomEventTest)
        {
            return new CustomEventTestAppBuilder();
        }
        else if(mode == PrivateChatTest)
        {
            return new PrivateChatTestAppBuilder();
        }
        else if(mode == PCReLoginTest)
        {
            return new PCReLoginTestAppBuilder();
        }
        else if(mode == ArbitrationClientTest)
        {
            return new ArbitrationClientTestAppBuilder();
        }
        else if(mode == ArbitrationServerTest)
        {
            return new ArbitrationServerTestAppBuilder();
        }
        else if(mode == DeiReplicationTest)
        {
            return new DeiTestAppBuilder();
        }
        else if(mode == ChangeSceneSender)
        {
            return new ChangeSceneSenderAppBuilder();
        }
        else if(mode == ChangeSceneReceiver)
        {
            return new ChangeSceneReceiverAppBuilder();
        }
        else if(mode == MultiOriginalTest)
        {
            return new MultiOriginalReplicationTestAppBuilder();
        }
        else if(mode == MultiPeersReplicationTest)
        {
            return new MultiPeersReplicationTestAppBuilder();
        }
        else if(mode == MeetAndApartTest)
        {
            return new MeetAndApartTestAppBuilder();
        }
        else if(mode == MeetAndApartWalker)
        {
            return new MeetAndApartWalkerAppBuilder();
        }
        else if(mode == PCMeetAndApartTest)
        {
            return new PCMeetAndApartTestAppBuilder();
        }
        else if(mode == OverloadTest)
        {
            return new OverloadTestAppBuilder();
        }
        else if(mode == OverloadServer)
        {
            return new OverloadServerAppBuilder();
        }
        else if(mode == SinglePeerDCTest)
        {
            return new SinglePeerDCTestAppBuilder();
        }
        else if(mode == LRRTest)
        {
            return new LRRTestAppBuilder();
        }
        else if(mode == StreamingSenderTest)
        {
            return new StreamingSenderTestAppBuilder();
        }
        else if(mode == StreamingReceiverTest)
        {
            return new StreamingReceiverTestAppBuilder();
        }
        else if(mode == ServiceDiscoveryClientTest)
        {
            return new ServiceDiscoveryClientTestAppBuilder();
        }
        else if(mode == ServiceDiscoveryServerTest)
        {
            return new ServiceDiscoveryServerTestAppBuilder();
        }

        throw std::runtime_error("Unknown program mode.");
    }
}