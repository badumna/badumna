//---------------------------------------------------------------------------------
// <copyright file="AppBuilder.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_APP_BUILDER_H
#define BADUMNA_AUTO_TEST_APP_BUILDER_H

#include <memory>

#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/Configuration/Options.h"

#include "Option/PeerConfigOptions.h"
#include "EventLogger/EventLog.h"
#include "Console/IConsole.h"
#include "App/ConsoleApp.h"

namespace BadumnaTestApp
{
    /**
     * The base builder class. The builder class builds all components (console, event logger, app, options) and 
     * assemble them into a test app. 
     */
    class AppBuilder
    {
    public:
        AppBuilder()
            : options_(NULL),
            console_(NULL),
            event_log_(NULL),
            console_app_(NULL),
            program_config_(NULL)
        {
        }

        virtual ~AppBuilder()
        {
        }

        void BuildConsole()
        {
            DoBuildConsole();
        }

        void BuildEventLog()
        {
            DoBuildEventLog();
        }

        void BuildOptions(ProgramConfig config)
        {
            program_config_ = new ProgramConfig(config);
            DoBuildOptions(config);
        }
        
        void BuildApp(ProgramConfig config, boost::asio::io_service &io)
        {
            Badumna::Options *o = options_->GetOptions();
            DoBuildApp(config, Badumna::NetworkFacade::Create(*o), io);
        }

        ConsoleApp *GetApp() const
        {
            return console_app_;
        }

    protected:
        virtual void DoBuildConsole() = 0;
        virtual void DoBuildEventLog() = 0;
        virtual void DoBuildApp(ProgramConfig config, Badumna::NetworkFacade *facade, boost::asio::io_service &io) = 0;
        
        virtual void DoBuildOptions(ProgramConfig config)
        {
            options_.reset(new PeerConfigOptions());
            options_->SetOptions(config);
        }

        std::auto_ptr<PeerConfigOptions> options_;
        IConsole *console_;
        EventLog *event_log_;
        ConsoleApp* console_app_;
        ProgramConfig* program_config_;

        DISALLOW_COPY_AND_ASSIGN(AppBuilder);
    };
}

#endif // BADUMNA_AUTO_TEST_APP_BUILDER_H