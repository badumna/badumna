//---------------------------------------------------------------------------------
// <copyright file="DeiTestAppBuilder.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_DEI_TEST_APP_BUILDER_H
#define BADUMNA_AUTO_TEST_DEI_TEST_APP_BUILDER_H

#include "Badumna/Utils/BasicTypes.h"

#include "AppBuilder/AppBuilder.h"
#include "App/DeiTestApp.h"
#include "Console/DeiTestAutoConsole.h"
#include "EventLogger/DeiTestEventLog.h"

namespace BadumnaTestApp
{
    class DeiTestAppBuilder : public AppBuilder
    {
    public:
        DeiTestAppBuilder()
            : AppBuilder()
        {
        }

    protected:
        void DoBuildConsole()
        {
            console_ = new DeiTestAutoConsole();
            console_->Start();
        }

        void DoBuildEventLog()
        {
            event_log_ = new DeiTestEventLog();
        }

        void DoBuildApp(ProgramConfig config, Badumna::NetworkFacade *facade, boost::asio::io_service &io)
        {
            console_app_ = new DeiTestApp(config, facade, io, console_, event_log_);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(DeiTestAppBuilder);
    };
}

#endif // BADUMNA_AUTO_TEST_DEI_TEST_APP_BUILDER_H