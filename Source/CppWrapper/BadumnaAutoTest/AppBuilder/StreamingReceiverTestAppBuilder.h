//---------------------------------------------------------------------------------
// <copyright file="StreamingReceiverTestAppBuilder.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_STREAMING_RECEIVER_TEST_APP_BUILDER_H
#define BADUMNA_AUTO_TEST_STREAMING_RECEIVER_TEST_APP_BUILDER_H

#include "Badumna/Utils/BasicTypes.h"

#include "AppBuilder/AppBuilder.h"
#include "App/StreamingReceiverTestApp.h"
#include "EventLogger/StreamingReceiverTestEventLog.h"
#include "Console/StreamingReceiverAutoConsole.h"

namespace BadumnaTestApp
{
    class StreamingReceiverTestAppBuilder : public AppBuilder
    {
    public:
        StreamingReceiverTestAppBuilder()
            : AppBuilder()
        {
        }

    protected:
        void DoBuildConsole()
        {
            console_ = new StreamingReceiverAutoConsole();
            console_->Start();
        }

        void DoBuildEventLog()
        {
            event_log_ = new StreamingReceiverTestEventLog();
        }

        void DoBuildApp(ProgramConfig config, Badumna::NetworkFacade *facade, boost::asio::io_service &io)
        {
            console_app_ = new StreamingReceiverTestApp(config, facade, io, console_, event_log_);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(StreamingReceiverTestAppBuilder);
    };
}

#endif // BADUMNA_AUTO_TEST_STREAMING_RECEIVER_TEST_APP_BUILDER_H