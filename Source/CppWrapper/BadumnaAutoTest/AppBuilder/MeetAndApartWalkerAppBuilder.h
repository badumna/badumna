//---------------------------------------------------------------------------------
// <copyright file="MeetAndApartWalkerAppBuilder.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_MEET_AND_APART_WALKER_APP_BUILDER_H
#define BADUMNA_AUTO_TEST_MEET_AND_APART_WALKER_APP_BUILDER_H

#include "Badumna/Utils/BasicTypes.h"

#include "AppBuilder/AppBuilder.h"
#include "App/MeetAndApartWalkerTestApp.h"
#include "EventLogger/NullEventLog.h"
#include "Console/MeetAndApartWalkerAutoConsole.h"

namespace BadumnaTestApp
{
    class MeetAndApartWalkerAppBuilder : public AppBuilder
    {
    public:
        MeetAndApartWalkerAppBuilder()
            : AppBuilder()
        {
        }

    protected:
        void DoBuildConsole()
        {
            console_ = new MeetAndApartWalkerAutoConsole();
            console_->Start();
        }

        void DoBuildEventLog()
        {
            event_log_ = new NullEventLog();
        }

        void DoBuildApp(ProgramConfig config, Badumna::NetworkFacade *facade, boost::asio::io_service &io)
        {
            console_app_ = new MeetAndApartWalkerTestApp(config, facade, io, console_, event_log_);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(MeetAndApartWalkerAppBuilder);
    };
}

#endif // BADUMNA_AUTO_TEST_MEET_AND_APART_WALKER_APP_BUILDER_H