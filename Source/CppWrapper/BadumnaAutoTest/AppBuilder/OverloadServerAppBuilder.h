//---------------------------------------------------------------------------------
// <copyright file="OverloadServerAppBuilder.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_OVERLOAD_SERVER_APP_BUILDER_H
#define BADUMNA_AUTO_TEST_OVERLOAD_SERVER_APP_BUILDER_H

#include "Badumna/Utils/BasicTypes.h"

#include "AppBuilder/AppBuilder.h"
#include "App/OverloadServerApp.h"
#include "EventLogger/OverloadServerEventLog.h"
#include "Console/OverloadServerAutoConsole.h"
#include "Option/OverloadServerOptions.h"

namespace BadumnaTestApp
{
    class OverloadServerAppBuilder : public AppBuilder
    {
    public:
        OverloadServerAppBuilder()
            : AppBuilder()
        {
        }

    protected:
        void DoBuildOptions(ProgramConfig config)
        {
            options_.reset(new OverloadServerOptions());
            options_->SetOptions(config);
        }

        void DoBuildConsole()
        {
            console_ = new OverloadServerAutoConsole();
            console_->Start();
        }

        void DoBuildEventLog()
        {
            event_log_ = new OverloadServerEventLog();
        }

        void DoBuildApp(ProgramConfig config, Badumna::NetworkFacade *facade, boost::asio::io_service &io)
        {
            console_app_ = new OverloadServerApp(config, facade, io, console_, event_log_);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(OverloadServerAppBuilder);
    };
}

#endif // BADUMNA_AUTO_TEST_OVERLOAD_SERVER_APP_BUILDER_H