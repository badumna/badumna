//---------------------------------------------------------------------------------
// <copyright file="ArbitrationClientTestAppBuilder.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_ARBITRATION_CLIENT_TEST_APP_BUILDER_H
#define BADUMNA_AUTO_TEST_ARBITRATION_CLIENT_TEST_APP_BUILDER_H

#include "Badumna/Utils/BasicTypes.h"

#include "AppBuilder/AppBuilder.h"
#include "App/ArbitrationClientTestApp.h"
#include "EventLogger/ArbitrationTestEventLog.h"
#include "Console/ArbitrationClientTestAutoConsole.h"
#include "Option/ArbitrationConfigOptions.h"

namespace BadumnaTestApp
{
    class ArbitrationClientTestAppBuilder : public AppBuilder
    {
    public:
        ArbitrationClientTestAppBuilder()
            : AppBuilder()
        {
        }

    protected:
        void DoBuildOptions(ProgramConfig config)
        {
            options_.reset(new ArbitrationConfigOptions());
            options_->SetOptions(config);
        }

        void DoBuildConsole()
        {
            console_ = new ArbitrationClientTestAutoConsole();
            console_->Start();
        }

        void DoBuildEventLog()
        {
            event_log_ = new ArbitrationTestEventLog();
        }

        void DoBuildApp(ProgramConfig config, Badumna::NetworkFacade *facade, boost::asio::io_service &io)
        {
            console_app_ = new ArbitrationClientTestApp(config, facade, io, console_, event_log_);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(ArbitrationClientTestAppBuilder);
    };
}

#endif // BADUMNA_AUTO_TEST_ARBITRATION_CLIENT_TEST_APP_BUILDER_H