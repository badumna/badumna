//---------------------------------------------------------------------------------
// <copyright file="PCReLoginTestAppBuilder.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_PC_RE_LOGIN_TEST_APP_BUILDER_H
#define BADUMNA_AUTO_TEST_PC_RE_LOGIN_TEST_APP_BUILDER_H

#include "Badumna/Utils/BasicTypes.h"

#include "AppBuilder/AppBuilder.h"
#include "App/AppConfig.h"
#include "App/PCMeetAndApartTestApp.h"
#include "EventLogger/PCReLoginTestEventLog.h"
#include "Console/PCReLoginTestAutoConsole.h"

namespace BadumnaTestApp
{
    class PCReLoginTestAppBuilder : public AppBuilder
    {
    public:
        PCReLoginTestAppBuilder()
            : AppBuilder()
        {
        }

    protected:
        void DoBuildConsole()
        {
            std::cout << "initting console with do_login = " << program_config_->do_relogin_ << std::endl;
            console_ = new PCReLoginTestAutoConsole(program_config_->do_relogin_);
            console_->Start();
        }

        void DoBuildEventLog()
        {
            event_log_ = new PCReLoginTestEventLog(program_config_->do_relogin_);
        }

        void DoBuildApp(ProgramConfig config, Badumna::NetworkFacade *facade, boost::asio::io_service &io)
        {
            console_app_ = new PrivateChatTestApp(config, options_->GetOptions(), facade, io, console_, event_log_);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(PCReLoginTestAppBuilder);
    };
}

#endif // BADUMNA_AUTO_TEST_PC_RE_LOGIN_TEST_APP_BUILDER_H