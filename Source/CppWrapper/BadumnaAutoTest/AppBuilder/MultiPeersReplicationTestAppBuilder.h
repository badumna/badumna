//---------------------------------------------------------------------------------
// <copyright file="MultiPeersReplicationTestAppBuilder.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_MULTI_PEERS_REPLICATION_TEST_APP_BUILDER_H
#define BADUMNA_AUTO_TEST_MULTI_PEERS_REPLICATION_TEST_APP_BUILDER_H

#include "Badumna/Utils/BasicTypes.h"

#include "AppBuilder/AppBuilder.h"
#include "App/ReplicationTestApp.h"
#include "EventLogger/MultiPeersReplicationTestEventLog.h"
#include "Console/MultiPeersReplicationTestAutoConsole.h"

namespace BadumnaTestApp
{
    class MultiPeersReplicationTestAppBuilder : public AppBuilder
    {
    public:
        MultiPeersReplicationTestAppBuilder()
            : AppBuilder()
        {
        }

    protected:
        void DoBuildConsole()
        {
            console_ = new MultiPeersReplicationTestAutoConsole();
            console_->Start();
        }

        void DoBuildEventLog()
        {
            event_log_ = new MultiPeersReplicationTestEventLog();
        }

        void DoBuildApp(ProgramConfig config, Badumna::NetworkFacade *facade, boost::asio::io_service &io)
        {
            console_app_ = new ReplicationTestApp(config, facade, io, console_, event_log_);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(MultiPeersReplicationTestAppBuilder);
    };
}

#endif // BADUMNA_AUTO_TEST_MULTI_PEERS_REPLICATION_TEST_APP_BUILDER_H