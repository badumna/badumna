//---------------------------------------------------------------------------------
// <copyright file="MultiOriginalReplicationTestAppBuilder.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_MULTI_ORIGINAL_REPLICATION_TEST_APP_BUILDER_H
#define BADUMNA_AUTO_TEST_MULTI_ORIGINAL_REPLICATION_TEST_APP_BUILDER_H

#include "Badumna/Utils/BasicTypes.h"

#include "AppBuilder/AppBuilder.h"
#include "App/MultiOriginalReplicationTestApp.h"
#include "EventLogger/MultiOriginalReplicationTestEventLog.h"
#include "Console/MultiOriginalReplicationTestAutoConsole.h"

namespace BadumnaTestApp
{
    class MultiOriginalReplicationTestAppBuilder : public AppBuilder
    {
    public:
        MultiOriginalReplicationTestAppBuilder()
            : AppBuilder()
        {
        }

    protected:
        void DoBuildConsole()
        {
            console_ = new MultiOriginalReplicationTestAutoConsole();
            console_->Start();
        }

        void DoBuildEventLog()
        {
            event_log_ = new MultiOriginalReplicationTestEventLog();
        }

        void DoBuildApp(ProgramConfig config, Badumna::NetworkFacade *facade, boost::asio::io_service &io)
        {
            console_app_ = new MultiOriginalReplicationTestApp(config, facade, io, console_, event_log_);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(MultiOriginalReplicationTestAppBuilder);
    };
}

#endif // BADUMNA_AUTO_TEST_MULTI_ORIGINAL_REPLICATION_TEST_APP_BUILDER_H