//---------------------------------------------------------------------------------
// <copyright file="MeetAndApartTestAppBuilder.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_MEET_AND_APART_TEST_APP_BUILDER_H
#define BADUMNA_AUTO_TEST_MEET_AND_APART_TEST_APP_BUILDER_H

#include "Badumna/Utils/BasicTypes.h"

#include "AppBuilder/AppBuilder.h"
#include "App/ReplicationTestApp.h"
#include "EventLogger/MeetAndApartTestEventLog.h"
#include "Console/MeetAndApartTestAutoConsole.h"

namespace BadumnaTestApp
{
    class MeetAndApartTestAppBuilder : public AppBuilder
    {
    public:
        MeetAndApartTestAppBuilder()
            : AppBuilder()
        {
        }

    protected:
        void DoBuildConsole()
        {
            console_ = new MeetAndApartTestAutoConsole();
            console_->Start();
        }

        void DoBuildEventLog()
        {
            event_log_ = new MeetAndApartTestEventLog();
        }

        void DoBuildApp(ProgramConfig config, Badumna::NetworkFacade *facade, boost::asio::io_service &io)
        {
            console_app_ = new ReplicationTestApp(config, facade, io, console_, event_log_);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(MeetAndApartTestAppBuilder);
    };
}

#endif // BADUMNA_AUTO_TEST_MEET_AND_APART_TEST_APP_BUILDER_H