//---------------------------------------------------------------------------------
// <copyright file="ServiceDiscoveryClientTestAppBuilder.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_SERVICE_DISCOVERY_CLIENT_TEST_APP_BUILDER_H
#define BADUMNA_AUTO_TEST_SERVICE_DISCOVERY_CLIENT_TEST_APP_BUILDER_H

#include "Badumna/Utils/BasicTypes.h"

#include "AppBuilder/AppBuilder.h"
#include "App/ServiceDiscoveryClientTestApp.h"
#include "EventLogger/ArbitrationTestEventLog.h"
#include "Console/ArbitrationClientTestAutoConsole.h"
#include "Option/ArbitrationConfigOptions.h"

namespace BadumnaTestApp
{
    class ServiceDiscoveryClientTestAppBuilder : public AppBuilder
    {
    public:
        ServiceDiscoveryClientTestAppBuilder()
            : AppBuilder()
        {
        }

    protected:
        void DoBuildOptions(ProgramConfig config)
        {
            options_.reset(new ArbitrationConfigOptions());
            options_->SetOptions(config);
        }

        void DoBuildConsole()
        {
            console_ = new ArbitrationClientTestAutoConsole();
            console_->Start();
        }

        void DoBuildEventLog()
        {
            event_log_ = new ArbitrationTestEventLog();
        }

        void DoBuildApp(ProgramConfig config, Badumna::NetworkFacade *facade, boost::asio::io_service &io)
        {
            console_app_ = new ServiceDiscoveryClientTestApp(config, facade, io, console_, event_log_);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(ServiceDiscoveryClientTestAppBuilder);
    };
}

#endif // BADUMNA_AUTO_TEST_SERVICE_DISCOVERY_CLIENT_TEST_APP_BUILDER_H