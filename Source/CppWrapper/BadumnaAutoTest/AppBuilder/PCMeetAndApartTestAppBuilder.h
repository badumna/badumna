//---------------------------------------------------------------------------------
// <copyright file="PCMeetAndApartTestAppBuilder.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_PC_MEET_AND_APART_TEST_APP_BUILDER_H
#define BADUMNA_AUTO_TEST_PC_MEET_AND_APART_TEST_APP_BUILDER_H

#include "Badumna/Utils/BasicTypes.h"

#include "AppBuilder/AppBuilder.h"
#include "App/PCMeetAndApartTestApp.h"
#include "EventLogger/PCMeetAndApartTestEventLog.h"
#include "Console/PrivateChatTestAutoConsole.h"

namespace BadumnaTestApp
{
    class PCMeetAndApartTestAppBuilder : public AppBuilder
    {
    public:
        PCMeetAndApartTestAppBuilder()
            : AppBuilder()
        {
        }

    protected:
        void DoBuildConsole()
        {
            console_ = new PrivateChatTestAutoConsole();
            console_->Start();
        }

        void DoBuildEventLog()
        {
            event_log_ = new PCMeetAndApartTestEventLog();
        }

        void DoBuildApp(ProgramConfig config, Badumna::NetworkFacade *facade, boost::asio::io_service &io)
        {
            console_app_ = new PCMeetAndApartTestApp(config, facade, io, console_, event_log_);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(PCMeetAndApartTestAppBuilder);
    };
}

#endif // BADUMNA_AUTO_TEST_PRIVATE_CHAT_TEST_APP_BUILDER_H