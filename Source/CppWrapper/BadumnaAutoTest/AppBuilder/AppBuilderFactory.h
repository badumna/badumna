//---------------------------------------------------------------------------------
// <copyright file="AppBuilderFactory.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_APP_BUILDER_FACTORY_H
#define BADUMNA_AUTO_TEST_APP_BUILDER_FACTORY_H

#include "Badumna/Utils/BasicTypes.h"
#include "App/AppConfig.h"
#include "AppBuilder/AppBuilder.h"

namespace BadumnaTestApp
{
    /**
     * The factory class used to create required builders.
     */
    class AppBuilderFactory
    {
    public:
        static AppBuilder* Create(ProgramMode mode);

    private:
        DISALLOW_COPY_AND_ASSIGN(AppBuilderFactory);
    };
}

#endif // BADUMNA_AUTO_TEST_APP_BUILDER_FACTORY_H