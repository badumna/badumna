//---------------------------------------------------------------------------------
// <copyright file="LRRTestAppBuilder.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_LRR_TEST_APP_BUILDER_H
#define BADUMNA_AUTO_TEST_LRR_TEST_APP_BUILDER_H

#include "Badumna/Utils/BasicTypes.h"

#include "App/LRRTestApp.h"
#include "AppBuilder/AppBuilder.h"
#include "Console/LRRTestAutoConsole.h"
#include "EventLogger/NullEventLog.h"

namespace BadumnaTestApp
{
    class LRRTestAppBuilder : public AppBuilder
    {
    public:
        LRRTestAppBuilder()
            : AppBuilder()
        {
        }

    protected:
        void DoBuildConsole()
        {
            console_ = new LRRTestAutoConsole();
            console_->Start();
        }

        void DoBuildEventLog()
        {
            event_log_ = new NullEventLog();
        }

        void DoBuildApp(ProgramConfig config, Badumna::NetworkFacade *facade, boost::asio::io_service &io)
        {
            console_app_ = new LRRTestApp(config, facade, io, console_, event_log_);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(LRRTestAppBuilder);
    };
}

#endif // BADUMNA_AUTO_TEST_LRR_TEST_APP_BUILDER_H