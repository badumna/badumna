//---------------------------------------------------------------------------------
// <copyright file="StreamingSenderTestAppBuilder.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_STREAMING_SENDER_TEST_APP_BUILDER_H
#define BADUMNA_AUTO_TEST_STREAMING_SENDER_TEST_APP_BUILDER_H

#include "Badumna/Utils/BasicTypes.h"

#include "AppBuilder/AppBuilder.h"
#include "App/StreamingSenderTestApp.h"
#include "EventLogger/StreamingSenderTestEventLog.h"
#include "Console/StreamingSenderAutoConsole.h"

namespace BadumnaTestApp
{
    class StreamingSenderTestAppBuilder : public AppBuilder
    {
    public:
        StreamingSenderTestAppBuilder()
            : AppBuilder()
        {
        }

    protected:
        void DoBuildConsole()
        {
            console_ = new StreamingSenderAutoConsole();
            console_->Start();
        }

        void DoBuildEventLog()
        {
            event_log_ = new StreamingSenderTestEventLog();
        }

        void DoBuildApp(ProgramConfig config, Badumna::NetworkFacade *facade, boost::asio::io_service &io)
        {
            console_app_ = new StreamingSenderTestApp(config, facade, io, console_, event_log_);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(StreamingSenderTestAppBuilder);
    };
}

#endif // BADUMNA_AUTO_TEST_STREAMING_SENDER_TEST_APP_BUILDER_H