//---------------------------------------------------------------------------------
// <copyright file="PrivateChatTestAppBuilder.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_PRIVATE_CHAT_TEST_APP_BUILDER_H
#define BADUMNA_AUTO_TEST_PRIVATE_CHAT_TEST_APP_BUILDER_H

#include "Badumna/Utils/BasicTypes.h"

#include "AppBuilder/AppBuilder.h"
#include "App/PrivateChatTestApp.h"
#include "EventLogger/PrivateChatTestEventLog.h"
#include "Console/PrivateChatTestAutoConsole.h"

namespace BadumnaTestApp
{
    class PrivateChatTestAppBuilder : public AppBuilder
    {
    public:
        PrivateChatTestAppBuilder()
            : AppBuilder()
        {
        }

    protected:
        void DoBuildConsole()
        {
            console_ = new PrivateChatTestAutoConsole();
            console_->Start();
        }

        void DoBuildEventLog()
        {
            event_log_ = new PrivateChatTestEventLog();
        }

        void DoBuildApp(ProgramConfig config, Badumna::NetworkFacade *facade, boost::asio::io_service &io)
        {
            console_app_ = new PrivateChatTestApp(config, facade, io, console_, event_log_);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(PrivateChatTestAppBuilder);
    };
}

#endif // BADUMNA_AUTO_TEST_PRIVATE_CHAT_TEST_APP_BUILDER_H