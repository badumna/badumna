//---------------------------------------------------------------------------------
// <copyright file="ArbitrationServerTestAppBuilder.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_ARBITRATION_SERVER_TEST_APP_BUILDER_H
#define BADUMNA_AUTO_TEST_ARBITRATION_SERVER_TEST_APP_BUILDER_H

#include "Badumna/Utils/BasicTypes.h"

#include "AppBuilder/AppBuilder.h"
#include "App/ArbitrationServerTestApp.h"
#include "EventLogger/NullEventLog.h"
#include "Console/ArbitrationServerTestAutoConsole.h"
#include "Option/ArbitrationConfigOptions.h"

namespace BadumnaTestApp
{
    class ArbitrationServerTestAppBuilder : public AppBuilder
    {
    public:
        ArbitrationServerTestAppBuilder()
            : AppBuilder()
        {
        }

    protected:
        void DoBuildOptions(ProgramConfig config)
        {
            options_.reset(new ArbitrationConfigOptions());
            options_->SetOptions(config);
        }

        void DoBuildConsole()
        {
            console_ = new ArbitrationServerTestAutoConsole();
            console_->Start();
        }

        void DoBuildEventLog()
        {
            event_log_ = new NullEventLog();
        }

        void DoBuildApp(ProgramConfig config, Badumna::NetworkFacade *facade, boost::asio::io_service &io)
        {
            console_app_ = new ArbitrationServerTestApp(config, facade, io, console_, event_log_);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(ArbitrationServerTestAppBuilder);
    };
}

#endif // BADUMNA_AUTO_TEST_ARBITRATION_SERVER_TEST_APP_BUILDER_H