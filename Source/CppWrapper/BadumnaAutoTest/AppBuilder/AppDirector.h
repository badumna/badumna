//---------------------------------------------------------------------------------
// <copyright file="AppDirector.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_APP_DIRECTOR_H
#define BADUMNA_AUTO_TEST_APP_DIRECTOR_H

#include <memory>
#include <boost/asio.hpp>

#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/Configuration/Options.h"

#include "App/AppConfig.h"
#include "AppBuilder/AppBuilderFactory.h"

namespace BadumnaTestApp
{
    /**
     * The director class used to manage the process of building and assembling all required components. 
     */
    class AppDirector
    {
    public:
        AppDirector(ProgramMode mode)
            : builder_(AppBuilderFactory::Create(mode))
        {
        }

        /**
         * Builds and assembles all components. 
         */
        ConsoleApp *CreateApp(ProgramConfig config, boost::asio::io_service &io)
        {
            builder_->BuildOptions(config);
            builder_->BuildConsole();
            builder_->BuildEventLog();

            builder_->BuildApp(config, io);

            return builder_->GetApp();
        }
    private:
        std::auto_ptr<AppBuilder> builder_;

        DISALLOW_COPY_AND_ASSIGN(AppDirector);
    };
}

#endif // BADUMNA_AUTO_TEST_APP_DIRECTOR_H