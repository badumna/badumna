//---------------------------------------------------------------------------------
// <copyright file="ProximityChatTestAppBuilder.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_PROXIMITY_CHAT_TEST_APP_BUILDER_H
#define BADUMNA_AUTO_TEST_PROXIMITY_CHAT_TEST_APP_BUILDER_H

#include "Badumna/Utils/BasicTypes.h"

#include "AppBuilder/AppBuilder.h"
#include "App/ProximityChatTestApp.h"
#include "EventLogger/ProximityChatTestEventLog.h"
#include "Console/ProximityChatTestAutoConsole.h"

namespace BadumnaTestApp
{
    class ProximityChatTestAppBuilder : public AppBuilder
    {
    public:
        ProximityChatTestAppBuilder()
            : AppBuilder()
        {
        }

    protected:
        void DoBuildConsole()
        {
            console_ = new ProximityChatTestAutoConsole();
            console_->Start();
        }

        void DoBuildEventLog()
        {
            event_log_ = new ProximityChatTestEventLog();
        }

        void DoBuildApp(ProgramConfig config, Badumna::NetworkFacade *facade, boost::asio::io_service &io)
        {
            console_app_ = new ProximityChatTestApp(config, facade, io, console_, event_log_);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(ProximityChatTestAppBuilder);
    };
}

#endif // BADUMNA_AUTO_TEST_PROXITIMITY_CHAT_TEST_APP_BUILDER_H