//---------------------------------------------------------------------------------
// <copyright file="ChangeSceneSenderAppBuilder.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_CHANGE_SCENE_SENDER_APP_BUILDER_H
#define BADUMNA_AUTO_TEST_CHANGE_SCENE_SENDER_APP_BUILDER_H

#include "Badumna/Utils/BasicTypes.h"

#include "App/ChangeSceneTestApp.h"
#include "AppBuilder/AppBuilder.h"
#include "EventLogger/NullEventLog.h"
#include "Console/ChangeSceneSenderAutoConsole.h"

namespace BadumnaTestApp
{
    class ChangeSceneSenderAppBuilder : public AppBuilder
    {
    public:
        ChangeSceneSenderAppBuilder()
            : AppBuilder()
        {
        }

    protected:
        void DoBuildConsole()
        {
            console_ = new ChangeSceneSenderAutoConsole();
            console_->Start();
        }

        void DoBuildEventLog()
        {
            event_log_ = new NullEventLog();
        }

        void DoBuildApp(ProgramConfig config, Badumna::NetworkFacade *facade, boost::asio::io_service &io)
        {
            console_app_ = new ChangeSceneTestApp(config, facade, io, console_, event_log_);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(ChangeSceneSenderAppBuilder);
    };
}

#endif // BADUMNA_AUTO_TEST_CHANGE_SCENE_SENDER_APP_BUILDER_H