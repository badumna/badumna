//---------------------------------------------------------------------------------
// <copyright file="CustomEventTestAppBuilder.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_CUSTOM_EVENT_TEST_APP_BUILDER_H
#define BADUMNA_AUTO_TEST_CUSTOM_EVENT_TEST_APP_BUILDER_H

#include "Badumna/Utils/BasicTypes.h"

#include "AppBuilder/AppBuilder.h"
#include "App/CustomEventTestApp.h"
#include "EventLogger/CustomEventTestEventLog.h"
#include "Console/CustomEventTestAutoConsole.h"

namespace BadumnaTestApp
{
    class CustomEventTestAppBuilder : public AppBuilder
    {
    public:
        CustomEventTestAppBuilder()
            : AppBuilder()
        {
        }

    protected:
        void DoBuildConsole()
        {
            console_ = new CustomEventTestAutoConsole();
            console_->Start();
        }

        void DoBuildEventLog()
        {
            event_log_ = new CustomEventTestEventLog();
        }

        void DoBuildApp(ProgramConfig config, Badumna::NetworkFacade *facade, boost::asio::io_service &io)
        {
            console_app_ = new CustomEventTestApp(config, facade, io, console_, event_log_);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(CustomEventTestAppBuilder);
    };
}

#endif // BADUMNA_AUTO_TEST_PROXITIMITY_CHAT_TEST_APP_BUILDER_H