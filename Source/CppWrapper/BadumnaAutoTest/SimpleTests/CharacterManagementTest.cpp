//---------------------------------------------------------------------------------
// <copyright file="CharacterManagementTest.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <iostream>
#include <stdexcept>
#include "Badumna/Dei/LoginResult.h"
#include "Badumna/Dei/IdentityProvider.h"
#include "SimpleTests/CharacterManagementTest.h"

using namespace Badumna;
using namespace Dei;

namespace BadumnaTestApp
{
    CharacterManagementTest::CharacterManagementTest(ProgramConfig program_config)
        : SimpleTest(false, program_config),
        session_(),
        username_("user"),
        password_("user")
    {
        this->InitDei();
    }

    void CharacterManagementTest::DoLogin()
    {
        LoginResult login_result = session_->Authenticate(username_, password_);
        if(!login_result.WasSuccessful())
        {
            throw std::runtime_error(std::string("Unable to login: ") + login_result.GetErrorDescription().UTF8CStr());
        }
    }

    int CharacterManagementTest::OnTest()
    {
        std::cout << "TestCreateAndAuthorizeCharacter ..." << std::endl;
        this->TestCreateAndAuthorizeCharacter();
        
        std::cout << "TestAuthorizeFailure ..." << std::endl;
        this->TestAuthorizeFailure();
        
        std::cout << "TestDeleteCharacter ..." << std::endl;
        this->TestDeleteCharacter();

        std::cout << "Tests complete." << std::endl;
        return 0;
    }

    void CharacterManagementTest::Shutdown()
    {
        std::cout << "Disposing session object" << std::endl;
        session_->Dispose();
    }

    bool CharacterManagementTest::AuthorizeAs(Character character)
    {
        this->ResetAuth();
        IdentityProvider identity_provider;

        std::cout << "Authorizing as: " << character.Name().UTF8CStr() << std::endl;
        bool authorized = session_->SelectIdentity(character, identity_provider).WasSuccessful();
        if(authorized)
        {
            std::cout << "Authorization succeeded, logging in." << std::endl;
            authorized = facade_->Login(identity_provider);
        }
        std::cout << "Authorization / login complete, success = " << authorized << std::endl;
        return authorized;
    }

    void CharacterManagementTest::InitDei()
    {
        std::cout << "Initting dei..." << std::endl;
        session_.reset(new Session(String("localhost"), 21248));
        session_->SetUseSslConnection(false);
    }

    void CharacterManagementTest::ResetAuth()
    {
        this->InitDei();
        std::cout << "Authenticating ..." << std::endl;
        LoginResult authentication = session_->Authenticate("user", "user");
        if(!authentication.WasSuccessful())
        {
            throw std::runtime_error("Can't authenticate");
        }
    }

    void CharacterManagementTest::TestCreateAndAuthorizeCharacter()
    {
        this->ResetAuth();
        std::auto_ptr<Character> character = session_->CreateCharacter(String("Bob"));
        if(character.get() == NULL)
        {
            throw std::runtime_error("Failed to create character");
        }

        std::vector<Badumna::Character> characters = session_->ListCharacters();
        bool found = false;
        std::vector<Character>::iterator iter;
        for (iter = characters.begin(); iter != characters.end(); ++iter)
        {
            if((*iter) == (*character))
            {
                found = true;
                break;
            }
        }

        if(!found)
        {
            throw std::runtime_error("Created character not returned by ListCharacters()");
        }

        std::auto_ptr<Character> character2 = session_->CreateCharacter(String("Bob"));
        if(character2.get() != NULL)
        {
           throw std::runtime_error("Created a character twice with the same name!");
        }
        
        if(!this->AuthorizeAs(*character))
        {
            throw std::runtime_error("Failed to authorize as newly-created character");
        }

    }

    void CharacterManagementTest::TestAuthorizeFailure()
    {
        if(this->AuthorizeAs(Character(2, String("Captain Two"))))
        {
            throw std::runtime_error("Logged in with made-up character");
        }
    }

    void CharacterManagementTest::TestDeleteCharacter()
    {
        this->ResetAuth();
        std::auto_ptr<Character> character = session_->CreateCharacter(String("Jack"));
        if(character.get() == NULL)
        {
            throw std::runtime_error("Failed to create character");
        }
        
        bool success = session_->DeleteCharacter(*character);
        if(!success)
        {
            throw std::runtime_error("Couldn't delete character");
        }

        success = session_->DeleteCharacter(*character);
        if(success)
        {
            throw std::runtime_error("Delete character twice!?");
        }
        
        if(this->AuthorizeAs(*character))
        {
            throw std::runtime_error("Managed to log in as a character that should have been deleted!");
        }
    }
}
