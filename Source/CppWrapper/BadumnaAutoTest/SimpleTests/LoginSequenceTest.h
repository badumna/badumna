//---------------------------------------------------------------------------------
// <copyright file="LoginSequenceTest.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_LOGIN_SEQUENCE_TEST_H
#define BADUMNA_AUTO_TEST_LOGIN_SEQUENCE_TEST_H

#include "Badumna/Replication/ISpatialReplica.h"
#include "Badumna/Replication/NetworkScene.h"
#include "Badumna/DataTypes/BadumnaId.h"
#include "SimpleTests/SimpleTest.h"
#include "App/AppConfig.h"

namespace BadumnaTestApp
{
    class LoginSequenceTest : public SimpleTest
    {
    public:
        LoginSequenceTest(ProgramConfig program_config);

    protected:
        void DoLogin();
        int OnTest();

        Badumna::ISpatialReplica *CreateSpatialReplica(
            Badumna::NetworkScene const &scene, 
            Badumna::BadumnaId const &id, 
            uint32_t type);
        
        void RemoveSpatialReplica(Badumna::NetworkScene const &scene, Badumna::ISpatialReplica const &replica);
    };
}

#endif // BADUMNA_AUTO_TEST_LOGIN_SEQUENCE_TEST_H