//---------------------------------------------------------------------------------
// <copyright file="SimpleTest.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_SIMPLE_TEST_H
#define BADUMNA_AUTO_TEST_SIMPLE_TEST_H

#include <memory>
#include "App/AppConfig.h"
#include "Badumna/Core/RuntimeInitializer.h"
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Configuration/Options.h"

namespace BadumnaTestApp
{
    /**
     * SimpleTest is the base class for all simple tests. Simple tests are tests that are significantly simpler than 
     * regular auto tests. 
     */
    class SimpleTest
    {
    public:
        SimpleTest(bool exception_expected, ProgramConfig program_config);
        virtual ~SimpleTest();

        int RunTest();
        void Initialize();
        void Shutdown();

    protected:
        virtual void DoLogin();
        virtual void DoShutdown();
        virtual void CreateOptions();
        virtual int OnTest() = 0;

        void ExceptionHandler(Badumna::String const &m1, Badumna::String const &m2, Badumna::String const &m3);
        void ErrorHandler(int32_t code, Badumna::String const &message);
        
#ifdef WIN32
        Badumna::BadumnaRuntimeInitializer initializer_;
#else
        Badumna::BadumnaRuntimeInitializer2 initializer_;
#endif 

        std::auto_ptr<Badumna::Options> options_;
        std::auto_ptr<Badumna::NetworkFacade> facade_;
        bool exception_expected_;
        bool exception_triggered_;
        bool facade_is_valid_;
        ProgramConfig program_config_;
    };
}

#endif // BADUMNA_AUTO_TEST_SIMPLE_TEST_H