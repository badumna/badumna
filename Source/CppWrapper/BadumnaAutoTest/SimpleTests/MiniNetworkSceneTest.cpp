//---------------------------------------------------------------------------------
// <copyright file="MiniNetworkSceneTest.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2012 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include "Badumna/Replication/SpatialReplicaDelegate.h"
#include "SimpleTests/MiniNetworkSceneTest.h"

namespace BadumnaTestApp
{
    MiniNetworkSceneTest::MiniNetworkSceneTest(ProgramConfig program_config)
        : SimpleTest(false, program_config)
    {
    }


    int MiniNetworkSceneTest::OnTest()
    {
        assert(facade_->IsLoggedIn());

        Badumna::CreateSpatialReplicaDelegate create_delegate(&MiniNetworkSceneTest::CreateSpatialReplica, *this);
        Badumna::RemoveSpatialReplicaDelegate remove_delegate(&MiniNetworkSceneTest::RemoveSpatialReplica, *this);
        Badumna::NetworkScene *scene = facade_->JoinMiniScene("test_scene_name", create_delegate, remove_delegate);

		assert(scene->MiniScene());
        return 0;
    }

    Badumna::ISpatialReplica *MiniNetworkSceneTest::CreateSpatialReplica(
            Badumna::NetworkScene const &, 
            Badumna::BadumnaId const &, 
            uint32_t)
    {
        return NULL;
    }
        
    void MiniNetworkSceneTest::RemoveSpatialReplica(
        Badumna::NetworkScene const &, 
        Badumna::ISpatialReplica const &)
    {
    }
}