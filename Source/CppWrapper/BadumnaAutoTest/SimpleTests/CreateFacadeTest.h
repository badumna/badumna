//---------------------------------------------------------------------------------
// <copyright file="CreateFacadeTest.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_CREATE_FACADE_TEST_H
#define BADUMNA_AUTO_TEST_CREATE_FACADE_TEST_H

#include "SimpleTests/SimpleTest.h"
#include "App/AppConfig.h"

namespace BadumnaTestApp
{
    class CreateFacadeTest : public SimpleTest
    {
    public:
        CreateFacadeTest(ProgramConfig program_config);

    protected:
        int OnTest();
        void CreateOptions();
    };
}

#endif // BADUMNA_AUTO_TEST_CREATE_FACADE_TEST_H