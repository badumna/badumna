//---------------------------------------------------------------------------------
// <copyright file="SimpleTestsFactory.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_SIMPLE_TEST_FACTORY_H
#define BADUMNA_AUTO_TEST_SIMPLE_TEST_FACTORY_H

#include "App/AppConfig.h"

namespace BadumnaTestApp
{
    class SimpleTestFactory
    {
    public:
        static SimpleTest *Create(ProgramConfig const &config);
    };
}

#endif // BADUMNA_AUTO_TEST_SIMPLE_TEST_FACTORY_H