//---------------------------------------------------------------------------------
// <copyright file="AnnouncePermissionTest.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_ANNOUNCE_PERMISSION_TEST_H
#define BADUMNA_AUTO_TEST_ANNOUNCE_PERMISSION_TEST_H

#include "Badumna/DataTypes/String.h"
#include "SimpleTests/SimpleTest.h"

namespace BadumnaTestApp
{
    class AnnouncePermissionTest : public SimpleTest
    {
    public:
        AnnouncePermissionTest(ProgramConfig program_config);
    
    private:
        int OnTest();
        void CreateOptions();
    };
}

#endif // BADUMNA_AUTO_TEST_ANNOUNCE_PERMISSION_TEST_H