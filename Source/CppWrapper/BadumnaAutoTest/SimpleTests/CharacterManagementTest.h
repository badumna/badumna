//---------------------------------------------------------------------------------
// <copyright file="CharacterManagementTest.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef CHARACTER_MANAGEMENT_TEST_H
#define CHARACTER_MANAGEMENT_TEST_H

#include "SimpleTests/SimpleTest.h"
#include "App/AppConfig.h"
#include "Badumna/Dei/LoginResult.h"
#include "Badumna/Dei/Session.h"

namespace BadumnaTestApp
{
    class CharacterManagementTest : public SimpleTest
    {
    public:
        CharacterManagementTest(ProgramConfig program_config);

    protected:
        void DoLogin();
        void Shutdown();
        int OnTest();

        bool AuthorizeAs(Badumna::Character character);

        void InitDei();

        void ResetAuth();

        std::auto_ptr<Dei::Session> session_;

        void TestCreateAndAuthorizeCharacter();

        void TestAuthorizeFailure();

        void TestDeleteCharacter();

        Badumna::String username_;
        Badumna::String password_;
    };
}

#endif // CHARACTER_MANAGEMENT_TEST_H
