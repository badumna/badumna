//---------------------------------------------------------------------------------
// <copyright file="SimpleMinimumTest.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_SIMPLE_MINIMUM_TEST_H
#define BADUMNA_AUTO_TEST_SIMPLE_MINIMUM_TEST_H

#include "Badumna/DataTypes/String.h"
#include "SimpleTests/SimpleTest.h"
#include "App/AppConfig.h"

namespace BadumnaTestApp
{
    class SimpleMinimumTest : public SimpleTest
    {
    public:
        SimpleMinimumTest(ProgramConfig program_config);
    
    private:
        int OnTest();
    };
}

#endif // BADUMNA_AUTO_TEST_SIMPLE_MINIMUM_TEST_H