//---------------------------------------------------------------------------------
// <copyright file="LoginSequenceTest.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include "Badumna/Replication/SpatialReplicaDelegate.h"
#include "SimpleTests/LoginSequenceTest.h"

namespace BadumnaTestApp
{
    LoginSequenceTest::LoginSequenceTest(ProgramConfig program_config)
        : SimpleTest(true, program_config)
    {
    }

    void LoginSequenceTest::DoLogin()
    {
        // skip login, do nothing here 
    }

    int LoginSequenceTest::OnTest()
    {
        assert(!facade_->IsLoggedIn());

        Badumna::CreateSpatialReplicaDelegate create_delegate(&LoginSequenceTest::CreateSpatialReplica, *this);
        Badumna::RemoveSpatialReplicaDelegate remove_delegate(&LoginSequenceTest::RemoveSpatialReplica, *this);
        facade_->JoinScene("test_scene_name", create_delegate, remove_delegate);

        return 0;
    }

    Badumna::ISpatialReplica *LoginSequenceTest::CreateSpatialReplica(
            Badumna::NetworkScene const &, 
            Badumna::BadumnaId const &, 
            uint32_t)
    {
        return NULL;
    }
        
    void LoginSequenceTest::RemoveSpatialReplica(
        Badumna::NetworkScene const &, 
        Badumna::ISpatialReplica const &)
    {
    }
}