//---------------------------------------------------------------------------------
// <copyright file="SimpleTest.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include "SimpleTests/SimpleTest.h"
#include "Option/PeerConfigOptions.h"
#include "Badumna/Core/RuntimeInitializer.h"
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/Core/BadumnaExceptionCallback.h"
#include "Badumna/Configuration/Options.h"

namespace BadumnaTestApp
{
    SimpleTest::SimpleTest(bool exception_expected, ProgramConfig program_config)
        :
#ifdef WIN32
        initializer_(
            Badumna::BadumnaExceptionCallback(&SimpleTest::ExceptionHandler, *this), 
            Badumna::InternalErrorCallback(&SimpleTest::ErrorHandler, *this)),
#else
        initializer_(
            Badumna::BadumnaExceptionCallback(&SimpleTest::ExceptionHandler, *this), 
            Badumna::InternalErrorCallback(&SimpleTest::ErrorHandler, *this), 
            "mono_config_file"),
#endif
        options_(NULL), 
        facade_(NULL),
        exception_expected_(exception_expected),
        exception_triggered_(false),
        facade_is_valid_(false),
        program_config_(program_config)
    {
    }
    
    SimpleTest::~SimpleTest()
    {
    }

    int SimpleTest::RunTest()
    {
        int ret = OnTest();

        if(exception_expected_ && !exception_triggered_)
        {
            std::cout << "exception expected, but not triggered." << std::endl;
            return EXIT_FAILURE;
        }

        if(!exception_expected_ && exception_triggered_)
        {
            std::cout << "unexpected exception triggered." << std::endl;
            return EXIT_FAILURE;
        }

        if(ret != 0)
        {
            return EXIT_FAILURE;
        }
        else
        {
            return EXIT_SUCCESS;
        }
    }

    void SimpleTest::CreateOptions()
    {
        options_.reset(new Badumna::Options());
        ConfigureDefaultOptions(*options_, program_config_);
    }

    void SimpleTest::Initialize()
    {
        CreateOptions();
        facade_.reset(Badumna::NetworkFacade::Create(*(options_.get())));

        if(!exception_triggered_)
        {
            facade_is_valid_ = true;
            DoLogin();
        }
        else
        {
            std::cout << "exception already triggered, skipping login" << std::endl;
        }
    }

    void SimpleTest::Shutdown()
    {
        DoShutdown();
    }

    void SimpleTest::DoLogin()
    {
        bool success = facade_->Login("defaultCharacter");
        if(!success)
        {
            throw std::runtime_error("failed to login.");
        }
    }

    void SimpleTest::DoShutdown()
    {
        if(facade_.get() != NULL && facade_is_valid_)
        {
            facade_->Shutdown();
        }
    }

    void SimpleTest::ExceptionHandler(Badumna::String const &stack_trace, Badumna::String const &exception_type_name, Badumna::String const &message)
    {
        std::cout << "Unhandled " << exception_type_name.UTF8CStr() << ": " << message.UTF8CStr() << std::endl;
        std::cout << stack_trace.UTF8CStr() << std::endl << std::endl;

        exception_triggered_ = true;
    }

    void SimpleTest::ErrorHandler(int32_t code, Badumna::String const &message)
    {
        std::cout << "internal error handler is called." << std::endl;
        std::cout << "code : " << code << std::endl;
        std::cout << "message : " << message.UTF8CStr() << std::endl;

        exception_triggered_ = true;
    }
}