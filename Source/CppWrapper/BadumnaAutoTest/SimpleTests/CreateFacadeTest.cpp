//---------------------------------------------------------------------------------
// <copyright file="CreateFacadeTest.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include "SimpleTests/CreateFacadeTest.h"
#include "App/AppConfig.h"

namespace BadumnaTestApp
{
    CreateFacadeTest::CreateFacadeTest(ProgramConfig program_config)
        : SimpleTest(true, program_config)
    {
    }

    int CreateFacadeTest::OnTest()
    {
        return 0;
    }
    
    void CreateFacadeTest::CreateOptions()
    {
        SimpleTest::CreateOptions();
        options_->GetConnectivityModule().SetStartPortRange(213000);
        options_->GetConnectivityModule().SetEndPortRange(219990);
    }
}