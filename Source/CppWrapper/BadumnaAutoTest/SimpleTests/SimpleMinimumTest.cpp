//---------------------------------------------------------------------------------
// <copyright file="SimpleMinimumTest.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <iostream>
#include "Badumna/Core/NetworkStatus.h"
#include "SimpleTests/SimpleMinimumTest.h"

namespace BadumnaTestApp
{
    SimpleMinimumTest::SimpleMinimumTest(ProgramConfig program_config)
        : SimpleTest(false, program_config)
    {
    }

    int SimpleMinimumTest::OnTest()
    {
        std::wcout << facade_->GetNetworkStatus() << std::endl;

        return 0;
    }
}