//---------------------------------------------------------------------------------
// <copyright file="AnnouncePermissionTest.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <iostream>
#include "SimpleTests/AnnouncePermissionTest.h"

namespace BadumnaTestApp
{
    AnnouncePermissionTest::AnnouncePermissionTest(ProgramConfig program_config)
        : SimpleTest(false, program_config)
    {
    }

    int AnnouncePermissionTest::OnTest()
    {
        std::cout << "going to call announce service" << std::endl;
        facade_->AnnounceService(Badumna::ServerType_OverloadServer);
        std::cout << "announce service returned. going to return 0" << std::endl;
        return 0;
    }

    void AnnouncePermissionTest::CreateOptions()
    {
        SimpleTest::CreateOptions();
        options_->GetOverloadModule().SetAsServer();
    }
}