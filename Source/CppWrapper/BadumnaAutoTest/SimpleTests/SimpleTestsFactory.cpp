//---------------------------------------------------------------------------------
// <copyright file="SimpleTestsFactory.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <iostream>
#include <stdexcept>
#include "Badumna/DataTypes/String.h"
#include "SimpleTests/SimpleTest.h"
#include "SimpleTests/SimpleTestsFactory.h"
#include "SimpleTests/SimpleMinimumTest.h"
#include "SimpleTests/LoginSequenceTest.h"
#include "SimpleTests/CreateFacadeTest.h"
#include "SimpleTests/AnnouncePermissionTest.h"
#include "SimpleTests/CharacterManagementTest.h"
#include "SimpleTests/MiniNetworkSceneTest.h"

namespace BadumnaTestApp
{
    SimpleTest *SimpleTestFactory::Create(ProgramConfig const &config)
    {
        Badumna::String app_name = "badumna_simple_test_";
        app_name += config.app_name_salt_;

        if(config.mode_ == MinimumTest)
        {
            std::cout << "Minimum test mode." << std::endl;
            return new SimpleMinimumTest(config);
        }
        else if(config.mode_ == SimpleLoginSequenceTest)
        {
            std::cout << "login sequence test mode. " << std::endl;
            return new LoginSequenceTest(config);
        }
        else if(config.mode_ == SimpleCreateFacadeTest)
        {
            std::cout << "create facade test mode. " << std::endl;
            return new CreateFacadeTest(config);
        }
        else if(config.mode_ == SimpleAnnouncePermissionTest)
        {
            std::cout << "announce permission test mode." << std::endl;
            return new AnnouncePermissionTest(config);
        }
        else if(config.mode_ == SimpleCharacterManagementTest)
        {
            std::cout << "character management test mode. " << std::endl;
            return new CharacterManagementTest(config);
        }
		else if(config.mode_ == SimpleMiniNetworkSceneTest)
		{
			std::cout << "mini network scene test mode. " << std::endl;
			return new MiniNetworkSceneTest(config);
		}

        throw std::runtime_error("unknown test mode.");
    }
}