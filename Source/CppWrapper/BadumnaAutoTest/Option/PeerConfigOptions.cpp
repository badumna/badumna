//---------------------------------------------------------------------------------
// <copyright file="PeerConfigOptions.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include "Option/PeerConfigOptions.h"
#include "Badumna/Configuration/TunnelMode.h"

namespace BadumnaTestApp
{
    void PeerConfigOptions::DoSetOptions(ProgramConfig program_config)
    {
        ConfigureDefaultOptions(*options_, program_config);
    }

    void ConfigureDefaultOptions(Badumna::Options &options, ProgramConfig const &program_config)
    {
        options.GetConnectivityModule().SetStartPortRange(21300);
        options.GetConnectivityModule().SetEndPortRange(21999);
        options.GetConnectivityModule().SetMaxPortsToTry(9);
        options.GetConnectivityModule().EnableBroadcast();
        options.GetConnectivityModule().SetBroadcastPort(21250);
        options.GetConnectivityModule().ConfigureForLan();
        options.GetConnectivityModule().SetApplicationName(program_config.app_name_salt_);

        if (program_config.use_tunnel_)
        {
            options.GetConnectivityModule().SetTunnelMode(Badumna::TunnelMode_On);
            options.GetConnectivityModule().AddTunnelUri("http://localhost:21247");
        }

        options.GetLoggerModule().EnableDotNetTraceForwarding();
        options.GetLoggerModule().SuppressAssertUi();
        options.GetLoggerModule().SetLoggerType(Badumna::LoggerType_File);
    }
}