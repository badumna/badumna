//---------------------------------------------------------------------------------
// <copyright file="OverloadServerOptions.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_OVERLOAD_SERVER_OPTIONS_H
#define BADUMNA_AUTO_TEST_OVERLOAD_SERVER_OPTIONS_H

#include "App/AppConfig.h"
#include "Option/PeerConfigOptions.h"
#include "Badumna/Configuration/Options.h"
#include "Badumna/Utils/BasicTypes.h"

namespace BadumnaTestApp
{
    class OverloadServerOptions : public PeerConfigOptions
    {
    public:
        OverloadServerOptions()
            : PeerConfigOptions()
        {
        }
    
    protected:
        virtual void DoSetOptions(ProgramConfig program_config)
        {
            // standard option
            PeerConfigOptions::DoSetOptions(program_config);
            options_->GetOverloadModule().SetAsServer();
        }

        DISALLOW_COPY_AND_ASSIGN(OverloadServerOptions);
    };
}

#endif // BADUMNA_AUTO_TEST_OVERLOAD_SERVER_OPTIONS_H