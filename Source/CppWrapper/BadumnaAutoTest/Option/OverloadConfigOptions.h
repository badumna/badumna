//---------------------------------------------------------------------------------
// <copyright file="OverloadConfigOptions.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_OVERLOAD_CONFIG_OPTIONS_H
#define BADUMNA_AUTO_TEST_OVERLOAD_CONFIG_OPTIONS_H

#include "App/AppConfig.h"
#include "Option/PeerConfigOptions.h"
#include "Badumna/Configuration/Options.h"
#include "Badumna/Utils/BasicTypes.h"

namespace BadumnaTestApp
{
    class OverloadConfigOptions : public PeerConfigOptions
    {
    public:
        OverloadConfigOptions()
            : PeerConfigOptions()
        {
        }

    protected:
        virtual void DoSetOptions(ProgramConfig program_config)
        {
            // standard option
            PeerConfigOptions::DoSetOptions(program_config);
            options_->GetOverloadModule().SetClientEnabled();
            options_->GetOverloadModule().EnableForcedOverload("i_understand_this_is_for_testing_only");
        }

        DISALLOW_COPY_AND_ASSIGN(OverloadConfigOptions);
    };
}

#endif // BADUMNA_AUTO_TEST_OVERLOAD_CONFIG_OPTIONS_H