//---------------------------------------------------------------------------------
// <copyright file="ArbitrationConfigOptions.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_ARBITRATION_CONFIG_OPTIONS_H
#define BADUMNA_AUTO_TEST_ARBITRATION_CONFIG_OPTIONS_H

#include "App/AppConfig.h"
#include "Option/PeerConfigOptions.h"
#include "Badumna/Configuration/Options.h"
#include "Badumna/Utils/BasicTypes.h"

namespace BadumnaTestApp
{
    class ArbitrationConfigOptions : public PeerConfigOptions
    {
    public:
        ArbitrationConfigOptions()
            : PeerConfigOptions()
        {
        }

    protected:
        virtual void DoSetOptions(ProgramConfig program_config)
        {
            // standard option
            PeerConfigOptions::DoSetOptions(program_config);
            options_->GetArbitrationModule().AddServer(L"arb_test");
        }

        DISALLOW_COPY_AND_ASSIGN(ArbitrationConfigOptions);
    };
}

#endif // BADUMNA_AUTO_TEST_ARBITRATION_CONFIG_OPTIONS_H