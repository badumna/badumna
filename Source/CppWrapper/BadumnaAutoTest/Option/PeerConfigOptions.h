//---------------------------------------------------------------------------------
// <copyright file="PeerConfigOptions.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_PEER_CONFIG_OPTIONS_H
#define BADUMNA_AUTO_TEST_PEER_CONFIG_OPTIONS_H

#include <memory>
#include "App/AppConfig.h"
#include "Badumna/Configuration/Options.h"
#include "Badumna/Configuration/LoggerType.h"
#include "Badumna/Utils/BasicTypes.h"

namespace BadumnaTestApp
{
    /**
     * PeerConfigOptions class is the base class for all options classes. Options classes are used to configure the 
     * Badumna engine.
     */
    class PeerConfigOptions
    {
    public:
        PeerConfigOptions()
            : options_(new Badumna::Options())
        {
        }

        void SetOptions(ProgramConfig program_config)
        {
            DoSetOptions(program_config);
        }

        virtual ~PeerConfigOptions()
        {
        }

        Badumna::Options* GetOptions() const
        {
            return options_.get();
        }
    
    protected:
        virtual void DoSetOptions(ProgramConfig program_config);

        std::auto_ptr<Badumna::Options> options_;
        DISALLOW_COPY_AND_ASSIGN(PeerConfigOptions);
    };

    void ConfigureDefaultOptions(Badumna::Options &options, ProgramConfig const &program_config);
}

#endif // BADUMNA_AUTO_TEST_PEER_CONFIG_OPTIONS_H