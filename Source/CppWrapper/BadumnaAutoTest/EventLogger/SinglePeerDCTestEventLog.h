//---------------------------------------------------------------------------------
// <copyright file="SinglePeerDCTestEventLog.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_SINGLE_PEER_DC_TEST_EVENT_LOG_H
#define BADUMNA_AUTO_TEST_SINGLE_PEER_DC_TEST_EVENT_LOG_H

#include "Badumna/Utils/BasicTypes.h"
#include "EventLogger/EventLog.h"

namespace BadumnaTestApp
{
    class SinglePeerDCTestEventLog : public EventLog
    {
    public:
        SinglePeerDCTestEventLog()
            : EventLog()
        {
        }

    protected:
        virtual bool DoCheck()
        {
            if(create_replica_counter <= 0)
            {
                std::cerr << "create replica delegate never called." << std::endl;
                return false;
            }

            if(remove_replica_counter <= 0)
            {
                std::cerr << "remove replica delegate never called." << std::endl;
                return false;
            }

            if(deserialize_counter < 2)
            {
                std::cerr << "deserialize #" << std::endl;
                return false;
            }

            return true;
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(SinglePeerDCTestEventLog);
    };
}

#endif // BADUMNA_AUTO_TEST_SINGLE_PEER_DC_TEST_EVENT_LOG_H