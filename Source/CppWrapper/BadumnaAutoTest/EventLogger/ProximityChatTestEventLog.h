//---------------------------------------------------------------------------------
// <copyright file="ProximityChatTestEventLog.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_PROXIMITY_CHAT_TEST_EVENT_LOG_H
#define BADUMNA_AUTO_TEST_PROXIMITY_CHAT_TEST_EVENT_LOG_H

#include "Badumna/Utils/BasicTypes.h"
#include "EventLogger/EventLog.h"

namespace BadumnaTestApp
{
    class ProximityChatTestEventLog : public EventLog
    {
    public:
        ProximityChatTestEventLog()
            : EventLog()
        {
        }

    protected:
        virtual bool DoCheck()
        {
            if(proximity_chat_message_counter < 4)
            {
                std::cerr << "expected 4 (or more?) proximity chat messages, got " << proximity_chat_message_counter << std::endl;
                return false;
            }

            return true;
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(ProximityChatTestEventLog);
    };
}

#endif // BADUMNA_AUTO_TEST_PROXIMITY_CHAT_TEST_EVENT_LOG_H