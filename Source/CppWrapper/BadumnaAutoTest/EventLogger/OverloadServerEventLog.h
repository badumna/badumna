//---------------------------------------------------------------------------------
// <copyright file="OverloadServerEventLog.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_OVERLOAD_SERVER_EVENT_LOG_H
#define BADUMNA_AUTO_TEST_OVERLOAD_SERVER_EVENT_LOG_H

#include <iostream>
#include "Badumna/Utils/BasicTypes.h"
#include "EventLogger/EventLog.h"

namespace BadumnaTestApp
{
    class OverloadServerEventLog : public EventLog
    {
    public:
        OverloadServerEventLog()
            : EventLog()
        {
        }

    protected:
        virtual bool DoCheck()
        {
            if(overload_clients != 2)
            {
                std::cerr << "overload client number #" << std::endl;
                return false;
            }

            return true;
        }
    private:
        DISALLOW_COPY_AND_ASSIGN(OverloadServerEventLog);
    };
}

#endif // BADUMNA_AUTO_TEST_OVERLOAD_SERVER_EVENT_LOG_H