//---------------------------------------------------------------------------------
// <copyright file="PCMeetAndApartTestEventLog.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_PC_MEET_AND_APART_TEST_EVENT_LOG_H
#define BADUMNA_AUTO_TEST_PC_MEET_AND_APART_TEST_EVENT_LOG_H

#include "Badumna/Utils/BasicTypes.h"
#include "EventLogger/EventLog.h"

namespace BadumnaTestApp
{
    class PCMeetAndApartTestEventLog : public EventLog
    {
    public:
        PCMeetAndApartTestEventLog()
            : EventLog()
        {
        }

    protected:
        virtual bool DoCheck()
        {
            if(private_chat_message_counter > 1)
            {
                std::cerr << "private chat message count : " << private_chat_message_counter << std::endl;
                return false;
            }

            if(private_chat_presence_counter > 3)
            {
                std::cerr << "private_chat_presence_counter : " << private_chat_presence_counter << std::endl;
                return false;
            }
            
            return true;
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(PCMeetAndApartTestEventLog);
    };
}

#endif // BADUMNA_AUTO_TEST_PC_MEET_AND_APART_TEST_EVENT_LOG_H