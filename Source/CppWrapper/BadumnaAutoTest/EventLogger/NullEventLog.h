//---------------------------------------------------------------------------------
// <copyright file="NullEventLog.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_NULL_EVENT_LOG_H
#define BADUMNA_AUTO_TEST_NULL_EVENT_LOG_H

#include "Badumna/Utils/BasicTypes.h"
#include "EventLogger/EventLog.h"

namespace BadumnaTestApp
{
    class NullEventLog : public EventLog
    {
    public:
        NullEventLog()
            : EventLog()
        {
        }

    protected:
        virtual bool DoCheck()
        {
            return true;
        }
    private:
        DISALLOW_COPY_AND_ASSIGN(NullEventLog);
    };
}

#endif // BADUMNA_AUTO_TEST_NULL_EVENT_LOG_H