//---------------------------------------------------------------------------------
// <copyright file="EventLog.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_EVENTLOG_H
#define BADUMNA_AUTO_TEST_EVENTLOG_H

namespace BadumnaTestApp
{
    /**
     * EventLog is the base class for all event loggers. Event loggers are used to log observed events so the Check 
     * method can verify whether all expected events happened at the end of the test. 
     */
    class EventLog
    {
    public:
        EventLog()
            : create_replica_counter(0),
            remove_replica_counter(0),
            serialize_counter(0),
            deserialize_counter(0),
            attempt_move_counter(0),
            proximity_chat_message_counter(0),
            replica_event(0),
            original_event(0),
            arbitration_server_message_counter(0),
            private_chat_message_counter(0),
            private_chat_presence_counter(0),
            private_chat_friend_came_online_counter(0),
            counter_update_counter(0),
            consistency_check_failed(false),
            overload_clients(0),
            stream_recieved_request_counter(0),
            stream_completed_operation_counter(0)
        {
        }

        virtual ~EventLog() {}

        void RecordCreateReplicaEvent()
        {
            create_replica_counter++;
        }

        void RecordRemoveReplicaEvent()
        {
            remove_replica_counter++;
        }

        void RecordSerializeEvent()
        {
            serialize_counter++;
        }

        void RecordDeserializeEvent()
        {
            deserialize_counter++;
        }

        void RecordAttemptMoveEvent()
        {
            attempt_move_counter++;
        }

        void RecordProximityMessageEvent()
        {
            proximity_chat_message_counter++;
        }

        void RecordReplicaEvent()
        {
            replica_event++;
        }

        void RecordOriginalEvent()
        {
            original_event++;
        }

        void RecordServerMessageEvent()
        {
            arbitration_server_message_counter++;
        }

        void RecordPrivateChatMessageEvent()
        {
            private_chat_message_counter++;
        }

        void RecordPrivateChatPresenceEvent()
        {
            private_chat_presence_counter++;
        }

        void RecordFriendCameOnlineEvent()
        {
            private_chat_friend_came_online_counter++;
        }

        void RecordCounterUpdateEvent()
        {
            counter_update_counter++;
        }

        void SetConsistencyCheckFailed()
        {
            consistency_check_failed = true;
        }

        void SetOverloadClients(int num)
        {
            if(overload_clients < num)
            {
                overload_clients = num;
            }
        }

        void RecordCompletedStreamOperation()
        {
            stream_completed_operation_counter++;
        }

        void RecordReceivedStreamRequest()
        {
            stream_recieved_request_counter++;
        }

        bool Check()
        {
            return DoCheck();
        }

    protected:
        virtual bool DoCheck() = 0;

        int create_replica_counter;
        int remove_replica_counter;
        int serialize_counter;
        int deserialize_counter;
        int attempt_move_counter;
        int proximity_chat_message_counter;

        int replica_event;
        int original_event;

        int arbitration_server_message_counter;
    
        int private_chat_message_counter;
        int private_chat_presence_counter;
        int private_chat_friend_came_online_counter;

        int counter_update_counter;
        bool consistency_check_failed;
        int overload_clients;

        int stream_recieved_request_counter;
        int stream_completed_operation_counter;

    private:
        DISALLOW_COPY_AND_ASSIGN(EventLog);
    };
}

#define CHECK_COUNT(var, op, expected)      \
    if(!((var) op (expected))) {         \
        std::cerr << "Check FAILED: expected " #var " " #op " " << (expected) << ", but it's " << (var) << std::endl;  \
        return false;               \
    } else {                        \
        std::cerr << "Check OK: " #var " " #op " " << (expected) << std::endl;  \
    }

#endif // BADUMNA_AUTO_TEST_EVENTLOG_H