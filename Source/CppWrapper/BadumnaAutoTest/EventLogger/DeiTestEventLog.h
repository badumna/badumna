//---------------------------------------------------------------------------------
// <copyright file="DeiTestEventLog.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_DEI_TEST_EVENT_LOG_H
#define BADUMNA_AUTO_TEST_DEI_TEST_EVENT_LOG_H

#include "Badumna/Utils/BasicTypes.h"
#include "EventLogger/EventLog.h"

namespace BadumnaTestApp
{
    class DeiTestEventLog : public EventLog
    {
    public:
        DeiTestEventLog()
            : EventLog()
        {
        }

    protected:
        virtual bool DoCheck()
        {
            if(create_replica_counter <= 0)
            {
                std::cerr << "no replica created." << std::endl;
                return false;
            }

            if(counter_update_counter < 12)
            {
                std::cerr << "counter update counter error : " << counter_update_counter << std::endl;
                return false;
            }

            return true;
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(DeiTestEventLog);
    };
}

#endif // BADUMNA_AUTO_TEST_DEI_TEST_EVENT_LOG_H