//---------------------------------------------------------------------------------
// <copyright file="OverloadTestEventLog.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_OVERLOAD_TEST_EVENT_LOG_H
#define BADUMNA_AUTO_TEST_OVERLOAD_TEST_EVENT_LOG_H

#include "Badumna/Utils/BasicTypes.h"
#include "EventLogger/EventLog.h"

namespace BadumnaTestApp
{
    class OverloadTestEventLog : public EventLog
    {
    public:
        OverloadTestEventLog()
            : EventLog()
        {
        }

    protected:
        virtual bool DoCheck()
        {
            if(create_replica_counter <= 0)
            {
                std::cerr << "create replica delegate never called." << std::endl;
                return false;
            }

            if(remove_replica_counter <= 0)
            {
                std::cerr << "remove replica delegate never called." << std::endl;
                return false;
            }

            if(deserialize_counter < 6)
            {
                std::cerr << "deserialize #" << deserialize_counter << std::endl;
                return false;
            }

            if(proximity_chat_message_counter < 6)
            {
                std::cerr << "proximity chat msg #" << proximity_chat_message_counter << std::endl;
                return false;
            }

            if(original_event < 6)
            {
                std::cerr << "original event #" << original_event << std::endl;
                return false;
            }

            return true;
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(OverloadTestEventLog);
    };
}

#endif // BADUMNA_AUTO_TEST_OVERLOAD_TEST_EVENT_LOG_H