//---------------------------------------------------------------------------------
// <copyright file="StreamingSenderTestEventLog.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_STREAMING_SENDER_TEST_EVENT_LOG_H
#define BADUMNA_AUTO_TEST_STREAMING_SENDER_TEST_EVENT_LOG_H

#include "Badumna/Utils/BasicTypes.h"
#include "EventLogger/EventLog.h"

namespace BadumnaTestApp
{
    class StreamingSenderTestEventLog : public EventLog
    {
    public:
        StreamingSenderTestEventLog()
            : EventLog()
        {
        }

    protected:
        virtual bool DoCheck()
        {
            if(create_replica_counter <= 0)
            {
                std::cerr << "create replica delegate never called." << std::endl;
                return false;
            }

            if(stream_completed_operation_counter <= 0)
            {
                std::cerr << "stream completed operation counter : " << stream_completed_operation_counter << std::endl;
                return false;
            }

            return true;
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(StreamingSenderTestEventLog);
    };
}

#endif // BADUMNA_AUTO_TEST_STREAMING_SENDER_TEST_EVENT_LOG_H