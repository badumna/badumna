//---------------------------------------------------------------------------------
// <copyright file="PrivateChatTestEventLog.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_PRIVATE_CHAT_TEST_EVENT_LOG_H
#define BADUMNA_AUTO_TEST_PRIVATE_CHAT_TEST_EVENT_LOG_H

#include "Badumna/Utils/BasicTypes.h"
#include "EventLogger/EventLog.h"

namespace BadumnaTestApp
{
    class PrivateChatTestEventLog : public EventLog
    {
    public:
        PrivateChatTestEventLog()
            : EventLog()
        {
        }

    protected:
        virtual bool DoCheck()
        {
            if(private_chat_message_counter < 3)
            {
                std::cerr << "private chat message count" << std::endl;
                return false;
            }

            if(private_chat_presence_counter < 3)
            {
                std::cerr << "private chat presence count" << std::endl;
                return false;
            }

            return true;
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(PrivateChatTestEventLog);
    };
}

#endif // BADUMNA_AUTO_TEST_PRIVATE_CHAT_TEST_EVENT_LOG_H