//---------------------------------------------------------------------------------
// <copyright file="MultiOriginalReplicationTestEventLog.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_MULTI_ORIGINAL_REPLICATION_TEST_EVENT_LOG_H
#define BADUMNA_AUTO_TEST_MULTI_ORIGINAL_REPLICATION_TEST_EVENT_LOG_H

#include "Badumna/Utils/BasicTypes.h"
#include "EventLogger/EventLog.h"

namespace BadumnaTestApp
{
    class MultiOriginalReplicationTestEventLog : public EventLog
    {
    public:
        MultiOriginalReplicationTestEventLog()
            : EventLog()
        {
        }

    protected:
        virtual bool DoCheck()
        {
            if(create_replica_counter < 3)
            {
                std::cerr << "create replica delegate never called." << create_replica_counter << std::endl;
                return false;
            }

            if(remove_replica_counter < 3)
            {
                std::cerr << "remove replica delegate never called." << create_replica_counter << std::endl;
                return false;
            }

            if(replica_event != 3)
            {
                std::cerr << "replica event : " << replica_event << std::endl;
                return false;
            }

            if(original_event != 3)
            {
                std::cerr << "original event : " << original_event << std::endl;
                return false;
            }

            if(counter_update_counter < 6)
            {
                std::cerr << "counter update counter : " << counter_update_counter << std::endl;
                return false;
            }

            if(proximity_chat_message_counter != 3)
            {
                std::cerr << "expected 3 proximity messages, got " << proximity_chat_message_counter << std::endl;
                return false;
            }

            return true;
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(MultiOriginalReplicationTestEventLog);
    };
}

#endif // BADUMNA_AUTO_TEST_MULTI_ORIGINAL_REPLICATION_TEST_EVENT_LOG_H