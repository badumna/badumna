//---------------------------------------------------------------------------------
// <copyright file="MultiPeersReplicationTestEventLog.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_MULTI_PEERS_REPLICATION_TEST_EVENT_LOG_H
#define BADUMNA_AUTO_TEST_MULTI_PEERS_REPLICATION_TEST_EVENT_LOG_H

#include "Badumna/Utils/BasicTypes.h"
#include "EventLogger/EventLog.h"

namespace BadumnaTestApp
{
    class MultiPeersReplicationTestEventLog : public EventLog
    {
    public:
        MultiPeersReplicationTestEventLog()
            : EventLog()
        {
        }

    protected:
        virtual bool DoCheck()
        {
            if(create_replica_counter < 7)
            {
                std::cerr << "create replica delegate never called." << std::endl;
                return false;
            }

            return true;
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(MultiPeersReplicationTestEventLog);
    };
}

#endif // BADUMNA_AUTO_TEST_MULTI_PEERS_REPLICATION_TEST_EVENT_LOG_H