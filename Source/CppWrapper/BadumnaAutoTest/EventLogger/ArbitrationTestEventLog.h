//---------------------------------------------------------------------------------
// <copyright file="ArbitrationTestEventLog.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_ARBITRATION_TEST_EVENT_LOG_H
#define BADUMNA_AUTO_TEST_ARBITRATION_TEST_EVENT_LOG_H

#include "Badumna/Utils/BasicTypes.h"
#include "EventLogger/EventLog.h"

namespace BadumnaTestApp
{
    class ArbitrationTestEventLog : public EventLog
    {
    public:
        ArbitrationTestEventLog()
            : EventLog()
        {
        }

    protected:
        virtual bool DoCheck()
        {
            if(arbitration_server_message_counter < 3)
            {
                std::cerr << "arbitration server message counter" << std::endl;
                return false;
            }

            return true;
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(ArbitrationTestEventLog);
    };
}

#endif // BADUMNA_AUTO_TEST_ARBITRATION_TEST_EVENT_LOG_H