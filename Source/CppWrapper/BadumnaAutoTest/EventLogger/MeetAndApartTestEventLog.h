//---------------------------------------------------------------------------------
// <copyright file="MeetAndApartTestEventLog.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_MEET_AND_APART_TEST_EVENT_LOG_H
#define BADUMNA_AUTO_TEST_MEET_AND_APART_TEST_EVENT_LOG_H

#include "Badumna/Utils/BasicTypes.h"
#include "EventLogger/EventLog.h"

namespace BadumnaTestApp
{
    class MeetAndApartTestEventLog : public EventLog
    {
    public:
        MeetAndApartTestEventLog()
            : EventLog()
        {
        }

    protected:
        virtual bool DoCheck()
        {
            if(create_replica_counter != 6)
            {
                std::cerr << "create_replica_counter: expected 6, got " << create_replica_counter << std::endl;
                return false;
            }

            if(remove_replica_counter !=6)
            {
                std::cerr << "remove_replica_counter: expected 6, got " << remove_replica_counter << std::endl;
                return false;
            }

            return true;
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(MeetAndApartTestEventLog);
    };
}

#endif // BADUMNA_AUTO_TEST_MEET_AND_APART_TEST_EVENT_LOG_H