//---------------------------------------------------------------------------------
// <copyright file="CustomEventTestEventLog.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_CUSTOM_EVENT_TEST_EVENT_LOG_H
#define BADUMNA_AUTO_TEST_CUSTOM_EVENT_TEST_EVENT_LOG_H

#include "Badumna/Utils/BasicTypes.h"
#include "EventLogger/EventLog.h"

namespace BadumnaTestApp
{
    class CustomEventTestEventLog : public EventLog
    {
    public:
        CustomEventTestEventLog()
            : EventLog()
        {
        }

    protected:
        virtual bool DoCheck()
        {
            if(create_replica_counter <= 0)
            {
                std::cerr << "create replica delegate never called." << std::endl;
                return false;
            }

            if(remove_replica_counter <= 0)
            {
                std::cerr << "remove replica delegate never called." << std::endl;
                return false;
            }

            if(replica_event < 2)
            {
                std::cerr << "replica event" << std::endl;
                return false;
            }

            if(original_event < 2)
            {
                std::cerr << "original event" << std::endl;
                return false;
            }

            return true;
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(CustomEventTestEventLog);
    };
}

#endif // BADUMNA_AUTO_TEST_CUSTOM_EVENT_TEST_EVENT_LOG_H