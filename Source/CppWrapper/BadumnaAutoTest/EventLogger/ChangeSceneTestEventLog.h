//---------------------------------------------------------------------------------
// <copyright file="ChangeSceneTestEventLog.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_CHANGE_SCENE_TEST_EVENT_LOG_H
#define BADUMNA_AUTO_TEST_CHANGE_SCENE_TEST_EVENT_LOG_H

#include "Badumna/Utils/BasicTypes.h"
#include "EventLogger/EventLog.h"

namespace BadumnaTestApp
{
    class ChangeSceneTestEventLog : public EventLog
    {
    public:
        ChangeSceneTestEventLog()
            : EventLog()
        {
        }

    protected:
        virtual bool DoCheck()
        {
            if(counter_update_counter < 7 || counter_update_counter > 11)
            {
                std::cerr << "counter update counter error. # = " << counter_update_counter << std::endl;
                return false;
            }

            if(create_replica_counter < 2)
            {
                std::cerr << "create replica delegate never called : " << create_replica_counter << std::endl;
                return false;
            }

            if(remove_replica_counter < 2)
            {
                std::cerr << "remove replica delegate never called." << remove_replica_counter << std::endl;
                return false;
            }

            return true;
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(ChangeSceneTestEventLog);
    };
}

#endif // BADUMNA_AUTO_TEST_CHANGE_SCENE_TEST_EVENT_LOG_H