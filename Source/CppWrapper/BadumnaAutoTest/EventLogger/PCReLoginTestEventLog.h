//---------------------------------------------------------------------------------
// <copyright file="PCReLoginTestEventLog.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_PC_RE_LOGIN_TEST_EVENT_LOG_H
#define BADUMNA_AUTO_TEST_PC_RE_LOGIN_TEST_EVENT_LOG_H

#include "Badumna/Utils/BasicTypes.h"
#include "EventLogger/EventLog.h"

namespace BadumnaTestApp
{
    class PCReLoginTestEventLog : public EventLog
    {
    public:
        PCReLoginTestEventLog(bool do_relogin)
            : EventLog(),
            do_relogin_(do_relogin)
        {
        }

    protected:
        virtual bool DoCheck()
        {
            int expected_messages = 5;
            int expected_presences = 5;
            int expected_friend_onlines = 1;
            
            if(do_relogin_)
            {
                expected_presences += 1; // When we come back online, the peer will respond with their current status
            }
            else
            {
                expected_friend_onlines += 1;
            }

            CHECK_COUNT(private_chat_message_counter, ==, expected_messages);
            CHECK_COUNT(private_chat_presence_counter, ==, expected_presences);
            CHECK_COUNT(private_chat_friend_came_online_counter, ==, expected_friend_onlines);
            return true;
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(PCReLoginTestEventLog);
        bool do_relogin_;
    };
}

#endif // BADUMNA_AUTO_TEST_PC_RE_LOGIN_TEST_EVENT_LOG_H