//---------------------------------------------------------------------------------
// <copyright file="Program.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <stdexcept>
#include <iostream>
#include <memory>
#include <string>

#include "App/AppConfig.h"
#include "Core/CommandLineOptions.h"
#include "Core/TestRunner.h"
#include "Core/TestRunnerFactory.h"

#ifdef WIN32
#include "Windows.h"
#include "signal.h"

// Custom abort handler to prevent error dialog popup
void __cdecl abort_handler(int)
{
    _exit(3);  // standard 'abort' exit code.
}
#endif



int app_main(int argc, char **argv)
{
#ifdef WIN32
    // Disable Windows Error Reporting dialog, which will hang the test script.
    SetErrorMode(SetErrorMode(0) | SEM_NOGPFAULTERRORBOX);
    signal(SIGABRT, abort_handler);
#endif

    BadumnaTestApp::ProgramConfig config;
    BadumnaTestApp::parse_commandline_options(argc, argv, &config);
    if(config.mode_ == BadumnaTestApp::HelpAndExit)
    {
        return 0;
    }
    else
    {
        // check the comments in TestRunner.cpp to see how each test get constructed, executed and verified. 
        std::auto_ptr<BadumnaTestApp::TestRunner> test(BadumnaTestApp::TestRunnerFactory::Create(config));
        return test->RunTest();
    }
}

int main(int argc, char **argv)
{
    int ret = 1;

    try
    {
        ret = app_main(argc, argv);
    }
    catch(std::invalid_argument &e)
    {
        std::cerr << "invalid argument exception: " << e.what() << std::endl;
        ret = 1;
    }
    catch(std::logic_error &e)
    {
        std::cerr << "logic_error, msg : " << e.what() << std::endl;
        ret = 1;
    }
    catch(std::runtime_error &e)
    {
        std::cerr << "runtime_error, msg : " << e.what() << std::endl;
        ret = 1;
    }
    catch(...)
    {
        std::cerr << "exception caught." << std::endl;
        ret = 1;
    }

    return ret;
}