DeiAccounts.s3db use for testing purposes

List of users:
-----------------------------------------
Username	: admin	
Password	: admin_password
Permission 	: Admin, Create users, 
			  View users, Create permissions, 
			  Assign permissions, View Permissions

Username	: user
Password	: user
Characters	: character
Permission 	: Participation

Username	: seedpeer
Password	: seedpeer
Permission 	: Participation, Announce

Username	: overloadserver
Password	: overloadserver
Permission 	: Participation, Announce

Username	: arbitrationserver
Password	: arbitrationserver
Permission 	: Participation, Announce

