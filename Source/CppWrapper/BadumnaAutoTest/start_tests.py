#!/usr/bin/env python
import os
import os.path
import sys
import subprocess
import time
import random
import unittest
import itertools
import shlex
from datetime import datetime
from shutil import copyfile, rmtree
from contextlib import contextmanager
from pprint import pprint

AUTO_TEST_EXE = os.environ.get('AUTO_TEST_EXE', os.path.join(os.path.dirname(__file__), 'BadumnaAutoTest'))
DEBUG = os.environ.get('DEBUG', 'false') != 'false'
DEBUGGER = os.environ.get('DEBUG_ATTACH', False)
LOG_DIRECTORY="logs"

################################################################################
# test utilities
################################################################################

def save_error_log(name):
	base_name, extension = os.path.splitext(name)
	timestamp = datetime.now().isoformat().replace(':', '_')
	destination_name = base_name + '.' + timestamp + extension
	copyfile(name, destination_name)
	sys.stderr.write("\n  wrote {0}".format(os.path.abspath(destination_name)))
	sys.stderr.flush()

@contextmanager
def output_files(*filenames):
	files = [open(filename, 'w') for filename in filenames]
	try:
		yield files
	except StandardError:
		map(save_error_log, filenames)
		raise
	finally:
		map(lambda f: f.close(), files)

def start_process(args, log):
	popen_args = dict(stdout = log, stderr = subprocess.STDOUT)
	
	if os.name == 'posix' and sys.platform != 'cygwin':
		args = ['/opt/mono/bin/mono'] + args
		popen_args['env'] = {'MONO_XMLSERIALIZER_THS': 'no'}

	if DEBUG:
		print >> sys.stderr, "\nRunning command: %s" % (" ".join(["'%s'" % arg for arg in args]),)

	return subprocess.Popen(args, **popen_args)

@contextmanager
def server_running(args, logname=os.devnull):
	with output_files(logname) as (log,):
		proc = start_process(args, log)
		time.sleep(5)
		try:
			yield
		finally:
			if proc.poll() == None: # process still running
				proc.kill()
				proc.wait()

@contextmanager
def volatile_copy(filename):
	"""
	Copy [filename].in to [filename] for the duration of this block,
	then delete the copy afterwards.
	"""
	source = filename + '.in'
	if DEBUG:
		print "\ncopying {0} -> {1}".format(source, filename)
	copyfile(source, filename)
	try:
		yield
	finally:
		if DEBUG:
			print "\nremoving {0}".format(filename)
		os.remove(filename)

class TestCase(unittest.TestCase):
	"""Shared TestCase functionality"""

	def setUp(self):
		self._appname = str(random.random())
		if TestCase.use_tunnel:
			self._tunnel_log = open(self._log_file_name('tunnel'), 'w')
			self._tunnel_process = start_process([os.path.join('.', 'servers', 'Tunnel'), '--application-name=' + self._appname], self._tunnel_log)

	def tearDown(self):
		if TestCase.use_tunnel:
			self._tunnel_process.kill()
			self._tunnel_log.close()

	@property
	def name(self):
		"""return the test id with the leading module name stripped"""
		return self.id().replace("%s." % (__name__,), '')
	
	def _log_file_name(self, peer_name):
		return os.path.join(LOG_DIRECTORY, "%s_%s.txt" % (self.name, peer_name))

	def _run(self, *arglists):
		with output_files(*[self._log_file_name(i) for i,_ in enumerate(arglists, 1)]) as outputs:
			procs = []
			for args, output in zip(arglists, outputs):
				procs.append(self._run_badumna(args, output))
				time.sleep(1)

			for proc in procs:
				proc.wait()

			for peer_num, proc in enumerate(procs, 1):
				assert proc.returncode == 0, 'peer %s failed (return code = %i)' % (peer_num, proc.returncode)

	def _run_badumna(self, args, output):
		cmd = [AUTO_TEST_EXE] + args + ['--appname', self._appname]
		if TestCase.use_tunnel:
			cmd += ['--tunnel']

		if DEBUGGER:
			debugger = str(DEBUGGER)
			if debugger in ('gdb','ddd'):
				debugger += " --args"
			cmd = shlex.split(debugger) + cmd
			output = sys.stdout
		if DEBUG:
			print >> sys.stderr, "\nRunning command: %s" % (" ".join(["'%s'" % arg for arg in cmd]),)
		
		return subprocess.Popen(cmd, shell = False, stdout = output, stderr = subprocess.STDOUT)

	@contextmanager
	def _dei_server_running(self):
		args = [os.path.join('.', 'servers', 'DeiServer.exe'), '--key-expiry=1', '--verbose']
		with volatile_copy("DeiAccounts.s3db"):
			with server_running(args, self._log_file_name('dei_server')):
				yield


	def __str__(self):
		return self.name

################################################################################
# simple tests
################################################################################

class SimpleTests(TestCase):
	def test_minimum(self):
		self._run(['--minimum_test'])

	def test_login_sequence(self):
		self._run(['--login_sequence_test'])

	def test_create_facade(self):
		self._run(['--create_facade_test'])

	def test_announce_permission(self):
		self._run(['--announce_permission_test'])
	
	def test_single_dc_peer(self):
		self._run(['--single_peer_dc_test'])

	def test_mini_network_scene(self):
		self._run(['--mini_network_scene_test'])


################################################################################
# server tests
################################################################################

class ServerTests(TestCase):
	def test_overload(self):
		server_args = ['--overload_server']
		client_args = ['--overload_test']
		# run two clients with the same args
		self._run(server_args, client_args, client_args)

	def test_character_management(self):
		with self._dei_server_running():
			self._run(['--character_management_test'])
	


################################################################################
# integration tests
################################################################################

class IntegrationTests(TestCase):
	def _run_both_as(self, args):
		self._run(args, args)

	def test_meet_and_apart(self):
		self._run(['--meet_and_apart_test'], ['--meet_and_apart_walker'])

	def test_replication(self):
		self._run_both_as(['--replication_test'])

	def test_proximity_chat(self):
		self._run_both_as(['--proximity_chat_test'])
	
	def test_custom_event(self):
		self._run_both_as(['--custom_event_test'])

	def test_private_chat(self):
		private_chat_args1=['--private_chat_test', 'mary.list']
		private_chat_args2=['--private_chat_test', 'john.list']
		
		self._run(private_chat_args1, private_chat_args2)

	def test_private_chat_relogin(self):
		private_chat_args1=['--private_chat_relogin_test', 'mary.list']
		private_chat_args2=['--do_relogin', '--private_chat_relogin_test', 'john.list']
		
		self._run(private_chat_args1, private_chat_args2)

	def test_private_chat_meet_and_apart(self):
		private_chat_args1=['--pc_meet_and_apart_test', 'mary.list']
		private_chat_args2=['--pc_meet_and_apart_test', 'john.list']
		
		self._run(private_chat_args1, private_chat_args2)
	
	def test_arbitration(self):
		arbitration_args1=['--arbitration_server']
		arbitration_args2=['--arbitration_client']

		self._run(arbitration_args1, arbitration_args2)

	def test_multi_original_replication(self):
		mor_args1=['--multiple_originals_test']
		mor_args2=['--multiple_originals_test']

		self._run(mor_args1, mor_args2)

	def test_change_scene(self):
		change_scene_args1=['--change_scene_sender']
		change_scene_args2=['--change_scene_receiver']

		self._run(change_scene_args1, change_scene_args2)

	def test_long_running_replication(self):
		# disabled by default,
		# uncomment the following line to run it:
		# self._run_both_as(['--long_running_replication_test'])
		return

	def test_simple_streaming(self):
		streaming_args1=['--streaming_sender_test']
		streaming_args2=['--streaming_receiver_test']

		self._run(streaming_args1, streaming_args2)
	
	def test_dei(self):
		dei_test_args1=['--dei_test']
		dei_test_args2=['--dei_test']

		with self._dei_server_running():
			self._run(dei_test_args1, dei_test_args2)

	def test_service_discovery(self):
		sd_test_args1=['--service_discovery_server_test']
		sd_test_args2=['--service_discovery_client_test']

		with self._dei_server_running():
			self._run(sd_test_args1, sd_test_args2)


################################################################################
# multiple peer tests
################################################################################

class MultiPeerTests(TestCase):
	def test_multi_peers_replication(self):
		args = ['--multiple_peers_test']
		arg_lists = [args] * 8
		self._run(*arg_lists)


################################################################################
# generate a Trac/Bitten compatible test report
################################################################################
def generate_xml_test_report(test_results):
	f = open('integration_test_report.xml', 'w')
	f.write('<?xml version=\"1.0\" encoding=\"utf-8\"?>\n')
	f.write('<TestLog>\n')
	f.write('<TestSuite name=\"BadumnaIntegrationTests\">\n')

	for r in test_results:
		f.write('<TestCase name=\"%s\">\n' % r.name)
		if r.failed:
			f.write('<Error file=\"%s\" line=\"-1\">failed</Error>\n' % r.name)

		f.write('<TestingTime>')
		f.write('%d' % r.runtime)
		f.write('</TestingTime>\n')
		f.write('</TestCase>\n')

	f.write('</TestSuite>\n')
	f.write('</TestLog>\n')
	f.close()

def generate_stdout_simple_report(test_results):
	failed_tests=0
	for i in test_results:
		if i.failed:
			failed_tests=failed_tests+1

	msg="Number of tests: "
	msg+=`len(test_results)`
	msg+=", failed: "
	msg+=`failed_tests`
	msg+="."

	print msg

def generate_test_report(test_results):
	generate_stdout_simple_report(test_results)
	generate_xml_test_report(test_results)

################################################################################
# run and perform tests
################################################################################

def perform_test(test):
	print 'Running test: %r ... ' % (test.name),
	sys.stdout.flush()

	time.sleep(2)
	
	start_time = time.time()

	result = unittest.TestResult()
	result.name = test.name
	test.run(result)
	if result.wasSuccessful():
		sys.stdout.write(' Passed ')
		result.failed = False
	else:
		def print_messages(type, messages):
			if len(messages) > 0:
				print type + '\n' + '=' * len(type)
				for message in messages:
					print message[1]

		# A single test can cause both an error and a failure.

		if DEBUG:
			sys.stdout.write(' Failed\n')
			print_messages('Failures', result.failures)
		else:
			if len(result.failures) > 0:
				# we only ran one test!
				assert len(result.failures) == 1
				error = result.failures[0][1] # reports[0] is a tuple of (name, error) but we already know the name
				# extract just the last line of the failure / error report for this test
				error_message = error.splitlines()[-1]
				sys.stdout.write(' Failed: %s ' % (error_message,))

		# Always report error detail
		print_messages('Errors', result.errors)

		result.failed = True

	elapsed = time.time() - start_time
	result.runtime = elapsed

	print "(in %ds)" % (elapsed,)
	return result

def num_failed_tests(results):
	failed_tests=0
	for i in results:
		if i.failed:
			failed_tests += 1
	return failed_tests

def init_logs(remove_old_logs):
	logs_exist = lambda: os.path.exists(LOG_DIRECTORY)
	if remove_old_logs and logs_exist():
		print >> sys.stderr, "cleaning up old log files... (you can prevent this with --keep-logs)"
		sys.stderr.flush()
		rmtree(LOG_DIRECTORY)

	if not logs_exist():
		print "making new logdir..."
		os.makedirs(LOG_DIRECTORY)

def extract_test_cases(suite):
	"""extract leaf tests out of a (potentially nested) test suite"""
	try:
		cases = list(suite)
	except TypeError:
		return [suite]
	else:
		sub_lists = map(extract_test_cases, cases)
		flattened = itertools.chain(*sub_lists)
		return list(flattened)

if __name__ == '__main__':

	from optparse import OptionParser
	p = OptionParser("Usage: %prog [options] [test_id [test_id_2]]")
	p.add_option('-k', '--keep-logs', action='store_true', help="Don't clean up previous logs")
	p.add_option('--debug', action='store_true', default=DEBUG, help="Print more information")
	p.add_option('-l', '--list', action='store_true', default=False, help="list test names")
	p.add_option('--exe', default=AUTO_TEST_EXE, help="BadumnaAutoTests.exe location")
	p.add_option('--attach', default=DEBUG, help="Attach a debugger (e.g 'gdb')")
	p.add_option('--tunnel', action='store_true', default=False, help="run all tests via the HTTP tunnel")
	opts, args = p.parse_args()

	DEBUG = opts.debug
	DEBUGGER = opts.attach
	AUTO_TEST_EXE = os.path.abspath(opts.exe)

	# run all tests from the folder this script is in
	os.chdir(os.path.dirname(os.path.abspath(__file__)))

	module = sys.modules['__main__']

	if len(args) > 0:
		tests = args
		suite = unittest.defaultTestLoader.loadTestsFromNames(tests, module)
	else:
		suite = unittest.defaultTestLoader.loadTestsFromModule(module)
	test_cases = extract_test_cases(suite)

	if opts.list:
		print "\n".join(sorted([test.name for test in test_cases]))
		sys.exit(0)

	init_logs(remove_old_logs = not opts.keep_logs)
	
	TestCase.use_tunnel = opts.tunnel
	
	results = map(perform_test, test_cases)
	generate_test_report(results)

	sys.exit(num_failed_tests(results))
