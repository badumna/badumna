//---------------------------------------------------------------------------------
// <copyright file="OverloadServerAutoConsole.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_OVERLOAD_SERVER_AUTO_CONSOLE_H
#define BADUMNA_AUTO_TEST_OVERLOAD_SERVER_AUTO_CONSOLE_H

#include "Badumna/Utils/BasicTypes.h"
#include "Console/AutoTestConsole.h"

namespace BadumnaTestApp
{
    class OverloadServerAutoConsole : public AutoTestConsole
    {
    public:
        OverloadServerAutoConsole()
            : AutoTestConsole()
        {
        }

        void RecordCommands()
        {
            RecordCommand(180000, BadumnaTestApp::EXIT_COMMAND);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(OverloadServerAutoConsole);
    };
}

#endif // BADUMNA_AUTO_TEST_OVERLOAD_SERVER_AUTO_CONSOLE_H