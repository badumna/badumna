//---------------------------------------------------------------------------------
// <copyright file="ArbitrationClientTestAutoConsole.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_ARBITRATION_CLIENT_TEST_AUTO_CONSOLE_H
#define BADUMNA_AUTO_TEST_ARBITRATION_CLIENT_TEST_AUTO_CONSOLE_H

#include "Badumna/Utils/BasicTypes.h"
#include "Console/AutoTestConsole.h"

namespace BadumnaTestApp
{
    class ArbitrationClientTestAutoConsole : public AutoTestConsole
    {
    public:
        ArbitrationClientTestAutoConsole()
            : AutoTestConsole()
        {
        }

        void RecordCommands()
        {
            RecordCommand(5000, BadumnaTestApp::SEND_ARBITRATION_EVENT_COMMAND);
            RecordCommand(6000, BadumnaTestApp::SEND_ARBITRATION_EVENT_COMMAND);
            RecordCommand(7000, BadumnaTestApp::SEND_ARBITRATION_EVENT_COMMAND);
            RecordCommand(8000, BadumnaTestApp::SEND_ARBITRATION_EVENT_COMMAND);
            RecordCommand(9000, BadumnaTestApp::SEND_ARBITRATION_EVENT_COMMAND);

            // quit
            RecordCommand(15000, BadumnaTestApp::EXIT_COMMAND);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(ArbitrationClientTestAutoConsole);
    };
}

#endif // BADUMNA_AUTO_TEST_ARBITRATION_CLIENT_TEST_AUTO_CONSOLE_H