//---------------------------------------------------------------------------------
// <copyright file="PrivateChatTestAutoConsole.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_PC_RE_LOGIN_TEST_AUTO_CONSOLE_H
#define BADUMNA_AUTO_TEST_PC_RE_LOGIN_TEST_AUTO_CONSOLE_H

#include "Badumna/Utils/BasicTypes.h"
#include "Console/AutoTestConsole.h"

namespace BadumnaTestApp
{
    class PCReLoginTestAutoConsole : public AutoTestConsole
    {
    public:
        PCReLoginTestAutoConsole(bool do_relogin)
            : AutoTestConsole(),
            do_relogin_(do_relogin)
        {
        }

        void RecordCommands()
        {
            RecordCommand(5000, BadumnaTestApp::SEND_PRIVATE_CHAT_COMMAND);
            RecordCommand(6000, BadumnaTestApp::CHANGE_PRESENCE_COMMAND);
            RecordCommand(7000, BadumnaTestApp::SEND_PRIVATE_CHAT_COMMAND);
            RecordCommand(8000, BadumnaTestApp::CHANGE_PRESENCE_COMMAND);
            if(do_relogin_)
            {
                RecordCommand(10000, BadumnaTestApp::RE_LOGIN);
            }
            RecordCommand(32000, BadumnaTestApp::SEND_PRIVATE_CHAT_COMMAND);
            RecordCommand(33000, BadumnaTestApp::CHANGE_PRESENCE_COMMAND);
            RecordCommand(34000, BadumnaTestApp::SEND_PRIVATE_CHAT_COMMAND);
            RecordCommand(35000, BadumnaTestApp::CHANGE_PRESENCE_COMMAND);
            RecordCommand(36000, BadumnaTestApp::SEND_PRIVATE_CHAT_COMMAND);
            RecordCommand(37000, BadumnaTestApp::CHANGE_PRESENCE_COMMAND);

            // quit
            RecordCommand(44000, BadumnaTestApp::EXIT_COMMAND);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(PCReLoginTestAutoConsole);
        bool do_relogin_;
    };
}

#endif // BADUMNA_AUTO_TEST_PC_RE_LOGIN_TEST_AUTO_CONSOLE_H