//---------------------------------------------------------------------------------
// <copyright file="ChangeSceneSenderAutoConsole.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_CHANGE_SCENE_SENDER_AUTO_CONSOLE_H
#define BADUMNA_AUTO_TEST_CHANGE_SCENE_SENDER_AUTO_CONSOLE_H

#include "Badumna/Utils/BasicTypes.h"
#include "Console/AutoTestConsole.h"

namespace BadumnaTestApp
{
    class ChangeSceneSenderAutoConsole : public AutoTestConsole
    {
    public:
        ChangeSceneSenderAutoConsole()
            : AutoTestConsole()
        {
        }

        void RecordCommands()
        {
            // replication
            for(size_t i = 0; i < 13; i++)
            {
                int t = i * 5000 + 10000;
                RecordCommand(t, BadumnaTestApp::INCREASE_COUNTER_COMMAND);
            }

            // quit
            RecordCommand(80000, BadumnaTestApp::EXIT_COMMAND);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(ChangeSceneSenderAutoConsole);
    };
}

#endif // BADUMNA_AUTO_TEST_CHANGE_SCENE_SENDER_AUTO_CONSOLE_H