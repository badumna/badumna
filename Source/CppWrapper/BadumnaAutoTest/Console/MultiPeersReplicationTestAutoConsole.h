//---------------------------------------------------------------------------------
// <copyright file="MultiPeersReplicationTestAutoConsole.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_MULTI_PEERS_REPLICATION_TEST_AUTO_CONSOLE_H
#define BADUMNA_AUTO_TEST_MULTI_PEERS_REPLICATION_TEST_AUTO_CONSOLE_H

#include "Badumna/Utils/BasicTypes.h"
#include "Console/AutoTestConsole.h"

namespace BadumnaTestApp
{
    class MultiPeersReplicationTestAutoConsole : public AutoTestConsole
    {
    public:
        MultiPeersReplicationTestAutoConsole()
            : AutoTestConsole()
        {
        }

        void RecordCommands()
        {
            // replication
            RecordCommand(40000, BadumnaTestApp::INCREASE_COUNTER_COMMAND);
            // quit
            RecordCommand(50000, BadumnaTestApp::EXIT_COMMAND);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(MultiPeersReplicationTestAutoConsole);
    };
}

#endif // BADUMNA_AUTO_TEST_MULTI_PEERS_REPLICATION_TEST_AUTO_CONSOLE_H