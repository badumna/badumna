//---------------------------------------------------------------------------------
// <copyright file="PrivateChatTestAutoConsole.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_PRIVATE_CHAT_TEST_AUTO_CONSOLE_H
#define BADUMNA_AUTO_TEST_PRIVATE_CHAT_TEST_AUTO_CONSOLE_H

#include "Badumna/Utils/BasicTypes.h"
#include "Console/AutoTestConsole.h"

namespace BadumnaTestApp
{
    class PrivateChatTestAutoConsole : public AutoTestConsole
    {
    public:
        PrivateChatTestAutoConsole()
            : AutoTestConsole()
        {
        }

        void RecordCommands()
        {
            RecordCommand(5000, BadumnaTestApp::SEND_PRIVATE_CHAT_COMMAND);
            RecordCommand(6000, BadumnaTestApp::CHANGE_PRESENCE_COMMAND);
            RecordCommand(7000, BadumnaTestApp::SEND_PRIVATE_CHAT_COMMAND);
            RecordCommand(8000, BadumnaTestApp::CHANGE_PRESENCE_COMMAND);
            RecordCommand(9000, BadumnaTestApp::SEND_PRIVATE_CHAT_COMMAND);
            RecordCommand(10000, BadumnaTestApp::CHANGE_PRESENCE_COMMAND);
            RecordCommand(11000, BadumnaTestApp::SEND_PRIVATE_CHAT_COMMAND);
            RecordCommand(12000, BadumnaTestApp::CHANGE_PRESENCE_COMMAND);
            RecordCommand(13000, BadumnaTestApp::SEND_PRIVATE_CHAT_COMMAND);
            RecordCommand(14000, BadumnaTestApp::CHANGE_PRESENCE_COMMAND);

            // quit
            RecordCommand(24000, BadumnaTestApp::EXIT_COMMAND);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(PrivateChatTestAutoConsole);
    };
}

#endif // BADUMNA_AUTO_TEST_PRIVATE_CHAT_TEST_AUTO_CONSOLE_H
