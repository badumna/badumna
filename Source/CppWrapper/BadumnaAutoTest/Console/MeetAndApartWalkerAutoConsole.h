//---------------------------------------------------------------------------------
// <copyright file="MeetAndApartWalkerAutoConsole.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_MEET_AND_APART_WALKER_AUTO_CONSOLE_H
#define BADUMNA_AUTO_TEST_MEET_AND_APART_WALKER_AUTO_CONSOLE_H

#include "Badumna/Utils/BasicTypes.h"
#include "Console/AutoTestConsole.h"

namespace BadumnaTestApp
{
    class MeetAndApartWalkerAutoConsole : public AutoTestConsole
    {
    public:
        MeetAndApartWalkerAutoConsole()
            : AutoTestConsole()
        {
        }

        void RecordCommands()
        {
            int t = 5000;
            for(size_t iteration = 0; iteration < 5; iteration++)
            {
                // move right
                for(size_t step = 0; step < 5; step++)
                {
                    RecordCommand(t, BadumnaTestApp::MOVE_RIGHT_FIX_DISTANCE);
                    t += 1000; 
                }

                // move left
                for(size_t step = 0; step < 5; step++)
                {
                    RecordCommand(t, BadumnaTestApp::MOVE_LEFT_FIX_DISTANCE);
                    t += 1000; 
                }
            }

            // quit
            RecordCommand(65000, BadumnaTestApp::EXIT_COMMAND);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(MeetAndApartWalkerAutoConsole);
    };
}

#endif // BADUMNA_AUTO_TEST_MEET_AND_APART_WALKER_AUTO_CONSOLE_H