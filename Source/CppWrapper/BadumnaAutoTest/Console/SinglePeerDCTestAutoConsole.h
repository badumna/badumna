//---------------------------------------------------------------------------------
// <copyright file="SinglePeerDCTestAutoConsole.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_SINGLE_PEER_DC_TEST_AUTO_CONSOLE_H
#define BADUMNA_AUTO_TEST_SINGLE_PEER_DC_TEST_AUTO_CONSOLE_H

#include "Badumna/Utils/BasicTypes.h"
#include "Console/AutoTestConsole.h"

namespace BadumnaTestApp
{
    class SinglePeerDCTestAutoConsole : public AutoTestConsole
    {
    public:
        SinglePeerDCTestAutoConsole()
            : AutoTestConsole()
        {
        }

        void RecordCommands()
        {
            // quit
            RecordCommand(60000, BadumnaTestApp::EXIT_COMMAND);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(SinglePeerDCTestAutoConsole);
    };
}

#endif // BADUMNA_AUTO_TEST_SINGLE_PEER_DC_TEST_AUTO_CONSOLE_H