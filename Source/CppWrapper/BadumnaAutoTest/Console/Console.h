//---------------------------------------------------------------------------------
// <copyright file="Console.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_EXAMPLE_CONSOLE_H
#define BADUMNA_EXAMPLE_CONSOLE_H

#include <boost/thread.hpp>
#include <queue>

#include "Badumna/Utils/BasicTypes.h"
#include "IConsole.h"

namespace BadumnaTestApp
{   
    typedef int InputCommand;

    class Console : public IConsole
    {
    public:
        Console();
        ~Console();

        void Start();
        void Shutdown();
        int GetCommand(int milliseconds_since_start);
    private:
        void ThreadMain();
        void AddCommand(InputCommand c);

        bool time_to_exit;
        
        boost::shared_ptr<boost::thread> thread;
        boost::mutex mutex;

        std::queue<InputCommand> commands;

        DISALLOW_COPY_AND_ASSIGN(Console);
    };
}

#endif // BADUMNA_EXAMPLE_CONSOLE_H