//---------------------------------------------------------------------------------
// <copyright file="MeetAndApartTestAutoConsole.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_MEET_AND_APART_TEST_AUTO_CONSOLE_H
#define BADUMNA_AUTO_TEST_MEET_AND_APART_TEST_AUTO_CONSOLE_H

#include "Badumna/Utils/BasicTypes.h"
#include "Console/AutoTestConsole.h"

namespace BadumnaTestApp
{
    class MeetAndApartTestAutoConsole : public AutoTestConsole
    {
    public:
        MeetAndApartTestAutoConsole()
            : AutoTestConsole()
        {
        }

        void RecordCommands()
        {
            // quit
            RecordCommand(65000, BadumnaTestApp::EXIT_COMMAND);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(MeetAndApartTestAutoConsole);
    };
}

#endif // BADUMNA_AUTO_TEST_MEET_AND_APART_TEST_AUTO_CONSOLE_H