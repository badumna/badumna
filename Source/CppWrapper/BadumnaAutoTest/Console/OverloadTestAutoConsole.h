//---------------------------------------------------------------------------------
// <copyright file="OverloadTestAutoConsole.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_OVERLOAD_TEST_AUTO_CONSOLE_H
#define BADUMNA_AUTO_TEST_OVERLOAD_TEST_AUTO_CONSOLE_H

#include "Badumna/Utils/BasicTypes.h"
#include "Console/AutoTestConsole.h"

namespace BadumnaTestApp
{
    class OverloadTestAutoConsole : public AutoTestConsole
    {
    public:
        OverloadTestAutoConsole()
            : AutoTestConsole()
        {
        }

        void RecordCommands()
        {
            // replication
            RecordCommand(5000, BadumnaTestApp::INCREASE_COUNTER_COMMAND);
            RecordCommand(7000, BadumnaTestApp::SEND_PROXIMITY_CHAT_COMMAND);
            RecordCommand(8000, BadumnaTestApp::SEND_EVENT_TO_ORIGINAL);
            
            RecordCommand(15000, BadumnaTestApp::INCREASE_COUNTER_COMMAND);
            RecordCommand(17000, BadumnaTestApp::SEND_PROXIMITY_CHAT_COMMAND);
            RecordCommand(18000, BadumnaTestApp::SEND_EVENT_TO_ORIGINAL);
            
            RecordCommand(25000, BadumnaTestApp::INCREASE_COUNTER_COMMAND);
            RecordCommand(27000, BadumnaTestApp::SEND_PROXIMITY_CHAT_COMMAND);
            RecordCommand(28000, BadumnaTestApp::SEND_EVENT_TO_ORIGINAL);

            RecordCommand(35000, BadumnaTestApp::INCREASE_COUNTER_COMMAND);
            RecordCommand(37000, BadumnaTestApp::SEND_PROXIMITY_CHAT_COMMAND);
            RecordCommand(38000, BadumnaTestApp::SEND_EVENT_TO_ORIGINAL);
            
            RecordCommand(45000, BadumnaTestApp::INCREASE_COUNTER_COMMAND);
            RecordCommand(47000, BadumnaTestApp::SEND_PROXIMITY_CHAT_COMMAND);
            RecordCommand(48000, BadumnaTestApp::SEND_EVENT_TO_ORIGINAL);

            RecordCommand(55000, BadumnaTestApp::INCREASE_COUNTER_COMMAND);
            RecordCommand(57000, BadumnaTestApp::SEND_PROXIMITY_CHAT_COMMAND);
            RecordCommand(58000, BadumnaTestApp::SEND_EVENT_TO_ORIGINAL);

            RecordCommand(65000, BadumnaTestApp::INCREASE_COUNTER_COMMAND);
            RecordCommand(67000, BadumnaTestApp::SEND_PROXIMITY_CHAT_COMMAND);
            RecordCommand(68000, BadumnaTestApp::SEND_EVENT_TO_ORIGINAL);

            // quit
            RecordCommand(79000, BadumnaTestApp::EXIT_COMMAND);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(OverloadTestAutoConsole);
    };
}

#endif // BADUMNA_AUTO_TEST_OVERLOAD_TEST_AUTO_CONSOLE_H