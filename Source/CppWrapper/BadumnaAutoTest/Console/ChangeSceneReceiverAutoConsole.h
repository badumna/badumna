//---------------------------------------------------------------------------------
// <copyright file="ChangeSceneReceiverAutoConsole.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_CHANGE_SCENE_RECEIVER_AUTO_CONSOLE_H
#define BADUMNA_AUTO_TEST_CHANGE_SCENE_RECEIVER_AUTO_CONSOLE_H

#include "Badumna/Utils/BasicTypes.h"
#include "Console/AutoTestConsole.h"

namespace BadumnaTestApp
{
    class ChangeSceneReceiverAutoConsole : public AutoTestConsole
    {
    public:
        ChangeSceneReceiverAutoConsole()
            : AutoTestConsole()
        {
        }

        void RecordCommands()
        {
            // replication
            RecordCommand(25000, BadumnaTestApp::CHANGE_SCENE_COMMAND);
            RecordCommand(50000, BadumnaTestApp::CHANGE_SCENE_COMMAND);
            
            // quit
            RecordCommand(80000, BadumnaTestApp::EXIT_COMMAND);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(ChangeSceneReceiverAutoConsole);
    };
}

#endif // BADUMNA_AUTO_TEST_CHANGE_SCENE_RECEIVER_AUTO_CONSOLE_H