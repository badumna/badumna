//---------------------------------------------------------------------------------
// <copyright file="Console.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <iostream>
#include "Console.h"

#ifdef WIN32
 #include <conio.h>
 #include <windows.h>
#else
 #include <cstdio>
 #include <cstdlib>
 #include <termios.h>

static struct termios oldt;

void restore_terminal_settings(void)
{
    tcsetattr(0, TCSANOW, &oldt);
}

void disable_waiting_for_enter(void)
{
    struct termios newt;
    tcgetattr(0, &oldt); 
    newt = oldt;  
    newt.c_lflag &= ~(ICANON | ECHO);  
    tcsetattr(0, TCSANOW, &newt);  
    atexit(restore_terminal_settings);
}
#endif


namespace
{
    int GetCharFromConsole()
    {
#ifdef WIN32
        return _getch();
#else
        int ch;
        disable_waiting_for_enter();
        ch = getchar();
        restore_terminal_settings();
        return ch;
#endif
    }
}

namespace BadumnaTestApp
{
    Console::Console() 
        : time_to_exit(false),
        thread(),
        mutex(),
        commands()
    {
    }

    Console::~Console()
    {
    }

    void Console::Start()
    {
        thread.reset(new boost::thread(boost::bind(&Console::ThreadMain, this)));
    }
    
    void Console::Shutdown()
    {
        time_to_exit = true;
        thread->join();
    }

    int Console::GetCommand(int /*milliseconds_since_start*/)
    {
        boost::mutex::scoped_lock lock(mutex);

        if(commands.size() > 0)
        {
            int c = commands.front();
            commands.pop();
            return c;
        }

        return NO_MORE_COMMAND;
    }
    
    void Console::ThreadMain()
    {
        while(!time_to_exit)
        {
            int c = ::GetCharFromConsole();
            if(c == EXIT_COMMAND ||
               c == MOVE_UP_COMMAND ||
               c == MOVE_DOWN_COMMAND ||
               c == MOVE_LEFT_COMMAND ||
               c == MOVE_RIGHT_COMMAND ||
               c == INCREASE_COUNTER_COMMAND || 
               c == NO_MORE_COMMAND ||
               c == SEND_ARBITRATION_EVENT_COMMAND ||
               c == SEND_PROXIMITY_CHAT_COMMAND ||
               c == SEND_PRIVATE_CHAT_COMMAND ||
               c == CHANGE_PRESENCE_COMMAND || 
               c == SHUTDOWN_TEST_COMMAND ||
               c == SEND_EVENT_TO_REMOTE_COPIES ||
               c == SEND_EVENT_TO_ORIGINAL)
            {
                AddCommand(c);
            }

            if(c == EXIT_COMMAND)
            {
                return;
            }
        }
    }

    void Console::AddCommand(InputCommand c)
    {
        boost::mutex::scoped_lock lock(mutex);
        commands.push(c);
    }
}