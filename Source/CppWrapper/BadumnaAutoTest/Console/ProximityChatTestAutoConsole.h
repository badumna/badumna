//---------------------------------------------------------------------------------
// <copyright file="ProximityChatTestAutoConsole.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_PROXIMITY_CHAT_TEST_AUTO_CONSOLE_H
#define BADUMNA_AUTO_TEST_PROXIMITY_CHAT_TEST_AUTO_CONSOLE_H

#include "Badumna/Utils/BasicTypes.h"
#include "Console/AutoTestConsole.h"

namespace BadumnaTestApp
{
    class ProximityChatTestAutoConsole : public AutoTestConsole
    {
    public:
        ProximityChatTestAutoConsole()
            : AutoTestConsole()
        {
        }

        void RecordCommands()
        {
            RecordCommand(5000, BadumnaTestApp::SEND_PROXIMITY_CHAT_COMMAND);
            RecordCommand(6000, BadumnaTestApp::SEND_PROXIMITY_CHAT_COMMAND);
            RecordCommand(7000, BadumnaTestApp::SEND_PROXIMITY_CHAT_COMMAND);
            RecordCommand(8000, BadumnaTestApp::SEND_PROXIMITY_CHAT_COMMAND);

            RecordCommand(15000, BadumnaTestApp::EXIT_COMMAND);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(ProximityChatTestAutoConsole);
    };
}

#endif // BADUMNA_AUTO_TEST_PROXIMITY_CHAT_TEST_AUTO_CONSOLE_H