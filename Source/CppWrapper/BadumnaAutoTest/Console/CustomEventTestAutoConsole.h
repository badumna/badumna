//---------------------------------------------------------------------------------
// <copyright file="CustomEventTestAutoConsole.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_CUSTOM_EVENT_TEST_AUTO_CONSOLE_H
#define BADUMNA_AUTO_TEST_CUSTOM_EVENT_TEST_AUTO_CONSOLE_H

#include "Badumna/Utils/BasicTypes.h"
#include "Console/AutoTestConsole.h"

namespace BadumnaTestApp
{
    class CustomEventTestAutoConsole : public AutoTestConsole
    {
    public:
        CustomEventTestAutoConsole()
            : AutoTestConsole()
        {
        }

        void RecordCommands()
        {
            RecordCommand(5000, BadumnaTestApp::SEND_EVENT_TO_REMOTE_COPIES);
            RecordCommand(6000, BadumnaTestApp::SEND_EVENT_TO_REMOTE_COPIES);
            RecordCommand(7000, BadumnaTestApp::SEND_EVENT_TO_REMOTE_COPIES);
            RecordCommand(8000, BadumnaTestApp::SEND_EVENT_TO_ORIGINAL);
            RecordCommand(9000, BadumnaTestApp::SEND_EVENT_TO_ORIGINAL);
            RecordCommand(10000, BadumnaTestApp::SEND_EVENT_TO_ORIGINAL);

            RecordCommand(15000, BadumnaTestApp::EXIT_COMMAND);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(CustomEventTestAutoConsole);
    };
}

#endif // BADUMNA_AUTO_TEST_CUSTOM_EVENT_TEST_AUTO_CONSOLE_H