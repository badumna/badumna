//---------------------------------------------------------------------------------
// <copyright file="AutoTestConsole.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <cassert>
#include <iostream>
#include "AutoTestConsole.h"

namespace BadumnaTestApp
{
    using std::pair;

    AutoTestConsole::AutoTestConsole()
        : commands_(),
        ready_time_(0),
        is_ready_(false)
    {
    }

    AutoTestConsole::~AutoTestConsole()
    {
    }

    void AutoTestConsole::Start()
    {
        RecordCommands();
    }

    void AutoTestConsole::Shutdown()
    {
        if(!commands_.empty())
        {
            std::cerr << "there are still " << commands_.size() << " cmds in the queue." << std::endl;
        }
    }

    int AutoTestConsole::GetCommand(int milliseconds_since_start)
    {
        if(!is_ready_)
        {
            return NO_MORE_COMMAND;
        }

        pair<int, InputCommand> element = commands_.front();
        if((element.first + ready_time_) < milliseconds_since_start)
        {
            commands_.pop();
            return element.second;
        }

        return NO_MORE_COMMAND;
    }

    void AutoTestConsole::RecordCommand(int milliseconds_since_start, InputCommand cmd)
    {
        if(!commands_.empty())
        {
            pair<int, InputCommand> element = commands_.front();
            assert(milliseconds_since_start >= element.first);
        }

        commands_.push(std::make_pair(milliseconds_since_start, cmd));
    }

    void AutoTestConsole::SetAsReady(int milliseconds_since_start)
    {
        if(!is_ready_)
        {
            std::cout << "Going to set the console as ready. time (ms) is " << milliseconds_since_start << std::endl;
            is_ready_ = true;
            ready_time_ = milliseconds_since_start;
        }
    }
}