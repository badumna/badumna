//---------------------------------------------------------------------------------
// <copyright file="MultiOriginalReplicationTestAutoConsole.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_MULTI_ORIGINAL_REPLICATION_TEST_AUTO_CONSOLE_H
#define BADUMNA_AUTO_TEST_MULTI_ORIGINAL_REPLICATION_TEST_AUTO_CONSOLE_H

#include "Badumna/Utils/BasicTypes.h"
#include "Console/AutoTestConsole.h"

namespace BadumnaTestApp
{
    class MultiOriginalReplicationTestAutoConsole : public AutoTestConsole
    {
    public:
        MultiOriginalReplicationTestAutoConsole()
            : AutoTestConsole()
        {
        }

        void RecordCommands()
        {
            // replication
            RecordCommand(5000, BadumnaTestApp::INCREASE_COUNTER_COMMAND);
            RecordCommand(10000, BadumnaTestApp::INCREASE_COUNTER_COMMAND);
            RecordCommand(15000, BadumnaTestApp::SEND_EVENT_TO_REMOTE_COPIES);
            RecordCommand(20000, BadumnaTestApp::SEND_EVENT_TO_ORIGINAL);
            RecordCommand(25000, BadumnaTestApp::SEND_PROXIMITY_CHAT_COMMAND);

            // quit
            RecordCommand(35000, BadumnaTestApp::EXIT_COMMAND);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(MultiOriginalReplicationTestAutoConsole);
    };
}

#endif // BADUMNA_AUTO_TEST_MULTI_ORIGINAL_REPLICATION_TEST_AUTO_CONSOLE_H