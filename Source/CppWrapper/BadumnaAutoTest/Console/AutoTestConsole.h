//---------------------------------------------------------------------------------
// <copyright file="AutoTestConsole.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_AUTO_TEST_CONSOLE_H
#define BADUMNA_AUTO_TEST_AUTO_TEST_CONSOLE_H

#include <queue>

#include "Badumna/Utils/BasicTypes.h"
#include "IConsole.h"

namespace BadumnaTestApp
{   
    typedef int InputCommand;

    /**
     * AutoTestConsole is the base class of all auto test console classes. Auto test console classes record a list of 
     * commands and its timestamps, these commands will be replayed at or after their specified time when GetCommand is
     * called. Commands are usually recorded in the constructor. 
     */ 
    class AutoTestConsole : public IConsole
    {
    public:
        AutoTestConsole();
        virtual ~AutoTestConsole();

        void Start();
        void Shutdown();
        int GetCommand(int milliseconds_since_start);

        void SetAsReady(int milliseconds_since_start);

    protected:
        virtual void RecordCommands() = 0;
        void RecordCommand(int milliseconds_since_start, InputCommand cmd);
        std::queue<std::pair<int, InputCommand> > commands_;

        int ready_time_;
        bool is_ready_;

    private:
        DISALLOW_COPY_AND_ASSIGN(AutoTestConsole);
    };
}

#endif // BADUMNA_AUTO_TEST_AUTO_TEST_CONSOLE_H