//---------------------------------------------------------------------------------
// <copyright file="StreamingSenderAutoConsole.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_STREAMING_SENDER_AUTO_CONSOLE_H
#define BADUMNA_AUTO_TEST_STREAMING_SENDER_AUTO_CONSOLE_H

#include "Badumna/Utils/BasicTypes.h"
#include "Console/AutoTestConsole.h"

namespace BadumnaTestApp
{
    class StreamingSenderAutoConsole : public AutoTestConsole
    {
    public:
        StreamingSenderAutoConsole()
            : AutoTestConsole()
        {
        }

        void RecordCommands()
        {
            // replication
            RecordCommand(10000, BadumnaTestApp::STREAMING_SEND_COMMAND);
            
            // quit
            RecordCommand(20000, BadumnaTestApp::EXIT_COMMAND);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(StreamingSenderAutoConsole);
    };
}

#endif // BADUMNA_AUTO_TEST_STREAMING_SENDER_AUTO_CONSOLE_H