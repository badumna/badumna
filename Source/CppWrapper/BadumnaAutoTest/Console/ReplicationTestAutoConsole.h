//---------------------------------------------------------------------------------
// <copyright file="ReplicationTestAutoConsole.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_REPLICATION_TEST_AUTO_CONSOLE_H
#define BADUMNA_AUTO_TEST_REPLICATION_TEST_AUTO_CONSOLE_H

#include "Badumna/Utils/BasicTypes.h"
#include "Console/AutoTestConsole.h"

namespace BadumnaTestApp
{
    class ReplicationTestAutoConsole : public AutoTestConsole
    {
    public:
        ReplicationTestAutoConsole()
            : AutoTestConsole()
        {
        }

        void RecordCommands()
        {
            // replication
            RecordCommand(5000, BadumnaTestApp::INCREASE_COUNTER_COMMAND);
            RecordCommand(6000, BadumnaTestApp::RANDOM_MOVE_COMMAND);
            RecordCommand(7000, BadumnaTestApp::INCREASE_COUNTER_COMMAND);
            RecordCommand(8000, BadumnaTestApp::INCREASE_COUNTER_COMMAND);
            RecordCommand(14000, BadumnaTestApp::CONSISTENCY_CHECK_COMMAND);

            // quit
            RecordCommand(24000, BadumnaTestApp::EXIT_COMMAND);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(ReplicationTestAutoConsole);
    };
}

#endif // BADUMNA_AUTO_TEST_REPLICATION_TEST_AUTO_CONSOLE_H