//---------------------------------------------------------------------------------
// <copyright file="LRRTestAutoConsole.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_LRR_TEST_AUTO_CONSOLE_H
#define BADUMNA_AUTO_TEST_LRR_TEST_AUTO_CONSOLE_H

#include "Badumna/Utils/BasicTypes.h"
#include "Console/AutoTestConsole.h"

namespace BadumnaTestApp
{
    class LRRTestAutoConsole : public AutoTestConsole
    {
    public:
        LRRTestAutoConsole()
            : AutoTestConsole()
        {
        }

        void RecordCommands()
        {
            int time = 5000;
            for(int i = 0; i < iteration; i++)
            {
                // keep moving in a loop
                for(int r = 0; r < 5; r++)
                {
                    AddCommand(&time, BadumnaTestApp::MOVE_RIGHT_COMMAND);
                }
                
                if(i > 20000)
                {
                    AddCommand(time, BadumnaTestApp::SEND_EVENT_TO_REMOTE_COPIES);
                }

                for(int r = 0; r < 5; r++)
                {
                    AddCommand(&time, BadumnaTestApp::MOVE_DOWN_COMMAND);
                }

                if(i > 20000)
                {
                    AddCommand(time, BadumnaTestApp::SEND_EVENT_TO_ORIGINAL);
                }
                
                for(int r = 0; r < 5; r++)
                {
                    AddCommand(&time, BadumnaTestApp::MOVE_LEFT_COMMAND);
                }
                
                if(i > 20000)
                {
                    AddCommand(time, BadumnaTestApp::SEND_PROXIMITY_CHAT_COMMAND);
                }

                for(int r = 0; r < 5; r++)
                {
                    AddCommand(&time, BadumnaTestApp::MOVE_UP_COMMAND);
                }
            }
            
            // quit
            RecordCommand(time + 10000, BadumnaTestApp::EXIT_COMMAND);
        }

        void AddCommand(int *time, InputCommand c)
        {
            RecordCommand(*time, c);
            *time = *time + 500;
        }

        void AddCommand(int time, InputCommand c)
        {
            RecordCommand(time, c);
        }

    private:
        // this takes about 1 minute
        // static const int iteration = 4;

        // this takes about 24 hours
        // static const int iteration = 5760;
        static const int iteration = 5760 * 20;

        DISALLOW_COPY_AND_ASSIGN(LRRTestAutoConsole);
    };
}

#endif // BADUMNA_AUTO_TEST_LRR_TEST_AUTO_CONSOLE_H