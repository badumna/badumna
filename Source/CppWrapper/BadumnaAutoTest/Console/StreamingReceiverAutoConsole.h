//---------------------------------------------------------------------------------
// <copyright file="StreamingReceiverAutoConsole.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_STREAMING_RECEIVER_AUTO_CONSOLE_H
#define BADUMNA_AUTO_TEST_STREAMING_RECEIVER_AUTO_CONSOLE_H

#include "Badumna/Utils/BasicTypes.h"
#include "Console/AutoTestConsole.h"

namespace BadumnaTestApp
{
    class StreamingReceiverAutoConsole : public AutoTestConsole
    {
    public:
        StreamingReceiverAutoConsole()
            : AutoTestConsole()
        {
        }

        void RecordCommands()
        {   
            // quit
            RecordCommand(20000, BadumnaTestApp::EXIT_COMMAND);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(StreamingReceiverAutoConsole);
    };
}

#endif // BADUMNA_AUTO_TEST_STREAMING_RECEIVER_AUTO_CONSOLE_H