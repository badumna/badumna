//---------------------------------------------------------------------------------
// <copyright file="IConsole.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_ICONSOLE_H
#define BADUMNA_AUTO_TEST_ICONSOLE_H

#include "Badumna/Utils/BasicTypes.h"

namespace BadumnaTestApp
{
    /**
     * All supported commands
     */
    enum ControlCommand
    {
        RANDOM_MOVE_COMMAND = 'b',
        MOVE_RIGHT_FIX_DISTANCE = 'e',
        MOVE_LEFT_FIX_DISTANCE = 'f',
#ifdef WIN32
        MOVE_UP_COMMAND = 72,
        MOVE_DOWN_COMMAND = 80,
        MOVE_LEFT_COMMAND = 75,
        MOVE_RIGHT_COMMAND = 77,
#else
        MOVE_UP_COMMAND = 65,
        MOVE_DOWN_COMMAND = 66,
        MOVE_LEFT_COMMAND = 68,
        MOVE_RIGHT_COMMAND = 67,
#endif // WIN32
        EXIT_COMMAND = 'x',
        CHANGE_SCENE_COMMAND = 'g',
        INCREASE_COUNTER_COMMAND = 'c',
        SEND_EVENT_TO_REMOTE_COPIES = 'r',
        SEND_EVENT_TO_ORIGINAL = 'o',
        SEND_ARBITRATION_EVENT_COMMAND = 'a',
        SEND_PROXIMITY_CHAT_COMMAND = 'p',
        SEND_PRIVATE_CHAT_COMMAND = 'q',
        CHANGE_PRESENCE_COMMAND = 's',
        RE_LOGIN = 'l',
        SHUTDOWN_TEST_COMMAND = 't',
        CONSISTENCY_CHECK_COMMAND = 'y',
        STREAMING_SEND_COMMAND = 'm',
        NO_MORE_COMMAND
    };

    /**
     * The interface class for Console.
     */
    class IConsole
    {
    public:
        IConsole() {}
        virtual ~IConsole() {}

        /**
         * Start the console.
         */
        virtual void Start() = 0;
        
        /**
         * Shutdown the console.
         */
        virtual void Shutdown() = 0;
        
        /**
         * Get the next command that is issued by milliseconds_since_start from the console.
         */
        virtual int GetCommand(int milliseconds_since_start) = 0;
        
        /**
         * Set the status to ready.
         */
        virtual void SetAsReady(int milliseconds_since_start) = 0;
    
    private:
        DISALLOW_COPY_AND_ASSIGN(IConsole);
    };
}

#endif // BADUMNA_AUTO_TEST_ICONSOLE_H