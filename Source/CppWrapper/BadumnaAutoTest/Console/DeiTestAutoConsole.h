//---------------------------------------------------------------------------------
// <copyright file="DeiTestAutoConsole.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_AUTO_TEST_DEI_TEST_AUTO_CONSOLE_H
#define BADUMNA_AUTO_TEST_DEI_TEST_AUTO_CONSOLE_H

#include "Badumna/Utils/BasicTypes.h"
#include "Console/AutoTestConsole.h"

namespace BadumnaTestApp
{
    class DeiTestAutoConsole : public AutoTestConsole
    {
    public:
        DeiTestAutoConsole()
            : AutoTestConsole()
        {
        }

        void RecordCommands()
        {
            for(int i = 5000; i < 70000; i = i + 5000)
            {
                RecordCommand(i, BadumnaTestApp::INCREASE_COUNTER_COMMAND);
            }
            
            RecordCommand(80000, BadumnaTestApp::EXIT_COMMAND);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(DeiTestAutoConsole);
    };
}

#endif // BADUMNA_AUTO_TEST_DEI_TEST_AUTO_CONSOLE_H