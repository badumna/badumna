#include "ReplicaObject.h"

namespace BadumnaUnitTest
{
    ReplicaObject::ReplicaObject() 
        : guid(BadumnaId::None()), 
        position(0.0f, 0.0f, 0.0f),  
        radius(0.0f), 
        area_of_interest_radius(0.0), 
        counter(0), 
        deserialize_counter(0),
        event_counter(0)
    {
    }

    BadumnaId ReplicaObject::Guid() const
    {
        return this->guid;
    }

    void ReplicaObject::SetGuid(BadumnaId const &id)
    {
        this->guid = id;
    }

    void ReplicaObject::HandleEvent(InputStream *)
    {
        event_counter++;
    }

    Vector3 ReplicaObject::Position() const
    {
        return this->position;
    }

    void ReplicaObject::SetPosition(Vector3 const &value)
    {
        this->position = value;
    }

    float ReplicaObject::Radius() const
    {
        return this->radius;
    }

    void ReplicaObject::SetRadius(float value)
    {
        this->radius = value;
    }

    float ReplicaObject::AreaOfInterestRadius() const
    {
        return this->area_of_interest_radius;
    }

    void ReplicaObject::SetAreaOfInterestRadius(float value)
    {
        this->area_of_interest_radius = value;
    }

    void ReplicaObject::Deserialize(
        BooleanArray const &included_parts, 
        InputStream *stream, 
        int /*estimated_milliseconds_since_departure*/)
    {
        deserialize_counter++;

        if(included_parts[Counter] == true)
        {
            (*stream) >> this->counter;
        }
    }
}