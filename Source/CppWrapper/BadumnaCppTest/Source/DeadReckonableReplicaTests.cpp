//---------------------------------------------------------------------------------
// <copyright file="DeadReckonableReplicaTests.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <boost/test/unit_test.hpp>

#include <iostream>
#include <sstream>

#include "DeadReckonableReplica.h"
#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/Replication/IDeadReckonableSpatialReplica.h"

using namespace Badumna;
using namespace BadumnaUnitTest;

struct DeadReckonableReplicaFixture
{
    DeadReckonableReplicaFixture()
        : replica(new DeadReckonableReplica())
    {
    }
    
    virtual ~DeadReckonableReplicaFixture() 
    {
        delete replica;
    }

    DeadReckonableReplica *replica;

private:
    DISALLOW_COPY_AND_ASSIGN(DeadReckonableReplicaFixture);
};

BOOST_FIXTURE_TEST_SUITE(BadumnaDeadReckonableReplica, DeadReckonableReplicaFixture)

BOOST_AUTO_TEST_CASE(DeadReckonableReplicaIsDeadReckonable)
{
    BOOST_REQUIRE(replica->IsDeadReckonable());
}

BOOST_AUTO_TEST_CASE(DeadReckonableReplicaIsSubClassOfIDeadReckonableSpatialReplica)
{
    IDeadReckonableSpatialReplica *base = dynamic_cast<IDeadReckonableSpatialReplica *>(replica);
    BOOST_REQUIRE(base != NULL);
}

BOOST_AUTO_TEST_CASE(CallToAttemptMovementChangesThePosition)
{
    Vector3 p(1.0f, 2.0f, 3.0f);
    Vector3 old_position = replica->Position();
    BOOST_CHECK_CLOSE(old_position.GetX(), 0.0f, 0.001f);
    BOOST_CHECK_CLOSE(old_position.GetY(), 0.0f, 0.001f);
    BOOST_CHECK_CLOSE(old_position.GetZ(), 0.0f, 0.001f);

    replica->AttemptMovement(p);
    Vector3 new_position = replica->Position();

    BOOST_CHECK_CLOSE(new_position.GetX(), p.GetX(), 0.001f);
    BOOST_CHECK_CLOSE(new_position.GetY(), p.GetY(), 0.001f);
    BOOST_CHECK_CLOSE(new_position.GetZ(), p.GetZ(), 0.001f);   
}

BOOST_AUTO_TEST_SUITE_END() // DeadReckonableReplica