#include <memory>
#include <boost/test/unit_test.hpp>

#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/Replication/ReplicaWrapper.h"
#include "Badumna/Replication/SpatialEntityStateSegment.h"

#include "ReplicaObject.h"
#include "DeadReckonableReplica.h"

using std::auto_ptr;
using namespace Badumna;
using namespace BadumnaUnitTest;

struct ReplicaWrapperFixture
{
    auto_ptr<ReplicaObject> replica;
    auto_ptr<ReplicaWrapper> replica_wrapper;
    String id;

    ReplicaWrapperFixture() 
        : replica(new ReplicaObject()),
        replica_wrapper(NULL),
        id(L"my_id")
    {
        replica_wrapper.reset(BadumnaRuntime::Instance().CreateReplicaWrapper(id, replica.get()));
    }
    
    virtual ~ReplicaWrapperFixture() 
    { 
    }
};

BOOST_FIXTURE_TEST_SUITE(BadumnaReplicaWrapper, ReplicaWrapperFixture)

BOOST_AUTO_TEST_CASE(ReplicaDeserializeIsInvoked)
{
    char buf[16];
    int count = 128;
    BooleanArray included_part;
    InputStream stream(buf, 16);

    BOOST_CHECK_EQUAL(0, replica->GetDeserializeCounter());

    for(int i = 0; i < count; i++)
    {
        replica_wrapper->Deserialize(included_part, &stream, 1);
        BOOST_CHECK_EQUAL(i + 1, replica->GetDeserializeCounter());
    }
}

BOOST_AUTO_TEST_CASE(ReplicaPositionCanBeSet)
{
    BOOST_CHECK_CLOSE(0.0f, replica->Position().GetX(), 0.01f);
    BOOST_CHECK_CLOSE(0.0f, replica->Position().GetY(), 0.01f);
    BOOST_CHECK_CLOSE(0.0f, replica->Position().GetZ(), 0.01f);
    replica_wrapper->SetReplicaProperty(Badumna::StateSegment_Position, 1.0f, 2.0f, 3.0f);
    BOOST_CHECK_CLOSE(1.0f, replica->Position().GetX(), 0.01f);
    BOOST_CHECK_CLOSE(2.0f, replica->Position().GetY(), 0.01f);
    BOOST_CHECK_CLOSE(3.0f, replica->Position().GetZ(), 0.01f);
}

BOOST_AUTO_TEST_CASE(ReplicaRadiusCanBeSet)
{
    BOOST_CHECK_CLOSE(0.0f, replica->Radius(), 0.01f);
    MonoObject *managed_float_value = MonoRuntime::GetManagedFloatValue(128.0f);
    replica_wrapper->SetReplicaProperty(Badumna::StateSegment_Radius, managed_float_value);
    BOOST_CHECK_CLOSE(128.0f, replica->Radius(), 0.01f);
}

BOOST_AUTO_TEST_CASE(ReplicaInterestRadiusCanBeSet)
{
    BOOST_CHECK_CLOSE(0.0f, replica->AreaOfInterestRadius(), 0.01f);
    MonoObject *managed_float_value = MonoRuntime::GetManagedFloatValue(128.0f);
    replica_wrapper->SetReplicaProperty(Badumna::StateSegment_InterestRadius, managed_float_value);
    BOOST_CHECK_CLOSE(128.0f, replica->AreaOfInterestRadius(), 0.01f);
}

BOOST_AUTO_TEST_CASE(RegularReplicaIsNotDeadReckonable)
{
    BOOST_REQUIRE(!replica->IsDeadReckonable());
}

BOOST_AUTO_TEST_CASE(AttemptMovementActuallyMovesTheReplica)
{
    auto_ptr<DeadReckonableReplica> deadreckonable_replica(new DeadReckonableReplica());
    auto_ptr<ReplicaWrapper> deadreckonable_wrapper(BadumnaRuntime::Instance().CreateReplicaWrapper(id, deadreckonable_replica.get()));

    BOOST_CHECK_CLOSE(0.0f, deadreckonable_replica->Position().GetX(), 0.01f);
    BOOST_CHECK_CLOSE(0.0f, deadreckonable_replica->Position().GetY(), 0.01f);
    BOOST_CHECK_CLOSE(0.0f, deadreckonable_replica->Position().GetZ(), 0.01f);

    Vector3 new_position(1.0f, 2.0f, 3.0f);
    deadreckonable_wrapper->AttemptMovement(new_position);

    BOOST_CHECK_CLOSE(new_position.GetX(), deadreckonable_replica->Position().GetX(), 0.01f);
    BOOST_CHECK_CLOSE(new_position.GetY(), deadreckonable_replica->Position().GetY(), 0.01f);
    BOOST_CHECK_CLOSE(new_position.GetZ(), deadreckonable_replica->Position().GetZ(), 0.01f);
}

BOOST_AUTO_TEST_CASE(DeadReckonableReplicaIsDeadReckonable)
{
    auto_ptr<DeadReckonableReplica> deadreckonable_replica(new DeadReckonableReplica());
    BOOST_REQUIRE(deadreckonable_replica->IsDeadReckonable());
}

BOOST_AUTO_TEST_SUITE_END() // ReplicaWrapper