#include <boost/test/unit_test.hpp>
#include "Badumna/Controller/CreateControllerDelegate.h"

using namespace Badumna;

int CreateControllerDelegateStubStaticCount = 1;

class CreateControllerDelegateStub
{
public:
    CreateControllerDelegateStub()
        : name(),
        count(0)
    {
    }

    int GetCounter() const
    {
        return count;
    }

    Badumna::String GetLastName() const
    {
        return name;
    }

    Badumna::DistributedSceneController *Create(Badumna::NetworkFacade *, Badumna::String const &name)
    {
        this->name = name;
        count++;
        return NULL;
    }

    static Badumna::DistributedSceneController *StaticCreate(Badumna::NetworkFacade *, Badumna::String const &)
    {
        CreateControllerDelegateStubStaticCount++;
        return NULL;
    }

private:
    String name;
    int count;
};

struct CreateControllerDelegateFixture
{
    CreateControllerDelegateFixture()
        : stub()
    {
    }
    
    virtual ~CreateControllerDelegateFixture() 
    { 
    }

    CreateControllerDelegateStub stub;
};

BOOST_FIXTURE_TEST_SUITE(BadumnaCreateControllerDelegateDelegate, CreateControllerDelegateFixture)

BOOST_AUTO_TEST_CASE(CreateControllerDelegateCanBeInvoked)
{
    int count = stub.GetCounter();
    CreateControllerDelegate d(&CreateControllerDelegateStub::Create, stub, NULL);
    d(L"what_ever_name");
    BOOST_CHECK_EQUAL(count + 1, stub.GetCounter());
}

BOOST_AUTO_TEST_CASE(CreateControllerDelegatePassesTheName)
{
    CreateControllerDelegate d(&CreateControllerDelegateStub::Create, stub, NULL);
    d(L"what_ever_name");
    BOOST_REQUIRE(stub.GetLastName() == L"what_ever_name");
}

BOOST_AUTO_TEST_CASE(CreateControllerDelegateWorksWithFreeFunction)
{
    int count = CreateControllerDelegateStubStaticCount;
    CreateControllerDelegate d(&CreateControllerDelegateStub::StaticCreate, NULL);
    d(L"what_ever_name");
    BOOST_CHECK_EQUAL(count + 1, CreateControllerDelegateStubStaticCount);
}

BOOST_AUTO_TEST_SUITE_END() // CreateControllerDelegate