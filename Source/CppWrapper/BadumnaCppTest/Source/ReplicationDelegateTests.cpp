#include <boost/test/unit_test.hpp>

#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/Replication/SpatialReplicaDelegate.h"
#include "ReplicationDelegateStub.h"
#include "ReplicaObject.h"

using namespace Badumna;

struct ReplicationDelegateFixture
{
    BadumnaId fake;
    BadumnaId &fake_ref;
    
    // network scene object can not be easily created without initializing the whole
    // badumna stack. so here a faked one is used. 
    NetworkScene &scene;

    ReplicationDelegateFixture() 
        : fake(BadumnaRuntime::Instance().CreateBadumnaId("Open|127.0.0.1:1-1")),
        fake_ref(fake),
        scene(reinterpret_cast<NetworkScene &>(fake_ref))
    {
    }
    
    virtual ~ReplicationDelegateFixture() 
    { 
    }
};

BOOST_FIXTURE_TEST_SUITE(BadumnaReplicationDelegate, ReplicationDelegateFixture)

BOOST_AUTO_TEST_CASE(CreateReplicaCanBeInvoked)
{
    ReplicationDelegateStub stub;
    CreateSpatialReplicaDelegate callback(&ReplicationDelegateStub::Create, stub);
    int count = stub.GetCreateCount();
    callback(scene, fake, 1);
    BOOST_CHECK_EQUAL(count + 1, stub.GetCreateCount());
}

BOOST_AUTO_TEST_CASE(RemoveReplicaCanBeInvoked)
{
    ReplicationDelegateStub stub;
    BadumnaUnitTest::ReplicaObject replica;
    RemoveSpatialReplicaDelegate callback(&ReplicationDelegateStub::Remove, stub);
    int count = stub.GetRemoveCount();
    callback(scene, replica);
    BOOST_CHECK_EQUAL(count + 1, stub.GetRemoveCount());
}

BOOST_AUTO_TEST_CASE(CreateReplicaCanBeCopiedAndAssigned)
{
    ReplicationDelegateStub stub;
    CreateSpatialReplicaDelegate callback(&ReplicationDelegateStub::Create, stub);
    CreateSpatialReplicaDelegate copied_callback(callback);
    int count = stub.GetCreateCount();
    copied_callback(scene, fake, 1);
    BOOST_CHECK_EQUAL(count + 1, stub.GetCreateCount());

    CreateSpatialReplicaDelegate assigned_callback = callback;
    assigned_callback(scene, fake, 1);
    BOOST_CHECK_EQUAL(count + 2, stub.GetCreateCount());
}

BOOST_AUTO_TEST_CASE(RemoveReplicaCanBeCopiedAndAssigned)
{
    ReplicationDelegateStub stub;
    BadumnaUnitTest::ReplicaObject replica;
    RemoveSpatialReplicaDelegate callback(&ReplicationDelegateStub::Remove, stub);
    RemoveSpatialReplicaDelegate copied_callback(callback);
    int count = stub.GetRemoveCount();
    copied_callback(scene, replica);
    BOOST_CHECK_EQUAL(count + 1, stub.GetRemoveCount());

    RemoveSpatialReplicaDelegate assigned_callback = callback;
    assigned_callback(scene, replica);
    BOOST_CHECK_EQUAL(count + 2, stub.GetRemoveCount());
}

BOOST_AUTO_TEST_SUITE_END() // ReplicationDelegates