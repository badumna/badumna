#include "Badumna/Datatypes/BadumnaId.h"
#include "Badumna/Chat/IChatChannel.h"
#include "Badumna/Chat/ChatChannelId.h"
#include "Badumna/Chat/ChatChannelType.h"
#include "Badumna/Core/BadumnaRuntime.h"
#include "ChatChannelStub.h"

using Badumna::String;
using Badumna::BadumnaId;
using Badumna::ChatChannelId;
using Badumna::BadumnaRuntime;

using Badumna::String;

namespace BadumnaUnitTest {

    ChatChannelStub::ChatChannelStub()
    {
    }

    ChatChannelStub::~ChatChannelStub()
    {
    }

    // TODO: allow constructor to specify an ID
    ChatChannelId ChatChannelStub::Id()
    {
        Badumna::BadumnaId id = BadumnaRuntime::Instance().CreateBadumnaId("Open|127.0.0.1:1-1");
        Badumna::ChatChannelId chanId = BadumnaRuntime::Instance().CreateChatChannelId(Badumna::PrivateType, id);
        return chanId;
    }

    void ChatChannelStub::SendMessage(String /*message*/)
    {
      // TODO...
    }

    void ChatChannelStub::Unsubscribe()
    {
    }
}