#include <iostream>
#include <boost/thread.hpp>
#include <boost/test/unit_test.hpp>

#include "Badumna/DataTypes/BooleanArray.h"
#include "Badumna/Core/ThreadGuard.h"

using namespace Badumna;

struct MultiTheadingFixture
{
    MultiTheadingFixture() 
        : error_detected_(false)
    {
    }
    
    virtual ~MultiTheadingFixture() 
    { 
    }

    void AccessRuntime()
    {
        ThreadGuard guard;
        BooleanArray boolean_array(true);
        for(size_t i = 0; i < boolean_array.HighestUsedIndex(); ++i)
        {
            // can't use BOOST_CHECK_EQUAL or BOOST_REQUIRE here as it is not thread safe. 
            bool ret = boolean_array[i];
            if(!ret)
            {
                error_detected_ = true;
            }
        }
    }

    bool error_detected_;
};

BOOST_FIXTURE_TEST_SUITE(BadumnaMultiThreading, MultiTheadingFixture)

BOOST_AUTO_TEST_CASE(RuntimeCanBeAccessedFromMultipleThreads)
{
    boost::shared_ptr<boost::thread> thread1;
    boost::shared_ptr<boost::thread> thread2;
    boost::shared_ptr<boost::thread> thread3;

    thread1.reset(new boost::thread(boost::bind(&MultiTheadingFixture::AccessRuntime, this)));
    thread2.reset(new boost::thread(boost::bind(&MultiTheadingFixture::AccessRuntime, this)));
    thread3.reset(new boost::thread(boost::bind(&MultiTheadingFixture::AccessRuntime, this)));

    thread1->join();
    thread2->join();
    thread3->join();

    BOOST_CHECK_EQUAL(false, error_detected_);
}

BOOST_AUTO_TEST_SUITE_END() // BadumnaMultiThreading