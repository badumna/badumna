//---------------------------------------------------------------------------------
// <copyright file="ChatDelegateStub.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_CHATDELEGATESTUB_H
#define BADUMNA_CHATDELEGATESTUB_H

#include "Badumna/Chat/ChatChannelType.h"
#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/DataTypes/String.h"

namespace BadumnaUnitTest
{
    using namespace Badumna;
    using namespace std;

    class ChatDelegateStub
    {
    public:
        ChatDelegateStub() 
            : chat_handler_called(0),
            presence_handler_called(0),
            invitation_handler_called(0)
        {
        }

        void MessageHandler(IChatChannel * /*channel*/, BadumnaId const & /*id*/, String const & /*msg*/)
        {
            chat_handler_called++;
        }

        void PresenceHandler(
            IChatChannel * /*channel*/, 
            BadumnaId const & /*id*/, 
            String const & /*name*/, 
            ChatStatus /*s*/)
        {
            presence_handler_called++;
        }

        void InvitationHandler(ChatChannelId const & /*channel*/, String const & /*name*/)
        {
            invitation_handler_called++;
        }
    
        int ChatHandlerCalled() { return chat_handler_called; }
        int PresenceHandlerCalled() { return presence_handler_called; }
        int InvitationHandlerCalled() { return invitation_handler_called; }

    private:
        int chat_handler_called;
        int presence_handler_called;
        int invitation_handler_called;
    };
}

#endif // BADUMNA_CHATDELEGATESTUB_H