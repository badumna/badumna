#include <string>
#include "Badumna/Core/Arguments.h"

#ifndef WIN32
#define BOOST_TEST_DYN_LINK
#endif

#define BOOST_TEST_MODULE Core
#include <boost/test/unit_test.hpp>

using namespace Badumna;

struct ArgumentsFixture
{
    ArgumentsFixture()
    {
    }

    virtual ~ArgumentsFixture()
    {
    }
};

BOOST_FIXTURE_TEST_SUITE(BadumnaArguments, ArgumentsFixture)

BOOST_AUTO_TEST_CASE(DefaultConstructInitializeTheObjectCorrectly)
{
    Arguments args;
    BOOST_CHECK_EQUAL(0, args.Count());
    void **a = args;
    BOOST_REQUIRE(NULL == a);
}

BOOST_AUTO_TEST_CASE(CountReturnsCorrectCountNum)
{
    Arguments args;
    int data1;
    float data2;
    float data3;

    args.AddArgument(&data1);
    args.AddArgument(&data2);
    args.AddArgument(&data3);

    BOOST_CHECK_EQUAL(3, args.Count());
}

BOOST_AUTO_TEST_CASE(GetArgumentsReturnExpectedPointerArray)
{
    Arguments args;
    int data1 = 1;
    float data2 = 2.0f;
    float data3 = 3.0f;
    bool data4 = false;

    args.AddArgument(&data1);
    args.AddArgument(&data2);
    args.AddArgument(&data3);
    args.AddArgument(&data4);
    args.AddNullArgument();

    BOOST_CHECK_EQUAL(5, args.Count());
    void **a = args;
    BOOST_REQUIRE(a != NULL);

    void **args_array = args;
    BOOST_CHECK_EQUAL(&data1, args_array[0]);
    BOOST_CHECK_EQUAL(&data2, args_array[1]);
    BOOST_CHECK_EQUAL(&data3, args_array[2]);
    BOOST_CHECK_EQUAL(&data4, args_array[3]);
    BOOST_REQUIRE(NULL == args_array[4]);
}

BOOST_AUTO_TEST_CASE(NULLArgumentWorksAsExpected)
{
    BOOST_REQUIRE(Arguments::None().Count() == 0);
    void **a = Arguments::None();
    BOOST_REQUIRE(a == NULL);
}

BOOST_AUTO_TEST_SUITE_END() // Arguments