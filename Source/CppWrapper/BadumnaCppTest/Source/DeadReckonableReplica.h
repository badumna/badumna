//---------------------------------------------------------------------------------
// <copyright file="DeadReckonableReplica.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef REPLICATION_TEST_DEADRECKONABLE_REPLICA_H
#define REPLICATION_TEST_DEADRECKONABLE_REPLICA_H

#include "Badumna/Replication/IDeadReckonableSpatialReplica.h"

namespace BadumnaUnitTest
{
    using Badumna::IDeadReckonableSpatialReplica;
    using Badumna::BadumnaId;
    using Badumna::Vector3;
    using Badumna::BooleanArray;
    using Badumna::InputStream;

    class DeadReckonableReplica : public IDeadReckonableSpatialReplica
    {
    public:
        DeadReckonableReplica();
        ~DeadReckonableReplica();

        //
        // IEntity properties
        // 
        BadumnaId Guid() const;

        void SetGuid(BadumnaId const &id);

        void HandleEvent(InputStream *stream);

        //
        // ISpatialEntity properties
        //
        Vector3 Position() const;

        void SetPosition(Vector3 const &position);

        float Radius() const;

        void SetRadius(float val);

        float AreaOfInterestRadius() const;

        void SetAreaOfInterestRadius(float val);

        void Deserialize(
            BooleanArray const &included_part, 
            InputStream *stream, 
            int estimated_milliseconds_since_departure);

        Vector3 Velocity() const;

        void SetVelocity(Vector3 const &velocity);

        void AttemptMovement(Vector3 const &reckoned_position);

        //
        // app specific.
        // 
        inline int GetCounter()
        {
            return this->counter;
        }

        inline void SetCounter(int value)
        {
            this->counter = value;
        }

        int GetDeserializeCounter()
        {
            return deserialize_counter;
        }

        int GetEventCounter()
        {
            return event_counter;
        }

    private:
        BadumnaId guid;

        Vector3 velocity;
        Vector3 position;
        float radius;
        float area_of_interest_radius;

        int counter; 
        int deserialize_counter;
        int event_counter;
    };
}

#endif // REPLICATION_TEST_DEADRECKONABLE_REPLICA_H