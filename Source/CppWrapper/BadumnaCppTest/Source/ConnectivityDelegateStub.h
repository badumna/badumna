//---------------------------------------------------------------------------------
// <copyright file="ConnectivityDelegateStub.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef CONNECTIVITY_DELEGATE_STUB_H
#define CONNECTIVITY_DELEGATE_STUB_H

#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/Replication/SpatialReplicaDelegate.h"
#include "ReplicaObject.h"

using namespace Badumna;

namespace BadumnaUnitTest
{
    class ConnectivityDelegateStub
    {
    public:
        ConnectivityDelegateStub()
            : online_count(0),
            offline_count(0),
            address_changed_count(0)
        {
        }

        void OnlineEventHandler()
        {
            online_count++;
        }

        void OfflineEventHandler()
        {
            offline_count++;
        }

        void AddressChangedEventHandler()
        {
            address_changed_count++;
        }

        int GetOnlineCount() const
        {
            return online_count;
        }

        int GetOfflineCount() const
        {
            return offline_count;
        }

        int GetAddressChangedCount() const
        {
            return address_changed_count;
        }

    private:
        int online_count;
        int offline_count;
        int address_changed_count;
    };
}

#endif // CONNECTIVITY_DELEGATE_STUB_H