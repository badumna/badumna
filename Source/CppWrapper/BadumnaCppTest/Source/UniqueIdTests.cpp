#include <iostream>
#include <sstream>
#include <exception>
#include <boost/test/unit_test.hpp>

#include "Badumna/Core/InternalUniqueId.h"

using namespace Badumna;

struct UniqueIdFixture
{
    UniqueIdFixture() 
    {
    }
    
    virtual ~UniqueIdFixture() 
    { 
    }
};

BOOST_FIXTURE_TEST_SUITE(BadumnaUniqueId, UniqueIdFixture)

BOOST_AUTO_TEST_CASE(GetChatSessionIdReturnsDifferentId)
{
    UniqueId id1 = InternalUniqueIdManager::GetChatSessionId();
    UniqueId id2 = InternalUniqueIdManager::GetChatSessionId();
    UniqueId id3 = InternalUniqueIdManager::GetChatSessionId();

    BOOST_REQUIRE(id1 != id2);
    BOOST_REQUIRE(id1 != id3);
    BOOST_REQUIRE(id2 != id3);
}

BOOST_AUTO_TEST_CASE(GetArbitratorIdReturnsTheSameIdWithSameName)
{
    UniqueId id1 = InternalUniqueIdManager::GetArbitratorId(L"test_id1");
    UniqueId id2 = InternalUniqueIdManager::GetArbitratorId(L"test_id1");

    BOOST_REQUIRE(id1 == id2);
}

BOOST_AUTO_TEST_CASE(GetArbitratorIdReturnsDifferentIdWithDifferentName)
{
    UniqueId id1 = InternalUniqueIdManager::GetArbitratorId(L"test_id1");
    UniqueId id2 = InternalUniqueIdManager::GetArbitratorId(L"test_id2");

    BOOST_REQUIRE(id1 != id2);
}

BOOST_AUTO_TEST_SUITE_END() // BadumnaUniqueId