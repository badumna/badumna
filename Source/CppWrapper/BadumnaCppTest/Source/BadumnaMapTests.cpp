#include <string>
#include <boost/test/unit_test.hpp>

#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/Chat/ChatDelegates.h"
#include "Badumna/Chat/ChatChannelId.h"
#include "Badumna/Core/BadumnaMap.h"
#include "Badumna/DataTypes/BadumnaId.h"
#include "ChatDelegateStub.h"
#include "ChatChannelStub.h"

using namespace std;
using namespace Badumna;
using namespace BadumnaUnitTest;
typedef BadumnaMap<int, string> TestMap;

struct BadumnaMapFixture
{
    BadumnaMapFixture()
    {
    }

    virtual ~BadumnaMapFixture()
    {
    }
};

BOOST_FIXTURE_TEST_SUITE(BadumnaBadumnaMap, BadumnaMapFixture)

BOOST_AUTO_TEST_CASE(DefaultConstructorWorksAsExpected)
{
    TestMap map;
    BOOST_CHECK_EQUAL(0, map.Size());
}

BOOST_AUTO_TEST_CASE(TryAddWillAcceptNewKey)
{
    TestMap map;
    BOOST_CHECK_EQUAL(0, map.Size());

    map.TryAdd("key1", 1);
    BOOST_CHECK_EQUAL(1, map.Size());

    map.TryAdd("key2", 2);
    BOOST_CHECK_EQUAL(2, map.Size());
    BOOST_REQUIRE(map.Contains("key1"));
    BOOST_REQUIRE(map.Contains("key2"));
    BOOST_REQUIRE(!map.Contains("key3"));
}

BOOST_AUTO_TEST_CASE(TryAddWillIgnoreDuplicatedKey)
{
    TestMap map;
    BOOST_CHECK_EQUAL(0, map.Size());

    map.TryAdd("key1", 1);
    BOOST_CHECK_EQUAL(1, map.Size());

    map.TryAdd("key1", 2);
    BOOST_CHECK_EQUAL(1, map.Size());

    BOOST_REQUIRE(map.Contains("key1"));
    BOOST_CHECK_EQUAL(1, map.GetValue("key1"));
}

BOOST_AUTO_TEST_CASE(AddOrReplaceReplacesDuplicatedKey)
{
    TestMap map;
    BOOST_CHECK_EQUAL(0, map.Size());

    map.TryAdd("key1", 1);
    BOOST_CHECK_EQUAL(1, map.Size());

    map.AddOrReplace("key1", 2);
    BOOST_CHECK_EQUAL(1, map.Size());

    BOOST_REQUIRE(map.Contains("key1"));
    BOOST_CHECK_EQUAL(2, map.GetValue("key1"));
}

BOOST_AUTO_TEST_CASE(AddOrReplaceAddsNewKey)
{
    TestMap map;
    BOOST_CHECK_EQUAL(0, map.Size());

    map.TryAdd("key1", 1);
    BOOST_CHECK_EQUAL(1, map.Size());

    map.AddOrReplace("key2", 2);
    BOOST_CHECK_EQUAL(2, map.Size());

    BOOST_REQUIRE(map.Contains("key1"));
    BOOST_REQUIRE(map.Contains("key2"));
}

BOOST_AUTO_TEST_CASE(TryRemoveRemovesExistKey)
{
    TestMap map;
    BOOST_CHECK_EQUAL(0, map.Size());

    map.TryAdd("key1", 1);
    BOOST_CHECK_EQUAL(1, map.Size());
    BOOST_REQUIRE(map.Contains("key1"));

    map.TryRemove("key1");
    BOOST_CHECK_EQUAL(0, map.Size());
    BOOST_REQUIRE(!map.Contains("key1"));
}

BOOST_AUTO_TEST_CASE(TryRemoveIgnoresNonExistKey)
{
    TestMap map;
    BOOST_CHECK_EQUAL(0, map.Size());

    map.TryAdd("key1", 1);
    BOOST_CHECK_EQUAL(1, map.Size());
    BOOST_REQUIRE(map.Contains("key1"));

    map.TryRemove("key2");
    BOOST_CHECK_EQUAL(1, map.Size());
    BOOST_REQUIRE(map.Contains("key1"));
}

BOOST_AUTO_TEST_CASE(RemoveReturnsCorrectValue)
{
    TestMap map;
    BOOST_CHECK_EQUAL(0, map.Size());

    map.TryAdd("key1", 1);
    bool ret = map.Remove("key2");
    BOOST_REQUIRE(!ret);
    BOOST_CHECK_EQUAL(1, map.Size());
    BOOST_REQUIRE(map.Contains("key1"));

    ret = map.Remove("key1");
    BOOST_REQUIRE(ret);
    BOOST_CHECK_EQUAL(0, map.Size());
    BOOST_REQUIRE(!map.Contains("key1"));
}

BOOST_AUTO_TEST_CASE(GetValueReturnsExistValue)
{
    TestMap map;
    BOOST_CHECK_EQUAL(0, map.Size());

    map.TryAdd("key1", 1);
    BOOST_CHECK_EQUAL(1, map.GetValue("key1"));
}

BOOST_AUTO_TEST_CASE(TryGetValueReturnsExistingValue)
{
    TestMap map;
    BOOST_CHECK_EQUAL(0, map.Size());

    map.TryAdd("key1", 1);
    Optional<int> result = map.TryGetValue("key1");
    BOOST_REQUIRE(result.IsSet());
    BOOST_CHECK_EQUAL(1, result.Get());
}

BOOST_AUTO_TEST_CASE(TryGetValueIgnoresNonExistingValue)
{
    TestMap map;
    BOOST_CHECK_EQUAL(0, map.Size());

    map.TryAdd("key1", 1);
    Optional<int> result = map.TryGetValue("keynope");
    BOOST_REQUIRE(!result.IsSet());
}

BOOST_AUTO_TEST_CASE(InvokeDelegate2WorksAsExpected)
{
    int key = 1032;
    BadumnaMap<ChatInvitationHandler, int> ChatInvitationHandlerMap;
    ChatDelegateStub stub;

    ChatInvitationHandler d(&ChatDelegateStub::InvitationHandler, stub);
    BOOST_CHECK_EQUAL(0, stub.InvitationHandlerCalled());
    ChatInvitationHandlerMap.AddOrReplace(key, d);
    ChatChannelStub channel = BadumnaUnitTest::ChatChannelStub();
    ChatInvitationHandlerMap.InvokeDelegate(key, channel.Id(), "test");
    BOOST_CHECK_EQUAL(1, stub.InvitationHandlerCalled());
}

BOOST_AUTO_TEST_CASE(InvokeDelegate3WorksAsExpected)
{
    int key = 1033;
    BadumnaMap<ChatMessageHandler, int> ChatMessageHandlerMap;
    ChatDelegateStub stub;

    ChatMessageHandler d(&ChatDelegateStub::MessageHandler, stub);
    BOOST_CHECK_EQUAL(0, stub.ChatHandlerCalled());
    ChatMessageHandlerMap.AddOrReplace(key, d);
    boost::shared_ptr<Badumna::IChatChannel> channel = boost::shared_ptr<Badumna::IChatChannel>(new BadumnaUnitTest::ChatChannelStub());
    ChatMessageHandlerMap.InvokeDelegate(key, channel.get(), BadumnaRuntime::Instance().CreateBadumnaId("Open|127.0.0.1:1-1"), "test");

    BOOST_CHECK_EQUAL(1, stub.ChatHandlerCalled());
}

BOOST_AUTO_TEST_SUITE_END() // BadumnaMap