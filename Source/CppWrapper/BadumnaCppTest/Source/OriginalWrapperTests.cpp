#include <memory>
#include <boost/test/unit_test.hpp>

#include "Badumna/Core/Arguments.h"
#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/DataTypes/InputStream.h"
#include "Badumna/Replication/OriginalWrapper.h"
#include "Badumna/Replication/SpatialEntityStateSegment.h"
#include "Badumna/Utils/BasicTypes.h"

#include "ReplicaObject.h"
#include "LocalObject.h"
#include "DeadReckonableOriginal.h"
#include "MonoRuntimeTestHelper.h"

using std::auto_ptr;
using namespace Badumna;
using namespace BadumnaUnitTest;

struct OriginalWrapperFixture
{
    auto_ptr<LocalObject> local_object;
    auto_ptr<OriginalWrapper> original_wrapper;
    String id;
    MonoObject *stub;
    MonoRuntime *runtime;

    OriginalWrapperFixture() 
        : local_object(new LocalObject()),
        original_wrapper(NULL),
        id(L"my_id"),
        stub(NULL),
        runtime(NULL)
    {
        original_wrapper.reset(BadumnaRuntime::Instance().CreateOriginalWrapper(local_object.get()));
        original_wrapper->SetUniqueId(id);
        runtime = original_wrapper->GetMonoRuntime();
    }
    
    virtual ~OriginalWrapperFixture() 
    { 
    }

    float GetFloatValue(String const &name)
    {
        Arguments args;
        MonoObject *ret = runtime->GetPropertyFromName(
            "Badumna.CppWrapperStub", 
            "SpatialOriginalStub", 
            original_wrapper->GetManagedMonoObject(),
            name.UTF8CStr(), 
            args, 
            NULL);
        return runtime->UnboxingObject<float>(ret);
    }

    float GetFloatValue2(String const &name, OriginalWrapper *wrapper)
    {
        Arguments args;
        MonoObject *ret = runtime->GetPropertyFromName(
            "Badumna.CppWrapperStub", 
            "DeadReckonableOriginalStub", 
            wrapper->GetManagedMonoObject(),
            name.UTF8CStr(), 
            args, 
            NULL);
        return runtime->UnboxingObject<float>(ret);
    }

private:
    DISALLOW_COPY_AND_ASSIGN(OriginalWrapperFixture);
};

BOOST_FIXTURE_TEST_SUITE(BadumnaOriginalWrapper, OriginalWrapperFixture)

BOOST_AUTO_TEST_CASE(OriginalSerializeIsInvoked)
{
    OutputStream stream;
    BooleanArray included_part;
    int count = 128;

    BOOST_CHECK_EQUAL(0, local_object->GetSerializeCounter());
    for(int i = 0; i < count; i++)
    {
        original_wrapper->Serialize(included_part, &stream);
        BOOST_CHECK_EQUAL(i + 1, local_object->GetSerializeCounter());
    }
}

BOOST_AUTO_TEST_CASE(RegularLocalObjectIsNotDeadReckonable)
{
    BOOST_REQUIRE(!local_object->IsDeadReckonable());
}

BOOST_AUTO_TEST_CASE(CallToSynchronizeOriginalPropertySynchronizesAllStates)
{
    float new_radius = 123.5f;
    float new_aoi = 135.6f;
    float px = 12.4f;
    float py = 12.8f;
    float pz = 23.4f;

    Vector3 new_position(px, py, pz);
    local_object->SetRadius(new_radius);
    local_object->SetAreaOfInterestRadius(new_aoi);
    local_object->SetPosition(new_position);

    original_wrapper->SynchronizeOriginalProperty();

    float ret_radius = GetFloatValue("Radius");
    float ret_aoi = GetFloatValue("AreaOfInterestRadius");
    float ret_px = GetFloatValue("PositionX");
    float ret_py = GetFloatValue("PositionY");
    float ret_pz = GetFloatValue("PositionZ");

    BOOST_CHECK_CLOSE(new_radius, ret_radius, 0.001f);
    BOOST_CHECK_CLOSE(new_aoi, ret_aoi, 0.001f);
    BOOST_CHECK_CLOSE(px, ret_px, 0.001f);
    BOOST_CHECK_CLOSE(py, ret_py, 0.001f);
    BOOST_CHECK_CLOSE(pz, ret_pz, 0.001f);
}

BOOST_AUTO_TEST_CASE(CallToHandleEventHandlesEvent)
{
    char buf[128];
    InputStream stream(buf, 128);
    
    int event_count = local_object->GetEventCounter();
    original_wrapper->HandleEvent(&stream);
    BOOST_REQUIRE(event_count + 1 == local_object->GetEventCounter());
}

BOOST_AUTO_TEST_CASE(DeadReckonableOriginalSynchronizesTheVelocityValue)
{
    auto_ptr<DeadReckonableOriginal> original(new DeadReckonableOriginal());
    auto_ptr<OriginalWrapper> wrapper(BadumnaRuntime::Instance().CreateOriginalWrapper(original.get()));

    float vx = 2.4f;
    float vy = 2.8f;
    float vz = 3.4f;

    original->SetVelocity(Vector3(vx, vy, vz));
    wrapper->SynchronizeOriginalProperty();

    float ret_vx = GetFloatValue2("VelocityX", wrapper.get());
    float ret_vy = GetFloatValue2("VelocityY", wrapper.get());
    float ret_vz = GetFloatValue2("VelocityZ", wrapper.get());

    BOOST_CHECK_CLOSE(vx, ret_vx, 0.001f);
    BOOST_CHECK_CLOSE(vy, ret_vy, 0.001f);
    BOOST_CHECK_CLOSE(vz, ret_vz, 0.001f);
}

BOOST_AUTO_TEST_CASE(DeadReckonableOriginalIsDeckonable)
{
    auto_ptr<DeadReckonableOriginal> original(new DeadReckonableOriginal());
    BOOST_REQUIRE(original->IsDeadReckonable());
}

BOOST_AUTO_TEST_SUITE_END() // OriginalWrapper