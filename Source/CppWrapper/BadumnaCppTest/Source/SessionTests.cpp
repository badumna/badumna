#include <iostream>
#include <string>
#include <boost/test/unit_test.hpp>

#include "Badumna/Dei/Session.h"
#include "Badumna/Dei/LoginResult.h"

using namespace std;
using namespace Dei;

struct BadumnaSessionFixture
{
    BadumnaSessionFixture()
    {
    }

    virtual ~BadumnaSessionFixture()
    {
    }
};

BOOST_FIXTURE_TEST_SUITE(BadumnaSession, BadumnaSessionFixture)

BOOST_AUTO_TEST_CASE(DeiTokenWithoutUsingSSLConnectionCanBeCreated)
{
    // this test cases checks whether the Dei.dll can be loaded correctly.
    Session session(L"example.com", 12345);
}

BOOST_AUTO_TEST_CASE(DeiTokenUsingPrivateKeyCanBeCreated)
{
    Session session(L"example.com", 12345, Session::GenerateKeyPair());
}

BOOST_AUTO_TEST_CASE(SslErrorsCanBeSetToIgnore)
{
    Session session(L"example.com", 12345);
    session.SetUseSslConnection(true);
    session.SetIgnoreSslErrors();
    session.SetIgnoreSslCertificateNameMismatch();
    session.SetIgnoreSslSelfSignedCertificateErrors();
}

BOOST_AUTO_TEST_CASE(FailedAuthenticateWillBeReported)
{
    cout << "\n\n---- Do not be alarmed, this test is expected to print a DNS failure ----\n";
    cout.flush();
    Session session(L"thisurljustcannotbereal1234567890.invaliddomainname", 12345, Session::GenerateKeyPair());
    LoginResult result = session.Authenticate(L"test", L"test");

    BOOST_REQUIRE(!result.WasSuccessful());

    LoginResult copied(result);
    BOOST_REQUIRE(!copied.WasSuccessful());
}


BOOST_AUTO_TEST_SUITE_END() // Session
