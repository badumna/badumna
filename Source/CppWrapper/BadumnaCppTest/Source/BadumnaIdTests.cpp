#include <iostream>
#include <sstream>
#include <boost/test/unit_test.hpp>

#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/DataTypes/String.h"

using namespace Badumna;
using std::wstringstream;

struct BadumnaIdFixture
{
    BadumnaIdFixture() 
    {
    }
    
    virtual ~BadumnaIdFixture() 
    { 
    }
};

BOOST_FIXTURE_TEST_SUITE(BadumnaBadumnaId, BadumnaIdFixture)

BOOST_AUTO_TEST_CASE(BadumnaIdEqualOperatorWorks)
{
    BadumnaId a = BadumnaRuntime::Instance().CreateBadumnaId("Open|127.0.0.1:1-1");
    BadumnaId b = BadumnaRuntime::Instance().CreateBadumnaId("Open|127.0.0.1:1-1");

    BOOST_REQUIRE(a == b);
}

BOOST_AUTO_TEST_CASE(BadumnaIdNotEqualOperatorWorks)
{
    BadumnaId none = BadumnaId::None();
    BadumnaId nextId = BadumnaRuntime::Instance().CreateBadumnaId("Open|127.0.0.1:1-1");

    BOOST_REQUIRE(none != nextId);
}

BOOST_AUTO_TEST_CASE(BadumnaIdCopyConstructorWorks)
{
    BadumnaId nextId = BadumnaRuntime::Instance().CreateBadumnaId("Open|127.0.0.1:1-1");
    BadumnaId copiedId(nextId);
    
    BOOST_REQUIRE(nextId == copiedId);
}

BOOST_AUTO_TEST_CASE(BadumnaIdAssignOperatorWorks)
{
    BadumnaId nextId1 = BadumnaRuntime::Instance().CreateBadumnaId("Open|127.0.0.1:1-1");
    BadumnaId nextId2 = BadumnaRuntime::Instance().CreateBadumnaId("Unknown|1.2.3.4:5-2");
    
    nextId2 = nextId1;

    BOOST_REQUIRE(nextId1 == nextId2);
}

BOOST_AUTO_TEST_CASE(BadumnaIdIsValidReturnsFalseByDefault)
{
    BadumnaId none = BadumnaId::None();
    
    BOOST_REQUIRE(!none.IsValid());
}

BOOST_AUTO_TEST_CASE(BadumnaIdGetNextUniqueIdGeneratesValidId)
{
    BadumnaId nextId = BadumnaRuntime::Instance().CreateBadumnaId("Open|127.0.0.1:1-1");

    BOOST_REQUIRE(nextId.IsValid());
}

BOOST_AUTO_TEST_CASE(BadumnaIdCanOutputToStream)
{
    BadumnaId nextId = BadumnaRuntime::Instance().CreateBadumnaId("Open|127.0.0.1:1-1");
    wstringstream s;

    s << nextId;

    BOOST_REQUIRE(nextId.ToString() == s.str().c_str());
}

BOOST_AUTO_TEST_CASE(ToInvariantIDStringReturnsStringRepresentationOfId)
{
    BadumnaId id1 = BadumnaRuntime::Instance().CreateBadumnaId("Open|127.0.0.1:1-1");
    BadumnaId id2 = BadumnaRuntime::Instance().CreateBadumnaId("Open|127.0.0.1:1-2");
    BadumnaId id_none = BadumnaId::None();

    String id1_string = id1.ToInvariantIDString();
    String id2_string = id2.ToInvariantIDString();
    String id_none_string = id_none.ToInvariantIDString();

    BOOST_REQUIRE(id1_string != id2_string);
    BOOST_REQUIRE(id1_string != id_none_string);
    BOOST_REQUIRE(id2_string != id_none_string);
}

BOOST_AUTO_TEST_SUITE_END() // BadumnaId