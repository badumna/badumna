#include <sstream>
#include <boost/thread.hpp>
#include <boost/test/unit_test.hpp>
#include "Badumna/DataTypes/BooleanArray.h"

using namespace Badumna;
using std::wstringstream;

struct BooleanArrayFixture
{
    BooleanArrayFixture() 
    {
    }
    
    virtual ~BooleanArrayFixture() 
    { 
    }
};

BOOST_FIXTURE_TEST_SUITE(BadumnaBooleanArray, BooleanArrayFixture)

BOOST_AUTO_TEST_CASE(BooleanArrayDefaultConstructorWorks)
{
    BooleanArray boolean_array;
    BOOST_CHECK_EQUAL(false, boolean_array.Any());
}

BOOST_AUTO_TEST_CASE(BooleanArraySimpleConstructorWorks)
{
    BooleanArray boolean_array(true);

    for(size_t i = 0; i < boolean_array.HighestUsedIndex(); ++i)
    {
        BOOST_CHECK_EQUAL(true, boolean_array[i]);
    }
}

BOOST_AUTO_TEST_CASE(BooleanArraySetValueWorks)
{
    BooleanArray boolean_array;
    BOOST_CHECK_EQUAL(false, boolean_array.Any());
    boolean_array.Set(10, true);
    BOOST_CHECK_EQUAL(true, boolean_array.Any());
    BOOST_CHECK_EQUAL(true, boolean_array[10]);

    for(size_t i = 0; i < 10; ++i)
    {
        BOOST_CHECK_EQUAL(false, boolean_array[i]);
    }
}

BOOST_AUTO_TEST_CASE(BooleanArraySetAllWorks)
{
    BooleanArray boolean_array;
    BOOST_CHECK_EQUAL(false, boolean_array.Any());

    boolean_array.SetAll(true);
    BOOST_CHECK_EQUAL(true, boolean_array.Any());
    for(size_t i = 0; i < boolean_array.HighestUsedIndex(); ++i)
    {
        BOOST_CHECK_EQUAL(true, boolean_array[i]);
    }

    boolean_array.SetAll(false);
    BOOST_CHECK_EQUAL(false, boolean_array.Any());
    for(size_t i = 0; i < boolean_array.HighestUsedIndex(); ++i)
    {
        BOOST_CHECK_EQUAL(false, boolean_array[i]);
    }
}

BOOST_AUTO_TEST_CASE(BooleanArrayOrOperationWorks)
{
    BooleanArray a1(false);
    BOOST_CHECK_EQUAL(false, a1.Any());

    BooleanArray a2(true);

    a1.Or(a2);
    for(size_t i = 0; i < a1.HighestUsedIndex(); ++i)
    {
        BOOST_CHECK_EQUAL(true, a1[i]);
    }
}

BOOST_AUTO_TEST_CASE(BooleanArrayIsCopyableAndAssignable)
{
    BooleanArray a(false);
    BooleanArray garbage(false);
    a.Set(1, true);
    a.Set(3, true);
    a.Set(5, true);
    garbage.Set(0, true);

    BooleanArray b(a);
    BooleanArray c = garbage;
    c = a;
    
    wstringstream sa, sb, sc;
    sa << a;
    sb << b;
    sc << c;

    BOOST_REQUIRE(sa.str() == sb.str());
    BOOST_REQUIRE(sa.str() == sc.str());

    // change the value of c won't affect a. 
    c.Set(0, true);
    BOOST_CHECK_EQUAL(true, c.Get(0));
    BOOST_CHECK_EQUAL(false, a.Get(0));
}

BOOST_AUTO_TEST_CASE(BooleanArrayCanOutputToStream)
{
    BooleanArray a;
    wstringstream s;
    
    s << a;
    BOOST_REQUIRE(a.ToString() == s.str().c_str());
}

BOOST_AUTO_TEST_CASE(TryParseWorksAsExpected)
{
    String p(L"01110");
    BooleanArray a = BooleanArray::TryParse(p);
    BOOST_CHECK_EQUAL(false, a.Get(0));
    BOOST_CHECK_EQUAL(true, a.Get(1));
    BOOST_CHECK_EQUAL(true, a.Get(2));
    BOOST_CHECK_EQUAL(true, a.Get(3));
    BOOST_CHECK_EQUAL(false, a.Get(4));
}

BOOST_AUTO_TEST_SUITE_END() // BooleanArray