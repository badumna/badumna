#include <boost/test/unit_test.hpp>

#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/Chat/ChatDelegates.h"
#include "Badumna/Chat/ChatStatus.h"
#include "ChatDelegateStub.h"
#include "ChatChannelStub.h"

using namespace Badumna;
using namespace BadumnaUnitTest;

struct ChatDelegateFixture
{
    ChatDelegateFixture() 
    {
        channel = BadumnaUnitTest::ChatChannelStub();
    }
    
    virtual ~ChatDelegateFixture() 
    { 
    }

    ChatChannelStub channel;
};

BOOST_FIXTURE_TEST_SUITE(BadumnaChatDelegate, ChatDelegateFixture)

BOOST_AUTO_TEST_CASE(MessageHandlerCanBeInvoked)
{
    ChatDelegateStub stub;
    ChatMessageHandler handler(&ChatDelegateStub::MessageHandler, stub);
    BOOST_CHECK_EQUAL(0, stub.ChatHandlerCalled());
    
    handler(&channel, BadumnaRuntime::Instance().CreateBadumnaId("Open|127.0.0.1:1-1"), L"test");
    BOOST_CHECK_EQUAL(1, stub.ChatHandlerCalled());

    handler(&channel, BadumnaRuntime::Instance().CreateBadumnaId("Open|127.0.0.1:1-2"), L"test");
    BOOST_CHECK_EQUAL(2, stub.ChatHandlerCalled());
}

BOOST_AUTO_TEST_CASE(PresenceHandlerCanBeInvoked)
{
    ChatDelegateStub stub;
    ChatPresenceHandler handler(&ChatDelegateStub::PresenceHandler, stub);
    BOOST_CHECK_EQUAL(0, stub.PresenceHandlerCalled());
    
    handler(&channel, BadumnaRuntime::Instance().CreateBadumnaId("Open|127.0.0.1:1-1"), L"test", ChatStatus_Online);
    BOOST_CHECK_EQUAL(1, stub.PresenceHandlerCalled());

    handler(&channel, BadumnaRuntime::Instance().CreateBadumnaId("Open|127.0.0.1:1-2"), L"test", ChatStatus_Online);
    BOOST_CHECK_EQUAL(2, stub.PresenceHandlerCalled());
}

BOOST_AUTO_TEST_CASE(InvitationHandlerCanBeInvoked)
{
    ChatDelegateStub stub;
    ChatInvitationHandler handler(&ChatDelegateStub::InvitationHandler, stub);
    BOOST_CHECK_EQUAL(0, stub.InvitationHandlerCalled());
    
    handler(channel.Id(), L"test");
    BOOST_CHECK_EQUAL(1, stub.InvitationHandlerCalled());

    handler(channel.Id(), L"test");
    BOOST_CHECK_EQUAL(2, stub.InvitationHandlerCalled());
}

BOOST_AUTO_TEST_CASE(ChatDelegatesAreCopibleAndAssignible)
{
    ChatDelegateStub stub;
    ChatDelegateStub stub2;
    ChatDelegateStub stub3;

    ChatInvitationHandler handler(&ChatDelegateStub::InvitationHandler, stub);
    ChatInvitationHandler handler2(&ChatDelegateStub::InvitationHandler, stub2);
    
    BOOST_CHECK_EQUAL(0, stub.InvitationHandlerCalled());
    BOOST_CHECK_EQUAL(0, stub2.InvitationHandlerCalled());
    
    ChatInvitationHandler copied_handler(handler);
    BOOST_CHECK_EQUAL(0, stub.InvitationHandlerCalled());
    copied_handler(channel.Id(), L"test");
    BOOST_CHECK_EQUAL(1, stub.InvitationHandlerCalled());

    ChatInvitationHandler assigned_handler(handler);
    assigned_handler = handler2;
    BOOST_CHECK_EQUAL(0, stub2.InvitationHandlerCalled());
    assigned_handler(channel.Id(), L"test");
    BOOST_CHECK_EQUAL(1, stub2.InvitationHandlerCalled());
}

BOOST_AUTO_TEST_SUITE_END() // ChatDelegates