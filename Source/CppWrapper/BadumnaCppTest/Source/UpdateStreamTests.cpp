#include <boost/test/unit_test.hpp>
#include "Badumna/DataTypes/InputStream.h"
#include "Badumna/DataTypes/OutputStream.h"

using namespace Badumna;

struct StreamFixture
{
    StreamFixture() 
    {
    }
    
    virtual ~StreamFixture() 
    { 
    }
};

BOOST_FIXTURE_TEST_SUITE(BadumnaStreams, StreamFixture)

BOOST_AUTO_TEST_CASE(StreamBasicReadWriteWorksFine)
{
    bool test_bool = false;
    char test_char = 'z';
    unsigned char test_unsigned_char = 'z';
    short test_short = 101;
    unsigned short test_unsigned_short = 101;
    int test_int = 1023;
    unsigned int test_unsigned_int = 1023;
    long test_long = 10000;
    unsigned long test_unsigned_long = 10000;
    float test_float = 1.5f;
    double test_double = 1.5;

    OutputStream output_stream;
    output_stream << test_bool;
    output_stream << test_char;
    output_stream << test_unsigned_char;
    output_stream << test_short;
    output_stream << test_unsigned_short;
    output_stream << test_int;
    output_stream << test_unsigned_int;
    output_stream << test_long;
    output_stream << test_unsigned_long;
    output_stream << test_float;
    output_stream << test_double;

    char *buffer = new char[output_stream.GetLength()];
    output_stream.GetBuffer(buffer, output_stream.GetLength());

    InputStream input_stream(buffer, output_stream.GetLength());
    
    bool bool_data;
    char char_data;
    unsigned char unsigned_char_data;
    short short_data;
    unsigned short unsigned_short_data ;
    int int_data;
    unsigned int unsigned_int_data;
    long long_data;
    unsigned long unsigned_long_data;
    float float_data;
    double double_data;

    input_stream >> bool_data;
    input_stream >> char_data;
    input_stream >> unsigned_char_data;
    input_stream >> short_data;
    input_stream >> unsigned_short_data;
    input_stream >> int_data;
    input_stream >> unsigned_int_data;
    input_stream >> long_data;
    input_stream >> unsigned_long_data;
    input_stream >> float_data;
    input_stream >> double_data;

    BOOST_CHECK_EQUAL(test_bool, bool_data);
    BOOST_CHECK_EQUAL(test_char, char_data);
    BOOST_CHECK_EQUAL(test_unsigned_char, unsigned_char_data);
    BOOST_CHECK_EQUAL(test_short, short_data);
    BOOST_CHECK_EQUAL(test_unsigned_short, unsigned_short_data);
    BOOST_CHECK_EQUAL(test_int, int_data);
    BOOST_CHECK_EQUAL(test_unsigned_int, unsigned_int_data);
    BOOST_CHECK_EQUAL(test_long, long_data);
    BOOST_CHECK_EQUAL(test_unsigned_long, unsigned_long_data);
    BOOST_CHECK_EQUAL(test_float, float_data);
    BOOST_CHECK_EQUAL(test_double, double_data);

    delete[] buffer;
}

BOOST_AUTO_TEST_CASE(OutputStreamMaintainsCorrectDataLength)
{
    int test_int = 1023;
    char test_ch = 'z';
    bool test_bool = false;

    OutputStream output_stream;
    BOOST_CHECK_EQUAL(0, output_stream.GetLength());

    output_stream << test_int;
    output_stream << test_ch;
    output_stream << test_bool;
    
    size_t expected_length = sizeof(test_int) + sizeof(test_ch) + sizeof(test_bool);
    BOOST_CHECK_EQUAL(expected_length, output_stream.GetLength());
}

BOOST_AUTO_TEST_CASE(StreamIOCanBeChained)
{
    int test_int = 1023;
    char test_ch = 'z';
    bool test_bool = false;

    OutputStream output_stream;
    output_stream << test_int << test_ch << test_bool;
    
    size_t expected_length = sizeof(test_int) + sizeof(test_ch) + sizeof(test_bool);
    BOOST_CHECK_EQUAL(expected_length, output_stream.GetLength());

    char *buffer = new char[output_stream.GetLength()];
    output_stream.GetBuffer(buffer, output_stream.GetLength());

    InputStream input_stream(buffer, output_stream.GetLength());

    int int_data;
    char char_data;
    bool bool_data;

    input_stream >> int_data >> char_data >> bool_data;

    BOOST_CHECK_EQUAL(test_int, int_data);
    BOOST_CHECK_EQUAL(test_ch, char_data);
    BOOST_CHECK_EQUAL(test_bool, bool_data);

    delete[] buffer;
}

BOOST_AUTO_TEST_CASE(StreamAcceptsConstVariables)
{
    const int test_int = 1023;
    const char test_ch = 'z';
    const bool test_bool = false;

    OutputStream output_stream;
    output_stream << test_int << test_ch << test_bool;
    
    size_t expected_length = sizeof(test_int) + sizeof(test_ch) + sizeof(test_bool);
    BOOST_CHECK_EQUAL(expected_length, output_stream.GetLength());
}

BOOST_AUTO_TEST_SUITE_END() // Streams