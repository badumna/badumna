//---------------------------------------------------------------------------------
// <copyright file="ReplicationDelegateStub.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef REPLICATION_DELEGATE_STUB_H
#define REPLICATION_DELEGATE_STUB_H

#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/Replication/SpatialReplicaDelegate.h"
#include "ReplicaObject.h"

using namespace Badumna;
using namespace BadumnaUnitTest;

namespace BadumnaUnitTest
{
    class ReplicationDelegateStub
    {
    public:
        ReplicationDelegateStub()
            : create_count(0),
            remove_count(0),
            replica(NULL)
        {
        }

        ~ReplicationDelegateStub()
        {
            if(replica != NULL)
            {
                delete replica;
            }
        }

        ISpatialReplica *Create(NetworkScene const &, BadumnaId const &, unsigned int)
        {
            create_count++;

            if(replica != NULL)
            {
                delete replica;
                replica = NULL;
            }

            replica = new ReplicaObject();

            return replica;
        }

        void Remove(NetworkScene const &, ISpatialReplica const &)
        {
            remove_count++;
        }

        inline int GetCreateCount() const
        {
            return create_count;
        }

        inline int GetRemoveCount() const
        {
            return remove_count;
        }

    private:
        int create_count;
        int remove_count;

        ISpatialReplica *replica;

        DISALLOW_COPY_AND_ASSIGN(ReplicationDelegateStub);
    };
}

#endif // REPLICATION_DELEGATE_STUB_H