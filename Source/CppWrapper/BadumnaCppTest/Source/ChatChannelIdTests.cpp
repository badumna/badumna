#include <iostream>

#include <boost/test/unit_test.hpp>
#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Chat/ChatChannelId.h"
#include "Badumna/Chat/ChatChannelType.h"

#include "ChatChannelStub.h"

using namespace Badumna;
using namespace BadumnaUnitTest;
using std::stringstream;

struct ChatChannelIdFixture
{
    ChatChannelIdFixture() 
    {
        channel = BadumnaUnitTest::ChatChannelStub();
    }
    
    virtual ~ChatChannelIdFixture() 
    { 
    }

    ChatChannelStub channel;
};

BOOST_FIXTURE_TEST_SUITE(BadumnaChatChannelId, ChatChannelIdFixture)

BOOST_AUTO_TEST_CASE(ChatChannelIdIsAssignable)
{
    ChatChannelId id = channel.Id();
    BOOST_CHECK_EQUAL(channel.Id().Type(), id.Type());
}

BOOST_AUTO_TEST_CASE(ChatChannelIdIsCopyable)
{
    ChatChannelId id(channel.Id());
    BOOST_CHECK_EQUAL(channel.Id().Type(), id.Type());
}

BOOST_AUTO_TEST_CASE(ChatChannelIdEqualsWorkAsExpected)
{
    ChatChannelId id1 = channel.Id();
    ChatChannelId id2(channel.Id());

    BOOST_CHECK_EQUAL(true, id1 == id2);
    BOOST_CHECK_EQUAL(false, id1 != id2);
}

BOOST_AUTO_TEST_CASE(ChatChannelIdNameCanBeSet)
{
    String name = L"test_name";
    ChatChannelId id = channel.Id();
    id.SetName(name);

    BOOST_REQUIRE(name == id.Name());
}

BOOST_AUTO_TEST_CASE(ChatChannelIdToStringWorks)
{
    ChatChannelId id = channel.Id();
    String name = id.ToString();

    BOOST_REQUIRE(name.Length());
}

BOOST_AUTO_TEST_SUITE_END() // ChatChannelId