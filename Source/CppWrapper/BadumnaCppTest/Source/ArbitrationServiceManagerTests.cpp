#include <iostream>
#include <boost/test/unit_test.hpp>

#include <string>
#include "ArbitrationDelegateStub.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Arbitration/ArbitrationDelegates.h"
#include "Badumna/Arbitration/ArbitrationServiceManager.h"

using std::string;
using namespace Badumna;
using namespace BadumnaUnitTest;

struct ArbitrationServiceManagerFixture
{
    ArbitrationDelegateStub stub;
    ArbitrationDelegateStub stub2;
    ArbitrationServiceManager manager;
    String id;
    ArbitrationConnectionDelegate callback1;
    ArbitrationConnectionFailureDelegate callback2;
    ArbitrationServerMessageDelegate callback3;

    ArbitrationConnectionDelegate callback4;
    ArbitrationConnectionFailureDelegate callback5;
    ArbitrationServerMessageDelegate callback6;

    ArbitrationClientMessageDelegate callback11;
    ArbitrationClientDisconnectDelegate callback12;

    ArbitrationClientMessageDelegate callback13;
    ArbitrationClientDisconnectDelegate callback14;

    int count1;
    int count2;
    int count3;

    int count4;
    int count5;
    int count6;

    int count11;
    int count12;

    int count13;
    int count14;

    ArbitrationServiceManagerFixture()
        : stub(),
        stub2(),
        manager(),
        id(L"my_id"),
        callback1(&ArbitrationDelegateStub::ConnectionDelegate, stub),
        callback2(&ArbitrationDelegateStub::ConnectionFailureDelegate, stub),
        callback3(&ArbitrationDelegateStub::ServerMessageDeledate, stub),

        callback4(&ArbitrationDelegateStub::ConnectionDelegate, stub2),
        callback5(&ArbitrationDelegateStub::ConnectionFailureDelegate, stub2),
        callback6(&ArbitrationDelegateStub::ServerMessageDeledate, stub2),

        callback11(&ArbitrationDelegateStub::ClientMessageDelegate, stub),
        callback12(&ArbitrationDelegateStub::ClientDisconnectDelegate, stub),

        callback13(&ArbitrationDelegateStub::ClientMessageDelegate, stub2),
        callback14(&ArbitrationDelegateStub::ClientDisconnectDelegate, stub2),

        count1(stub.GetConnectionDelegateCount()),
        count2(stub.GetConnectionFailureDelegateCount()),
        count3(stub.GetServerMessageDelegateCount()),

        count4(stub2.GetConnectionDelegateCount()),
        count5(stub2.GetConnectionFailureDelegateCount()),
        count6(stub2.GetServerMessageDelegateCount()),

        count11(stub.GetClientMessageDelegateCount()),
        count12(stub.GetClientDisconnectDelegateCount()),

        count13(stub2.GetClientMessageDelegateCount()),
        count14(stub2.GetClientDisconnectDelegateCount())
    {
        manager.SetClientDelegates(id, callback1, callback2, callback3);
        manager.SetServerDelegates(id, callback11, callback12);
    }
    
    virtual ~ArbitrationServiceManagerFixture() 
    {
    }
};

BOOST_FIXTURE_TEST_SUITE(BadumnaArbitrationServiceManager, ArbitrationServiceManagerFixture)

BOOST_AUTO_TEST_CASE(ConnectionDelegateCanBeInvoked)
{
    manager.InvokeConnectionDelegate(id, Badumna::ServiceConnection_Success);
    BOOST_CHECK_EQUAL(count1 + 1, stub.GetConnectionDelegateCount());
}

BOOST_AUTO_TEST_CASE(ConnectionFailureDelegateCanBeInvoked)
{
    manager.InvokeConnectionFailureDelegate(id);
    BOOST_CHECK_EQUAL(count2 + 1, stub.GetConnectionFailureDelegateCount());
}

BOOST_AUTO_TEST_CASE(ServerMessageDelegateCanBeInvoked)
{
    char buffer[16];
    InputStream s(buffer, 16);
    manager.InvokeServerMessageDelegate(id, &s);
    BOOST_CHECK_EQUAL(count3 + 1, stub.GetServerMessageDelegateCount());
}

BOOST_AUTO_TEST_CASE(ClientMessageDelegateCanBeInvoked)
{
    char buffer[16];
    InputStream s(buffer, 16);
    manager.InvokeClientMessageDelegate(id, 1, &s);
    BOOST_CHECK_EQUAL(count11 + 1, stub.GetClientMessageDelegateCount());
}

BOOST_AUTO_TEST_CASE(ClientDisconnectDelegateCanBeInvoked)
{
    manager.InvokeClientDisconnectDelegate(id, 1);
    BOOST_CHECK_EQUAL(count12 + 1, stub.GetClientDisconnectDelegateCount());
}

BOOST_AUTO_TEST_CASE(SetClientDelegatesCanReplaceDelegates)
{
    char buffer[16];
    InputStream s(buffer, 16);
    manager.SetClientDelegates(id, callback4, callback5, callback6);
    manager.InvokeConnectionDelegate(id, Badumna::ServiceConnection_Success);
    manager.InvokeConnectionFailureDelegate(id);
    manager.InvokeServerMessageDelegate(id, &s);

    // not invoked on the old stub
    BOOST_CHECK_EQUAL(count1, stub.GetConnectionDelegateCount());
    BOOST_CHECK_EQUAL(count2, stub.GetConnectionFailureDelegateCount());
    BOOST_CHECK_EQUAL(count3, stub.GetServerMessageDelegateCount());

    BOOST_CHECK_EQUAL(count4 + 1, stub2.GetConnectionDelegateCount());
    BOOST_CHECK_EQUAL(count5 + 1, stub2.GetConnectionFailureDelegateCount());
    BOOST_CHECK_EQUAL(count6 + 1, stub2.GetServerMessageDelegateCount());
}

BOOST_AUTO_TEST_CASE(SetServerDelegatesCanReplaceDelegates)
{
    char buffer[16];
    InputStream s(buffer, 16);
    manager.SetServerDelegates(id, callback13, callback14);
    manager.InvokeClientMessageDelegate(id, 1, &s);
    manager.InvokeClientDisconnectDelegate(id, 1);

    // old delegates not invoked
    BOOST_CHECK_EQUAL(count11, stub.GetClientMessageDelegateCount());
    BOOST_CHECK_EQUAL(count12, stub.GetClientDisconnectDelegateCount());

    // new delegates are invoked
    BOOST_CHECK_EQUAL(count13 + 1, stub2.GetClientMessageDelegateCount());
    BOOST_CHECK_EQUAL(count14 + 1, stub2.GetClientDisconnectDelegateCount());
}

BOOST_AUTO_TEST_SUITE_END() // ArbitrationDelegates