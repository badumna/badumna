#include <boost/test/unit_test.hpp>
#include "Badumna/Core/ExceptionDetails.h"

using namespace Badumna;

struct ExceptionDetailsFixture
{
    ExceptionDetailsFixture() 
    {
    }
    
    virtual ~ExceptionDetailsFixture() 
    { 
    }
};

BOOST_FIXTURE_TEST_SUITE(BadumnaExceptionDetails, ExceptionDetailsFixture)

BOOST_AUTO_TEST_CASE(SimpleConstructorWorks)
{
    ExceptionDetails details;

    BOOST_REQUIRE(details.method_name.Length() == 0);
    BOOST_REQUIRE(details.message.Length() == 0);
    BOOST_REQUIRE(details.managed_exception_type_name.Length() == 0);
}

BOOST_AUTO_TEST_CASE(ExceptionDetailsObjectIsCopyable)
{
    ExceptionDetails details;
    details.method_name = "method";
    details.message = "test_message";

    ExceptionDetails copyed_details(details);
    BOOST_REQUIRE(copyed_details.managed_exception_type_name.Length() == 0);
    BOOST_REQUIRE(copyed_details.message == "test_message");
    BOOST_REQUIRE(copyed_details.method_name == "method");
}   

BOOST_AUTO_TEST_CASE(ExceptionDetailsObjectIsAssignable)
{
    ExceptionDetails details;
    details.method_name = "method";
    details.message = "test_message";
    
    ExceptionDetails copyed_details;
    copyed_details = details;
    BOOST_REQUIRE(copyed_details.managed_exception_type_name.Length() == 0);
    BOOST_REQUIRE(copyed_details.message == "test_message");
    BOOST_REQUIRE(copyed_details.method_name == "method");
}

BOOST_AUTO_TEST_SUITE_END()