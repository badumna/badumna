#include <iostream>
#include <memory>
#include <boost/test/unit_test.hpp>

#include "Badumna/DataTypes/BooleanArrayImpl.h"
#include "Badumna/Core/MonoPInvokes.h"
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/Replication/OriginalWrapper.h"
#include "Badumna/Replication/ReplicaWrapper.h"
#include "Badumna/Replication/SpatialEntityStateSegment.h"

#include "ArbitrationDelegateStub.h"
#include "Badumna/Arbitration/ArbitrationDelegates.h"
#include "Badumna/Core/FacadeServiceManager.h"
#include "Badumna/Chat/ChatChannelId.h"
#include "Badumna/Chat/ChatChannelIdImpl.h"
#include "Badumna/Chat/ChatDelegates.h"
#include "Badumna/Chat/ChatStatus.h"
#include "Badumna/Chat/ChatSession.h"
#include "Badumna/Chat/ChatSessionManager.h"
#include "Badumna/DataTypes/BadumnaIdImpl.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

#include "ChatDelegateStub.h"
#include "ConnectivityDelegateStub.h"
#include "LocalObject.h"
#include "ReplicaObject.h"
#include "DeadReckonableReplica.h"

using std::auto_ptr;
using namespace Badumna;
using namespace BadumnaUnitTest;

namespace
{
    bool ContainsInternalCall(vector<pair<String, void const *> > details, String const &name)
    {
        vector<pair<String, void const *> >::iterator iter = details.begin();
        while(iter != details.end())
        {
            if(iter->first == name)
            {
                return true;
            }

            iter++;
        }

        return false;
    }
}

struct MonoPInvokesFixture
{
    NetworkFacade* facade;
    LocalObject* local_object;
    ReplicaObject* replica_object;

    BadumnaIdImpl *b;
    MonoObject *sender_id;


    MonoPInvokesFixture() 
        : facade(NULL),
        local_object(new LocalObject()),
        replica_object(new ReplicaObject()),
        b(BadumnaRuntime::Instance().CreateBadumnaIdImpl()),
        sender_id(b->GetManagedMonoObject())
    {
    }
    
    virtual ~MonoPInvokesFixture() 
    {
        delete local_object;
        delete replica_object;
        delete b;
    }

private:
    DISALLOW_COPY_AND_ASSIGN(MonoPInvokesFixture);
};

BOOST_FIXTURE_TEST_SUITE(BadumnaMonoPInvokes, MonoPInvokesFixture)

BOOST_AUTO_TEST_CASE(GetServiceManagerReturnsTheManager)
{
    ReplicationServiceManager *p1(MonoPInvokes::ReplicationManager());
    BOOST_REQUIRE(p1 != NULL);

    ChatSessionManager *p2(MonoPInvokes::ChatManager());
    BOOST_REQUIRE(p2 != NULL);

    ArbitrationServiceManager *p3(MonoPInvokes::ArbitrationManager());
    BOOST_REQUIRE(p3 != NULL);
}


BOOST_AUTO_TEST_CASE(InvokeSerializeActuallyInvokesSerializeDelegate)
{
    String original_id(L"original_id");
    OutputStream stream;
    auto_ptr<BooleanArrayImpl> boolean_array_impl(BadumnaRuntime::Instance().CreateBooleanArrayImpl());
    MonoObject *boolean_array_object = boolean_array_impl->GetManagedMonoObject();

    OriginalWrapper *original_wrapper = BadumnaRuntime::Instance().CreateOriginalWrapper(local_object);
    original_wrapper->SetUniqueId(original_id);

    MonoPInvokes::EntityService()->AddOriginal(original_id, original_wrapper);
    int count = local_object->GetSerializeCounter();

    ::InvokeSerialize(MonoRuntime::GetString(original_id), boolean_array_object);
    BOOST_CHECK_EQUAL(count + 1, local_object->GetSerializeCounter());
}

BOOST_AUTO_TEST_CASE(InvokeDeserializeActuallyInvokesDeserializeDelegate)
{
    char data[16];
    String replica_id(L"replica_id");

    auto_ptr<BooleanArrayImpl> boolean_array_impl(BadumnaRuntime::Instance().CreateBooleanArrayImpl());
    MonoObject *boolean_array_object = boolean_array_impl->GetManagedMonoObject();

    InputStream stream(data, 16); 

    ReplicaWrapper *replica_wrapper = BadumnaRuntime::Instance().CreateReplicaWrapper(replica_id, replica_object);
    MonoPInvokes::EntityService()->AddReplica(replica_id, replica_wrapper);
    int count = replica_object->GetDeserializeCounter();

    ::InvokeDeserialize(
        MonoRuntime::GetString(replica_id), 
        boolean_array_object, 
        MonoRuntime::GetMonoByteArray(data, 16), 
        1);

    BOOST_CHECK_EQUAL(count + 1, replica_object->GetDeserializeCounter());
}


BOOST_AUTO_TEST_CASE(InvokeHandleOriginalEventInvokesTheEventHandler)
{
    char data[16];
    String original_id(L"original_id_handle_event");
        
    OriginalWrapper *original_wrapper = BadumnaRuntime::Instance().CreateOriginalWrapper(local_object);
    original_wrapper->SetUniqueId(original_id);

    MonoPInvokes::EntityService()->AddOriginal(original_id, original_wrapper);
    int count = local_object->GetEventCounter();

    ::InvokeHandleOriginalEvent(MonoRuntime::GetString(original_id), MonoRuntime::GetMonoByteArray(data, 16));

    BOOST_CHECK_EQUAL(count + 1, local_object->GetEventCounter());
}


BOOST_AUTO_TEST_CASE(InvokeHandleReplicaEventInvokesTheEventHandler)
{
    char data[16];
    String replica_id(L"replica_id_handle_event");
    
    ReplicaWrapper *replica_wrapper = BadumnaRuntime::Instance().CreateReplicaWrapper(replica_id, replica_object);
    MonoPInvokes::EntityService()->AddReplica(replica_id, replica_wrapper);
    
    int count = replica_object->GetEventCounter();

    ::InvokeHandleReplicaEvent(MonoRuntime::GetString(replica_id), MonoRuntime::GetMonoByteArray(data, 16));

    BOOST_CHECK_EQUAL(count + 1, replica_object->GetEventCounter());
}

BOOST_AUTO_TEST_CASE(InvokeSetPositionPropertyActuallySetsPosition)
{
    String replica_id(L"replica_id_set_position");
    ReplicaWrapper *replica_wrapper = BadumnaRuntime::Instance().CreateReplicaWrapper(replica_id, replica_object);
    MonoPInvokes::EntityService()->AddReplica(replica_id, replica_wrapper);

    replica_object->SetPosition(Vector3(2.3f, 3.4f, 4.5f));

    float x = 12.3f;
    float y = -3.4f;
    float z = 5.6f;

    ::InvokeSetPositionProperty(MonoRuntime::GetString(replica_id), Badumna::StateSegment_Position, x, y, z);

    BOOST_CHECK_CLOSE(x, replica_object->Position().GetX(), 0.01f);
    BOOST_CHECK_CLOSE(y, replica_object->Position().GetY(), 0.01f);
    BOOST_CHECK_CLOSE(z, replica_object->Position().GetZ(), 0.01f);
}

BOOST_AUTO_TEST_CASE(InvokeSetVelocityPropertyActuallySetsVelocity)
{
    String replica_id(L"deadreckonable_replica");
    
    auto_ptr<DeadReckonableReplica> replica(new DeadReckonableReplica());
    ReplicaWrapper *replica_wrapper = BadumnaRuntime::Instance().CreateReplicaWrapper(replica_id, replica.get());
    MonoPInvokes::EntityService()->AddReplica(replica_id, replica_wrapper);

    float x = 12.3f;
    float y = -3.4f;
    float z = 5.6f;

    ::InvokeSetVelocityProperty(MonoRuntime::GetString(replica_id), x, y, z);

    BOOST_CHECK_CLOSE(x, replica->Velocity().GetX(), 0.01f);
    BOOST_CHECK_CLOSE(y, replica->Velocity().GetY(), 0.01f);
    BOOST_CHECK_CLOSE(z, replica->Velocity().GetZ(), 0.01f);
}

BOOST_AUTO_TEST_CASE(InvokeAttemptMovementActuallyMovesTheReplica)
{
    String replica_id(L"deadreckonable_replica2");
    
    auto_ptr<DeadReckonableReplica> replica(new DeadReckonableReplica());
    ReplicaWrapper *replica_wrapper = BadumnaRuntime::Instance().CreateReplicaWrapper(replica_id, replica.get());
    MonoPInvokes::EntityService()->AddReplica(replica_id, replica_wrapper);

    float x = 12.3f;
    float y = -3.4f;
    float z = 5.6f;

    ::InvokeAttemptMovement(MonoRuntime::GetString(replica_id), x, y, z);

    BOOST_CHECK_CLOSE(x, replica->Position().GetX(), 0.01f);
    BOOST_CHECK_CLOSE(y, replica->Position().GetY(), 0.01f);
    BOOST_CHECK_CLOSE(z, replica->Position().GetZ(), 0.01f);
}

BOOST_AUTO_TEST_CASE(InvokeArbitrationClientDelegateActuallyInvokesTheDelegate)
{
    char data[16];
    String client_delegate_id(L"client_delegate_id");

    ArbitrationDelegateStub stub;
    ArbitrationConnectionDelegate callback1(&ArbitrationDelegateStub::ConnectionDelegate, stub);
    ArbitrationConnectionFailureDelegate callback2(&ArbitrationDelegateStub::ConnectionFailureDelegate, stub);
    ArbitrationServerMessageDelegate callback3(&ArbitrationDelegateStub::ServerMessageDeledate, stub);

    MonoPInvokes::ArbitrationManager()->SetClientDelegates(client_delegate_id, callback1, callback2, callback3);
    
    int count1(stub.GetConnectionDelegateCount());
    int count2(stub.GetConnectionFailureDelegateCount());
    int count3(stub.GetServerMessageDelegateCount());

    ::InvokeArbitrationConnectionDelegate(MonoRuntime::GetString(client_delegate_id), 1);
    ::InvokeArbitrationConnectionFailureDelegate(MonoRuntime::GetString(client_delegate_id));
    ::InvokeArbitrationServerMessageDelegate(
        MonoRuntime::GetString(client_delegate_id), 
        MonoRuntime::GetMonoByteArray(data, 16));

    BOOST_CHECK_EQUAL(count1 + 1, stub.GetConnectionDelegateCount());
    BOOST_CHECK_EQUAL(count2 + 1, stub.GetConnectionFailureDelegateCount());
    BOOST_CHECK_EQUAL(count3 + 1, stub.GetServerMessageDelegateCount());
}

BOOST_AUTO_TEST_CASE(InvokeArbitrationServerDelegateActuallyInvokesTheDelegate)
{
    char data[16];
    String server_delegate_id(L"server_delegate_id");
    ArbitrationDelegateStub stub;
    ArbitrationClientMessageDelegate callback1(&ArbitrationDelegateStub::ClientMessageDelegate, stub);
    ArbitrationClientDisconnectDelegate callback2(&ArbitrationDelegateStub::ClientDisconnectDelegate, stub);

    MonoPInvokes::ArbitrationManager()->SetServerDelegates(server_delegate_id, callback1, callback2);

    int count1(stub.GetClientMessageDelegateCount());
    int count2(stub.GetClientDisconnectDelegateCount());

    ::InvokeArbitrationClientMessageDelegate(
        MonoRuntime::GetString(server_delegate_id), 
        1, 
        MonoRuntime::GetMonoByteArray(data, 16));

    ::InvokeArbitrationClientDisconnectDelegate(MonoRuntime::GetString(server_delegate_id), 1);

    BOOST_CHECK_EQUAL(count1 + 1, stub.GetClientMessageDelegateCount());
    BOOST_CHECK_EQUAL(count2 + 1, stub.GetClientDisconnectDelegateCount());
}

BOOST_AUTO_TEST_CASE(InvokeOnlineEventHandlerActuallyInvokesTheDelegate)
{
    ConnectivityDelegateStub stub;
    ConnectivityStatusDelegate d(&ConnectivityDelegateStub::OnlineEventHandler, stub);

    MonoPInvokes::FacadeManager()->SetOnlineEventDelegate(d);
    int count = stub.GetOnlineCount();

    ::InvokeOnlineEventDelegate();
    BOOST_CHECK_EQUAL(count + 1, stub.GetOnlineCount());
}

BOOST_AUTO_TEST_CASE(InvokeOfflineEventHandlerActuallyInvokesTheDelegate)
{
    ConnectivityDelegateStub stub;
    ConnectivityStatusDelegate d(&ConnectivityDelegateStub::OfflineEventHandler, stub);

    MonoPInvokes::FacadeManager()->SetOfflineEventDelegate(d);
    int count = stub.GetOfflineCount();

    ::InvokeOfflineEventDelegate();
    BOOST_CHECK_EQUAL(count + 1, stub.GetOfflineCount());
}

BOOST_AUTO_TEST_CASE(InvokeAddressChangedEventHandlerActuallyInvokesTheDelegate)
{
    ConnectivityDelegateStub stub;
    ConnectivityStatusDelegate d(&ConnectivityDelegateStub::AddressChangedEventHandler, stub);

    MonoPInvokes::FacadeManager()->SetAddressChangedEventDelegate(d);
    int count = stub.GetAddressChangedCount();

    ::InvokeAddressChangedEventDelegate();
    BOOST_CHECK_EQUAL(count + 1, stub.GetAddressChangedCount());
}

BOOST_AUTO_TEST_CASE(AllInternalCallsAreRegistered)
{
    vector<pair<String, void const *> > details;
    MonoPInvokes::GetInternalCallDetails(&details);

    BOOST_REQUIRE(ContainsInternalCall(details, "SpatialEntityDelegates::CallCreateSpatialReplica"));
    BOOST_REQUIRE(ContainsInternalCall(details, "SpatialEntityDelegates::CallRemoveSpatialReplica"));
    BOOST_REQUIRE(ContainsInternalCall(details, "SpatialReplicaStub::CallDeserialize"));
    BOOST_REQUIRE(ContainsInternalCall(details, "SpatialOriginalStub::CallSerialize"));
    BOOST_REQUIRE(ContainsInternalCall(details, "SpatialReplicaStub::SetProperty"));
    BOOST_REQUIRE(ContainsInternalCall(details, "SpatialReplicaStub::SetPositionProperty"));

    BOOST_REQUIRE(ContainsInternalCall(details, "SpatialOriginalStub::CallOriginalHandleEvent"));
    BOOST_REQUIRE(ContainsInternalCall(details, "SpatialReplicaStub::CallReplicaHandleEvent"));

    BOOST_REQUIRE(ContainsInternalCall(details, "DeadReckonableReplicaStub::CallAttemptMovement"));
    BOOST_REQUIRE(ContainsInternalCall(details, "DeadReckonableReplicaStub::SetVelocityProperty"));

    BOOST_REQUIRE(ContainsInternalCall(details, "ChatSessionStub::CallMessageHandler"));
    BOOST_REQUIRE(ContainsInternalCall(details, "ChatSessionStub::CallPresenceHandler"));
    BOOST_REQUIRE(ContainsInternalCall(details, "ChatSessionStub::CallInvitationHandler"));
    
    BOOST_REQUIRE(ContainsInternalCall(details, "ArbitrationClientStub::CallConnectionResultHandler"));
    BOOST_REQUIRE(ContainsInternalCall(details, "ArbitrationClientStub::CallConnectionFailureHandler"));
    BOOST_REQUIRE(ContainsInternalCall(details, "ArbitrationClientStub::CallServerMessageHandler"));
    BOOST_REQUIRE(ContainsInternalCall(details, "ArbitrationServerStub::CallClientMessageHandler"));
    BOOST_REQUIRE(ContainsInternalCall(details, "ArbitrationServerStub::CallClientDisconnectHandler"));

    BOOST_REQUIRE(ContainsInternalCall(details, "NetworkFacadeStub::CallOnlineEventHandler"));
    BOOST_REQUIRE(ContainsInternalCall(details, "NetworkFacadeStub::CallOfflineEventHandler"));
    BOOST_REQUIRE(ContainsInternalCall(details, "NetworkFacadeStub::CallAddressChangedEventHandler"));

    BOOST_REQUIRE(ContainsInternalCall(details, "DistributedControllerStub::CallCreateController"));
    BOOST_REQUIRE(ContainsInternalCall(details, "DistributedControllerStub::CallTakeControlOfDCEntity"));
    BOOST_REQUIRE(ContainsInternalCall(details, "DistributedControllerStub::CallCreateDCSpatialReplica"));
    BOOST_REQUIRE(ContainsInternalCall(details, "DistributedControllerStub::CallRemoveDCSpatialReplica"));
    BOOST_REQUIRE(ContainsInternalCall(details, "DistributedControllerStub::CallDCCheckpoint"));
    BOOST_REQUIRE(ContainsInternalCall(details, "DistributedControllerStub::CallDCRecover"));
    BOOST_REQUIRE(ContainsInternalCall(details, "DistributedControllerStub::CallDCRecover2"));
    BOOST_REQUIRE(ContainsInternalCall(details, "DistributedControllerStub::CallDCProcess"));
    BOOST_REQUIRE(ContainsInternalCall(details, "DistributedControllerStub::CallDCWake"));
    BOOST_REQUIRE(ContainsInternalCall(details, "DistributedControllerStub::CallDCSleep"));
    BOOST_REQUIRE(ContainsInternalCall(details, "DistributedControllerStub::CallDCShutdown"));
    BOOST_REQUIRE(ContainsInternalCall(details, "Badumna.Controllers.ControllerInitializerHelper::CallIsRequiredControllerNameRegistered"));
    
    BOOST_REQUIRE(ContainsInternalCall(details, "StreamingManagerStub::CallStreamSendComplete"));
    BOOST_REQUIRE(ContainsInternalCall(details, "StreamingManagerStub::CallStreamRequestHandler"));
    BOOST_REQUIRE(ContainsInternalCall(details, "StreamRequestStub::CallStreamReceiveComplete"));
}

BOOST_AUTO_TEST_SUITE_END() // CallbackTestSuite