#include <boost/test/unit_test.hpp>
#include "Badumna/Streaming/ReceiveStreamRequestDelegate.h"

using namespace Badumna;

class StreamRequestDelegateStub
{
public:
    StreamRequestDelegateStub()
        : counter(0)
    {
    }

    void Request(StreamRequest *)
    {
        counter++;
    }

    int GetCounter() const
    {
        return counter;
    }

private:
    int counter;
};

int g_receive_request_delegate_counter = 0;

void StreamRequestDelegateFunction(StreamRequest *)
{
    g_receive_request_delegate_counter++;
}

struct StreamRequestDelegateFixture
{
    StreamRequestDelegateFixture()
    {
    }
    
    virtual ~StreamRequestDelegateFixture() 
    {
    }
};

BOOST_FIXTURE_TEST_SUITE(BadumnaStreamRequestDelegate, StreamRequestDelegateFixture)

BOOST_AUTO_TEST_CASE(StreamRequestDelegateCanBeInvoked)
{
    StreamRequestDelegateStub stub;
    int counter = stub.GetCounter();
    ReceiveStreamRequestDelegate d1(&StreamRequestDelegateStub::Request, stub);
    d1(NULL);
    BOOST_CHECK_EQUAL(counter + 1, stub.GetCounter());
}

BOOST_AUTO_TEST_CASE(StreamRequestDelegateWithFreeFuncCanBeInvoked)
{
    int counter = g_receive_request_delegate_counter;
    ReceiveStreamRequestDelegate d1(StreamRequestDelegateFunction);
    d1(NULL);
    BOOST_CHECK_EQUAL(counter + 1, g_receive_request_delegate_counter);
}

BOOST_AUTO_TEST_CASE(StreamRequestDelegateIsCopyableAndAssignable)
{
    StreamRequestDelegateStub stub1;
    StreamRequestDelegateStub stub2;

    int counter = stub1.GetCounter();
    ReceiveStreamRequestDelegate d1(&StreamRequestDelegateStub::Request, stub1);
    ReceiveStreamRequestDelegate d2(&StreamRequestDelegateStub::Request, stub2);
    ReceiveStreamRequestDelegate d3(d2);

    d2 = d1;
    d2(NULL);
    BOOST_CHECK_EQUAL(counter + 1, stub1.GetCounter());
}

BOOST_AUTO_TEST_SUITE_END()