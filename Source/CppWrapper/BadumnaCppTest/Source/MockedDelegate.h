//---------------------------------------------------------------------------------
// <copyright file="MockedDelegate.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_MOCKEDDELEGATE_H
#define BADUMNA_MOCKEDDELEGATE_H

#include "Badumna/Core/Delegate.h"

namespace BadumnaUnitTest
{
    // class used for testing purpose. 
    class Myint
    {
    public:
        Myint(int v) : value(v)
        {
        }

        int GetValue() const
        {
            return this->value;
        }

    private:
        int value;
    };

    // the class which actually provides the delegate methods
    class Delegate1Tester
    {
    public:
        Delegate1Tester()
        {
        }

        ~Delegate1Tester()
        {
        }

        static Myint* GetDataStaticMethod(Myint const &input)
        {
            Myint *result = new Myint(input.GetValue() + 1);
            return result;
        }

        Myint* GetDataMemberMethod(Myint const &input)
        {
            Myint *result = new Myint(input.GetValue() + 1);
            return result;
        }
    };

    // the delegate type
    class MockedDelegate1 : public Badumna::Delegate1<Myint *, Myint const &>
    {
    public:
        template<typename K>
        MockedDelegate1(Myint* (K::*mfpointer)(Myint const &), K& object) 
            : Badumna::Delegate1<Myint *, Myint const &>(mfpointer, object)
        {
        }

        MockedDelegate1(Myint* (*mfpointer)(Myint const &)) 
            : Badumna::Delegate1<Myint *, Myint const &>(mfpointer)
        {
        }
    };
}

#endif // BADUMNA_MOCKEDDELEGATE_H