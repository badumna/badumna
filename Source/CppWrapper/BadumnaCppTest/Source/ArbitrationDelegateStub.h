//---------------------------------------------------------------------------------
// <copyright file="ArbitrationDelegateStub.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef BADUMNA_ARBITRATIONDELEGATESTUB_H
#define BADUMNA_ARBITRATIONDELEGATESTUB_H

#include "Badumna/DataTypes/InputStream.h"
#include "Badumna/Arbitration/ServiceConnectionResultType.h"

using namespace Badumna;

namespace BadumnaUnitTest
{
    class ArbitrationDelegateStub
    {
    public:
        ArbitrationDelegateStub()
            : connection_delegate_count(0),
            connection_failure_delegate_count(0),
            server_message_delegate_count(0),
            client_message_delegate_count(0),
            client_disconnect_delegate_count(0)
        {
        }

        int GetConnectionDelegateCount() const
        {
            return connection_delegate_count;
        }

        int GetConnectionFailureDelegateCount() const
        {
            return connection_failure_delegate_count;
        }

        int GetServerMessageDelegateCount() const
        {
            return server_message_delegate_count;
        }

        int GetClientMessageDelegateCount() const
        {
            return client_message_delegate_count;
        }

        int GetClientDisconnectDelegateCount() const
        {
            return client_disconnect_delegate_count;
        }

        void ConnectionDelegate(ServiceConnectionResultType /*type*/)
        {
            connection_delegate_count++;
        }

        void ConnectionFailureDelegate()
        {
            connection_failure_delegate_count++;
        }

        void ServerMessageDeledate(InputStream *)
        {
            server_message_delegate_count++;
        }

        void ClientMessageDelegate(int /*session_id*/, InputStream *)
        {
            client_message_delegate_count++;
        }

        void ClientDisconnectDelegate(int /*session_id*/)
        {
            client_disconnect_delegate_count++;
        }

    private:
        int connection_delegate_count;
        int connection_failure_delegate_count;
        int server_message_delegate_count;
        int client_message_delegate_count;
        int client_disconnect_delegate_count;
    };
}

#endif // BADUMNA_ARBITRATIONDELEGATESTUB_H