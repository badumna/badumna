#include <boost/test/unit_test.hpp>

#include "Badumna/Configuration/Options.h"
#include "Badumna/DataTypes/String.h"

using namespace Badumna;

struct OptionsFixture
{
    Options options;

    OptionsFixture()
        : options()
    {
    }
    
    virtual ~OptionsFixture() 
    { 
    }
};

BOOST_FIXTURE_TEST_SUITE(BadumnaOptions, OptionsFixture)

BOOST_AUTO_TEST_CASE(OptionsCanBeConstructedUsingTheDefaultConstructor)
{
    Options options2;
}

BOOST_AUTO_TEST_CASE(DefaultOptionsWithAppNameIsValid)
{
    String app_name = "unit_test_app_name";
    options.GetConnectivityModule().SetApplicationName(app_name);
    options.Validate();
}

/***********************************************************************************************************************
* Connectivity
***********************************************************************************************************************/
BOOST_AUTO_TEST_CASE(ApplicationNameCanBeSetAndGet)
{
    String app_name = "unit_test_app_name";
    options.GetConnectivityModule().SetApplicationName(app_name);
    String returned_app_name = options.GetConnectivityModule().GetApplicationName();

    BOOST_REQUIRE(app_name == returned_app_name);
}

BOOST_AUTO_TEST_CASE(TransportLimiterIsDisabledByDefault)
{
    BOOST_REQUIRE(!options.GetConnectivityModule().IsTransportLimiterEnabled());
}

BOOST_AUTO_TEST_CASE(PortRangeCanBeSet)
{
    int start_port = 23456;
    int end_port = 24567;

    options.GetConnectivityModule().SetStartPortRange(start_port);
    options.GetConnectivityModule().SetEndPortRange(end_port);

    BOOST_CHECK_EQUAL(start_port, options.GetConnectivityModule().GetStartPortRange());
    BOOST_CHECK_EQUAL(end_port, options.GetConnectivityModule().GetEndPortRange());
}

BOOST_AUTO_TEST_CASE(PortCanBeSet)
{
    int port = 32145;
    options.GetConnectivityModule().ConfigureForSpecificPort(port);

    BOOST_CHECK_EQUAL(port, options.GetConnectivityModule().GetStartPortRange());
    BOOST_CHECK_EQUAL(port, options.GetConnectivityModule().GetEndPortRange());
}

BOOST_AUTO_TEST_CASE(StunServerCanBeAdded)
{
    String server1(L"server1.example.com:12345");
    String server2(L"server2.example.com:12345");

    options.GetConnectivityModule().AddStunServer(server1);
    String current = options.GetConnectivityModule().GetStunServers();

    size_t pos = current.Find(server1);
    BOOST_REQUIRE(pos != String::npos);

    pos = current.Find(server2);
    BOOST_REQUIRE(pos == String::npos);

    options.GetConnectivityModule().AddStunServer(server2);
    current = options.GetConnectivityModule().GetStunServers();
    size_t pos2 = current.Find(server2);
    BOOST_REQUIRE(pos2 != String::npos);
}

BOOST_AUTO_TEST_CASE(MaxPortToTryCanBeSet)
{
    int num = 6;
    options.GetConnectivityModule().SetMaxPortsToTry(num);
    BOOST_CHECK_EQUAL(num, options.GetConnectivityModule().GetMaxPortsToTry());

    num = 8;
    options.GetConnectivityModule().SetMaxPortsToTry(num);
    BOOST_CHECK_EQUAL(num, options.GetConnectivityModule().GetMaxPortsToTry());
}

BOOST_AUTO_TEST_CASE(PortForwardingOptionCanBeSet)
{
    options.GetConnectivityModule().EnablePortForwarding();
    BOOST_REQUIRE(options.GetConnectivityModule().IsPortForwardingEnabled());
}

BOOST_AUTO_TEST_CASE(BroadcastOptionCanBeSet)
{
    options.GetConnectivityModule().EnableBroadcast();
    BOOST_REQUIRE(options.GetConnectivityModule().IsBroadcastEnabled());

    int port = 12323;
    options.GetConnectivityModule().SetBroadcastPort(port);
    BOOST_CHECK_EQUAL(port, options.GetConnectivityModule().GetBroadcastPort());
}

BOOST_AUTO_TEST_CASE(TunnelModeOptionCanBeSet)
{
    options.GetConnectivityModule().SetTunnelMode(TunnelMode_Off);
    BOOST_CHECK_EQUAL(TunnelMode_Off, options.GetConnectivityModule().GetTunnelMode());

    options.GetConnectivityModule().SetTunnelMode(TunnelMode_On);
    BOOST_CHECK_EQUAL(TunnelMode_On, options.GetConnectivityModule().GetTunnelMode());

    options.GetConnectivityModule().SetTunnelMode(TunnelMode_Auto);
    BOOST_CHECK_EQUAL(TunnelMode_Auto, options.GetConnectivityModule().GetTunnelMode());
}

BOOST_AUTO_TEST_CASE(HostedServiceOptionCanBeSet)
{
    options.GetConnectivityModule().EnableHostedService();
    BOOST_REQUIRE(options.GetConnectivityModule().IsHostedService());
}

BOOST_AUTO_TEST_CASE(ConfigForLanCanBeSet)
{
    options.GetConnectivityModule().ConfigureForLan();
}

BOOST_AUTO_TEST_CASE(TunnelUriCanBeSet)
{
    String server1(L"server1.example.com:12345");
    String server2(L"server2.example.com:12345");

    options.GetConnectivityModule().AddTunnelUri(server1);
    String current = options.GetConnectivityModule().GetTunnelUris();

    size_t pos = current.Find(server1);
    BOOST_REQUIRE(pos != String::npos);

    pos = current.Find(server2);
    BOOST_REQUIRE(pos == String::npos);

    options.GetConnectivityModule().AddTunnelUri(server2);
    current = options.GetConnectivityModule().GetTunnelUris();
    size_t pos2 = current.Find(server2);
    BOOST_REQUIRE(pos2 != String::npos);
}

BOOST_AUTO_TEST_CASE(SeedPeerCanBeSet)
{
    String server1(L"server1.example.com:12345");
    String server2(L"server2.example.com:12345");

    options.GetConnectivityModule().AddSeedPeer(server1);
    String current = options.GetConnectivityModule().GetSeedPeers();

    size_t pos = current.Find(server1);
    BOOST_REQUIRE(pos != String::npos);

    pos = current.Find(server2);
    BOOST_REQUIRE(pos == String::npos);

    options.GetConnectivityModule().AddSeedPeer(server2);
    current = options.GetConnectivityModule().GetSeedPeers();
    size_t pos2 = current.Find(server2);
    BOOST_REQUIRE(pos2 != String::npos);
}

/***********************************************************************************************************************
* Logger
***********************************************************************************************************************/
BOOST_AUTO_TEST_CASE(DotNetTraceForwardingOptionCanBeSet)
{
    options.GetLoggerModule().EnableDotNetTraceForwarding();
    BOOST_REQUIRE(options.GetLoggerModule().IsDotNetTraceForwardingEnabled());
}

BOOST_AUTO_TEST_CASE(AssertUiCanBeSuppressed)
{
    options.GetLoggerModule().SuppressAssertUi();
    BOOST_REQUIRE(options.GetLoggerModule().IsAssertUiSuppressed());
}

BOOST_AUTO_TEST_CASE(LoggerTypeOptionCanBeSet)
{
    options.GetLoggerModule().SetLoggerType(LoggerType_NoLogger);
    BOOST_REQUIRE(LoggerType_NoLogger == options.GetLoggerModule().GetLoggerType());

    options.GetLoggerModule().SetLoggerType(LoggerType_DotNetTrace);
    BOOST_REQUIRE(LoggerType_DotNetTrace == options.GetLoggerModule().GetLoggerType());

    options.GetLoggerModule().SetLoggerType(LoggerType_UnityTrace);
    BOOST_REQUIRE(LoggerType_UnityTrace == options.GetLoggerModule().GetLoggerType());
    
    options.GetLoggerModule().SetLoggerType(LoggerType_Log4Net);
    BOOST_REQUIRE(LoggerType_Log4Net == options.GetLoggerModule().GetLoggerType());
}

BOOST_AUTO_TEST_CASE(LoggerConfigOptionCanBeSet)
{
    String config = L"This is test config.";

    options.GetLoggerModule().SetLoggerConfig(config);
    BOOST_REQUIRE(config == options.GetLoggerModule().GetLoggerConfig());
}

/***********************************************************************************************************************
* Overload
***********************************************************************************************************************/
BOOST_AUTO_TEST_CASE(OverloadPeerOptionCanBeSet)
{
    String server(L"overload.server.example.com");
    
    options.GetOverloadModule().SetServerAddress(server);
    BOOST_REQUIRE(server == options.GetOverloadModule().GetServerAddress());
}

BOOST_AUTO_TEST_CASE(OverloadAcceptedOptionCanBeSet)
{
    options.GetOverloadModule().SetAsServer();
    BOOST_REQUIRE(options.GetOverloadModule().IsServer());
}

BOOST_AUTO_TEST_CASE(OverloadUsedOptionCanBeSet)
{
    options.GetOverloadModule().SetClientEnabled();
    BOOST_REQUIRE(options.GetOverloadModule().IsClientEnabled());
}

BOOST_AUTO_TEST_CASE(ForcedOverloadOptionCanBeChanged)
{
    BOOST_REQUIRE(!options.GetOverloadModule().IsForced());
    options.GetOverloadModule().EnableForcedOverload(L"i_understand_this_is_for_testing_only");
    BOOST_REQUIRE(options.GetOverloadModule().IsForced());
    options.GetOverloadModule().DisableForcedOverload();
    BOOST_REQUIRE(!options.GetOverloadModule().IsForced());
}

/***********************************************************************************************************************
* Arbitration
***********************************************************************************************************************/
BOOST_AUTO_TEST_CASE(ServiceDiscoveryBasedArbitrationServerCanBeAdded)
{
    options.GetArbitrationModule().AddServer(L"arbitration_test_type");
    String servers = options.GetArbitrationModule().GetServers();
    BOOST_REQUIRE(0 != servers.Length());
}

BOOST_AUTO_TEST_CASE(RegularArbitrationServerCanBeAdded)
{
    options.GetArbitrationModule().AddServer(L"arbitration_test_type", L"server1.arbitration.example.com:12345");
    String servers = options.GetArbitrationModule().GetServers();
    BOOST_REQUIRE(0 != servers.Length());
}

BOOST_AUTO_TEST_CASE(NoArbitrationServerByDefault)
{
    String servers = options.GetArbitrationModule().GetServers();
    BOOST_CHECK_EQUAL(0, servers.Length());
}

BOOST_AUTO_TEST_SUITE_END() // Options