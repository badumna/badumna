//---------------------------------------------------------------------------------
// <copyright file="MonoRuntimeTestHelper.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef MONORUNTIME_TEST_HELPER_H
#define MONORUNTIME_TEST_HELPER_H

#include "Badumna/Utils/Singleton.h"
#include "Badumna/Core/MonoRuntime.h"
#include "Badumna/Core/MonoRuntimeConfig.h"

using namespace Badumna;

namespace BadumnaUnitTest
{
    class MonoRuntimeTestHelper : public Singleton<MonoRuntimeTestHelper>
    {
        friend class Singleton<MonoRuntimeTestHelper>;
    public:
        MonoRuntimeTestHelper()
            : runtime()
        {
            Badumna::MonoRuntimeConfig *config = new Badumna::MonoRuntimeConfig();
#ifndef WIN32
            config->ConfigFromFile("mono_config_file");
#endif
            config->SetAssemblyPath("CppWrapperTestStub.dll");
            is_initialized = runtime.Initialize(config);
        }

        MonoRuntime& Runtime()
        {
            return runtime;
        }

        bool IsInitialized()
        {
            return is_initialized;
        }

    private:
        MonoRuntime runtime;
        bool is_initialized;
    };
}

#endif