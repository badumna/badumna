#include <iostream>

#include <boost/test/unit_test.hpp>
#include "MockedDelegate.h"

struct DelegateFixture
{
    DelegateFixture() 
    {
    }
    
    virtual ~DelegateFixture() 
    { 
    }
};

BOOST_FIXTURE_TEST_SUITE(BadumnaDelegate, DelegateFixture)

BOOST_AUTO_TEST_CASE(MemeberMethodDelegate1CanBeInvoked)
{
    int initial_value = 1234;

    BadumnaUnitTest::Delegate1Tester tester;
    BadumnaUnitTest::MockedDelegate1 dk1(&BadumnaUnitTest::Delegate1Tester::GetDataMemberMethod, tester);
    BadumnaUnitTest::Myint test_data(initial_value);
    BadumnaUnitTest::Myint * result = dk1(test_data);
    BOOST_CHECK_EQUAL(initial_value + 1, result->GetValue());
    delete result;
}

BOOST_AUTO_TEST_CASE(StaticMemeberDelegate1CanBeInvoked)
{
    int initial_value = 1234;

    BadumnaUnitTest::Delegate1Tester tester;
    BadumnaUnitTest::MockedDelegate1 dk1(BadumnaUnitTest::Delegate1Tester::GetDataStaticMethod);
    BadumnaUnitTest::Myint test_data(initial_value);
    BadumnaUnitTest::Myint * result = dk1(test_data);
    BOOST_CHECK_EQUAL(initial_value + 1, result->GetValue());

    delete result;
}

BOOST_AUTO_TEST_CASE(DelegateIsCopyableAndAssignable)
{
    int initial_value = 1234;

    BadumnaUnitTest::Delegate1Tester tester;
    BadumnaUnitTest::MockedDelegate1 dk1(BadumnaUnitTest::Delegate1Tester::GetDataStaticMethod);
    BadumnaUnitTest::MockedDelegate1 dk2(dk1);

    BadumnaUnitTest::Myint test_data(initial_value);
    BadumnaUnitTest::Myint *result = dk2(test_data);
    BOOST_CHECK_EQUAL(initial_value + 1, result->GetValue());
    delete result;
}

BOOST_AUTO_TEST_SUITE_END() // Delegates