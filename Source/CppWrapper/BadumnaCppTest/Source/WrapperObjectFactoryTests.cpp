//---------------------------------------------------------------------------------
// <copyright file="BadumnaRuntimeTests.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <memory>
#include <boost/test/unit_test.hpp>
#include "Badumna/Core/MonoRuntime.h"
#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/Chat/ChatChannelIdImpl.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/DataTypes/BadumnaIdImpl.h"
#include "Badumna/DataTypes/BooleanArrayImpl.h"
#include "Badumna/Configuration/OptionsImpl.h"

#include "ReplicaObject.h"
#include "LocalObject.h"
#include "DeadReckonableReplica.h"
#include "DeadReckonableOriginal.h"

using std::auto_ptr;
using namespace Badumna;
using namespace BadumnaUnitTest;

struct BadumnaRuntimeFixture
{
    BadumnaRuntimeFixture() 
    {
    }
    
    virtual ~BadumnaRuntimeFixture() 
    { 
    }

    void ExpectedClassNameMatches(MonoClass *klass, String expected_name)
    {
        String class_name = mono_class_get_name(klass);
        BOOST_REQUIRE(class_name == expected_name);
    }   
};

BOOST_FIXTURE_TEST_SUITE(BadumnaBadumnaRuntime, BadumnaRuntimeFixture)

BOOST_AUTO_TEST_CASE(RegularReplicaIsAssocatedWithRegularReplicaStub)
{
    String id(L"replica_id");
    auto_ptr<ReplicaObject> replica(new ReplicaObject());
    auto_ptr<ReplicaWrapper> wrapper(BadumnaRuntime::Instance().CreateReplicaWrapper(id, replica.get()));

    MonoClass *klass = wrapper->GetManagedMonoClass();
    String class_name = mono_class_get_name(klass);
    String expected_name = "SpatialReplicaStub";
    BOOST_REQUIRE(class_name == expected_name);
}

BOOST_AUTO_TEST_CASE(DeadReckonableReplicaIsAssocatedWithDeadReckonableReplicaStub)
{
    String id(L"replica_id");
    auto_ptr<DeadReckonableReplica> replica(new DeadReckonableReplica());
    auto_ptr<ReplicaWrapper> wrapper(BadumnaRuntime::Instance().CreateReplicaWrapper(id, replica.get()));

    MonoClass *klass = wrapper->GetManagedMonoClass();
    String class_name = mono_class_get_name(klass);
    String expected_name = "DeadReckonableReplicaStub";
    BOOST_REQUIRE(class_name == expected_name);
}

BOOST_AUTO_TEST_CASE(RegularOriginalIsAssocatedWithRegularOriginalStub)
{
    auto_ptr<LocalObject> original(new LocalObject());
    auto_ptr<OriginalWrapper> wrapper(BadumnaRuntime::Instance().CreateOriginalWrapper(original.get()));

    MonoClass *klass = wrapper->GetManagedMonoClass();
    String class_name = mono_class_get_name(klass);
    String expected_name = "SpatialOriginalStub";
    BOOST_REQUIRE(class_name == expected_name);
}

BOOST_AUTO_TEST_CASE(DeadReckonableOriginalIsAssocatedWithDeadReckonableOriginalStub)
{
    auto_ptr<DeadReckonableOriginal> original(new DeadReckonableOriginal());
    auto_ptr<OriginalWrapper> wrapper(BadumnaRuntime::Instance().CreateOriginalWrapper(original.get()));

    MonoClass *klass = wrapper->GetManagedMonoClass();
    String class_name = mono_class_get_name(klass);
    String expected_name = "DeadReckonableOriginalStub";
    BOOST_REQUIRE(class_name == expected_name);
}

BOOST_AUTO_TEST_CASE(CreateBadumnaIdImplDoesCreateTheImplObject)
{
    auto_ptr<BadumnaIdImpl> impl(BadumnaRuntime::Instance().CreateBadumnaIdImpl());
    ExpectedClassNameMatches(impl->GetManagedMonoClass(), "BadumnaId");
}

BOOST_AUTO_TEST_CASE(CreateBooleanArrayImplDoesCreateTheImplObject)
{
    auto_ptr<BooleanArrayImpl> impl(BadumnaRuntime::Instance().CreateBooleanArrayImpl());
    ExpectedClassNameMatches(impl->GetManagedMonoClass(), "BooleanArray");
}

BOOST_AUTO_TEST_CASE(CreateBooleanArrayImpl2DoesCreateTheImplObject)
{
    auto_ptr<BooleanArrayImpl> impl(BadumnaRuntime::Instance().CreateBooleanArrayImpl(true));
    ExpectedClassNameMatches(impl->GetManagedMonoClass(), "BooleanArray");
}

BOOST_AUTO_TEST_CASE(CreateOptionsImplDoesCreateTheImplObject)
{
    auto_ptr<OptionsImpl> impl(BadumnaRuntime::Instance().CreateOptionsImpl());
    ExpectedClassNameMatches(impl->GetManagedMonoClass(), "Options");
}

BOOST_AUTO_TEST_SUITE_END() // BadumnaRuntime