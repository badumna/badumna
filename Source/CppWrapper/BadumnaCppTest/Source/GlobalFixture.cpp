#include <iostream>
#include <boost/test/unit_test.hpp>

#include "Badumna/Utils/FileIO.h"
#include "Badumna/Core/RuntimeInitializer.h"

using namespace Badumna;

namespace 
{
    using std::string;

    bool IsMonoRuntimeStubTests(int argc, char **argv)
    {
        for(int i = 0; i <= argc; i++)
        {
            string parameter(argv[i]);
            if(parameter.find("--is_stub_test") != string::npos)
            {
                return true;
            }
        }

        return false;
    }
}

struct GlobalFixture 
{
    GlobalFixture()
    {
        int argc = boost::unit_test::framework::master_test_suite().argc;
        char **argv = boost::unit_test::framework::master_test_suite().argv;
        // global setup
        
        if(!::IsMonoRuntimeStubTests(argc, argv))
        {
#ifdef WIN32
            InitializeRuntime();
#else
            InitializeRuntimeFromConfigFile("mono_config_file");
#endif
        }
    }

    ~GlobalFixture()
    {
        // global teardown
    }
};

BOOST_GLOBAL_FIXTURE(GlobalFixture);