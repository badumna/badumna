#include <boost/test/unit_test.hpp>

#include "ConnectivityDelegateStub.h"
#include "Badumna/Core/FacadeServiceManager.h"

using namespace Badumna;
using namespace BadumnaUnitTest;

struct FacadeServiceManagerFixture
{
    FacadeServiceManager manager;

    FacadeServiceManagerFixture()
        : manager()
    {
    }
    
    virtual ~FacadeServiceManagerFixture() 
    { 
    }
};

BOOST_FIXTURE_TEST_SUITE(BadumnaFacadeServiceManager, FacadeServiceManagerFixture)

BOOST_AUTO_TEST_CASE(OnlineEventDelegateCanBeInvoked)
{
    ConnectivityDelegateStub stub;
    ConnectivityStatusDelegate d(&ConnectivityDelegateStub::OnlineEventHandler, stub);

    BOOST_CHECK_EQUAL(0, stub.GetOnlineCount());
    manager.SetOnlineEventDelegate(d);
    manager.InvokeOnlineEventDelegate();
    BOOST_CHECK_EQUAL(1, stub.GetOnlineCount());
}

BOOST_AUTO_TEST_CASE(OfflineEventDelegateCanBeInvoked)
{
    ConnectivityDelegateStub stub;
    ConnectivityStatusDelegate d(&ConnectivityDelegateStub::OfflineEventHandler, stub);

    BOOST_CHECK_EQUAL(0, stub.GetOfflineCount());
    manager.SetOfflineEventDelegate(d);
    manager.InvokeOfflineEventDelegate();
    BOOST_CHECK_EQUAL(1, stub.GetOfflineCount());
}

BOOST_AUTO_TEST_CASE(AddressChangedEventDelegateCanBeInvoked)
{
    ConnectivityDelegateStub stub;
    ConnectivityStatusDelegate d(&ConnectivityDelegateStub::AddressChangedEventHandler, stub);

    BOOST_CHECK_EQUAL(0, stub.GetAddressChangedCount());
    manager.SetAddressChangedEventDelegate(d);
    manager.InvokeAddressChangedEventDelegate();
    BOOST_CHECK_EQUAL(1, stub.GetAddressChangedCount());
}

BOOST_AUTO_TEST_CASE(OnlineEventDelegateCanBeReplaced)
{
    ConnectivityDelegateStub stub;
    ConnectivityDelegateStub stub2;
    ConnectivityStatusDelegate d(&ConnectivityDelegateStub::OnlineEventHandler, stub);
    ConnectivityStatusDelegate d2(&ConnectivityDelegateStub::OnlineEventHandler, stub2);

    BOOST_CHECK_EQUAL(0, stub.GetOnlineCount());
    BOOST_CHECK_EQUAL(0, stub2.GetOnlineCount());

    manager.SetOnlineEventDelegate(d);
    manager.SetOnlineEventDelegate(d2);
    manager.InvokeOnlineEventDelegate();

    BOOST_CHECK_EQUAL(0, stub.GetOnlineCount());
    BOOST_CHECK_EQUAL(1, stub2.GetOnlineCount());
}

BOOST_AUTO_TEST_CASE(OfflineEventDelegateCanBeReplaced)
{
    ConnectivityDelegateStub stub;
    ConnectivityDelegateStub stub2;
    ConnectivityStatusDelegate d(&ConnectivityDelegateStub::OfflineEventHandler, stub);
    ConnectivityStatusDelegate d2(&ConnectivityDelegateStub::OfflineEventHandler, stub2);

    BOOST_CHECK_EQUAL(0, stub.GetOfflineCount());
    BOOST_CHECK_EQUAL(0, stub2.GetOfflineCount());

    manager.SetOfflineEventDelegate(d);
    manager.SetOfflineEventDelegate(d2);
    manager.InvokeOfflineEventDelegate();

    BOOST_CHECK_EQUAL(0, stub.GetOfflineCount());
    BOOST_CHECK_EQUAL(1, stub2.GetOfflineCount());
}

BOOST_AUTO_TEST_CASE(AddressChangedEventDelegateCanBeReplaced)
{
    ConnectivityDelegateStub stub;
    ConnectivityDelegateStub stub2;
    ConnectivityStatusDelegate d(&ConnectivityDelegateStub::AddressChangedEventHandler, stub);
    ConnectivityStatusDelegate d2(&ConnectivityDelegateStub::AddressChangedEventHandler, stub2);

    BOOST_CHECK_EQUAL(0, stub.GetAddressChangedCount());
    BOOST_CHECK_EQUAL(0, stub2.GetAddressChangedCount());

    manager.SetAddressChangedEventDelegate(d);
    manager.SetAddressChangedEventDelegate(d2);
    manager.InvokeAddressChangedEventDelegate();

    BOOST_CHECK_EQUAL(0, stub.GetAddressChangedCount());
    BOOST_CHECK_EQUAL(1, stub2.GetAddressChangedCount());
}

BOOST_AUTO_TEST_SUITE_END() // FacadeServiceManager