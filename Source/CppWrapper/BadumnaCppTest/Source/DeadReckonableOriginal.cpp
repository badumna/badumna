//---------------------------------------------------------------------------------
// <copyright file="DeadReckonableOriginal.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include "DeadReckonableOriginal.h"
#include "ReplicaObject.h"

namespace BadumnaUnitTest
{
    DeadReckonableOriginal::DeadReckonableOriginal() 
        : guid(BadumnaId::None()),
        velocity(0.0f, 0.0f, 0.0f),
        position(0.0f, 0.0f, 0.0f),  
        radius(0.0f), 
        area_of_interest_radius(0.0f), 
        counter(0),
        serialize_counter(0),
        event_counter(0)
    {
    }

    DeadReckonableOriginal::~DeadReckonableOriginal()
    {
    }

    BadumnaId DeadReckonableOriginal::Guid() const
    {
        return this->guid;
    }

    void DeadReckonableOriginal::SetGuid(BadumnaId const &id)
    {
        this->guid = id;
    }

    void DeadReckonableOriginal::HandleEvent(InputStream *)
    {
        event_counter++;
    }

    Vector3 DeadReckonableOriginal::Position() const
    {
        return this->position;
    }

    void DeadReckonableOriginal::SetPosition(Vector3 const &value)
    {
        this->position = value;
    }

    float DeadReckonableOriginal::Radius() const
    {
        return this->radius;
    }

    void DeadReckonableOriginal::SetRadius(float value)
    {
        this->radius = value;
    }

    float DeadReckonableOriginal::AreaOfInterestRadius() const
    {
        return this->area_of_interest_radius;
    }

    void DeadReckonableOriginal::SetAreaOfInterestRadius(float value)
    {
        this->area_of_interest_radius = value;
    }

    void DeadReckonableOriginal::Serialize(BooleanArray const &required_parts, OutputStream *stream)
    {
        serialize_counter++;
        if(required_parts[Counter])
        {
            (*stream) << this->counter;
        }
    }

    void DeadReckonableOriginal::SetCounter(int value)
    {
        this->counter = value;
    }

    Vector3 DeadReckonableOriginal::Velocity() const
    {
        return velocity;
    }

    void DeadReckonableOriginal::SetVelocity(Vector3 const &v)
    {
        velocity = v;
    }

    void DeadReckonableOriginal::AttemptMovement(Vector3 const &reckoned_position)
    {
        position = reckoned_position;
    }
}