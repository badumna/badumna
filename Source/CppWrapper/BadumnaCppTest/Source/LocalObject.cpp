#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/Replication/SpatialEntityStateSegment.h"

#include "LocalObject.h"
#include "ReplicaObject.h"

namespace BadumnaUnitTest
{
    using Badumna::NetworkFacade;

    LocalObject::LocalObject() 
        : guid(BadumnaId::None()), 
        position(0.0f, 0.0f, 0.0f),  
        radius(0.0f), 
        area_of_interest_radius(0.0f), 
        counter(0),
        serialize_counter(0),
        event_counter(0)
    {
    }

    LocalObject::~LocalObject()
    {
    }

    BadumnaId LocalObject::Guid() const
    {
        return this->guid;
    }

    void LocalObject::SetGuid(BadumnaId const &id)
    {
        this->guid = id;
    }

    void LocalObject::HandleEvent(InputStream *)
    {
        event_counter++;
    }

    Vector3 LocalObject::Position() const
    {
        return this->position;
    }

    void LocalObject::SetPosition(Vector3 const &value)
    {
        this->position = value;
    }

    float LocalObject::Radius() const
    {
        return this->radius;
    }

    void LocalObject::SetRadius(float value)
    {
        this->radius = value;
    }

    float LocalObject::AreaOfInterestRadius() const
    {
        return this->area_of_interest_radius;
    }

    void LocalObject::SetAreaOfInterestRadius(float value)
    {
        this->area_of_interest_radius = value;
    }

    void LocalObject::Serialize(BooleanArray const &required_parts, OutputStream *stream)
    {
        serialize_counter++;
        if(required_parts[Counter])
        {
            (*stream) << this->counter;
        }
    }

    void LocalObject::SetCounter(int value)
    {
        this->counter = value;
    }
}