#include <cstring>
#include <boost/test/unit_test.hpp>
#include "Badumna/DataTypes/String.h"

using namespace Badumna;

struct StringFixture
{
    StringFixture() 
    {
    }
    
    virtual ~StringFixture() 
    { 
    }
};

BOOST_FIXTURE_TEST_SUITE(BadumnaString, StringFixture)

BOOST_AUTO_TEST_CASE(EmptyStringCanBeCreated)
{
    String s;
    BOOST_REQUIRE(s.Length() == 0);
}

BOOST_AUTO_TEST_CASE(ConstructorsWorkAsExpected)
{
    String one("a");
    String wone(L"a");

    BOOST_REQUIRE(one.Length() == 1);
    BOOST_REQUIRE(wone.Length() == 1);
}

BOOST_AUTO_TEST_CASE(CopyConstructorWorksAsExpected)
{
    String value("value");
    String copied(value);

    BOOST_REQUIRE(value.Length() == copied.Length());

    for(size_t i = 0; i < value.Length(); ++i)
    {
        BOOST_REQUIRE(value.CStr()[i] == copied.CStr()[i]);
    }
}

BOOST_AUTO_TEST_CASE(StringAssignWorksAsExpected)
{
    String s;
    BOOST_REQUIRE(s.Length() == 0);

    s = L"ABC";
    BOOST_REQUIRE(s.Length() == 3);

    String four("ABCD");
    s = four;
    BOOST_REQUIRE(s.Length() == 4);
}

BOOST_AUTO_TEST_CASE(EqualToOperatorWorksAsExpected)
{
    String s1("ABCD");
    String s2("ABCD");

    BOOST_REQUIRE(s1 == s2);

    String s3("ABCDE");
    String s4("ABCDe");
    
    BOOST_REQUIRE(s1 != s3);
    BOOST_REQUIRE(s1 != s4);
    BOOST_REQUIRE(s3 != s4);

    s3 = s1;
    BOOST_REQUIRE(s1 == s3);

    String s5;
    String s6;

    BOOST_REQUIRE(s5 == s6);
    BOOST_REQUIRE(s5 != s1);
}

BOOST_AUTO_TEST_CASE(LengthWorksAsExpected)
{
    String s0;
    String s1("a");
    String s1_2(L"a");
    String s2("ab");

    BOOST_REQUIRE(s0.Length() == 0);
    BOOST_REQUIRE(s1.Length() == 1);
    BOOST_REQUIRE(s1_2.Length() == 1);
    BOOST_REQUIRE(s2.Length() == 2);
}

BOOST_AUTO_TEST_CASE(EmptyWorksAsExpected)
{
    String s0;
    BOOST_REQUIRE(s0.Length() == 0);
    BOOST_REQUIRE(s0.Empty());

    s0 = "abcd";
    BOOST_REQUIRE(s0.Length() == 4);
    BOOST_REQUIRE(!s0.Empty());
}

BOOST_AUTO_TEST_CASE(AddOperatorWorksAsExpected)
{
    String s0;
    String s1("a");
    String s2("bc");
    String s3("abc");
    String result("abcabc");

    s0 += s1;
    BOOST_REQUIRE(s0.Length() == 1);
    BOOST_REQUIRE(s0 == s1);

    s0 = s1 + s2;
    BOOST_REQUIRE(s0.Length() == 3);
    BOOST_REQUIRE(s0 == s3);

    s0 = s1 + s2 + s3;
    BOOST_REQUIRE(s0.Length() == 6);
    BOOST_REQUIRE(s0 == result);
}

BOOST_AUTO_TEST_CASE(LessThanOperatorWorksAsExpected)
{
    String s0;
    String s1("a");
    String s2("ab");
    String s3("b");

    BOOST_REQUIRE(s1 < s3);
    BOOST_REQUIRE(s0 < s1);
    BOOST_REQUIRE(s1 < s2);
    BOOST_REQUIRE(s2 < s3);

    String t1("aaaaxxxx");
    String t2("aaabcccc");
    BOOST_REQUIRE(t1 < t2);
}

BOOST_AUTO_TEST_CASE(FindWorksAsExpected)
{
    String t1("this is a test");
    String t2("test");

    BOOST_REQUIRE(t1.Find(t2, 0) != String::npos);
    BOOST_REQUIRE(t1.Find(t2, 0) == 10);

    String t3("not_here!!");
    BOOST_REQUIRE(t1.Find(t3) == String::npos);
}

BOOST_AUTO_TEST_CASE(UTF8CStrWorksAsExpected)
{
    String s0;
    BOOST_REQUIRE(strlen(s0.UTF8CStr()) == 0);
    BOOST_REQUIRE(s0.UTF8CStr()[0] == 0);

    String s1("a");
    BOOST_REQUIRE(strlen(s1.UTF8CStr()) == 1);
    BOOST_REQUIRE(s1.UTF8CStr()[0] == 'a');
    BOOST_REQUIRE(s1.UTF8CStr()[1] == 0);

    String s3("abc");
    BOOST_REQUIRE(strlen(s3.UTF8CStr()) == 3);
    BOOST_REQUIRE(s3.UTF8CStr()[0] == 'a');
    BOOST_REQUIRE(s3.UTF8CStr()[1] == 'b');
    BOOST_REQUIRE(s3.UTF8CStr()[2] == 'c');
    BOOST_REQUIRE(s3.UTF8CStr()[3] == 0);
}

BOOST_AUTO_TEST_CASE(CStrWorksAsExpected)
{
    String s0;
    BOOST_REQUIRE(wcslen(s0.CStr()) == 0);
    BOOST_REQUIRE(s0.CStr()[0] == 0);

    String s1(L"a");
    BOOST_REQUIRE(wcslen(s1.CStr()) == 1);
    BOOST_REQUIRE(s1.CStr()[0] == L'a');
    BOOST_REQUIRE(s1.CStr()[1] == 0);

    String s3(L"abc");
    BOOST_REQUIRE(wcslen(s3.CStr()) == 3);
    BOOST_REQUIRE(s3.CStr()[0] == L'a');
    BOOST_REQUIRE(s3.CStr()[1] == L'b');
    BOOST_REQUIRE(s3.CStr()[2] == L'c');
    BOOST_REQUIRE(s3.CStr()[3] == 0);
}

BOOST_AUTO_TEST_SUITE_END() // StringFixture