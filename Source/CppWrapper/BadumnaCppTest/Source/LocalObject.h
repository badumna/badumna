//---------------------------------------------------------------------------------
// <copyright file="LocalObject.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef REPLICATION_TEST_LOCAL_OBJECT_H
#define REPLICATION_TEST_LOCAL_OBJECT_H

#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/DataTypes/Vector3.h"
#include "Badumna/Replication/ISpatialOriginal.h"

namespace BadumnaUnitTest
{
    using Badumna::ISpatialOriginal;
    using Badumna::BadumnaId;
    using Badumna::Vector3;
    using Badumna::BooleanArray;
    using Badumna::OutputStream;
    using Badumna::InputStream;

    class LocalObject : public ISpatialOriginal
    {
    public:
        LocalObject();

        ~LocalObject();

        //
        // IEntity properties
        // 
        BadumnaId Guid() const;

        void SetGuid(BadumnaId const &id);

        void HandleEvent(InputStream *stream);

        //
        // ISpatialEntity properties
        //
        Vector3 Position() const;

        void SetPosition(Vector3 const &position);

        float Radius() const;

        void SetRadius(float val);

        float AreaOfInterestRadius() const;

        void SetAreaOfInterestRadius(float val);

        //
        // ISpatialOriginal properties
        //
        void Serialize(BooleanArray const &required_parts, OutputStream *stream);

        //
        // app specific.
        // 
        int GetCounter()
        {
            return this->counter;
        }

        void SetCounter(int value);

        int GetSerializeCounter()
        {
            return serialize_counter;
        }

        int GetEventCounter()
        {
            return event_counter;
        }
        
    private:
        BadumnaId guid;
        
        Vector3 position;
        float radius;
        float area_of_interest_radius;

        int counter; 
        int serialize_counter;
        int event_counter;
    };
}

#endif // REPLICATION_TEST_LOCAL_OBJECT_H