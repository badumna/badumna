#include <string>
#include <vector>
#include <iostream>
#include <boost/test/unit_test.hpp>

#include "Badumna/Core/ExceptionDetails.h"
#include "Badumna/Core/MonoRuntime.h"
#include "Badumna/Core/MonoRuntimeConfig.h"
#include "Badumna/Core/Arguments.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"

#include "MonoRuntimeTestHelper.h"

using namespace std;
using namespace Badumna;
using namespace BadumnaUnitTest;

class ExceptionHandlerStub
{
public:
    ExceptionHandlerStub()
        : count(0)
    {
    }

    void HandleException(String const &/*s1*/, String const &/*s2*/, String const &/*s3*/)
    {
        count++;
    }

    int GetCount() const
    {
        return count;
    }

private:
    int count;
};


struct MonoRuntimeFixture
{
    MonoClass* k;
    MonoObject* object;

    MonoRuntimeFixture()
        : k(NULL), 
        object(NULL)
    {
        BOOST_REQUIRE_MESSAGE(MonoRuntimeTestHelper::Instance().IsInitialized(), "MonoRuntimeTestHelper is not initialized (maybe CppWrapperTestStub.dll could not be found?)");
        k = MonoRuntimeTestHelper::Instance().Runtime().GetClass("CppWrapperTestStub", "BaseClass");
        object = MonoRuntimeTestHelper::Instance().Runtime().CreateObject(k, NULL);
    }
    
    virtual ~MonoRuntimeFixture() 
    {
    }

private:
    DISALLOW_COPY_AND_ASSIGN(MonoRuntimeFixture);
};

BOOST_FIXTURE_TEST_SUITE(StubMonoRuntime, MonoRuntimeFixture)


BOOST_AUTO_TEST_CASE(ObjectCanBeCreated)
{
    BOOST_REQUIRE(k != NULL);
    BOOST_REQUIRE(object != NULL);
}

BOOST_AUTO_TEST_CASE(InvokeMethodReturnBoolWorksAsExpected)
{
    MonoObject* returned_object = MonoRuntimeTestHelper::Instance().Runtime().InvokeMethod(
        k, 
        object, 
        "BoolMethodReturnTrue", 
        Arguments::None(), 
        NULL);

    bool unboxed_result = MonoRuntimeTestHelper::Instance().Runtime().UnboxingObject<bool>(returned_object);
    BOOST_REQUIRE(unboxed_result);
}

BOOST_AUTO_TEST_CASE(InvokeMethodReturnVoidWorksAsExpected)
{
    MonoRuntimeTestHelper::Instance().Runtime().InvokeMethod(k, object, "VoidMethod", Arguments::None(), NULL);
}

BOOST_AUTO_TEST_CASE(InvokeMethodReturnInputValueWorksAsExpected)
{
    Arguments args;
    bool input = true;
    args.AddArgument(&input);

    MonoObject* returned_object = MonoRuntimeTestHelper::Instance().Runtime().InvokeMethod(
        k, 
        object, 
        "BoolMethodReturnInputValue", 
        args, 
        NULL);
    
    bool unboxed_result = MonoRuntimeTestHelper::Instance().Runtime().UnboxingObject<bool>(returned_object);
    BOOST_REQUIRE(unboxed_result == input);

    Arguments args2;
    input = false;
    args2.AddArgument(&input);

    returned_object = MonoRuntimeTestHelper::Instance().Runtime().InvokeMethod(
        k, 
        object, 
        "BoolMethodReturnInputValue", 
        args2, 
        NULL);
    
    unboxed_result = MonoRuntimeTestHelper::Instance().Runtime().UnboxingObject<bool>(returned_object);
    BOOST_REQUIRE(unboxed_result == input);
}

BOOST_AUTO_TEST_CASE(StringCanBeHandled)
{
    String input = "test string...";
    MonoString *str = MonoRuntime::GetString(input);
    String result_string = MonoRuntime::MonoStringToString(str);

    BOOST_REQUIRE(result_string == input);
}

BOOST_AUTO_TEST_CASE(WStringCanBeHandled)
{
    String input = L"wide string...";
    MonoString *str = MonoRuntime::GetString(input);
    String result_string = MonoRuntime::MonoStringToWString(str);

    BOOST_REQUIRE(result_string == input);
}

BOOST_AUTO_TEST_CASE(PropertyGetAnSetWorksAsExpected)
{
    int input_value = 128;
    Arguments input_args;
    input_args.AddArgument(&input_value);
    MonoRuntimeTestHelper::Instance().Runtime().SetPropertyValue(k, object, "PropertyInt", input_args, NULL);

    Arguments args;
    MonoObject* returned_object = MonoRuntimeTestHelper::Instance().Runtime().GetPropertyFromName(
        k, 
        object, 
        "PropertyInt", 
        args, NULL);
    int unboxed_result = MonoRuntimeTestHelper::Instance().Runtime().UnboxingObject<int>(returned_object);

    BOOST_CHECK_EQUAL(input_value, unboxed_result);
}

BOOST_AUTO_TEST_CASE(StaticPropertyGetWorksAsExpected)
{
    MonoObject *returned_object = MonoRuntimeTestHelper::Instance().Runtime().InvokeGetPropertyFromName(
        "CppWrapperTestStub", 
        "BaseClass", 
        "StaticIntegerPropertyReturn100", 
        NULL);
    int unboxed_result = MonoRuntimeTestHelper::Instance().Runtime().UnboxingObject<int>(returned_object);
    
    BOOST_CHECK_EQUAL(100, unboxed_result);
}

BOOST_AUTO_TEST_CASE(VirtualMethodWorksAsExpectedOnBaseClass)
{
    MonoObject *returned_object = MonoRuntimeTestHelper::Instance().Runtime().InvokeVirtualMethod(
        k, 
        object, 
        "VirtualMethodReturnsStringName", 
        Arguments::None(),
        NULL);

    MonoString *str = reinterpret_cast<MonoString*>(returned_object);
    String result_string = MonoRuntime::MonoStringToString(str);

    BOOST_REQUIRE(result_string == "base");
}

BOOST_AUTO_TEST_CASE(VirtualMethodWorksAsExpectedOnSubClass)
{
    MonoClass* subclass;
    MonoObject* subclass_object;

    subclass = MonoRuntimeTestHelper::Instance().Runtime().GetClass("CppWrapperTestStub", "SubClass");
    subclass_object = MonoRuntimeTestHelper::Instance().Runtime().CreateObject(subclass, NULL);
    
    MonoObject *returned_object = MonoRuntimeTestHelper::Instance().Runtime().InvokeVirtualMethod(
        subclass, 
        subclass_object, 
        "VirtualMethodReturnsStringName", 
        Arguments::None(), 
        NULL);

    MonoString *str = reinterpret_cast<MonoString*>(returned_object);
    String result_string = MonoRuntime::MonoStringToString(str);

    BOOST_REQUIRE(result_string == "sub");
}

BOOST_AUTO_TEST_CASE(VirtualMethodWorksAsExpectedOnSubClassWhenCalledOnForcedType)
{
    MonoClass* subclass;
    MonoObject* subclass_object;

    subclass = MonoRuntimeTestHelper::Instance().Runtime().GetClass("CppWrapperTestStub", "SubClass");
    subclass_object = MonoRuntimeTestHelper::Instance().Runtime().CreateObject(subclass, NULL);

    // when calling the virtual method, the correct virtual method should always be called even when 
    // the class parameter specified doesn't match its actual type. 
    MonoObject *returned_object = MonoRuntimeTestHelper::Instance().Runtime().InvokeVirtualMethod(
        k, 
        subclass_object, 
        "VirtualMethodReturnsStringName", 
        Arguments::None(), 
        NULL);

    MonoString *str = reinterpret_cast<MonoString*>(returned_object);
    String result_string = MonoRuntime::MonoStringToString(str);

    BOOST_REQUIRE(result_string == "sub");
}

BOOST_AUTO_TEST_CASE(ConstructorWithParameterIsHandledCorrectly)
{
    int init_value = 1024;
    Arguments args;
    args.AddArgument(&init_value);

    MonoObject* another_object = MonoRuntimeTestHelper::Instance().Runtime().CreateObject(k, args, NULL);
    MonoObject* returned_object = MonoRuntimeTestHelper::Instance().Runtime().GetPropertyFromName(
        k, 
        another_object, 
        "PropertyInt", 
        Arguments::None(),
        NULL);
    int unboxed_result = MonoRuntimeTestHelper::Instance().Runtime().UnboxingObject<int>(returned_object);

    BOOST_CHECK_EQUAL(init_value, unboxed_result);
}

BOOST_AUTO_TEST_CASE(GCHandlerWorksAsExpected)
{
    // magic number
    int init_value = 65511;
    Arguments args;
    args.AddArgument(&init_value);

    MonoObject* another_object = MonoRuntimeTestHelper::Instance().Runtime().CreateObject(k, args, NULL);

    MonoGCHandle gc_handler = MonoRuntime::GetObjectGCHandle(another_object);
    MonoObject* object_from_handler = MonoRuntime::GetObjectFromGCHandle(gc_handler);

    MonoObject* returned_object = MonoRuntimeTestHelper::Instance().Runtime().GetPropertyFromName(
        k, 
        object_from_handler, 
        "PropertyInt", 
        Arguments::None(), 
        NULL);
    int unboxed_result = MonoRuntimeTestHelper::Instance().Runtime().UnboxingObject<int>(returned_object);

    BOOST_CHECK_EQUAL(init_value, unboxed_result);

    MonoRuntime::FreeObjectGCHandle(gc_handler);
}

BOOST_AUTO_TEST_CASE(MonoArrayWorksAsExpected)
{
    char *buffer = new char[128];
    for(int i = 0; i < 128; i++)
    {
        buffer[i] = static_cast<char>(i);
    }

    MonoArray* test_array = MonoRuntime::GetMonoByteArray(const_cast<const char *>(buffer), 128);
    std::pair<char *, size_t> p = MonoRuntime::GetArrayData(test_array);
    BOOST_CHECK_EQUAL(128, p.second);

    for(int i = 0; i < 128; i++)
    {
        BOOST_CHECK_EQUAL(buffer[i], p.first[i]);
    }

    delete[] p.first;
    delete[] buffer;
}

BOOST_AUTO_TEST_CASE(GetObjectTypeNameWorksAsExpected)
{
    String expected_name = "BaseClass";
    String name = MonoRuntime::GetObjectTypeNameFromObject(object);
    BOOST_REQUIRE(expected_name == name);
}

BOOST_AUTO_TEST_CASE(ManagedFloatCanBeCreated)
{
    MonoObject *f = MonoRuntimeTestHelper::Instance().Runtime().GetManagedFloatValue(128.0f);
    float value = MonoRuntimeTestHelper::Instance().Runtime().UnboxingObject<float>(f);

    BOOST_CHECK_CLOSE(128.0f, value, 0.1f);
}

BOOST_AUTO_TEST_CASE(DllImportIsConfiguredCorrectly)
{
    // to see whether we can invoke some System methods. 
    MonoObject *ret = MonoRuntimeTestHelper::Instance().Runtime().InvokeMethod(
        k, 
        object, 
        "CanGetHostName", 
        Arguments::None(),
        NULL);
    bool success = MonoRuntimeTestHelper::Instance().Runtime().UnboxingObject<bool>(ret);

    BOOST_REQUIRE(success);
}

/*
BOOST_AUTO_TEST_CASE(ExceptionHandlerIsCalledOnExceptionWhenNotInterestedInDetails)
{
    ExceptionHandlerStub stub;
    BadumnaExceptionCallback handler(&ExceptionHandlerStub::HandleException, stub);
    MonoRuntimeConfig::Instance().SetExceptionCallback(handler);

    BOOST_CHECK_EQUAL(0, stub.GetCount());
    
    MonoRuntimeTestHelper::Instance().Runtime().InvokeMethod(
        k, 
        object, 
        "VoidMethodThrowInvalidOperationException", 
        Arguments::None(),
        NULL);

    BOOST_CHECK_EQUAL(1, stub.GetCount());
}

BOOST_AUTO_TEST_CASE(ExceptionHandlerIsNotCalledOnExceptionWhenDetailsAreRequired)
{
    ExceptionDetails details;
    ExceptionHandlerStub stub;
    BadumnaExceptionCallback handler(&ExceptionHandlerStub::HandleException, stub);
    MonoRuntimeConfig::Instance().SetExceptionCallback(handler);

    BOOST_CHECK_EQUAL(0, stub.GetCount());
    
    MonoRuntimeTestHelper::Instance().Runtime().InvokeMethod(
        k, 
        object, 
        "VoidMethodThrowInvalidOperationException", 
        Arguments::None(),
        &details);

    // the callback should not be called
    BOOST_CHECK_EQUAL(0, stub.GetCount());

    BOOST_REQUIRE(details.message == L"expected");
    BOOST_REQUIRE(details.managed_exception_type_name == L"InvalidOperationException");
    BOOST_REQUIRE(details.method_name == L"VoidMethodThrowInvalidOperationException");
}*/

BOOST_AUTO_TEST_CASE(NonVirtualPublicMethodInBaseClassCanBeInvokedFromSubClass)
{
    MonoClass* subclass;
    MonoObject* subclass_object;

    subclass = MonoRuntimeTestHelper::Instance().Runtime().GetClass("CppWrapperTestStub", "SubClass");
    subclass_object = MonoRuntimeTestHelper::Instance().Runtime().CreateObject(subclass, NULL);
    
    MonoObject* returned_object = MonoRuntimeTestHelper::Instance().Runtime().InvokeMethod(
        subclass, subclass_object, "BoolMethodReturnTrue", Arguments::None(), NULL);

    bool success = MonoRuntimeTestHelper::Instance().Runtime().UnboxingObject<bool>(returned_object);
    BOOST_REQUIRE(success);
}

BOOST_AUTO_TEST_CASE(NonVirtualPublicPropertyInBaseClassCanBeInvokedFromSubClass)
{
    MonoClass* subclass;
    MonoObject* subclass_object;

    subclass = MonoRuntimeTestHelper::Instance().Runtime().GetClass("CppWrapperTestStub", "SubClass");
    subclass_object = MonoRuntimeTestHelper::Instance().Runtime().CreateObject(subclass, NULL);

    MonoObject* returned_object = MonoRuntimeTestHelper::Instance().Runtime().GetPropertyFromName(
        subclass, 
        subclass_object, 
        "PropertyInt", 
        Arguments::None(), 
        NULL);

    int unboxed_result = MonoRuntimeTestHelper::Instance().Runtime().UnboxingObject<int>(returned_object);
    BOOST_CHECK_EQUAL(0, unboxed_result);
}

/*
BOOST_AUTO_TEST_CASE(MonoArrayBandwidthIsAcceptable)
{
    std::vector<char *> test_data;

    size_t count = 10 * 1024 * 1024 / 128;
    for(size_t i = 0; i < count; i++)
    {
        char *p = new char[128];
        test_data.push_back(p);
    }

    for(size_t i = 0; i < count; i++)
    {
        char *data = test_data[i];
        MonoArray* test_array = MonoRuntime::GetMonoByteArray(const_cast<const char *>(data), 128);
        std::pair<char *, size_t> p = MonoRuntime::GetArrayData(test_array);
        delete[] p.first;
    }

    for(size_t i = 0; i < count; i++)
    {
        char *p = test_data[i];
        delete[] p;
    }
}*/

BOOST_AUTO_TEST_SUITE_END() // MonoRuntime