#include <string>
#include <memory>
#include <iostream>

#include <boost/test/unit_test.hpp>
#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/Core/EntityManager.h"
#include "Badumna/Replication/SpatialEntityStateSegment.h"
#include "Badumna/Replication/OriginalWrapper.h"
#include "Badumna/Replication/ReplicaWrapper.h"
#include "ReplicationDelegateStub.h"
#include "LocalObject.h"
#include "ReplicaObject.h"

using namespace std;
using namespace Badumna;
using namespace BadumnaUnitTest;

using std::string;
using std::auto_ptr;

struct EntityManagerFixture
{
    String id;
    EntityManager manager;
    LocalObject* local_object;
    OriginalWrapper* original_wrapper;
    ReplicaObject* replica_object;
    ReplicaWrapper* replica_wrapper;

    BadumnaId fake;
    BadumnaId &fake_ref;
    
    // network scene object can not be easily created without initializing the whole
    // badumna stack. so here a faked one is used. 
    NetworkScene &scene;

    EntityManagerFixture()
        : id(L"my_id"),
        manager(),
        local_object(new LocalObject()),
        original_wrapper(NULL),
        replica_object(new ReplicaObject()),
        replica_wrapper(NULL),
        fake(BadumnaRuntime::Instance().CreateBadumnaId("Open|127.0.0.1:1-1")),
        fake_ref(fake),
        scene(reinterpret_cast<NetworkScene &>(fake_ref))
    {
    }

    virtual ~EntityManagerFixture()
    {
        delete local_object;
        delete replica_object;
    }
private:
    DISALLOW_COPY_AND_ASSIGN(EntityManagerFixture);
};

BOOST_FIXTURE_TEST_SUITE(BadumnaEntityManager, EntityManagerFixture)

BOOST_AUTO_TEST_CASE(OriginalWrapperCanBeAddedAndRemoved)
{   
    String original_id(L"original_id");

    original_wrapper = BadumnaRuntime::Instance().CreateOriginalWrapper(local_object);
    original_wrapper->SetUniqueId(original_id);

    manager.AddOriginal(original_id, original_wrapper);
    OriginalWrapper* ret = manager.GetOriginal(original_id);

    BOOST_REQUIRE(manager.ContainsOriginal(original_id));
    BOOST_REQUIRE(manager.ContainsOriginal(*(original_wrapper->GetSpatialOriginal())));
    BOOST_CHECK_EQUAL(ret, original_wrapper);

    manager.RemoveOriginal(original_id);
    BOOST_REQUIRE(!manager.ContainsOriginal(original_id));
}

BOOST_AUTO_TEST_CASE(ReplicaWrapperCanBeAddedAndRemoved)
{
    String replica_id(L"replica_id");
    replica_wrapper = BadumnaRuntime::Instance().CreateReplicaWrapper(id, replica_object);
    
    manager.AddReplica(replica_id, replica_wrapper);
    ReplicaWrapper *ret = manager.GetReplica(replica_id);

    BOOST_REQUIRE(manager.ContainsReplica(replica_id));
    BOOST_REQUIRE(manager.ContainsReplica(*(replica_wrapper->GetReplica())));
    BOOST_CHECK_EQUAL(ret, replica_wrapper);

    manager.RemoveReplica(replica_id);
    BOOST_REQUIRE(!manager.ContainsReplica(replica_id));
}

BOOST_AUTO_TEST_CASE(InvokeDeserializeCanBeInvoked)
{
    char data[16];
    String replica_id(L"replica_id");

    BooleanArray included_parts; 
    InputStream stream(data, 16); 

    replica_wrapper = BadumnaRuntime::Instance().CreateReplicaWrapper(id, replica_object);
    manager.AddReplica(replica_id, replica_wrapper);
    int count = replica_object->GetDeserializeCounter();
    manager.InvokeDeserialize(replica_id, included_parts, &stream, 1);
    BOOST_CHECK_EQUAL(count + 1, replica_object->GetDeserializeCounter());
    manager.RemoveReplica(replica_id);
}

BOOST_AUTO_TEST_CASE(InvokeSerializeCanBeInvoked)
{
    String original_id(L"original_id");

    BooleanArray included_parts;
    OutputStream stream;

    original_wrapper = BadumnaRuntime::Instance().CreateOriginalWrapper(local_object);
    original_wrapper->SetUniqueId(original_id);
    manager.AddOriginal(original_id, original_wrapper);
    int count = local_object->GetSerializeCounter();
    manager.InvokeSerialize(original_id, included_parts, &stream);
    BOOST_CHECK_EQUAL(count + 1, local_object->GetSerializeCounter());
    manager.RemoveOriginal(original_id);
}

BOOST_AUTO_TEST_CASE(InovkeSetReplicaPropertyWorksAsExpected)
{
    String replica_id(L"replica_id");
    replica_wrapper = BadumnaRuntime::Instance().CreateReplicaWrapper(id, replica_object);
    manager.AddReplica(replica_id, replica_wrapper);

    float x = 12.3f;
    float y = -3.4f;
    float z = 5.6f;

    manager.InovkeSetReplicaProperty(replica_id, Badumna::StateSegment_Position, x, y, z);

    BOOST_CHECK_CLOSE(x, replica_object->Position().GetX(), 0.01f);
    BOOST_CHECK_CLOSE(y, replica_object->Position().GetY(), 0.01f);
    BOOST_CHECK_CLOSE(z, replica_object->Position().GetZ(), 0.01f);
    manager.RemoveReplica(replica_id);
}

BOOST_AUTO_TEST_CASE(InvokeHandleReplicaEventWorksAsExpected)
{
    char data[16];
    InputStream stream(data, 16); 
    String replica_id(L"replica_id");
    replica_wrapper = BadumnaRuntime::Instance().CreateReplicaWrapper(id, replica_object);
    manager.AddReplica(replica_id, replica_wrapper);

    int c = replica_object->GetEventCounter();
    manager.InvokeHandleReplicaEvent(replica_id, &stream);
    BOOST_CHECK_EQUAL(c + 1, replica_object->GetEventCounter());
    manager.RemoveReplica(replica_id);
}

BOOST_AUTO_TEST_CASE(InvokeHandleOriginalEventWorksAsExpected)
{
    char data[16];
    InputStream stream(data, 16); 
    String original_id(L"original_id");
    original_wrapper = BadumnaRuntime::Instance().CreateOriginalWrapper(local_object);
    original_wrapper->SetUniqueId(original_id);
    manager.AddOriginal(original_id, original_wrapper);
    int counter = local_object->GetEventCounter();

    manager.InvokeHandleOriginalEvent(original_id, &stream);
    BOOST_CHECK_EQUAL(counter + 1, local_object->GetEventCounter());
    manager.RemoveOriginal(original_id);
}

BOOST_AUTO_TEST_SUITE_END() // EntityManager