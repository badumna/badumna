//---------------------------------------------------------------------------------
// <copyright file="DeadReckonableReplica.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include "DeadReckonableReplica.h"
#include "ReplicaObject.h"

namespace BadumnaUnitTest
{
    DeadReckonableReplica::DeadReckonableReplica() 
        : guid(BadumnaId::None()),
        velocity(0.0f, 0.0f, 0.0f), 
        position(0.0f, 0.0f, 0.0f),  
        radius(0.0f), 
        area_of_interest_radius(0.0f), 
        counter(0),
        deserialize_counter(0),
        event_counter(0)
    {
    }

    DeadReckonableReplica::~DeadReckonableReplica()
    {
    }

    BadumnaId DeadReckonableReplica::Guid() const
    {
        return this->guid;
    }

    void DeadReckonableReplica::SetGuid(BadumnaId const &id)
    {
        this->guid = id;
    }

    void DeadReckonableReplica::HandleEvent(InputStream *)
    {
        event_counter++;
    }

    Vector3 DeadReckonableReplica::Position() const
    {
        return this->position;
    }

    void DeadReckonableReplica::SetPosition(Vector3 const &value)
    {
        this->position = value;
    }

    float DeadReckonableReplica::Radius() const
    {
        return this->radius;
    }

    void DeadReckonableReplica::SetRadius(float value)
    {
        this->radius = value;
    }

    float DeadReckonableReplica::AreaOfInterestRadius() const
    {
        return this->area_of_interest_radius;
    }

    void DeadReckonableReplica::SetAreaOfInterestRadius(float value)
    {
        this->area_of_interest_radius = value;
    }

    void DeadReckonableReplica::Deserialize(
        BooleanArray const &included_parts, 
        InputStream *stream, 
        int /*estimated_milliseconds_since_departure*/)
    {
        deserialize_counter++;

        if(included_parts[Counter] == true)
        {
            (*stream) >> this->counter;
        }
    }

    Vector3 DeadReckonableReplica::Velocity() const
    {
        return velocity;
    }

    void DeadReckonableReplica::SetVelocity(Vector3 const &v)
    {
        velocity = v;
    }

    void DeadReckonableReplica::AttemptMovement(Vector3 const &reckoned_position)
    {
        position = reckoned_position;
    }
}