//---------------------------------------------------------------------------------
// <copyright file="DeadReckonableOriginal.h" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#ifndef REPLICATION_TEST_DEADRECKONABLE_LOCAL_OBJECT_H
#define REPLICATION_TEST_DEADRECKONABLE_LOCAL_OBJECT_H

#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/DataTypes/Vector3.h"
#include "Badumna/Replication/IDeadReckonableSpatialOriginal.h"

namespace BadumnaUnitTest
{
    using Badumna::IDeadReckonableSpatialOriginal;
    using Badumna::BadumnaId;
    using Badumna::Vector3;
    using Badumna::BooleanArray;
    using Badumna::OutputStream;
    using Badumna::InputStream;

    class DeadReckonableOriginal : public IDeadReckonableSpatialOriginal
    {
    public:
        DeadReckonableOriginal();

        ~DeadReckonableOriginal();

        //
        // IEntity properties
        // 
        BadumnaId Guid() const;

        void SetGuid(BadumnaId const &id);

        void HandleEvent(InputStream *stream);

        //
        // ISpatialEntity properties
        //
        Vector3 Position() const;

        void SetPosition(Vector3 const &position);

        float Radius() const;

        void SetRadius(float val);

        float AreaOfInterestRadius() const;

        void SetAreaOfInterestRadius(float val);

        //
        // ISpatialOriginal properties
        //
        void Serialize(BooleanArray const &required_parts, OutputStream *stream);

        Vector3 Velocity() const;

        void SetVelocity(Vector3 const &velocity);

        void AttemptMovement(Vector3 const &reckoned_position);

        //
        // app specific.
        // 
        int GetCounter()
        {
            return this->counter;
        }

        void SetCounter(int value);

        int GetSerializeCounter()
        {
            return serialize_counter;
        }

        int GetEventCounter()
        {
            return event_counter;
        }
        
    private:
        BadumnaId guid;
        
        Vector3 velocity;
        Vector3 position;
        float radius;
        float area_of_interest_radius;

        int counter; 
        int serialize_counter;
        int event_counter;
    };
}

#endif // REPLICATION_TEST_DEADRECKONABLE_LOCAL_OBJECT_H