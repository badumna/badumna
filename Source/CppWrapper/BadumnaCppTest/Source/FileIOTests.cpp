//---------------------------------------------------------------------------------
// <copyright file="FileIOTests.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <boost/test/unit_test.hpp>
#include "Badumna/Utils/FileIO.h"

using namespace Badumna;

struct FileIOFixture
{
    FileIOFixture() 
    {
    }
    
    virtual ~FileIOFixture() 
    {
    }
};

BOOST_FIXTURE_TEST_SUITE(BadumnaFileIO, FileIOFixture)

BOOST_AUTO_TEST_CASE(GetCurrentDirReturnsSomething)
{
    String cur_dir = GetCurrentDir();
    BOOST_REQUIRE(cur_dir.Length() > 0);
}

BOOST_AUTO_TEST_CASE(GetCurrentDirReturnsADirectoryThatDoesExist)
{
    String cur_dir = GetCurrentDir();
    BOOST_REQUIRE(DirectoryExists(cur_dir));
}

BOOST_AUTO_TEST_CASE(DirectoryExistsResultMakesSense)
{
    BOOST_REQUIRE(!DirectoryExists("i_dont_think_this_dir_exists_on_any_dev_machine_123456789"));
}

BOOST_AUTO_TEST_SUITE_END() // FileIO