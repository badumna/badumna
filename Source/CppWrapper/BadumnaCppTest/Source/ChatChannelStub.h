#ifndef CHAT_CHANNEL_STUB_H
#define CHAT_CHANNEL_STUB_H

#include "Badumna/Chat/IChatChannel.h"
#include "Badumna/Chat/ChatChannelId.h"
#include "Badumna/Datatypes/BadumnaId.h"
#include "Badumna/Datatypes/String.h"


namespace BadumnaUnitTest {
    class ChatChannelStub : public Badumna::IChatChannel
    {

    public:
        ~ChatChannelStub();

        Badumna::ChatChannelId Id();

        void SendMessage(Badumna::String message);

        void Unsubscribe();

        explicit ChatChannelStub();
    };
}
#endif // CHAT_CHANNEL_STUB_H