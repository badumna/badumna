//---------------------------------------------------------------------------------
// <copyright file="DeadReckonableOriginalTests.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <boost/test/unit_test.hpp>

#include <iostream>
#include <sstream>

#include "DeadReckonableOriginal.h"
#include "Badumna/Utils/BasicTypes.h"
#include "Badumna/Replication/IDeadReckonableSpatialOriginal.h"

using namespace Badumna;
using namespace BadumnaUnitTest;

struct DeadReckonableOriginalFixture
{
    DeadReckonableOriginalFixture()
        : original(new DeadReckonableOriginal())
    {
    }
    
    virtual ~DeadReckonableOriginalFixture() 
    { 
        delete original;
    }

    DeadReckonableOriginal *original;

private:
    DISALLOW_COPY_AND_ASSIGN(DeadReckonableOriginalFixture);
};

BOOST_FIXTURE_TEST_SUITE(BadumnaDeadReckonableOriginal, DeadReckonableOriginalFixture)

BOOST_AUTO_TEST_CASE(DeadReckonableOriginalIsDeadReckonable)
{
    BOOST_REQUIRE(original->IsDeadReckonable());
}

BOOST_AUTO_TEST_CASE(DeadReckonableOriginalIsSubClassOfIDeadReckonableSpatialOriginal)
{
    IDeadReckonableSpatialOriginal *base = dynamic_cast<IDeadReckonableSpatialOriginal *>(original);
    BOOST_REQUIRE(base != NULL);
}

BOOST_AUTO_TEST_SUITE_END() // DeadReckonableOriginal