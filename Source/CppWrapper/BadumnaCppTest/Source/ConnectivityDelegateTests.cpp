#include <boost/test/unit_test.hpp>

#include "ConnectivityDelegateStub.h"
#include "Badumna/Core/ConnectivityStatusDelegate.h"

using namespace Badumna;
using namespace BadumnaUnitTest;

struct ConnectivityDelegateFixture
{
    ConnectivityDelegateFixture() 
    {
    }
    
    virtual ~ConnectivityDelegateFixture() 
    { 
    }
};

BOOST_FIXTURE_TEST_SUITE(BadumnaConnectivityDelegate, ConnectivityDelegateFixture)

BOOST_AUTO_TEST_CASE(DelegateCanBeInvoked)
{
    ConnectivityDelegateStub stub;
    BOOST_CHECK_EQUAL(0, stub.GetOnlineCount());
    ConnectivityStatusDelegate d(&ConnectivityDelegateStub::OnlineEventHandler, stub);
    d();
    BOOST_CHECK_EQUAL(1, stub.GetOnlineCount());
}

BOOST_AUTO_TEST_SUITE_END() // ConnectivityDelegate