#include <string>
#include <memory>
#include <iostream>

#include <boost/test/unit_test.hpp>
#include "Badumna/Core/BadumnaRuntime.h"
#include "Badumna/Replication/SpatialEntityStateSegment.h"
#include "Badumna/Replication/ReplicationServiceManager.h"
#include "Badumna/Replication/OriginalWrapper.h"
#include "Badumna/Replication/ReplicaWrapper.h"
#include "ReplicationDelegateStub.h"
#include "LocalObject.h"
#include "ReplicaObject.h"

using namespace std;
using namespace Badumna;
using namespace BadumnaUnitTest;

using std::string;
using std::auto_ptr;

struct ReplicationServiceManagerFixture
{
    String id;
    ReplicationServiceManager manager;
    LocalObject* local_object;
    OriginalWrapper* original_wrapper;
    ReplicaObject* replica_object;
    ReplicaWrapper* replica_wrapper;

    BadumnaId fake;
    BadumnaId &fake_ref;
    
    // network scene object can not be easily created without initializing the whole
    // badumna stack. so here a faked one is used. 
    NetworkScene &scene;

    ReplicationServiceManagerFixture()
        : id(L"my_id"),
        manager(),
        local_object(new LocalObject()),
        original_wrapper(NULL),
        replica_object(new ReplicaObject()),
        replica_wrapper(NULL),
        fake(BadumnaRuntime::Instance().CreateBadumnaId("Open|127.0.0.1:1-1")),
        fake_ref(fake),
        scene(reinterpret_cast<NetworkScene &>(fake_ref))
    {
    }

    virtual ~ReplicationServiceManagerFixture()
    {
        delete local_object;
        delete replica_object;
    }
private:
    DISALLOW_COPY_AND_ASSIGN(ReplicationServiceManagerFixture);
};

BOOST_FIXTURE_TEST_SUITE(BadumnaReplicationServiceManager, ReplicationServiceManagerFixture)

BOOST_AUTO_TEST_CASE(SpatialDelegatesCanBeAddedAndAccessed)
{
    String id(L"delegate_id");
    ReplicationDelegateStub stub;
    
    int create_count = stub.GetCreateCount();
    int remove_count = stub.GetRemoveCount();
    CreateSpatialReplicaDelegate create_callback(&ReplicationDelegateStub::Create, stub);
    RemoveSpatialReplicaDelegate remove_callback(&ReplicationDelegateStub::Remove, stub);
    manager.AddReplicaDelegate(id, create_callback, remove_callback);

    CreateSpatialReplicaDelegate ret_create_callback = manager.GetCreateReplicaDelegate(id);
    RemoveSpatialReplicaDelegate ret_remove_callback = manager.GetRemoveReplicaDelegate(id);

    ret_create_callback(scene, fake, 1);
    ret_remove_callback(scene, *replica_object);

    BOOST_CHECK_EQUAL(create_count + 1, stub.GetCreateCount());
    BOOST_CHECK_EQUAL(remove_count + 1, stub.GetRemoveCount());

    manager.RemoveReplicaDelegate(id);
}

BOOST_AUTO_TEST_CASE(SpatialDelegatesCanBeAddedAndRemoved)
{
    String id(L"delegate_id");
    ReplicationDelegateStub stub;
    
    CreateSpatialReplicaDelegate create_callback(&ReplicationDelegateStub::Create, stub);
    RemoveSpatialReplicaDelegate remove_callback(&ReplicationDelegateStub::Remove, stub);
    manager.AddReplicaDelegate(id, create_callback, remove_callback);
    manager.RemoveReplicaDelegate(id);
}

BOOST_AUTO_TEST_CASE(CreateSpatialReplicaDelegateCanBeInvoked)
{
    String id(L"delegate_id");
    ReplicationDelegateStub stub;
    
    int create_count = stub.GetCreateCount();
    CreateSpatialReplicaDelegate create_callback(&ReplicationDelegateStub::Create, stub);
    RemoveSpatialReplicaDelegate remove_callback(&ReplicationDelegateStub::Remove, stub);
    manager.AddReplicaDelegate(id, create_callback, remove_callback);

    manager.InvokeCreateSpatialReplicaDelegate(id, scene, fake, 1);

    BOOST_CHECK_EQUAL(create_count + 1, stub.GetCreateCount());
    manager.RemoveReplicaDelegate(id);
}

BOOST_AUTO_TEST_CASE(RemoveSpatialReplicaDelegateCanBeInvoked)
{
    String id(L"delegate_id");
    ReplicationDelegateStub stub;
    
    int remove_count = stub.GetRemoveCount();
    CreateSpatialReplicaDelegate create_callback(&ReplicationDelegateStub::Create, stub);
    RemoveSpatialReplicaDelegate remove_callback(&ReplicationDelegateStub::Remove, stub);
    manager.AddReplicaDelegate(id, create_callback, remove_callback);

    std::auto_ptr<ReplicaWrapper> r(BadumnaRuntime::Instance().CreateReplicaWrapper(id, replica_object));
    manager.InvokeRemoveSpatialReplicaDelegate(id, scene, r.get());
    BOOST_CHECK_EQUAL(remove_count + 1, stub.GetRemoveCount());
    manager.RemoveReplicaDelegate(id);
}

BOOST_AUTO_TEST_SUITE_END() // ReplicationServiceManager