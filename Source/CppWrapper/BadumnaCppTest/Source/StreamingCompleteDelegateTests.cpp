#include <boost/test/unit_test.hpp>
#include "Badumna/Streaming/StreamingCompleteDelegate.h"

using namespace Badumna;

class StreamingCompleteDelegateStub
{
public:
    StreamingCompleteDelegateStub()
        : counter(0)
    {
    }

    void Complete(String const &, String const &)
    {
        counter++;
    }

    int GetCounter() const
    {
        return counter;
    }

private:
    int counter;
};

int g_complete_delegate_counter = 0;

void StreamingCompleteDelegateFunction(String const &, String const &)
{
    g_complete_delegate_counter++;
}

struct StreamingCompleteDelegateFixture
{
    StreamingCompleteDelegateFixture()
    {
    }
    
    virtual ~StreamingCompleteDelegateFixture() 
    { 
    }
};

BOOST_FIXTURE_TEST_SUITE(BadumnaStreamingCompleteDelegate, StreamingCompleteDelegateFixture)

BOOST_AUTO_TEST_CASE(StreamingCompleteDelegateCanBeInvoked)
{
    String s;
    StreamingCompleteDelegateStub stub;
    int counter = stub.GetCounter();
    StreamingCompleteDelegate d1(&StreamingCompleteDelegateStub::Complete, stub);
    d1(s, s);
    BOOST_CHECK_EQUAL(counter + 1, stub.GetCounter());
}

BOOST_AUTO_TEST_CASE(StreamingCompleteDelegateWithFreeFuncCanBeInvoked)
{
    String s;
    int counter = g_complete_delegate_counter;
    StreamingCompleteDelegate d1(StreamingCompleteDelegateFunction);
    d1(s, s);
    BOOST_CHECK_EQUAL(counter + 1, g_complete_delegate_counter);
}

BOOST_AUTO_TEST_CASE(StreamingCompleteDelegateIsCopyableAndAssignable)
{
    String s;
    StreamingCompleteDelegateStub stub;
    StreamingCompleteDelegateStub stub2;
    StreamingCompleteDelegate d1(&StreamingCompleteDelegateStub::Complete, stub);
    StreamingCompleteDelegate d2(&StreamingCompleteDelegateStub::Complete, stub2);
    StreamingCompleteDelegate d3 = d2;
    
    int counter = stub2.GetCounter();
    d1 = d2;
    d1(s, s);
    BOOST_CHECK_EQUAL(counter + 1, stub2.GetCounter());
}

BOOST_AUTO_TEST_SUITE_END()