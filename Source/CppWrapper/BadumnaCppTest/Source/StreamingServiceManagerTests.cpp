#include <boost/test/unit_test.hpp>
#include "Badumna/Streaming/StreamingServiceManager.h"

using namespace Badumna;

int g_receive_request_delegate_counter2 = 0;
void StreamRequestDelegateFunction2(StreamRequest *)
{
    g_receive_request_delegate_counter2++;
}

int g_complete_delegate_counter2 = 0;
void StreamingCompleteDelegateFunction2(String const &, String const &)
{
    g_complete_delegate_counter2++;
}

struct StreamingServiceManagerFixture
{
    StreamingServiceManagerFixture()
        : manager(),
        id("test_id")
    {
    }
    
    virtual ~StreamingServiceManagerFixture() 
    {
    }

    StreamingServiceManager manager;
    String id;
};

BOOST_FIXTURE_TEST_SUITE(BadumnaStreamingServiceManager, StreamingServiceManagerFixture)

BOOST_AUTO_TEST_CASE(SendCompleteDelegateCanBeAddedAndInvoked)
{
    String s;
    StreamingCompleteDelegate send_delegate(StreamingCompleteDelegateFunction2);
    manager.AddSendCompleteDelegate(id, send_delegate);
    int counter = g_complete_delegate_counter2;
    manager.InvokeSendCompleteDelegate(id, s, s);
    BOOST_CHECK_EQUAL(counter + 1, g_complete_delegate_counter2);
}

BOOST_AUTO_TEST_CASE(ReceiveCompleteDelegateCanBeAddedAndInvoked)
{
    String s;
    StreamingCompleteDelegate receive_delegate(StreamingCompleteDelegateFunction2);
    manager.AddReceiveCompleteDelegate(id, receive_delegate);
    int counter = g_complete_delegate_counter2;
    manager.InvokeReceiveCompleteDelegate(id, s, s);
    BOOST_CHECK_EQUAL(counter + 1, g_complete_delegate_counter2);
}

BOOST_AUTO_TEST_CASE(RequestDelegateCanBeAddedAndInvoked)
{
    ReceiveStreamRequestDelegate d1(StreamRequestDelegateFunction2);
    manager.AddRequestDelegate(id, d1);
    int counter = g_receive_request_delegate_counter2;
    manager.InvokeRequestDelegate(id, NULL);
    BOOST_CHECK_EQUAL(counter + 1, g_receive_request_delegate_counter2);
}

BOOST_AUTO_TEST_SUITE_END()