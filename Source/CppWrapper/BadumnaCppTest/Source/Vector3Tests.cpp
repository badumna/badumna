# include <sstream>
#include <exception>
#include <cmath>

#include <boost/test/unit_test.hpp>
#include "Badumna/DataTypes/Vector3.h"
#include "Badumna/DataTypes/BooleanArray.h"

using namespace Badumna;
using std::wstringstream;

struct Vector3Fixture
{
    Vector3Fixture() 
    {
    }
    
    virtual ~Vector3Fixture() 
    { 
    }
};

BOOST_FIXTURE_TEST_SUITE(BadumnaVector3, Vector3Fixture)

BOOST_AUTO_TEST_CASE(Vector3SimpleConstructorWorks)
{
    Vector3 v(10.0f, 10.0f, 10.0f);
    BOOST_CHECK_CLOSE(10.0, v.GetX(), 1E-5f);   
    BOOST_CHECK_CLOSE(10.0, v.GetY(), 1E-5f);
    BOOST_CHECK_CLOSE(10.0, v.GetZ(), 1E-5f);
}

BOOST_AUTO_TEST_CASE(Vector3DefaultConstructorWorks)
{
    Vector3 v;
    BOOST_CHECK_CLOSE(0.0, v.GetX(), 1E-5f);    
    BOOST_CHECK_CLOSE(0.0, v.GetY(), 1E-5f);
    BOOST_CHECK_CLOSE(0.0, v.GetZ(), 1E-5f);
}

BOOST_AUTO_TEST_CASE(Vector3CopyConstructorWorks)
{
    Vector3 v1(10.0f, 10.0f, 10.0f);
    Vector3 v2(v1);

    BOOST_CHECK_CLOSE(10.0f, v1.GetX(), 1E-5f); 
    BOOST_CHECK_CLOSE(10.0f, v1.GetY(), 1E-5f);
    BOOST_CHECK_CLOSE(10.0f, v1.GetZ(), 1E-5f);

    BOOST_CHECK_CLOSE(v1.GetX(), v2.GetX(), 1E-5f);
    BOOST_CHECK_CLOSE(v1.GetY(), v2.GetY(), 1E-5f);
    BOOST_CHECK_CLOSE(v1.GetZ(), v2.GetZ(), 1E-5f);
}

BOOST_AUTO_TEST_CASE(Vector3AssignOperatorWorks)
{
    Vector3 v1(10.0f, 10.0f, 10.0f);
    
    BOOST_CHECK_CLOSE(10.0, v1.GetX(), 1E-5f);  
    BOOST_CHECK_CLOSE(10.0, v1.GetY(), 1E-5f);
    BOOST_CHECK_CLOSE(10.0, v1.GetZ(), 1E-5f);

    Vector3 v2 = v1;

    BOOST_CHECK_CLOSE(v1.GetX(), v2.GetX(), 1E-5f); 
    BOOST_CHECK_CLOSE(v1.GetY(), v2.GetY(), 1E-5f);
    BOOST_CHECK_CLOSE(v1.GetZ(), v2.GetZ(), 1E-5f);

    // change the value of v2 won't affect v1
    v2.SetX(20.0f);
    BOOST_CHECK_CLOSE(10.0, v1.GetX(), 1E-5);   
    BOOST_CHECK_CLOSE(20.0, v2.GetX(), 1E-5);   
}

BOOST_AUTO_TEST_CASE(MagnitudeReturnsCorrectResult)
{
    Vector3 v1(1.0f, 1.0f, 1.0f);
    BOOST_CHECK_CLOSE(1.732f, v1.Magnitude(), 1E-2f);

    Vector3 v2(0.0f, 0.0f, 0.0f);
    BOOST_CHECK_CLOSE(0.0f, v2.Magnitude(), 1E-2f);

    Vector3 v3(100.0f, 200.0, 300.0);
    BOOST_CHECK_CLOSE(374.1658f, v3.Magnitude(), 1E-2f);
}

BOOST_AUTO_TEST_CASE(Vector3CanOutputToStream)
{
    Vector3 v(1.5f, 2.6f, 3.7f);
    wstringstream s;

    s << v;
    BOOST_REQUIRE(v.ToString() == s.str().c_str());
}

BOOST_AUTO_TEST_CASE(AdditionOperatorWorksAsExpected)
{
    Vector3 v1(1.0f, 2.0f, 3.0f);
    Vector3 v2(1.5f, 2.5f, 3.5f);

    Vector3 v3 = v1 + v2;
    v1 += v2;

    BOOST_REQUIRE(fabs(v1.GetX() - v3.GetX()) < 1E-5f);
    BOOST_REQUIRE(fabs(v1.GetY() - v3.GetY()) < 1E-5f);
    BOOST_REQUIRE(fabs(v1.GetZ() - v3.GetZ()) < 1E-5f);

    BOOST_REQUIRE(fabs(v1.GetX() - 2.5f) < 1E-5f);
    BOOST_REQUIRE(fabs(v1.GetY() - 4.5f) < 1E-5f);
    BOOST_REQUIRE(fabs(v1.GetZ() - 6.5f) < 1E-5f);
}

BOOST_AUTO_TEST_CASE(SubtractionOperatorWorksAsExpected)
{
    Vector3 v1(1.5f, 2.6f, 3.7f);
    Vector3 v2(1.0f, 2.0f, 3.0f);
    
    Vector3 v3 = v1 - v2;
    v1 -= v2;

    BOOST_REQUIRE(fabs(v1.GetX() - v3.GetX()) < 1E-5f);
    BOOST_REQUIRE(fabs(v1.GetY() - v3.GetY()) < 1E-5f);
    BOOST_REQUIRE(fabs(v1.GetZ() - v3.GetZ()) < 1E-5f);

    BOOST_REQUIRE(fabs(v1.GetX() - 0.5f) < 1E-5f);
    BOOST_REQUIRE(fabs(v1.GetY() - 0.6f) < 1E-5f);
    BOOST_REQUIRE(fabs(v1.GetZ() - 0.7f) < 1E-5f);
}

BOOST_AUTO_TEST_CASE(MultiplicationOperatorWorksAsExpected)
{
    Vector3 v1(1.0f, 2.0f, 3.0f);
    Vector3 v2 = v1 * 2.0f;
    Vector3 v3 = v1 * 2;
    Vector3 v4 = 2.0f * v1;
    
    v1 *= 2.0f;
    v1 *=2;

    BOOST_REQUIRE(fabs(v2.GetX() - 2.0f) < 1E-5f);
    BOOST_REQUIRE(fabs(v2.GetY() - 4.0f) < 1E-5f);
    BOOST_REQUIRE(fabs(v2.GetZ() - 6.0f) < 1E-5f);

    BOOST_REQUIRE(fabs(v3.GetX() - 2.0f) < 1E-5f);
    BOOST_REQUIRE(fabs(v3.GetY() - 4.0f) < 1E-5f);
    BOOST_REQUIRE(fabs(v3.GetZ() - 6.0f) < 1E-5f);

    BOOST_REQUIRE(fabs(v4.GetX() - 2.0f) < 1E-5f);
    BOOST_REQUIRE(fabs(v4.GetY() - 4.0f) < 1E-5f);
    BOOST_REQUIRE(fabs(v4.GetZ() - 6.0f) < 1E-5f);

    BOOST_REQUIRE(fabs(v1.GetX() - 4.0f) < 1E-5f);
    BOOST_REQUIRE(fabs(v1.GetY() - 8.0f) < 1E-5f);
    BOOST_REQUIRE(fabs(v1.GetZ() - 12.0f) < 1E-5f);
}

BOOST_AUTO_TEST_CASE(DivisionOperatorWorksAsExpected)
{
    Vector3 v1(16.0f, 8.0f, 4.0f);
    Vector3 v2 = v1 / 2.0f;
    Vector3 v3 = v1 / 2;

    v1 /= 2.0f;
    v1 /= 2;

    BOOST_REQUIRE(fabs(v2.GetX() - 8.0f) < 1E-5f);
    BOOST_REQUIRE(fabs(v2.GetY() - 4.0f) < 1E-5f);
    BOOST_REQUIRE(fabs(v2.GetZ() - 2.0f) < 1E-5f);

    BOOST_REQUIRE(fabs(v3.GetX() - 8.0f) < 1E-5f);
    BOOST_REQUIRE(fabs(v3.GetY() - 4.0f) < 1E-5f);
    BOOST_REQUIRE(fabs(v3.GetZ() - 2.0f) < 1E-5f);

    BOOST_REQUIRE(fabs(v1.GetX() - 4.0f) < 1E-5f);
    BOOST_REQUIRE(fabs(v1.GetY() - 2.0f) < 1E-5f);
    BOOST_REQUIRE(fabs(v1.GetZ() - 1.0f) < 1E-5f);
}

BOOST_AUTO_TEST_CASE(EqualToOperatorWorksAsExpected)
{
    Vector3 v1(1.0f, 2.0f, 3.0f);
    Vector3 v2(1.0f, 2.0f, 3.0f);
    Vector3 v3(1.1f, 2.0f, 3.0f);
    
    BOOST_REQUIRE(v1 == v2);
    BOOST_REQUIRE(v2 == v1);
    BOOST_REQUIRE(v3 != v1);
    BOOST_REQUIRE(v1 != v3);
}

BOOST_AUTO_TEST_SUITE_END() // Vector3