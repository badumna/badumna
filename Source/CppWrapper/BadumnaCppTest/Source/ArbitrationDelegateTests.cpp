//---------------------------------------------------------------------------------
// <copyright file="ArbitrationDelegateTests.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <boost/test/unit_test.hpp>

#include "ArbitrationDelegateStub.h"
#include "Badumna/Arbitration/ArbitrationDelegates.h"

using namespace Badumna;
using namespace BadumnaUnitTest;

struct ArbitrationDelegateFixture
{
    ArbitrationDelegateStub stub;

    ArbitrationDelegateFixture()
        : stub()
    {
    }
    
    virtual ~ArbitrationDelegateFixture() 
    {
    }
};

BOOST_FIXTURE_TEST_SUITE(BadumnaArbitrationDelegate, ArbitrationDelegateFixture)

BOOST_AUTO_TEST_CASE(ArbitrationConnectionDelegateCanBeInvoked)
{
    int count = stub.GetConnectionDelegateCount();
    ArbitrationConnectionDelegate callback(&ArbitrationDelegateStub::ConnectionDelegate, stub);
    callback(Badumna::ServiceConnection_Success);
    callback(Badumna::ServiceConnection_ConnectionTimeout);
    callback(Badumna::ServiceConnection_ServiceNotAvailavle);
    BOOST_CHECK_EQUAL(count + 3, stub.GetConnectionDelegateCount());
}

BOOST_AUTO_TEST_CASE(ArbitrationConnectionFailureDelegateCanBeInvoked)
{
    int count = stub.GetConnectionFailureDelegateCount();
    ArbitrationConnectionFailureDelegate callback(&ArbitrationDelegateStub::ConnectionFailureDelegate, stub);   
    callback();
    BOOST_CHECK_EQUAL(count + 1, stub.GetConnectionFailureDelegateCount());
}

BOOST_AUTO_TEST_CASE(ArbitrationServerMessageDelegateCanBeInvoked)
{
    char buffer[16];
    int count = stub.GetServerMessageDelegateCount();
    ArbitrationServerMessageDelegate callback(&ArbitrationDelegateStub::ServerMessageDeledate, stub);
    InputStream s(buffer, 16);
    callback(&s);
    BOOST_CHECK_EQUAL(count + 1, stub.GetServerMessageDelegateCount());
}

BOOST_AUTO_TEST_CASE(ArbitrationClientMessageDelegateCanBeInvoked)
{
    char buffer[16];
    int count = stub.GetClientMessageDelegateCount();
    ArbitrationClientMessageDelegate callback(&ArbitrationDelegateStub::ClientMessageDelegate, stub);
    InputStream s(buffer, 16);
    callback(1, &s);
    BOOST_CHECK_EQUAL(count + 1, stub.GetClientMessageDelegateCount());
}

BOOST_AUTO_TEST_CASE(ArbitrationClientDisconnectDelegateCanBeInvoked)
{
    int count = stub.GetClientDisconnectDelegateCount();
    ArbitrationClientDisconnectDelegate callback(&ArbitrationDelegateStub::ClientDisconnectDelegate, stub);
    callback(1);
    BOOST_CHECK_EQUAL(count + 1, stub.GetClientDisconnectDelegateCount());
}


BOOST_AUTO_TEST_SUITE_END() // ArbitrationDelegates