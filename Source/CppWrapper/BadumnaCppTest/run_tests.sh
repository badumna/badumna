#!/bin/bash

./BadumnaCppTests --run_test=Badumna* --report_format=xml --report_level=detailed --report_sink=badumna_tests.xml
./BadumnaCppTests --run_test=Stub* --report_format=xml --report_level=detailed --report_sink=badumna_stub_tests.xml
