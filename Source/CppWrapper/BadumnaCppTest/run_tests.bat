@echo off

REM always run all tests so all failures will be kept in log. 
BadumnaCppTests.exe --run_test=Badumna* --report_format=xml --report_level=detailed --report_sink=badumna_tests.xml
set RET1=%ERRORLEVEL%
BadumnaCppTests.exe --run_test=Stub* --report_format=xml --report_level=detailed --report_sink=badumna_stub_tests.xml
set RET2=%ERRORLEVEL%

msxsl.exe badumna_tests.xml BoostToBitten.xsl > badumna_tests_report.xml
msxsl.exe badumna_stub_tests.xml BoostToBitten.xsl > badumna_stub_tests_report.xml 

if not %RET1%==0 exit /b -1
if not %RET2%==0 exit /b -1

