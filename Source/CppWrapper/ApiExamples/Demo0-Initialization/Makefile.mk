
DEMO0=Demo0-Initialization

ifeq ($(OS), Windows_NT)
DEMO0:=$(DEMO0).exe
endif

DEMO0_PATH=Demo0-Initialization
DEMO0_OBJS=$(DEMO0_PATH)/Program.o

$(DEMO0): $(DEMO0_OBJS) | check_libmono_path
	@echo [Linking]: $@
	$(VERBOSE_MODE)$(CXX) $(LDFLAGS) -o $@ $(DEMO0_OBJS) $(LIBS)

DEMOS+=$(DEMO0)
ALL_OBJS+=$(DEMO0_OBJS)
INSTALL_FILES+=$(DEMO0)
