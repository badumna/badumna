//-----------------------------------------------------------------------
// <copyright file="Program.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include <iostream>
#include <stdio.h>
#include <memory>
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/Core/RuntimeInitializer.h"
#include "Badumna/Configuration/Options.h"

int StartBadumna()
{
#ifdef CLOUD
    std::wcout << "Enter cloud ID: ";
    std::wstring cloud_id;
    std::wcin >> cloud_id;
    std::auto_ptr<Badumna::NetworkFacade> facade(Badumna::NetworkFacade::Create(cloud_id.c_str()));
#else
    // configuration options used for this local badumna client. 
    Badumna::Options options;
    options.GetConnectivityModule().SetStartPortRange(21300);
    options.GetConnectivityModule().SetEndPortRange(21399);
    options.GetConnectivityModule().SetMaxPortsToTry(3);
    options.GetConnectivityModule().EnableBroadcast();
    options.GetConnectivityModule().SetBroadcastPort(21250);
    options.GetConnectivityModule().ConfigureForLan();
    options.GetConnectivityModule().SetApplicationName(L"demo0-initialization");

    std::auto_ptr<Badumna::NetworkFacade> facade(Badumna::NetworkFacade::Create(options));
#endif // CLOUD

    if(!facade->Login())
    {
        // failed to login to the badumna network
        std::wcerr << L"failed to login to the badumna network." << std::endl;
        return 1;
    }

    // print out the network status
    // you should be able to see the network status (e.g. private and public address) of this client peer. 
    std::wcout << facade->GetNetworkStatus() << std::endl;
	std::wcout << "Press <Enter> to exit..." << std::endl;
	getchar();

    // shutdown the badumna engine.
    facade->Shutdown();

    return 0;
}

int main(int, char **)
{
    // initializer is a RAII object which will initialize the runtime in its constructor and shutdown the runtime
    // in its destructor. 
#ifdef WIN32
    Badumna::BadumnaRuntimeInitializer initializer;
#else
    Badumna::BadumnaRuntimeInitializer2 initializer("mono_config_file");
#endif

    return StartBadumna();
}