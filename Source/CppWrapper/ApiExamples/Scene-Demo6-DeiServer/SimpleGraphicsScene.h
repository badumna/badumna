//-----------------------------------------------------------------------
// <copyright file="SimpleGraphicsScene.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef SIMPLE_GRAPHICS_SCENE_H
#define SIMPLE_GRAPHICS_SCENE_H

#include <QtGui>

#include "LocalAvatar.h"

namespace ApiExamples
{
    class SimpleGraphicsScene : public QGraphicsScene
    {
    public:
        SimpleGraphicsScene() 
            : QGraphicsScene(), 
            local_avatar_(NULL)
        {
        }

        void RegisterLocalEntity(LocalAvatar *local);
        void RemoveLocalEntity(LocalAvatar *local);

        void RegisterReplicaEntity(Avatar *replica);
        void RemoveReplicaEntity(Avatar *replica);

    protected:
        void keyPressEvent(QKeyEvent *e);
        
    private:
        LocalAvatar *local_avatar_;

        // disallow copy and assign
        SimpleGraphicsScene(SimpleGraphicsScene const &other);
        SimpleGraphicsScene &operator=(SimpleGraphicsScene const &rhs);
    };
}

#endif // SIMPLE_GRAPHICS_SCENE_H