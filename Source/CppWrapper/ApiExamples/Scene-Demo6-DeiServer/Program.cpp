#include <signal.h>
#include <iostream>
#include <QtGui>
#include <QDialog>

#include "Badumna/Core/RuntimeInitializer.h"
#include "MainWindow.h"

using std::auto_ptr;
using Badumna::NetworkFacade;

using ApiExamples::LocalEntity;
using ApiExamples::MainWindow;

int main(int argc, char **)
{
    QApplication app(argc, 0);
    qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));

#ifdef WIN32
    Badumna::BadumnaRuntimeInitializer initializer;
#else
    Badumna::BadumnaRuntimeInitializer2 initializer("mono_config_file");
#endif

    MainWindow window;
    window.Initialize(QObject::tr("Scene Demo 6 - Dei Server"));

    return window.exec();
}