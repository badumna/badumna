//-----------------------------------------------------------------------
// <copyright file="MainWindow.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include <cassert>
#include <QtGui>
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/Dei/IdentityProvider.h"
#include "Badumna/Dei/Session.h"

#include "MainWindow.h"
#include "ReplicationManager.h"

namespace
{
    using Badumna::String;

    QString BStringToQString(String const &str)
    {
#ifdef WIN32
        assert(sizeof(wchar_t) == 2);
        QString msg;
        msg.setUtf16((ushort *)str.CStr(), str.Length());
        return msg;
#else
        assert(sizeof(wchar_t) == 4);
        ushort *tmp = new ushort[str.Length() + 1];
        for(size_t i = 0; i < str.Length(); ++i)
        {
            tmp[i] = (ushort)(str.CStr()[i]);
        }
        tmp[str.Length()] = 0; 

        QString msg;
        msg.setUtf16(tmp, str.Length());
        delete[] tmp;

        return msg;
#endif
    }
}

namespace ApiExamples
{
    using Dei::LoginResult;
    using Dei::Session;
    using Dei::IdentityProvider;
    using Badumna::Options;
    using Badumna::NetworkFacade;

    MainWindow::MainWindow()
        : facade_(NULL),
        local_entity_(NULL),
        scene_(new SimpleGraphicsScene),
        view_(new QGraphicsView(scene_.get())),
        scene_timer_(new QTimer(scene_.get())),
        regular_timer_(new QTimer(this)),
        main_layout_(NULL),
        replication_manager_(NULL),
        identity_provider_()
    {
    }

    MainWindow::~MainWindow()
    {
        if(replication_manager_.get() != NULL)
        {
            replication_manager_->Shutdown();
        }

        if(local_entity_.get() != NULL)
        {
            scene_->RemoveLocalEntity(local_entity_.get());
        }

        if(facade_.get() != NULL)
        {
            facade_->Shutdown();            
        }
    }

    void MainWindow::Initialize(QString title)
    {
        CreateDeiDetailsBox();

        scene_->setSceneRect(0, 0, 400, 400);
        scene_->setItemIndexMethod(QGraphicsScene::NoIndex);
    
        main_layout_ = new QVBoxLayout;
        main_layout_->addWidget(dei_details_box_);
        main_layout_->addWidget(view_);
        main_layout_->setSizeConstraint(QLayout::SetFixedSize);
        setLayout(main_layout_);

        view_->setCacheMode(QGraphicsView::CacheBackground);
        view_->setRenderHint(QPainter::Antialiasing);
        view_->setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);
        view_->resize(500, 400);
        view_->setFixedWidth(500);
        view_->setFixedHeight(400);
        view_->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        view_->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        view_->setWindowFlags((view_->windowFlags() | Qt::CustomizeWindowHint) & ~Qt::WindowMaximizeButtonHint); 

        QObject::connect(scene_timer_, SIGNAL(timeout()), scene_.get(), SLOT(advance()));
        scene_timer_->start(1000 / 33);

        QObject::connect(regular_timer_, SIGNAL(timeout()), this, SLOT(ProcessRegularState()));
        regular_timer_->start(1000 / 60);

        setWindowTitle(title);
    }

    void MainWindow::CreateDeiDetailsBox()
    {
        // input
        QHBoxLayout *input_layout = new QHBoxLayout;    
        dei_username_edit_ = new QLineEdit();
        dei_password_edit_ = new QLineEdit();
        dei_password_edit_->setEchoMode(QLineEdit::Password);
        QLabel *username_label = new QLabel(tr("Username: "));
        QLabel *password_label = new QLabel(tr("Password: "));

        input_layout->addWidget(username_label);
        input_layout->addWidget(dei_username_edit_);
        input_layout->addWidget(password_label);
        input_layout->addWidget(dei_password_edit_);

        login_button_ = new QPushButton(tr("Login"));
        connect(login_button_, SIGNAL(clicked()), this, SLOT(LoginClicked()));
        input_layout->addWidget(login_button_);

        dei_details_box_ = new QGroupBox(tr("Dei Login Details:"));
        dei_details_box_->setLayout(input_layout);
    }

    void MainWindow::ProcessRegularState()
    {
        if(facade_.get() != NULL && facade_->IsLoggedIn())
        {
            facade_->ProcessNetworkState();
        }
    }

    void MainWindow::LoginClicked()
    {
        QString username = dei_username_edit_->text();
        QString password = dei_password_edit_->text();

        if(TryLoginToDei(username, password))
        {
            InitializeBadumna();
        }
    }

    bool MainWindow::TryLoginToDei(QString const &username, QString const &password)
    {
        Badumna::String bstr_username = (wchar_t *)(username.utf16());
        Badumna::String bstr_password = (wchar_t *)(password.utf16());

        // Create an IdentityProvider object with specified Dei server hostname and port. 
        Session session(L"localhost", 21248);
		session.SetUseSslConnection(false);

        // Authenticate with the Dei server, using the specified username and password
        LoginResult result = session.Authenticate(bstr_username, bstr_password);
        if(!result.WasSuccessful())
        {
            DisplayDeiLoginError(result.GetErrorDescription());
            return false;
        }

        if(result.Characters().empty())
        {
            DisplayDeiLoginError("This user has no characters");
            return false;
        }

        result = session.SelectIdentity(result.Characters()[0], identity_provider_);
        if(!result.WasSuccessful())
        {
            DisplayDeiLoginError(result.GetErrorDescription());
            return false;
        }

        return true;
    }

    void MainWindow::DisplayDeiLoginError(Badumna::String const &message)
    {
        QMessageBox message_box(this);
        message_box.setWindowTitle("Failed to login to Dei.");
        QString msg = ::BStringToQString(message);
        message_box.setText(msg);
        message_box.exec();
    }

    void MainWindow::InitializeBadumna()
    {
        Options options;
        options.GetConnectivityModule().SetStartPortRange(21300);
        options.GetConnectivityModule().SetEndPortRange(21399);
        options.GetConnectivityModule().SetMaxPortsToTry(3);
        options.GetConnectivityModule().EnableBroadcast();
        options.GetConnectivityModule().SetBroadcastPort(21250);
        options.GetConnectivityModule().ConfigureForLan();
        options.GetConnectivityModule().SetApplicationName(L"scene-demo6-deiserver");

        facade_.reset(NetworkFacade::Create(options));
        local_entity_.reset(new LocalEntity(facade_.get()));

        // Login to the badumna network and pass the IdentityProvider object to the facade object.
        facade_->Login(identity_provider_);

        scene_->RegisterLocalEntity(local_entity_.get());

        replication_manager_.reset(new ReplicationManager(this, facade_.get(), local_entity_.get()));
        DisableDeiDetailsInput();
    }

    void MainWindow::DisableDeiDetailsInput()
    {
        dei_username_edit_->setEnabled(false);
        dei_password_edit_->setEnabled(false);
        login_button_->setEnabled(false);
    }
}
