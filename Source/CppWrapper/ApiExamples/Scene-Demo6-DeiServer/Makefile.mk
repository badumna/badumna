
SCENEDEMO6=Scene-Demo6-DeiServer

ifeq ($(OS), Windows_NT)
SCENEDEMO6:=$(SCENEDEMO6).exe
endif

SCENEDEMO6_PATH=Scene-Demo6-DeiServer
SCENEDEMO6_OBJS=$(SCENEDEMO6_PATH)/Avatar.o \
	      $(SCENEDEMO6_PATH)/LocalEntity.o \
	      $(SCENEDEMO6_PATH)/MainWindow.o \
	      $(SCENEDEMO6_PATH)/Program.o \
	      $(SCENEDEMO6_PATH)/ReplicaObject.o \
	      $(SCENEDEMO6_PATH)/ReplicationManager.o \
	      $(SCENEDEMO6_PATH)/SimpleGraphicsScene.o \
	      $(SCENEDEMO6_PATH)/MainWindow.moc.o

$(SCENEDEMO6): $(SCENEDEMO6_OBJS) | check_libmono_path
	@echo [Linking]: $@
	$(VERBOSE_MODE)$(CXX) $(LDFLAGS) -o $@ $(SCENEDEMO6_OBJS) $(LIBS) $(NATIVE_WINDOWS_APP_FLAG)

DEMOS+=$(SCENEDEMO6)
ALL_OBJS+=$(SCENEDEMO6_OBJS)
INSTALL_FILES+=$(SCENEDEMO6)
