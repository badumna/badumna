//-----------------------------------------------------------------------
// <copyright file="Avatar.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef AVATAR_H
#define AVATAR_H

#include <QGraphicsItem>

namespace ApiExamples
{
    class Avatar : public QGraphicsItem
    {
    public:
        Avatar()
            : color_(255, 0, 0)
        {
        }

        virtual ~Avatar()
        {
        }

        QRectF boundingRect() const;
        QPainterPath shape() const;
        void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    protected:
        QColor color_;

        // disallow copy and assign
        Avatar(Avatar const &other);
        Avatar &operator= (Avatar const &rhs);
    };
}

#endif // AVATAR_H