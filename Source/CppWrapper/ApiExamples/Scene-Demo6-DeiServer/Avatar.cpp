//-----------------------------------------------------------------------
// <copyright file="Avatar.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include <QGraphicsScene>
#include <QPainter>
#include <QStyleOption>

#include "Avatar.h"

namespace ApiExamples
{
    QRectF Avatar::boundingRect() const
    {
        qreal adjust = 0.5;
        return QRectF(-8 - adjust, -8 - adjust, 16 + adjust, 16 + adjust);
    }

    QPainterPath Avatar::shape() const
    {
        QPainterPath path;
        path.addRect(-8, -8, 16, 16);
        return path;
    }

    void Avatar::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
    {
        painter->setBrush(color_);
        painter->drawEllipse(-8, -8, 16, 16);
    }
}