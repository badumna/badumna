
SCENEDEMO1=Scene-Demo1-Replication

ifeq ($(OS), Windows_NT)
SCENEDEMO1:=$(SCENEDEMO1).exe
endif

SCENEDEMO1_PATH=Scene-Demo1-Replication
SCENEDEMO1_OBJS=$(SCENEDEMO1_PATH)/Avatar.o \
	      $(SCENEDEMO1_PATH)/LocalEntity.o \
	      $(SCENEDEMO1_PATH)/MainWindow.o \
	      $(SCENEDEMO1_PATH)/Program.o \
	      $(SCENEDEMO1_PATH)/Conversion.o \
	      $(SCENEDEMO1_PATH)/ReplicaObject.o \
	      $(SCENEDEMO1_PATH)/ReplicationManager.o \
	      $(SCENEDEMO1_PATH)/SimpleGraphicsScene.o \
	      $(SCENEDEMO1_PATH)/MainWindow.moc.o

$(SCENEDEMO1): $(SCENEDEMO1_OBJS) | check_libmono_path
	@echo [Linking]: $@
	$(VERBOSE_MODE)$(CXX) $(LDFLAGS) -o $@ $(SCENEDEMO1_OBJS) $(LIBS) $(NATIVE_WINDOWS_APP_FLAG)

DEMOS+=$(SCENEDEMO1)
ALL_OBJS+=$(SCENEDEMO1_OBJS)
INSTALL_FILES+=$(SCENEDEMO1)
