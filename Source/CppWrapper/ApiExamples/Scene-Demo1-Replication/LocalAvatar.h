//-----------------------------------------------------------------------
// <copyright file="LocalAvatar.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef LOCAL_AVATAR_H
#define LOCAL_AVATAR_H

#include "Avatar.h"

namespace ApiExamples
{
    class LocalAvatar : public Avatar
    {
    public:
        LocalAvatar()
            : Avatar()
        {
        }

        virtual void MoveRight() = 0;
        virtual void MoveLeft() = 0;
        virtual void MoveUp() = 0;
        virtual void MoveDown() = 0;

        virtual void SetRandomColor() = 0;

    private:
        // disallow copy and assign
        LocalAvatar(LocalAvatar const &other);
        LocalAvatar &operator=(LocalAvatar const &rhs);
    };
}

#endif // AVATAR_H