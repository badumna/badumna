//-----------------------------------------------------------------------
// <copyright file="LocalEntity.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include <QGraphicsScene>
#include <QPainter>
#include <QStyleOption>

#include "Conversion.h"
#include "Badumna/Match/Match.h"

#include "LocalEntity.h"
#include "RPCDictionary.h"

namespace ApiExamples
{
    using Badumna::Match;
    using Badumna::InputStream;
    using Badumna::OutputStream;
    using Badumna::BooleanArray;

    LocalEntity::LocalEntity(QTextEdit *log, Match *match)
        : LocalAvatar(),
		log_(log),
        match_(match)
    {
        setZValue(10);
    }

    void LocalEntity::HandleEvent(InputStream *stream)
    {
		int rpc_type;
		(*stream) >> rpc_type;

		if ((RPCDictionary)rpc_type == RPC_To_Original)
		{
			log_->append(QString("Reply: RPC from replica"));
		}
    }

    void LocalEntity::SetPosition(qreal x, qreal y)
    {
        setPos(x, y);

        // Notify Badumna that the position property has been updated, it needs to be repliccated to replicas
		match_->FlagForUpdate(this, Segment_Position);
	}

    void LocalEntity::Serialize(BooleanArray const &required_parts, OutputStream *stream)
    {
		if (required_parts[Segment_Position])
		{
			QPointF position = pos();
			(*stream) << position.x();
			(*stream) << position.y();
		}

        if(required_parts[Segment_Color])
        {
            (*stream) << color_.red();
            (*stream) << color_.green();
            (*stream) << color_.blue();
        }
    }

    void LocalEntity::advance(int)
    {
    }

    void LocalEntity::MoveRight()
    {
        QPointF p = this->pos();
        qreal x = p.x() + move_unit;
        qreal y = p.y();

        SetPosition(x, y);
    }
    
    void LocalEntity::MoveLeft()
    {
        QPointF p = this->pos();
        qreal x = p.x() - move_unit;
        qreal y = p.y();

        SetPosition(x, y);
    }
    
    void LocalEntity::MoveUp()
    {
        QPointF p = this->pos();
        qreal x = p.x();
        qreal y = p.y() - move_unit;

        SetPosition(x, y);
    }
    
    void LocalEntity::MoveDown()
    {
        QPointF p = this->pos();
        qreal x = p.x();
        qreal y = p.y() + move_unit;

        SetPosition(x, y);
    }

    void LocalEntity::SetRandomColor()
    {
        color_.setRgb(qrand() % 256, qrand() % 256, qrand() % 256);
        match_->FlagForUpdate(this, Segment_Color);
        update();
    }

	void LocalEntity::SendRPCToReplicas()
	{
		OutputStream stream;
		stream << (int)RPC_To_Replicas;
		match_->SendCustomMessageToReplicas(this, &stream); 
	}
}