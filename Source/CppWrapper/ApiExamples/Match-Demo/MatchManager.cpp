//-----------------------------------------------------------------------
// <copyright file="MatchManager.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2013 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include <algorithm>
#include "Conversion.h"
#include "Badumna/DataTypes/Vector3.h"
#include "EntityType.h"
#include "RPCDictionary.h"
#include "MatchManager.h"

namespace ApiExamples
{
    MatchManager::MatchManager(
		std::auto_ptr<Badumna::MatchFacade> match_facade,
		Badumna::MatchmakingCriteria const &matchmaking_criteria,
		Badumna::String const &player_name,
		SimpleGraphicsScene *graphics_scene,
		QTextEdit *log)
        : local_entity_(),
        replica_objects_(),
        match_(),
		npc_(),
        graphics_scene_(graphics_scene),
		log_(log)
    {
		int max_players = 5;
		match_controller_.reset(new MatchController(log_));
		
		//match_ = match_facade->CreatePrivateMatch(
		match_ = match_facade->CreateMatch(
			match_controller_.get(),
			matchmaking_criteria,
			max_players,
			player_name,
			Badumna::CreateMatchReplicaDelegate(&MatchManager::CreateReplica, *this),
			Badumna::RemoveMatchReplicaDelegate(&MatchManager::RemoveReplica, *this),
			Badumna::CreateMatchHostedReplicaDelegate(&MatchManager::CreateHostedReplica, *this),
			Badumna::RemoveMatchHostedReplicaDelegate(&MatchManager::RemoveHostedReplica, *this));

		InitializeMatch();
    }

	MatchManager::MatchManager(
		std::auto_ptr<Badumna::MatchFacade> match_facade,
		Badumna::MatchmakingResult const &matchmaking_result,
		Badumna::String const &player_name,
		SimpleGraphicsScene *graphics_scene,
		QTextEdit *log)
        : local_entity_(),
        replica_objects_(),
        match_(),
		npc_(),
        graphics_scene_(graphics_scene),
		log_(log)
    {
		match_controller_.reset(new MatchController(log_));

		match_ = match_facade->JoinMatch(
			match_controller_.get(),
			matchmaking_result,
			player_name,
			Badumna::CreateMatchReplicaDelegate(&MatchManager::CreateReplica, *this),
			Badumna::RemoveMatchReplicaDelegate(&MatchManager::RemoveReplica, *this),
			Badumna::CreateMatchHostedReplicaDelegate(&MatchManager::CreateHostedReplica, *this),
			Badumna::RemoveMatchHostedReplicaDelegate(&MatchManager::RemoveHostedReplica, *this));

		InitializeMatch();
    }

	void MatchManager::InitializeMatch()
	{
		// Subscribe to status notifications
		match_->SetStateChangedDelegate(Badumna::MatchStateChangedDelegate(&MatchManager::OnStateChanged, *this));
		match_->SetChatMessageReceivedDelegate(Badumna::MatchChatMessageDelegate(&MatchManager::OnChatMessage, *this));
		match_->SetMemberAddedDelegate(Badumna::MatchMembershipDelegate(&MatchManager::OnMemberAdded, *this));
		match_->SetMemberRemovedDelegate(Badumna::MatchMembershipDelegate(&MatchManager::OnRemovedAdded, *this));

        // Initialize the local avatar
		local_entity_.reset(new LocalEntity(log_, match_.get()));
        local_entity_->SetPosition(30.0, 30.0);
        graphics_scene_->RegisterLocalEntity(local_entity_.get());

        // Register the local avatar with the match
        match_->RegisterEntity(local_entity_.get(), NormalEntity);
	}

	void MatchManager::Update()
	{
		if (match_->State() == Badumna::MatchStatus_Hosting)
		{
			QPointF centre(90.0, 90.0);
			double radius = 50.0;

			if (npc_.get() == NULL)
			{
				npc_.reset(new NPC(log_, match_.get()));
				match_->RegisterHostedEntity(npc_.get(), NormalEntity);
				graphics_scene_->RegisterHostedEntity(npc_.get());
				npc_->SetPosition(centre.x(), centre.y());
			}

			// Update the NPC to move in a circle
			QPointF pos = npc_->pos();
			double angle = atan2(pos.y() - centre.y(), pos.x() - centre.x());
			angle += 0.01;
			npc_->SetPosition(centre.x() + radius * cos(angle), centre.y() + radius * sin(angle));
		}
		else
		{
			if (npc_.get() != NULL)
			{
				npc_.get()->Update();
			}
		}

		for (std::vector<ReplicaObject*>::iterator i = replica_objects_.begin(); i != replica_objects_.end(); ++i)
		{
			(*i)->Update();
		}
	}

    void MatchManager::Shutdown()
    {
        if (match_.get() == NULL)
		{
			return;
		}

		match_->Leave();
		match_.reset();
    }

	void MatchManager::OnStateChanged(Badumna::Match &, Badumna::MatchStatusEventArgs const &status_args)
	{
		log_->append(QString("Match status = %1 / %2.").arg(status_args.status).arg(status_args.error));

		if (status_args.status == Badumna::MatchStatus_Connected)
		{
			graphics_scene_->OnJoinedMatch(match_.get(), match_controller_.get());
		}
		else if(status_args.status == Badumna::MatchStatus_Hosting)
		{
			graphics_scene_->OnHostMigration(match_.get(), match_controller_.get());
		}
	}

	void MatchManager::OnChatMessage(Badumna::Match &, Badumna::MatchChatEventArgs const &chat_args)
	{
		log_->append(QString("%1 %2: %3")
			.arg(chat_args.type == Badumna::ChatType_Private ? "whispers" : "says")
			.arg(BStringToQString(chat_args.sender->Name()))
			.arg(BStringToQString(chat_args.message)));
	}

	void MatchManager::OnMemberAdded(Badumna::Match &, Badumna::MemberIdentity const &member)
	{
		log_->append(QString("%1 joined.").arg(BStringToQString(member.Name())));
	}

	void MatchManager::OnRemovedAdded(Badumna::Match &, Badumna::MemberIdentity const &member)
	{
		log_->append(QString("%1 left.").arg(BStringToQString(member.Name())));
	}

	Badumna::IMatchReplica *MatchManager::CreateReplica(Badumna::Match const &, uint32_t)
	{
		ReplicaObject* replica = new ReplicaObject(log_, match_.get());
		replica_objects_.push_back(replica);
		graphics_scene_->RegisterReplicaEntity(replica);
		return replica;
	}

    void MatchManager::RemoveReplica(Badumna::Match const &, Badumna::IMatchReplica *replica)
	{
		std::vector<ReplicaObject*>::iterator iter;
        iter = std::find(
            replica_objects_.begin(), 
            replica_objects_.end(), 
            replica);
        if(iter != replica_objects_.end())
        {
            ReplicaObject* to_remove = *iter;
            graphics_scene_->RemoveReplicaEntity(to_remove);
            replica_objects_.erase(iter);
            delete to_remove;
        }
	}
	
	Badumna::IMatchHosted *MatchManager::CreateHostedReplica(Badumna::Match const &, uint32_t)
	{
		npc_.reset(new NPC(log_, match_.get()));
		graphics_scene_->RegisterHostedEntity(npc_.get());
		return npc_.get();
	}

    void MatchManager::RemoveHostedReplica(Badumna::Match const &, Badumna::IMatchHosted *)
	{
	}

	void MatchManager::SendChatMessage(Badumna::String const &message)
	{
		match_->Chat(message);
	}
}
