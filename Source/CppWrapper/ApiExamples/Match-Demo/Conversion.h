//-----------------------------------------------------------------------
// <copyright file="Conversion.h" company="National ICT Australia Ltd">
//     Copyright (c) 2013 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef CONVERSION_H
#define CONVERSION_H

#include <QtGui>
#include "Badumna/DataTypes/String.h"

QString BStringToQString(Badumna::String const &str);
Badumna::String QStringToBString(QString const &str);

#endif // CONVERSION_H