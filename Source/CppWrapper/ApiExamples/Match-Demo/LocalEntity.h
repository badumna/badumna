//-----------------------------------------------------------------------
// <copyright file="LocalEntity.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef LOCAL_ENTITY_H
#define LOCAL_ENTITY_H

#include "Badumna/Match/Match.h"
#include "Badumna/Match/IMatchOriginal.h"

#include "StateSegment.h"
#include "LocalAvatar.h"

namespace ApiExamples
{
    class LocalEntity : public LocalAvatar, public Badumna::IMatchOriginal
    {
    public:
        LocalEntity(QTextEdit *log, Badumna::Match *match);

        void MoveRight();
        void MoveLeft();
        void MoveUp();
        void MoveDown();

        void SetRandomColor();

		void SetPosition(qreal x, qreal y);

        void HandleEvent(Badumna::InputStream *stream);
        void Serialize(Badumna::BooleanArray const &required_parts, Badumna::OutputStream *stream);

		void SendRPCToReplicas();

    protected:
         void advance(int step);

    private:
		QTextEdit *log_;
        Badumna::Match *match_;

        static const int move_unit = 2;

        // disallow copy and assign
        LocalEntity(LocalEntity const &other);
        LocalEntity& operator=(LocalEntity const &rhs);
    };
}

#endif // LOCAL_ENTITY_H