//-----------------------------------------------------------------------
// <copyright file="MatchManager.h" company="National ICT Australia Ltd">
//     Copyright (c) 2013 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef MATCH_MANAGER_H
#define MATCH_MANAGER_H

#include <memory>
#include <vector>
#include "SimpleGraphicsScene.h"
#include "LocalEntity.h"
#include "ReplicaObject.h"
#include "NPC.h"
#include "MatchController.h"
#include "Badumna/Match/Match.h"
#include "Badumna/Match/MatchFacade.h"

namespace ApiExamples
{
    class MainWindow;

    class MatchManager
    {
    public:
		MatchManager(std::auto_ptr<Badumna::MatchFacade> match_facade, Badumna::MatchmakingCriteria const &matchmaking_criteria, Badumna::String const &player_name, SimpleGraphicsScene *graphics_scene, QTextEdit *log);
		MatchManager(std::auto_ptr<Badumna::MatchFacade> match_facade, Badumna::MatchmakingResult const &matchmaking_result, Badumna::String const &player_name, SimpleGraphicsScene *graphics_scene, QTextEdit *log);
		void Update();
        void Shutdown();

		Badumna::IMatchReplica *CreateReplica(Badumna::Match const &match, uint32_t entity_type);
		void RemoveReplica(Badumna::Match const &match, Badumna::IMatchReplica *replica);
		Badumna::IMatchHosted *CreateHostedReplica(Badumna::Match const &match, uint32_t entity_type);
		void RemoveHostedReplica(Badumna::Match const &match, Badumna::IMatchHosted *hosted);

		void SendChatMessage(Badumna::String const &message);
    
    private:
        std::auto_ptr<LocalEntity> local_entity_;
        std::vector<ReplicaObject*> replica_objects_;
		std::auto_ptr<Badumna::Match> match_;
		std::auto_ptr<MatchController> match_controller_;
		std::auto_ptr<NPC> npc_;

        SimpleGraphicsScene *graphics_scene_;
		QTextEdit *log_;

		void InitializeMatch();
		void OnStateChanged(Badumna::Match &match, Badumna::MatchStatusEventArgs const &status_args);
		void OnChatMessage(Badumna::Match &match, Badumna::MatchChatEventArgs const &chat_args);
		void OnMemberAdded(Badumna::Match &match, Badumna::MemberIdentity const &member);
		void OnRemovedAdded(Badumna::Match &match, Badumna::MemberIdentity const &member);

        // disallow copy and assign
        MatchManager(MatchManager const &other);
        MatchManager &operator=(MatchManager const &rhs);
    };
}

#endif // MATCH_MANAGER_H