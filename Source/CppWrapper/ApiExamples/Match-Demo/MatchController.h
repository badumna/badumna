//-----------------------------------------------------------------------
// <copyright file="MatchController.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef MATCH_CONTROLLER_H
#define MATCH_CONTROLLER_H

#include <qtextedit.h>
#include "Badumna/Match/IMatchHosted.h"
#include "Badumna/Match/Match.h"
#include "StateSegment.h"

namespace ApiExamples
{
	class MatchController : public Badumna::IMatchHosted
	{
	public:
		MatchController(QTextEdit *log);

		void HandleEvent(Badumna::InputStream *stream);
		void Serialize(Badumna::BooleanArray const &required_parts, Badumna::OutputStream *stream);
		void Deserialize(
			Badumna::BooleanArray const &included_part,
			Badumna::InputStream *stream,
			int estimated_milliseconds_since_departure);
		
		void AddSequenceNumber(Badumna::Match *match);

	private:
		QTextEdit *log_;
		int sequence_num_;

		// disallow copy and assign
		MatchController(MatchController const &other);
		MatchController &operator=(MatchController const &rhs);
	};
}

#endif // MATCH_CONTROLLER_H