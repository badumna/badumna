//-----------------------------------------------------------------------
// <copyright file="ReplicaObject.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef REPLICA_OBJECT_H
#define REPLICA_OBJECT_H

#include <QtGui>
#include "Badumna/Match/IMatchReplica.h"
#include "Badumna/Utils/BasicTypes.h"

#include "PositionSmoother.h"
#include "Avatar.h"

namespace ApiExamples
{
    class ReplicaObject : public Badumna::IMatchReplica,  public Avatar
    {
    public:
		ReplicaObject(QTextEdit *log, Badumna::Match *match);

		void HandleEvent(Badumna::InputStream *stream);
        void Deserialize(
            Badumna::BooleanArray const &included_part, 
            Badumna::InputStream *stream, 
            int estimated_milliseconds_since_departure);
		
		void Update();
    private:
		QTextEdit *log_;
		Badumna::Match *match_;
		PositionSmoother position_smoother_;

		void SendRPCToOriginal();

        // disallow copy and assign
        ReplicaObject(ReplicaObject const &other);
        ReplicaObject &operator= (ReplicaObject const &rhs);
    };
}

#endif // BADUMNA_EXAMPLE_REPLICAOBJECT_H