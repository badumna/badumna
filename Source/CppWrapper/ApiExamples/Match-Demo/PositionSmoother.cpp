//-----------------------------------------------------------------------------
// <copyright file="PositionSmoother.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------------

#include "PositionSmoother.h"

namespace ApiExamples
{
	PositionSmoother::PositionSmoother(int interpolation_interval)
		: positions_(),
		interpolation_interval_(interpolation_interval),
		past_position_()
	{
	}

	void PositionSmoother::Update(const QPointF &pos)
	{
		clock_t now = clock();

		AdvanceQueue();
		if (positions_.size() == 0)
		{
			past_position_ = Position(pos, now);
		}

		Position position;
		position.time = now + (float)interpolation_interval_/1000 * CLOCKS_PER_SEC;
		position.value = pos;

		positions_.push_back(position);
	}

	QPointF PositionSmoother::GetCurrentValue()
	{
		AdvanceQueue();
		if (positions_.size() > 0)
		{
			return Interpolate(past_position_, positions_.front(), clock());
		}

		return past_position_.value;
	}

	QPointF PositionSmoother::Interpolate(Position const &start, Position const &end, clock_t current_time)
	{
		clock_t time_step = current_time - start.time;
		clock_t time_interval = end.time - start.time;
		
		float proportion = (float)time_step/time_interval;
		
		QPointF total_displacement = end.value - start.value;
		QPointF displacement = total_displacement * proportion;
		
		return start.value + displacement;
	}

	void PositionSmoother::AdvanceQueue()
	{
		clock_t now = clock();

		while (positions_.size() > 0 && positions_.front().time < now)
		{
			past_position_ = positions_.front();
			positions_.pop_front();
		}
	}
}