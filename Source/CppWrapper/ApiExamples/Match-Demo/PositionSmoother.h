//---------------------------------------------------------------------------
// <copyright file="PositionSmoother.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------

#ifndef POSITION_SMOOTHER_H
#define POSITION_SMOOTHER_H

#include <time.h>
#include <QGraphicsItem>
#include <deque>

namespace ApiExamples
{
	struct Position
	{
		QPointF value;
		clock_t time; // unit: clock click

		Position() : value(), time() {}
		Position(const QPointF &value, clock_t time) : value(value), time(time) {}
	};

	class PositionSmoother
	{
	public:
		PositionSmoother(int interpolation_interval);

		void Update(QPointF const &pos);
		QPointF GetCurrentValue();
	
	private:
		std::deque<Position> positions_;
		int interpolation_interval_; // unit: millisecond
		Position past_position_;

		QPointF Interpolate(Position const &start, Position const &end, clock_t current_time);		
		void AdvanceQueue();
	};
}

#endif // POSITION_SMOOTHER_H