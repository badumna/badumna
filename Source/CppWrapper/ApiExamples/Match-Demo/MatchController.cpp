//----------------------------------------------------------------------------
// <copyright file="MatchController.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//----------------------------------------------------------------------------

#include "Conversion.h"
#include "MatchController.h"
#include "StateSegment.h"
#include "RPCDictionary.h"

namespace ApiExamples
{
	using Badumna::InputStream;
	using Badumna::OutputStream;
	using Badumna::BooleanArray;

	using Badumna::Match;

	MatchController::MatchController(QTextEdit *log)
		: log_(log),
		sequence_num_(0)
	{
	}

	void MatchController::HandleEvent(InputStream *stream)
	{
		int rpc_type;
		(*stream) >> rpc_type;

		switch((RPCDictionary)rpc_type)
		{
			case RPC_To_Members:
				log_->append(QString("Broadcast message from host."));
				break;
			case RPC_To_Host:
				log_->append(QString("Reply: Broadcast message received."));
				break;
			case RPC_To_Replicas:
			case RPC_To_Original:
				break;			
		}
	}

	void MatchController::Serialize(BooleanArray const &required_parts, OutputStream *stream)
	{
		if (required_parts[Segment_Sequence])
		{
			(*stream) << sequence_num_;
		}
	}

	void MatchController::Deserialize(
		Badumna::BooleanArray const &included_part, 
		Badumna::InputStream *stream, 
		int)
	{
		if(included_part[Segment_Sequence])
		{
			(*stream) >> sequence_num_;
			log_->append(QString("Current sequence number: %1").arg(sequence_num_));
		}
	}

	void MatchController::AddSequenceNumber(Match *match)
	{
		sequence_num_ ++;
		match->FlagForUpdate(this, Segment_Sequence);
		log_->append(QString("Increment sequence number: %1").arg(sequence_num_));
	}
}