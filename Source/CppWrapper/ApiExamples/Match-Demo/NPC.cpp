//-----------------------------------------------------------------------
// <copyright file="NPC.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include "Conversion.h"
#include "StateSegment.h"
#include "RPCDictionary.h"
#include "NPC.h"

namespace ApiExamples
{
    using Badumna::InputStream;
    using Badumna::OutputStream;
    using Badumna::BooleanArray;

    NPC::NPC(QTextEdit *log, Badumna::Match *match)
        : Avatar(),
		log_(log),
		match_(match),
		position_smoother_(200)
    {
    }

    void NPC::HandleEvent(InputStream *stream)
    {
		int rpc_type;
		(*stream) >> rpc_type;
		
		switch((RPCDictionary)rpc_type)
		{
			case RPC_To_Replicas:
				log_->append(QString("RPC message send by hosted entity original."));
				break;
			case RPC_To_Original:
				log_->append(QString("RPC message send by hosted entity replica."));
				break;
			case RPC_To_Members:
			case RPC_To_Host:			
				break;
		}		
    }

    void NPC::SetPosition(qreal x, qreal y)
    {
        setPos(x, y);

        // Notify Badumna that the position property has been updated, it needs to be repliccated to replicas
		match_->FlagForUpdate(this, Segment_Position);
	}

    void NPC::Serialize(BooleanArray const &required_parts, OutputStream *stream)
    {
		if (required_parts[Segment_Position])
		{
			QPointF position = pos();
			(*stream) << position.x();
			(*stream) << position.y();
		}

        if(required_parts[Segment_Color])
        {
            (*stream) << color_.red();
            (*stream) << color_.green();
            (*stream) << color_.blue();
        }
    }

    void NPC::Deserialize(
        BooleanArray const &included_parts, 
        InputStream *stream, 
        int /*estimated_milliseconds_since_departure*/)
    {
		if (included_parts[Segment_Position])
		{
			qreal x, y;
			(*stream) >> x;
			(*stream) >> y;
			position_smoother_.Update(QPointF(x,y));
		}

        if(included_parts[Segment_Color])
        {
            int r, g, b;
            (*stream) >> r;
            (*stream) >> g;
            (*stream) >> b;

            color_.setRgb(r, g, b);
            update();
        }
    }

	void NPC::Update()
	{
		setPos(position_smoother_.GetCurrentValue());
	}
}