//-----------------------------------------------------------------------
// <copyright file="RPCDictionary.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef RPC_DICTIONARY_H
#define RPC_DICTIONARY_H

namespace ApiExamples
{
    enum RPCDictionary
    {
		RPC_To_Replicas = 0,
        RPC_To_Original,
		RPC_To_Members,
		RPC_To_Host
    };
}

#endif // RPC_DICTIONARY_H