//-----------------------------------------------------------------------
// <copyright file="ReplicaObject.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include <QPainter>

#include "Conversion.h"
#include "Badumna/Replication/SpatialEntityStateSegment.h"
#include "LocalEntity.h"
#include "ReplicaObject.h"
#include "RPCDictionary.h"

namespace ApiExamples
{
    using Badumna::InputStream;
    using Badumna::BooleanArray;
	using Badumna::OutputStream;

	ReplicaObject::ReplicaObject(QTextEdit *log, Badumna::Match *match) 
        : Avatar(),
		log_(log),
		match_(match),
		position_smoother_(200)
    {
        setZValue(5);
    }

    void ReplicaObject::HandleEvent(InputStream *stream)
    {	
		int rpc_type;
		(*stream) >> rpc_type;

		if ((RPCDictionary)rpc_type == RPC_To_Replicas)
		{
			log_->append(QString("RPC from original"));
			SendRPCToOriginal();
		}
    }

    void ReplicaObject::Deserialize(
        BooleanArray const &included_parts, 
        InputStream *stream, 
        int /*estimated_milliseconds_since_departure*/)
    {
		if (included_parts[Segment_Position])
		{
			qreal x, y;
			(*stream) >> x;
			(*stream) >> y;
			position_smoother_.Update(QPointF(x, y));
		}

        if(included_parts[Segment_Color])
        {
            int r, g, b;
            (*stream) >> r;
            (*stream) >> g;
            (*stream) >> b;

            color_.setRgb(r, g, b);
            update();
        }
    }

	void ReplicaObject::SendRPCToOriginal()
	{
		OutputStream stream;
		stream << (int)RPC_To_Original;
		match_->SendCustomMessageToOriginal(this, &stream);
	}

	void ReplicaObject::Update()
	{
		setPos(position_smoother_.GetCurrentValue());
	}
}