//-----------------------------------------------------------------------
// <copyright file="SimpleGraphicsScene.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include <QGraphicsItem>
#include "SimpleGraphicsScene.h"
#include "RPCDictionary.h"

namespace ApiExamples
{
	using Badumna::Match;
    using Badumna::OutputStream;

	SimpleGraphicsScene::SimpleGraphicsScene() 
		: QGraphicsScene(),
        rpc_button_(NULL),
        hosted_rpc_button_(NULL),
        broadcast_button_(NULL),
        reply_broadcast_button_(NULL),
        increment_seq_button_(NULL),
        local_avatar_(NULL),
        npc_(NULL),
        match_(NULL),
        match_controller_(NULL)
    {
		InitializeGui();
	}

    void SimpleGraphicsScene::RegisterLocalEntity(LocalAvatar *local)
    {
		rpc_button_->setDisabled(false);

        local_avatar_ = local;
        addItem(local);
    }

    void SimpleGraphicsScene::RemoveLocalEntity(LocalAvatar *local)
    {
		rpc_button_->setDisabled(true);
        removeItem(local);
    }
    
    void SimpleGraphicsScene::RegisterReplicaEntity(Avatar *replica)
    {
        addItem(replica);
    }

    void SimpleGraphicsScene::RemoveReplicaEntity(Avatar *replica)
    {
        removeItem(replica);
    }

	void SimpleGraphicsScene::RegisterHostedEntity(NPC *npc)
    {
		hosted_rpc_button_->setDisabled(false);

		npc_ = npc;
        addItem(npc);
    }

    void SimpleGraphicsScene::RemoveHostedEntity(NPC *npc)
    {
		hosted_rpc_button_->setDisabled(true);
        removeItem(npc);
    }

	void SimpleGraphicsScene::OnJoinedMatch(Match *match, MatchController *match_controller)
	{
		match_ = match;
		match_controller_ = match_controller;
		
		broadcast_button_->setDisabled(true);			
		reply_broadcast_button_->setDisabled(false);
		increment_seq_button_->setDisabled(true);
	}

	void SimpleGraphicsScene::OnHostMigration(Match *match, MatchController *match_controller)
	{
		match_ = match;
		match_controller_ = match_controller;

		broadcast_button_->setDisabled(false);			
		reply_broadcast_button_->setDisabled(true);			
		increment_seq_button_->setDisabled(false);
	}

    void SimpleGraphicsScene::keyPressEvent(QKeyEvent *e)
    {
        switch(e->key())
        {
            case Qt::Key_Up:
                local_avatar_->MoveUp();
                break;
            case Qt::Key_Left:
                local_avatar_->MoveLeft();
                break;
            case Qt::Key_Right:
                local_avatar_->MoveRight();
                break;
            case Qt::Key_Down:
                local_avatar_->MoveDown();
                break;
            case Qt::Key_C:
                local_avatar_->SetRandomColor();
                break;
        }
    }

	void SimpleGraphicsScene::InitializeGui()
	{
		// Add RPC button
		rpc_button_ = new QPushButton("Entity RPC");
		rpc_button_->setGeometry(QRect(5, 346, 93, 22));
		rpc_button_->setDisabled(true);
		QObject::connect(rpc_button_, SIGNAL(clicked()),this, SLOT(OnRPCButtonClicked()));
		this->addWidget(rpc_button_);

		// Add Hosted RPC button
		hosted_rpc_button_ = new QPushButton("Hosted entity RPC");
		hosted_rpc_button_->setGeometry(QRect(103, 346, 157, 22));
		hosted_rpc_button_->setDisabled(true);
		QObject::connect(hosted_rpc_button_, SIGNAL(clicked()),this, SLOT(OnHostedRPCButtonClicked()));
		this->addWidget(hosted_rpc_button_);

		// Add broadcast button
		broadcast_button_ = new QPushButton("Broadcast");
		broadcast_button_->setGeometry(QRect(5, 373, 74, 22));
		broadcast_button_->setDisabled(true);		
		QObject::connect(broadcast_button_, SIGNAL(clicked()),this, SLOT(OnBroadcastButtonClicked()));		
		this->addWidget(broadcast_button_);

		// Add reply broadcast button
		reply_broadcast_button_ = new QPushButton("Reply Broadcast");
		reply_broadcast_button_->setGeometry(QRect(84, 373, 140, 22));
		reply_broadcast_button_->setDisabled(true);		
		QObject::connect(reply_broadcast_button_, SIGNAL(clicked()),this, SLOT(OnReplyBroadcastButtonClicked()));		
		this->addWidget(reply_broadcast_button_);

		// Add increment sequence button
		increment_seq_button_ = new QPushButton("Increment seq");
		increment_seq_button_->setGeometry(QRect(229, 373, 121, 22));
		increment_seq_button_->setDisabled(true);		
		QObject::connect(increment_seq_button_, SIGNAL(clicked()),this, SLOT(OnIncrementSequenceButtonClicked()));		
		this->addWidget(increment_seq_button_);
	}

	void SimpleGraphicsScene::OnRPCButtonClicked()
	{
		if (local_avatar_ == NULL)
		{
			return;
		}

		dynamic_cast<LocalEntity*>(local_avatar_)->SendRPCToReplicas();
	}

	void SimpleGraphicsScene::OnHostedRPCButtonClicked()
	{
		if (match_ == NULL)
		{
			return;
		}

		OutputStream stream;
		if (match_->State() == Badumna::MatchStatus_Hosting)
		{
			stream << (int)RPC_To_Replicas;
			match_->SendCustomMessageToReplicas(npc_, &stream);
			
		}
		else
		{
			stream << (int)RPC_To_Original;
			match_->SendCustomMessageToOriginal(npc_, &stream);
		}
	}

	void SimpleGraphicsScene::OnBroadcastButtonClicked()
	{
		if (match_controller_ == NULL || match_ == NULL)
		{
			return;
		}

		// Send RPC message from match controller to all members
		OutputStream stream;
		stream << (int)RPC_To_Members;
		match_->SendCustomMessageToMembers(&stream);
	}

	void SimpleGraphicsScene::OnReplyBroadcastButtonClicked()
	{
		if (match_controller_ == NULL || match_ == NULL)
		{
			return;
		}

		// Send RPC message to host
		OutputStream stream;
		stream << (int)RPC_To_Host;
		match_->SendCustomMessageToHost(&stream);
	}

	void SimpleGraphicsScene::OnIncrementSequenceButtonClicked()
	{
		match_controller_->AddSequenceNumber(match_);
	}
}