//-----------------------------------------------------------------------
// <copyright file="NPC.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef NPC_H
#define NPC_H

#include <QGraphicsScene>

#include "Avatar.h"
#include "PositionSmoother.h"

#include "Badumna/Match/Match.h"
#include "Badumna/Match/IMatchHosted.h"

namespace ApiExamples
{
	class NPC : public Avatar, public Badumna::IMatchHosted
    {
    public:
        NPC(QTextEdit *log, Badumna::Match *match);

		void SetPosition(qreal x, qreal y);

        void HandleEvent(Badumna::InputStream *stream);
        void Serialize(Badumna::BooleanArray const &required_parts, Badumna::OutputStream *stream);
        void Deserialize(
            Badumna::BooleanArray const &included_part, 
            Badumna::InputStream *stream, 
            int estimated_milliseconds_since_departure);
		
		void Update();
    private:
		QTextEdit *log_;
        Badumna::Match *match_;
		PositionSmoother position_smoother_;

        // disallow copy and assign
        NPC(NPC const &other);
        NPC &operator=(NPC const &rhs);
    };
}

#endif // NPC_H