//-----------------------------------------------------------------------
// <copyright file="SimpleGraphicsScene.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef SIMPLE_GRAPHICS_SCENE_H
#define SIMPLE_GRAPHICS_SCENE_H

#include <QtGui>

#include "LocalAvatar.h"
#include "LocalEntity.h"
#include "NPC.h"

#include "Badumna/Match/Match.h"
#include "MatchController.h"

namespace ApiExamples
{
    class SimpleGraphicsScene : public QGraphicsScene
    {
		Q_OBJECT
    public:
        SimpleGraphicsScene();

        void RegisterLocalEntity(LocalAvatar *local);
        void RemoveLocalEntity(LocalAvatar *local);

        void RegisterReplicaEntity(Avatar *replica);
        void RemoveReplicaEntity(Avatar *replica);

		void RegisterHostedEntity(NPC *npc);
		void RemoveHostedEntity(NPC *npc);

		void OnJoinedMatch(Badumna::Match *match, MatchController *match_controller);
		void OnHostMigration(Badumna::Match *match, MatchController *match_controller);
	public slots:
		void OnRPCButtonClicked();
		void OnHostedRPCButtonClicked();
		void OnBroadcastButtonClicked();
		void OnReplyBroadcastButtonClicked();
		void OnIncrementSequenceButtonClicked();

    protected:
        void keyPressEvent(QKeyEvent *e);

		QPushButton *rpc_button_;
		QPushButton *hosted_rpc_button_;
		QPushButton *broadcast_button_;
		QPushButton *reply_broadcast_button_;
		QPushButton *increment_seq_button_;
        
    private:
		void InitializeGui();

        LocalAvatar *local_avatar_;
		NPC *npc_;
		Badumna::Match *match_;
		MatchController *match_controller_;

        // disallow copy and assign
        SimpleGraphicsScene(SimpleGraphicsScene const &other);
        SimpleGraphicsScene &operator=(SimpleGraphicsScene const &rhs);
    };
}

#endif // SIMPLE_GRAPHICS_SCENE_H