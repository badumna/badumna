
MATCHDEMO=Match-Demo

ifeq ($(OS), Windows_NT)
MATCHDEMO:=$(MATCHDEMO).exe
endif

MATCHDEMO_PATH=Match-Demo
MATCHDEMO_OBJS=$(MATCHDEMO_PATH)/Avatar.o \
	      $(MATCHDEMO_PATH)/LocalEntity.o \
	      $(MATCHDEMO_PATH)/Conversion.o \
	      $(MATCHDEMO_PATH)/MatchController.o \
	      $(MATCHDEMO_PATH)/PositionSmoother.o \
	      $(MATCHDEMO_PATH)/MainWindow.o \
	      $(MATCHDEMO_PATH)/MatchManager.o \
	      $(MATCHDEMO_PATH)/NPC.o \
	      $(MATCHDEMO_PATH)/Program.o \
	      $(MATCHDEMO_PATH)/ReplicaObject.o \
	      $(MATCHDEMO_PATH)/SimpleGraphicsScene.o \
	      $(MATCHDEMO_PATH)/MainWindow.moc.o \
	      $(MATCHDEMO_PATH)/SimpleGraphicsScene.moc.o

$(MATCHDEMO): $(MATCHDEMO_OBJS) | check_libmono_path
	@echo [Linking]: $@
	$(VERBOSE_MODE)$(CXX) $(LDFLAGS) -o $@ $(MATCHDEMO_OBJS) $(LIBS) $(NATIVE_WINDOWS_APP_FLAG)

DEMOS+=$(MATCHDEMO)
ALL_OBJS+=$(MATCHDEMO_OBJS)
INSTALL_FILES+=$(MATCHDEMO)
