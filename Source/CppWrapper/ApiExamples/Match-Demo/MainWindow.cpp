//-----------------------------------------------------------------------
// <copyright file="MainWindow.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include <QtGui>
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/Match/MatchmakingCriteria.h"
#include "Badumna/Match/Match.h"
#include "Badumna/Match/MatchDelegate.h"

#include "MainWindow.h"
#include "Conversion.h"
#include "LocalEntity.h"
#include "SimpleGraphicsScene.h"

namespace ApiExamples
{
    using std::auto_ptr;
    using Badumna::NetworkFacade;
    using Badumna::Options;
	using Badumna::MatchFacade;
    using ApiExamples::LocalEntity;
    
    MainWindow::MainWindow()
        : facade_(NULL),
        scene_(new SimpleGraphicsScene),
		last_find_results_(),
		match_manager_(NULL),
        view_(new QGraphicsView(scene_.get())),
        scene_timer_(new QTimer(scene_.get())),
        regular_timer_(new QTimer(this)),
        main_layout_(NULL),
		user_name_(NULL),
		create_match_button_(NULL),
		new_match_name_(NULL),
		find_matches_button_(NULL),
		join_match_button_(NULL),
		match_list_(NULL),
		log_(NULL)
	{
    }

    MainWindow::~MainWindow()
    {
		if (match_manager_.get() != NULL)
		{
			match_manager_->Shutdown();
			match_manager_.reset();
		}

        // Shutdown the facade object
        facade_->Shutdown();
    }

    void MainWindow::Initialize(QString title)
    {
        InitializeGui(title);
		InitializeBadumna();
    }

    void MainWindow::InitializeBadumna()
    {
#ifdef CLOUD
        // NOTE: Hardcode your app id here to save entering it every time.
        Badumna::String app_id("");
        if (app_id.Length() == 0)
        {
            bool ok;
            QString q_app_id = QInputDialog::getText(
                this,
                "Enter app id",
                "Create your app at cloud.badumna.com then enter your app id here:",
                QLineEdit::Normal,
                QString(),
                &ok);

            if (!ok)
            {
                exit(1);
            }

            app_id = QStringToBString(q_app_id);
        }

        // Create the facade
        facade_.reset(NetworkFacade::Create(app_id));
#else
        // Set the seed peer / matchmaking server address
        // NOTE: Hardcode your server here to save entering it every time.
        Badumna::String matchmaking_server("");
        if (matchmaking_server.Length() == 0)
        {
            bool ok;
            QString q_matchmaking_server = QInputDialog::getText(
                this,
                "Seed / matchmaking server address",
                "Enter the address of your seed peer / matchmaking server here (localhost cannot be used):",
                QLineEdit::Normal,
                "address:port",
                &ok);

            if (!ok)
            {
                exit(1);
            }

            matchmaking_server = QStringToBString(q_matchmaking_server);
        }

        // Use the Options class to configure the badumna peer. 
        Options options;
        options.GetConnectivityModule().SetStartPortRange(21300);
        options.GetConnectivityModule().SetEndPortRange(21399);
        options.GetConnectivityModule().SetMaxPortsToTry(3);
        options.GetConnectivityModule().EnableBroadcast();
        options.GetConnectivityModule().SetBroadcastPort(21250);

        options.GetConnectivityModule().AddSeedPeer(matchmaking_server);

        // Set the application name
        options.GetConnectivityModule().SetApplicationName(L"match-demo");


        options.GetMatchmakingModule().SetServerAddress(matchmaking_server);

        // Create the facade
        facade_.reset(NetworkFacade::Create(options));
#endif // CLOUD

        // Log in to the network
        bool succeed = facade_->Login();
        if(!succeed)
        {
            QMessageBox message_box;
            message_box.setWindowTitle("Error");
            message_box.setText("Failed to initialize badumna.");
            message_box.exec();
            exit(1);
        }
    }

	void MainWindow::ProcessMatchmakingResults(Badumna::MatchmakingQueryResult const &result)
	{
		join_match_button_->setDisabled(true);  // We enable it below in the case that we got a result.

		QString status;
		if (result.GetError() == Badumna::MatchError_None)
		{
			match_list_->clear();
			last_find_results_ = result.GetResults();
			for (uint i = 0; i < last_find_results_.size(); ++i)
			{
				QListWidgetItem *item = new QListWidgetItem(BStringToQString(last_find_results_[i]->GetCriteria().match_name), match_list_);
				item->setData(Qt::UserRole, QVariant(i));
			}

			if (last_find_results_.size() == 0)
			{
				status = "No matches found.";
			}
			else
			{
				status = QString("Found %1 match%2.").arg(last_find_results_.size()).arg(last_find_results_.size() == 1 ? "" : "es");
				join_match_button_->setEnabled(true);
			}
		}
		else if (result.GetError() == Badumna::MatchError_CannotConnectToMatchmaker)
		{
			status = "Could not connect to matchmaking server.";
		}
		else
		{
			status = "Matchmaking query failed.";
		}

		log_->append(status);
		find_matches_button_->setEnabled(true);
	}

	void MainWindow::OnCreateMatchClicked()
	{
		QString match_name = new_match_name_->text();
		if (match_name.size() == 0)
		{
			return;
		}

		Badumna::MatchmakingCriteria criteria;
		criteria.match_name = QStringToBString(match_name);
		EnterMatch(new MatchManager(facade_->GetMatchFacade(), criteria, QStringToBString(user_name_->text()), scene_.get(), log_), match_name);
	}

	void MainWindow::OnFindMatchesClicked()
	{
		log_->append("Finding matches...");
		find_matches_button_->setDisabled(true);

		Badumna::MatchmakingCriteria criteria;
		criteria.match_name = QStringToBString(new_match_name_->text());
		facade_->GetMatchFacade()->FindMatches(
			criteria,
			Badumna::MatchmakingResultDelegate(&MainWindow::ProcessMatchmakingResults, *this));
	}

	void MainWindow::OnJoinMatchClicked()
	{
		if (match_list_->selectedItems().isEmpty())
		{
			return;
		}

		uint index = match_list_->selectedItems().first()->data(Qt::UserRole).toUInt();
		Badumna::linked_ptr<Badumna::MatchmakingResult> result = last_find_results_[index];
		QString match_name = BStringToQString(result->GetCriteria().match_name);
		EnterMatch(new MatchManager(facade_->GetMatchFacade(), *result, QStringToBString(user_name_->text()), scene_.get(), log_), match_name);
	}

	void MainWindow::OnSendChatClicked()
	{
		match_manager_->SendChatMessage(QStringToBString(chat_text_->text()));
		log_->append(QString("%1 says: %2")
			.arg(user_name_->text())
			.arg(chat_text_->text()));

		chat_text_->setText("");
	}

	void MainWindow::EnterMatch(MatchManager *manager, QString match_name)
	{
		match_manager_.reset(manager);
		create_match_button_->setDisabled(true);
		join_match_button_->setDisabled(true);
		find_matches_button_->setDisabled(true);
		log_->append(QString("In match '%1'.").arg(match_name));
	}

    void MainWindow::InitializeGui(QString window_title)
    {
		setWindowTitle(window_title);

        main_layout_ = new QGridLayout;
        setLayout(main_layout_);

		// Match joining
		QVBoxLayout *join_layout = new QVBoxLayout;
		
		join_layout->addWidget(new QLabel("Username"));
		user_name_ = new QLineEdit(this);
		join_layout->addWidget(user_name_);
		
		join_layout->addWidget(new QLabel("Match name"));
		new_match_name_ = new QLineEdit(this);
        create_match_button_ = new QPushButton("Create Match", this);
		QObject::connect(create_match_button_, SIGNAL(clicked()), this, SLOT(OnCreateMatchClicked()));
		join_layout->addWidget(new_match_name_);
		join_layout->addWidget(create_match_button_);

		find_matches_button_ = new QPushButton("Find Matches", this);
		QObject::connect(find_matches_button_, SIGNAL(clicked()), this, SLOT(OnFindMatchesClicked()));
		match_list_ = new QListWidget(this);
		join_match_button_ = new QPushButton("Join Match", this);
		QObject::connect(join_match_button_, SIGNAL(clicked()), this, SLOT(OnJoinMatchClicked()));
		join_match_button_->setDisabled(true);
		join_layout->addWidget(find_matches_button_);
		join_layout->addWidget(match_list_);
		join_layout->addWidget(join_match_button_);
		main_layout_->addLayout(join_layout, 0, 0);

		// Scene
		scene_->setSceneRect(0, 0, 400, 400);
        scene_->setItemIndexMethod(QGraphicsScene::NoIndex);

        view_->setCacheMode(QGraphicsView::CacheBackground);
        view_->setRenderHint(QPainter::Antialiasing);
        view_->setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);
        view_->resize(400, 400);
        view_->setFixedWidth(400);
        view_->setFixedHeight(400);
        view_->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        view_->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        main_layout_->addWidget(view_, 0, 1);
		
		// Chat
		QVBoxLayout *chat_layout = new QVBoxLayout;
		chat_layout->addWidget(new QLabel("Chat:"));

		QHBoxLayout *send_chat_layout = new QHBoxLayout;
		chat_text_ = new QLineEdit(this);
		chat_send_button_ = new QPushButton("Send", this);
		QObject::connect(chat_send_button_, SIGNAL(clicked()), this, SLOT(OnSendChatClicked()));

		send_chat_layout->addWidget(chat_text_);
		send_chat_layout->addWidget(chat_send_button_);
		chat_layout->addLayout(send_chat_layout);
		
		main_layout_->addLayout(chat_layout, 1, 0, 1, 2);

		// Log
		QVBoxLayout *log_layout = new QVBoxLayout;
		log_layout->addWidget(new QLabel("Log:"));

		log_ = new QTextEdit(this);
		log_->setReadOnly(true);
		log_layout->addWidget(log_);
		
		main_layout_->addLayout(log_layout, 2, 0, 1, 2);

		// Timers
        QObject::connect(scene_timer_, SIGNAL(timeout()), scene_.get(), SLOT(advance()));
        scene_timer_->start(1000 / 33);

        QObject::connect(regular_timer_, SIGNAL(timeout()), this, SLOT(ProcessRegularState()));
        regular_timer_->start(1000 / 60);
    }

    void MainWindow::ProcessRegularState()
    {
        if(facade_.get() != NULL && facade_->IsLoggedIn())
        {
			if (match_manager_.get() != NULL)
			{
				match_manager_->Update();
			}

            // ProcessNetworkState should be for every frame
            facade_->ProcessNetworkState();
        }
    }
}