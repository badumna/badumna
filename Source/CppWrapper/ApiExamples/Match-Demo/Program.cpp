//-----------------------------------------------------------------------
// <copyright file="Program.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include <QtGui>
#include <QDialog>

#include "Badumna/Core/RuntimeInitializer.h"
#include "MainWindow.h"
#include "Conversion.h"

using ApiExamples::MainWindow;

void OnException(Badumna::String const &stack_trace, Badumna::String const &exception_type_name, Badumna::String const &message)
{
    QMessageBox message_box;
    message_box.setWindowTitle("Exception");
    message_box.setText(
		QString("%1: %2").arg(BStringToQString(exception_type_name)).arg(BStringToQString(message)));
    message_box.exec();
    exit(1);
}

void OnBadumnaInternalError(int32_t error_code, Badumna::String const &message)
{
    QMessageBox message_box;
    message_box.setWindowTitle("Badumna Error");
    message_box.setText(BStringToQString(message));
    message_box.exec();
    exit(1);
}

int main(int argc, char **)
{
    QApplication app(argc, 0);
    qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));

    // initializer is a RAII object which will initialize the runtime in its constructor and shutdown the runtime
    // in its destructor. 
#ifdef WIN32
    Badumna::BadumnaRuntimeInitializer initializer(OnException, OnBadumnaInternalError);
#else
    Badumna::BadumnaRuntimeInitializer2 initializer("mono_config_file");
#endif

    MainWindow window;
    window.Initialize(QObject::tr("Match Demo"));

    return window.exec();
}
