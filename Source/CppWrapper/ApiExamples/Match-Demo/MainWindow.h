//-----------------------------------------------------------------------
// <copyright file="MainWindow.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <memory>

#include <QDialog>
#include "Badumna/DataTypes/String.h"
#include "Badumna/Core/NetworkFacade.h"
#include "MatchManager.h"
#include "SimpleGraphicsScene.h"

namespace ApiExamples
{
    class MainWindow : public QDialog
    {
        Q_OBJECT
    public:
        MainWindow();
        ~MainWindow();
        
        void Initialize(QString title);

        SimpleGraphicsScene *GetGraphicScene() const
        {
            return scene_.get();
        }

    public slots:
        void ProcessRegularState();
		void OnCreateMatchClicked();
		void OnFindMatchesClicked();
		void OnJoinMatchClicked();
		void OnSendChatClicked();

    protected:
        void InitializeBadumna();
        void InitializeGui(QString window_title);
		void EnterMatch(MatchManager *manager, QString match_name);

        std::auto_ptr<Badumna::NetworkFacade> facade_;
        std::auto_ptr<SimpleGraphicsScene> scene_;
		std::vector<Badumna::linked_ptr<Badumna::MatchmakingResult> > last_find_results_;
        std::auto_ptr<MatchManager> match_manager_;

        QGraphicsView *view_;
        QTimer *scene_timer_;
        QTimer *regular_timer_;
        QGridLayout *main_layout_;
		QLineEdit *user_name_;
		QPushButton *create_match_button_;
		QLineEdit *new_match_name_;
		QPushButton *find_matches_button_;
		QPushButton *join_match_button_;
		QListWidget *match_list_;
		QTextEdit *log_;
		QLineEdit *chat_text_;
		QPushButton *chat_send_button_;

    private:
        // disallow copy and assign
        MainWindow(MainWindow const &other);
        MainWindow &operator= (MainWindow const &rhs);
		
		void ProcessMatchmakingResults(Badumna::MatchmakingQueryResult const &result);
    };
}

#endif // MAIN_WINDOW_H