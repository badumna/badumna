@rem @echo off

set target_dir=%~s1
call %~dp0AssemblyRefs.scalify-internal.bat

FOR %%i IN (%target_dir%) DO IF EXIST %%~si\NUL GOTO COPYFILES

:NOTARGETDIR
echo The specified target dir doesn't exist, please check.
GOTO USAGE

:USAGE
echo.
echo Usage: copy_libraries.bat target_dir
echo  - target_dir is the location where you want to copy files to.
exit /B 1

:COPYFILES
call:COPYFILE %assembly_dir% Badumna.dll
IF EXIST %assembly_dir%\Dei.dll call:COPYFILE %assembly_dir% Dei.dll

echo Copying the mono library
xcopy /E /Y /I /Q %mono_dir%\Mono-2.8.1 %target_dir%\mono
if ERRORLEVEL 1 exit /B 1
call:COPYFILE %mono_dir%\Mono-2.8.1 mono-2.0.dll

echo QT_MSVC_PATH is set to %QT_MSVC_PATH%
IF EXIST %QT_MSVC_PATH%\lib\NUL GOTO QTAVAILABLE
echo Qt installation cannot be located, please manually copy QtCore4.dll and QtGui4.dll to the target directory (%target_dir%)
exit /B 1

:QTAVAILABLE
call:COPYFILE %QT_MSVC_PATH%\lib QtCore4.dll
call:COPYFILE %QT_MSVC_PATH%\lib QtGui4.dll
echo Succeeded.
exit /B 0


:: Copies a file
:COPYFILE
set base=%~1
set filename=%~2
echo Copying %filename%
set source=%base%\%filename%
IF EXIST "%source%" (
	copy "%source%" "%target_dir%\" >nul
	if ERRORLEVEL 1 call:halt
) ELSE (
	echo ERROR: No such file: %source%
	call:USAGE
	call:halt
)
GOTO:EOF


:: Sets the errorlevel and stops the batch immediatly
:halt
call :__setError
call :__die 2> nul
goto :eof

:__die
() creates an syntax error, stops immediatly
goto :eof

:__setError
exit /B 1
goto :eof
