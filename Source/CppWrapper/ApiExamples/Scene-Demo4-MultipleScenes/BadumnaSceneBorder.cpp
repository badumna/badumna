//-----------------------------------------------------------------------
// <copyright file="BadumnaSceneBorder.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include <QGraphicsScene>
#include <QPainter>
#include <QStyleOption>

#include "BadumnaSceneBorder.h"

namespace ApiExamples
{
    QRectF BadumnaSceneBorder::boundingRect() const
    {
        return QRectF(101, 0, 1, 400);
    }

    QPainterPath BadumnaSceneBorder::shape() const
    {
        QPainterPath path;
        path.addRect(101, 0, 1, 400);
        return path;
    }

    void BadumnaSceneBorder::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
    {
        painter->setBrush(color_);
        painter->drawLine(101, 0, 101, 400);
    }
}