//-----------------------------------------------------------------------
// <copyright file="MainWindow.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include <QtGui>
#include "Badumna/Core/NetworkFacade.h"

#include "MainWindow.h"
#include "LocalEntity.h"
#include "SimpleGraphicsScene.h"
#include "ReplicationManager.h"
#include "BadumnaSceneBorder.h"
#include "Conversion.h"

namespace ApiExamples
{
    using std::auto_ptr;
    using Badumna::NetworkFacade;
    using Badumna::Options;
    using ApiExamples::LocalEntity;
    
    MainWindow::MainWindow()
        : facade_(NULL),
        local_entity_(NULL),
        scene_(new SimpleGraphicsScene),
        replication_manager_(NULL),
        view_(new QGraphicsView(scene_.get())),
        scene_timer_(new QTimer(scene_.get())),
        regular_timer_(new QTimer(this)),
        main_layout_(NULL)
    {
    }

    MainWindow::~MainWindow()
    {
        replication_manager_->Shutdown();
        scene_->RemoveLocalEntity(local_entity_.get());

        facade_->Shutdown();
    }

    void MainWindow::Initialize(QString title)
    {
        InitializeGui(title);
        InitializeBadumna();
    }

    void MainWindow::InitializeBadumna()
    {
#ifdef CLOUD
        // NOTE: Hardcode your app id here to save entering it every time.
        Badumna::String app_id("");
        if (app_id.Length() == 0)
        {
            bool ok;
            QString q_app_id = QInputDialog::getText(
                this,
                "Enter app id",
                "Create your app at cloud.badumna.com then enter your app id here:",
                QLineEdit::Normal,
                QString(),
                &ok);

            if (!ok)
            {
                exit(1);
            }

            app_id = QStringToBString(q_app_id);
        }

        // Create the facade
        facade_.reset(NetworkFacade::Create(app_id));
#else
        Options options;
        options.GetConnectivityModule().SetStartPortRange(21300);
        options.GetConnectivityModule().SetEndPortRange(21399);
        options.GetConnectivityModule().SetMaxPortsToTry(3);
        options.GetConnectivityModule().EnableBroadcast();
        options.GetConnectivityModule().SetBroadcastPort(21250);
        options.GetConnectivityModule().ConfigureForLan();
        options.GetConnectivityModule().SetApplicationName(L"scene-demo4-multiplescenes");

        facade_.reset(NetworkFacade::Create(options));
#endif // CLOUD

        bool succeed = facade_->Login();
        if(!succeed)
        {
            QMessageBox message_box;
            message_box.setWindowTitle("Error");
            message_box.setText("Failed to initialize badumna.");
            message_box.exec();
            exit(1);
        }

        local_entity_.reset(new LocalEntity(facade_.get()));
        replication_manager_.reset(new ReplicationManager(this, facade_.get(), local_entity_.get()));
        scene_->RegisterReplicationManager(replication_manager_.get());
        scene_->RegisterLocalEntity(local_entity_.get());

        BadumnaSceneBorder *border = new BadumnaSceneBorder();
        border->setZValue(0);
        scene_->RegisterBadumnaSceneBorder(border);
    }

    void MainWindow::InitializeGui(QString window_title)
    {
        scene_->setSceneRect(0, 0, 400, 400);
        scene_->setItemIndexMethod(QGraphicsScene::NoIndex);

        main_layout_ = new QVBoxLayout;
        main_layout_->addWidget(view_);
        main_layout_->setSizeConstraint(QLayout::SetFixedSize);
        setLayout(main_layout_);

        view_->setCacheMode(QGraphicsView::CacheBackground);
        view_->setRenderHint(QPainter::Antialiasing);
        view_->setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);
        view_->resize(400, 400);
        view_->setFixedWidth(400);
        view_->setFixedHeight(400);
        view_->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        view_->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        view_->setWindowFlags((view_->windowFlags() | Qt::CustomizeWindowHint) & ~Qt::WindowMaximizeButtonHint); 
        view_->show();

        QObject::connect(scene_timer_, SIGNAL(timeout()), scene_.get(), SLOT(advance()));
        scene_timer_->start(1000 / 33);

        QObject::connect(regular_timer_, SIGNAL(timeout()), this, SLOT(ProcessRegularState()));
        regular_timer_->start(1000 / 60);

        setWindowTitle(window_title);
    }

    void MainWindow::ProcessRegularState()
    {
        if(facade_.get() != NULL && facade_->IsLoggedIn())
        {
            facade_->ProcessNetworkState();
        }
    }
}