//-----------------------------------------------------------------------
// <copyright file="ReplicaObject.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef REPLICA_OBJECT_H
#define REPLICA_OBJECT_H

#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/DataTypes/Vector3.h"
#include "Badumna/Replication/ISpatialReplica.h"
#include "Badumna/Utils/BasicTypes.h"

#include "Avatar.h"

namespace ApiExamples
{
    class ReplicaObject : public Badumna::ISpatialReplica,  public Avatar
    {
    public:
        ReplicaObject();

        //
        // IEntity properties
        // 
        Badumna::BadumnaId Guid() const;
        void SetGuid(Badumna::BadumnaId const &id);
        void HandleEvent(Badumna::InputStream *stream);

        //
        // ISpatialEntity properties
        //
        Badumna::Vector3 Position() const;
        void SetPosition(Badumna::Vector3 const &position);
        float Radius() const;
        void SetRadius(float val);
        float AreaOfInterestRadius() const;
        void SetAreaOfInterestRadius(float val);

        void Deserialize(
            Badumna::BooleanArray const &included_part, 
            Badumna::InputStream *stream, 
            int estimated_milliseconds_since_departure);

        void SetColor(QColor c);

    private:
        Badumna::Vector3 position_;
        Badumna::BadumnaId guid_;
        float radius_;
        float area_of_interest_radius_;

        // disallow copy and assign
        ReplicaObject(ReplicaObject const &other);
        ReplicaObject &operator= (ReplicaObject const &rhs);
    };
}

#endif // BADUMNA_EXAMPLE_REPLICAOBJECT_H