//-----------------------------------------------------------------------
// <copyright file="LocalEntity.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include <QGraphicsScene>
#include <QPainter>
#include <QStyleOption>

#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/Replication/SpatialEntityStateSegment.h"

#include "LocalEntity.h"

namespace ApiExamples
{
    using Badumna::NetworkFacade;
    using Badumna::BadumnaId;
    using Badumna::InputStream;
    using Badumna::OutputStream;
    using Badumna::Vector3;
    using Badumna::BooleanArray;

    LocalEntity::LocalEntity(NetworkFacade *facade)
        : LocalAvatar(), 
        facade_(facade), 
        guid_(BadumnaId::None()),
        position_(0.0f, 0.0f, 0.0f), 
        radius_(0.0f),
        area_of_interest_radius_(0.0f)
    {
        setZValue(10);
    }

    BadumnaId LocalEntity::Guid() const
    {
        return guid_;
    }

    void LocalEntity::SetGuid(BadumnaId const &id)
    {
        guid_ = id;
    }

    void LocalEntity::HandleEvent(InputStream *)
    {
    }

    Vector3 LocalEntity::Position() const
    {
        return position_;
    }

    void LocalEntity::SetPosition(Vector3 const &value)
    {
        position_ = value;
        setPos(value.GetX(), value.GetY());
        facade_->FlagForUpdate(*this, Badumna::StateSegment_Position);
    }

    void LocalEntity::SetPosition(qreal x, qreal y)
    {
        Vector3 v(x, y, 0.0f);
        SetPosition(v);
    }

    float LocalEntity::Radius() const
    {
        return radius_;
    }

    void LocalEntity::SetRadius(float value)
    {
        radius_ = value;
        facade_->FlagForUpdate(*this, Badumna::StateSegment_Radius);
    }

    float LocalEntity::AreaOfInterestRadius() const
    {
        return area_of_interest_radius_;
    }

    void LocalEntity::SetAreaOfInterestRadius(float value)
    {
        area_of_interest_radius_ = value;
        facade_->FlagForUpdate(*this, Badumna::StateSegment_InterestRadius);
    }

    void LocalEntity::Serialize(BooleanArray const &required_parts, OutputStream *stream)
    {
        if(required_parts[Color])
        {
            (*stream) << color_.red();
            (*stream) << color_.green();
            (*stream) << color_.blue();
        }
    }

    void LocalEntity::advance(int)
    {
    }

    void LocalEntity::MoveRight()
    {
        QPointF p = this->pos();
        qreal x = p.x() + move_unit;
        qreal y = p.y();

        SetPosition(x, y);
    }
    
    void LocalEntity::MoveLeft()
    {
        QPointF p = this->pos();
        qreal x = p.x() - move_unit;
        qreal y = p.y();

        SetPosition(x, y);
    }
    
    void LocalEntity::MoveUp()
    {
        QPointF p = this->pos();
        qreal x = p.x();
        qreal y = p.y() - move_unit;

        SetPosition(x, y);
    }
    
    void LocalEntity::MoveDown()
    {
        QPointF p = this->pos();
        qreal x = p.x();
        qreal y = p.y() + move_unit;

        SetPosition(x, y);
    }

    void LocalEntity::SetRandomColor()
    {
        color_.setRgb(qrand() % 256, qrand() % 256, qrand() % 256);
        facade_->FlagForUpdate(*this, Color);
        update();
    }
}