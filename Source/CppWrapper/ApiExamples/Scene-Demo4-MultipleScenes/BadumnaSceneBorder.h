//-----------------------------------------------------------------------
// <copyright file="BadumnaSceneBorder.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef BADUMNA_SCENE_BORDER_H
#define BADUMNA_SCENE_BORDER_H

#include <QGraphicsItem>

namespace ApiExamples
{
    class BadumnaSceneBorder : public QGraphicsItem
    {
    public:
        BadumnaSceneBorder()
            : color_(0, 0, 255)
        {
        }

        QRectF boundingRect() const;
        QPainterPath shape() const;
        void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    protected:
        QColor color_;

    private:
        BadumnaSceneBorder(BadumnaSceneBorder const &other);
        BadumnaSceneBorder &operator=(BadumnaSceneBorder const &rhs);
    };
}

#endif // BADUMNA_SCENE_BORDER_H