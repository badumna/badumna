
SCENEDEMO4=Scene-Demo4-MultipleScenes

ifeq ($(OS), Windows_NT)
SCENEDEMO4:=$(SCENEDEMO4).exe
endif

SCENEDEMO4_PATH=Scene-Demo4-MultipleScenes
SCENEDEMO4_OBJS=$(SCENEDEMO4_PATH)/Avatar.o \
	      $(SCENEDEMO4_PATH)/LocalEntity.o \
	      $(SCENEDEMO4_PATH)/MainWindow.o \
	      $(SCENEDEMO4_PATH)/Program.o \
	      $(SCENEDEMO4_PATH)/Conversion.o \
	      $(SCENEDEMO4_PATH)/ReplicaObject.o \
	      $(SCENEDEMO4_PATH)/ReplicationManager.o \
	      $(SCENEDEMO4_PATH)/SimpleGraphicsScene.o \
	      $(SCENEDEMO4_PATH)/BadumnaSceneBorder.o \
	      $(SCENEDEMO4_PATH)/MainWindow.moc.o

$(SCENEDEMO4): $(SCENEDEMO4_OBJS) | check_libmono_path
	@echo [Linking]: $@
	$(VERBOSE_MODE)$(CXX) $(LDFLAGS) -o $@ $(SCENEDEMO4_OBJS) $(LIBS) $(NATIVE_WINDOWS_APP_FLAG)

DEMOS+=$(SCENEDEMO4)
ALL_OBJS+=$(SCENEDEMO4_OBJS)
INSTALL_FILES+=$(SCENEDEMO4)
