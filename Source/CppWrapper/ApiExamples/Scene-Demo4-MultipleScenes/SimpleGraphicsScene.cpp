//-----------------------------------------------------------------------
// <copyright file="SimpleGraphicsScene.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include <QGraphicsItem>
#include "LocalEntity.h"
#include "SimpleGraphicsScene.h"
#include "ReplicationManager.h"
#include "BadumnaSceneBorder.h"

namespace ApiExamples
{
    using Badumna::Vector3;

    void SimpleGraphicsScene::RegisterBadumnaSceneBorder(BadumnaSceneBorder *border)
    {
        addItem(border);
    }

    void SimpleGraphicsScene::RegisterLocalEntity(LocalAvatar *local)
    {
        local_avatar_ = local;
        addItem(local);
    }

    void SimpleGraphicsScene::RemoveLocalEntity(LocalAvatar *local)
    {
        removeItem(local);
    }
    
    void SimpleGraphicsScene::RegisterReplicaEntity(Avatar *replica)
    {
        addItem(replica);
    }

    void SimpleGraphicsScene::RemoveReplicaEntity(Avatar *replica)
    {
        removeItem(replica);
    }

    void SimpleGraphicsScene::RegisterReplicationManager(ReplicationManager *replication_manager)
    {
        this->replication_manager_ = replication_manager;
    }

    void SimpleGraphicsScene::keyPressEvent(QKeyEvent *e)
    {
        LocalEntity *le = dynamic_cast<LocalEntity *>(local_avatar_);
        Vector3 old_position = le->Position();

        switch(e->key())
        {
            case Qt::Key_Up:
                local_avatar_->MoveUp();
                break;
            case Qt::Key_Left:
                local_avatar_->MoveLeft();
                CheckChangeScene(old_position);
                break;
            case Qt::Key_Right:
                local_avatar_->MoveRight();
                CheckChangeScene(old_position);
                break;
            case Qt::Key_Down:
                local_avatar_->MoveDown();
                break;
            case Qt::Key_C:
                local_avatar_->SetRandomColor();
                break;
        }
    }

    void SimpleGraphicsScene::CheckChangeScene(Badumna::Vector3 old_position)
    {
        LocalEntity *le = dynamic_cast<LocalEntity *>(local_avatar_);
        if(old_position.GetX() < 101.0f && le->Position().GetX() >= 101.0f)
        {
            replication_manager_->ChangeScene();
            return;
        }

        if(old_position.GetX() > 101.0f && le->Position().GetX() <= 101.0f)
        {
            replication_manager_->ChangeScene();
            return;
        }
    }
}