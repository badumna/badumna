//-----------------------------------------------------------------------
// <copyright file="ReplicaObject.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include <QPainter>

#include "Badumna/Replication/SpatialEntityStateSegment.h"
#include "LocalEntity.h"
#include "ReplicaObject.h"

namespace ApiExamples
{
    using Badumna::BadumnaId;
    using Badumna::InputStream;
    using Badumna::Vector3;
    using Badumna::BooleanArray;

    ReplicaObject::ReplicaObject() 
        : Avatar(), 
        position_(0.0f, 0.0f, 0.0f), 
        guid_(BadumnaId::None()),
        radius_(0.0f),
        area_of_interest_radius_(0.0f)
    {
        setZValue(5);
    }

    BadumnaId ReplicaObject::Guid() const
    {
        return guid_;
    }

    void ReplicaObject::SetGuid(BadumnaId const &id)
    {
        guid_ = id;
    }

    void ReplicaObject::HandleEvent(InputStream *)
    {
    }

    Vector3 ReplicaObject::Position() const
    {
        return position_;
    }

    void ReplicaObject::SetPosition(Vector3 const &value)
    {
        position_ = value;
        this->setPos(position_.GetX(), position_.GetY());
    }

    float ReplicaObject::Radius() const
    {
        return radius_;
    }

    void ReplicaObject::SetRadius(float value)
    {
        radius_ = value;
    }

    float ReplicaObject::AreaOfInterestRadius() const
    {
        return area_of_interest_radius_;
    }

    void ReplicaObject::SetAreaOfInterestRadius(float value)
    {
        area_of_interest_radius_ = value;
    }

    void ReplicaObject::Deserialize(BooleanArray const &included_parts, InputStream *stream, 
            int /*estimated_milliseconds_since_departure*/)
    {
        Vector3 v = Position();
        setPos(v.GetX(), v.GetY());
        
        if(included_parts[Color])
        {
            int r, g, b;
            (*stream) >> r;
            (*stream) >> g;
            (*stream) >> b;

            color_.setRgb(r, g, b);
            update();
        }
    }
}