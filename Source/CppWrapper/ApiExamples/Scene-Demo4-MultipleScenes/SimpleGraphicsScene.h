//-----------------------------------------------------------------------
// <copyright file="SimpleGraphicsScene.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef SIMPLE_GRAPHICS_SCENE_H
#define SIMPLE_GRAPHICS_SCENE_H

#include <QtGui>

#include "LocalAvatar.h"
#include "BadumnaSceneBorder.h"
#include "Badumna/DataTypes/Vector3.h"

namespace ApiExamples
{
    class ReplicationManager;

    class SimpleGraphicsScene : public QGraphicsScene
    {
    public:
        SimpleGraphicsScene() 
            : QGraphicsScene(), 
            local_avatar_(NULL),
            replication_manager_(NULL)
        {
        }

        void RegisterBadumnaSceneBorder(BadumnaSceneBorder *border);
        void RegisterLocalEntity(LocalAvatar *local);
        void RemoveLocalEntity(LocalAvatar *local);

        void RegisterReplicaEntity(Avatar *replica);
        void RemoveReplicaEntity(Avatar *replica);

        void RegisterReplicationManager(ReplicationManager *replication_manager);

    protected:
        void keyPressEvent(QKeyEvent *e);
        
    private:
        void CheckChangeScene(Badumna::Vector3 old_position);

        LocalAvatar *local_avatar_;
        ReplicationManager *replication_manager_;
    };
}

#endif // SIMPLE_GRAPHICS_SCENE_H