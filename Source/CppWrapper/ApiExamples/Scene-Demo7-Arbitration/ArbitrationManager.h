//-----------------------------------------------------------------------
// <copyright file="ArbitrationManager.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef ARBITRATION_MANAGER_H
#define ARBITRATION_MANAGER_H

#include "MainWindow.h"

namespace ApiExamples
{
    class ArbitrationManager
    {
    public:
        ArbitrationManager(MainWindow *dialog, Badumna::NetworkFacade *facade, bool server_mode);

        void Initialize();

        void ArbitrationServerConnectionResultHandler(Badumna::ServiceConnectionResultType type);
        void ArbitrationServerConnectionFailureHandler();
        void ArbitrationServerMessageHandler(Badumna::InputStream *stream);
        void SendArbitrationEventToServer();

        void ArbitrationClientMessageHandler(int32_t session_id, Badumna::InputStream *stream);
        void ArbitrationClientDisconnectHandler(int32_t session_id);
        void SendArbitrationEventToClient(int32_t destination_session_id);

    private:
        void UpdateRequests();
        void UpdateReplies();
        void UpdateConnectionStatus();

        MainWindow *dialog_;
        Badumna::NetworkFacade *facade_;
        std::auto_ptr<Badumna::IArbitrator> arbitrator_;

        bool server_mode_;
        bool connected_to_server_;
        int request_count_;
        int reply_count_;
    };
}

#endif // ARBITRATION_MANAGER_H