//-----------------------------------------------------------------------
// <copyright file="MainWindow.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <memory>
#include <QDialog>
#include <QtGui>
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/DataTypes/String.h"

namespace ApiExamples
{
    class ArbitrationManager;

    class MainWindow : public QDialog
    {
        Q_OBJECT
    public:
        MainWindow();
        ~MainWindow();
        
        void Initialize(QString title);

        void SetRequests(const int request_count);
        void SetReplies(const int reply_count);
        void SetConnectionStatus(const bool connected_to_server);

        void ShowErrorAndExit(const Badumna::String &error_text);

    public slots:
        void ProcessRegularState();
        void SendArbitrationEvent();

    private:
        void SelectClientOrServerMode();

        void InitializeGui(QString window_title);
        void InitializeBadumna();
        void InitializeArbitration();

        std::auto_ptr<Badumna::NetworkFacade> facade_;
        std::auto_ptr<QGraphicsScene> scene_;
        std::auto_ptr<ArbitrationManager> arbitration_manager_;

        QTimer *scene_timer_;
        QTimer *regular_timer_;

        QLabel *requests_label_;
        QLabel *replies_label_;
        QLabel *status_label_;
        QPushButton *send_button_;

        bool server_mode_;

        // disallow copy and assign
        MainWindow(const MainWindow &other);
        MainWindow &operator= (const MainWindow &rhs);
    };
}

#endif // MAIN_WINDOW_H