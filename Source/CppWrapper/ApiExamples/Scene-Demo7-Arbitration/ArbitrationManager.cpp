//-----------------------------------------------------------------------
// <copyright file="ArbitrationManager.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include "ArbitrationManager.h"

namespace ApiExamples
{
    using Badumna::ArbitrationClientDisconnectDelegate;
    using Badumna::ArbitrationClientMessageDelegate;
    using Badumna::ArbitrationConnectionDelegate;
    using Badumna::ArbitrationConnectionFailureDelegate;
    using Badumna::ArbitrationServerMessageDelegate;
    using Badumna::InputStream;
    using Badumna::NetworkFacade;
    using Badumna::OutputStream;
    using Badumna::ServiceConnectionResultType;

    ArbitrationManager::ArbitrationManager(MainWindow *dialog, NetworkFacade *facade, bool server_mode)
        : dialog_(dialog)
        , facade_(facade)
        , arbitrator_()
        , server_mode_(server_mode)
        , connected_to_server_(false)
        , request_count_(0)
        , reply_count_(0)
    {
    }

    void ArbitrationManager::Initialize()
    {
        if (server_mode_)
        {
            facade_->RegisterArbitrationHandler(
                ArbitrationClientMessageDelegate(&ArbitrationManager::ArbitrationClientMessageHandler, *this),
                60,
                ArbitrationClientDisconnectDelegate(&ArbitrationManager::ArbitrationClientDisconnectHandler, *this));
            facade_->AnnounceService(Badumna::ServerType_ArbitrationServer);
        }
        else
        {
            arbitrator_.reset(facade_->GetArbitrator("Messages"));
            arbitrator_->Connect(
                ArbitrationConnectionDelegate(&ArbitrationManager::ArbitrationServerConnectionResultHandler, *this),
                ArbitrationConnectionFailureDelegate(&ArbitrationManager::ArbitrationServerConnectionFailureHandler, *this),
                ArbitrationServerMessageDelegate(&ArbitrationManager::ArbitrationServerMessageHandler, *this));

            UpdateConnectionStatus();
        }

        UpdateRequests();
        UpdateReplies();
    }

    void ArbitrationManager::ArbitrationServerConnectionResultHandler(ServiceConnectionResultType type)
    {
        if (type == Badumna::ServiceConnection_Success)
        {
            connected_to_server_ = true;
            UpdateConnectionStatus();
        }
    }

    void ArbitrationManager::ArbitrationServerConnectionFailureHandler()
    {
        if (connected_to_server_)
        {
            // Might want to limit the number of reconnects.
            arbitrator_->Connect(
                ArbitrationConnectionDelegate(&ArbitrationManager::ArbitrationServerConnectionResultHandler, *this),
                ArbitrationConnectionFailureDelegate(&ArbitrationManager::ArbitrationServerConnectionFailureHandler, *this),
                ArbitrationServerMessageDelegate(&ArbitrationManager::ArbitrationServerMessageHandler, *this));

            connected_to_server_ = false;
            UpdateConnectionStatus();
        }
    }

    void ArbitrationManager::ArbitrationServerMessageHandler(InputStream *)
    {
        ++reply_count_;
        UpdateReplies();
    }

    void ArbitrationManager::SendArbitrationEventToServer()
    {
        if (arbitrator_.get() == NULL)
        {
            return;
        }

        OutputStream s;
        s << 'r' << 'e' << 'q' << 'u' << 'e' << 's' << 't';
        arbitrator_->SendEvent(&s);

        ++request_count_;
        UpdateRequests();
    }

    void ArbitrationManager::ArbitrationClientMessageHandler(int32_t session_id, InputStream *)
    {
        ++request_count_;
        UpdateRequests();

        SendArbitrationEventToClient(session_id);
    }

    void ArbitrationManager::ArbitrationClientDisconnectHandler(int32_t)
    {
    }

    void ArbitrationManager::SendArbitrationEventToClient(int32_t destination_session_id)
    {
        OutputStream s;
        s << 'r' << 'e' << 'p' << 'l' << 'y';
        facade_->SendServerArbitrationEvent(destination_session_id, &s);

        ++reply_count_;
        UpdateReplies();
    }

    void ArbitrationManager::UpdateRequests()
    {
        dialog_->SetRequests(request_count_);
    }

    void ArbitrationManager::UpdateReplies()
    {
        dialog_->SetReplies(reply_count_);
    }

    void ArbitrationManager::UpdateConnectionStatus()
    {
        dialog_->SetConnectionStatus(connected_to_server_);
    }
}