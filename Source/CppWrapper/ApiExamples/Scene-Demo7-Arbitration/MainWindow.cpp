//-----------------------------------------------------------------------
// <copyright file="MainWindow.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include <cassert>
#include <sstream>
#include <QtGui>
#include "Badumna/Core/NetworkFacade.h"
#include "ArbitrationManager.h"

#include "MainWindow.h"

namespace
{
    using std::auto_ptr;
    using Badumna::String;
    using Badumna::NetworkFacade;
    using Badumna::Options;

    QString BStringToQString(String const &str)
    {
#ifdef WIN32
        assert(sizeof(wchar_t) == 2);
        QString msg;
        msg.setUtf16((ushort *)str.CStr(), str.Length());
        return msg;
#else
        assert(sizeof(wchar_t) == 4);
        ushort *tmp = new ushort[str.Length() + 1];
        for(size_t i = 0; i < str.Length(); ++i)
        {
            tmp[i] = (ushort)(str.CStr()[i]);
        }
        tmp[str.Length()] = 0; 

        QString msg;
        msg.setUtf16(tmp, str.Length());
        delete[] tmp;

        return msg;
#endif
    }

    String QStringToBString(QString const &str)
    {
#ifdef WIN32
        assert(sizeof(wchar_t) == 2);
        String msg = (wchar_t *)(str.utf16());
        return msg;
#else
        assert(sizeof(wchar_t) == 4);
        wchar_t *tmp = new wchar_t[str.length() + 1];
        for(int i = 0; i < str.length(); ++i)
        {
            tmp[i] = (wchar_t)(str.utf16()[i]);
        }
        tmp[str.length()] = 0;

        String msg = tmp;
        delete[] tmp;

        return msg;
#endif
    }
}

namespace ApiExamples
{
    MainWindow::MainWindow()
        : facade_()
        , scene_(new QGraphicsScene())
        , arbitration_manager_()
        , scene_timer_(new QTimer(scene_.get()))
        , regular_timer_(new QTimer(this))
        , requests_label_()
        , replies_label_()
        , status_label_()
        , send_button_()
        , server_mode_(false)
    {
    }

    MainWindow::~MainWindow()
    {
        facade_->Shutdown();
    }

    void MainWindow::Initialize(QString title)
    {
        SelectClientOrServerMode();
        InitializeGui(title);
        InitializeBadumna();
        InitializeArbitration();
    }

    void MainWindow::SetRequests(const int request_count)
    {
        std::stringstream buf;
        buf << request_count;
        requests_label_->setText(tr(buf.str().c_str()));
    }

    void MainWindow::SetReplies(const int reply_count)
    {
        std::stringstream buf;
        buf << reply_count;
        replies_label_->setText(tr(buf.str().c_str()));
    }

    void MainWindow::SetConnectionStatus(const bool connected_to_server)
    {
        if (connected_to_server)
        {
            status_label_->setText(tr("Connected to server."));
        }
        else
        {
            status_label_->setText(tr("Not connected to server."));
        }
    }

    void MainWindow::ShowErrorAndExit(const String &error_text)
    {
        QMessageBox message_box;
        message_box.setWindowTitle(tr("Error"));
        message_box.setText(BStringToQString(error_text));
        message_box.exec();
        exit(1);
    }

    void MainWindow::ProcessRegularState()
    {
        if ((facade_.get() != NULL) && facade_->IsLoggedIn())
        {
            facade_->ProcessNetworkState();
        }
    }
    
    void MainWindow::SendArbitrationEvent()
    {
        arbitration_manager_->SendArbitrationEventToServer();
    }

    void MainWindow::SelectClientOrServerMode()
    {
        QMessageBox message_box;
        message_box.addButton(tr("Client"), QMessageBox::ActionRole);
        QAbstractButton *server_button = message_box.addButton(tr("Server"), QMessageBox::ActionRole);
        message_box.setWindowTitle(tr("Scene Demo 7 - Arbitration"));
        message_box.setText(tr("Select client or server mode."));
        message_box.exec();

        if (message_box.clickedButton() == server_button)
        {
            server_mode_ = true;
        }
    }

    void MainWindow::InitializeGui(QString window_title)
    {
        scene_->setSceneRect(0, 0, 400, 400);
        scene_->setItemIndexMethod(QGraphicsScene::NoIndex);
    
        QGroupBox *status_box = new QGroupBox();
        requests_label_ = new QLabel();
        replies_label_ = new QLabel();

        QHBoxLayout *status_layout = new QHBoxLayout;
        status_layout->addWidget(new QLabel(tr("No. Requests")));
        status_layout->addWidget(requests_label_);
        status_layout->addWidget(new QLabel(tr("No. Replies")));
        status_layout->addWidget(replies_label_);
        status_box->setLayout(status_layout);

        QVBoxLayout *main_layout = new QVBoxLayout;
        main_layout->addWidget(status_box);

        if (!server_mode_)
        {
            QGroupBox *send_box = new QGroupBox();
            status_label_ = new QLabel();
            send_button_ = new QPushButton(tr("Send Request"));

            connect(send_button_, SIGNAL(clicked()), this, SLOT(SendArbitrationEvent()));

            QHBoxLayout *send_layout = new QHBoxLayout;
            send_layout->addWidget(status_label_);
            send_layout->addWidget(send_button_);
            send_box->setLayout(send_layout);

            main_layout->addWidget(send_box);
        }

        setLayout(main_layout);

        connect(scene_timer_, SIGNAL(timeout()), scene_.get(), SLOT(advance()));
        scene_timer_->start(1000 / 33);

        connect(regular_timer_, SIGNAL(timeout()), this, SLOT(ProcessRegularState()));
        regular_timer_->start(1000 / 60);

        setWindowTitle(window_title + (server_mode_ ? tr(" Server") : tr(" Client")));
    }

    void MainWindow::InitializeBadumna()
    {
        Options options;
        options.GetConnectivityModule().SetStartPortRange(server_mode_ ? 21260 : 21300);
        options.GetConnectivityModule().SetEndPortRange(server_mode_ ? 21260 : 21399);
        options.GetConnectivityModule().SetMaxPortsToTry(3);
        options.GetConnectivityModule().EnableBroadcast();
        options.GetConnectivityModule().SetBroadcastPort(21250);
        options.GetConnectivityModule().ConfigureForLan();
        options.GetConnectivityModule().SetApplicationName(L"scene-demo7-arbitration");
        options.GetArbitrationModule().AddServer("Messages");

        facade_.reset(NetworkFacade::Create(options));

        if (!facade_->Login())
        {
            ShowErrorAndExit("Failed to initialize badumna.");
        }
    }

    void MainWindow::InitializeArbitration()
    {
        arbitration_manager_.reset(new ArbitrationManager(this, facade_.get(), server_mode_));
        arbitration_manager_->Initialize();
    }
}