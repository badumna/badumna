
SCENEDEMO7=Scene-Demo7-Arbitration

ifeq ($(OS), Windows_NT)
SCENEDEMO7:=$(SCENEDEMO7).exe
endif

SCENEDEMO7_PATH=Scene-Demo7-Arbitration
SCENEDEMO7_OBJS=$(SCENEDEMO7_PATH)/ArbitrationManager.o \
	      $(SCENEDEMO7_PATH)/MainWindow.o \
	      $(SCENEDEMO7_PATH)/Program.o \
		  $(SCENEDEMO7_PATH)/MainWindow.moc.o 

$(SCENEDEMO7): $(SCENEDEMO7_OBJS) | check_libmono_path
	@echo [Linking]: $@
	$(VERBOSE_MODE)$(CXX) $(LDFLAGS) -o $@ $(SCENEDEMO7_OBJS) $(LIBS) $(NATIVE_WINDOWS_APP_FLAG)

DEMOS+=$(SCENEDEMO7)
ALL_OBJS+=$(SCENEDEMO7_OBJS)
INSTALL_FILES+=$(SCENEDEMO7)
