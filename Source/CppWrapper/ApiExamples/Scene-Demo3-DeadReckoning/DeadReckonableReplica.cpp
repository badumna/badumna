//-----------------------------------------------------------------------
// <copyright file="DeadReckonableReplica.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include "DeadReckonableReplica.h"

namespace ApiExamples
{
    using Badumna::Vector3;
    using Badumna::BadumnaId;
    using Badumna::InputStream;
    using Badumna::BooleanArray;

    DeadReckonableReplica::DeadReckonableReplica() 
        : Avatar(), 
        position_(0.0f, 0.0f, 0.0f), 
        velocity_(0.0f, 0.0f, 0.0f),
        guid_(BadumnaId::None()),
        radius_(0.0f),
        area_of_interest_radius_(0.0f)
    {
        setZValue(5);
    }

    BadumnaId DeadReckonableReplica::Guid() const
    {
        return guid_;
    }

    void DeadReckonableReplica::SetGuid(BadumnaId const &id)
    {
        guid_ = id;
    }

    void DeadReckonableReplica::HandleEvent(InputStream *)
    {
    }

    Vector3 DeadReckonableReplica::Position() const
    {
        return position_;
    }

    void DeadReckonableReplica::SetPosition(Vector3 const &value)
    {
        position_ = value;
        this->setPos(position_.GetX(), position_.GetY());
    }

    float DeadReckonableReplica::Radius() const
    {
        return radius_;
    }

    void DeadReckonableReplica::SetRadius(float value)
    {
        radius_ = value;
    }

    float DeadReckonableReplica::AreaOfInterestRadius() const
    {
        return area_of_interest_radius_;
    }

    void DeadReckonableReplica::SetAreaOfInterestRadius(float value)
    {
        area_of_interest_radius_ = value;
    }

    void DeadReckonableReplica::Deserialize(BooleanArray const &included_parts, InputStream *stream, 
            int /*estimated_milliseconds_since_departure*/)
    {
        Vector3 v = Position();
        setPos(v.GetX(), v.GetY());
        
        if(included_parts[Color])
        {
            int r, g, b;
            (*stream) >> r;
            (*stream) >> g;
            (*stream) >> b;

            color_.setRgb(r, g, b);
            update();
        }
    }

    Vector3 DeadReckonableReplica::Velocity() const
    {
        return velocity_;
    }

    void DeadReckonableReplica::SetVelocity(Vector3 const &v)
    {
        velocity_ = v;
    }

    // The AttemptMovement method will be periodically called by Badumna with an estimated currented position.
    // It is application code's responibility to determine whether the estimated position is valid or not.
    void DeadReckonableReplica::AttemptMovement(Vector3 const &reckoned_position)
    {
        SetPosition(reckoned_position);
    }
}