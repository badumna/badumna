
#include "Conversion.h"

QString BStringToQString(Badumna::String const &str)
{
#ifdef WIN32
    assert(sizeof(wchar_t) == 2);
    QString msg;
    msg.setUtf16((ushort *)str.CStr(), str.Length());
    return msg;
#else
    assert(sizeof(wchar_t) == 4);
    ushort *tmp = new ushort[str.Length() + 1];
    for(size_t i = 0; i < str.Length(); ++i)
    {
        tmp[i] = (ushort)(str.CStr()[i]);
    }
    tmp[str.Length()] = 0; 

    QString msg;
    msg.setUtf16(tmp, str.Length());
    delete[] tmp;

    return msg;
#endif
}

Badumna::String QStringToBString(QString const &str)
{
#ifdef WIN32
    assert(sizeof(wchar_t) == 2);
    Badumna::String msg = (wchar_t *)(str.utf16());
    return msg;
#else
    assert(sizeof(wchar_t) == 4);
    wchar_t *tmp = new wchar_t[str.length() + 1];
    for(int i = 0; i < str.length(); ++i)
    {
        tmp[i] = (wchar_t)(str.utf16()[i]);
    }
    tmp[str.length()] = 0;

    Badumna::String msg = tmp;
    delete[] tmp;

    return msg;
#endif
}
