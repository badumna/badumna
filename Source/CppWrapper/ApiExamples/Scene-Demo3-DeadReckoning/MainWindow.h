//-----------------------------------------------------------------------
// <copyright file="MainWindow.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <memory>
#include <QDialog>
#include "Badumna/DataTypes/String.h"
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/Replication/ISpatialOriginal.h"

#include "DeadReckonableLocalEntity.h"
#include "SimpleGraphicsScene.h"

namespace ApiExamples
{
    class ReplicationManager;

    class MainWindow : public QDialog
    {
        Q_OBJECT
    public:
        MainWindow();
        ~MainWindow();
        
        void Initialize(QString title);

        SimpleGraphicsScene *GetGraphicScene() const
        {
            return scene_.get();
        }

    public slots:
        void ProcessRegularState();

    private:
        void InitializeBadumna();
        void InitializeGui(QString window_title);

        std::auto_ptr<Badumna::NetworkFacade> facade_;
        std::auto_ptr<DeadReckonableLocalEntity> local_entity_;
        std::auto_ptr<SimpleGraphicsScene> scene_;
        std::auto_ptr<ReplicationManager> replication_manager_;
        
        QGraphicsView *view_;
        QTimer *scene_timer_;
        QTimer *regular_timer_;
        QVBoxLayout *main_layout_;

        // disallow copy and assign
        MainWindow(MainWindow const &other);
        MainWindow &operator=(MainWindow const &rhs);
    };
}

#endif // MAIN_WINDOW_H