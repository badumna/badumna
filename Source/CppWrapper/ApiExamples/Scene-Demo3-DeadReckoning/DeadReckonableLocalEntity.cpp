//-----------------------------------------------------------------------
// <copyright file="DeadReckonableLocalEntity.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include <QGraphicsScene>
#include <QPainter>
#include <QStyleOption>

#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Replication/SpatialEntityStateSegment.h"

#include "StateSegment.h"
#include "DeadReckonableLocalEntity.h"

namespace ApiExamples
{
    using Badumna::NetworkFacade;
    using Badumna::String;
    using Badumna::BadumnaId;
    using Badumna::InputStream;
    using Badumna::OutputStream;
    using Badumna::Vector3;
    using Badumna::BooleanArray;

    DeadReckonableLocalEntity::DeadReckonableLocalEntity(NetworkFacade *facade)
        : LocalAvatar(), 
        facade_(facade), 
        guid_(BadumnaId::None()),
        velocity_(0.0f, 0.0f, 0.0f),
        position_(0.0f, 0.0f, 0.0f), 
        radius_(0.0f),
        area_of_interest_radius_(0.0f),
        last_pos_(0.0f, 0.0f, 0.0f),
        timer_(),
        last_update_time_ms_(0)
    {
        setZValue(10);
        timer_.start();
    }

    BadumnaId DeadReckonableLocalEntity::Guid() const
    {
        return guid_;
    }

    void DeadReckonableLocalEntity::SetGuid(BadumnaId const &id)
    {
        guid_ = id;
    }

    void DeadReckonableLocalEntity::HandleEvent(InputStream *)
    {
    }

    Vector3 DeadReckonableLocalEntity::Position() const
    {
        return position_;
    }

    void DeadReckonableLocalEntity::SetPosition(Vector3 const &value)
    {
        position_ = value;
        setPos(value.GetX(), value.GetY());
        facade_->FlagForUpdate(*this, Badumna::StateSegment_Position);
        last_pos_ = value;
    }

    void DeadReckonableLocalEntity::SetPosition(qreal x, qreal y)
    {
        Vector3 v(x, y, 0.0f);
        SetPosition(v);
    }

    float DeadReckonableLocalEntity::Radius() const
    {
        return radius_; 
    }

    void DeadReckonableLocalEntity::SetRadius(float value)
    {
        radius_ = value;
        facade_->FlagForUpdate(*this, Badumna::StateSegment_Radius);
    }

    float DeadReckonableLocalEntity::AreaOfInterestRadius() const
    {
        return area_of_interest_radius_;
    }

    void DeadReckonableLocalEntity::SetAreaOfInterestRadius(float value)
    {
        area_of_interest_radius_ = value;
        facade_->FlagForUpdate(*this, Badumna::StateSegment_InterestRadius);
    }

    void DeadReckonableLocalEntity::Serialize(BooleanArray const &required_parts, OutputStream *stream)
    {
        if(required_parts[Color])
        {
            (*stream) << color_.red();
            (*stream) << color_.green();
            (*stream) << color_.blue();
        }
    }

    void DeadReckonableLocalEntity::advance(int)
    {
    }

    void DeadReckonableLocalEntity::MoveRight()
    {
        float vx;
        Vector3 v = Velocity();
        
        if(v.GetX() < 0.0f)
        {
            vx = 0.0f;
        }
        else
        {
            vx = move_unit;
        }
    
        SetVelocity(Vector3(vx, v.GetY(), 0.0f));
    }
    
    void DeadReckonableLocalEntity::MoveLeft()
    {
        float vx;
        Vector3 v = Velocity();
        
        if(v.GetX() > 0.0f)
        {
            vx = 0.0f;
        }
        else
        {
            vx = -1.0 * move_unit;
        }
    
        SetVelocity(Vector3(vx, v.GetY(), 0.0f));
    }
    
    void DeadReckonableLocalEntity::MoveUp()
    {
        float vy;
        Vector3 v = Velocity();
        
        if(v.GetY() > 0.0f)
        {
            vy = 0.0f;
        }
        else
        {
            vy = -1.0 * move_unit;
        }
    
        SetVelocity(Vector3(v.GetX(), vy, 0.0f));
    }
    
    void DeadReckonableLocalEntity::MoveDown()
    {
        float vy;
        Vector3 v = Velocity();
        
        if(v.GetY() < 0.0f)
        {
            vy = 0.0f;
        }
        else
        {
            vy = move_unit;
        }
    
        SetVelocity(Vector3(v.GetX(), vy, 0.0f));
    }

    void DeadReckonableLocalEntity::SetRandomColor()
    {
        color_.setRgb(qrand() % 256, qrand() % 256, qrand() % 256);
        facade_->FlagForUpdate(*this, Color);
        update();
    }

    Vector3 DeadReckonableLocalEntity::Velocity() const
    {
        return velocity_;
    }

    void DeadReckonableLocalEntity::SetVelocity(Vector3 const &v)
    {
        velocity_ = v;
        facade_->FlagForUpdate(*this, Badumna::StateSegment_Velocity);
    }

    void DeadReckonableLocalEntity::AttemptMovement(Vector3 const &/*reckoned_position*/)
    {
    }

    void DeadReckonableLocalEntity::UpdatePosition()
    {
        int now = timer_.elapsed();
        int elapsed = now - last_update_time_ms_;
        float elapsed_second = (elapsed / 1000.0f);

        Vector3 displacement(velocity_.GetX() * elapsed_second, velocity_.GetY() * elapsed_second, 0.0f);
        Vector3 new_position(displacement.GetX() + last_pos_.GetX(), displacement.GetY() + last_pos_.GetY(), 0.0f);

        SetPosition(new_position);
        last_update_time_ms_ = now;
    }
}