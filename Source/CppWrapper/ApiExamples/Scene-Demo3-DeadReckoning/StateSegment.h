//-----------------------------------------------------------------------
// <copyright file="StateSegment.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef STATE_SEGMENT_H
#define STATE_SEGMENT_H

#include "Badumna/Replication/SpatialEntityStateSegment.h"

namespace ApiExamples
{
    enum StateSegment
    {
        Color = Badumna::StateSegment_FirstAvailableSegment,
    };
}

#endif // STATE_SEGMENT_H