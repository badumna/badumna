//-----------------------------------------------------------------------
// <copyright file="DeadReckonableLocalEntity.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef DEADRECKONABLE_LOCAL_ENTITY_H
#define DEADRECKONABLE_LOCAL_ENTITY_H

#include <QtGui>
#include <QDialog>

#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/DataTypes/Vector3.h"
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/Replication/IDeadReckonableSpatialOriginal.h"

#include "StateSegment.h"
#include "LocalAvatar.h"

namespace ApiExamples
{
    class DeadReckonableLocalEntity : public LocalAvatar, public Badumna::IDeadReckonableSpatialOriginal
    {
    public:
        explicit DeadReckonableLocalEntity(Badumna::NetworkFacade *facade);

        virtual void MoveRight();
        virtual void MoveLeft();
        virtual void MoveUp();
        virtual void MoveDown();

        virtual void SetRandomColor();

        //
        // IEntity properties
        // 
        Badumna::BadumnaId Guid() const;
        void SetGuid(Badumna::BadumnaId const &id);
        void HandleEvent(Badumna::InputStream *stream);

        //
        // ISpatialEntity properties
        //
        Badumna::Vector3 Position() const;
        void SetPosition(qreal x, qreal y);
        void SetPosition(Badumna::Vector3 const &position);
        float Radius() const;
        void SetRadius(float val);
        float AreaOfInterestRadius() const;
        void SetAreaOfInterestRadius(float val);

        //
        // ISpatialOriginal properties
        //
        void Serialize(Badumna::BooleanArray const &required_parts, Badumna::OutputStream *stream);

        // Deadreckoning
        Badumna::Vector3 Velocity() const;
        void SetVelocity(Badumna::Vector3 const &velocity);
        void AttemptMovement(Badumna::Vector3 const &reckoned_position);

        // compute the position
        void UpdatePosition();

        static const int move_unit = 20;

    protected:
        void advance(int step);
    
    private:
        Badumna::NetworkFacade *facade_;

        Badumna::BadumnaId guid_;
        Badumna::Vector3 velocity_;
        Badumna::Vector3 position_;

        float radius_;
        float area_of_interest_radius_;

        // for deadreckoning
        Badumna::Vector3 last_pos_;

        QTime timer_;
        int last_update_time_ms_;

        // disallow copy and assign
        DeadReckonableLocalEntity(DeadReckonableLocalEntity const &other);
        DeadReckonableLocalEntity& operator=(DeadReckonableLocalEntity const &rhs);
    };
}

#endif // DEADRECKONABLE_LOCAL_ENTITY_H