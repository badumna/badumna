//-----------------------------------------------------------------------
// <copyright file="DeadReckonableReplica.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef DEADRECKONABLE_REPLICA_H
#define DEADRECKONABLE_REPLICA_H

#include <QtGui>
#include <QDialog>

#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/DataTypes/Vector3.h"
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/Replication/IDeadReckonableSpatialReplica.h"

#include "StateSegment.h"
#include "Avatar.h"

namespace ApiExamples
{
    class DeadReckonableReplica : public Avatar, public Badumna::IDeadReckonableSpatialReplica
    {
    public:
        DeadReckonableReplica();
        
        //
        // IEntity properties
        // 
        Badumna::BadumnaId Guid() const;
        void SetGuid(Badumna::BadumnaId const &id);
        void HandleEvent(Badumna::InputStream *stream);

        //
        // ISpatialEntity properties
        //
        Badumna::Vector3 Position() const;
        void SetPosition(Badumna::Vector3 const &position);
        float Radius() const;
        void SetRadius(float val);
        float AreaOfInterestRadius() const;
        void SetAreaOfInterestRadius(float val);

        void Deserialize(
            Badumna::BooleanArray const &included_parts, 
            Badumna::InputStream *stream, 
            int estimated_milliseconds_since_departure);

        void SetColor(QColor c);
        
        Badumna::Vector3 Velocity() const;
        void SetVelocity(Badumna::Vector3 const &velocity);
        void AttemptMovement(Badumna::Vector3 const &reckoned_position);

    private:
        Badumna::Vector3 position_;
        Badumna::Vector3 velocity_;
        Badumna::BadumnaId guid_;
        float radius_;
        float area_of_interest_radius_;

        // disallow copy and assign
        DeadReckonableReplica(DeadReckonableReplica const &other);
        DeadReckonableReplica &operator=(DeadReckonableReplica const &rhs);
    };
}

#endif // DEADRECKONABLE_REPLICA_H