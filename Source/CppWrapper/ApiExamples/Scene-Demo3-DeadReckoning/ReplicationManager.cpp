//-----------------------------------------------------------------------
// <copyright file="ReplicationManager.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include <cassert>
#include <iostream>
#include <string>
#include <algorithm>

#include "Badumna/DataTypes/Vector3.h"
#include "Badumna/DataTypes/BadumnaId.h"
#include "ReplicationManager.h"
#include "EntityType.h"

namespace ApiExamples
{
    using std::wstring;
    using Badumna::ISpatialReplica;
    using Badumna::Vector3;
    using Badumna::BadumnaId;
    using Badumna::NetworkFacade;
    using Badumna::NetworkScene;

    struct GuidEqualityComparable : public std::binary_function<ISpatialReplica *, BadumnaId, bool>
    {
        bool operator()(ISpatialReplica *replica, BadumnaId const &id) const
        {
            return replica->Guid() == id;
        }
    };

    ReplicationManager::ReplicationManager(
        MainWindow *dialog, 
        NetworkFacade *network_facade, 
        DeadReckonableLocalEntity *entity)
        : dialog_(dialog), 
        facade_(network_facade),
        local_entity_(entity),
        replica_objects_(),
        scene_(NULL),
        graphics_scene_(dialog_->GetGraphicScene())
    {
        Initialize();
    }

    void ReplicationManager::Shutdown()
    {
        if(scene_.get() != NULL)
        {
            scene_->UnregisterEntity(*local_entity_);
            scene_->Leave();
        }
    }

    void ReplicationManager::Initialize()
    {
        // join scene
        scene_.reset(facade_->JoinScene(L"demo3_scene",
            Badumna::CreateSpatialReplicaDelegate(&ReplicationManager::CreateSpatialReplica, *this),
            Badumna::RemoveSpatialReplicaDelegate(&ReplicationManager::RemoveSpatialReplica, *this)));

		float aoi = 50.0f;
		facade_->RegisterEntityDetails(aoi, DeadReckonableLocalEntity::move_unit);

        // initialize the local avatar
        Vector3 initial_position(30.0f, 30.0f, 0.0f);
        
        local_entity_->SetPosition(initial_position);
        local_entity_->SetRadius(10.0f);
        local_entity_->SetAreaOfInterestRadius(aoi);

        // register the entity, this is a regular entity
        scene_->RegisterEntity(local_entity_, NormalEntity);
        local_entity_->SetPosition(initial_position);
    }

    ISpatialReplica *ReplicationManager::CreateSpatialReplica(
        NetworkScene const &, 
        BadumnaId const &id, 
        uint32_t)
    {
        std::vector<DeadReckonableReplica*>::iterator iter; 
        iter = std::find_if(
            replica_objects_.begin(), 
            replica_objects_.end(), 
            std::bind2nd(GuidEqualityComparable(), id));
        if(iter != replica_objects_.end())
        {
            return *iter;
        }
        else
        {
            DeadReckonableReplica* replica = new DeadReckonableReplica();
            replica->SetGuid(id);
            replica_objects_.push_back(replica);

            graphics_scene_->RegisterReplicaEntity(replica);
            return replica;
        }
    }
    
    void ReplicationManager::RemoveSpatialReplica(NetworkScene const &, ISpatialReplica const &replica)
    {
        std::vector<DeadReckonableReplica*>::iterator iter;
        iter = std::find_if(
            replica_objects_.begin(), 
            replica_objects_.end(), 
            std::bind2nd(GuidEqualityComparable(), replica.Guid()));
        if(iter != replica_objects_.end())
        {
            DeadReckonableReplica* to_remove = *iter;
            graphics_scene_->RemoveReplicaEntity(to_remove);
            replica_objects_.erase(iter);
            delete to_remove;
        }
    }
}