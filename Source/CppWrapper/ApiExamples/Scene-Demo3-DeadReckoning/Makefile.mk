
SCENEDEMO3=Scene-Demo3-DeadReckoning

ifeq ($(OS), Windows_NT)
SCENEDEMO3:=$(SCENEDEMO3).exe
endif

SCENEDEMO3_PATH=Scene-Demo3-DeadReckoning
SCENEDEMO3_OBJS=$(SCENEDEMO3_PATH)/Avatar.o \
	      $(SCENEDEMO3_PATH)/DeadReckonableLocalEntity.o \
	      $(SCENEDEMO3_PATH)/MainWindow.o \
	      $(SCENEDEMO3_PATH)/Program.o \
	      $(SCENEDEMO3_PATH)/Conversion.o \
	      $(SCENEDEMO3_PATH)/DeadReckonableReplica.o \
	      $(SCENEDEMO3_PATH)/ReplicationManager.o \
	      $(SCENEDEMO3_PATH)/SimpleGraphicsScene.o \
	      $(SCENEDEMO3_PATH)/MainWindow.moc.o

$(SCENEDEMO3): $(SCENEDEMO3_OBJS) | check_libmono_path
	@echo [Linking]: $@
	$(VERBOSE_MODE)$(CXX) $(LDFLAGS) -o $@ $(SCENEDEMO3_OBJS) $(LIBS) $(NATIVE_WINDOWS_APP_FLAG)

DEMOS+=$(SCENEDEMO3)
ALL_OBJS+=$(SCENEDEMO3_OBJS)
INSTALL_FILES+=$(SCENEDEMO3)
