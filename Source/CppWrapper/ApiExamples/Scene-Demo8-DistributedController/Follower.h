//-----------------------------------------------------------------------
// <copyright file="Follower.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef NPC_FOLLOWER_H
#define NPC_FOLLOWER_H

#include <vector>

#include "Badumna/Replication/ISpatialReplica.h"
#include "Badumna/Replication/ISpatialOriginal.h"
#include "Badumna/Utils/BasicTypes.h"

namespace ApiExamples
{
    class Follower
    {
    public:
        explicit Follower(Badumna::ISpatialOriginal *original);

        void SetPossibleTargets(std::vector<Badumna::ISpatialReplica *> const &replicas);
        void Update();

        bool IsTargetting() const
        {
            return is_targetting_;
        }

    private:
        bool is_targetting_;
        Badumna::ISpatialOriginal *original_;
        Badumna::Vector3 target_;

        int speed_;

        // disallow copy and assign
        Follower(Follower const &other);
        Follower &operator=(Follower const &rhs);
    };
}

#endif // NPC_FOLLOWER_H