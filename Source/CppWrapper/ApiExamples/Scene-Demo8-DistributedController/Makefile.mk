
SCENEDEMO8=Scene-Demo8-DistributedController

ifeq ($(OS), Windows_NT)
SCENEDEMO8:=$(SCENEDEMO8).exe
endif

SCENEDEMO8_PATH=Scene-Demo8-DistributedController
SCENEDEMO8_OBJS=$(SCENEDEMO8_PATH)/Avatar.o \
	      $(SCENEDEMO8_PATH)/LocalEntity.o \
	      $(SCENEDEMO8_PATH)/MainWindow.o \
	      $(SCENEDEMO8_PATH)/Program.o \
	      $(SCENEDEMO8_PATH)/ReplicaObject.o \
	      $(SCENEDEMO8_PATH)/ReplicationManager.o \
	      $(SCENEDEMO8_PATH)/SimpleGraphicsScene.o \
	      $(SCENEDEMO8_PATH)/Follower.o \
	      $(SCENEDEMO8_PATH)/DistributedNPC.o \
	      $(SCENEDEMO8_PATH)/MainWindow.moc.o 

$(SCENEDEMO8): $(SCENEDEMO8_OBJS) | check_libmono_path
	@echo [Linking]: $@
	$(VERBOSE_MODE)$(CXX) $(LDFLAGS) -o $@ $(SCENEDEMO8_OBJS) $(LIBS) $(NATIVE_WINDOWS_APP_FLAG)

DEMOS+=$(SCENEDEMO8)
ALL_OBJS+=$(SCENEDEMO8_OBJS)
INSTALL_FILES+=$(SCENEDEMO8)
