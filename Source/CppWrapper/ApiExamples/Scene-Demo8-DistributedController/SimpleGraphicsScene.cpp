//-----------------------------------------------------------------------
// <copyright file="SimpleGraphicsScene.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include <QGraphicsItem>
#include "SimpleGraphicsScene.h"

namespace ApiExamples
{
    void SimpleGraphicsScene::RegisterLocalEntity(LocalAvatar *local)
    {
        local_avatar_ = local;
        addItem(local);
    }

    void SimpleGraphicsScene::RemoveLocalEntity(LocalAvatar *local)
    {
        removeItem(local);
    }
    
    void SimpleGraphicsScene::RegisterReplicaEntity(Avatar *replica)
    {
        addItem(replica);
    }

    void SimpleGraphicsScene::RemoveReplicaEntity(Avatar *replica)
    {
        removeItem(replica);
    }

    void SimpleGraphicsScene::keyPressEvent(QKeyEvent *e)
    {
        switch(e->key())
        {
            case Qt::Key_Up:
                local_avatar_->MoveUp();
                break;
            case Qt::Key_Left:
                local_avatar_->MoveLeft();
                break;
            case Qt::Key_Right:
                local_avatar_->MoveRight();
                break;
            case Qt::Key_Down:
                local_avatar_->MoveDown();
                break;
            case Qt::Key_C:
                local_avatar_->SetRandomColor();
                break;
        }
    }
}