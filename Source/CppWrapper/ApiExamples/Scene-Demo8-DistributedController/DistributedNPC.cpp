//-----------------------------------------------------------------------
// <copyright file="DistributedNPC.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include <vector>
#include <algorithm>
#include <QtGui>

#include "Badumna/Core/NetworkFacade.h"
#include "DistributedNPC.h"
#include "LocalEntity.h"
#include "EntityType.h"

namespace ApiExamples
{
    using std::string;
    using namespace Badumna;

    struct GuidEqualityComparable2 : public std::binary_function<ISpatialReplica *, BadumnaId, bool>
    {
        bool operator()(ISpatialReplica *replica, BadumnaId const &id) const
        {
            return replica->Guid() == id;
        }
    };

    DistributedNPC::DistributedNPC(NetworkFacade *facade, String const &name)
        : DistributedSceneController(facade, name), 
        original_(NULL), 
        replica_objects_(), 
        follower_(NULL),
        process_called_(0)
    {
    }

    DistributedNPC::~DistributedNPC()
    {
    }

    uint32_t DistributedNPC::GetEntityType() const
    {
        return NPCEntity;
    }

    ISpatialOriginal *DistributedNPC::TakeControlOfEntity(BadumnaId const &entity_id, uint32_t /*entity_type*/)
    {
        if(original_ == NULL)
        {
            original_ = new LocalEntity(facade_);
            original_->SetGuid(entity_id);
            Vector3 initial_position(20.0f, 20.0f, 0.0f);
            original_->SetPosition(initial_position);
            original_->SetRadius(10.0f);
            original_->SetAreaOfInterestRadius(50.0f);
            original_->SetRandomColor();

            follower_ = new Follower(original_);

            Replicate();
        }

        return original_;
    }
    
    ISpatialReplica *DistributedNPC::InstantiateRemoteEntity(BadumnaId const &entity_id, uint32_t /*entity_type*/)
    {
        ReplicaObject *replica = new ReplicaObject();
        replica->SetGuid(entity_id);
        replica_objects_.push_back(replica);
        return replica;
    }
    
    void DistributedNPC::RemoveEntity(ISpatialReplica const &replica)
    {
        std::vector<ISpatialReplica*>::iterator iter;
        iter = std::find_if(
            replica_objects_.begin(), 
            replica_objects_.end(), 
            std::bind2nd(GuidEqualityComparable2(), replica.Guid()));
        if(iter != replica_objects_.end())
        {
            ISpatialReplica* to_remove = *iter;
            replica_objects_.erase(iter);
            delete to_remove;
        }
    }

    void DistributedNPC::Checkpoint(OutputStream *stream)
    {
        float x = original_->Position().GetX();
        float y = original_->Position().GetY();

        int r = original_->GetColorR();
        int g = original_->GetColorG();
        int b = original_->GetColorB();

        (*stream) << x;
        (*stream) << y;
        (*stream) << r;
        (*stream) << g;
        (*stream) << b;
    }

    void DistributedNPC::Recover(InputStream *stream)
    {
        float x;
        float y;

        int r; 
        int g;
        int b;

        (*stream) >> x;
        (*stream) >> y;

        (*stream) >> r;
        (*stream) >> g;
        (*stream) >> b;

        Vector3 v(x, y, 0);
        original_->SetPosition(v);
        original_->SetColor(r, g, b);
    }

    void DistributedNPC::Recover()
    {
        Vector3 v(20, 20, 0);
        original_->SetPosition(v);
        original_->SetRandomColor();
    }
        
    void DistributedNPC::Process(int /*duration_in_ms*/)
    {
        process_called_++;
        
        if(process_called_ % 6 == 0)
        { 
            if(follower_ != NULL && !follower_->IsTargetting() && replica_objects_.size() > 0)
            {
                follower_->SetPossibleTargets(replica_objects_);
            }

            if(follower_ != NULL && follower_->IsTargetting())
            {
                follower_->Update();
            }
        }

        if(process_called_ % 200 == 0)
        {
            Replicate();
        }
    }
    
    void DistributedNPC::Wake()
    {
    }
    
    void DistributedNPC::Sleep()
    {
        if(original_ != NULL)
        {
            delete original_;
            original_ = NULL;
        }

        if(follower_ != NULL)
        {
            delete follower_;
            follower_ = NULL;
        }
    }
}