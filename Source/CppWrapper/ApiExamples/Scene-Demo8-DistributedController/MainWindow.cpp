//-----------------------------------------------------------------------
// <copyright file="MainWindow.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include <QtGui>
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/Controller/CreateControllerDelegate.h"

#include "MainWindow.h"
#include "LocalEntity.h"
#include "SimpleGraphicsScene.h"
#include "ReplicationManager.h"
#include "DistributedNPC.h"

namespace ApiExamples
{
    using std::auto_ptr;
    using Badumna::NetworkFacade;
    using Badumna::Options;
    using Badumna::String;
    using Badumna::DistributedSceneController;
    
    DistributedSceneController *CreateDistributedNPC(NetworkFacade *facade, String const &name)
    {
        DistributedSceneController *controller = new DistributedNPC(facade, name);
        return controller;
    }

    MainWindow::MainWindow()
        : facade_(NULL),
        local_entity_(NULL),
        scene_(new SimpleGraphicsScene),
        replication_manager_(NULL),
        view_(new QGraphicsView(scene_.get())),
        scene_timer_(new QTimer(scene_.get())),
        regular_timer_(new QTimer(this)),
        main_layout_(NULL)
    {
    }

    MainWindow::~MainWindow()
    {
        replication_manager_->Shutdown();
        scene_->RemoveLocalEntity(local_entity_.get());

        facade_->Shutdown();
    }

    void MainWindow::Initialize(QString title)
    {
        InitializeGui(title);
        InitializeBadumna();
    }

    void MainWindow::InitializeBadumna()
    {
        Options options;
        options.GetConnectivityModule().SetStartPortRange(21300);
        options.GetConnectivityModule().SetEndPortRange(21399);
        options.GetConnectivityModule().SetMaxPortsToTry(3);
        options.GetConnectivityModule().EnableBroadcast();
        options.GetConnectivityModule().SetBroadcastPort(21250);
        options.GetConnectivityModule().ConfigureForLan();
        options.GetConnectivityModule().SetApplicationName(L"scene-demo8-distributedcontroller");

        facade_.reset(NetworkFacade::Create(options));

        // Register the distributed controller type before calling Login
        Badumna::CreateControllerDelegate c(CreateDistributedNPC, facade_.get());
        facade_->RegisterController("dnpc", c);

        bool succeed = facade_->Login();
        if(!succeed)
        {
            QMessageBox message_box;
            message_box.setWindowTitle("Error");
            message_box.setText("Failed to initialize badumna.");
            message_box.exec();
            exit(1);
        }

        // Start controller. 
        facade_->StartController(L"demo8_scene", L"dnpc", 16);

        local_entity_.reset(new LocalEntity(facade_.get()));
        replication_manager_.reset(new ReplicationManager(this, facade_.get(), local_entity_.get()));
        
        scene_->RegisterLocalEntity(local_entity_.get());
    }

    void MainWindow::InitializeGui(QString window_title)
    {
        scene_->setSceneRect(0, 0, 400, 400);
        scene_->setItemIndexMethod(QGraphicsScene::NoIndex);

        main_layout_ = new QVBoxLayout;
        main_layout_->addWidget(view_);
        main_layout_->setSizeConstraint(QLayout::SetFixedSize);
        setLayout(main_layout_);

        view_->setCacheMode(QGraphicsView::CacheBackground);
        view_->setRenderHint(QPainter::Antialiasing);
        view_->setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);
        view_->resize(400, 400);
        view_->setFixedWidth(400);
        view_->setFixedHeight(400);
        view_->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        view_->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        view_->setWindowFlags((view_->windowFlags() | Qt::CustomizeWindowHint) & ~Qt::WindowMaximizeButtonHint); 
        view_->show();

        QObject::connect(scene_timer_, SIGNAL(timeout()), scene_.get(), SLOT(advance()));
        scene_timer_->start(1000 / 33);

        QObject::connect(regular_timer_, SIGNAL(timeout()), this, SLOT(ProcessRegularState()));
        regular_timer_->start(1000 / 60);

        setWindowTitle(window_title);
    }

    void MainWindow::ProcessRegularState()
    {
        if(facade_.get() != NULL && facade_->IsLoggedIn())
        {
            facade_->ProcessNetworkState();
        }
    }
}