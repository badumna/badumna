//-----------------------------------------------------------------------
// <copyright file="EntityType.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef ENTITY_TYPE_H
#define ENTITY_TYPE_H

namespace ApiExamples
{
    enum EntityType
    {
        NormalEntity = 100,
        DeadRecknonableEntity = 101,
        NPCEntity = 102,
    };
}

#endif // ENTITY_TYPE_H