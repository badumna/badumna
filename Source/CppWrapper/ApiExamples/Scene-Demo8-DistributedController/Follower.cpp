//-----------------------------------------------------------------------
// <copyright file="Follower.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include <iostream>
#include <cassert>
#include <QtGui>
#include "Badumna/DataTypes/Vector3.h"
#include "Follower.h"

namespace ApiExamples
{
    using Badumna::Vector3;

    Follower::Follower(Badumna::ISpatialOriginal *original)
        : is_targetting_(false),
        original_(original),
        target_(),
        speed_((qrand() % 3) * 10 + 10)
    {
    }

    void Follower::SetPossibleTargets(std::vector<Badumna::ISpatialReplica *> const &replicas)
    {
        assert(replicas.size() > 0 && !is_targetting_);

        if(replicas.size() == 1)
        {
            target_ = replicas.at(0)->Position();
        }
        else
        {
            int r = qrand() % replicas.size();
            target_ = replicas.at(r)->Position();
        }

        is_targetting_ = true;
    }

    void Follower::Update()
    {
        if(!is_targetting_)
        {
            return;
        }

        Vector3 direction = target_ - original_->Position();
        if(direction.GetX() > 0.0)
        {
            direction.SetX(1.0);
        }

        if(direction.GetX() < 0.0)
        {
            direction.SetX(-1.0);
        }

        if(direction.GetY() > 0.0)
        {
            direction.SetY(1.0);
        }

        if(direction.GetY() < 0.0)
        {
            direction.SetY(-1.0);
        }

        Vector3 path = direction * (speed_ * 0.1);

        float remaining = (target_ - original_->Position()).Magnitude();
        if(path.Magnitude() >= remaining || remaining < 5.0f)
        {
            is_targetting_ = false;
        }
        else
        {
            Vector3 cur_pos = original_->Position();
            original_->SetPosition(cur_pos + path);
        }
    }
}