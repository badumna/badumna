//-----------------------------------------------------------------------
// <copyright file="Program.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include <QtGui>
#include <QDialog>

#include "Badumna/Core/RuntimeInitializer.h"
#include "MainWindow.h"

using ApiExamples::MainWindow;

int main(int argc, char **)
{
    QApplication app(argc, 0);
    qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));

#ifdef WIN32
    Badumna::BadumnaRuntimeInitializer initializer;
#else
    Badumna::BadumnaRuntimeInitializer2 initializer("mono_config_file");
#endif

    MainWindow window;
    window.Initialize(QObject::tr("Scene Demo 8 - Distributed Controller"));

    return window.exec();
}