//-----------------------------------------------------------------------
// <copyright file="DistributedNPC.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef DISTRIBUTED_NPC_H
#define DISTRIBUTED_NPC_H

#include <vector>
#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/Controller/DistributedSceneController.h"

#include "LocalEntity.h"
#include "ReplicaObject.h"
#include "Follower.h"

namespace ApiExamples
{
    // This class implement the follower NPC.
    class DistributedNPC : public Badumna::DistributedSceneController
    {
    public:
        DistributedNPC(Badumna::NetworkFacade *facade, Badumna::String const &name);
        virtual ~DistributedNPC();

        virtual uint32_t GetEntityType() const;

        virtual Badumna::ISpatialOriginal *TakeControlOfEntity(
            Badumna::BadumnaId const &entity_id, 
            uint32_t entity_type);

        virtual Badumna::ISpatialReplica *InstantiateRemoteEntity(
            Badumna::BadumnaId const &entity_id, 
            uint32_t entity_type);

        virtual void RemoveEntity(Badumna::ISpatialReplica const &replica);
        
        virtual void Checkpoint(Badumna::OutputStream *stream);
        virtual void Recover(Badumna::InputStream *stream);
        virtual void Recover();
        
        virtual void Process(int duration_in_ms);
        virtual void Wake();
        virtual void Sleep();

    private:
        LocalEntity *original_;
        std::vector<Badumna::ISpatialReplica*> replica_objects_;
        Follower *follower_;

        int process_called_;

        // disallow copy and assign
        DistributedNPC(DistributedNPC const &other);
        DistributedNPC &operator=(DistributedNPC const &rhs);
    };
}

#endif // DISTRIBUTED_NPC_H