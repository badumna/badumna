//-----------------------------------------------------------------------
// <copyright file="MainWindow.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <memory>
#include <QDialog>
#include <QtGui>
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/DataTypes/String.h"

namespace ApiExamples
{
    class ChatManager;

    class MainWindow : public QDialog
    {
        Q_OBJECT
    public:
        MainWindow();
        ~MainWindow();
        
        void Initialize(QString title);

        void SetFriendsList(const Badumna::String &text);
        void SetSendToList(std::vector<Badumna::String> &names);
        void AddPrivateChatMessage(const Badumna::String &username, const Badumna::String &message);

        void ShowErrorAndExit(const Badumna::String &error_text);

    public slots:
        void ProcessRegularState();
        void SetPresenceStatus();
        void SendPrivateMessage();

    private:
        void CreatePresenceBox();
        void CreateMessageBox();

        void InitializeGui(QString window_title);
        void InitializeBadumna();
        void LoadFriendsList();
        void InviteFriends();

        std::auto_ptr<Badumna::NetworkFacade> facade_;
        std::auto_ptr<QGraphicsScene> scene_;
        std::auto_ptr<ChatManager> chat_manager_;

        QTimer *scene_timer_;
        QTimer *regular_timer_;

        QGroupBox *presence_box_;
        QComboBox *presence_combo_;
        QPushButton *presence_button_;
        QTextEdit *presence_panel_;

        QGroupBox *message_box_;
        QLineEdit *message_line_;
        QComboBox *message_combo_;
        QPushButton *message_button_;
        QTextEdit *message_panel_;
        QString messages_;

        // disallow copy and assign
        MainWindow(const MainWindow &other);
        MainWindow &operator= (const MainWindow &rhs);
    };
}

#endif // MAIN_WINDOW_H