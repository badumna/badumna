//-----------------------------------------------------------------------
// <copyright file="MainWindow.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include <cassert>
#include <QtGui>
#include "Badumna/Core/NetworkFacade.h"
#include "ChatManager.h"

#include "MainWindow.h"

namespace
{
    using std::auto_ptr;
    using std::vector;
    using Badumna::ChatStatus;
    using Badumna::String;
    using Badumna::NetworkFacade;
    using Badumna::Options;

    QString BStringToQString(String const &str)
    {
#ifdef WIN32
        assert(sizeof(wchar_t) == 2);
        QString msg;
        msg.setUtf16((ushort *)str.CStr(), str.Length());
        return msg;
#else
        assert(sizeof(wchar_t) == 4);
        ushort *tmp = new ushort[str.Length() + 1];
        for(size_t i = 0; i < str.Length(); ++i)
        {
            tmp[i] = (ushort)(str.CStr()[i]);
        }
        tmp[str.Length()] = 0; 

        QString msg;
        msg.setUtf16(tmp, str.Length());
        delete[] tmp;

        return msg;
#endif
    }

    String QStringToBString(QString const &str)
    {
#ifdef WIN32
        assert(sizeof(wchar_t) == 2);
        String msg = (wchar_t *)(str.utf16());
        return msg;
#else
        assert(sizeof(wchar_t) == 4);
        wchar_t *tmp = new wchar_t[str.length() + 1];
        for(int i = 0; i < str.length(); ++i)
        {
            tmp[i] = (wchar_t)(str.utf16()[i]);
        }
        tmp[str.length()] = 0;

        String msg = tmp;
        delete[] tmp;

        return msg;
#endif
    }
}

namespace ApiExamples
{
    MainWindow::MainWindow()
        : facade_()
        , scene_(new QGraphicsScene())
        , chat_manager_()
        , scene_timer_(new QTimer(scene_.get()))
        , regular_timer_(new QTimer(this))
        , presence_box_()
        , presence_combo_()
        , presence_button_()
        , presence_panel_()
        , message_box_()
        , message_line_()
        , message_combo_()
        , message_button_()
        , message_panel_()
        , messages_()
    {
    }

    MainWindow::~MainWindow()
    {
        facade_->Shutdown();
    }

    void MainWindow::Initialize(QString title)
    {
        InitializeGui(title);
        LoadFriendsList();
        InitializeBadumna();
        InviteFriends();
    }

    void MainWindow::SetFriendsList(const String &text)
    {
        presence_panel_->setText(BStringToQString(text));
    }

    void MainWindow::SetSendToList(vector<String> &names)
    {
        for (vector<String>::iterator iter = names.begin(); iter != names.end(); ++iter)
        {
            message_combo_->addItem(BStringToQString(*iter));
        }
    }

    void MainWindow::AddPrivateChatMessage(const String &username, const String &message)
    {
        QString msg = BStringToQString(username) + tr(" : ") + BStringToQString(message);
        messages_ += msg;
        messages_ += tr("\n");
        message_panel_->setText(messages_);
    }

    void MainWindow::ShowErrorAndExit(const String &error_text)
    {
        QMessageBox message_box;
        message_box.setWindowTitle(tr("Error"));
        message_box.setText(BStringToQString(error_text));
        message_box.exec();
        exit(1);
    }

    void MainWindow::ProcessRegularState()
    {
        if ((facade_.get() != NULL) && facade_->IsLoggedIn())
        {
            facade_->ProcessNetworkState();
        }
    }

    void MainWindow::SetPresenceStatus()
    {
        QString statusText = presence_combo_->currentText();
        ChatStatus status = Badumna::ChatStatus_Offline;

        if (statusText == tr("Online"))
        {
            status = Badumna::ChatStatus_Online;
        }
        else if (statusText == tr("Away"))
        {
            status = Badumna::ChatStatus_Away;
        }
        else if (statusText == tr("Chat"))
        {
            status = Badumna::ChatStatus_Chat;
        }
        else if (statusText == tr("DoNotDisturb"))
        {
            status = Badumna::ChatStatus_DoNotDisturb;
        }
        else if (statusText == tr("ExtendedAway"))
        {
            status = Badumna::ChatStatus_ExtendedAway;
        }

        chat_manager_->SetPresenceStatus(status);
    }

    void MainWindow::SendPrivateMessage()
    {
        String username = QStringToBString(message_combo_->currentText());
        String message = QStringToBString(message_line_->text());
        chat_manager_->SendPrivateMessage(username, message);
        message_line_->setText(tr(""));

        String my_name = chat_manager_->MyName() + " (to " + username + ")";
        AddPrivateChatMessage(my_name, message);
    }

    void MainWindow::CreatePresenceBox()
    {
        presence_box_ = new QGroupBox();
        presence_combo_ = new QComboBox();
        presence_button_ = new QPushButton(tr("Set"));
        presence_panel_ = new QTextEdit();

        presence_combo_->addItem(tr("Online"));
        presence_combo_->addItem(tr("Away"));
        presence_combo_->addItem(tr("Chat"));
        presence_combo_->addItem(tr("DoNotDisturb"));
        presence_combo_->addItem(tr("ExtendedAway"));

        connect(presence_button_, SIGNAL(clicked()), this, SLOT(SetPresenceStatus()));

        QGridLayout *layout = new QGridLayout;
        layout->addWidget(new QLabel(tr("My Presence:")), 1, 1);
        layout->addWidget(presence_combo_, 1, 2);
        layout->addWidget(presence_button_, 1, 3);
        layout->addWidget(presence_panel_, 2, 1, 1, 3);

        presence_box_->setLayout(layout);
    }

    void MainWindow::CreateMessageBox()
    {
        message_box_ = new QGroupBox();
        message_line_ = new QLineEdit();
        message_combo_ = new QComboBox();
        message_button_ = new QPushButton(tr("Send"));
        message_panel_ = new QTextEdit();

        connect(message_button_, SIGNAL(clicked()), this, SLOT(SendPrivateMessage()));

        QGridLayout *layout = new QGridLayout;
        layout->addWidget(new QLabel(tr("Msg:")), 1, 1);
        layout->addWidget(message_line_, 1, 2);
        layout->addWidget(new QLabel(tr("to:")), 1, 3);
        layout->addWidget(message_combo_, 1, 4);
        layout->addWidget(message_button_, 1, 5);
        layout->addWidget(message_panel_, 2, 1, 1, 5);

        message_box_->setLayout(layout);
    }
    
    void MainWindow::InitializeGui(QString window_title)
    {
        CreatePresenceBox();
        CreateMessageBox();

        scene_->setSceneRect(0, 0, 400, 400);
        scene_->setItemIndexMethod(QGraphicsScene::NoIndex);
    
        QVBoxLayout *main_layout = new QVBoxLayout;
        main_layout->addWidget(presence_box_);
        main_layout->addWidget(message_box_);
        main_layout->setSizeConstraint(QLayout::SetFixedSize);
        setLayout(main_layout);

        connect(scene_timer_, SIGNAL(timeout()), scene_.get(), SLOT(advance()));
        scene_timer_->start(1000 / 33);

        connect(regular_timer_, SIGNAL(timeout()), this, SLOT(ProcessRegularState()));
        regular_timer_->start(1000 / 60);

        setWindowTitle(window_title);
    }

    void MainWindow::InitializeBadumna()
    {
        Options options;
        options.GetConnectivityModule().SetStartPortRange(21300);
        options.GetConnectivityModule().SetEndPortRange(21399);
        options.GetConnectivityModule().SetMaxPortsToTry(3);
        options.GetConnectivityModule().EnableBroadcast();
        options.GetConnectivityModule().SetBroadcastPort(21250);
        options.GetConnectivityModule().ConfigureForLan();
        options.GetConnectivityModule().SetApplicationName(L"scene-demo5-privatechat");

        facade_.reset(NetworkFacade::Create(options));

        if (!facade_->Login(chat_manager_->MyName()))
        {
            ShowErrorAndExit("Failed to initialize badumna.");
        }
    }

    void MainWindow::LoadFriendsList()
    {
        String friends_list_file;
        QMessageBox message_box;
        QAbstractButton * button_1 = message_box.addButton(tr("john"), QMessageBox::ActionRole);
        message_box.addButton(tr("mary"), QMessageBox::ActionRole);
        message_box.setWindowTitle(tr("Scene Demo 5 - Private Chat"));
        message_box.setText(tr("Select who you want to be."));
        message_box.exec();

        if (message_box.clickedButton() == button_1)
        {
            friends_list_file = "john.list";
        }
        else
        {
            friends_list_file = "mary.list";
        }

        chat_manager_.reset(new ChatManager(this));
        chat_manager_->Initialize(friends_list_file);
    }

    void MainWindow::InviteFriends()
    {
        chat_manager_->InviteFriends(facade_->ChatSession());
    }
}
