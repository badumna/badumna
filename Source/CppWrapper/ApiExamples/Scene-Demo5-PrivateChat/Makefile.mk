
SCENEDEMO5=Scene-Demo5-PrivateChat

ifeq ($(OS), Windows_NT)
SCENEDEMO5:=$(SCENEDEMO5).exe
endif

SCENEDEMO5_PATH=Scene-Demo5-PrivateChat
SCENEDEMO5_OBJS=$(SCENEDEMO5_PATH)/ChatManager.o \
	      $(SCENEDEMO5_PATH)/Friend.o \
	      $(SCENEDEMO5_PATH)/FriendsList.o \
	      $(SCENEDEMO5_PATH)/Program.o \
		  $(SCENEDEMO5_PATH)/MainWindow.o \
		  $(SCENEDEMO5_PATH)/MainWindow.moc.o

$(SCENEDEMO5): $(SCENEDEMO5_OBJS) | check_libmono_path
	@echo [Linking]: $@
	$(VERBOSE_MODE)$(CXX) $(LDFLAGS) -o $@ $(SCENEDEMO5_OBJS) $(LIBS) $(NATIVE_WINDOWS_APP_FLAG)

DEMOS+=$(SCENEDEMO5)
ALL_OBJS+=$(SCENEDEMO5_OBJS)
INSTALL_FILES+=$(SCENEDEMO5) $(SCENEDEMO5_PATH)/mary.list $(SCENEDEMO5_PATH)/john.list
