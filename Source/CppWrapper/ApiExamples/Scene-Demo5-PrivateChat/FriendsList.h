//-----------------------------------------------------------------------
// <copyright file="FriendsList.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef FRIENDS_LIST_H
#define FRIENDS_LIST_H

#include <map>
#include <vector>

#include "Badumna/Chat/IChatChannel.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/Utils/BasicTypes.h"
#include "Friend.h"

namespace ApiExamples
{
    class FriendsList
    {
    public:
        FriendsList();

        bool LoadFriendsList(const Badumna::String &path);
        bool IsFriend(const Badumna::String &name) const;
        void SetFriendChatStatus(const Badumna::String &name, Badumna::ChatStatus status);
        void SetFriendChatChannel(const Badumna::String &name, Badumna::IChatChannel *channel);
        Friend GetFriend(const Badumna::String &name);
        Badumna::String GetFriendNameByChannel(Badumna::IChatChannel *channel);
        std::vector<Badumna::String> GetFriendNames() const;

    private:
        void AddFriend(Friend f);

        typedef std::map<Badumna::String, Friend> FriendsMap;
        FriendsMap friends_map_;

        DISALLOW_COPY_AND_ASSIGN(FriendsList);
    };
}

#endif // FRIENDS_LIST_H