//-----------------------------------------------------------------------
// <copyright file="ChatManager.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include <fstream>
#include <string>
#include <vector>

#include "ChatManager.h"

namespace ApiExamples
{
    using std::ifstream;
    using std::string;
    using std::vector;
    using Badumna::BadumnaId;
    using Badumna::ChatChannelId;
    using Badumna::ChatInvitationHandler;
    using Badumna::ChatMessageHandler;
    using Badumna::ChatPresenceHandler;
    using Badumna::ChatStatus;
    using Badumna::IChatChannel;
    using Badumna::IChatSession;
    using Badumna::String;

    ChatManager::ChatManager(MainWindow *dialog)
        : dialog_(dialog)
        , session_()
        , friends_list_()
        , my_name_()
    {
    }

    void ChatManager::Initialize(const String &friends_list_file)
    {
        my_name_ = ReadUsername(friends_list_file);

        if (!friends_list_.LoadFriendsList(friends_list_file))
        {
            dialog_->ShowErrorAndExit("Friend list file couldn't be opened.");
        }
    }

    void ChatManager::InviteFriends(IChatSession *session)
    {
        session_ = session;
        session_->OpenPrivateChannels(ChatInvitationHandler(&ChatManager::HandleChannelInvitation, *this));
        session_->ChangePresence(Badumna::ChatStatus_Online);

        vector<String> vec = friends_list_.GetFriendNames();

        for (vector<String>::iterator iter = vec.begin(); iter != vec.end(); ++iter)
        {
            session_->InviteUserToPrivateChannel(*iter);
        }

        UpdateFriendsList();
        UpdateSendToList();
    }

    String ChatManager::MyName() const
    {
        return my_name_;
    }

    void ChatManager::SetPresenceStatus(ChatStatus status)
    {
        session_->ChangePresence(status);
    }

    void ChatManager::SendPrivateMessage(const String &username, const String &message)
    {
        Friend f = friends_list_.GetFriend(username);

        if (f.IsValid() && (f.Name() == username))
        {
            f.Channel()->SendMessage(message);
        }
    }

    String ChatManager::ReadUsername(const String &friends_list_file)
    {
        string line;
        String name;
        ifstream list_file(friends_list_file.UTF8CStr());
    
        if (list_file.is_open())
        {
            if (list_file.good())
            {
                getline(list_file, line);
                name = line.c_str();
                list_file.close();
            }
            else
            {
                list_file.close();
                dialog_->ShowErrorAndExit("Can't read from friend list.");
            }
        }
        else
        {
            dialog_->ShowErrorAndExit("Can't open friend list.");
        }

        return name;
    }

    void ChatManager::HandleChannelInvitation(const ChatChannelId &channel_id, const String &username)
    {
        if (friends_list_.IsFriend(username))
        {
            Friend f = friends_list_.GetFriend(username);
            if(f.IsValid())
            {
                if(f.Channel()->Id() == channel_id)
                {
                    // we already have this channel
                    return;
                }
                f.Unsubscribe();
            }

            ChatMessageHandler messageHandler = ChatMessageHandler(&ChatManager::HandlePrivateMessage, *this);
            ChatPresenceHandler presenceHandler = ChatPresenceHandler(&ChatManager::HandlePresence, *this);
            IChatChannel * channel = session_->AcceptInvitation(channel_id, messageHandler, presenceHandler);
            friends_list_.SetFriendChatChannel(username, channel);
        }
        else
        {
            dialog_->ShowErrorAndExit("Unknown friend.");
        }
    }

    void ChatManager::HandlePrivateMessage(IChatChannel *channel, const BadumnaId &, const String &message)
    {
        String name = friends_list_.GetFriendNameByChannel(channel);

        if (name == "")
        {
            dialog_->ShowErrorAndExit("Unknown friend.");
        }

        UpdatePrivateMessages(name, message);
    }

    void ChatManager::HandlePresence(IChatChannel *, const BadumnaId &, const String &username, ChatStatus status)
    {
        friends_list_.SetFriendChatStatus(username, status);
        UpdateFriendsList();

        if (status == Badumna::ChatStatus_Offline)
        {
            Friend f = friends_list_.GetFriend(username);

            if (f.IsValid())
            {
                f.Unsubscribe();
            }
        }
    }

    void ChatManager::UpdateFriendsList()
    {
        int i = 0;
        String content = "";
        vector<String> vec = friends_list_.GetFriendNames();

        for (vector<String>::iterator iter = vec.begin(); iter != vec.end(); ++iter)
        {
            Friend f = friends_list_.GetFriend(*iter);

            String record = f.Name() + " : ";

            switch (f.Status())
            {
            case Badumna::ChatStatus_Online:
                record += "Online";
                break;

            case Badumna::ChatStatus_Away:
                record += "Away";
                break;

            case Badumna::ChatStatus_Chat:
                record += "Chat";
                break;

            case Badumna::ChatStatus_DoNotDisturb:
                record += "DoNotDisturb";
                break;

            case Badumna::ChatStatus_ExtendedAway:
                record += "ExtendedAway";
                break;

            default:
                record += "Offline";
                break;
            }

            if (i == 0)
            {
                content += record;
            }
            else
            {
                content += "\r\n" + record;
            }

            ++i;
        }

        dialog_->SetFriendsList(content);
    }

    void ChatManager::UpdateSendToList()
    {
        vector<String> vec = friends_list_.GetFriendNames();
        dialog_->SetSendToList(vec);
    }

    void ChatManager::UpdatePrivateMessages(const String &username, const String &message)
    {
        dialog_->AddPrivateChatMessage(username, message);
    }
}