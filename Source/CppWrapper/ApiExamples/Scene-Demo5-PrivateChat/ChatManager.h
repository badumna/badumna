//-----------------------------------------------------------------------
// <copyright file="ChatManager.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef CHAT_MANAGER_H
#define CHAT_MANAGER_H

#include <memory>
#include "Badumna/Chat/ChatChannelId.h"
#include "Badumna/Chat/ChatStatus.h"
#include "Badumna/Chat/IChatChannel.h"
#include "Badumna/Chat/IChatSession.h"
#include "Badumna/DataTypes/BadumnaId.h"
#include "FriendsList.h"
#include "MainWindow.h"

namespace ApiExamples
{
    class ChatManager
    {
    public:
        ChatManager(MainWindow *dialog);

        void Initialize(const Badumna::String &friends_list_file);
        void InviteFriends(Badumna::IChatSession *session);

        Badumna::String MyName() const;

        void SetPresenceStatus(Badumna::ChatStatus status);
        void SendPrivateMessage(const Badumna::String &username, const Badumna::String &message);

    private:
        Badumna::String ReadUsername(const Badumna::String &friends_list_file);

        void HandleChannelInvitation(const Badumna::ChatChannelId &channel, const Badumna::String &username);
        void HandlePrivateMessage(Badumna::IChatChannel *channel, const Badumna::BadumnaId &user_id, const Badumna::String &message);
        void HandlePresence(Badumna::IChatChannel *channel, const Badumna::BadumnaId &user_id, const Badumna::String &username, Badumna::ChatStatus status);

        void UpdateFriendsList();
        void UpdateSendToList();
        void UpdatePrivateMessages(const Badumna::String &username, const Badumna::String &message);

        MainWindow *dialog_;
        Badumna::IChatSession * session_;

        FriendsList friends_list_;
        Badumna::String my_name_;
    };
}

#endif // CHAT_MANAGER_H