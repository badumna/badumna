//-----------------------------------------------------------------------
// <copyright file="Friend.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef FRIEND_H
#define FRIEND_H

#include "Badumna/Chat/IChatChannel.h"
#include "Badumna/Chat/ChatStatus.h"
#include "Badumna/DataTypes/String.h"

namespace ApiExamples
{
    class Friend
    {
    public:
        Friend();
        Friend(Badumna::String const &name);
        Friend(Friend const &other);

        Friend& operator=(Friend const &rhs);

        Badumna::String Name() const;
        void SetName(Badumna::String const &name);

        Badumna::ChatStatus Status() const;
        void SetStatus(Badumna::ChatStatus s);

        Badumna::IChatChannel * Channel();

        void SetChannel(Badumna::IChatChannel * channel);

        bool IsValid() const;
        void Unsubscribe();

    private:
        bool valid_;
        Badumna::String username_;
        Badumna::ChatStatus status_;
        Badumna::IChatChannel * channel_;
    };
}

#endif // FRIEND_H