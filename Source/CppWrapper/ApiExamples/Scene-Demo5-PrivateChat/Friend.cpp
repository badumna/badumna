//-----------------------------------------------------------------------
// <copyright file="Friend.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include "Friend.h"

namespace ApiExamples
{
    using Badumna::IChatChannel;
    using Badumna::ChatStatus;
    using Badumna::String;

    Friend::Friend()
        : valid_(false)
        , username_()
        , status_(Badumna::ChatStatus_Offline)
        , channel_()
    {
    }

    Friend::Friend(String const &name)
        : valid_(false)
        , username_(name)
        , status_(Badumna::ChatStatus_Offline)
        , channel_()
    {
    }

    Friend::Friend(const Friend &other)
        : valid_(other.valid_)
        , username_(other.username_)
        , status_(other.status_)
        , channel_(other.channel_)
    {
    }

    Friend &Friend::operator=(const Friend &rhs)
    {
        if (this != &rhs)
        {
            valid_ = rhs.valid_;
            username_ = rhs.username_;
            status_ = rhs.status_;
            
            if (valid_)
            {
                channel_ = rhs.channel_;
            }
        }

        return *this;
    }

    String Friend::Name() const 
    {
        return username_;
    }

    void Friend::SetName(String const &name)
    {
        username_ = name;
    }

    ChatStatus Friend::Status() const
    {
        return status_;
    }

    void Friend::SetStatus(ChatStatus status)
    {
        status_ = status;
    }

    IChatChannel * Friend::Channel()
    {
        return channel_;
    }

    void Friend::SetChannel(IChatChannel *channel)
    {
        channel_ = channel;
        valid_ = true;
    }

    bool Friend::IsValid() const
    {
        return valid_;
    }

    void Friend::Unsubscribe()
    {
        if(valid_)
        {
            channel_->Unsubscribe();
            channel_ = NULL;
            valid_ = false;
        }
    }
}