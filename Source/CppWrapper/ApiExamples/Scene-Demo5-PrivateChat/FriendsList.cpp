//---------------------------------------------------------------------------------
// <copyright file="FriendsList.cpp" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

#include <fstream>
#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>

#include "Badumna/Chat/ChatChannelId.h"

#include "FriendsList.h"

namespace ApiExamples
{
    using std::cerr;
    using std::ifstream;
    using std::string;
    using Badumna::ChatChannelId;
    using Badumna::IChatChannel;
    using Badumna::ChatStatus;
    using Badumna::String;
    
    FriendsList::FriendsList()
        : friends_map_()
    {
    }

    bool FriendsList::LoadFriendsList(const String &path)
    {
        ifstream friends_list(path.UTF8CStr());

        if (friends_list.is_open())
        {
            string line;
            int line_number = 1;

            while (friends_list.good())
            {
                getline(friends_list, line);

                if ((line.size() > 0) && (line_number != 1))
                {
                    Friend f(line.c_str());
                    AddFriend(f);
                }

                ++line_number;
            }

            friends_list.close();

            return true;
        }
        else
        {
            return false;
        }
    }

    bool FriendsList::IsFriend(const String &name) const
    {
        FriendsMap::const_iterator iter = friends_map_.find(name);

        if (iter != friends_map_.end())
        {
            return true;
        }

        return false;
    }

    void FriendsList::SetFriendChatStatus(const String &name, ChatStatus status)
    {
        FriendsMap::iterator iter = friends_map_.find(name);

        if (iter != friends_map_.end())
        {
            Friend f = friends_map_[name];
            f.SetStatus(status);
            friends_map_[name] = f;
        }
    }

    void FriendsList::SetFriendChatChannel(const String &name, IChatChannel *channel)
    {
        FriendsMap::iterator iter = friends_map_.find(name);

        if (iter != friends_map_.end())
        {
            Friend f = friends_map_[name];
            f.SetChannel(channel);
            friends_map_[name] = f;
        }
    }

    Friend FriendsList::GetFriend(const String &name)
    {
        FriendsMap::iterator iter = friends_map_.find(name);

        if (iter != friends_map_.end())
        {
            Friend f = friends_map_[name];

            return f;
        }

        return Friend();
    }

    String FriendsList::GetFriendNameByChannel(IChatChannel *channel)
    {
        Friend friend_;
        ChatChannelId id = channel->Id();
        for(FriendsMap::iterator iter = friends_map_.begin(); iter != friends_map_.end(); ++iter)
        {
            friend_ = iter->second;
            if(friend_.IsValid() && friend_.Channel()->Id() == id)
            {
                return iter->first;
            }
        }

        return "";
    }

    std::vector<String> FriendsList::GetFriendNames() const
    {
        std::vector<String> vec;
        
        for (FriendsMap::const_iterator iter = friends_map_.begin(); iter != friends_map_.end(); ++iter)
        {
            vec.push_back(iter->first);
        }

        return vec;
    }

    void FriendsList::AddFriend(Friend f)
    {
        FriendsMap::iterator iter = friends_map_.find(f.Name());

        if (iter == friends_map_.end())
        {
            friends_map_[f.Name()] = f;
        }
    }
}