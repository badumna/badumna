# compiler and compiler options
CXX=g++
CXX_OPTIONS=-Wall -Wextra

###############################################################################
# guess the OS type
###############################################################################
UNAME:=$(shell uname)
ifneq (,$(findstring MINGW32, $(UNAME)))
OS=Windows_NT
OSTYPE=mingw
endif

ifneq (,$(findstring Linux, $(UNAME)))
OS=Linux
OSTYPE=Linux
endif

###############################################################################
# whether to use static or shared badumna cpp library
###############################################################################
# libbadumna and libmono locations
BADUMNA_LIB_PATH=$(BADUMNA_CPP)/Build/lib

BADUMNA_CPP_LIB_DLL_NAME=$(BADUMNA_LIB_PATH)/libbadumna-cpp.dll
BADUMNA_CPP_LIB=-lbadumna-cpp

###############################################################################
# release or debug build
###############################################################################
ifdef release
CXX_OPTIONS+=-O3
else
CXX_OPTIONS+=-O0 -g -DNDEBUG -DDEBUG
endif

CLOUD_OPTIONS=
CXX_OPTIONS+=$(CLOUD_OPTIONS)

###############################################################################
# verbose build
###############################################################################
ifdef verbose
VERBOSE_MODE=
else
VERBOSE_MODE=@
endif

###############################################################################
# QT_PATH is the location where the Qt library is installed.
# e.g. when using MinGW and Qt 4.7.3, it should be set to
# /C/QtSDK/Desktop/Qt/4.7.3/mingw when the QtSDK is installed to the default
# location on drive C:. 
###############################################################################
# uncomment this and set it to the 
# QT_PATH=

###############################################################################
# MONO_2_PATH should point to the location where mono-2 is installed. 
###############################################################################
# uncomment the following line, set it to the location of the mono 2 library
# MONO_2_PATH=

ifeq ($(OS), Windows_NT)
QT_PATH?=/C/QtSDK/Desktop/Qt/4.7.3/mingw
MOC_BIN:=$(QT_PATH)/bin/moc
DEL=rm -f
RD=rm -rf
MONO_2_PATH=$(BADUMNA_CPP)/libs/Mono-2.8.1
LIBMONO_PATH=$(MONO_2_PATH)
LIBMONO_LDFLAGS=-L$(LIBMONO_PATH) -lmono
COPY=cp
DIRCOPY=cp -rf
NATIVE_WINDOWS_APP_FLAG=-mwindows
endif

ifeq ($(OS), Linux)
MOC_BIN=/usr/bin/moc
QT_PATH=/usr/include/qt4
DEL=rm -f
RD=rm -rf
ifeq ($(MONO_2_PATH),)
LIBMONO_LDFLAGS=$(shell pkg-config --libs mono-2)
else
LIBMONO_PATH=$(MONO_2_PATH)/lib
LIBMONO_LDFLAGS=-L$(LIBMONO_PATH) -lmono-2.0
endif
COPY=cp
DIRCOPY=cp -rf
NATIVE_WINDOWS_APP_FLAG=
endif

include AssemblyRefs.scalify-internal.mk

# installation related
MONO_INSTALLATION_DIR=mono
MONO_DLL=$(MONO_2_PATH)/mono-2.0.dll

# CXXFLAGS and LDFLAGS
ifeq ($(OS), Windows_NT)
CXXFLAGS=$(CXX_OPTIONS) -I. -I$(BADUMNA_CPP) -I$(QT_PATH)/include \
	 -I$(QT_PATH)/include/QtGUI
LIBS=-L$(BADUMNA_LIB_PATH) $(BADUMNA_CPP_LIB) $(LIBMONO_LDFLAGS) -L$(QT_PATH)/lib \
     -lQtGui4 -lQtCore4 -lQtMain
endif

ifeq ($(OS), Linux)
CXXFLAGS=$(CXX_OPTIONS) -I. -I$(BADUMNA_CPP) -I$(QT_PATH) -I$(QT_PATH)/QtGui
LIBS=-L$(BADUMNA_LIB_PATH) $(BADUMNA_CPP_LIB) $(LIBMONO_LDFLAGS) -lQtGui -lQtCore
endif

INSTALL_DIR=bin

%.o: %.cpp
	@echo [Compiling]: $<
	$(VERBOSE_MODE)$(CXX) $(CXXFLAGS) -o $@ -c $<

%.moc.cpp: %.h
	@echo [Mocing files]: $@
	$(VERBOSE_MODE)mkdir -p $(basename $@)
	$(VERBOSE_MODE)$(MOC_BIN) $< -o $@


.DEFAULT_GOAL := all

DEMOS=
ALL_OBJS=
INSTALL_FILES=

include $(wildcard */Makefile.mk)

.PHONY: all
all: $(DEMOS)


.PHONY: install
install: $(DEMOS) | check_ostype
	@echo [Installing]:...
	$(VERBOSE_MODE)mkdir -p "$(INSTALL_DIR)"
	$(VERBOSE_MODE)$(COPY) $(INSTALL_FILES) "$(INSTALL_DIR)"
	$(VERBOSE_MODE)$(COPY) "$(MONO_DLL)" "$(INSTALL_DIR)"
	$(VERBOSE_MODE)$(COPY) "$(BADUMNA_DLL)" "$(INSTALL_DIR)"
	$(VERBOSE_MODE)$(COPY) "$(BADUMNA_LIB_PATH)"/* "$(INSTALL_DIR)"
	-$(VERBOSE_MODE)$(COPY) "$(DEI_DLL)" "$(INSTALL_DIR)"
	$(VERBOSE_MODE)$(DIRCOPY) "$(MONO_2_PATH)" "$(INSTALL_DIR)/mono"
	$(VERBOSE_MODE)$(COPY) "$(QT_PATH)/lib/QtGui4.dll" "$(INSTALL_DIR)"
	$(VERBOSE_MODE)$(COPY) "$(QT_PATH)/lib/QtCore4.dll" "$(INSTALL_DIR)"

.PHONY: check_ostype
check_ostype:
	@ if [ "$(OSTYPE)" != "mingw" ]; then \
		echo "The install target is only supported on windows/mingw."; \
		exit 1; \
	fi

.PHONY: check_libmono_path
check_libmono_path:
	@ if [ "$(LIBMONO_LDFLAGS)" = "" ]; then \
		echo "MONO_2_PATH is unknown."; \
		exit 1; \
	fi

.PHONY: clean
clean:
	$(VERBOSE_MODE)$(DEL) $(DEMOS) $(ALL_OBJS)


.PHONY: printvars
printvars:
	@echo OS: $(OS)
	@echo OSTYPE: $(OSTYPE)
	@echo QT_PATH: $(QT_PATH)
	@echo MOC_BIN: $(MOC_BIN)
	@echo CXXFLAGS: $(CXXFLAGS)
	@echo LIBS: $(LIBS)
	@echo DEMOS: $(DEMOS)
