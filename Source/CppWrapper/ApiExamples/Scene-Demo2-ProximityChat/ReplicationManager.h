//-----------------------------------------------------------------------
// <copyright file="ReplicationManager.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef REPLICATION_MANAGER_H
#define REPLICATION_MANAGER_H

#include <memory>
#include <vector>

#include <QtGui>

#include "Badumna/Chat/IChatSession.h"
#include "Badumna/Chat/IChatChannel.h"
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/Replication/NetworkScene.h"
#include "Badumna/Replication/ISpatialOriginal.h"
#include "Badumna/Replication/ISpatialReplica.h"
#include "Badumna/Utils/BasicTypes.h"

#include "LocalEntity.h"
#include "ReplicaObject.h"
#include "SimpleGraphicsScene.h"
#include "MainWindow.h"

namespace ApiExamples
{
    class ReplicationManager
    {
    public:
        ReplicationManager(MainWindow *dialog, Badumna::NetworkFacade *facade, LocalEntity *entity);
        
        void Shutdown();

        // replication delegates
        Badumna::ISpatialReplica *CreateSpatialReplica(
            Badumna::NetworkScene const &scene,
            Badumna::BadumnaId const &id,
            uint32_t type);
        
        void RemoveSpatialReplica(Badumna::NetworkScene const &scene, Badumna::ISpatialReplica const &replica);

        // proximity chat
        void ProximityChatMessageHandler(
            Badumna::IChatChannel * channel,
            Badumna::BadumnaId const &user_id,
            Badumna::String const &message);

        void SendProximityChatMessage(QString message);
    
    private:
        void Initialize();

        MainWindow *dialog_;
        Badumna::NetworkFacade *facade_;
        Badumna::IChatSession *chat_session_;
        Badumna::IChatChannel * proximity_chat_channel_;

        LocalEntity *local_entity_;
        std::vector<ReplicaObject*> replica_objects_;
        std::auto_ptr<Badumna::NetworkScene> scene_;

        SimpleGraphicsScene *graphics_scene_;

        // disallow copy and assign
        ReplicationManager(ReplicationManager const &other);
        ReplicationManager &operator=(ReplicationManager const &rhs);
    };
}

#endif // REPLICATION_MANAGER_H