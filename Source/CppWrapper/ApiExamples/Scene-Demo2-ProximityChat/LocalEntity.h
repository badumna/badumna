//-----------------------------------------------------------------------
// <copyright file="LocalEntity.h" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#ifndef LOCAL_ENTITY_H
#define LOCAL_ENTITY_H

#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/DataTypes/Vector3.h"
#include "Badumna/Replication/ISpatialOriginal.h"
#include "Badumna/Replication/SpatialEntityStateSegment.h"

#include "StateSegment.h"
#include "LocalAvatar.h"

namespace ApiExamples
{
    class LocalEntity : public LocalAvatar, public Badumna::ISpatialOriginal
    {
    public:
        explicit LocalEntity(Badumna::NetworkFacade *facade);

        void MoveRight();
        void MoveLeft();
        void MoveUp();
        void MoveDown();

        void SetRandomColor();

        //
        // IEntity properties
        // 
        Badumna::BadumnaId Guid() const;
        void SetGuid(Badumna::BadumnaId const &id);
        void HandleEvent(Badumna::InputStream *stream);

        //
        // ISpatialEntity properties
        //
        Badumna::Vector3 Position() const;
        void SetPosition(Badumna::Vector3 const &position);
        void SetPosition(qreal x, qreal y);
        float Radius() const;
        void SetRadius(float val);
        float AreaOfInterestRadius() const;
        void SetAreaOfInterestRadius(float val);

        //
        // ISpatialOriginal properties
        //
        void Serialize(Badumna::BooleanArray const &required_parts, Badumna::OutputStream *stream);

    protected:
         void advance(int step);

    private:
        Badumna::NetworkFacade *facade_;

        Badumna::BadumnaId guid_;
        Badumna::Vector3 position_;

        float radius_;
        float area_of_interest_radius_;

        static const int move_unit = 2;

        // disallow copy and assign
        LocalEntity(LocalEntity const &other);
        LocalEntity& operator=(LocalEntity const &rhs);
    };
}

#endif // LOCAL_ENTITY_H