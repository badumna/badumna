
SCENEDEMO2=Scene-Demo2-ProximityChat

ifeq ($(OS), Windows_NT)
SCENEDEMO2:=$(SCENEDEMO2).exe
endif

SCENEDEMO2_PATH=Scene-Demo2-ProximityChat
SCENEDEMO2_OBJS=$(SCENEDEMO2_PATH)/Avatar.o \
	      $(SCENEDEMO2_PATH)/LocalEntity.o \
	      $(SCENEDEMO2_PATH)/MainWindow.o \
	      $(SCENEDEMO2_PATH)/Program.o \
	      $(SCENEDEMO2_PATH)/Conversion.o \
	      $(SCENEDEMO2_PATH)/ReplicaObject.o \
	      $(SCENEDEMO2_PATH)/ReplicationManager.o \
	      $(SCENEDEMO2_PATH)/SimpleGraphicsScene.o \
	      $(SCENEDEMO2_PATH)/MainWindow.moc.o

$(SCENEDEMO2): $(SCENEDEMO2_OBJS) | check_libmono_path
	@echo [Linking]: $@
	$(VERBOSE_MODE)$(CXX) $(LDFLAGS) -o $@ $(SCENEDEMO2_OBJS) $(LIBS) $(NATIVE_WINDOWS_APP_FLAG)

DEMOS+=$(SCENEDEMO2)
ALL_OBJS+=$(SCENEDEMO2_OBJS)
INSTALL_FILES+=$(SCENEDEMO2)
