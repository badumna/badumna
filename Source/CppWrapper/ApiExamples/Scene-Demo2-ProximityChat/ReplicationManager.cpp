//-----------------------------------------------------------------------
// <copyright file="ReplicationManager.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include <cassert>
#include <iostream>
#include <string>
#include <algorithm>

#include "Badumna/DataTypes/Vector3.h"
#include "Badumna/DataTypes/String.h"
#include "Badumna/DataTypes/BadumnaId.h"
#include "ReplicationManager.h"
#include "EntityType.h"

namespace
{
    using Badumna::String;

    String QStringToBString(QString const &str)
    {
#ifdef WIN32
        assert(sizeof(wchar_t) == 2);
        String msg = (wchar_t *)(str.utf16());
        return msg;
#else
        assert(sizeof(wchar_t) == 4);
        wchar_t *tmp = new wchar_t[str.length() + 1];
        for(int i = 0; i < str.length(); ++i)
        {
            tmp[i] = (wchar_t)(str.utf16()[i]);
        }
        tmp[str.length()] = 0;

        String msg = tmp;
        delete[] tmp;

        return msg;
#endif
    }
}

namespace ApiExamples
{
    using Badumna::Vector3;
    using Badumna::String;
    using Badumna::BadumnaId;
    using Badumna::IChatChannel;
    using Badumna::NetworkFacade;
    using Badumna::NetworkScene;
    using Badumna::ISpatialReplica;
    using Badumna::ChatMessageHandler;
    using Badumna::CreateSpatialReplicaDelegate;
    using Badumna::RemoveSpatialReplicaDelegate;

    struct GuidEqualityComparable : public std::binary_function<ISpatialReplica *, BadumnaId, bool>
    {
        bool operator()(ISpatialReplica *replica, BadumnaId const &id) const
        {
            return replica->Guid() == id;
        }
    };

    ReplicationManager::ReplicationManager(
        MainWindow *dialog, 
        NetworkFacade *network_facade, 
        LocalEntity *entity)
        : dialog_(dialog), 
        facade_(network_facade),
        chat_session_(facade_->ChatSession()),
        local_entity_(entity),
        replica_objects_(),
        scene_(NULL),
        graphics_scene_(dialog_->GetGraphicScene())
    {
        Initialize();
    }

    void ReplicationManager::Shutdown()
    {
        if(scene_.get() != NULL)
        {
            scene_->UnregisterEntity(*local_entity_);
            scene_->Leave();
        }
    }

    void ReplicationManager::Initialize()
    {
        scene_.reset(facade_->JoinScene(L"demo2_scene",
            CreateSpatialReplicaDelegate(&ReplicationManager::CreateSpatialReplica, *this),
            RemoveSpatialReplicaDelegate(&ReplicationManager::RemoveSpatialReplica, *this)));

		float aoi = 50.0f;
		float max_speed = 20.0f;
		facade_->RegisterEntityDetails(aoi, max_speed);

        Vector3 initial_position(30.0f, 30.0f, 0.0f);
        
        local_entity_->SetPosition(initial_position);
        local_entity_->SetRadius(10.0f);
        local_entity_->SetAreaOfInterestRadius(aoi);

        scene_->RegisterEntity(local_entity_, NormalEntity);
        local_entity_->SetPosition(initial_position);

        // Subscribe to the proximity chat channel
        // the ReplicationManager::ProximityChatMessageHandler method will be called when there is incoming proximity
        // chat message. 
        ChatMessageHandler handler(&ReplicationManager::ProximityChatMessageHandler, *this);
        proximity_chat_channel_ = chat_session_->SubscribeToProximityChannel(local_entity_->Guid(), handler);
    }

    ISpatialReplica *ReplicationManager::CreateSpatialReplica(
        NetworkScene const &, 
        BadumnaId const &id, 
        uint32_t)
    {
        std::vector<ReplicaObject*>::iterator iter; 
        iter = std::find_if(
            replica_objects_.begin(), 
            replica_objects_.end(), 
            std::bind2nd(GuidEqualityComparable(), id));
        if(iter != replica_objects_.end())
        {
            return *iter;
        }
        else
        {
            ReplicaObject* replica = new ReplicaObject();
            replica->SetGuid(id);
            replica_objects_.push_back(replica);

            graphics_scene_->RegisterReplicaEntity(replica);
            return replica;
        }
    }
    
    void ReplicationManager::RemoveSpatialReplica(NetworkScene const &, ISpatialReplica const &replica)
    {
        std::vector<ReplicaObject*>::iterator iter;
        iter = std::find_if(
            replica_objects_.begin(), 
            replica_objects_.end(), 
            std::bind2nd(GuidEqualityComparable(), replica.Guid()));
        if(iter != replica_objects_.end())
        {
            ReplicaObject* to_remove = *iter;
            graphics_scene_->RemoveReplicaEntity(to_remove);
            replica_objects_.erase(iter);
            delete to_remove;
        }
    }

    void ReplicationManager::ProximityChatMessageHandler(
        IChatChannel *,
        BadumnaId const &,
        String const &message)
    {
        dialog_->AddProximityChatMessage(message);
    }

    void ReplicationManager::SendProximityChatMessage(QString message)
    {
        Badumna::String message_to_send = ::QStringToBString(message);

        if(message_to_send.Length() > 0)
        {
            // Send a proximity chat message to all nearby entities.
            proximity_chat_channel_->SendMessage(message_to_send);
        }
    }
}