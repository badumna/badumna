//-----------------------------------------------------------------------
// <copyright file="MainWindow.cpp" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#include <cassert>
#include <QtGui>
#include "Badumna/Core/NetworkFacade.h"

#include "MainWindow.h"
#include "LocalEntity.h"
#include "SimpleGraphicsScene.h"
#include "ReplicationManager.h"
#include "Conversion.h"

namespace ApiExamples
{
    using Badumna::Options;
    using Badumna::NetworkFacade;
    using Badumna::String;

    MainWindow::MainWindow()
        : facade_(NULL),
        local_entity_(NULL),
        scene_(new SimpleGraphicsScene),
        replication_manager_(NULL),
        view_(new QGraphicsView(scene_.get())),
        scene_timer_(new QTimer(scene_.get())),
        regular_timer_(new QTimer(this))
    {
    }

    MainWindow::~MainWindow()
    {
        replication_manager_->Shutdown();
        scene_->RemoveLocalEntity(local_entity_.get());

        facade_->Shutdown();
    }

    void MainWindow::Initialize(QString title)
    {
        InitializeGui(title);
        InitializeBadumna();
    }

    void MainWindow::CreateProximityMessageBox()
    {
        proximity_message_box_ = new QGroupBox(tr("Proximity Chat"));
        proximity_message_edit_ = new QLineEdit();

        QHBoxLayout *layout = new QHBoxLayout;
        layout->addWidget(proximity_message_edit_);

        send_proximity_message_button_ = new QPushButton(tr("Send"));
        connect(send_proximity_message_button_, SIGNAL(clicked()), this, SLOT(SendProximityMessage()));
        layout->addWidget(send_proximity_message_button_);
        
        proximity_message_box_->setLayout(layout);
    }

    void MainWindow::CreateMessagePanelBox()
    {
        message_box_ = new QGroupBox(tr("Messages"));
        message_panel_ = new QTextEdit();

        QHBoxLayout *layout = new QHBoxLayout();
        layout->addWidget(message_panel_);

        message_box_->setLayout(layout);
    }

    void MainWindow::InitializeBadumna()
    {
#ifdef CLOUD
        // NOTE: Hardcode your app id here to save entering it every time.
        Badumna::String app_id("");
        if (app_id.Length() == 0)
        {
            bool ok;
            QString q_app_id = QInputDialog::getText(
                this,
                "Enter app id",
                "Create your app at cloud.badumna.com then enter your app id here:",
                QLineEdit::Normal,
                QString(),
                &ok);

            if (!ok)
            {
                exit(1);
            }

            app_id = QStringToBString(q_app_id);
        }

        // Create the facade
        facade_.reset(NetworkFacade::Create(app_id));
#else
        Options options;
        options.GetConnectivityModule().SetStartPortRange(21300);
        options.GetConnectivityModule().SetEndPortRange(21399);
        options.GetConnectivityModule().SetMaxPortsToTry(3);
        options.GetConnectivityModule().EnableBroadcast();
        options.GetConnectivityModule().SetBroadcastPort(21250);
        options.GetConnectivityModule().ConfigureForLan();
        options.GetConnectivityModule().SetApplicationName(L"scene-demo2-proximitychat");

        facade_.reset(NetworkFacade::Create(options));
#endif // CLOUD

        bool succeed = facade_->Login("default username");
        if(!succeed)
        {
            QMessageBox message_box;
            message_box.setWindowTitle("Error");
            message_box.setText("Failed to initialize badumna.");
            message_box.exec();
            exit(1);
        }

        local_entity_.reset(new LocalEntity(facade_.get()));
        replication_manager_.reset(new ReplicationManager(this, facade_.get(), local_entity_.get()));
        
        scene_->RegisterLocalEntity(local_entity_.get());
    }
    
    void MainWindow::InitializeGui(QString window_title)
    {
        CreateProximityMessageBox();
        CreateMessagePanelBox();

        scene_->setSceneRect(0, 0, 400, 400);
        scene_->setItemIndexMethod(QGraphicsScene::NoIndex);
    
        QVBoxLayout *main_layout = new QVBoxLayout;
        main_layout->addWidget(view_);
        main_layout->addWidget(proximity_message_box_);
        main_layout->addWidget(message_box_);
        main_layout->setSizeConstraint(QLayout::SetFixedSize);
        setLayout(main_layout);

        view_->setCacheMode(QGraphicsView::CacheBackground);
        view_->setRenderHint(QPainter::Antialiasing);
        view_->setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);
        view_->setWindowTitle(QT_TRANSLATE_NOOP(QGraphicsView, "Scene Demo 2 - Proximity Chat"));
        view_->resize(400, 400);
        view_->setFixedWidth(400);
        view_->setFixedHeight(400);
        view_->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        view_->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        view_->setWindowFlags( (view_->windowFlags() | Qt::CustomizeWindowHint) & ~Qt::WindowMaximizeButtonHint); 
        view_->show();

        QObject::connect(scene_timer_, SIGNAL(timeout()), scene_.get(), SLOT(advance()));
        scene_timer_->start(1000 / 33);

        QObject::connect(regular_timer_, SIGNAL(timeout()), this, SLOT(ProcessRegularState()));
        regular_timer_->start(1000 / 60);

        setWindowTitle(window_title);
    }

    void MainWindow::AddProximityChatMessage(String const &incoming_message)
    {
        QString msg = ::BStringToQString(incoming_message); 
        messages_ += msg;
        messages_ += tr("\n");
    
        message_panel_->setText(messages_);
    }

    void MainWindow::SendProximityMessage()
    {
        QString message = proximity_message_edit_->text();
        replication_manager_->SendProximityChatMessage(message);
		proximity_message_edit_->setText("");
    }

    void MainWindow::ProcessRegularState()
    {
        if(facade_.get() != NULL && facade_->IsLoggedIn())
        {
            facade_->ProcessNetworkState();
        }
    }
}