﻿//---------------------------------------------------------------------------------
// <copyright file="KnownPeersManager.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace SeedPeer
{
    using System.Collections.Generic;
    using System.IO;
    using System.Net;

    /// <summary>
    /// The known peer manager class is used to save and load known peers list file.
    /// </summary>
    internal class KnownPeersManager
    {
        /// <summary>
        /// Saves the known peer details into a file.
        /// </summary>
        /// <param name="peers">The peers.</param>
        /// <param name="fullPath">The full path of the file.</param>
        /// <returns>Whether successful.</returns>
        public static bool SaveKnownPeers(List<IPEndPoint> peers, string fullPath)
        {
            try
            {
                using (FileStream stream = new FileStream(fullPath, FileMode.Create, FileAccess.Write))
                {
                    using (StreamWriter writer = new StreamWriter(stream))
                    {
                        foreach (IPEndPoint ep in peers)
                        {
                            writer.Write(ep.Address);
                            writer.Write(":");
                            writer.Write(ep.Port);
                            writer.Write("\r\n");
                        }

                        writer.Flush();
                        return true;
                    }
                }
            }
            catch
            {
            }

            return false;
        }

        /// <summary>
        /// Loads the known peers from a file.
        /// </summary>
        /// <param name="fullPath">The full path of the known peers file.</param>
        /// <returns>A list of IPEndPoints</returns>
        public static List<IPEndPoint> LoadKnownPeers(string fullPath)
        {
            List<IPEndPoint> peers = new List<IPEndPoint>();

            try
            {
                using (FileStream stream = new FileStream(fullPath, FileMode.Open, FileAccess.Read))
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        string currentLine;
                        while ((currentLine = reader.ReadLine()) != null && peers.Count <= 32)
                        {
                            string trimmed = currentLine.Trim();
                            string[] parts = trimmed.Split(':');
                            if (parts != null && parts.Length == 2)
                            {
                                try
                                {
                                    IPEndPoint ep = new IPEndPoint(IPAddress.Parse(parts[0]), int.Parse(parts[1]));
                                    peers.Add(ep);
                                }
                                catch
                                {
                                }
                            }
                        }
                    }
                }
            }
            catch
            {
            }

            return peers;
        }
    }
}
