//---------------------------------------------------------------------------------
// <copyright file="Program.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
// <summary>Main class for seed peer.</summary>
//---------------------------------------------------------------------------------

namespace SeedPeer
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Text;
    using Badumna;
    using Badumna.Core;
    using Badumna.Utilities;
    using GermHarness;
    using Mono.Options;

    /// <summary>
    /// Seed Peer should be the first seed peer running on the badumna network and
    /// it should have an open or full cone connection (i.e. it should run on the
    /// machine that has a public ip address). SeedPeer can be started manually or
    /// from the control center, and can be started with and without the dei. 
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Option set for parsing command line arguments.
        /// </summary>
        private readonly IOptionSet optionSet;

        /// <summary>
        /// The peer harness for hosting the seed peer process, to allow control
        /// center control.
        /// </summary>
        private readonly IPeerHarness peerHarness;

        /// <summary>
        /// Writer for writing messages to the console.
        /// </summary>
        private readonly IConsoleWriter consoleWriter;

        /// <summary>
        /// Flag indicating whether an existing network should be rejoined.
        /// </summary>
        private bool rejoin = false;

        /// <summary>
        /// Initializes a new instance of the Program class.
        /// </summary>
        public Program()
            : this(
            new OptionSet(),
            o => new PeerHarness(o),
            new ConsoleWriter())
        {
        }

        /// <summary>
        /// Initializes a new instance of the Program class.
        /// </summary>
        /// <param name="optionSet">OptionSet for parsing command line arguments.</param>
        /// <param name="peerHarnessFactory">Factory for creating PeerHarness.</param>
        /// <param name="consoleWriter">A writer for outputing messages to the console.</param>
        public Program(
            IOptionSet optionSet,
            PeerHarnessFactory peerHarnessFactory,
            IConsoleWriter consoleWriter)
        {
            if (optionSet == null)
            {
                throw new ArgumentNullException("optionSet");
            }

            if (peerHarnessFactory == null)
            {
                throw new ArgumentNullException("peerHarnessFactory");
            }

            if (consoleWriter == null)
            {
                throw new ArgumentNullException("consoleWriter");
            }

            this.optionSet = optionSet;
            this.peerHarness = peerHarnessFactory(Program.GetDefaultOptions());
            this.consoleWriter = consoleWriter;

            this.AddCommandLineOptions();
        }

        /// <summary>
        /// SeedPeer main function.
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        internal void Run(string[] args)
        {
            // Parse BaseHarness specific command line arguments.
            List<string> extras = null;
            try
            {
                extras = this.optionSet.Parse(args);

                // if rejoin is specified, then read the known peer list and add them as initial peers.
                if (this.rejoin)
                {
                    List<IPEndPoint> knownPeers = KnownPeersManager.LoadKnownPeers("seedpeer_known_peers.txt");
                    if (knownPeers != null && knownPeers.Count > 0)
                    {
                        this.peerHarness.AddInitialPeers(knownPeers);
                    }
                }

                string[] harnessArgs = extras.ToArray();
                this.peerHarness.Initialize(ref harnessArgs, new SeedPeerHostedProcess(this.peerHarness));
                if (harnessArgs.Length > 0)
                {
                    StringBuilder errorMessage = new StringBuilder("Unknown argument(s):");
                    foreach (string arg in harnessArgs)
                    {
                        errorMessage.AppendFormat(" {0}", arg);
                    }

                    this.HandleCommandLineArgumentErrors(errorMessage.ToString());
                }
                else
                {
                    // install the cancel key handler
                    this.consoleWriter.CancelKeyPress += delegate(object sender, ConsoleCancelEventArgs e)
                    {
                        e.Cancel = true;
                        this.peerHarness.Stop();
                    };

                    // start the harness to launch the seed peer, this will not return until the seed peer process is terminated.
                    if (!this.peerHarness.Start())
                    {
                        Console.WriteLine("Seed peer failed to start.");
                        Environment.Exit((int)ExitCode.Others);
                    }
                    else
                    {
                        Console.WriteLine("Seed peer has been stopped.");
                    }
                }
            }
            catch (OptionException ex)
            {
                this.HandleCommandLineArgumentErrors(
                    string.Format("Error parsing option {0}: {1}", ex.OptionName, ex.Message));
            }
            catch (DeiConfigException ex)
            {
                this.HandleCommandLineArgumentErrors(
                    string.Format("Error in Dei config: {0}", ex.Message));
            }
            catch (DeiAuthenticationException ex)
            {
                this.consoleWriter.WriteLine(ex.Message);
                Environment.Exit((int)ExitCode.DeiAuthenticationFailed);
            }
            catch (ConfigurationException ex)
            {
                this.consoleWriter.WriteLine(ex.Message);
                Environment.Exit((int)ExitCode.ConfigurationError);
            }
        }

        /// <summary>
        /// Entry point for program.
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        private static void Main(string[] args)
        {
            new Program().Run(args);
        }

        /// <summary>
        /// Gets an options object with default SeedPeer configuration.
        /// </summary>
        /// <returns>Default options.</returns>
        private static Options GetDefaultOptions()
        {
            Options options = new Options();

            options.Connectivity.ConfigureForSpecificPort(21251);
            options.Connectivity.IsHostedService = true;

            return options;
        }

        /// <summary>
        /// Add options for processing command line arguments.`
        /// </summary>
        private void AddCommandLineOptions()
        {
            this.optionSet.Add<string>(
                "r|rejoin",
                "rejoin an existing Badumna network.",
                v =>
                {
                    if (v != null)
                    {
                        this.rejoin = true;
                    }
                });
            this.optionSet.Add<string>(
                "h|help",
                "show this message and exit.",
                v =>
                {
                    if (v != null)
                    {
                        this.WriteUsageAndExit();
                    }
                });
        }

        /// <summary>
        /// Handle errors encountered during processing of command line options.
        /// </summary>
        /// <param name="message">A message describing the error.</param>
        private void HandleCommandLineArgumentErrors(string message)
        {
            this.consoleWriter.WriteLine("SeedPeer: {0}", message);
            this.consoleWriter.WriteLine("Try `SeedPeer --help' for more information.");
            Environment.Exit((int)ExitCode.InvalidArguments);
        }

        /// <summary>
        /// Write usage instructions and exit.
        /// </summary>
        private void WriteUsageAndExit()
        {
            this.consoleWriter.WriteLine("Usage: SeedPeer [Options]");
            this.consoleWriter.WriteLine("Start a SeedPeer on the local machine.");
            this.consoleWriter.WriteLine(string.Empty);
            this.consoleWriter.WriteLine("Options:");
            this.peerHarness.WriteOptionDescriptions(this.consoleWriter.Out);
            this.optionSet.WriteOptionDescriptions(this.consoleWriter.Out);
            Environment.Exit((int)ExitCode.DisplayHelp);
        }
    }
}
