﻿//---------------------------------------------------------------------------------
// <copyright file="SeedPeerHostedProcess.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace SeedPeer
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using Badumna;
    using GermHarness;

    /// <summary>
    /// The regular task processed by the seed peer.
    /// </summary>
    internal class SeedPeerHostedProcess : IHostedProcess
    {
        /// <summary>
        /// The harness hosting this process.
        /// </summary>
        private readonly IPeerHarness peerHarness;

        /// <summary>
        /// The total delay.
        /// </summary>
        private long totalDelayMilliseconds = 0;

        /// <summary>
        /// Initializes a new instance of the SeedPeerHostedProcess class.
        /// </summary>
        /// <param name="peerHarness">The harness hosting this process.</param>
        public SeedPeerHostedProcess(IPeerHarness peerHarness)
        {
            if (peerHarness == null)
            {
                throw new ArgumentNullException("peerHarness");
            }

            this.peerHarness = peerHarness;
        }

        #region IHostedProcess Members

        /// <summary>
        /// Called when the peer harness has started, before any other calls are made.
        /// </summary>
        /// <param name="arguments">An array of arguments that were passed to the process.</param>
        public void OnInitialize(ref string[] arguments)
        {
            return;
        }

        /// <summary>
        /// The peer is shutting down. Called when either the process is about to end
        /// or a remote user has requested the process shutdown.
        /// </summary>
        public void OnShutdown()
        {
            return;
        }

        /// <summary>
        /// Called when the process has received a request to start.
        /// Is called after OnInitialize() and before any other methods in the IPeer interface.
        /// </summary>
        /// <returns>Whether success.</returns>
        public bool OnStart()
        {
            return true;
        }

        /// <summary>
        /// Called at regular intervals (<see cref="Harness.ProcessNetworkStateIntervalMs">ProcessNetworkStateIntervalMs</see>).
        /// </summary>
        /// <param name="delayMilliseconds">The delay since the last call to this method</param>
        /// <returns>
        /// True if the wishes to continue in the run loop. If false the process will shutdown and exit.
        /// </returns>
        public bool OnPerformRegularTasks(int delayMilliseconds)
        {
            this.totalDelayMilliseconds += delayMilliseconds;

            // dump open peer connections every 60 seconds.
            if (this.totalDelayMilliseconds >= (long)TimeSpan.FromSeconds(60).TotalMilliseconds)
            {
                this.totalDelayMilliseconds = 0;
                this.DumpOpenConnectionAddress();
            }

            return true;
        }

        /// <summary>
        /// Process any remote requests.
        /// </summary>
        /// <param name="requestType">An id for the type of request</param>
        /// <param name="request">Any data associated with the request</param>
        /// <returns>The reply to the request</returns>
        public byte[] OnProcessRequest(int requestType, byte[] request)
        {
            return null;
        }

        /// <summary>
        /// Write list of supported command line arguments.
        /// </summary>
        /// <param name="tw">The text writer to use.</param>
        public void WriteOptionsDescription(TextWriter tw)
        {
            // No process-specific arguments supported.
        }

        #endregion

        /// <summary>
        /// Dumps the open connection address to a text file.
        /// </summary>
        private void DumpOpenConnectionAddress()
        {
            if (this.peerHarness.NetworkFacadeInstance != null)
            {
                if (this.peerHarness.NetworkFacadeInstance.IsLoggedIn)
                {
                    List<IPEndPoint> endpoints = ((INetworkFacadeInternal)this.peerHarness.NetworkFacadeInstance).GetRecentActiveOpenPeers();
                    if (endpoints != null && endpoints.Count > 0)
                    {
                        KnownPeersManager.SaveKnownPeers(endpoints, "seedpeer_known_peers.txt");
                    }
                }
            }
        }
    }
}
