﻿//-----------------------------------------------------------------------
// <copyright file="IDatabaseManager.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Unity.Utilities
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// IDatabaseManager interface.
    /// </summary>
    public interface IDatabaseManager : IDisposable
    {
        /// <summary>
        /// Update the record on database.
        /// </summary>
        /// <param name="userId">Ther user id.</param>
        /// <param name="table">The table name.</param>
        /// <param name="itemNames">Item need to be updated.</param>
        /// <param name="values">The updated value.</param>
        /// <returns>Return true on sucess.</returns>
        bool UpdateRecord(int userId, string table, string[] itemNames, object[] values);

        /// <summary>
        /// Insert record to database.
        /// </summary>
        /// <param name="table">The table name.</param>
        /// <param name="itemNames">Item need to be inserted.</param>
        /// <param name="values">The value of the items.</param>
        /// <returns>Return true on success.</returns>
        bool InsertRecord(string table, string[] itemNames, object[] values);

        /// <summary>
        /// Delete record from database.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="table">The table name.</param>
        /// <returns>Return true on success.</returns>
        bool DeleteRecord(int userId, string table);

        /// <summary>
        /// Get the record from database.
        /// </summary>
        /// <param name="itemName">The item name.</param>
        /// <param name="table">The table name.</param>
        /// <param name="condition">Condition given.</param>
        /// <returns>Return the record if exist.</returns>
        object GetRecord(string itemName, string table, string condition);
    }
}
