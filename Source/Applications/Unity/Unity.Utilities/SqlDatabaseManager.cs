//-----------------------------------------------------------------------
// <copyright file="SqlDatabaseManager.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Unity.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    using MySql.Data.MySqlClient;

    /// <summary>
    /// SqlDatabaseManager class implement IDatabaseManager interface.
    /// </summary>
    public abstract class SqlDatabaseManager : IDatabaseManager
    {
        /// <summary>
        /// IDbConnection instance.
        /// </summary>
        private IDbConnection connection;

        /// <summary>
        /// The key validation period.
        /// <remarks>Do we need this in this case.</remarks>
        /// </summary>
        private TimeSpan keyValidityPeriod; 

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlDatabaseManager"/> class.
        /// </summary>
        /// <param name="keyValidityPeriod">Key validity period.</param>
        public SqlDatabaseManager(TimeSpan keyValidityPeriod)
        {
            this.keyValidityPeriod = keyValidityPeriod;
        }

        /// <summary>
        /// Get the record from database <seealso cref="IDatabaseManager.GetRecord(string, string, string)"/>.
        /// </summary>
        /// <param name="itemName">The item name.</param>
        /// <param name="table">The table name.</param>
        /// <param name="condition">Condition given.</param>
        /// <returns>Return the record if exist.</returns>
        public object GetRecord(string itemName, string table, string condition)
        {
            if (string.IsNullOrEmpty(table) || string.IsNullOrEmpty(condition) || string.IsNullOrEmpty(itemName))
            {
                return null;
            }

            try
            {
                IDbCommand command = this.GetSqlCommand();
                if (command != null)
                {
                    return this.GetRecordQuerry(command, itemName, table, condition);
                }
            }
            catch (Exception e)
            {
                Trace.TraceError("Exception while trying to get record : {0}", e.Message);
                Console.WriteLine("Exception while trying to get record : {0}", e.Message);
            }

            return null;
        }

        /// <summary>
        /// Update record on database <seealso cref="IDatabaseManager.UpdateRecord(int,string,string[],object[])"/>.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="table">The table name.</param>
        /// <param name="itemNames">The item names need to be updated.</param>
        /// <param name="values">The updated value.</param>
        /// <returns>Return true on success.</returns>
        public bool UpdateRecord(int userId, string table, string[] itemNames, object[] values)
        {
            if (string.IsNullOrEmpty(table) || itemNames.Length <= 0 || values.Length <= 0 || userId < 0)
            {
                return false;
            }

            try
            {
                IDbCommand command = this.GetSqlCommand();
                if (command != null)
                {
                    return this.UpdateRecordQuerry(command, userId, table, itemNames, values);
                }
            }
            catch (Exception e)
            {
                Trace.TraceError("Exception whilst trying to update record : {0}", e.Message);
                Console.WriteLine("Exception whilst trying to update record : {0}", e.Message);
            }

            return false;
        }

        /// <summary>
        /// Insert new record to database <seealso cref="IDatabaseManager.InsertRecord(string, string[], object[])"/>.
        /// </summary>
        /// <param name="table">The table name.</param>
        /// <param name="itemNames">The item names need to be inserted.</param>
        /// <param name="values">The value of the item names.</param>
        /// <returns>Return true on success.</returns>
        public bool InsertRecord(string table, string[] itemNames, object[] values)
        {
            if (string.IsNullOrEmpty(table) || itemNames.Length <= 0 || values.Length <= 0)
            {
                return false;
            }

            try
            {
                IDbCommand command = this.GetSqlCommand();
                if (command != null)
                {
                    return this.InsertRecordQuerry(command, table, itemNames, values);
                }
            }
            catch (System.IO.IOException e)
            {
                Trace.TraceError("Exception whilst trying to insert record : {0}", e.Message);
                Console.WriteLine("Exception whilst trying to insert record : {0}", e.Message);
                this.connection.Close();

                //// Note: Is not a good solution
                Console.WriteLine("Recovering from exception whilst trying to insert record");
                Console.WriteLine("Reinsert record to database");
                IDbCommand command = this.GetSqlCommand();
                if (command != null)
                {
                    return this.InsertRecordQuerry(command, table, itemNames, values);
                }
            }
            catch (Exception e)
            {
                Trace.TraceError("Exception whilst trying to insert record : {0}", e.Message);
                Console.WriteLine("Exception whilst trying to insert record : {0}", e.ToString());
                Console.WriteLine("Connection status : {0}", this.connection.State.ToString());
                //// Note: try to close the connection when the exception is occured
                this.connection.Close();
            }

            return false;
        }

        /// <summary>
        /// Delete a record from database <seealso cref="IDatabaseManager.DeleteRecord(int, string)"/>.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="table">The table name.</param>
        /// <returns>Return true on success.</returns>
        public bool DeleteRecord(int userId, string table)
        {
            if (string.IsNullOrEmpty(table) || userId < 0)
            {
                return false;
            }

            try
            {
                IDbCommand command = this.GetSqlCommand();
                if (command != null)
                {
                    return this.DeleteRecordQuerry(command, userId, table);
                }
            }
            catch (Exception e)
            {
                Trace.TraceError("Exception whilst trying to delete record : {0}", e.Message);
            }

            return false;
        }

        /// <summary>
        /// Close the connection when dispose this class.
        /// </summary>
        public void Dispose()
        {
            if (this.connection != null)
            {
                this.connection.Close();
                this.connection = null;
            }
        }

        /// <summary>
        /// Create a database connection, need to be created on the application class that is derived from this class.
        /// It depend on what database provider is used.
        /// </summary>
        /// <returns>Return the database connection instance.</returns>
        protected abstract IDbConnection CreateConnection();

        /// <summary>
        /// Get the record query.
        /// </summary>
        /// <param name="command">Database command.</param>
        /// <param name="itemName">The item name.</param>
        /// <param name="table">The table name.</param>
        /// <param name="condition">The query condition.</param>
        /// <returns>Return the record if exist.</returns>
        protected virtual object GetRecordQuerry(IDbCommand command, string itemName, string table, string condition)
        {
            object record = null;

            //// check the validity of the condition
            //// TODO: is there any better way to do this, should this handle in the caller class
            if (condition.Contains(";"))
            {
                return null; //// most likely this could be a SQL injection
            }

            command.CommandText = string.Format("SELECT {0} FROM {1} WHERE {2}", itemName, table, condition);

            using (IDataReader reader = command.ExecuteReader())
            {
                if (reader.Read())
                {
                    record = reader.GetValue(0);
                }
            }

            return record;
        }

        /// <summary>
        /// Execute the update querry.
        /// </summary>
        /// <param name="command">Database command.</param>
        /// <param name="userId">Ther user id.</param>
        /// <param name="table">The table name.</param>
        /// <param name="itemNames">The item names need to be updated.</param>
        /// <param name="values">Updated value (new value).</param>
        /// <returns>Return true on success.</returns>
        protected virtual bool UpdateRecordQuerry(IDbCommand command, int userId, string table, string[] itemNames, object[] values)
        {
            if (itemNames.Length != values.Length)
            {
                return false;
            }

            string setQuerry = string.Empty;
            for (int i = 0; i < itemNames.Length; i++)
            {
                IDbDataParameter parameter = command.CreateParameter();
                parameter.ParameterName = "@" + itemNames[i];
                parameter.Value = values[i];
                command.Parameters.Add(parameter);
                setQuerry += itemNames[i] + "=" + parameter.ParameterName;
                if (i != (itemNames.Length - 1))
                {
                    setQuerry += ',';
                }
            }

            IDbDataParameter id = command.CreateParameter();
            id.ParameterName = "@id";
            id.Value = userId;
            command.Parameters.Add(id);

            command.CommandText = string.Format(@"UPDATE {0} SET {1} WHERE id={2} ;", table, setQuerry, id.ParameterName);

            int rowsUpdated = command.ExecuteNonQuery();
            return rowsUpdated == 1;
        }

        /// <summary>
        /// Execute insert record querry.
        /// </summary>
        /// <param name="command">Database command.</param>
        /// <param name="table">The table name.</param>
        /// <param name="itemNames">The item names need to be inserted.</param>
        /// <param name="values">The item names value.</param>
        /// <returns>Return true on success.</returns>
        protected virtual bool InsertRecordQuerry(IDbCommand command, string table, string[] itemNames, object[] values)
        {
            string fieldName = string.Empty;
            string fieldValue = string.Empty;
            if (itemNames.Length != values.Length)
            {
                return false;
            }

            for (int i = 0; i < itemNames.Length; i++)
            {
                IDbDataParameter parameter = command.CreateParameter();
                parameter.ParameterName = "@" + itemNames[i];
                parameter.Value = values[i];
                command.Parameters.Add(parameter);

                fieldName += itemNames[i];
                fieldValue += parameter.ParameterName;

                if (i != (itemNames.Length - 1))
                {
                    fieldName += ',';
                    fieldValue += ',';
                }
            }

            command.CommandText = string.Format(@"INSERT INTO {0} ({1}) VALUES({2})", table, fieldName, fieldValue);

            int rowsUpdated = command.ExecuteNonQuery();
            return rowsUpdated == 1;
        }

        /// <summary>
        /// Execute delete record querry.
        /// </summary>
        /// <param name="command">Database command.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="table">The table name.</param>
        /// <returns>Returns true on success.</returns>
        protected virtual bool DeleteRecordQuerry(IDbCommand command, int userId, string table)
        {
            IDbDataParameter id = command.CreateParameter();
            id.ParameterName = "@id";
            id.Value = userId;
            command.Parameters.Add(id);

            command.CommandText = string.Format(@"DELETE FROM '{0}' WHERE id='{1}'", table, id.ParameterName);

            int rowsUpdated = command.ExecuteNonQuery();
            return rowsUpdated == 1;
        }

        /// <summary>
        /// Get the sql commnad
        /// </summary>
        /// <returns>Return the sql command.</returns>
        private IDbCommand GetSqlCommand()
        {
            IDbConnection connection = this.GetConnection();
            if (connection != null)
            {
                IDbCommand queryCommand = connection.CreateCommand();
                return queryCommand;
            }

            Trace.TraceError("Failed to get sql command");
            return null;
        }

        /// <summary>
        /// Get the database connection.
        /// </summary>
        /// <returns>Return the database connection.</returns>
        private IDbConnection GetConnection()
        {
            if (this.connection != null && !(this.connection.State == ConnectionState.Closed || this.connection.State == ConnectionState.Broken))
            {
                return this.connection;
            }

            if (this.connection != null && this.connection.State == ConnectionState.Broken)
            {
                Trace.TraceWarning("Connection is broken");
                Console.WriteLine("Connection is broken");
                this.connection.Close();
            }

            IDbConnection connection = this.CreateConnection();
            if (connection == null)
            {
                return null;
            }

            connection.Open();
            this.connection = connection;

            return this.connection;
        }
    }
}
