﻿//-----------------------------------------------------------------------
// <copyright file="ClientEventSet.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace UnityGuestBookEvents
{
    using Badumna.Arbitration;

    /// <summary>
    /// ClientEventSet class is responsible to register all available client events.
    /// </summary>
    public class ClientEventSet
    {
        /// <summary>
        /// Arbitration event set instance.
        /// </summary>
        private static ArbitrationEventSet instance;

        /// <summary>
        /// Initializes static members of the <see cref="ClientEventSet"/> class. 
        /// </summary>
        static ClientEventSet()
        {
            ClientEventSet.instance = new ArbitrationEventSet();

            //// TODO: register the available event here
            ClientEventSet.instance.Register(typeof(RequestGuestbookEvent), typeof(AddToGuestbookEvent), typeof(SendClientInfoEvent));
        }

        /// <summary>
        /// Gets the arbitration event set.
        /// </summary>
        public static ArbitrationEventSet Instance
        {
            get { return ClientEventSet.instance; }
        }
    }
}
