﻿//-----------------------------------------------------------------------
// <copyright file="SendGuestbookMessageEvent.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace UnityGuestBookEvents
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    /// <summary>
    /// SendGuestbookMessageEvent is a server event, this event is used to send the guestbook messages
    /// from server to client side. 
    /// </summary>
    public class SendGuestbookMessageEvent : ServerEvent
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SendGuestbookMessageEvent"/> class.
        /// </summary>
        /// <param name="entries">Guestbook entries.</param>
        public SendGuestbookMessageEvent(List<GuestbookEntry> entries)
        {
            this.GuestbookEntries = entries;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SendGuestbookMessageEvent"/> class.
        /// </summary>
        /// <param name="reader">Binary reader.</param>
        public SendGuestbookMessageEvent(BinaryReader reader)
        {
            int length = int.Parse(reader.ReadString());

            string message = string.Empty;
            string playerName = string.Empty;
            long time;

            if (this.GuestbookEntries == null)
            {
                this.GuestbookEntries = new List<GuestbookEntry>();
            }

            for (int i = 0; i < length; i++)
            {
                message = reader.ReadString();
                playerName = reader.ReadString();
                time = reader.ReadInt64();
                this.GuestbookEntries.Add(new GuestbookEntry(message, playerName, time));
            }
        }

        /// <summary>
        /// Gets the serialized data length.
        /// </summary>
        public override int SerializedLength
        {
            get { return this.CalculateSizeOfGuestbookEntries(); }
        }

        /// <summary>
        /// Gets or sets the list of guestbook entries.
        /// </summary>
        public List<GuestbookEntry> GuestbookEntries { get; set; }

        /// <summary>
        /// Serialize the data that need to be sent.
        /// </summary>
        /// <param name="writer">Binary writer.</param>
        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(this.GuestbookEntries.Count.ToString());
            foreach (GuestbookEntry entry in this.GuestbookEntries)
            {
                writer.Write(entry.Message);
                writer.Write(entry.PlayerName);
                writer.Write(entry.Time.Ticks);
            }
        }

        /// <summary>
        /// Calculate the size of the serialized data, depend on the number of guestbook entries.
        /// </summary>
        /// <returns>Return the size of the serialized data.</returns>
        private int CalculateSizeOfGuestbookEntries()
        {
            int size = this.GuestbookEntries.Count.ToString().Length * sizeof(char);
            foreach (GuestbookEntry entry in this.GuestbookEntries)
            {
                size += (entry.Message.Length + entry.PlayerName.Length) * sizeof(char);
                size += sizeof(long);
            }

            return size;
        }
    }
}
