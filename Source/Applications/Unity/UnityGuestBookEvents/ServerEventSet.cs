﻿//-----------------------------------------------------------------------
// <copyright file="ServerEventSet.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace UnityGuestBookEvents
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Badumna.Arbitration;

    /// <summary>
    /// ServerEventSet class has responsibility to register all available server events.
    /// </summary>
    public class ServerEventSet
    {
        /// <summary>
        /// The instance of arbitration event set.
        /// </summary>
        private static ArbitrationEventSet instance;

        /// <summary>
        /// Initializes static members of the <see cref="ServerEventSet"/> class.
        /// </summary>
        static ServerEventSet()
        {
            ServerEventSet.instance = new ArbitrationEventSet();
            
            //// TODO: register all server events here
            ServerEventSet.instance.Register(typeof(SendGuestbookMessageEvent));
        }

        /// <summary>
        /// Gets the arbitration event set instance.
        /// </summary>
        public static ArbitrationEventSet Instance
        {
            get { return ServerEventSet.instance; }
        }
    }
}
