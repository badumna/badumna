﻿//-----------------------------------------------------------------------
// <copyright file="ClientEvent.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace UnityGuestBookEvents
{
    using Badumna.Arbitration;

    /// <summary>
    /// Client event class is derived from ArbitrationEvent class.
    /// </summary>
    public abstract class ClientEvent : ArbitrationEvent
    {
        /// <summary>
        /// Serialized length in int.
        /// </summary>
        public override int SerializedLength 
        { 
            get { return 0; } 
        }

        /// <summary>
        /// Serialize the data that need to be sent.
        /// </summary>
        /// <param name="writer">Binary writer.</param>
        public override void Serialize(System.IO.BinaryWriter writer)
        {
            ////TODO: the serialize would be implemented in the specific client event class
            //// that derived from this class
        }
    }
}
