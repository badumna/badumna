﻿//-----------------------------------------------------------------------
// <copyright file="ServerEvent.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace UnityGuestBookEvents
{
    using System.Diagnostics;
    using Badumna.Arbitration;

    /// <summary>
    /// ServerEvent class is derived from ArbitrationEvent class.
    /// </summary>
    public abstract class ServerEvent : ArbitrationEvent
    {
        /// <summary>
        /// Serialized length in int.
        /// </summary>
        public override int SerializedLength 
        { 
            get 
            { 
                Debug.Fail("Shoudl never be here."); 
                return 0; 
            } 
        }

        /// <summary>
        /// Serialize data that need to be sent.
        /// </summary>
        /// <param name="writer">Binary writer.</param>
        public override void Serialize(System.IO.BinaryWriter writer)
        {
            ////TODO: the serialize would be implemented in the specific server event class
            //// that derived from this class
        }
    }
}
