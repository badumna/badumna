﻿//-----------------------------------------------------------------------
// <copyright file="AddToGuestbookEvent.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace UnityGuestBookEvents
{
    using System;
    using System.IO;

    /// <summary>
    /// AddToGuestbookEvent class is a client event, it has responsibility to send the guestbook entry from the client to
    /// server side.
    /// <remarks>At the moment Guestbook message should be 1000 char length max</remarks>
    /// </summary>
    public class AddToGuestbookEvent : ClientEvent
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AddToGuestbookEvent"/> class.
        /// </summary>
        /// <param name="message">Guestbook message.</param>
        /// <param name="playerName">The player name.</param>
        public AddToGuestbookEvent(string message, string playerName)
        {
            this.Message = message;
            this.PlayerName = playerName;
            this.Time = DateTime.Now;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AddToGuestbookEvent"/> class.
        /// </summary>
        /// <param name="reader">Binary reader.</param>
        public AddToGuestbookEvent(BinaryReader reader)
        {
            this.Message = reader.ReadString();
            this.PlayerName = reader.ReadString();
            this.Time = new DateTime(reader.ReadInt64());
        }

        /// <summary>
        /// Gets the serialized data length.
        /// </summary>
        public override int SerializedLength
        {
            get { return (this.Message.Length * sizeof(char)) + sizeof(long) + (this.PlayerName.Length * sizeof(char)); }
        }

        /// <summary>
        /// Gets or sets the guestbook message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the player name.
        /// </summary>
        public string PlayerName { get; set; }

        /// <summary>
        /// Gets or sets the time.
        /// </summary>
        public DateTime Time { get; set; }

        /// <summary>
        /// Serialized the data that need to be sent.
        /// </summary>
        /// <param name="writer">Binary writer.</param>
        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(this.Message);
            writer.Write(this.PlayerName);
            writer.Write(this.Time.Ticks);
        }
    }
}
