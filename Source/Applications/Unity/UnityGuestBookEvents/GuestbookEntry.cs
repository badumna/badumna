﻿//-----------------------------------------------------------------------
// <copyright file="GuestbookEntry.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace UnityGuestBookEvents
{
    using System;

    /// <summary>
    /// GuestbookEntry store the information about each of the guestbook entry
    /// </summary>
    public class GuestbookEntry
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GuestbookEntry"/> class.
        /// </summary>
        /// <param name="message">Guestbook message.</param>
        /// <param name="playername">Player name.</param>
        /// <param name="ticks">Time on long ticks format.</param>
        public GuestbookEntry(string message, string playername, long ticks)
        {
            this.Message = message;
            this.PlayerName = playername;
            this.Time = new DateTime(ticks);
        }

        /// <summary>
        /// Gets or sets guestbook message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the guestbook sender.
        /// </summary>
        public string PlayerName { get; set; }

        /// <summary>
        /// Gets or sets the time when the guestbook is added.
        /// </summary>
        public DateTime Time { get; set; }
    }
}
