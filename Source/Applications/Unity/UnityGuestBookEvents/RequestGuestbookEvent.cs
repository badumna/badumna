﻿//-----------------------------------------------------------------------
// <copyright file="RequestGuestbookEvent.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace UnityGuestBookEvents
{
    using System.IO;

    /// <summary>
    /// RequestGuestbookEvent is a client event, it is used by the client for
    /// requesting guestbook messages.
    /// </summary>
    public class RequestGuestbookEvent : ClientEvent
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RequestGuestbookEvent"/> class.
        /// </summary>
        public RequestGuestbookEvent() 
        { 
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RequestGuestbookEvent"/> class.
        /// </summary>
        /// <param name="reader">Binary reader.</param>
        public RequestGuestbookEvent(BinaryReader reader) 
        { 
        }
    }
}
