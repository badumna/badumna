﻿//-----------------------------------------------------------------------
// <copyright file="SendClientInfoEvent.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace UnityGuestBookEvents
{
    using System;
    using System.IO;

    /// <summary>
    /// SendClientInfoEvent class is a client event, it responsible to send the client information back to the server side.
    /// </summary>
    public class SendClientInfoEvent : ClientEvent
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SendClientInfoEvent"/> class.
        /// </summary>
        /// <param name="natType">The Nat type.</param>
        /// <param name="hashValueOfIpAddress">Ip has value.</param>
        public SendClientInfoEvent(string natType, string hashValueOfIpAddress)
        {
            this.NatType = natType;
            this.HashValueOfIpAddress = hashValueOfIpAddress;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SendClientInfoEvent"/> class.
        /// </summary>
        /// <param name="reader">Binary reader.</param>
        public SendClientInfoEvent(BinaryReader reader)
        {
            this.NatType = reader.ReadString();
            this.HashValueOfIpAddress = reader.ReadString();
        }

        /// <summary>
        /// Gets the serialized data length.
        /// </summary>
        public override int SerializedLength
        {
            get { return (this.NatType.Length + this.HashValueOfIpAddress.Length) * sizeof(char); }
        }

        /// <summary>
        /// Gets or sets the nat type.
        /// </summary>
        public string NatType { get; set; }

        /// <summary>
        /// Gets or sets the ip hash value.
        /// </summary>
        public string HashValueOfIpAddress { get; set; }

        /// <summary>
        /// Serialized data that need to be sent.
        /// </summary>
        /// <param name="writer">Binary writer.</param>
        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(this.NatType);
            writer.Write(this.HashValueOfIpAddress);
        }
    }
}
