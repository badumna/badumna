using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Badumna.Arbitration;
using UnityGuestBookEvents;
using Badumna;
using System.Threading;

namespace UnityGuestbookTester
{
    class Program
    {
        static IArbitrator arbitrator= null;

        static void Main(string[] args)
        {
            ManualResetEvent done = new ManualResetEvent(false);
            Console.CancelKeyPress += delegate { done.Set(); };

            Options options = new Options();
            options.Connectivity.SeedPeers.Add("128.250.77.144:21251");
            options.Connectivity.StartPortRange = 20002;
            options.Connectivity.EndPortRange = 20018;
            options.Connectivity.BroadcastPort = 20020;
            options.Connectivity.ApplicationName = "guestbook";
            options.Arbitration.Servers.Add(new ArbitrationServerDetails("guestbook"));

            INetworkFacade networkFacade = NetworkFacade.Create(options);

            networkFacade.Login();

            Program.arbitrator = networkFacade.GetArbitrator("guestbook");
            Program.arbitrator.Connect(
                HandleArbitrationConnectionResut,
                HandleArbitrationConnectionFailure,
                HandleArbitrationEvent);

            while (true)
            {
                Thread.Sleep(100);
                networkFacade.ProcessNetworkState();
            }
        }

        static void HandleArbitrationEvent(byte[] message)
        {
            ServerEvent serverEvent = (ServerEvent)ServerEventSet.Instance.Deserialize(message);

            if (serverEvent is SendGuestbookMessageEvent)
            {
                foreach (UnityGuestBookEvents.GuestbookEntry entry in ((SendGuestbookMessageEvent)serverEvent).GuestbookEntries)
                {
                    Console.WriteLine("PlayerName:{0},Message:{1},Time:{2}", entry.PlayerName, entry.Message, entry.ToString());
                }
            }
        }

        static void HandleArbitrationConnectionResut(ServiceConnectionResultType result)
        {
            if (result == ServiceConnectionResultType.Success)
            {
                Console.WriteLine("Connected to guestbook arbitration server");
                Program.arbitrator.SendEvent(ClientEventSet.Instance.Serialize(new RequestGuestbookEvent()));
                Program.arbitrator.SendEvent(ClientEventSet.Instance.Serialize(new AddToGuestbookEvent(string.Format("Insert record at {0}", DateTime.Now.ToString()), "Admin")));
            }
            else
            {
                Console.WriteLine("Could not connect to guestbook arbitration server");
            }
        }
        
        static void HandleArbitrationConnectionFailure()
        {
            Console.WriteLine("The connection the Guestbook was lost.");
        }
    }
}
