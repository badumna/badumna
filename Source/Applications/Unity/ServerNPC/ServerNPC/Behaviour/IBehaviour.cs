﻿//-----------------------------------------------------------------------
// <copyright file="IBehaviour.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace ServerNPC.Behaviour
{
    /// <summary>
    /// IBehaviour interface.
    /// </summary>
    public interface IBehaviour
    {
        /// <summary>
        /// Execute behaviour update here.
        /// </summary>
        /// <param name="delayInMiliseconds">Delay in miliseconds.</param>
        void FixedUpdate(int delayInMiliseconds);
    }
}
