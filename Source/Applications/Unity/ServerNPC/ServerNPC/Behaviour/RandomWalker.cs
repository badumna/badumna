﻿//-----------------------------------------------------------------------
// <copyright file="RandomWalker.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace ServerNPC.Behaviour
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Badumna.DataTypes;
    using UnityTerrainReader;

    /// <summary>
    /// RandomWalker is one of the available NPC behaviour. The NPC will walk randomly on the certain area.
    /// </summary>
    public class RandomWalker : IBehaviour
    {
        /// <summary>
        /// Player speed.
        /// </summary>
        private float speed = 2.25f;

        /// <summary>
        /// Current player direction.
        /// </summary>
        private Vector3 direction = new Vector3(0.0f, 0.0f, 0.0f);

        /// <summary>
        /// Current player destination.
        /// </summary>
        private Vector3 destination = new Vector3(0.0f, 0.0f, 0.0f);

        /// <summary>
        /// A value indicating whether the player has arrived on the destination.
        /// </summary>
        private bool reachDestination = true;

        /// <summary>
        /// LocalAvatar script.
        /// </summary>
        private LocalAvatar localAvatar;

        /// <summary>
        /// Random generator.
        /// </summary>
        private Random randomGenerator;

        /// <summary>
        /// Terrain information.
        /// </summary>
        private TerrainInfo terrainInfo;

        /// <summary>
        /// Initializes a new instance of the <see cref="RandomWalker"/> class.
        /// </summary>
        /// <param name="localAvatar">Local avatar.</param>
        public RandomWalker(LocalAvatar localAvatar)
        {
            this.localAvatar = localAvatar;
            this.randomGenerator = new Random();

            if (Type.GetType("Mono.Runtime") != null)
            {
                this.terrainInfo = new TerrainInfo("Heightmap/Terrain.bin", new UnityEngine.Vector3(-10, -18.52212f, -10));
            }
            else
            {
                this.terrainInfo = new TerrainInfo("Heightmap\\Terrain.bin", new UnityEngine.Vector3(-10, -18.52212f, -10));
            }
        }

        #region IBehaviour Members

        /// <summary>
        /// Check for the remaining distance to destination, recalculate the destination when the player
        /// has arrived to the current destination.
        /// </summary>
        /// <param name="delayInMiliseconds">Delay in miliseconds.</param>
        public void FixedUpdate(int delayInMiliseconds)
        {
            if (this.localAvatar == null)
            {
                return;
            }

            if (this.reachDestination)
            {
                this.ChooseDestination();
                this.reachDestination = false;
                this.localAvatar.Velocity = new Vector3(0, 0, 0);
                this.localAvatar.SetAnimation("idle");
            }
            else
            {
                this.direction = (this.destination - this.localAvatar.Position).Normalize();
                this.direction *= this.speed;
                Vector3 path = this.direction * (delayInMiliseconds / 1000.0f);

                // remaining distance in 2D
                float remainingDistance = (float)Math.Sqrt(
                    Math.Pow((this.destination.X - this.localAvatar.Position.X), 2) +
                    Math.Pow((this.destination.Z - this.localAvatar.Position.Z), 2));
                Vector3 path2D = new Vector3(path.X, 0.0f, path.Z);

                if (remainingDistance <= path2D.Magnitude || remainingDistance < 0.5f)
                {
                    this.reachDestination = true;
                    this.localAvatar.Velocity = new Vector3(0, 0, 0);
                    this.localAvatar.SetAnimation("idle");
                }
                else
                {
                    Vector3 tempPosition = this.localAvatar.Position + path;

                    // update the height
                    tempPosition.Y = this.terrainInfo.GetHeight(tempPosition.X, tempPosition.Z);

                    // update the orientation
                    float orientation = (float)(Math.Atan2(this.direction.X, this.direction.Z) * 180.0f / Math.PI);

                    this.localAvatar.Orientation = orientation;
                    this.localAvatar.Position = tempPosition;
                    this.localAvatar.Velocity = this.direction;
                    this.localAvatar.SetAnimation("walk");
                }
            }
        }

        #endregion

        /// <summary>
        /// This method is used to determine the next destination (i.e. next random 
        /// destination).
        /// </summary>
        private void ChooseDestination()
        {
            Vector3 tempDestination = new Vector3(
                this.NextDouble(-10f, 10f),
                0.0f,
                this.NextDouble(-10f, 10f));

            tempDestination.Y = this.terrainInfo.GetHeight(tempDestination.X, tempDestination.Z);

            this.destination = tempDestination;
        }

        /// <summary>
        /// Get the next random double value within the given range.
        /// </summary>
        /// <param name="min">Minimum value.</param>
        /// <param name="max">Maximum value.</param>
        /// <returns>Rerturns the next random double.</returns>
        private float NextDouble(float min, float max)
        {
            return ((float)this.randomGenerator.NextDouble() * (max - min)) + min;
        }
    }
}
