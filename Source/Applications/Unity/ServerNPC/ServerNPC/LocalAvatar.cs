﻿//-----------------------------------------------------------------------
// <copyright file="LocalAvatar.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace ServerNPC
{
    using System;
    using System.IO;
    using Badumna;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;
    using ServerNPC.Behaviour;

    /// <summary>
    /// LocalAvatar class implements ISpatialOriginal and IDeadReckonable. 
    /// </summary>
    public class LocalAvatar : ISpatialOriginal, IDeadReckonable
    {
        /// <summary>
        /// Area of interest radius.
        /// </summary>
        private float areaOfInterestRadius;

        /// <summary>
        /// Local avatar position.
        /// </summary>
        private Vector3 position;

        /// <summary>
        /// Local avatar velocity.
        /// </summary>
        private Vector3 velocity;

        /// <summary>
        /// Local avatar radius.
        /// </summary>
        private float radius;

        /// <summary>
        /// Local avatar guid.
        /// </summary>
        private BadumnaId guid;

        /// <summary>
        /// Local avatar name.
        /// </summary>
        private string localAvatarName;

        /// <summary>
        /// Local avatar orientation.
        /// </summary>
        private float orientation;

        /// <summary>
        /// The character animation name.
        /// </summary>
        private string animationName;

        /// <summary>
        /// A value indicating whether the animation has been changed.
        /// </summary>
        private bool isAnimationChanged = false;

        /// <summary>
        /// A value indicating whether the velocity has been changed.
        /// </summary>
        private bool isVelocityChanged = false;

        /// <summary>
        /// A value indicating whether the orientation has been changed.
        /// </summary>
        private bool isOrientationChanged = false;

        /// <summary>
        /// Badumna boolean array which is used to flag the paramaters for update, set all to true to send
        /// a complete update.
        /// </summary>
        private BooleanArray requiredParts = new BooleanArray(false);

        /// <summary>
        /// NPC behaviour
        /// </summary>
        private RandomWalker walker;

        /// <summary>
        /// Network facade instance.
        /// </summary>
        private INetworkFacade facade;

        /// <summary>
        /// Initializes a new instance of the <see cref="LocalAvatar"/> class.
        /// </summary>
        /// <param name="initialPosition">Initial position.</param>
        /// <param name="initialOrientation">Initial orientation.</param>
        /// <param name="playerName">Player name.</param>
        /// <param name="facade">Network facade instance.</param>
        public LocalAvatar(Vector3 initialPosition, float initialOrientation, string playerName, INetworkFacade facade)
        {
            this.localAvatarName = playerName;
            this.position = initialPosition;
            this.orientation = initialOrientation;
            this.facade = facade;

            this.walker = new RandomWalker(this);
            this.animationName = "idle";

            this.radius = 2.0f;
            this.areaOfInterestRadius = 20f;
        }

        #region IDeadReckonable Members

        /// <summary>
        /// Gets or sets the remote avatar velocity.
        /// </summary>
        public Vector3 Velocity
        {
            get 
            { 
                return this.velocity; 
            }

            set 
            { 
                this.velocity = value;
                this.isVelocityChanged = true;
            }
        }

        #endregion

        #region ISpatialEntity Members

        /// <summary>
        /// Gets or sets the area of interest radius.
        /// </summary>
        public float AreaOfInterestRadius
        {
            get { return this.areaOfInterestRadius; }
            set { this.areaOfInterestRadius = value; }
        }

        /// <summary>
        /// Gets or sets the local avatar position.
        /// </summary>
        public Vector3 Position
        {
            get
            {
                return this.position;
            }

            set
            {
                this.position = value;
            }
        }

        /// <summary>
        /// Gets or sets the local avatar radius.
        /// </summary>
        public float Radius
        {
            get { return this.radius; }
            set { this.radius = value; }
        }

        /// <summary>
        /// Gets or sets the local avatar orientation
        /// </summary>
        public float Orientation
        {
            get
            {
                return this.orientation;
            }

            set
            {
                this.orientation = value;
                this.isOrientationChanged = true;
            }
        }

        /// <summary>
        /// Gets or sets the character animation name.
        /// </summary>
        public string AnimationName
        {
            get { return this.animationName; }
            set { this.animationName = value; }
        }

        #endregion

        #region IEntity Members

        /// <summary>
        /// Gets or sets the Guid.
        /// </summary>
        public BadumnaId Guid
        {
            get { return this.guid; }
            set { this.guid = value; }
        }

        /// <summary>
        /// Handle incoming custom message.
        /// </summary>
        /// <param name="stream">Badumna stream.</param>
        public void HandleEvent(Stream stream)
        {
        }

        #endregion

        #region ISpatialOriginal Members

        /// <summary>
        /// Serialize method will be called when there are some properties need to be updated.
        /// </summary>
        /// <param name="requiredParts">Boolean array.</param>
        /// <param name="stream">Badumna stream.</param>
        public void Serialize(BooleanArray requiredParts, Stream stream)
        {
            BinaryWriter writer = new BinaryWriter(stream);

            // 1. Orientation
            if (requiredParts[(int)UpdateParameter.Orientation])
            {
                writer.Write(this.orientation);
            }

            // 2. Animation
            if (requiredParts[(int)UpdateParameter.AnimationName])
            {
                writer.Write(this.animationName);
            }
        }

        #endregion

        /// <summary>
        /// We should implement the attempt movement on RemoteAvatar class.
        /// </summary>
        /// <param name="reckonedPosition">Reckoned position (estimated position)</param>
        public void AttemptMovement(Vector3 reckonedPosition)
        {
        }

        /// <summary>
        /// Check for any properties update.         
        /// </summary>
        /// <param name="delayInMiliseconds">Delay in miliseconds</param>
        public void FixedUpdate(int delayInMiliseconds)
        {
            // Call the behaviour update if available
            if (this.walker != null)
            {
                this.walker.FixedUpdate(delayInMiliseconds);
            }

            this.requiredParts.SetAll(false);

            if (this.isVelocityChanged)
            {
                this.isVelocityChanged = false;
                this.requiredParts[(int)SpatialEntityStateSegment.Position] = true;
                this.requiredParts[(int)SpatialEntityStateSegment.Velocity] = true;
            }

            if (this.isOrientationChanged)
            {
                this.isOrientationChanged = false;
                this.requiredParts[(int)UpdateParameter.Orientation] = true;
            }

            if (this.isAnimationChanged)
            {
                this.requiredParts[(int)UpdateParameter.AnimationName] = true;
                this.isAnimationChanged = false;
            }

            // Tell Badumna which parts have changed and should thus be updated.
            this.facade.FlagForUpdate(this, this.requiredParts);
        }

        /// <summary>
        /// Apply an animation to the avatar
        /// </summary>
        /// <param name="animationName">Animation name.</param>
        public void SetAnimation(string animationName)
        {
            if (this.animationName != animationName || this.animationName == null)
            {
                this.animationName = animationName;
                this.isAnimationChanged = true;
            }
        }
    }
}
