﻿//-----------------------------------------------------------------------
// <copyright file="Enumerations.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace ServerNPC
{
    using System;
    using Badumna.SpatialEntities;

    /// <summary>
    /// An enumeration of player types, an application specific identification of the 
    /// meshes / objects that is known to all clients. 
    /// </summary>
    public enum PlayerType
    {
        //// TODO: modify this according to player tag name
        //// that available in the game

        /// <summary>
        /// A small lerpz model 
        /// </summary>
        SmallLerpz = 0,

        /// <summary>
        /// A big lerpz model 
        /// </summary>
        MonsterLerpz = 1
    }

    /// <summary>
    /// The UpdateParameter enum is used to index which parameter has been modified when generating and 
    /// applying updates. The value is used as the index in the BooleanArray, passed to Serialize and
    /// Deserialize on the LocalAvatar and RemoteAvatar respectively.
    /// Note: Modify or add new paramater here as required
    /// </summary>
    public enum UpdateParameter : int
    {
        /// <summary>
        /// The first unused index for update parameters will be used.
        /// Orientation paramater.
        /// </summary>
        Orientation = SpatialEntityStateSegment.FirstAvailableSegment,

        /// <summary>
        /// Animation name paramter.
        /// </summary>
        AnimationName
    }
}
