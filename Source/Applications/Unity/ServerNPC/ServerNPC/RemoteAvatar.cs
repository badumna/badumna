﻿//-----------------------------------------------------------------------
// <copyright file="RemoteAvatar.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace ServerNPC
{
    using System.IO;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;

    /// <summary>
    /// Remote Avatar class implements ISpatialReplica and IDeadReckonable.
    /// </summary>
    public class RemoteAvatar : ISpatialReplica, IDeadReckonable
    {
        /// <summary>
        /// Area of interest radius.
        /// </summary>
        private float areaOfInterestRadius;

        /// <summary>
        /// Remote avatar position.
        /// </summary>
        private Vector3 position;

        /// <summary>
        /// Remote avatar velocity.
        /// </summary>
        private Vector3 velocity;

        /// <summary>
        /// Remote avatar radius.
        /// </summary>
        private float radius;

        /// <summary>
        /// Remote avatar guid.
        /// </summary>
        private BadumnaId guid;

        /// <summary>
        /// Remote avatar orientation.
        /// </summary>
        private float orientation;

        /// <summary>
        /// The character animation name.
        /// </summary>
        private string animationName;

        /// <summary>
        /// Initializes a new instance of the <see cref="RemoteAvatar"/> class.
        /// </summary>
        public RemoteAvatar()
        {
            this.radius = 2.0f;
            this.areaOfInterestRadius = 20f;
        }

        #region IDeadReckonable Members

        /// <summary>
        /// Gets or sets the remote avatar velocity.
        /// </summary>
        public Vector3 Velocity
        {
            get { return this.velocity; }
            set { this.velocity = value; }
        }

        #endregion

        #region ISpatialEntity Members

        /// <summary>
        /// Gets or sets the area of interest radius.
        /// </summary>
        public float AreaOfInterestRadius
        {
            get { return this.areaOfInterestRadius; }
            set { this.areaOfInterestRadius = value; }
        }

        /// <summary>
        /// Gets or sets the remote avatar position.
        /// </summary>
        public Vector3 Position
        {
            get { return this.position; }
            set { this.position = value; }
        }

        /// <summary>
        /// Gets or sets the remote avatar radius.
        /// </summary>
        public float Radius
        {
            get { return this.radius; }
            set { this.radius = value; }
        }

        #endregion

        #region IEntity Members

        /// <summary>
        /// Gets or sets the Guid.
        /// </summary>
        public BadumnaId Guid
        {
            get { return this.guid; }
            set { this.guid = value; }
        }

        /// <summary>
        /// Handle incoming custom message.
        /// </summary>
        /// <param name="stream">Badumna stream.</param>
        public void HandleEvent(Stream stream)
        {
        }

        #endregion

        #region ISpatialReplica Members

        /// <summary>
        /// Deserialize method will be called if there is incoming update from the original.
        /// </summary>
        /// <param name="includedParts">Boolean array indicating the properties will be updated.</param>
        /// <param name="stream">Badumna stream.</param>
        /// <param name="estimatedMillisecondsSinceDeparture">Estimated in milliseconds since departure (delay in ms).</param>
        public void Deserialize(BooleanArray includedParts, Stream stream, int estimatedMillisecondsSinceDeparture)
        {
            BinaryReader reader = new BinaryReader(stream);

            // 1. Orientation
            if (includedParts[(int)UpdateParameter.Orientation])
            {
                this.orientation = reader.ReadSingle();
            }

            // 2. Animation
            if (includedParts[(int)UpdateParameter.AnimationName])
            {
                this.animationName = reader.ReadString();
            }
        }

        #endregion

        /// <summary>
        /// Apply the current position using reckoned position.
        /// </summary>
        /// <param name="reckonedPosition">Reckoned position.</param>
        public void AttemptMovement(Vector3 reckonedPosition)
        {
            this.position = reckonedPosition;
        }
    }
}
