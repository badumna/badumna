﻿//-----------------------------------------------------------------------
// <copyright file="NetworkInitialization.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace ServerNPC
{
    using System;
    using System.Collections.Generic;
    using Badumna;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;

    /// <summary>
    /// NetworkInitialization class is used for loging to Badumna network
    /// and register the NPC to the scene.
    /// </summary>
    public class NetworkInitialization
    {
        /// <summary>
        /// Collection of remote entities.
        /// </summary>
        private Dictionary<BadumnaId, RemoteAvatar> remoteEntities = new Dictionary<BadumnaId, RemoteAvatar>();

        /// <summary>
        /// Collection of local entities.
        /// </summary>
        private Dictionary<BadumnaId, LocalAvatar> localEntities = new Dictionary<BadumnaId, LocalAvatar>();

        /// <summary>
        /// Badumna network scene.
        /// </summary>
        private NetworkScene networkScene;

        /// <summary>
        /// Badumna network scene name.
        /// </summary>
        private string networkSceneName = "Demo9";

        /// <summary>
        /// Network facade instance.
        /// </summary>
        private INetworkFacade facade;

        /// <summary>
        /// Initializes a new instance of the <see cref="NetworkInitialization"/> class.
        /// </summary>
        public NetworkInitialization()
        {
        }

        /// <summary>
        /// Sets the network facade instance.
        /// </summary>
        internal INetworkFacade Facade
        {
            set { this.facade = value; }
        }

        /// <summary>
        /// Join to badumna network scene.
        /// </summary>
        /// <returns>Returns true on sucess.</returns>
        public bool Start()
        {
            if (this.facade.IsLoggedIn)
            {
                // REGISTER ENTITY DETAILS BEFORE JOIN THE SCENE
                this.facade.RegisterEntityDetails(20.0f, new Vector3(6.0f, 6.0f, 6.0f).Magnitude);

                this.networkScene = this.facade.JoinScene(this.networkSceneName, this.CreateEntity, this.RemoveEntity);

                Console.WriteLine(this.facade.GetNetworkStatus().ToString());
            }
            else
            {
                Console.WriteLine("Login error");
                return false;
            }

            // register NPC
            LocalAvatar npc1 = new LocalAvatar(new Vector3(0, -10f, 0), 0f, "NPC1", this.facade);
            LocalAvatar npc2 = new LocalAvatar(new Vector3(0, -10f, 0), 0f, "NPC2", this.facade);

            this.networkScene.RegisterEntity(npc1, (uint)PlayerType.SmallLerpz);
            this.networkScene.RegisterEntity(npc2, (uint)PlayerType.SmallLerpz);

            this.localEntities.Add(npc1.Guid, npc1);
            this.localEntities.Add(npc2.Guid, npc2);

            return true;
        }

        /// <summary>
        /// Call the ProcessNetworkState regularly on fixed update method.
        /// </summary>
        /// <param name="delayInMiliseconds">Delay in miliseconds.</param>
        public void FixedUpdate(int delayInMiliseconds)
        {
            // Call any method that required to be called regularly here.
            if (this.facade.IsLoggedIn)
            {
                foreach (LocalAvatar localAvatar in this.localEntities.Values)
                {
                    localAvatar.FixedUpdate(delayInMiliseconds);
                }
            }
        }

        /// <summary>
        /// Shut down badumna network here.
        /// </summary>
        public void OnDisable()
        {
            foreach (LocalAvatar localAvatar in this.localEntities.Values)
            {
                this.networkScene.UnregisterEntity(localAvatar);
            }

            this.localEntities.Clear();
            this.networkScene.Leave();

            Console.WriteLine(string.Format("Leave {0} scene.", this.networkSceneName));
        }

        #region NetworkScene members

        /// <summary>
        /// Create entity callback function.
        /// </summary>
        /// <param name="scene">Badumna network scene.</param>
        /// <param name="entityId">Entity id.</param>
        /// <param name="entityType">Entity type.</param>
        /// <returns>Return the instance of remote avatar.</returns>
        private ISpatialReplica CreateEntity(NetworkScene scene, BadumnaId entityId, uint entityType)
        {
            Console.WriteLine("Create Remote Entity " + entityType + " with id " + entityId);

            RemoteAvatar remoteAvatar = new RemoteAvatar();

            // add to mRemoteEntities
            this.remoteEntities.Add(entityId, remoteAvatar);

            Console.WriteLine("Finish creating entity with id " + entityId);

            return remoteAvatar as ISpatialReplica;
        }

        /// <summary>
        /// RemoveEntity callback function.
        /// </summary>
        /// <param name="scene">Network scene.</param>
        /// <param name="replica">Remote avatar need to be removed.</param>
        private void RemoveEntity(NetworkScene scene, ISpatialReplica replica)
        {
            RemoteAvatar remoteAvatar = (RemoteAvatar)replica;
            if (this.remoteEntities.TryGetValue(remoteAvatar.Guid, out remoteAvatar))
            {
                this.remoteEntities.Remove(remoteAvatar.Guid);
            }
        }

        #endregion
    }
}
