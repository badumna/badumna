//-----------------------------------------------------------------------
// <copyright file="NetworkInitialization.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace TrollBasherNPC
{
    using System;
    using System.Collections.Generic;
    using Badumna;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;
    using TrollBasherNPC.Behaviour;
    using TrollBasherNPC.Character;

    /// <summary>
    /// NetworkInitialization class has responsibility to initializing the Badumna network
    /// and register the dummy NPC to the TrollBasher game.
    /// </summary>
    public class NetworkInitialization
    {
        /// <summary>
        /// Collection of remote entities.
        /// </summary>
        private Dictionary<BadumnaId, RemoteAvatar> remoteEntities = new Dictionary<BadumnaId, RemoteAvatar>();

        /// <summary>
        /// Collection of local entities.
        /// </summary>
        private Dictionary<BadumnaId, LocalAvatar> localEntities = new Dictionary<BadumnaId, LocalAvatar>();

        /// <summary>
        /// Collection of character action.
        /// </summary>
        private Dictionary<PlayerType, CharacterAction[]> characterActionList = new Dictionary<PlayerType, CharacterAction[]>();

        /// <summary>
        /// Badumna network scene.
        /// </summary>
        private NetworkScene networkScene;

        /// <summary>
        /// Badumna network scene name.
        /// </summary>
        private string networkSceneName = "BadumnaWorldDemo";

        /// <summary>
        /// Network facade instance.
        /// </summary>
        private INetworkFacade network;

        /// <summary>
        /// Initializes a new instance of the <see cref="NetworkInitialization"/> class.
        /// </summary>
        /// Initialize and configure badumna network here.
        public NetworkInitialization()
        {
            this.InitializingCharacterInformation();
        }

        /// <summary>
        /// Sets the network facade instance.
        /// </summary>
        internal INetworkFacade Facade
        {
            set 
            { 
                this.network = value;
                DistributedNPCController.Network = value;
            }
        }

        /// <summary>
        /// Join to badumna network scene.
        /// </summary>
        /// <returns>Returns true on sucess.</returns>
        public bool Start()
        {
            if (this.network.IsLoggedIn)
            {
                // REGISTER ENTITY DETAILS BEFORE JOIN THE SCENE
                this.network.RegisterEntityDetails(130.0f, new Vector3(10, 10, 10).Magnitude);

                this.networkScene = this.network.JoinScene(this.networkSceneName, this.CreateEntity, this.RemoveEntity);

                Console.WriteLine(this.network.GetNetworkStatus().ToString());
            }
            else
            {
                Console.WriteLine("Login error");
                return false;
            }

            // register NPC
            // Note: Known location
            //      * 805.95, 0.086, 301.87, angle of 17.12
            //      * 250, 9, 200, angle of 17.12
            //      * 110.287, 49.598, 282.4838, angle of 54.83044
            //      * 115.9798, 45.05537, 305.5673, angle of 54.83044
            ////LocalAvatar warrior = new LocalAvatar(new Vector3(250f, 9f, 200f), 17.12f, "NPC", 250f, this.facade);
            ////warrior.Character = new RandomWalker(warrior);
            ////warrior.Character.Position = new Vector3(250f, 9f, 200f);
            ////warrior.Character.Orientation = 17.12f;
            ////this.networkScene.RegisterEntity(warrior, (uint)PlayerType.Warrior);

            LocalAvatar troll01 = new LocalAvatar(new Vector3(250f, 9f, 200f), 17.12f, "Troll01", 250f, this.network);
            troll01.Character = new AttackingTroll(troll01, this.remoteEntities, "Troll01", this.network, this.characterActionList[PlayerType.Troll]);
            troll01.Character.Position = new Vector3(110.287f, 49.598f, 282.4838f);
            troll01.Character.Orientation = 54.83044f;
            this.networkScene.RegisterEntity(troll01, (uint)PlayerType.Troll);

            LocalAvatar troll02 = new LocalAvatar(new Vector3(250f, 9f, 200f), 17.12f, "Troll02", 250f, this.network);
            troll02.Character = new AttackingTroll(troll02, this.remoteEntities, "Troll02", this.network, this.characterActionList[PlayerType.Troll]);
            troll02.Character.Position = new Vector3(115.9798f, 45.05537f, 305.5673f);
            troll02.Character.Orientation = 54.83044f;
            this.networkScene.RegisterEntity(troll02, (uint)PlayerType.Troll);
            
            ////LocalAvatar wizard = new LocalAvatar(new Vector3(622.28f, 2.35f, 308.15f), 333f, "NPC", 100f);
            ////this.networkScene.RegisterEntity(wizard, (uint)PlayerType.Wizard);

            this.localEntities.Add(troll02.Guid, troll02);
            this.localEntities.Add(troll01.Guid, troll01);
            ////this.localEntities.Add(wizard.Guid, wizard);

            // just register the distributed controller type
            this.network.StartController<DistributedNPCController>(this.networkSceneName);
            this.network.StopController<DistributedNPCController>(this.networkSceneName);

            return true;
        }

        /// <summary>
        /// Call the ProcessNetworkState regularly on fixed update method.
        /// </summary>
        /// <param name="delayInMiliseconds">Delay in miliseconds.</param>
        public void FixedUpdate(int delayInMiliseconds)
        {
            // Call any method that required to be called regularly here.
            if (this.network != null && this.network.IsLoggedIn)
            {
                foreach (LocalAvatar localAvatar in this.localEntities.Values)
                {
                    if (localAvatar.Character as AttackingTroll != null
                        && localAvatar.Character.Mode.Equals(CharacterMode.Respawn))
                    {
                        this.networkScene.UnregisterEntity(localAvatar);
                        
                        if (localAvatar.Character.PlayerName.Equals("Troll01"))
                        {
                            localAvatar.Character.Position = new Vector3(110.287f, 49.598f, 282.4838f);
                        }
                        else
                        {
                            localAvatar.Character.Position = new Vector3(115.9798f, 45.05537f, 305.5673f);
                        }

                        localAvatar.Character.Orientation = 54.83044f;
                        ((AttackingTroll)localAvatar.Character).ReinitCharacter();
                        this.networkScene.RegisterEntity(localAvatar, (uint)PlayerType.Troll);
                    }

                    localAvatar.FixedUpdate(delayInMiliseconds);
                }
            }
        }

        /// <summary>
        /// Shut down badumna network here.
        /// </summary>
        public void OnDisable()
        {
            foreach (LocalAvatar localAvatar in this.localEntities.Values)
            {
                this.networkScene.UnregisterEntity(localAvatar);
            }

            this.localEntities.Clear();
            this.networkScene.Leave();

            Console.WriteLine(string.Format("Leave {0} scene.", this.networkSceneName));
        }

        #region NetworkScene members

        /// <summary>
        /// Create entity callback function.
        /// </summary>
        /// <param name="scene">Badumna network scene.</param>
        /// <param name="entityId">Entity id.</param>
        /// <param name="entityType">Entity type.</param>
        /// <returns>Return the instance of remote avatar.</returns>
        private ISpatialReplica CreateEntity(NetworkScene scene, BadumnaId entityId, uint entityType)
        {
            Console.WriteLine("Create Remote Entity " + entityType + " with id " + entityId);

            RemoteAvatar remoteAvatar = new RemoteAvatar(this.localEntities, this.characterActionList[(PlayerType)entityType]);

            // add to mRemoteEntities
            this.remoteEntities.Add(entityId, remoteAvatar);

            Console.WriteLine("Finish creating entity with id " + entityId);

            return remoteAvatar as ISpatialReplica;
        }

        /// <summary>
        /// RemoveEntity callback function.
        /// </summary>
        /// <param name="scene">Network scene.</param>
        /// <param name="replica">Remote avatar need to be removed.</param>
        private void RemoveEntity(NetworkScene scene, ISpatialReplica replica)
        {
            RemoteAvatar remoteAvatar = (RemoteAvatar)replica;
            if (this.remoteEntities.TryGetValue(remoteAvatar.Guid, out remoteAvatar))
            {
                this.remoteEntities.Remove(remoteAvatar.Guid);
            }
        }

        #endregion

        /// <summary>
        /// Initializing the character information, currently only used to initialize the action for each
        /// character.
        /// </summary>
        private void InitializingCharacterInformation()
        {
            // initializing character actions
            CharacterAction[] actions = new CharacterAction[3];
            actions[0] = new CharacterAction("Sword slash", 1, 25, 10);
            actions[1] = new CharacterAction("Shield bash", 1.1f, 20, 20);
            actions[2] = new CharacterAction("Kick", 1.6f, 10, 10);

            this.characterActionList.Add(PlayerType.Warrior, actions);

            actions = new CharacterAction[3];
            actions[0] = new CharacterAction("Firebolt", 1.6f, 30, 20);
            actions[1] = new CharacterAction("Blast", 1.5f, 5, 10);
            actions[2] = new CharacterAction("Healing", 1.833334f, 200, 100);

            this.characterActionList.Add(PlayerType.Wizard, actions);

            actions = new CharacterAction[1];
            actions[0] = new CharacterAction("Club bash", 1.333f, 15, 10);

            this.characterActionList.Add(PlayerType.Troll, actions);
            this.characterActionList.Add(PlayerType.RedTroll, actions);
        }
    }
}
