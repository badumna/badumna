//-----------------------------------------------------------------------
// <copyright file="LocalAvatar.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace TrollBasherNPC
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using Badumna;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;
    using TrollBasherNPC.Behaviour;
    using TrollBasherNPC.Character;

    /// <summary>
    /// LocalAvatar class implements ISpatialOriginal and IDeadReckonable. 
    /// </summary>
    /// It will use a very basic implementation.
    public class LocalAvatar : ISpatialOriginal, IDeadReckonable
    {
        /// <summary>
        /// Area of interest radius.
        /// </summary>
        private float areaOfInterestRadius;

        /// <summary>
        /// Local avatar position.
        /// </summary>
        private Vector3 position;

        /// <summary>
        /// Local avatar velocity.
        /// </summary>
        private Vector3 velocity;

        /// <summary>
        /// Local avatar radius.
        /// </summary>
        private float radius;

        /// <summary>
        /// Local avatar guid.
        /// </summary>
        private BadumnaId guid;

        /// <summary>
        /// Local avatar name.
        /// </summary>
        private string localAvatarName;

        /// <summary>
        /// Local avatar orientation.
        /// </summary>
        private float orientation;

        /// <summary>
        /// Moving state.
        /// </summary>
        private int moving = 0;

        /// <summary>
        /// Turning state.
        /// </summary>
        private int turning = 0;

        /// <summary>
        /// Jumping state.
        /// </summary>
        private bool jumping = false;

        /// <summary>
        /// OnGround state.
        /// </summary>
        private bool onGround = false;

        /// <summary>
        /// Boolean array indicating which update part need to be sent.
        /// </summary>
        private BooleanArray requiredParts = new BooleanArray(false);

        /// <summary>
        /// Local avatar current health.
        /// </summary>
        /// (we need to change the health in percentage instead).
        private float health;

        /// <summary>
        /// This NPC behaviour.
        /// </summary>
        private CharacterBehaviour character;

        /// <summary>
        /// Network facade instance.
        /// </summary>
        private INetworkFacade facade;

        /// <summary>
        /// Last check for update.
        /// </summary>
        private DateTime lastTimeCheckForUpdate;

        /// <summary>
        /// Initializes a new instance of the <see cref="LocalAvatar"/> class.
        /// </summary>
        /// <param name="initialPosition">Initial position.</param>
        /// <param name="initialOrientation">Initial orientation.</param>
        /// <param name="playerName">Player name.</param>
        /// <param name="health">The initial health.</param>
        /// <param name="facade">Network facade instance.</param>
        public LocalAvatar(Vector3 initialPosition, float initialOrientation, string playerName, float health, INetworkFacade facade)
        {
            this.localAvatarName = playerName;
            this.position = initialPosition;
            this.orientation = initialOrientation;
            this.health = health;
            this.facade = facade;

            this.radius = 5.0f;
            this.areaOfInterestRadius = 130f;
        }

        #region IDeadReckonable Members

        /// <summary>
        /// Gets or sets the remote avatar velocity.
        /// </summary>
        public Vector3 Velocity
        {
            get { return this.velocity; }
            set { this.velocity = value; }
        }

        #endregion
        #region ISpatialEntity Members

        /// <summary>
        /// Gets or sets the area of interest radius.
        /// </summary>
        public float AreaOfInterestRadius
        {
            get { return this.areaOfInterestRadius; }
            set { this.areaOfInterestRadius = value; }
        }

        /// <summary>
        /// Gets or sets the local avatar position.
        /// </summary>
        public Vector3 Position
        {
            get
            {
                return this.position;
            }

            set
            {
                this.position = value;
            }
        }

        /// <summary>
        /// Gets or sets the local avatar radius.
        /// </summary>
        public float Radius
        {
            get { return this.radius; }
            set { this.radius = value; }
        }

        /// <summary>
        /// Gets or sets the local avatar orientation
        /// </summary>
        public float Orientation
        {
            get
            {
                return this.orientation;
            }

            set
            {
                this.orientation = value;
            }
        }

        /// <summary>
        /// Gets or sets the local avatar moving state.
        /// </summary>
        public int Moving
        {
            get 
            { 
                return this.moving; 
            }

            set 
            { 
                this.moving = value;
            }
        }

        #endregion

        /// <summary>
        /// Gets or sets the character behaviour.
        /// </summary>
        public CharacterBehaviour Character
        {
            get { return this.character; }
            set { this.character = value; }
        }

        #region IEntity Members

        /// <summary>
        /// Gets or sets the Guid.
        /// </summary>
        public BadumnaId Guid
        {
            get { return this.guid; }
            set { this.guid = value; }
        }

        /// <summary>
        /// Handle incoming custom message.
        /// </summary>
        /// <param name="stream">Badumna stream.</param>
        public void HandleEvent(Stream stream)
        {
        }

        #endregion

        #region ISpatialOriginal Members

        /// <summary>
        /// Serialize method will be called when there are some properties need to be updated.
        /// </summary>
        /// <param name="requiredParts">Boolean array.</param>
        /// <param name="stream">Badumna stream.</param>
        public void Serialize(BooleanArray requiredParts, Stream stream)
        {
            BinaryWriter writer = new BinaryWriter(stream);

            if (requiredParts[(int)UpdateParameter.Orientation])
            {
                writer.Write(this.orientation);
            }

            if (requiredParts[(int)UpdateParameter.Moving])
            {
                writer.Write(this.moving);
            }

            if (requiredParts[(int)UpdateParameter.Turning])
            {
                writer.Write(this.turning);
            }

            if (requiredParts[(int)UpdateParameter.Health])
            {
                writer.Write(this.health);
            }

            if (requiredParts[(int)UpdateParameter.OnGround])
            {
                writer.Write(this.onGround);
            }

            if (requiredParts[(int)UpdateParameter.PlayerName])
            {
                writer.Write(this.localAvatarName);
                System.Console.WriteLine("Serialize player name: " + this.localAvatarName);
            }
        }

        #endregion

        /// <summary>
        /// We should implement the attempt movement on RemoteAvatar class.
        /// </summary>
        /// <param name="reckonedPosition">Reckoned position (estimated position)</param>
        public void AttemptMovement(Vector3 reckonedPosition)
        {
        }

        /// <summary>
        /// Check for any properties update.
        /// </summary>
        /// <param name="delayInMiliseconds">Delay in miliseconds.</param>
        public void FixedUpdate(int delayInMiliseconds)
        {
            if (this.facade == null)
            {
                return;
            }

            if (!this.facade.IsLoggedIn)
            {
                return;
            }

            if (this.character == null)
            {
                return;
            }

            this.character.FixedUpdate(delayInMiliseconds);

            DateTime now = DateTime.Now;
            if ((now - this.lastTimeCheckForUpdate).TotalMilliseconds < 100)
            {
                return;
            }

            this.lastTimeCheckForUpdate = now;
            this.requiredParts.SetAll(false);

            if (Math.Abs(this.orientation - this.character.Orientation) >= 5)
            {
                this.orientation = this.character.Orientation;
                this.requiredParts[(int)UpdateParameter.Orientation] = true;
            }

            if (this.jumping != this.character.Jumping)
            {
                this.jumping = this.character.Jumping;
                if (this.jumping)
                {
                    this.SendJumpEvent();
                }
            }

            if (this.onGround != this.character.OnGround)
            {
                this.onGround = this.character.OnGround;
                this.requiredParts[(int)UpdateParameter.OnGround] = true;
            }

            if (this.health != this.character.Health.CurrentHealth)
            {
                this.health = this.character.Health.CurrentHealth;
                this.requiredParts[(int)UpdateParameter.Health] = true;
            }

            // Currently we do not simulate the character velocity.
            float velocityChange = (this.velocity - this.character.Velocity).Magnitude;
            float positionChange = (this.position - this.character.Position).Magnitude;

            if (velocityChange > 0.1f || positionChange > 10.0f)
            {
                this.position = this.character.Position;
                this.velocity = this.character.Velocity;
                this.requiredParts[(int)SpatialEntityStateSegment.Position] = true;
                this.requiredParts[(int)SpatialEntityStateSegment.Velocity] = true;
            }

            if (this.turning != this.character.Turning)
            {
                this.turning = this.character.Turning;
                this.requiredParts[(int)UpdateParameter.Turning] = true;
            }

            if (this.moving != this.character.Moving)
            {
                this.moving = this.character.Moving;
                this.requiredParts[(int)UpdateParameter.Moving] = true;
            }

            this.facade.FlagForUpdate(this, this.requiredParts);
        }

        /// <summary>
        /// Send jump event to the remote copies.
        /// </summary>
        private void SendJumpEvent()
        {
            using (MemoryStream eventStream = new MemoryStream(sizeof(int)))
            {
                BinaryWriter writer = new BinaryWriter(eventStream);
                writer.Write((int)CustomEventType.Jumping);
#if DEBUG
                Trace.TraceInformation("Send jump event");
#endif
                this.facade.SendCustomMessageToRemoteCopies(this, eventStream);
            }
        }
    }
}
