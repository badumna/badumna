﻿//-----------------------------------------------------------------------
// <copyright file="Follower.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2011 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace TrollBasherNPC.Behaviour
{
    using System;
    using System.Collections.Generic;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;
    using TrollBasherNPC.Character;
    using UnityTerrainReader;

    /// <summary>
    /// Follower script is one of the available NPC behaviour.
    /// </summary>
    public class Follower : CharacterBehaviour
    {
        /// <summary>
        /// Collection of remote entities.
        /// </summary>
        private Dictionary<BadumnaId, RemoteAvatar> remoteEntities;

        /// <summary>
        /// Terrain information.
        /// </summary>
        private TerrainInfo terrainInfo;

        /// <summary>
        /// Current NPC direction.
        /// </summary>
        private Vector3 direction = new Vector3(0.0f, 0.0f, 0.0f);

        /// <summary>
        /// Initializes a new instance of the <see cref="Follower"/> class.
        /// </summary>
        /// <param name="avatar">ISpatialEntity instance (in this case a local avatar instance).</param>
        /// <param name="remoteEntities">Colletion of remote entities.</param>
        /// <param name="playerName">Player name.</param>
        public Follower(ISpatialEntity avatar, Dictionary<BadumnaId, RemoteAvatar> remoteEntities, string playerName)
            : base(playerName, -1)
        {
            this.Avatar = avatar;
            this.remoteEntities = remoteEntities;

            // Note: shouldn't be set statically from here.
            this.terrainInfo = new TerrainInfo("heightmap\\Terrain_Valley.bin", new UnityEngine.Vector3(0, 0, -300));
        }

        /// <summary>
        /// Check for the remaining distance to destination, the destination in this case is the position
        /// of the followed object.
        /// </summary>
        /// <param name="delayInMiliseconds">Delay in milliseconds.</param>
        public override void FixedUpdate(int delayInMiliseconds)
        {
            if (this.Avatar == null)
            {
                return;
            }

            // Find the closest replicas and follow them.
            ISpatialReplica followedObject = null;
            float distance = 0.0f;

            foreach (RemoteAvatar remoteAvatar in this.remoteEntities.Values)
            {
                if (followedObject == null)
                {
                    distance = (this.Position - remoteAvatar.Position).Magnitude;
                    followedObject = remoteAvatar;
                }
                else
                {
                    if ((this.Position - remoteAvatar.Position).Magnitude < distance)
                    {
                        distance = (this.Position - remoteAvatar.Position).Magnitude;
                        followedObject = remoteAvatar;
                    }
                }
            }

            if (followedObject != null)
            {
                this.direction = (followedObject.Position - this.Position).Normalize();
                this.direction *= this.ForwardSpeed;

                Vector3 path = this.direction * (delayInMiliseconds / 1000.0f);

                // remaining distance in 2D
                float remainingDistance = (float)Math.Sqrt(
                    Math.Pow((followedObject.Position.X - this.Position.X), 2) +
                    Math.Pow((followedObject.Position.Z - this.Position.Z), 2));
                Vector3 path2D = new Vector3(path.X, 0.0f, path.Z);

                if (remainingDistance <= path2D.Magnitude || remainingDistance < 1.5f)
                {
                    this.Velocity = new Vector3(0, 0, 0);
                    this.Moving = 0;
                }
                else
                {
                    Vector3 tempPosition = this.Position + path;

                    // update the height
                    tempPosition.Y = this.terrainInfo.GetHeight(tempPosition.X, tempPosition.Z);

                    // update the orientation
                    float orientation = (float)(Math.Atan2(this.direction.X, this.direction.Z) * 180.0f / Math.PI);

                    this.Orientation = orientation;
                    this.Position = tempPosition;
                    this.Velocity = this.direction;
                    this.Moving = 1;
                }
            }
        }
    }
}
