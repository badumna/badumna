﻿//-----------------------------------------------------------------------
// <copyright file="AttackingTroll.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2011 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace TrollBasherNPC.Behaviour
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using Badumna;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;
    using TrollBasherNPC.Character;
    using UnityTerrainReader;

    /// <summary>
    /// Attacking troll behaviour imitate from the NPCController class.
    /// </summary>
    public class AttackingTroll : CharacterBehaviour
    {
        /// <summary>
        /// Collection of remote entities.
        /// </summary>
        private Dictionary<BadumnaId, RemoteAvatar> remoteEntities;

        /// <summary>
        /// Terrain information.
        /// </summary>
        private TerrainInfo terrainInfo;

        /// <summary>
        /// Current NPC direction.
        /// </summary>
        private Vector3 direction = new Vector3(0.0f, 0.0f, 0.0f);

        /// <summary>
        /// Listen radius.
        /// </summary>
        private float listenRadius = 15f;

        /// <summary>
        /// Network facade instance.
        /// </summary>
        private INetworkFacade facade;

        /// <summary>
        /// Initializes a new instance of the <see cref="AttackingTroll"/> class.
        /// </summary>
        /// <param name="avatar">Local avatar.</param>
        /// <param name="remoteEntities">Collection of remote entities.</param>
        /// <param name="playerName">Player name.</param>
        /// <param name="facade">Network facade instance.</param>
        /// <param name="actions">List of character actions.</param>
        public AttackingTroll(
            ISpatialEntity avatar,
            Dictionary<BadumnaId, RemoteAvatar> remoteEntities,
            string playerName,
            INetworkFacade facade,
            CharacterAction[] actions)
            : base(playerName, -1)
        {
            this.Actions = actions;
            this.ResurectionDelay = 90;
            this.Health.MaxHealth = 300;

            this.Avatar = avatar;
            this.remoteEntities = remoteEntities;
            this.facade = facade;

            // Note: shouldn't be set statically from here.
            this.terrainInfo = new TerrainInfo("heightmap\\Terrain_Valley.bin", new UnityEngine.Vector3(0, 0, -300));

            this.RegisterActionImpactCallback(this.SendActionImpact);
        }

        /// <summary>
        /// Check for any target and attack the target if there is one.
        /// </summary>
        /// <param name="delayInMiliseconds">Delay in milliseconds.</param>
        public override void FixedUpdate(int delayInMiliseconds)
        {
            if (this.Mode != CharacterMode.Dead && this.Mode != CharacterMode.Respawn)
            {
                if (this.BeingPushed)
                {
                    this.direction = this.PushForce;
                    Vector3 path = this.direction * (delayInMiliseconds / 1000.0f);
                    Vector3 tempPosition = this.Position + path;
                    
                    // update the height
                    tempPosition.Y = this.terrainInfo.GetHeight(tempPosition.X, tempPosition.Z);

                    // smoothing the velocity.
                    this.direction = (tempPosition - this.Position) / (delayInMiliseconds / 1000.0f);

                    this.Position = tempPosition;
                    this.Velocity = this.direction;
                }
                else
                {
                    this.FindNewTarget();

                    if (this.Target != null)
                    {
                        this.direction = (this.Target.Position - this.Position).Normalize();

                        // update the orientation
                        float orientation = (float)(Math.Atan2(this.direction.X, this.direction.Z) * 180.0f / Math.PI);
                        this.Orientation = orientation;

                        if (this.Mode != CharacterMode.ExecutingAction)
                        {
                            if ((this.Target.Position - this.Position).Magnitude < this.AttackRange)
                            {
                                if (this.Moving > 0)
                                {
                                    if ((this.Target.Position - this.Position).Magnitude < this.AttackRange)
                                    {
                                        this.Velocity = new Vector3(0, 0, 0);
                                        this.Moving = 0;
                                    }
                                }
                                else if (!this.PerformingActions)
                                {
                                    this.Velocity = new Vector3(0, 0, 0);
                                    this.Moving = 0;
                                    this.ExecuteAction(0);
                                }
                            }
                            else
                            {
                                this.direction *= this.ForwardSpeed;
                                Vector3 path = this.direction * (delayInMiliseconds / 1000.0f);
                                Vector3 tempPosition = this.Position + path;

                                // update the height
                                tempPosition.Y = this.terrainInfo.GetHeight(tempPosition.X, tempPosition.Z);

                                // smoothing the velocity.
                                this.direction = (tempPosition - this.Position) / (delayInMiliseconds / 1000.0f);

                                if (this.IsWithinTerrain(tempPosition))
                                {
                                    this.Position = tempPosition;
                                    this.Velocity = this.direction;
                                    this.Moving = 1;
                                }
                                else
                                {
                                    this.Moving = 0;
                                    this.Velocity = new Vector3(0, 0, 0);
                                }
                            }
                        }
                    }
                    else if (this.Moving != 0)
                    {
                        this.Velocity = new Vector3(0, 0, 0);
                        this.Moving = 0;
                    }
                }
            }
            
            base.FixedUpdate(delayInMiliseconds);    
        }

        /// <summary>
        /// Reinitialize character.
        /// </summary>
        public void ReinitCharacter()
        {
            this.Health.Reset();
            this.Mode = CharacterMode.Idle;

            using (MemoryStream eventStream = new MemoryStream(sizeof(int)))
            {
                BinaryWriter writer = new BinaryWriter(eventStream);
                writer.Write((int)CustomEventType.Respawn);
#if DEBUG
                Trace.TraceInformation("Send respawn event");
#endif
                this.facade.SendCustomMessageToRemoteCopies((ISpatialOriginal)this.Avatar, eventStream);
            }
        }

        /// <summary>
        /// Action impact callback, send the information to the remote copies.
        /// </summary>
        /// <param name="action">Action type.</param>
        /// <param name="target">Target character.</param>
        private void SendActionImpact(int action, ISpatialEntity target)
        {
            using (MemoryStream eventStream = new MemoryStream())
            {
                BinaryWriter writer = new BinaryWriter(eventStream);

                if (target != null)
                {
                    float healthImpact = this.Actions[action].HealthImpact();
                    writer.Write((int)CustomEventType.Action);
                    writer.Write((int)action);
                    writer.Write(healthImpact);
                    writer.Write(target.Guid.ToString());

                    if (this.facade.IsLoggedIn)
                    {
#if DEBUG
                        Trace.TraceInformation(string.Format("Send Custom Message To Remote copies, action: {0}, impact: {1}", action, healthImpact));
#endif
                        this.facade.SendCustomMessageToRemoteCopies((ISpatialOriginal)this.Avatar, eventStream);
                    }
                }
            }
        }

        /// <summary>
        /// Finding a new target.
        /// </summary>
        private void FindNewTarget()
        {
            // Finding the replicas within the listen radius if there is one.
            ISpatialReplica target = null;
            float distance = 0.0f;

            foreach (RemoteAvatar remoteAvatar in this.remoteEntities.Values)
            {
                if (remoteAvatar.Health > 0)
                {
                    if (target == null && (this.Position - remoteAvatar.Position).Magnitude < this.listenRadius)
                    {
                        distance = (this.Position - remoteAvatar.Position).Magnitude;
                        target = remoteAvatar;
                    }
                    else
                    {
                        if ((this.Position - remoteAvatar.Position).Magnitude < distance)
                        {
                            distance = (this.Position - remoteAvatar.Position).Magnitude;
                            target = remoteAvatar;
                        }
                    }
                }
            }

            this.Target = target;
        }

        /// <summary>
        /// A method use to check whether the position is a valid coordinate.
        /// </summary>
        /// <param name="position">Position in vector 3.</param>
        /// <returns>Return true if valid.</returns>
        private bool IsWithinTerrain(Vector3 position)
        {
            // NOTE: the Valley area that still visible by the player is
            // X-coordinate : 100 - 450
            // Y-coordinate : -50 - 500
            if (position.X > 100 && position.X < 450 && position.Z > -50 && position.Z < 500)
            {
                return true;
            }

            return false;
        }
    }
}
