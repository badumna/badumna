﻿//-----------------------------------------------------------------------
// <copyright file="RandomWalker.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace TrollBasherNPC.Behaviour
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;
    using TrollBasherNPC.Character;
    using UnityTerrainReader;

    /// <summary>
    /// RandomWalker is one of NPC behaviour, this type of NPC walks randomly on the specified area.
    /// </summary>
    public class RandomWalker : CharacterBehaviour
    {
        /// <summary>
        /// Current NPC direction.
        /// </summary>
        private Vector3 direction = new Vector3(0.0f, 0.0f, 0.0f);

        /// <summary>
        /// Current NPC destination.
        /// </summary>
        private Vector3 destination = new Vector3(0.0f, 0.0f, 0.0f);

        /// <summary>
        /// A value indicating whether the NPC has reach its destination.
        /// </summary>
        private bool reachDestination = true;

        /// <summary>
        /// Random generator.
        /// </summary>
        private Random randomGenerator;

        /// <summary>
        /// Terrain information.
        /// </summary>
        private TerrainInfo terrainInfo;

        /// <summary>
        /// Initializes a new instance of the <see cref="RandomWalker"/> class.
        /// </summary>
        /// <param name="localAvatar">Local avatar.</param>
        public RandomWalker(ISpatialEntity localAvatar)
            : base("RandomWalker", 8)
        {
            this.Avatar = localAvatar;
            this.randomGenerator = new Random();

            // Note: shouldn't be set statically from here.
            this.terrainInfo = new TerrainInfo("heightmap\\Terrain_Valley.bin", new UnityEngine.Vector3(0, 0, -300));
        }

        /// <summary>
        /// Check for the remaining distance to destination, recalculate the destination when the player
        /// has arrived to the current destination.
        /// </summary>
        /// <param name="delayInMiliseconds">Delay in miliseconds.</param>
        public override void FixedUpdate(int delayInMiliseconds)
        {
            if (this.Avatar == null)
            {
                return;
            }

            if (this.reachDestination)
            {
                this.ChooseDestination();
                this.reachDestination = false;
                this.Moving = 0;
            }
            else
            {
                this.direction = (this.destination - this.Position).Normalize();
                this.direction *= this.ForwardSpeed;
                Vector3 path = this.direction * (delayInMiliseconds / 1000.0f);

                // remaining distance in 2D
                float remainingDistance = (float)Math.Sqrt(
                    Math.Pow((this.destination.X - this.Position.X), 2) +
                    Math.Pow((this.destination.Z - this.Position.Z), 2));
                Vector3 path2D = new Vector3(path.X, 0.0f, path.Z);

                if (remainingDistance <= path2D.Magnitude || remainingDistance < 0.5f)
                {
                    this.reachDestination = true;
                }
                else
                {
                    Vector3 tempPosition = this.Position + path;

                    // update the height
                    tempPosition.Y = this.terrainInfo.GetHeight(tempPosition.X, tempPosition.Z);

                    // update the orientation
                    float orientation = (float)(Math.Atan2(this.direction.X, this.direction.Z) * 180.0f / Math.PI);

                    this.Orientation = orientation;
                    this.Position = tempPosition;
                    this.Velocity = this.direction;
                    this.Moving = 1;
                }
            }
        }

        /// <summary>
        /// Choose a new destination.
        /// </summary>
        private void ChooseDestination()
        {
            Vector3 tempDestination = new Vector3(
                this.NextDouble(250f, 300f),
                0.0f,
                this.NextDouble(200f, 250f));

            tempDestination.Y = this.terrainInfo.GetHeight(tempDestination.X, tempDestination.Z);

            this.destination = tempDestination;
#if DEBUG
            ////Trace.TraceInformation("Destination: " + this.destination.ToString());
            Console.WriteLine("Destination: " + this.destination.ToString());
#endif
        }

        /// <summary>
        /// Get the next random double value within the given range.
        /// </summary>
        /// <param name="min">Minimum value.</param>
        /// <param name="max">Maximum value.</param>
        /// <returns>Rerturns the next random double.</returns>
        private float NextDouble(float min, float max)
        {
            return ((float)this.randomGenerator.NextDouble() * (max - min)) + min;
        }
    }
}
