﻿//-----------------------------------------------------------------------
// <copyright file="HealthController.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2011 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace TrollBasherNPC.Character
{
    using System;
    using System.Threading;
    using UnityEngine;

    /// <summary>
    /// HealthController control the health of the character and update it accordingly.
    /// </summary>
    public class HealthController
    {
        /// <summary>
        /// Character behaviour.
        /// </summary>
        private CharacterBehaviour character;

        /// <summary>
        /// Character health.
        /// </summary>
        private float health = 100f;

        /// <summary>
        /// Character current health.
        /// </summary>
        private float currentHealth = 100f;

        /// <summary>
        /// Normalized current health.
        /// </summary>
        private float normalizedCurrentHealth = 1;

        /// <summary>
        /// Inverse health.
        /// </summary>
        private float invHealth;

        /// <summary>
        /// Health impact thread.
        /// </summary>
        private Thread healthImpactThread;

        /// <summary>
        /// Initializes a new instance of the <see cref="HealthController"/> class.
        /// </summary>
        public HealthController()
        {
            this.invHealth = 1f / this.health;
            this.currentHealth = this.health;
        }

        /// <summary>
        /// Gets or sets the current health.
        /// </summary>
        public float CurrentHealth
        {
            get
            {
                return this.currentHealth;
            }

            set
            {
                this.currentHealth = value;
                this.normalizedCurrentHealth = this.currentHealth * this.invHealth;
            }
        }

        /// <summary>
        /// Gets the normalized current health.
        /// </summary>
        public float NormalizedCurrentHealth
        {
            get { return this.normalizedCurrentHealth; }
        }

        /// <summary>
        /// Gets or sets character behaviour.
        /// </summary>
        public CharacterBehaviour Character
        {
            get { return this.character; }
            set { this.character = value; }
        }

        /// <summary>
        /// Gets or sets the maximum health.
        /// </summary>
        internal float MaxHealth
        {
            get 
            { 
                return this.health; 
            }

            set
            {
                this.health = value;
                this.currentHealth = this.health;
            }
        }

        /// <summary>
        /// Reset the health controller.
        /// </summary>
        public void Reset()
        {
            this.currentHealth = this.health;
        }

        /// <summary>
        /// Receive a health impact (damage if impact is negative).
        /// </summary>
        /// <param name="impact">Health impact.</param>
        /// <param name="delay">Delay in seconds</param>
        public void ReceiveHealthImpact(float impact, float delay)
        {
            this.healthImpactThread = new Thread(delegate()
            {
                Thread.Sleep((int)(delay * 1000));
                this.ChangeCurrentHealth(impact);
                if (this.currentHealth == 0 && this.Character.Mode != CharacterMode.Dead && this.Character.Mode != CharacterMode.Respawn)
                {
                    this.Character.Die();
                }
            });

            this.healthImpactThread.IsBackground = true;
            this.healthImpactThread.Start();
        }

        /// <summary>
        /// Change the character current health.
        /// </summary>
        /// <param name="change">The health impact (damage if it is negative).</param>
        public void ChangeCurrentHealth(float change)
        {
            this.currentHealth = Mathf.Clamp(this.currentHealth + change, 0, this.health);
            this.normalizedCurrentHealth = this.currentHealth * this.invHealth;
        }
    }
}
