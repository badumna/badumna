﻿//-----------------------------------------------------------------------
// <copyright file="CharacterAction.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2011 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace TrollBasherNPC.Character
{
    using System;

    /// <summary>
    /// Action class is held the information about an action (e.g. attack action 1)
    /// </summary>
    public class CharacterAction
    {
        /// <summary>
        /// Action name.
        /// </summary>
        private string name;

        /// <summary>
        /// Action duration (seconds).
        /// </summary>
        private float duration;

        /// <summary>
        /// Action cooldown time (seconds).
        /// </summary>
        private float cooldownTime = 3;

        /// <summary>
        /// Main demage.
        /// </summary>
        private float main;

        /// <summary>
        /// Additional damage.
        /// </summary>
        private float addon;

        /// <summary>
        /// Additional damage multiplier.
        /// </summary>
        private int addonMultiplier = 1;

        /// <summary>
        /// Random generator.
        /// </summary>
        private Random randomGenerator;

        /// <summary>
        /// Initializes a new instance of the <see cref="CharacterAction"/> class.
        /// </summary>
        /// <param name="name">Action name.</param>
        /// <param name="duration">Action duration.</param>
        /// <param name="main">Main damage.</param>
        /// <param name="addon">Additional damage.</param>
        public CharacterAction(string name, float duration, float main, float addon)
        {
            this.name = name;
            this.duration = duration;
            this.main = main;
            this.addon = addon;
            this.randomGenerator = new Random((int)DateTime.Now.Ticks);
        }
        
        /// <summary>
        /// Gets the action name.
        /// </summary>
        public string Name
        {
            get { return this.name; }
        }

        /// <summary>
        /// Gets the action duration.
        /// </summary>
        public float Duration
        {
            get { return this.duration; }
        }

        /// <summary>
        /// Gets the action cooldown time.
        /// </summary>
        public float CooldownTime
        {
            get { return this.cooldownTime; }
        }

        /// <summary>
        /// Calculate the health impact due to this action.
        /// </summary>
        /// <returns>Return the health impact (negative if do damage).</returns>
        public float HealthImpact()
        {
            float healthImpact = this.main;
            for (int i = 0; i < this.addonMultiplier; i++)
            {
                healthImpact += (float)this.randomGenerator.NextDouble() * this.addon;
            }

            ////if (this.Type == ActionType.Damage)
            ////{
                healthImpact *= -1f;
            ////}

            return healthImpact;
        }
    }
}
