﻿//-----------------------------------------------------------------------
// <copyright file="CharacterBehaviour.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2011 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace TrollBasherNPC.Character
{
    using System;
    using System.Threading;
    using Badumna.SpatialEntities;
    using UnityEngine;

    /// <summary>
    /// DeathHandle delegate, this delegate will be called when the character died.
    /// </summary>
    public delegate void DeathHandle();

    /// <summary>
    /// ActionImpactCallback will be called when the player fight with other character/enemy.
    /// Applying the health impact on the target character.
    /// </summary>
    /// <param name="action">Action index.</param>
    /// <param name="target">Target character.</param>
    public delegate void ActionImpactCallback(int action, ISpatialEntity target);

    /// <summary>
    /// CharacterBehaviour class has responsibility to determine the character behaviour
    /// given the input from the user or AI. Also store character information.
    /// </summary>
    public class CharacterBehaviour
    {
        /// <summary>
        /// Action object lock.
        /// </summary>
        private readonly object actionLock = new object();

        /// <summary>
        /// It can be either a local or a remote avatar.
        /// </summary>
        private ISpatialEntity avatar;

        /// <summary>
        /// Character health.
        /// </summary>
        private HealthController health;

        /// <summary>
        /// Character name.
        /// </summary>
        private string playerName = string.Empty;

        /// <summary>
        /// Character mode.
        /// </summary>
        private CharacterMode mode = CharacterMode.Idle;

        /// <summary>
        /// Forward speed.
        /// </summary>
        private float forwardSpeed = 10f;

        /// <summary>
        /// Rotate speed.
        /// </summary>
        private float rotateSpeed = 90f;

        /// <summary>
        /// Jump speed.
        /// </summary>
        private float jumpSpeed = 7f;

        /// <summary>
        /// In air gravity factor.
        /// </summary>
        private float inAirGravityFactor = 3f;

        /// <summary>
        /// Character attack range.
        /// </summary>
        private float attackRange = 3f;

        /// <summary>
        /// Character view distance.
        /// </summary>
        private float viewDistance = 50f;

        /// <summary>
        /// A value indicating whether the character resurrect after death.
        /// </summary>
        private bool resurrectAfterDeath = false;

        /// <summary>
        /// Time resurrection delay in seconds.
        /// </summary>
        private float resurectionDelay = 5;

        #region Action members

        /// <summary>
        /// Colletion of available action.
        /// </summary>
        private CharacterAction[] actions;

        /// <summary>
        /// The current action.
        /// </summary>
        private CharacterAction currentAction;

        /// <summary>
        /// A value indicating whether a character is performing action.
        /// </summary>
        private bool performingActions = false;

        /// <summary>
        /// Action impact callback.
        /// </summary>
        private ActionImpactCallback actionImpact;

        /// <summary>
        /// Next action index.
        /// </summary>
        private int nextActionIndex;

        /// <summary>
        /// Action thread.
        /// </summary>
        private Thread actionThread;

        /// <summary>
        /// Action effect thread.
        /// </summary>
        private Thread effectThread;

        #endregion

        /// <summary>
        /// Death handle callback.
        /// </summary>
        private DeathHandle die;

        /// <summary>
        /// Death handle thread.
        /// </summary>
        private Thread deathThread;

        /// <summary>
        /// A value indicating whether the character is on the ground.
        /// </summary>
        private bool onGround = true;

        /// <summary>
        /// Is set to 1 when character is moving.
        /// </summary>
        private int moving = 0;

        /// <summary>
        /// Is set to 1 when the characater is turning.
        /// </summary>
        private int turning = 0;

        /// <summary>
        /// A value indicating whether the character is jumping.
        /// </summary>
        private bool jumping = false;

        /// <summary>
        /// Character current position.
        /// </summary>
        private Badumna.DataTypes.Vector3 position;

        /// <summary>
        /// Character current velocity.
        /// </summary>
        private Badumna.DataTypes.Vector3 velocity;

        /// <summary>
        /// Character current orientation.
        /// </summary>
        private float orientation;

        /// <summary>
        /// A value indicating whether jump is allowed.
        /// </summary>
        private bool jumpAllowed = true;

        /// <summary>
        /// Normalized cooldown time.
        /// </summary>
        private float normalizedCooldown = 0f;

        /// <summary>
        /// Push slope limits
        /// </summary>
        private float pushSlopeLimits = 89;

        /// <summary>
        /// Push step offset.
        /// </summary>
        private float pushStepOffset = 1;

        /// <summary>
        /// Enemy layers mask.
        /// </summary>
        private LayerMask enemyLayers = -1;

        /// <summary>
        /// A value indicating whether this character is being pushed.
        /// </summary>
        private bool beingPushed = false;

        /// <summary>
        /// Push force vector.
        /// </summary>
        private Badumna.DataTypes.Vector3 pushForce = new Badumna.DataTypes.Vector3(0, 0, 0);

        /// <summary>
        /// Current character target.
        /// </summary>
        private ISpatialReplica target;

        /// <summary>
        /// Initializes a new instance of the <see cref="CharacterBehaviour"/> class.
        /// </summary>
        /// <param name="playerName">Player name.</param>
        /// <param name="enemyLayers">Enemy layers.</param>
        public CharacterBehaviour(string playerName, LayerMask enemyLayers)
        {
            this.Die = new DeathHandle(this.IDie);
            this.playerName = playerName;

            this.mode = CharacterMode.Idle;
            this.health = new HealthController();
            this.health.Character = this;
        }

        /// <summary>
        /// Gets or sets character health.
        /// </summary>
        public HealthController Health
        {
            get { return this.health; }
            set { this.health = value; }
        }

        /// <summary>
        /// Gets or sets the character name.
        /// </summary>
        public string PlayerName
        {
            get { return this.playerName; }
            set { this.playerName = value; }
        }

        /// <summary>
        /// Gets or sets the character mode.
        /// </summary>
        public CharacterMode Mode
        {
            get { return this.mode; }
            set { this.mode = value; }
        }

        /// <summary>
        /// Gets or sets the forward speed.
        /// </summary>
        public float ForwardSpeed
        {
            get { return this.forwardSpeed; }
            set { this.forwardSpeed = value; }
        }

        /// <summary>
        /// Gets or sets the death handle callback.
        /// </summary>
        public DeathHandle Die
        {
            get { return this.die; }
            set { this.die = value; }
        }

        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        public Badumna.DataTypes.Vector3 Position
        {
            get { return this.position; }
            set { this.position = value; }
        }

        /// <summary>
        /// Gets or sets the velocity.
        /// </summary>
        public Badumna.DataTypes.Vector3 Velocity
        {
            get { return this.velocity; }
            set { this.velocity = value; }
        }

        /// <summary>
        /// Gets or sets the character orientation.
        /// </summary>
        public float Orientation
        {
            get { return this.orientation; }
            set { this.orientation = value; }
        }

        /// <summary>
        /// Gets or sets the current moving state.
        /// </summary>
        public int Moving
        {
            get { return this.moving; }
            set { this.moving = value; }
        }

        /// <summary>
        /// Gets or sets the current turning state.
        /// </summary>
        public int Turning
        {
            get { return this.turning; }
            set { this.turning = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the character is jumping.
        /// </summary>
        public bool Jumping
        {
            get { return this.jumping; }
            set { this.jumping = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the charcater is on the ground.
        /// </summary>
        public bool OnGround
        {
            get { return this.onGround; }
            set { this.onGround = value; }
        }

        /// <summary>
        /// Gets or sets the character attack range.
        /// </summary>
        public float AttackRange
        {
            get { return this.attackRange; }
            set { this.attackRange = value; }
        }

        /// <summary>
        /// Gets or sets the action list.
        /// </summary>
        public CharacterAction[] Actions
        {
            get { return this.actions; }
            set { this.actions = value; }
        }

        /// <summary>
        /// Gets or sets the character target.
        /// </summary>
        public ISpatialReplica Target
        {
            get 
            { 
                return this.target; 
            }

            set
            {
                lock (this.actionLock)
                {
                    this.target = value;
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether the character currently perfroming an action.
        /// </summary>
        public bool PerformingActions
        {
            get { return this.performingActions; }
        }

        /// <summary>
        /// Gets a value indicating whether the character is in action.
        /// </summary>
        public bool InAction
        {
            get { return this.Mode == CharacterMode.CombatIdle || this.Mode == CharacterMode.ExecutingAction || this.Mode == CharacterMode.Chasing; }
        }

        /// <summary>
        /// Gets or sets the resurection delay.
        /// </summary>
        public float ResurectionDelay
        {
            get { return this.resurectionDelay; }
            set { this.resurectionDelay = value; }
        }

        /// <summary>
        /// Gets a value indicating whether the character is being pushed.
        /// </summary>
        public bool BeingPushed
        {
            get { return this.beingPushed; }
        }

        /// <summary>
        /// Gets the push force vector.
        /// </summary>
        public Badumna.DataTypes.Vector3 PushForce
        {
            get { return this.pushForce; }
        }

        /// <summary>
        /// Gets or sets the avatar.
        /// </summary>
        protected ISpatialEntity Avatar
        {
            get { return this.avatar; }
            set { this.avatar = value; }
        }

        /// <summary>
        /// Death handle callback function.
        /// </summary>
        public void IDie()
        {
            lock (this.actionLock)
            {
                this.mode = CharacterMode.Dead;
                this.target = null;
            }

            this.deathThread = new Thread(this.RespawnDelayed);
            this.deathThread.IsBackground = true;
            this.deathThread.Start();
        }

        /// <summary>
        /// Check for the remaining distance to destination, recalculate the destination when the player
        /// has arrived to the current destination.
        /// </summary>
        /// <param name="delayInMiliseconds">Delay in miliseconds.</param>
        public virtual void FixedUpdate(int delayInMiliseconds)
        {
            if (this.mode == CharacterMode.Dead || this.mode == CharacterMode.Respawn)
            {
                return;
            }

            this.UpdateMode();
        }

        /// <summary>
        /// Update the character mode, and do not update a dead character.
        /// </summary>
        public void UpdateMode()
        {
            if (this.Mode == CharacterMode.Dead || this.Mode == CharacterMode.Respawn)
            {
                return;
            }

            if (this.performingActions)
            {
                if (this.mode != CharacterMode.ExecutingAction)
                {
                    if (this.onGround)
                    {
                        if (this.jumping)
                        {
                            this.mode = CharacterMode.Jumping;
                        }
                        else if (this.moving != 0)
                        {
                            this.mode = CharacterMode.Chasing;
                        }
                        else
                        {
                            this.Mode = CharacterMode.CombatIdle;
                        }
                    }
                }
            }
            else if (this.mode != CharacterMode.Jumping)
            {
                if (this.onGround)
                {
                    if (this.jumping)
                    {
                        this.mode = CharacterMode.Jumping;
                    }
                    else if (this.moving != 0 || this.turning != 0)
                    {
                        this.mode = CharacterMode.RunningForward;
                    }
                    else
                    {
                        this.Mode = CharacterMode.Idle;
                    }
                }
            }
        }

        /// <summary>
        /// Execute action with a given index.
        /// </summary>
        /// <param name="action">Action index.</param>
        public void ExecuteAction(int action)
        {
            this.nextActionIndex = action;
            
            if (!this.performingActions)
            {
                this.actionThread = new Thread(this.ExecuteActions);
                this.actionThread.IsBackground = true;
                this.actionThread.Start();
            }
        }

        /// <summary>
        /// Received damage.
        /// </summary>
        /// <param name="damage">Amount of damage.</param>
        /// <param name="delay">Delay in seconds.</param>
        public void ReceiveDamage(float damage, float delay)
        {
            this.Health.ReceiveHealthImpact(damage, delay);
        }

        /// <summary>
        /// Start push effect
        /// </summary>
        /// <param name="pushForce">Push force vector.</param>
        /// <param name="duration">Duration in seconds.</param>
        /// <param name="delay">Delay in seconds.</param>
        public void StartPush(Badumna.DataTypes.Vector3 pushForce, float duration, float delay)
        {
            this.effectThread = new Thread(
                delegate()
                {
                    Thread.Sleep((int)(delay * 1000));
                    if (!this.beingPushed)
                    {
                        this.beingPushed = true;
                    }

                    this.pushForce += pushForce;

                    Thread.Sleep((int)(duration * 1000));
                    this.pushForce -= pushForce;

                    if (Math.Pow(this.pushForce.Magnitude, 2) < 0.1f)
                    {
                        this.pushForce = new Badumna.DataTypes.Vector3(0, 0, 0);
                        this.beingPushed = false;
                        this.Velocity = new Badumna.DataTypes.Vector3(0, 0, 0);
                    }
                });
            this.effectThread.IsBackground = true;
            this.effectThread.Start();
        }

        /// <summary>
        /// On disable should be called when the troll migrating to another machine.
        /// </summary>
        public void OnDisable()
        {
            this.target = null;

            // Stop all thread before going to destroy the character.
            if (this.actionThread != null && this.actionThread.IsAlive)
            {
                this.actionThread.Abort();
            }

            if (this.effectThread != null && this.effectThread.IsAlive)
            {
                this.effectThread.Abort();
            }

            if (this.deathThread != null && this.deathThread.IsAlive)
            {
                this.deathThread.Abort();
            }
        }

        /// <summary>
        /// Register action impact callback.
        /// </summary>
        /// <param name="actionImpactCallback">Action impact callback.</param>
        protected void RegisterActionImpactCallback(ActionImpactCallback actionImpactCallback)
        {
            this.actionImpact = actionImpactCallback;
        }

        /// <summary>
        /// Execute action in different thread.
        /// </summary>
        private void ExecuteActions()
        {
            lock (this.actionLock)
            {
                this.performingActions = true;
            }

            while (!this.onGround)
            {
                Thread.Sleep(10);
            }

            while (this.performingActions)
            {
                lock (this.actionLock)
                {
                    if (this.target != null && (this.target.Position - this.position).Magnitude > this.attackRange)
                    {
                        break;
                    }

                    if (this.target == null || this.Mode == CharacterMode.Dead)
                    {
                        break;
                    }

                    this.currentAction = this.actions[this.nextActionIndex];

                    if (this.actionImpact != null)
                    {
                        this.actionImpact(this.nextActionIndex, this.target);
                    }

                    this.mode = CharacterMode.ExecutingAction;
                }

                Thread.Sleep((int)(this.currentAction.Duration * 1000));

                if (this.mode == CharacterMode.Dead)
                {
                    break;
                }

                this.mode = CharacterMode.CombatIdle;

                Thread.Sleep((int)(this.currentAction.CooldownTime * 1000));
            }

            lock (this.actionLock)
            {
                this.currentAction = null;
                this.performingActions = false;
            }
        }

        /// <summary>
        /// Respawn delayed.
        /// </summary>
        private void RespawnDelayed()
        {
            Thread.Sleep((int)(this.resurectionDelay * 1000));
            
            lock (this.actionLock)
            {
                this.Mode = CharacterMode.Respawn;
            }
        }
    }
}
