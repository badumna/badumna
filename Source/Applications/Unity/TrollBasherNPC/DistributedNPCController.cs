﻿//-----------------------------------------------------------------------
// <copyright file="DistributedNPCController.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace TrollBasherNPC
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using Badumna;
    using Badumna.Chat;
    using Badumna.Controllers;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;
    using TrollBasherNPC.Behaviour;
    using TrollBasherNPC.Character;

    /// <summary>
    /// DistributedNPCController class is derived from DistrbutedSceneController is used 
    /// to control the NPC using distributed controller mechanism.
    /// </summary>
    public class DistributedNPCController : DistributedSceneController
    {
        /// <summary>
        /// Network facade instance.
        /// </summary>
        private static INetworkFacade network;

        /// <summary>
        /// There will be only one local entity per controller 
        /// </summary>
        private LocalAvatar localEntity;

        /// <summary>
        /// Keep track the numbers of remote entity that still within the sphere interest radius of 
        /// local entity(ies)
        /// </summary>
        private Dictionary<BadumnaId, RemoteAvatar> remoteEntities = new Dictionary<BadumnaId, RemoteAvatar>();

        /// <summary>
        /// Collection of character action.
        /// </summary>
        private Dictionary<PlayerType, CharacterAction[]> characterActionList = new Dictionary<PlayerType, CharacterAction[]>();

        /// <summary>
        /// Last Checkpoint time.
        /// </summary>
        private DateTime lastCheckpoint;

        /// <summary>
        /// Random generator.
        /// </summary>
        private Random randomGenerator;

        /// <summary>
        /// All possible spawn points of NPC.
        /// </summary>
        private Vector3[] spawnPoints = new Vector3[]
        {
            new Vector3(286, 6.5f, 279),
            new Vector3(275, 6, 282),
            new Vector3(362, 7, 235),
            new Vector3(365, 15, 98),
            new Vector3(368, 17, 90)
        };

        #region DistributedSceneController members

        /// <summary>
        /// Initializes a new instance of the <see cref="DistributedNPCController"/> class.
        /// </summary>
        /// <param name="uniqueName">Distributed controller scene name.</param>
        public DistributedNPCController(string uniqueName)
            : base(uniqueName)
        {
            this.randomGenerator = new Random((int)DateTime.Now.Ticks);
            this.InitializingCharacterInformation();
            this.ConstructControlledEntity((uint)PlayerType.RedTroll);
        }

        /// <summary>
        /// Gets or sets network facade instance.
        /// </summary>
        public static INetworkFacade Network
        {
            get { return network; }
            set { network = value; }
        }

        /// <summary>
        /// Override TakeControlOfEntity method, initiate/create NPC character here.
        /// </summary>
        /// <param name="entityId">Entity id.</param>
        /// <param name="remoteEntity">Remote entity.</param>
        /// <param name="entityType">Entity type.</param>
        /// <returns>Return the local avatar instance.</returns>
        protected override ISpatialOriginal TakeControlOfEntity(BadumnaId entityId, ISpatialReplica remoteEntity, uint entityType)
        {
#if DEBUG
            Trace.TraceInformation("(NPC code) Create the instance of RedTroll");
#endif
            this.localEntity = new LocalAvatar(new Vector3(0, 0, 0), 0, "Red Troll", 100f, network);
            this.localEntity.Character = new AttackingTroll(
                this.localEntity,
                this.remoteEntities,
                "Red Troll",
                network,
                this.characterActionList[PlayerType.RedTroll]);
            this.localEntity.Character.Health.MaxHealth = 100;
            this.localEntity.Guid = entityId;

            int index = this.randomGenerator.Next(this.spawnPoints.Length);

#if DEBUG
            Trace.TraceInformation(string.Format("(NPC code) Index of {0} with position {1}", index, this.spawnPoints[index]));
#endif

            this.localEntity.Character.Position = this.spawnPoints[index];
            this.lastCheckpoint = DateTime.Now;
#if DEBUG
            Trace.TraceInformation("(NPC code)  Finish creating red troll");
#endif

            network.ChatSession.SubscribeToProximityChannel(
                entityId,
                delegate(IChatChannel channel, BadumnaId userId, string message)
                {
                    Trace.TraceInformation(message);
                });

            return this.localEntity as ISpatialOriginal;
        }

        /// <summary>
        /// Override InstantiateRemoteEntity method, similar to CreateEntity method on NetworkInitialization.cs
        /// </summary>
        /// <param name="entityId">Entity id.</param>
        /// <param name="entityType">Entity type.</param>
        /// <returns>Return the remote avatar class.</returns>
        protected override ISpatialReplica InstantiateRemoteEntity(BadumnaId entityId, uint entityType)
        {
#if DEBUG
            Trace.TraceInformation("(NPC code) NPC create a remote entity " + entityType + " with id " + entityId);
#endif
            RemoteAvatar remoteAvatar = new RemoteAvatar(new Dictionary<BadumnaId, LocalAvatar>() { { this.localEntity.Guid, this.localEntity } }, this.characterActionList[(PlayerType)entityType]);

            // add to remoteEntities
            this.remoteEntities.Add(entityId, remoteAvatar);

#if DEBUG
            Trace.TraceInformation("(NPC code)Finish creating replica");
#endif
            return remoteAvatar as ISpatialReplica;
        }

        /// <summary>
        /// RemoveEntity callback function.
        /// </summary>
        /// <param name="replica">Remote avatar need to be removed.</param>
        protected override void RemoveEntity(ISpatialReplica replica)
        {
            RemoteAvatar remoteAvatar = (RemoteAvatar)replica;

#if DEBUG
            Trace.TraceInformation("(NPC code) Remove entity with id:" + remoteAvatar.Guid);
#endif

            if (this.remoteEntities.TryGetValue(remoteAvatar.Guid, out remoteAvatar))
            {
                this.remoteEntities.Remove(remoteAvatar.Guid);
            }
        }

        /// <summary>
        /// Override method checkpoint. Write the character state that required to be recovered when the 
        /// NPC migrate from one peer to the others.
        /// </summary>
        /// <param name="writer">Binary writer.</param>
        protected override void Checkpoint(BinaryWriter writer)
        {
            //// Note: The localEntity is recognize as null while we still be able to access the properties of the class.
            //// remove the conditional statement and just try to write everything to the binary stream.
#if DEBUG_CHECKPOINT
        Trace.TraceInformation("(NPC code) Checkpoint");
        Trace.TraceInformation("(NPC code) Starting write checkpoint data");
#endif
            writer.Write(true);
            writer.Write(this.localEntity.Character.Position.X);
            writer.Write(this.localEntity.Character.Position.Y);
            writer.Write(this.localEntity.Character.Position.Z);
            writer.Write(this.localEntity.Character.Orientation);
            writer.Write(this.localEntity.Character.Health.CurrentHealth);

#if DEBUG_CHECKPOINT
        Trace.TraceInformation("(NPC code) Finish write checkpoint data, guid: " + this.localEntity.Guid + " position:" + this.localEntity.transform.position);
#endif
        }

        /// <summary>
        /// Override Process method, call any method that need to be called regularly.
        /// </summary>
        /// <param name="duration">Duration from the last call.</param>
        protected override void Process(TimeSpan duration)
        {
            // Check the death troll 
            this.CheckForDeathTroll();
            this.localEntity.FixedUpdate((int)duration.TotalMilliseconds);

            // Call the replication every 10 second
            DateTime now = DateTime.Now;
            if ((now - this.lastCheckpoint).TotalSeconds > 10)
            {
                this.lastCheckpoint = now;
                this.Replicate();
            }
        }

        /// <summary>
        /// Override ReceiveMessage method, custom message send through Badumna.
        /// </summary>
        /// <param name="message">Message stream.</param>
        /// <param name="source">Sender id.</param>
        protected override void ReceiveMessage(Badumna.Core.MessageStream message, BadumnaId source)
        {
            //// TODO: Implement ReceiveMessage method
        }

        /// <summary>
        /// Override Recover method, it will be called when the NPC migrate to a new peer and recover
        /// from the last checkpoint.
        /// </summary>
        /// <param name="reader">Binary reader.</param>
        protected override void Recover(BinaryReader reader)
        {
#if DEBUG
            Trace.TraceInformation("(NPC code) Recover");
#endif
            bool containsData = reader.ReadBoolean();

            if (containsData)
            {
#if DEBUG
                Trace.TraceInformation("(NPC code) Contains data, try to recover");
#endif
                Vector3 position = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                float orientation = reader.ReadSingle();
                float health = reader.ReadSingle();

                if (this.localEntity != null)
                {
                    this.localEntity.Character.Position = position;
                    this.localEntity.Character.Orientation = orientation;
                    this.localEntity.Character.Health.CurrentHealth = health;

#if DEBUG
                    Trace.TraceInformation("(NPC code) Success to recover the local entity, guid: " + this.localEntity.Guid + " with position: " + position);
#endif
                }
            }
        }

        /// <summary>
        /// Override Recover() method, this will be called only it the last check point data does not exist.
        /// </summary>
        protected override void Recover()
        {
            // Set the NPC using the default value
#if DEBUG
            Trace.TraceInformation("(NPC code) Recover() is called");
#endif
            if (this.localEntity != null)
            {
                int index = this.randomGenerator.Next(this.spawnPoints.Length);
                this.localEntity.Character.Position = this.spawnPoints[index];
            }
        }

        /// <summary>
        /// Override Wake method, it will be called when the controller awake.
        /// </summary>
        protected override void Wake()
        {
#if DEBUG
            Trace.TraceInformation("(NPC code) Controller is awake");
#endif
        }

        /// <summary>
        /// Override Sleep method, it will be called when the controller sleep,
        /// Check whether we should activate a non-active replica of this controller
        /// as the controller where about to migrate to another peer.
        /// </summary>
        protected override void Sleep()
        {
            // We should remove the local entity here
#if DEBUG
            Trace.TraceInformation("(NPC code) Sleep is called, remove entity with entity Id of " + this.localEntity.Guid);
#endif
            this.localEntity.Character.OnDisable();
            this.localEntity = null;
        }

        #endregion

        /// <summary>
        /// Check for the death troll.
        /// </summary>
        private void CheckForDeathTroll()
        {
            if (this.localEntity != null)
            {
                if (this.localEntity.Character.Mode.Equals(CharacterMode.Respawn))
                {
#if DEBUG
                    Trace.TraceInformation("(NPC code) Reinitialize the npc controller.");
#endif
                    int index = this.randomGenerator.Next(this.spawnPoints.Length);
                    this.localEntity.Character.Position = this.spawnPoints[index];
                    ((AttackingTroll)this.localEntity.Character).ReinitCharacter();

#if DEBUG
                    Trace.TraceInformation("(NPC code) Successfully reinit");
#endif
                }
            }
#if DEBUG
            else
            {
                Trace.TraceInformation("(NPC code) local entity is null, cannot check the death troll");
            }
#endif
        }

        /// <summary>
        /// Initializing the character information, currently only used to initialize the action for each
        /// character.
        /// </summary>
        private void InitializingCharacterInformation()
        {
            // initializing character actions
            CharacterAction[] actions = new CharacterAction[3];
            actions[0] = new CharacterAction("Sword slash", 1, 25, 10);
            actions[1] = new CharacterAction("Shield bash", 1.1f, 20, 20);
            actions[2] = new CharacterAction("Kick", 1.6f, 10, 10);

            this.characterActionList.Add(PlayerType.Warrior, actions);

            actions = new CharacterAction[3];
            actions[0] = new CharacterAction("Firebolt", 1.6f, 30, 20);
            actions[1] = new CharacterAction("Blast", 1.5f, 5, 10);
            actions[2] = new CharacterAction("Healing", 1.833334f, 200, 100);

            this.characterActionList.Add(PlayerType.Wizard, actions);

            actions = new CharacterAction[1];
            actions[0] = new CharacterAction("Club bash", 1.333f, 15, 10);

            this.characterActionList.Add(PlayerType.Troll, actions);
            this.characterActionList.Add(PlayerType.RedTroll, actions);
        }
    }
}
