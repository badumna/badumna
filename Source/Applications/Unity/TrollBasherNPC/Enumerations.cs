//-----------------------------------------------------------------------
// <copyright file="Enumerations.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace TrollBasherNPC
{
    using System;
    using Badumna.SpatialEntities;

    /// <summary>
    /// An enumeration of player types, an application specific identification of the 
    /// meshes / objects that is known to all clients. 
    /// </summary>
    /// Enumerations.cs copied from the Enumerations scritp from Badumna Troll Basher source.
    public enum PlayerType
    {
        /// <summary>
        /// Player type : warrior.
        /// </summary>
        Warrior,

        /// <summary>
        /// Player type : wizard.
        /// </summary>
        Wizard,

        /// <summary>
        /// Player type : troll.
        /// </summary>
        Troll,

        /// <summary>
        /// Player type : red troll (aka small troll).
        /// </summary>
        RedTroll
    }

    /// <summary>
    /// <para>
    /// The UpdateParameter enum is used to index which parameter has been modified when generating and 
    /// applying updates. The value is used as the index in the BooleanArray, passed to Serialize and
    /// Deserialize on the LocalAvatar and RemoteAvatar respectively.
    /// </para>
    /// Note: Modify or add new paramater here as required
    /// </summary>
    public enum UpdateParameter : int
    {
        /// <summary>
        /// Orientation angle.
        /// </summary>
        Orientation = SpatialEntityStateSegment.FirstAvailableSegment, //// The first unused index for update parameters

        /// <summary>
        /// Moving property.
        /// </summary>
        Moving,

        /// <summary>
        /// Turning property.
        /// </summary>
        Turning,

        /// <summary>
        /// Player name.
        /// </summary>
        PlayerName,

        /// <summary>
        /// Character health.
        /// </summary>
        Health,

        /// <summary>
        /// Character is on the ground.
        /// </summary>
        OnGround
    }

    /// <summary>
    /// Badumna Troll Basher event type.
    /// </summary>
    public enum CustomEventType
    {
        /// <summary>
        /// Jumping property.
        /// </summary>
        Jumping,

        /// <summary>
        /// Action index.
        /// </summary>
        Action,

        /// <summary>
        /// Respawn the replica.
        /// </summary>
        Respawn,
    }

    /// <summary>
    /// Badumna Troll Basher character modes.
    /// </summary>
    public enum CharacterMode
    {
        /// <summary>
        /// Characate mode: Idle.
        /// </summary>
        Idle,

        /// <summary>
        /// Characate mode: Running forward.
        /// </summary>
        RunningForward,

        /// <summary>
        /// Characate mode: Running backward.
        /// </summary>
        RunningBackward,

        /// <summary>
        /// Characate mode: Jumping.
        /// </summary>
        Jumping,

        /// <summary>
        /// Characate mode: Combat idle.
        /// </summary>
        CombatIdle,

        /// <summary>
        /// Characate mode: Executing action.
        /// </summary>
        ExecutingAction,

        /// <summary>
        /// Characate mode: Chasing.
        /// </summary>
        Chasing,

        /// <summary>
        /// Characate mode: Dead.
        /// </summary>
        Dead,

        /// <summary>
        /// Characate mode: Resurrecting.
        /// </summary>
        Ressurecting,

        /// <summary>
        /// Characate mode: Respawn.
        /// </summary>
        Respawn
    }
}
