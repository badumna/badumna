//-----------------------------------------------------------------------
// <copyright file="TrollBasherNPCProcess.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace TrollBasherNPC
{
    using System;
    using System.IO;
    using GermHarness;

    /// <summary>
    /// TrollBasherNPCProcess class derived from IHostedProcess, and use PeerHarness
    /// to automatically start the badumna process with this process.
    /// </summary>
    public class TrollBasherNPCProcess : IHostedProcess
    {
        /// <summary>
        /// Network initialization instance.
        /// </summary>
        private NetworkInitialization network;

        /// <summary>
        /// The harness hosting this process.
        /// </summary>
        private IPeerHarness peerHarness;

        /// <summary>
        /// Initializes a new instance of the <see cref="TrollBasherNPCProcess"/> class.
        /// </summary>
        /// <param name="peerHarness">The harness hosting this process.</param>
        public TrollBasherNPCProcess(IPeerHarness peerHarness)
        {
            this.peerHarness = peerHarness;
        }

        #region IHostedProcess Members

        /// <summary>
        /// Create a new instance of NetworkInitialization class OnInitialize .
        /// </summary>
        /// <param name="arguments">Passed arguments.</param>
        public void OnInitialize(ref string[] arguments)
        {
            this.network = new NetworkInitialization();
        }

        /// <summary>
        /// Called network fixed update regularly.
        /// </summary>
        /// <param name="delayMilliseconds">Delay in milliseconds.</param>
        /// <returns>Returns true on success.</returns>
        public bool OnPerformRegularTasks(int delayMilliseconds)
        {
            this.network.FixedUpdate(delayMilliseconds);
            return true;
        }

        /// <summary>
        /// Currently we do not use on process request yet.
        /// </summary>
        /// <param name="requestType">Request type.</param>
        /// <param name="request">Request data.</param>
        /// <returns>Returns the request reply.</returns>
        public byte[] OnProcessRequest(int requestType, byte[] request)
        {
            return null;
        }

        /// <summary>
        /// Call network OnDisable() method.
        /// </summary>
        public void OnShutdown()
        {
            this.network.OnDisable();
        }

        /// <summary>
        /// Start the TrollBasher NPC by registering all NPCs.
        /// </summary>
        /// <returns>Returns true on success.</returns>
        public bool OnStart()
        {
            if (this.peerHarness.NetworkFacadeInstance == null)
            {
                Console.WriteLine("Could not retrieve NetworkFacadeInstance from peer harness");
                Environment.Exit(1);
            }

            this.network.Facade = this.peerHarness.NetworkFacadeInstance;
            return this.network.Start();
        }

        /// <summary>
        /// Write option description if available.
        /// </summary>
        /// <param name="tw">Text writer.</param>
        public void WriteOptionsDescription(TextWriter tw)
        {
        }

        #endregion
    }
}
