//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace TrollBasherNPC
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using Badumna;
    using GermHarness;
    using Mono.Options;

    /// <summary>
    /// TrollBasherNPC entry point, initiate the NetworkInitialization instance and start
    /// the npc process.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Application name.
        /// </summary>
        private static string applicationName = "BadumnaTrollBasher-Dev";

        /// <summary>
        /// The peer harness for hosting the process and communicating with Control Center.
        /// </summary>
        private static PeerHarness peerHarness;

        /// <summary>
        /// The arbitration process hosted by the peer harness.
        /// </summary>
        private static TrollBasherNPCProcess process;

        /// <summary>
        /// The option set for parsing command line arguments.
        /// </summary>
        private static OptionSet optionSet = new OptionSet()
            {
                { "h|help", "show this message and exit.", var =>
                    {
                        if (var != null)
                        {
                            Program.WriteUsageAndExit();
                        }
                    }
                }
            };

        /// <summary>
        /// Entry point to the TrollBasherNPC application.
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        internal static void Main(string[] args)
        {
            List<string> extras = null;

            try
            {
                extras = optionSet.Parse(args);

                string[] harnessArgs = extras.ToArray();

                ConfigureBadumna();
                process = new TrollBasherNPCProcess(peerHarness);

                if (peerHarness.Initialize(ref harnessArgs, process))
                {
                    if (harnessArgs.Length > 0)
                    {
                        string assemblyName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
                        Console.Write("{0}: Unknown argument(s):", assemblyName);
                        foreach (string arg in harnessArgs)
                        {
                            Console.Write(" " + arg);
                        }

                        Console.WriteLine(".");
                        Console.WriteLine("Try `{0} --help' for more information.", assemblyName);
                    }
                    else
                    {
                        // install the cancel key handler
                        Console.CancelKeyPress += delegate(object sender, ConsoleCancelEventArgs e)
                        {
                            e.Cancel = true;
                            peerHarness.Stop();
                        };

                        // Start the NPC process.
                        peerHarness.Start();
                    }
                }
                else
                {
                    Console.WriteLine("Could not initialize peer harness.");
                }
            }
            catch (OptionException ex)
            {
                string assemblyName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
                Console.Write("{0}: ", assemblyName);
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try `{0} --help' for more information.", assemblyName);
            }
        }

        /// <summary>
        /// Writes the usage.
        /// </summary>
        private static void WriteUsageAndExit()
        {
            string assemblyName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
            Console.WriteLine("Usage: {0} [Options]", assemblyName);
            Console.WriteLine("Start a TrollBasherNPC on the local machine.");
            Console.WriteLine(string.Empty);
            Console.WriteLine("Options:");
            Program.peerHarness.WriteOptionDescriptions(Console.Out);
            Program.process.WriteOptionsDescription(Console.Out);
            Program.optionSet.WriteOptionDescriptions(Console.Out);
            Environment.Exit(0);
        }

        /// <summary>
        /// Configure badumna network programatically and initialize PeerHarness with this 
        /// configuration.
        /// </summary>
        private static void ConfigureBadumna()
        {
            // set the options programatically
            Options badumnaConfigOptions = new Options();

            // The type of discovery mechanism to use. 
            // Badumna requires the knowledge of one existing peer to connect with in 
            // order to join the network. The address of any other peer can be 'discovered' through
            // SeedPeers(peers that are 'always' running).

            // Use LAN mode for testing
            badumnaConfigOptions.Connectivity.ConfigureForLan();

            // To use on the Internet, comment out the ConfigureForLan() line above and
            // add a known seed peer.  e.g.:
            // badumnaConfigOptions.Connectivity.SeedPeers.Add("216.18.23.185:21251");

            // Disable/enable the use of broadcast
            badumnaConfigOptions.Connectivity.IsBroadcastEnabled = true;

            // The port used for discovery of peers via UDP broadcast.
            badumnaConfigOptions.Connectivity.BroadcastPort = 21250;

            // The broadcast port shouldn't be withtin the peer port range, 
            // otherwise badumna will throw an exception
            badumnaConfigOptions.Connectivity.StartPortRange = 21300;
            badumnaConfigOptions.Connectivity.EndPortRange = 21399;

            // set the application name
            badumnaConfigOptions.Connectivity.ApplicationName = applicationName;

            peerHarness = new PeerHarness(badumnaConfigOptions);
        }
    }
}
