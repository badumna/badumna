//-----------------------------------------------------------------------
// <copyright file="RemoteAvatar.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace TrollBasherNPC
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;
    using TrollBasherNPC.Character;

    /// <summary>
    /// Remote Avatar class implements ISpatialReplica and IDeadReckonable.
    /// </summary>
    /// It will use a very basic implementation.
    public class RemoteAvatar : ISpatialReplica, IDeadReckonable
    {
        /// <summary>
        /// Collection of local entities.
        /// </summary>
        private Dictionary<BadumnaId, LocalAvatar> localEntities;

        /// <summary>
        /// Area of interest radius.
        /// </summary>
        private float areaOfInterestRadius;

        /// <summary>
        /// Remote avatar position.
        /// </summary>
        private Vector3 position;

        /// <summary>
        /// Remote avatar velocity.
        /// </summary>
        private Vector3 velocity;

        /// <summary>
        /// Remote avatar radius.
        /// </summary>
        private float radius;

        /// <summary>
        /// Remote avatar guid.
        /// </summary>
        private BadumnaId guid;

        /// <summary>
        /// Remote avatar current health.
        /// </summary>
        /// (we need to change the health in percentage instead).
        private float health;

        /// <summary>
        /// Collection of character actions.
        /// </summary>
        private CharacterAction[] characterActions;

        /// <summary>
        /// Initializes a new instance of the <see cref="RemoteAvatar"/> class.
        /// </summary>
        /// <param name="localEntities">Collections of local entities.</param>
        /// <param name="actions">Character action list.</param>
        public RemoteAvatar(Dictionary<BadumnaId, LocalAvatar> localEntities, CharacterAction[] actions)
        {
            this.characterActions = actions;
            this.localEntities = localEntities;
            this.radius = 5.0f;
            this.areaOfInterestRadius = 130f;
        }

        /// <summary>
        /// Gets the remote avatar current health.
        /// </summary>
        public float Health
        {
            get { return this.health; }
        }

        #region IDeadReckonable Members

        /// <summary>
        /// Gets or sets the remote avatar velocity.
        /// </summary>
        public Vector3 Velocity
        {
            get { return this.velocity; }
            set { this.velocity = value; }
        }

        #endregion

        #region ISpatialEntity Members

        /// <summary>
        /// Gets or sets the area of interest radius.
        /// </summary>
        public float AreaOfInterestRadius
        {
            get { return this.areaOfInterestRadius; }
            set { this.areaOfInterestRadius = value; }
        }

        /// <summary>
        /// Gets or sets the remote avatar position.
        /// </summary>
        public Vector3 Position
        {
            get { return this.position; }
            set { this.position = value; }
        }

        /// <summary>
        /// Gets or sets the remote avatar radius.
        /// </summary>
        public float Radius
        {
            get { return this.radius; }
            set { this.radius = value; }
        }

        #endregion

        #region IEntity Members

        /// <summary>
        /// Gets or sets the Guid.
        /// </summary>
        public BadumnaId Guid
        {
            get { return this.guid; }
            set { this.guid = value; }
        }

        /// <summary>
        /// Handle incoming custom message.
        /// </summary>
        /// <param name="stream">Badumna stream.</param>
        public void HandleEvent(Stream stream)
        {
            BinaryReader reader = new BinaryReader(stream);
            int eventType = reader.ReadInt32();

            switch (eventType)
            {
                case (int)CustomEventType.Action:
                    int action = reader.ReadInt32();
                    float healthImpact = reader.ReadSingle();
                    string targetGuid = reader.ReadString();
                    
                    foreach (KeyValuePair<BadumnaId, LocalAvatar> pair in this.localEntities)
                    {
                        if (pair.Key.ToString().Equals(targetGuid))
                        {
                            pair.Value.Character.ReceiveDamage(healthImpact, this.characterActions[action].Duration);
#if DEBUG
                            Trace.TraceInformation(string.Format("Received Custom Message from original, action: {0}, impact: {1}", action, healthImpact));
#endif
                            if (this.characterActions[action].Name.Equals("Blast"))
                            {
                                // TODO: move this to some appropriate class.
                                Vector3 pushVector = (pair.Value.Character.Position - this.Position).Normalize();
                                float pushMagnitude = 10;
                                float duration = 2;
                                pair.Value.Character.StartPush(pushVector * pushMagnitude, duration, this.characterActions[action].Duration);
                            }
                        }
                    }

                    break;
                default:
                    break;
            }
        }

        #endregion

        #region ISpatialReplica Members

        /// <summary>
        /// Deserialize method will be called if there is incoming update from the original.
        /// </summary>
        /// <param name="includedParts">Boolean array indicating the properties will be updated.</param>
        /// <param name="stream">Badumna stream.</param>
        /// <param name="estimatedMillisecondsSinceDeparture">Estimated in milliseconds since departure (delay in ms).</param>
        public void Deserialize(BooleanArray includedParts, Stream stream, int estimatedMillisecondsSinceDeparture)
        {
            BinaryReader reader = new BinaryReader(stream);

            if (includedParts[(int)UpdateParameter.Health])
            {
                this.health = reader.ReadSingle();
            }
        }

        #endregion

        /// <summary>
        /// Apply the current position using reckoned position.
        /// </summary>
        /// <param name="reckonedPosition">Reckoned position.</param>
        public void AttemptMovement(Vector3 reckonedPosition)
        {
            this.position = reckonedPosition;
        }
    }
}
