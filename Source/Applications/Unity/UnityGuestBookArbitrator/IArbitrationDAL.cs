﻿//-----------------------------------------------------------------------
// <copyright file="IArbitrationDAL.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace UnityGuestBookArbitrator
{
    /// <summary>
    /// Specifies the interface for database managers handling arbitration requests.
    /// </summary>
    internal interface IArbitrationDAL
    {
        /// <summary>
        /// Prepare the databsae with test data.
        /// </summary>
        void PrepareDB();

        /// <summary>
        /// Prepare for arbitration.
        /// </summary>
        /// <param name="arbitratorName">A string identifying the arbitrator.</param>
        /// <param name="dataProvider">The data provider to use for database access.</param>
        /// <param name="connectionString">A string used to connect to the database.</param>
        /// <remarks>Arbitrate() should not be called before this method is called.</remarks>
        void Initialize(string arbitratorName, string dataProvider, string connectionString);

        /// <summary>
        /// Perform any clean up after arbitration has finished.
        /// </summary>
        /// <remarks>Arbitrate() should not be called after this method is called.</remarks>
        void Shutdown();

        /// <summary>
        /// Insert record to database.
        /// </summary>
        /// <param name="table">The table name.</param>
        /// <param name="itemNames">Item need to be inserted.</param>
        /// <param name="values">The value of the items.</param>
        /// <returns>Return true on success.</returns>
        bool InsertRecord(string table, string[] itemNames, object[] values);

        /// <summary>
        /// Get the record from database.
        /// </summary>
        /// <param name="itemName">The item name.</param>
        /// <param name="table">The table name.</param>
        /// <param name="condition">Condition given.</param>
        /// <returns>Return the record if exist.</returns>
        object GetRecord(string itemName, string table, string condition);
    }
}
