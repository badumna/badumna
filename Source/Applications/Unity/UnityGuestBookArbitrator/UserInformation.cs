﻿//-----------------------------------------------------------------------
// <copyright file="UserInformation.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace UnityGuestBookArbitrator
{
    using System;

    /// <summary>
    /// User information.
    /// </summary>
    internal class UserInformation
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserInformation"/> class.
        /// </summary>
        /// <param name="lastPage">Last page.</param>
        /// <param name="guestbookEntriesCount">Guestbook entires count.</param>
        public UserInformation(int lastPage, int guestbookEntriesCount)
        {
            this.LastPage = lastPage;
            this.GuestbookEntriesCount = guestbookEntriesCount;
        }

        /// <summary>
        /// Gets or sets the last page that has being sent.
        /// </summary>
        public int LastPage { get; set; }

        /// <summary>
        /// Gets or sets the guestbook entries count.
        /// </summary>
        public int GuestbookEntriesCount { get; set; }
    }
}
