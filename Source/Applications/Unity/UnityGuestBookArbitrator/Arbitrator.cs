﻿//-----------------------------------------------------------------------
// <copyright file="Arbitrator.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace UnityGuestBookArbitrator
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Timers;
    using Badumna;
    using UnityGuestBookEvents;

    /// <summary>
    /// Arbitrator class for BadumnaTrollBasher guestbook.
    /// </summary>
    internal class Arbitrator
    {
        /// <summary>
        /// Timers lock object.
        /// </summary>
        private readonly object timerLock = new object();

        /// <summary>
        /// The Data Access Layer (DAL) through which the database is accessed.
        /// </summary>
        private readonly IArbitrationDAL arbitrationDAL;

        /// <summary>
        /// The network facade instance.
        /// </summary>
        private INetworkFacade networkFacade;

        /// <summary>
        /// List of guestbook entries.
        /// </summary>
        private List<GuestbookEntry> guestbookEntries;

        /// <summary>
        /// The collection of user information (connected user).
        /// </summary>
        private Dictionary<int, UserInformation> usersInformation;

        /// <summary>
        /// The guestbook entries per page.
        /// </summary>
        private int entriesPerPage = 10;

        /// <summary>
        /// Expired time (timers) to scheduling a task in this arbitrator.
        /// </summary>
        private List<Action> expiredTimers = new List<Action>();

        /// <summary>
        /// Initializes a new instance of the <see cref="Arbitrator"/> class.
        /// </summary>
        /// <param name="networkFacade">Network facade instance.</param>
        /// <param name="arbitrationDAL">Arbitration data access layer.</param>
        /// <param name="dataProvider">Data provider to use.</param>        
        /// <param name="connectionString">Connection string to use.</param>
        public Arbitrator(INetworkFacade networkFacade, IArbitrationDAL arbitrationDAL, string dataProvider, string connectionString)
        {
            if (arbitrationDAL == null)
            {
                throw new ArgumentNullException("arbitrationDAL");
            }

            this.networkFacade = networkFacade;
            this.arbitrationDAL = arbitrationDAL;
            this.arbitrationDAL.Initialize("Guestbook", dataProvider, connectionString);
            this.usersInformation = new Dictionary<int, UserInformation>();

            this.LoadGuestbookEntries();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Arbitrator"/> class.
        /// </summary>
        /// <param name="networkFacade">Network facade instance.</param>
        /// <param name="dataProvider">Data provider to use.</param>
        /// <param name="connectionString">Database connection string.</param>
        public Arbitrator(INetworkFacade networkFacade, string dataProvider, string connectionString)
            : this(networkFacade, new GuestbookDAL(), dataProvider, connectionString)
        {
        }

        /// <summary>
        /// Receive message from the client, which then this message will be 
        /// deserialize and pass it to Handle function.
        /// </summary>
        /// <param name="sessionId">Session Id.</param>
        /// <param name="message">Received message.</param>
        public void HandleClientEvent(int sessionId, byte[] message)
        {
            ClientEvent clientEvent = (ClientEvent)ClientEventSet.Instance.Deserialize(message);
            Console.WriteLine("{0} is received from session id:{1}", clientEvent.GetType().Name, sessionId);

            RequestGuestbookEvent requestGuestbookEvent = clientEvent as RequestGuestbookEvent;
            if (requestGuestbookEvent != null)
            {
                Console.WriteLine("Request guestbook event");
                this.RequestGuestbookHandler(clientEvent, sessionId);
                return;
            }

            AddToGuestbookEvent addToGuestbookEvent = clientEvent as AddToGuestbookEvent;
            if (addToGuestbookEvent != null)
            {
                Console.WriteLine("Add guestbook event");
                this.AddToGuestbookHandler(clientEvent, sessionId);
            }

            SendClientInfoEvent sendClientInfoEvent = clientEvent as SendClientInfoEvent;
            if (sendClientInfoEvent != null)
            {
                Console.WriteLine("Send client info event");
                this.SendClientInfoHandler(clientEvent, sessionId);
            }
        }

        /// <summary>
        /// This callback function will be called when there is a disconnected Player
        /// </summary>
        /// <param name="sessionId">Session Ud</param>
        public void HandleClientDisconnect(int sessionId)
        {
            this.RemoveUserInformation(sessionId);
        }

        /// <summary>
        /// Called to tidy up when finished with arbitrator.
        /// </summary>
        public void Shutdown()
        {
            this.arbitrationDAL.Shutdown();
        }

        /// <summary>
        /// Both tick and schedule are used to schedule the event that would be sent back to the client.
        /// </summary>
        public void Tick()
        {
            List<Action> expired;
            lock (this.timerLock)
            {
                expired = this.expiredTimers;
                this.expiredTimers = new List<Action>();
            }

            foreach (Action action in expired)
            {
                action();
            }
        }

        /// <summary>
        /// Both tick and schedule are used to schedule the event that would be sent back to the client
        /// </summary>
        /// <param name="delay">Time delay.</param>
        /// <param name="action">Scheduled task.</param>
        public void Schedule(TimeSpan delay, Action action)
        {
            Timer timer = new Timer
            {
                AutoReset = false,
                Interval = delay.TotalMilliseconds
            };
            timer.Elapsed +=
                delegate
                {
                    lock (this.timerLock)
                    {
                        this.expiredTimers.Add(action);
                    }
                };
            timer.Start();
        }

        /// <summary>
        /// Send the event from the server to clients.
        /// </summary>
        /// <param name="destinationSessionId">Destination session id.</param>
        /// <param name="serverEvent">The server event need to be sent.</param>
        private void SendServerEvent(int destinationSessionId, ServerEvent serverEvent)
        {
            this.networkFacade.SendServerArbitrationEvent(
                destinationSessionId,
                ServerEventSet.Instance.Serialize(serverEvent));
        }

        /// <summary>
        /// Load the guestbook entries from database.
        /// </summary>
        private void LoadGuestbookEntries()
        {
            // fetch all the guestbook entries from database and store it in ArbitrationServer, this will reduce the database access cost
            this.guestbookEntries = (List<GuestbookEntry>)this.arbitrationDAL.GetRecord("player_name,time,messages", "guestbook", "*");
        }

        /// <summary>
        /// Request guestbook event handler.
        /// </summary>
        /// <param name="clientEvent">Client event.</param>
        /// <param name="sessionId">Session id.</param>
        private void RequestGuestbookHandler(ClientEvent clientEvent, int sessionId)
        {
            List<GuestbookEntry> records = new List<GuestbookEntry>();
            UserInformation userInfo;
            int lastPage, numEntries, start, end;

            if (!this.usersInformation.TryGetValue(sessionId, out userInfo))
            {
                // new user ask for the first page, we should send back 20 records from guestbook entries (the first two pages)
                userInfo = new UserInformation(1, this.guestbookEntries.Count);
                this.usersInformation.Add(sessionId, userInfo);
            }

            lastPage = userInfo.LastPage;
            numEntries = userInfo.GuestbookEntriesCount;

            if (lastPage == 1)
            {
                start = numEntries - 1;
                end = start - 19;

                if (numEntries < 20)
                {
                    end = 0;
                }
            }
            else
            {
                if (numEntries < 20)
                {
                    // no need to send back any other record, all records has been sent
                    start = -1;
                    end = 0;
                }
                else if (numEntries < ((lastPage + 1) * this.entriesPerPage))
                {
                    start = numEntries - (lastPage * this.entriesPerPage) - 1;
                    end = 0;
                }
                else
                {
                    start = numEntries - (lastPage * this.entriesPerPage) - 1;
                    end = start + 1 - this.entriesPerPage;
                }
            }

            for (int i = start; i >= end; i--)
            {
                records.Add(this.guestbookEntries[i]);
            }

            if (records.Count > 0)
            {
                userInfo.LastPage = lastPage + 1;
                this.usersInformation[sessionId] = userInfo;
                this.SendServerEvent(sessionId, new SendGuestbookMessageEvent(records));
            }
        }

        /// <summary>
        /// Add to guestbook event handler
        /// </summary>
        /// <param name="clientEvent">Client event.</param>
        /// <param name="sessionId">Session id.</param>
        private void AddToGuestbookHandler(ClientEvent clientEvent, int sessionId)
        {
            AddToGuestbookEvent guestbookEvent = (AddToGuestbookEvent)clientEvent;
            string[] itemNames = { "id", "player_name", "time", "messages" };
            object[] values = { guestbookEntries.Count + 1, guestbookEvent.PlayerName, guestbookEvent.Time.Ticks, guestbookEvent.Message };

            this.guestbookEntries.Add(new GuestbookEntry(guestbookEvent.Message, guestbookEvent.PlayerName, guestbookEvent.Time.Ticks));
            try
            {
                this.arbitrationDAL.InsertRecord("guestbook", itemNames, values);

                // Write to output.txt in case there is an exception (backup the guestbook entry)
                using (StreamWriter writer = new StreamWriter("output.txt", true))
                {
                    writer.WriteLine("{0};{1};{2}", guestbookEvent.PlayerName, guestbookEvent.Time.Ticks, guestbookEvent.Message);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error message: {0}", e.Message);
                Console.WriteLine("{0};{1};{2}", guestbookEvent.PlayerName, guestbookEvent.Time.ToString(), guestbookEvent.Message);
            }
        }

        /// <summary>
        /// Send the client info event handler.
        /// </summary>
        /// <param name="clientEvent">Client event.</param>
        /// <param name="sessionId">Session id.</param>
        private void SendClientInfoHandler(ClientEvent clientEvent, int sessionId)
        {
            SendClientInfoEvent clientInfoEvent = (SendClientInfoEvent)clientEvent;
            string[] itemNames = { "session_id", "nat_type", "ip_hash" };
            object[] values = { sessionId, clientInfoEvent.NatType, clientInfoEvent.HashValueOfIpAddress };

            // Add to database here, and create a backup into a .txt file as well
            try
            {
                this.arbitrationDAL.InsertRecord("client_info", itemNames, values);

                // Write to client_info.txt in case there is an exception (backup the guestbook entry)
                using (StreamWriter writer = new StreamWriter("client_info.txt", true))
                {
                    writer.WriteLine("{0};{1};{2}", sessionId, clientInfoEvent.NatType, clientInfoEvent.HashValueOfIpAddress);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error message: {0}", e.Message);
                Console.WriteLine("{0};{1};{2}", sessionId, clientInfoEvent.NatType, clientInfoEvent.HashValueOfIpAddress);
            }
        }

        /// <summary>
        /// Remove user information when the user is not already online or disconnected.
        /// </summary>
        /// <param name="sessionId">Session id.</param>
        private void RemoveUserInformation(int sessionId)
        {
            if (this.usersInformation.ContainsKey(sessionId))
            {
                this.usersInformation.Remove(sessionId);
            }
        }
    }
}
