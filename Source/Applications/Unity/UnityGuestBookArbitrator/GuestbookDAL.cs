﻿//-----------------------------------------------------------------------
// <copyright file="GuestbookDAL.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace UnityGuestBookArbitrator
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using UnityGuestBookEvents;

    /// <summary>
    /// Specifies the interface for database managers handling arbitration requests.
    /// </summary>
    internal class GuestbookDAL : IArbitrationDAL
    {
        /// <summary>
        /// A connection to the database.
        /// </summary>
        private DbConnection databaseConnection;

        /// <summary>
        /// The name of the arbitrator.
        /// </summary>
        private string name = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="GuestbookDAL"/> class.
        /// </summary>
        public GuestbookDAL()
        {
        }

        /// <summary>
        /// Prepare the database with test data.
        /// </summary>
        public void PrepareDB()
        {
        }

        /// <summary>
        /// Prepare for the arbitration.
        /// </summary>
        /// <param name="name">Arbirtrator name.</param>
        /// <param name="dataProvider">The data provider to use.</param>
        /// <param name="connectionString">A connection string to use.</param>
        public void Initialize(string name, string dataProvider, string connectionString)
        {
            this.name = name;
            DbProviderFactory factory = DbProviderFactories.GetFactory(dataProvider);
            this.databaseConnection = factory.CreateConnection();
            this.databaseConnection.ConnectionString = connectionString;
            this.databaseConnection.Open();

            this.InitializeDatabase();
        }

        /// <summary>
        /// Perform any clean up after arbitration has finished.
        /// </summary>
        public void Shutdown()
        {
            if (this.databaseConnection != null)
            {
                this.databaseConnection.Close();
                this.databaseConnection = null;
            }
        }

        /// <summary>
        /// Get the record from database <seealso cref="IDatabaseManager.GetRecord(string, string, string)"/>.
        /// </summary>
        /// <param name="itemName">The item name.</param>
        /// <param name="table">The table name.</param>
        /// <param name="condition">Condition given.</param>
        /// <returns>Return the record if exist.</returns>
        public object GetRecord(string itemName, string table, string condition)
        {
            if (string.IsNullOrEmpty(table) || string.IsNullOrEmpty(condition) || string.IsNullOrEmpty(itemName))
            {
                return null;
            }

            object result = null;

            using (DbCommand command = this.databaseConnection.CreateCommand())
            {
                List<GuestbookEntry> guestbookEntries = new List<GuestbookEntry>();

                if (condition.Contains(";"))
                {
                    // most likely this could be a SQL injection
                    return null; 
                }
                else if (condition.Equals("*"))
                {
                    // to make sure the records have a correct order
                    command.CommandText = string.Format("SELECT {0} FROM {1} ORDER BY id", itemName, table);
                }
                else
                {
                    command.CommandText = string.Format("SELECT {0} FROM {1} WHERE {2} ORDER BY id", itemName, table, condition);
                }

                using (IDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        guestbookEntries.Add(new GuestbookEntry((string)reader[2], (string)reader[0], long.Parse((string)reader[1])));
                    }

                    result = guestbookEntries;
                }
            }

            return result;
        }

        /// <summary>
        /// Insert new record to database <seealso cref="IDatabaseManager.InsertRecord(string, string[], object[])"/>.
        /// </summary>
        /// <param name="table">The table name.</param>
        /// <param name="itemNames">The item names need to be inserted.</param>
        /// <param name="values">The value of the item names.</param>
        /// <returns>Return true on success.</returns>
        public bool InsertRecord(string table, string[] itemNames, object[] values)
        {
            if (string.IsNullOrEmpty(table) || itemNames.Length <= 0 || values.Length <= 0)
            {
                return false;
            }

            using (DbCommand command = this.databaseConnection.CreateCommand())
            {
                string fieldName = string.Empty;
                string fieldValue = string.Empty;
                
                if (itemNames.Length != values.Length)
                {
                    return false;
                }

                for (int i = 0; i < itemNames.Length; i++)
                {
                    IDbDataParameter parameter = command.CreateParameter();
                    parameter.ParameterName = "@" + itemNames[i];
                    parameter.Value = values[i];
                    command.Parameters.Add(parameter);

                    fieldName += itemNames[i];
                    fieldValue += parameter.ParameterName;

                    if (i != (itemNames.Length - 1))
                    {
                        fieldName += ',';
                        fieldValue += ',';
                    }
                }

                command.CommandText = string.Format(@"INSERT INTO {0} ({1}) VALUES({2})", table, fieldName, fieldValue);

                int rowsUpdated = command.ExecuteNonQuery();
                return rowsUpdated == 1;
            }
        }

        /// <summary>
        /// Initialize guestbook arbitration database.
        /// </summary>
        private void InitializeDatabase()
        {
            if (!this.TableExists("guestbook"))
            {
                DbCommand command = this.databaseConnection.CreateCommand();
                command.CommandText = "CREATE TABLE guestbook (id int(11) NOT NULL, player_name char(20) NOT NULL, time char(64) NOT NULL, messages varchar(1000) NOT NULL, PRIMARY KEY (id))";
                command.ExecuteNonQuery();
            }

            if (!this.TableExists("client_info"))
            {
                DbCommand command = this.databaseConnection.CreateCommand();
                command.CommandText = "CREATE TABLE client_info (session_id int(4) NOT NULL, nat_type char(30) NOT NULL, ip_hash char(32) NOT NULL)";
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Check to see if the table exists.
        /// </summary>
        /// <param name="tableName">The name of the table.</param>
        /// <returns>Return true if the given table name exists.</returns>
        private bool TableExists(string tableName)
        {
            DataTable dataTable = this.databaseConnection.GetSchema(
                "TABLES",
                new string[] { null, null, tableName });
            
            return dataTable.Rows.Count > 0;
        }

        /// <summary>
        /// Execute a SQL non query
        /// </summary>
        /// <param name="sql">The SQL command.</param>
        /// <param name="tx"> The transaction to wrap the query in.</param>
        /// <returns>The number of rows affected.</returns>
        private int ExecuteNonQuery(string sql, DbTransaction tx)
        {
            using (DbCommand command = this.databaseConnection.CreateCommand())
            {
                command.CommandText = sql;
                command.Transaction = tx;
                int rowsAffected = command.ExecuteNonQuery();
                return rowsAffected;
            }
        }
    }
}
