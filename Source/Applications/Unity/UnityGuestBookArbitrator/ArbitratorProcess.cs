//-----------------------------------------------------------------------
// <copyright file="ArbitratorProcess.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace UnityGuestBookArbitrator
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Text;
    using Badumna;
    using GermHarness;
    using Mono.Options;

    /// <summary>
    /// Hosted process  to provide test guestbook arbitration service for BTB.
    /// </summary>
    internal class ArbitratorProcess : IHostedProcess
    {
        /// <summary>
        /// The arbitrator.
        /// </summary>
        private Arbitrator arbitrator;

        /// <summary>
        /// The harness hosting this process.
        /// </summary>
        private IPeerHarness peerHarness;

        /// <summary>
        /// Initializes a new instance of the <see cref="ArbitratorProcess"/> class.
        /// </summary>
        /// <param name="peerHarness">The harness hosting this process.</param>
        public ArbitratorProcess(IPeerHarness peerHarness)
        {
            if (peerHarness == null)
            {
                throw new ArgumentNullException("peerHarness");
            }

            this.peerHarness = peerHarness;
        }

        #region IHostedProcess Members

        /// <summary>
        /// Called by process host to initialize the process.
        /// </summary>
        /// <param name="arguments">Command line arguments passed by process host.</param>
        public void OnInitialize(ref string[] arguments)
        {
        }

        /// <summary>
        /// Called periodically by the process host, to trigger regularly scheduled processing.
        /// </summary>
        /// <param name="delayMilliseconds">Milliseconds since last called.</param>
        /// <returns>Flag indicating success or errors.</returns>
        public bool OnPerformRegularTasks(int delayMilliseconds)
        {
            if (this.arbitrator != null)
            {
                this.arbitrator.Tick();
            }

            return true;
        }

        /// <summary>
        /// Called by the process host to pass a request.
        /// Does nothing, but is required by interface.
        /// </summary>
        /// <param name="requestType">Type of the request (usually cast from enum)</param>
        /// <param name="request">The request that the process should know how to handle.</param>
        /// <returns>Response for host.</returns>
        public byte[] OnProcessRequest(int requestType, byte[] request)
        {
            return null;
        }

        /// <summary>
        /// Called by the process host on shutdown.
        /// </summary>
        public void OnShutdown() 
        {
            this.arbitrator.Shutdown();
        }

        /// <summary>
        /// Called by process host on start.
        /// </summary>
        /// <returns>Success flag (always true at present).</returns>
        public bool OnStart()
        {
            string dataProvider = ConfigurationManager.AppSettings["dbProvider"];
            string connectionString = ConfigurationManager.ConnectionStrings[dataProvider].ConnectionString;

            // registering the ArbitrationEventHandler
            this.arbitrator = new Arbitrator(this.peerHarness.NetworkFacadeInstance, dataProvider, connectionString);
            this.peerHarness.NetworkFacadeInstance.RegisterArbitrationHandler(
                this.arbitrator.HandleClientEvent, 
                TimeSpan.FromMinutes(10), 
                this.arbitrator.HandleClientDisconnect);
            Console.WriteLine("The arbitrator is in session.");
            return true;
        }

        /// <summary>
        /// Write out supported command line options.
        /// </summary>
        /// <param name="tw">The text writer to use.</param>
        public void WriteOptionsDescription(TextWriter tw)
        {
            // No process-specific options.
        }

        #endregion
    }
}
