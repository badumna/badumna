//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace UnityGuestBookArbitrator
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using Badumna;
    using Badumna.ServiceDiscovery;
    using GermHarness;
    using Mono.Options;
    using Badumna.Security;

    /// <summary>
    /// Program class to hold main routine.
    /// </summary>
    internal class Program
    {        
        /// <summary>
        /// Application name.
        /// </summary>
        private static string applicationName = "BadumnaTrollBasher-Dev";

        /// <summary>
        /// The peer harness for hosting the process and communicating with Control Center.
        /// </summary>
        private static PeerHarness peerHarness;

        /// <summary>
        /// The arbitration process hosted by the peer harness.
        /// </summary>
        private static ArbitratorProcess process;

        /// <summary>
        /// The option set for parsing command line arguments.
        /// </summary>
        private static OptionSet optionSet = new OptionSet()
            {
                { "h|help", "show this message and exit.", var =>
                    {
                        if (var != null)
                        {
                            Program.WriteUsageAndExit();
                        }
                    }
                }
            };

        /// <summary>
        /// Entry point to program.
        /// Creates a PeerHarness and uses it to run the arbitration process.
        /// Command line arguments accepted: [--db_config=database] [--dei_config=deiConfig]
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        internal static void Main(string[] args)
        {
            List<string> extras = null;
            try
            {
                extras = optionSet.Parse(args);

                string[] harnessArgs = extras.ToArray();

                ConfigureBadumna();
                
                // For arbitration server, reduce the process network state interval to 10ms.   
                peerHarness.ProcessNetworkStateIntervalMs = 10;
                
                process = new ArbitratorProcess(peerHarness);

                if (peerHarness.Initialize(ref harnessArgs, process))
                {
                    if (harnessArgs.Length > 0)
                    {
                        string assemblyName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
                        Console.Write("{0}: Unknown argument(s):", assemblyName);
                        foreach (string arg in harnessArgs)
                        {
                            Console.Write(" " + arg);
                        }

                        Console.WriteLine(".");
                        Console.WriteLine("Try `{0} --help' for more information.", assemblyName);
                    }
                    else
                    {
                        // Start the arbitration process.
                        peerHarness.RegisterService(ServerType.Arbitration);
                        peerHarness.Start();
                    }
                }
                else
                {
                    Console.WriteLine("Could not initialize peer harness.");
                }
            }
            catch (OptionException ex)
            {
                string assemblyName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
                Console.Write("{0}: ", assemblyName);
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try `{0} --help' for more information.", assemblyName);
                Environment.Exit((int)ExitCode.InvalidArguments);
            }
            catch (DeiAuthenticationException ex)
            {
                Console.WriteLine(ex.Message);
                Environment.Exit((int)ExitCode.DeiAuthenticationFailed);
            }
            catch (SecurityException ex)
            {
                Console.WriteLine("Exception caught : " + ex.Message);
                Environment.Exit((int)ExitCode.AnnounceServiceFailed);
            }
        }

        /// <summary>
        /// Writes the usage.
        /// </summary>
        private static void WriteUsageAndExit()
        {
            string assemblyName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
            Console.WriteLine("Usage: {0} [Options]", assemblyName);
            Console.WriteLine("Start a Guestbook Arbitration Server on the local machine.");
            Console.WriteLine(string.Empty);
            Console.WriteLine("Options:");
            if (peerHarness == null)
            {
                ConfigureBadumna();
            }

            Program.peerHarness.WriteOptionDescriptions(Console.Out);
            Program.optionSet.WriteOptionDescriptions(Console.Out);
            Environment.Exit((int)ExitCode.DisplayHelp);
        }

        /// <summary>
        /// Configure badumna network programatically and initialize PeerHarness with this
        /// configuration.
        /// </summary>
        private static void ConfigureBadumna()
        {
            // set the options programatically
            Options badumnaConfigOptions = new Options();

            // The type of discovery mechanism to use. 
            // Badumna requires the knowledge of one existing peer to connect with in 
            // order to join the network. The address of any other peer can be 'discovered' through
            // SeedPeers(peers that are 'always' running).

            // Use LAN mode for testing
            badumnaConfigOptions.Connectivity.ConfigureForLan();

            // To use on the Internet, comment out the ConfigureForLan() line above and
            // add a known seed peer.  e.g.:
            //badumnaConfigOptions.Connectivity.SeedPeers.Add("128.250.77.144:21251");

            // Disable/enable the use of broadcast
            badumnaConfigOptions.Connectivity.IsBroadcastEnabled = true;

            // The port used for discovery of peers via UDP broadcast.
            badumnaConfigOptions.Connectivity.BroadcastPort = 21250;

            // The broadcast port shouldn't be withtin the peer port range, 
            // otherwise badumna will throw an exception
            badumnaConfigOptions.Connectivity.StartPortRange = 21260;
            badumnaConfigOptions.Connectivity.EndPortRange = 21260;

            // set the application name
            badumnaConfigOptions.Connectivity.ApplicationName = applicationName;

            // Arbitration server setting
            badumnaConfigOptions.Arbitration.Servers.Add(new ArbitrationServerDetails("Guestbook"));

            peerHarness = new PeerHarness(badumnaConfigOptions);
        }
    }
}