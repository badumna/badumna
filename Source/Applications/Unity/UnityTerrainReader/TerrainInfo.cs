﻿//-----------------------------------------------------------------------
// <copyright file="TerrainInfo.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace UnityTerrainReader
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using UnityEngine;

    /// <summary>
    /// Store the information about a terrain.
    /// </summary>
    public class TerrainInfo
    {
        /// <summary>
        /// File name where the terrain information is stored.
        /// </summary>
        private string fileName;

        /// <summary>
        /// Terrain size.
        /// </summary>
        private int terrainSize;

        /// <summary>
        /// Terrain height maps;
        /// </summary>
        private float[,] heightMaps;

        /// <summary>
        /// Terrain coordinate scale.
        /// </summary>
        private Vector3 scale;

        /// <summary>
        /// Terrain absolute position.
        /// </summary>
        private Vector3 terrainCoordinate;

        /// <summary>
        /// Initializes a new instance of the <see cref="TerrainInfo"/> class.
        /// </summary>
        /// <param name="fileName">File name.</param>
        public TerrainInfo(string fileName)
            : this(fileName, Vector3.zero)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TerrainInfo"/> class. 
        /// </summary>
        /// <param name="fileName">File name.</param>
        /// <param name="terrainCoordinate">Terrain absolute position.</param>
        public TerrainInfo(string fileName, Vector3 terrainCoordinate)
        {
            this.fileName = fileName;
            this.terrainCoordinate = terrainCoordinate;
            this.ProcessTerrainInfo();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TerrainInfo"/> class.
        /// </summary>
        /// <param name="terrain">Terrain object.</param>
        /// <param name="terrainCoordinate">Terrain absolute position.</param>
        public TerrainInfo(Terrain terrain, Vector3 terrainCoordinate)
        {
            this.terrainCoordinate = terrainCoordinate;
            this.ProcessTerrainInfo(terrain);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TerrainInfo"/> class.
        /// </summary>
        /// <param name="terrainData">Terrain data.</param>
        /// <param name="terrainCoordinate">Terrain absolute position.</param>
        public TerrainInfo(TerrainData terrainData, Vector3 terrainCoordinate)
        {
            this.terrainCoordinate = terrainCoordinate;
            this.ProcessTerrainInfo(terrainData);
        }

        /// <summary>
        /// Gets or sets the terrain size.
        /// </summary>
        public int TerrainSize
        {
            get { return this.terrainSize; }
            set { this.terrainSize = value; }
        }

        /// <summary>
        /// Get the heights at the given coordinate.
        /// </summary>
        /// <param name="x">X world cooridnate.</param>
        /// <param name="z">Z world coordinate.</param>
        /// <returns>The height world cooridnate at given x,z world coordinate.</returns>
        public float GetHeight(float x, float z)
        {
            // Normalize the game coordinate.
            x -= this.terrainCoordinate.x;
            z -= this.terrainCoordinate.z;

            // Scaled the game coordinate to height map coordinate;
            x /= this.scale.x;
            z /= this.scale.z;

            int x1 = (int)Math.Floor(x);
            int x2 = x1 + 1;

            int z1 = (int)Math.Floor(z);
            int z2 = z1 + 1;

            // Use Bilinear interpolation to get the height.
            float height = (this.heightMaps[z1, x1] * (x2 - x) * (z2 - z) / (x2 - x1) / (z2 - z1)) +
                (this.heightMaps[z1, x2] * (x - x1) * (z2 - z) / (x2 - x1) / (z2 - z1)) +
                (this.heightMaps[z2, x1] * (x2 - x) * (z - z1) / (x2 - x1) / (z2 - z1)) +
                (this.heightMaps[z2, x2] * (x - x1) * (z - z1) / (x2 - x1) / (z2 - z1));

            return (height * this.scale.y) + this.terrainCoordinate.y;
        }

        /// <summary>
        /// Override object to string method.
        /// </summary>
        /// <returns>Return a string stored the information of the terrain name and size.</returns>
        public override string ToString()
        {
            return string.Format("- {0} - size: {1}\n", this.fileName, this.terrainSize);
        }

        /// <summary>
        /// Read and process the terrain information from a binary file and store it temporarily into
        /// memory for reading.
        /// </summary>
        private void ProcessTerrainInfo()
        {
            FileInfo fileInfo = new FileInfo(this.fileName);
            this.terrainSize = (int)Math.Sqrt((fileInfo.Length - 12) / 4);

            using (BinaryReader reader = new BinaryReader(File.OpenRead(this.fileName)))
            {
                // Get the vector scale first.
                this.scale = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                this.heightMaps = new float[this.terrainSize, this.terrainSize];

                for (int y = 0; y < this.terrainSize; y++)
                {
                    for (int x = 0; x < this.terrainSize; x++)
                    {
                        this.heightMaps[x, y] = reader.ReadSingle();
                    }
                }
            }
        }

        /// <summary>
        /// Process the terrain information from a given unity terrain object.
        /// </summary>
        /// <param name="terrain">Terrain object.</param>
        private void ProcessTerrainInfo(Terrain terrain)
        {
            int w = terrain.terrainData.heightmapWidth;
            int h = terrain.terrainData.heightmapHeight;
            Vector3 meshScale = terrain.terrainData.size;

            this.terrainSize = terrain.terrainData.heightmapWidth;
            this.scale = new Vector3(meshScale.x / (h - 1), meshScale.y, meshScale.z / (w - 1));
            this.heightMaps = terrain.terrainData.GetHeights(0, 0, w, h);
            this.fileName = terrain.name;
        }

        /// <summary>
        /// Process the terrain information from a given unity terrain object.
        /// </summary>
        /// <param name="terrainData">Terrain data.</param>
        private void ProcessTerrainInfo(TerrainData terrainData)
        {
            int w = terrainData.heightmapWidth;
            int h = terrainData.heightmapHeight;
            Vector3 meshScale = terrainData.size;

            this.terrainSize = terrainData.heightmapWidth;
            this.scale = new Vector3(meshScale.x / (h - 1), meshScale.y, meshScale.z / (w - 1));
            this.heightMaps = terrainData.GetHeights(0, 0, w, h);
            this.fileName = terrainData.name;
        }
    }
}
