﻿//-----------------------------------------------------------------------
// <copyright file="FriendListEventSet.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace DemoArbitrationEvents
{
    using System.Collections.Generic;
    using System.IO;
    using Badumna.Arbitration;

    /// <summary>
    /// Events used for communication between arbitration server and client
    /// </summary>
    public sealed class FriendListEventSet
    {
        /// <summary>
        /// The arbitrationEventSet to use to serialize and deserialize arbitration events.
        /// </summary>
        private static ArbitrationEventSet eventSet = CreateEventSet();

        /// <summary>
        /// Prevents a default instance of the FriendListEventSet class from being created.
        /// </summary>
        /// <remarks>Private constructor to prevent creation of public default constructor.</remarks>
        private FriendListEventSet()
        {
        }

        /// <summary>
        /// Deserialize an arbitration event.
        /// </summary>
        /// <param name="message">A serialized arbitration event.</param>
        /// <returns>The deserialized arbitration event.</returns>
        public static ArbitrationEvent Deserialize(byte[] message)
        {
            return eventSet.Deserialize(message);
        }

        /// <summary>
        /// Serialize an arbitrationEvent.
        /// </summary>
        /// <param name="arbitrationEvent">The arbitration event.</param>
        /// <returns>The serialized arbitration event.</returns>
        public static byte[] Serialize(ArbitrationEvent arbitrationEvent)
        {
            return eventSet.Serialize(arbitrationEvent);
        }

        /// <summary>
        /// Create an arbitration event set containing all arbitration events.
        /// </summary>
        /// <returns>A new arbitration event set.</returns>
        private static ArbitrationEventSet CreateEventSet()
        {
            eventSet = new ArbitrationEventSet();
            eventSet.Register(
                typeof(FriendListRequest),
                typeof(FriendListReply));
            return eventSet;
        }
    }
}
