﻿//-----------------------------------------------------------------------
// <copyright file="FriendListReply.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace DemoArbitrationEvents
{
    using System.Collections.ObjectModel;
    using System.IO;
    using Badumna.Arbitration;

    /// <summary>
    /// Reply to a friend list request.
    /// </summary>
    public class FriendListReply : ArbitrationEvent
    {
        /// <summary>
        /// Initialises a new instance of the FriendListReply class.
        /// </summary>
        /// <param name="friendNames">A list of the names of any friends.</param>
        public FriendListReply(Collection<string> friendNames)
        {
            this.FriendNames = friendNames;
        }

        /// <summary>
        /// Initialises a new instance of the FriendListReply class.
        /// </summary>
        /// <param name="reader">The binary reader holding a serialized FriendListRequest.</param>
        public FriendListReply(BinaryReader reader)
        {
            Collection<string> friendNames = new Collection<string>();
            int friendCount = reader.ReadInt32();
            while (friendCount > 0)
            {
                friendNames.Add(reader.ReadString());
                friendCount--;
            }

            this.FriendNames = friendNames;
        }

        /// <summary>
        /// Gets the list of friend names.
        /// </summary>
        public Collection<string> FriendNames
        {
            get;
            private set;
        }

        /// <summary>
        /// Writes the FriendListReply using a binary writer.
        /// </summary>
        /// <remarks>Used by ArbitrationEventSet to serialize the event to a byte array.</remarks>
        /// <param name="writer">The binary writer to use.</param>
        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(this.FriendNames.Count);
            foreach (string name in this.FriendNames)
            {
                writer.Write(name);
            }
        }
    }
}