﻿//-----------------------------------------------------------------------
// <copyright file="FriendListRequest.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace DemoArbitrationEvents
{
    using System.IO;
    using Badumna.Arbitration;

    /// <summary>
    /// Request for a friend list.
    /// </summary>
    public class FriendListRequest : ArbitrationEvent
    {
        /// <summary>
        /// Initialises a new instance of the FriendListRequest class.
        /// </summary>
        /// <param name="userName">The user name of the player whose friends list is requested.</param>
        public FriendListRequest(string userName)
        {
            this.UserName = userName;
        }

        /// <summary>
        /// Initialises a new instance of the FriendListRequest class.
        /// </summary>
        /// <param name="reader">The binary reader holding a serialized FriendListRequest.</param>
        public FriendListRequest(BinaryReader reader)
        {
            this.UserName = reader.ReadString();
        }

        /// <summary>
        /// Gets the ID of the player making request.
        /// </summary>
        public string UserName
        {
            get;
            private set;
        }

        /// <summary>
        /// Writes the FriendListRequest using a binary writer.
        /// </summary>
        /// <remarks>Used by ArbitrationEventSet to serialize the event to a byte array.</remarks>
        /// <param name="writer">The binary writer to use.</param>
        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(this.UserName);
        }
    }
}
