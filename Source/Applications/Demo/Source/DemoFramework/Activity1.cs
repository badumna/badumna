﻿//-----------------------------------------------------------------------
// <copyright file="Activity1.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using Android.App;
using Android.OS;
using DemoFramework;
using Microsoft.Xna.Framework;
using Badumna.Security;

namespace DemoGame.Android
{
    /// <summary>
    /// Toplevel activity type for an Android application.
    /// </summary>
    [Activity(Label = "DemoGame.Android", MainLauncher = true, Icon = "@drawable/icon")]
    public class Activity1 : AndroidGameActivity
    {
        private DemoFramework.DemoGame game;

        private static string PrivateKeyXml;

        /// <summary>
        /// Called by the OS when the application is made active,
        /// either for the first time or after it has been paused.
        /// </summary>
        /// <remarks>
        /// Ideally we would only need to initialize the game once and pause
        /// it as required, however MonoGame on android currently has
        /// difficulty redrawing a game after it leaves the foreground.
        /// </remarks>
        protected override void OnResume()
        {
            // The private key pair can take some time to generate on a mobile device,
            // so we'll use the same one for the duration of the application runtime.
            // This can be stored for use across sessions, but care should be taken
            // to ensure it cannot be accessed by any other users / applications.
            if (PrivateKeyXml == null)
            {
                PrivateKeyXml = UnverifiedIdentityProvider.GenerateKeyPair();
            }

            SetContentView(Resource.Layout.Main);
            Game.Activity = this;

            // prepare the demo builder boostrap object with required settings
            var builder = new DemoBuilder
            {
                DeadReckoningEnabled = true,
                MultipleScenesEnabled = true,
                ProximityChatEnabled = true,
                PrivateChatEnabled = true,
                DistributedEntitiesEnabled = true
            };
            ////builder.SecureIdentitiesEnabled = true;
            ////builder.ArbitrationServerEnabled = true;

            this.game = builder.Build(PrivateKeyXml);
            SetContentView(this.game.Window);
            base.OnResume();

            this.game.Content.RootDirectory = "Content";

            // Start MonoGame
            this.game.Run();
        }

        /// <summary>
        /// Called by the OS when the application is no longer visible
        /// (or when the lockscreen activates)
        /// </summary>
        protected override void OnPause()
        {
            base.OnPause();

            // Trigger graceful shutdown of Badumna,
            // as it will be recreated in OnResume()
            this.game.Shutdown();
        }
    }
}

