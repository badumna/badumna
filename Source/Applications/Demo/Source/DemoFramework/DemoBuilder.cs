﻿// -----------------------------------------------------------------------
// <copyright file="DemoBuilder.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace DemoFramework
{
    using System;
    using System.Collections.Generic;
    using Badumna;
    using BadumnaDemo;
    using Microsoft.Xna.Framework;

    /// <summary>
    /// Configures and runs the demonstration.
    /// </summary>
    public class DemoBuilder
    {
        /// <summary>
        /// Gets or sets a value indicating whether dead reckoning should be enabled.
        /// </summary>
        public bool DeadReckoningEnabled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether multiple scenes should be enabled.
        /// </summary>
        public bool MultipleScenesEnabled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether proximity chat should be enabled.
        /// </summary>
        public bool ProximityChatEnabled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether private chat should be enabled.
        /// </summary>
        public bool PrivateChatEnabled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether verified identities should be enabled.
        /// </summary>
        public bool VerifiedIdentitiesEnabled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether an arbitration server should be used.
        /// </summary>
        /// <remarks>The demo arbitration server is used to retrieve a list of friends
        /// for private chat, and is only used if private chat is enabled.</remarks>
        public bool ArbitrationServerEnabled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether distributed entities should be enabled.
        /// </summary>
        public bool DistributedEntitiesEnabled { get; set; }

        /// <summary>
        /// Build the demonstration according to configuration settings already set.
        /// </summary>
        /// <param name="keyPairXml">Optional pre-generated key pair for encrypting communications with the Badumna network.</param>
        /// <returns>A new demo game.</returns>
        public DemoGame Build(string keyPairXml = null)
        {
            // Configure Badumna network.

            // Use LAN mode for testing
            var options = new Options();
            options.Connectivity.ConfigureForLan();

            // To use on the Internet, comment out the ConfigureForLan() line above and
            // add a known seed peer.  e.g.:
            // options.Connectivity.SeedPeers.Add("seedpeer.example.com:21251");

            // Set the application name
            options.Connectivity.ApplicationName = "api-example";

            // Set the arbitration server options, if required.
            if (this.ArbitrationServerEnabled)
            {
                options.Arbitration.Servers.Add(new ArbitrationServerDetails("friendserver"));
            }

            // Create demo modules.
            var networkModule = new NetworkModule(options);
            var identityModule = this.VerifiedIdentitiesEnabled ?
                (IIdentityModule)new VerifiedIdentityModule(networkModule, "localhost", 21248, keyPairXml) :
                    (IIdentityModule)new UnverifiedIdentityModule(networkModule, keyPairXml);
            var replicationModule = this.DeadReckoningEnabled ?
                new ReplicationModule(networkModule, n => new DeadReckonedOriginalAvatar(n), () => new DeadReckonedReplicaAvatar()) :
                new ReplicationModule(networkModule);

            SceneModule sceneModule = null;
            if (this.MultipleScenesEnabled)
            {
                var scenes = new List<SceneModule.Scene>()
                {
                    new SceneModule.Scene("Scene 2", 200, 300, 100, 100)
                };
                sceneModule = new SceneModule(replicationModule, ReplicationModule.DefaultSceneName, scenes);
            }

            ProximityChatModule proximityChatModule = null;
            if (this.ProximityChatEnabled)
            {
                proximityChatModule = new ProximityChatModule(replicationModule);
            }

            PrivateChatModule privateChatModule = null;
            if (this.PrivateChatEnabled)
            {
                var friendListProvider = this.ArbitrationServerEnabled ?
                    (IFriendListProvider)new OnlineFriendListProvider() :
                    new FriendListProvider();
                privateChatModule = new PrivateChatModule(networkModule, identityModule, friendListProvider);
            }

            if (this.DistributedEntitiesEnabled)
            {
                // The distributed entity module is kept from being garbage collected by its subscription
                // to events on the network module.
                new DistributedEntityModule(networkModule);
            }

            // Create presentation models.
            var presentations = new List<ITickablePresentation>();
            var log = new LogPresentation();
            var statusAggregator = new AggregateStatusPresentation();

            VerifiedIdentityPresentation verifiedIdentityPresentation = null;
            UnverifiedIdentityPresentation unverifiedIdentityPresentation = null;
            if (this.VerifiedIdentitiesEnabled)
            {
                var verifiedIdentityModule = identityModule as VerifiedIdentityModule;
                verifiedIdentityPresentation = new VerifiedIdentityPresentation(verifiedIdentityModule);
                presentations.Add(verifiedIdentityPresentation);
                log.Subscribe(verifiedIdentityPresentation);
                statusAggregator.AddReporter(verifiedIdentityPresentation);
            }
            else
            {
                var unverifiedIdentityModule = identityModule as UnverifiedIdentityModule;
                unverifiedIdentityPresentation = new UnverifiedIdentityPresentation(unverifiedIdentityModule, networkModule);
                statusAggregator.AddReporter(unverifiedIdentityPresentation);
            }

            var networkPresentation = new NetworkPresentation(networkModule);
            statusAggregator.AddReporter(networkPresentation);

            var gamePresentation = new GamePresentation(networkModule);
            presentations.Add(gamePresentation);

            var replicationPresentation = new ReplicationPresentation(replicationModule, networkModule);
            presentations.Add(replicationPresentation);

            // Configure multiple scenes demo.
            ScenePresentation scenePresentation = null;
            if (this.MultipleScenesEnabled)
            {
                scenePresentation = new ScenePresentation(sceneModule, networkModule);
                presentations.Add(scenePresentation);
            }

            // Configure proximity chat.
            ProximityChatPresentation proximityChatPresentation = null;
            if (this.ProximityChatEnabled)
            {
                proximityChatPresentation = new ProximityChatPresentation(proximityChatModule);
                log.Subscribe(proximityChatPresentation);
            }

            PrivateChatPresencePresentation privateChatPresencePresentation = null;
            PrivateChatMessagePresentation privateChatMessagePresentation = null;
            if (this.PrivateChatEnabled)
            {
                privateChatPresencePresentation = new PrivateChatPresencePresentation(privateChatModule);
                privateChatMessagePresentation = new PrivateChatMessagePresentation(privateChatModule);
                statusAggregator.AddReporter(privateChatPresencePresentation);
                log.Subscribe(privateChatMessagePresentation);
            }

            // Create views.
            var views = new List<IView>();

            var logView = new LogView(log, 0);
            views.Add(logView);

            var statusView = new StatusView(statusAggregator, 0, new Vector2(20, 100));
            views.Add(statusView);

            ControlView characterSelectionControlView;
            if (this.VerifiedIdentitiesEnabled)
            {
                var loginControlView = new ControlView(() => verifiedIdentityPresentation.AuthenticationEnabled, 0);
                loginControlView.AddCommandSet(new ControlView.CommandList(verifiedIdentityPresentation.AuthenticationCommands, 1));
                views.Add(loginControlView);

                characterSelectionControlView = new ControlView(() => verifiedIdentityPresentation.CharacterSelectionEnabled, 0);
                characterSelectionControlView.AddCommandSet(new ControlView.CommandList(verifiedIdentityPresentation.CharacterSelectionCommands, 2));
            }
            else
            {
                characterSelectionControlView = new ControlView(() => unverifiedIdentityPresentation.CharacterSelectionEnabled, 0);
                characterSelectionControlView.AddCommandSet(new ControlView.CommandList(unverifiedIdentityPresentation.Commands, 1));
            }

            views.Add(characterSelectionControlView);

            var gameControlView = new ControlView(() => networkModule.IsLoggedIn, 0);
            gameControlView.AddCommandSet(gamePresentation);
            views.Add(gameControlView);

            var replicationView = new ReplicationView(replicationPresentation, 0.5f);
            gameControlView.AddCommandSet(replicationPresentation);
            views.Add(replicationView);

            if (this.MultipleScenesEnabled)
            {
                var sceneView = new SceneView(scenePresentation, 1);
                views.Add(sceneView);
            }

            if (this.ProximityChatEnabled)
            {
                gameControlView.AddCommandSet(proximityChatPresentation);
            }

            if (this.PrivateChatEnabled)
            {
                gameControlView.AddCommandSet(new ControlView.CommandList(privateChatPresencePresentation.Commands, 2));
                gameControlView.AddCommandSet(new ControlView.CommandList(privateChatMessagePresentation.Commands, 1));
            }

            var game = new DemoGame(presentations, views);

            // Subscribe login module to shutdown warnings so it can gracefully shutdown Badumna.
            game.ShuttingDown += networkModule.Shutdown;

            return game;
        }
    }
}
