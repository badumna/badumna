﻿//-----------------------------------------------------------------------
// <copyright file="InputManager.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace DemoFramework
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Input;

#if ANDROID
    using Microsoft.Xna.Framework.Input.Touch;
#endif

    /// <summary>
    /// Responsible for testing for input.
    /// </summary>
    public class InputManager
    {
        /// <summary>
        /// The current mouse state.
        /// </summary>
        private Vector2 location;

#if !ANDROID
        /// <summary>
        /// State of the mouse in the last update
        /// </summary>
        private MouseState lastMouseState;
#endif

        /// <summary>
        /// A value indicating whether a new touch or mouse-button press was detected last update.
        /// </summary>
        private bool newTouch = false;

        /// <summary>
        /// A value indicating whether there is a touch or mouse-button-press in progress.
        /// </summary>
        private bool touch = false;

        /// <summary>
        /// Test if there is a touch or mouse-button press in progress.
        /// </summary>
        /// <param name="x">When this method returns, contains the x-coordinate of the touch or mouse location,
        /// if a touch or mouse-button press is in progress.</param>
        /// <param name="y">When this method returns, contains the y-coordinate of the touch or mouse location,
        /// if a touch or mouse-button press is in progress.</param>
        /// <returns>A value indicating whether there is a touch or mouse-button press in progress.</returns>
        public bool IsTouched(out int x, out int y)
        {
            x = (int)this.location.X;
            y = (int)this.location.Y;
            return this.touch;
        }

        /// <summary>
        /// Test whether a new touch or mouse-button press was detected last update.
        /// </summary>
        /// <param name="x">When this method returns, contains the x-coordinate of the touch or mouse location,
        /// if a new touch or mouse-button press was detected last update.</param>
        /// <param name="y">When this method returns, contains the y-coordinate of the touch or mouse location,
        /// if a new touch or mouse-button press was detected last update.</param>
        /// <returns>A value indicating whether a new touch or mouse-button press was detected last update.</returns>
        public bool IsNewlyTouched(out int x, out int y)
        {
            x = (int)this.location.X;
            y = (int)this.location.Y;
            return this.newTouch;
        }

        /// <summary>
        /// Update the status of the input, testing for new or continuing touches or mouse-button presses.
        /// </summary>
        public void Update()
        {
#if ANDROID
            var touches = TouchPanel.GetState();
            bool alreadyTouched = this.touch;
            this.touch = false;
            this.newTouch = false;

            foreach (var t in touches)
            {
                if (t.State == TouchLocationState.Pressed || t.State == TouchLocationState.Moved)
                {
                    this.newTouch = !alreadyTouched;
                    this.touch = true;
                    this.location = t.Position;
                    break;
                }
            }
#else
            MouseState newMouseState = Mouse.GetState();
            this.newTouch = (newMouseState.LeftButton == ButtonState.Pressed) &&
                (this.lastMouseState.LeftButton == ButtonState.Released);
            this.touch = newMouseState.LeftButton == ButtonState.Pressed;

            if (this.touch || this.newTouch)
            {
                this.location = new Vector2(newMouseState.X, newMouseState.Y);
            }

            this.lastMouseState = newMouseState;
#endif
        }
    }
}
