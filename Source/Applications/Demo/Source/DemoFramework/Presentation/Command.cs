﻿//-----------------------------------------------------------------------
// <copyright file="Command.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace DemoFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Encapsulates an invokable action and a function determining whether the action is currently enabled.
    /// </summary>
    public class Command
    {
        /// <summary>
        /// An `isEnabled` function to use for always-enabled buttons
        /// </summary>
        private static Func<bool> always = () => true;

        /// <summary>
        /// A name to display for the command.
        /// </summary>
        private readonly string name;

        /// <summary>
        /// An action.
        /// </summary>
        private readonly Action action;
        
        /// <summary>
        /// A value indicating whether the action can be invoked.
        /// </summary>
        private readonly Func<bool> isEnabled;

        /// <summary>
        /// Initialises a new instance of the Command class.
        /// </summary>
        /// <param name="name">A name to display for the command.</param>
        /// <param name="action">An action.</param>
        /// <param name="isEnabled">A function to determine whether the action is enabled.</param>
        public Command(string name, Action action, Func<bool> isEnabled)
        {
            this.name = name;
            this.action = action;
            this.isEnabled = isEnabled;
        }

        /// <summary>
        /// Gets an 'isEnabled' function to use for always-enabled buttons
        /// </summary>
        public static Func<bool> Always
        {
            get { return Command.always; }
        }

        /// <summary>
        /// Gets a name to display for the command.
        /// </summary>
        public string Name
        {
            get { return this.name; }
        }

        /// <summary>
        /// Gets a value indicating whether the action is enabled.
        /// </summary>
        public bool IsEnabled
        {
            get { return this.isEnabled.Invoke(); }
        }

        /// <summary>
        /// Invoke that action, if enabled, otherwise do nothing.
        /// </summary>
        public void Invoke()
        {
            if (this.isEnabled.Invoke())
            {
                this.action.Invoke();
            }
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return string.Format("<#Command: {0}>", this.Name);
        }
    }
}
