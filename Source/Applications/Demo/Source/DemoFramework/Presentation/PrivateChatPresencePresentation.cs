﻿// -----------------------------------------------------------------------
// <copyright file="PrivateChatPresencePresentation.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace DemoFramework
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Badumna.Chat;
    using BadumnaDemo;

    /// <summary>
    /// Wraps the private chat module, exposing presence status commands available in-game.
    /// </summary>
    public class PrivateChatPresencePresentation : ICommandPresentation, IStatusPresentation
    {
        /// <summary>
        /// The private chat module.
        /// </summary>
        private readonly PrivateChatModule module;

        /// <summary>
        /// Commands for controlling presence status.
        /// </summary>
        private ObservableCollection<Command> commands = new ObservableCollection<Command>();

        /// <summary>
        /// Initialises a new instance of the PrivateChatPresencePresentation class.
        /// </summary>
        /// <param name="module">The private chat module.</param>
        public PrivateChatPresencePresentation(PrivateChatModule module)
        {
            this.module = module;

            this.commands.Add(new Command(
                    "Chat",
                    () => module.SetPresence(ChatStatus.Chat),
                    () => module.IsEnabled));

            this.commands.Add(new Command(
                    "Busy",
                    () => module.SetPresence(ChatStatus.DoNotDisturb),
                    () => module.IsEnabled));
        }

        /// <inheritdoc />
        public ObservableCollection<Command> Commands
        {
            get { return this.commands; }
        }

        /// <inheritdoc />
        public IEnumerable<string> Status
        {
            get
            {
                foreach (Friend friend in this.module.Friends)
                {
                    yield return friend.Name + ": " + friend.Status.ToString();
                }
            }
        }
    }
}
