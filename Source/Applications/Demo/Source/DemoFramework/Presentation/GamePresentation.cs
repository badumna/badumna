﻿//-----------------------------------------------------------------------
// <copyright file="GamePresentation.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace DemoFramework
{
    using System.Collections.ObjectModel;
    using BadumnaDemo;

    /// <summary>
    /// Triggers updates of the Badumna game world (via the LoginModule's NetworkFacade)
    /// </summary>
    internal class GamePresentation : ITickablePresentation, ICommandPresentation
    {
        /// <summary>
        /// The network module to update.
        /// </summary>
        private readonly NetworkModule networkModule;

        /// <summary>
        /// The set of game commands.
        /// </summary>
        private ObservableCollection<Command> commands = new ObservableCollection<Command>();
        
        /// <summary>
        /// Initialises a new instance of the <see cref="GamePresentation"/> class.
        /// </summary>
        /// <param name="networkModule">The network module.</param>
        public GamePresentation(NetworkModule networkModule)
        {
            this.networkModule = networkModule;

            this.commands.Add(new Command(
                "Exit",
                this.networkModule.Shutdown,
                Command.Always));
        }

        /// <inheritdoc/>
        public ObservableCollection<Command> Commands
        {
            get { return this.commands; }
        }

        /// <summary>
        /// Call each tick to update the network.
        /// </summary>
        /// <param name="milliseconds">Milliseconds elapsed since last update.</param>
        public void Update(float milliseconds)
        {
            this.networkModule.Update(milliseconds);
        }
    }
}
