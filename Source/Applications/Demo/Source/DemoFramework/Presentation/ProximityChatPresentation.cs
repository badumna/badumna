﻿// -----------------------------------------------------------------------
// <copyright file="ProximityChatPresentation.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace DemoFramework
{
    using System.Collections.ObjectModel;
    using BadumnaDemo;

    /// <summary>
    /// Presentation for incoming / outgoing proximity chat messages.
    /// </summary>
    public class ProximityChatPresentation : ICommandPresentation, ILogWriter
    {
        /// <summary>
        /// A command for sending a chat message.
        /// </summary>
        private ObservableCollection<Command> commands = new ObservableCollection<Command>();

        /// <summary>
        /// Initialises a new instance of the ProximityChatPresentation class.
        /// </summary>
        /// <param name="proximityChatModule">The proximity chat module.</param>
        public ProximityChatPresentation(ProximityChatModule proximityChatModule)
        {
            this.commands.Add(new Command(
                "Proximity Chat",
                () => proximityChatModule.SendMessage("Hello!"),
                () => proximityChatModule.IsEnabled));

            proximityChatModule.ProximtyChatEventOccurred += this.OnMessageReceived;
        }

        /// <summary>
        /// Event raised when a message has been received.
        /// </summary>
        public event LogMessageHandler MessageReceived;

        /// <inheritdoc />
        ObservableCollection<Command> ICommandPresentation.Commands
        {
            get { return this.commands; }
        }

        /// <summary>
        /// Handler for proximity chat messages.
        /// </summary>
        /// <param name="message">The proximity chat message received.</param>
        private void OnMessageReceived(string message)
        {
            LogMessageHandler handler = this.MessageReceived;
            if (handler != null)
            {
                handler.Invoke(message);
            }
        }
    }
}
