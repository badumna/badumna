﻿// -----------------------------------------------------------------------
// <copyright file="PrivateChatMessagePresentation.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace DemoFramework
{
    using System;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using BadumnaDemo;

    /// <summary>
    /// Wraps the private chat module, exposing message sending commands available in-game.
    /// </summary>
    public class PrivateChatMessagePresentation : ICommandPresentation, ILogWriter
    {
        /// <summary>
        /// The private chat module.
        /// </summary>
        private readonly PrivateChatModule module;

        /// <summary>
        /// Commands for controlling presence status.
        /// </summary>
        private ObservableCollection<Command> commands = new ObservableCollection<Command>();

        /// <summary>
        /// Initialises a new instance of the PrivateChatMessagePresentation class.
        /// </summary>
        /// <param name="module">The private chat module.</param>
        public PrivateChatMessagePresentation(PrivateChatModule module)
        {
            this.module = module;
            module.Friends.CollectionChanged += this.OnFriendsChanged;
            module.PrivateChatEventOccurred += this.PrivateChatMessageReceived;
            this.UpdateCommands();
        }

        /// <inheritdoc />
        public event LogMessageHandler MessageReceived;

        /// <inheritdoc />
        public ObservableCollection<Command> Commands
        {
            get { return this.commands; }
        }

        /// <summary>
        /// Update the commands to include one for chatting to each friend.
        /// </summary>
        private void UpdateCommands()
        {
            this.commands.Clear();
            foreach (Friend friend in this.module.Friends)
            {
                // Don't close over the loop variable until C# 5.
                Friend friendForClosure = friend;

                this.commands.Add(new Command(
                    "Message " + friendForClosure.Name,
                    () => this.module.SendPrivateMessage(friendForClosure.Name, "Hi!"),
                    () => friendForClosure.Channel != null));
            }
        }

        /// <summary>
        /// Handles changes in the private chat module's friend list.
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="e">Event arguments.</param>
        private void OnFriendsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            this.UpdateCommands();
        }

        /// <summary>
        /// Handler for subscribing to module's private chat message received events.
        /// </summary>
        /// <param name="message">The private chat message received.</param>
        private void PrivateChatMessageReceived(string message)
        {
            LogMessageHandler handler = this.MessageReceived;
            if (handler != null)
            {
                handler.Invoke(message);
            }
        }
    }
}
