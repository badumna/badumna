﻿// -----------------------------------------------------------------------
// <copyright file="AggregateStatusPresentation.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace DemoFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Aggregates statuses of multiple status reports for presenting to a single view.
    /// </summary>
    public class AggregateStatusPresentation : IStatusPresentation
    {
        /// <summary>
        /// Status reporters registered with this aggregator.
        /// </summary>
        private List<IStatusPresentation> reporters = new List<IStatusPresentation>();

        /// <summary>
        /// Gets the aggregate status from all reporters.
        /// </summary>
        public IEnumerable<string> Status
        {
            get
            {
                foreach (var reporter in this.reporters)
                {
                    foreach (var line in reporter.Status)
                    {
                        yield return line;
                    }
                }
            }
        }

        /// <summary>
        /// Add a status reporter for aggregating.
        /// </summary>
        /// <param name="reporter">The status reporter to add.</param>
        public void AddReporter(IStatusPresentation reporter)
        {
            this.reporters.Add(reporter);
        }
    }
}
