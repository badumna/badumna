﻿// -----------------------------------------------------------------------
// <copyright file="ReplicationPresentation.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace DemoFramework
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using BadumnaDemo;

    /// <summary>
    /// Wraps the game world, exposing commands available in-game, and the game state for drawing.
    /// </summary>
    public class ReplicationPresentation : ICommandPresentation, ITickablePresentation
    {
        /// <summary>
        /// Limit on the number of log messages to keep.
        /// </summary>
        private const int LogSize = 5;

        /// <summary>
        /// The replication module.
        /// </summary>
        private readonly ReplicationModule replicationModule;

        /// <summary>
        /// The network module.
        /// </summary>
        private readonly NetworkModule networkModule;

        /// <summary>
        /// Commands for the world.
        /// </summary>
        private ObservableCollection<Command> commands = new ObservableCollection<Command>();
        
        /// <summary>
        /// Initialises a new instance of the ReplicationPresentation class.
        /// </summary>
        /// <param name="replicationModule">The replication module.</param>
        /// <param name="networkModule">The network module.</param>
        public ReplicationPresentation(ReplicationModule replicationModule, NetworkModule networkModule)
        {
            this.replicationModule = replicationModule;
            this.networkModule = networkModule;

            this.commands.Add(new Command(
                "Colour",
                () => this.replicationModule.OriginalAvatar.Colour = Colour.RandomColour(),
                () => this.replicationModule.OriginalAvatar != null));
        }

        /// <summary>
        /// Gets a value indicating whether this presentation is visible.
        /// </summary>
        public bool IsVisible
        {
            get { return this.networkModule.IsLoggedIn; }
        }

        /// <summary>
        /// Gets all the command available.
        /// </summary>
        public ObservableCollection<Command> Commands
        {
            get { return this.commands; }
        }

        /// <summary>
        /// Gets all avatars currently known about in the world.
        /// </summary>
        public IEnumerable<IAvatar> Avatars
        {
            get
            {
                if (this.replicationModule.OriginalAvatar != null)
                {
                    yield return this.replicationModule.OriginalAvatar;
                }

                foreach (Avatar avatar in this.replicationModule.ReplicaAvatars)
                {
                    yield return avatar;
                }
            }
        }

        /// <summary>
        /// Tell the original avatar to move to a given location.
        /// </summary>
        /// <param name="x">The x-coordinate of the location to move to.</param>
        /// <param name="y">The y-coordinate of the location to move to.</param>
        public void Move(float x, float y)
        {
            if (this.replicationModule.OriginalAvatar != null)
            {
                this.replicationModule.OriginalAvatar.SetTarget(x, y);
            }
        }

        /// <summary>
        /// Call each tick to update the world.
        /// </summary>
        /// <param name="milliseconds">Milliseconds elapsed since last update.</param>
        public void Update(float milliseconds)
        {
            this.replicationModule.Update(milliseconds);
        }
    }
}
