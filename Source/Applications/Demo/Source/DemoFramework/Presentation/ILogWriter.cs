﻿// -----------------------------------------------------------------------
// <copyright file="ILogWriter.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace DemoFramework
{
    /// <summary>
    /// Delegate for handling a message.
    /// </summary>
    /// <param name="message">The message.</param>
    public delegate void LogMessageHandler(string message);

    /// <summary>
    /// Interface for presentations that raise messages for logging.
    /// </summary>
    public interface ILogWriter
    {
        /// <summary>
        /// Event raised when a message requires logging.
        /// </summary>
        event LogMessageHandler MessageReceived;
    }
}
