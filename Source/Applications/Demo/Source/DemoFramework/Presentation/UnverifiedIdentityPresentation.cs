﻿// -----------------------------------------------------------------------
// <copyright file="UnverifiedIdentityPresentation.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace DemoFramework
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using BadumnaDemo;

    /// <summary>
    /// Exposes commands for selecting an identity.
    /// </summary>
    public class UnverifiedIdentityPresentation : ICommandPresentation, IStatusPresentation
    {
        /// <summary>
        /// The identity module.
        /// </summary>
        private UnverifiedIdentityModule identityModule;

        /// <summary>
        /// Commands for identity selection.
        /// </summary>
        private ObservableCollection<Command> commands = new ObservableCollection<Command>();

        /// <summary>
        /// Initialises a new instance of the UnverifiedIdentityPresentation class.
        /// </summary>
        /// <param name="identityModule">The identity module.</param>
        /// <param name="networkModule">The network module.</param>
        public UnverifiedIdentityPresentation(UnverifiedIdentityModule identityModule, NetworkModule networkModule)
        {
            this.identityModule = identityModule;

            this.commands.Add(new Command(
                "alice",
                () => this.identityModule.SelectIdentity("alice"),
                Command.Always));

            this.commands.Add(new Command(
                "bob",
                () => this.identityModule.SelectIdentity("bob"),
                Command.Always));
        }

        /// <summary>
        /// Gets a value indicating whether character selection is enabled.
        /// </summary>
        public bool CharacterSelectionEnabled
        {
            get { return !this.identityModule.HasIdentity; }
        }

        /// <summary>
        /// Gets all the command available.
        /// </summary>
        public ObservableCollection<Command> Commands
        {
            get { return this.commands; }
        }

        /// <summary>
        /// Gets the status of the world.
        /// </summary>
        public IEnumerable<string> Status
        {
            get { yield return "Identity: " + this.identityModule.CharacterName; }
        }
    }
}
