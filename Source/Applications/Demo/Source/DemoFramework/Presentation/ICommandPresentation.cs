﻿// -----------------------------------------------------------------------
// <copyright file="ICommandPresentation.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace DemoFramework
{
    using System.Collections.ObjectModel;

    /// <summary>
    /// Interface for presentations that contain sets of commands.
    /// </summary>
    public interface ICommandPresentation
    {
        /// <summary>
        /// Gets an observable collection of commands held in the set.
        /// </summary>
        ObservableCollection<Command> Commands { get; }
    }
}
