﻿// -----------------------------------------------------------------------
// <copyright file="NetworkPresentation.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace DemoFramework
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using BadumnaDemo;

    /// <summary>
    /// Provides a login command as well as displaying the current login status.
    /// </summary>
    public class NetworkPresentation : IStatusPresentation
    {
        /// <summary>
        /// The network module.
        /// </summary>
        private NetworkModule networkModule;

        /// <summary>
        /// Initialises a new instance of the NetworkPresentation class.
        /// </summary>
        /// <param name="networkModule">The network module.</param>
        public NetworkPresentation(NetworkModule networkModule)
        {
            this.networkModule = networkModule;
        }

        /// <summary>
        /// Gets the status of the world.
        /// </summary>
        public IEnumerable<string> Status
        {
            get { yield return "Network " + this.networkModule.StatusDescription; }
        }
    }
}
