﻿// -----------------------------------------------------------------------
// <copyright file="ITickablePresentation.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace DemoFramework
{
    using System.Collections.ObjectModel;

    /// <summary>
    /// Interface for presentations that needs to be updated each tick.
    /// </summary>
    public interface ITickablePresentation
    {
        /// <summary>
        /// Update a presentation.
        /// </summary>
        /// <param name="milliseconds">Milliseconds elapsed since last update.</param>
        void Update(float milliseconds);
    }
}
