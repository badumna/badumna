﻿// -----------------------------------------------------------------------
// <copyright file="ScenePresentation.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace DemoFramework
{
    using System.Collections.Generic;
    using BadumnaDemo;

    /// <summary>
    /// Responsible for updating the scene module.
    /// </summary>
    public class ScenePresentation : ITickablePresentation
    {
        /// <summary>
        /// The scene module.
        /// </summary>
        private readonly SceneModule sceneModule;

        /// <summary>
        /// The network module.
        /// </summary>
        private readonly NetworkModule networkModule;

        /// <summary>
        /// Initialises a new instance of the ScenePresentation class.
        /// </summary>
        /// <param name="sceneModule">The scene module.</param>
        /// <param name="networkModule">The network module.</param>
        public ScenePresentation(SceneModule sceneModule, NetworkModule networkModule)
        {
            this.sceneModule = sceneModule;
            this.networkModule = networkModule;
        }

        /// <summary>
        /// Gets a value indicating whether this presentation is visible.
        /// </summary>
        public bool IsVisible
        {
            get { return this.networkModule.IsLoggedIn; }
        }

        /// <summary>
        /// Gets the scenes.
        /// </summary>
        public IEnumerable<SceneModule.Scene> Scenes
        {
            get
            {
                return this.sceneModule.Scenes;
            }
        }

        /// <inheritdoc />
        public void Update(float milliseconds)
        {
            this.sceneModule.Update(milliseconds);
        }
    }
}
