﻿// -----------------------------------------------------------------------
// <copyright file="LogPresentation.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace DemoFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Receives log events from multiple senders and stores the most recent messages.
    /// </summary>
    public class LogPresentation
    {
        /// <summary>
        /// The number of messages to store in the log.
        /// </summary>
        private const int LogSize = 5;

        /// <summary>
        /// A log to store the most recent messages received.
        /// </summary>
        private Queue<string> log = new Queue<string>(LogSize + 1);

        /// <summary>
        /// Gets the most recent messages received.
        /// </summary>
        public IEnumerable<string> Messages
        {
            get
            {
                return this.log.Reverse();
            }
        }

        /// <summary>
        /// Subscribe to message events from an event logger.
        /// </summary>
        /// <param name="eventLogger">The event logger whose message events are to be subscribed to.</param>
        public void Subscribe(ILogWriter eventLogger)
        {
            eventLogger.MessageReceived += this.OnMessageReceived;
        }

        /// <summary>
        /// Remove all existing log messages.
        /// </summary>
        public void Clear()
        {
            this.log.Clear();
        }

        /// <summary>
        /// Message handler used when subscribing to message events.
        /// </summary>
        /// <param name="message">The message received.</param>
        private void OnMessageReceived(string message)
        {
            this.log.Enqueue(DateTime.Now.ToString("hh:mm:ss: ") + message);
            if (this.log.Count > LogSize)
            {
                this.log.Dequeue();
            }
        }
    }
}
