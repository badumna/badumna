﻿// -----------------------------------------------------------------------
// <copyright file="IStatusPresentation.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace DemoFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Status presentations have a status for a view to draw.
    /// </summary>
    public interface IStatusPresentation
    {
        /// <summary>
        /// Gets the current status.
        /// </summary>
        IEnumerable<string> Status { get; }
    }
}
