﻿// -----------------------------------------------------------------------
// <copyright file="VerifiedIdentityPresentation.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace DemoFramework
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.Linq;
    using Badumna.Security;
    using BadumnaDemo;

    /// <summary>
    /// Exposes commands for selecting an identity.
    /// </summary>
    public class VerifiedIdentityPresentation : IStatusPresentation, ITickablePresentation, ILogWriter
    {
        /// <summary>
        /// The identity module.
        /// </summary>
        private readonly VerifiedIdentityModule identityModule;

        /// <summary>
        /// The set of commands for authenticating the user with Dei.
        /// </summary>
        private ObservableCollection<Command> authenticationCommands;

        /// <summary>
        /// The set of commands for selecting and manipulating
        /// the user's list of characters.
        /// </summary>
        private ObservableCollection<Command> characterSelectionCommands;

        /// <summary>
        /// Initialises a new instance of the VerifiedIdentityPresentation class.
        /// </summary>
        /// <param name="identityModule">The identity module.</param>
        public VerifiedIdentityPresentation(VerifiedIdentityModule identityModule)
        {
            this.identityModule = identityModule;

            this.identityModule.MessageHandler += this.OnAuthenticationMessage;

            this.authenticationCommands = new ObservableCollection<Command>();
            this.authenticationCommands.Add(new Command(
                "alice@example.com",
                () => this.identityModule.Authenticate("alice@example.com", "alice_password"),
                () => !this.identityModule.Busy));

            this.authenticationCommands.Add(new Command(
                "bob@example.com",
                () => this.identityModule.Authenticate("bob@example.com", "bob_password"),
                () => !this.identityModule.Busy));

            this.characterSelectionCommands = new ObservableCollection<Command>();
            this.identityModule.Characters.CollectionChanged += (sender, args) => this.UpdateCharactermanagementCommands();
        }

        /// <inheritdoc />
        public event LogMessageHandler MessageReceived;

        /// <summary>
        /// Gets a value indicating whether authentication is enabled.
        /// </summary>
        public bool AuthenticationEnabled
        {
            get { return this.identityModule.CanAuthenticate; }
        }

        /// <summary>
        /// Gets a value indicating whether character selection is enabled.
        /// </summary>
        public bool CharacterSelectionEnabled
        {
            get { return this.identityModule.CanSelectCharacter; }
        }

        /// <summary>
        /// Gets the set of commands for authenticating the user with Dei.
        /// </summary>
        public ObservableCollection<Command> AuthenticationCommands
        {
            get { return this.authenticationCommands; }
        }

        /// <summary>
        /// Gets the set of commands for selecting and manipulating
        /// the user's list of characters.
        /// </summary>
        public ObservableCollection<Command> CharacterSelectionCommands
        {
            get { return this.characterSelectionCommands; }
        }

        /// <summary>
        /// Gets the status of the world.
        /// </summary>
        public IEnumerable<string> Status
        {
            get { yield return this.identityModule.Status; }
        }

        /// <summary>
        /// Gets the character name base. This base is used to create numbered character names.
        /// </summary>
        private string CharacterNameBase
        {
            get
            {
                string username = this.identityModule.Username;
                Debug.Assert(username != null, "no authenticated user!");
                return username.Split('@')[0];
            }
        }

        /// <inheritdoc />
        public void Update(float milliseconds)
        {
            this.identityModule.Update();
        }

        /// <summary>
        /// Updates the set of character management commands
        /// according to the current list of characters for the user.
        /// </summary>
        /// <remarks>
        /// The character management commands include a single "add character" button,
        /// plus a "select character" and "delete character" button for each existing character.
        /// </remarks>
        private void UpdateCharactermanagementCommands()
        {
            this.characterSelectionCommands.Clear();

            var characters = this.identityModule.Characters;

            Func<bool> canPerformAction = () => !this.identityModule.Busy && this.identityModule.CanSelectCharacter;

            Action<Character> addCharacterCommands = delegate(Character character)
            {
                this.characterSelectionCommands.Add(new Command(
                    character.Name,
                    () => this.identityModule.SelectIdentity(character),
                    canPerformAction));

                this.characterSelectionCommands.Add(new Command(
                    "-",
                    () => this.identityModule.DeleteCharacter(character),
                    canPerformAction));
            };

            foreach (var character in characters.OrderBy(character => character.Name.Length).ThenBy(character => character.Name))
            {
                addCharacterCommands(character);
            }

            this.characterSelectionCommands.Add(new Command(
                "+ new",
                () => this.identityModule.AddCharacter(this.NewCharacterName()),
                canPerformAction));

            this.characterSelectionCommands.Add(new Command(
                    "Back",
                    this.identityModule.Clear,
                    () => !this.identityModule.Busy));
        }

        /// <summary>
        /// Generates a new character name that isn't yet present in the user's list of characters.
        /// </summary>
        /// <returns>The newly generated character name.</returns>
        private string NewCharacterName()
        {
            List<string> existingNames = this.identityModule.Characters.Select(c => c.Name).ToList();
            foreach (string newName in this.GenerateCharacterNames())
            {
                if (!existingNames.Contains(newName))
                {
                    return newName;
                }
            }

            return null;
        }

        /// <summary>
        /// Generates an very long sequence of character names based on this.CharacterNameBase.
        /// AddCharacter uses this method to get the first name that doesn't already exist as a character.
        /// </summary>
        /// <returns>A collection of generated character names.</returns>
        private IEnumerable<string> GenerateCharacterNames()
        {
            string name = this.CharacterNameBase;
            yield return name;
            for (int i = 1; /* loop forever */; i += 1)
            {
                yield return string.Format("{0}-{1}", name, i);
            }
        }

        /// <summary>
        /// Handle authentication messages and pass them on to interested parties.
        /// </summary>
        /// <param name="message">A message regarding authentication progress.</param>
        private void OnAuthenticationMessage(string message)
        {
            var logHandler = this.MessageReceived;
            if (logHandler != null)
            {
                logHandler.Invoke(message);
            }
        }
    }
}
