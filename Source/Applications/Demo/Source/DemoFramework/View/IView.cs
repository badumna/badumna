﻿// -----------------------------------------------------------------------
// <copyright file="IView.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace DemoFramework
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;

    /// <summary>
    /// Views are responsible for drawing content on screen.
    /// </summary>
    public interface IView
    {
        /// <summary>
        /// Gets a value indicating whether the view is currently visible.
        /// </summary>
        bool IsVisible { get; }

        /// <summary>
        /// Gets the depth at which to draw this view.
        /// </summary>
        /// <remarks>Draw depth also determined input handling priority.</remarks>
        float Depth { get; }

        /// <summary>
        /// Load content required by this view.
        /// </summary>
        /// <param name="content">The content module for loading content.</param>
        void LoadContent(ContentManager content);

        /// <summary>
        /// Display the view on screen.
        /// </summary>
        /// <param name="spriteBatch">Sprite batch for drawing.</param>
        /// <param name="graphicsDevice">The graphics device.</param>
        void Draw(SpriteBatch spriteBatch, GraphicsDevice graphicsDevice);

        /// <summary>
        /// Handle input (mouse clicks or touches).
        /// </summary>
        /// <param name="input">Location of the click or touch.</param>
        /// <returns><c>true</c> if the input was handled by this view, otherwise <c>false</c>.</returns>
        bool HandleInput(Point input);
    }
}
