﻿//-----------------------------------------------------------------------
// <copyright file="Button.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace DemoFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;

    /// <summary>
    /// A clickable button with custom text and action.
    /// </summary>
    public class Button
    {
        /// <summary>
        /// The bounds of the button in pixels.
        /// </summary>
        private Rectangle bounds;

        /// <summary>
        /// The command to invoke when the button is clicked or touched.
        /// </summary>
        private Command command;

        /// <summary>
        /// Initialises a new instance of the Button class.
        /// </summary>
        /// <param name="bounds">The bounds of the button in pixels.</param>
        /// <param name="command">The command to invoke when the button is clicked or touched.</param>
        public Button(Rectangle bounds, Command command)
        {
            this.bounds = bounds;
            this.command = command;
        }

        /// <summary>
        /// Gets the bounds of the button.
        /// </summary>
        public Rectangle Bounds
        {
            get { return this.bounds; }
        }

        /// <summary>
        /// Gets the button's command.
        /// </summary>
        public Command Command
        {
            get { return this.command; }
        }

        /// <summary>
        /// Invoke the button's command.
        /// </summary>
        public void Invoke()
        {
            this.command.Invoke();
        }

        /// <summary>
        /// Handle a touch or click if it's within this button's bounds.
        /// </summary>
        /// <param name="location">The location of a touch or click.</param>
        /// <returns><c>true</c> if the input was handled, otherwise <c>false</c>.</returns>
        public bool HandleInput(Point location)
        {
            if (this.bounds.Contains(location))
            {
                this.Invoke();
                return true;
            }

            return false;
        }
    }
}
