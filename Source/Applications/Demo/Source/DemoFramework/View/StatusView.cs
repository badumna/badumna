﻿// -----------------------------------------------------------------------
// <copyright file="StatusView.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace DemoFramework
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;

    /// <summary>
    /// View for displaying status information.
    /// </summary>
    public class StatusView : IView
    {
        /// <summary>
        /// Distance to space the status lines at.
        /// </summary>
        private const float LineSpacing = 20f;

        /// <summary>
        ///  A status aggregator
        /// </summary>
        private readonly IStatusPresentation reporter;

        /// <summary>
        /// The depth to draw the view at.
        /// </summary>
        private readonly float depth;

        /// <summary>
        /// The location to begin drawing the status information.
        /// </summary>
        private readonly Vector2 origin;
        
        /// <summary>
        /// A font for drawing status text on screen.
        /// </summary>
        private SpriteFont font;

        /// <summary>
        /// Initialises a new instance of the StatusView class.
        /// </summary>
        /// <param name="reporter">The reporter whose status should be displayed.</param>
        /// <param name="depth">The depth to draw the view at.</param>
        /// <param name="origin">The location to begin drawing the status information.</param>
        public StatusView(IStatusPresentation reporter, float depth, Vector2 origin)
        {
            this.reporter = reporter;
            this.depth = depth;
            this.origin = origin;
        }

        /// <inheritdoc />
        /// <remarks>The status view is always visible.</remarks>
        public bool IsVisible
        {
            get { return true; }
        }

        /// <inheritdoc />
        public float Depth
        {
            get { return this.depth; }
        }

        /// <inheritdoc />
        public void LoadContent(ContentManager content)
        {
            this.font = content.Load<SpriteFont>("Font");
        }

        /// <inheritdoc />
        public void Draw(SpriteBatch spriteBatch, GraphicsDevice graphicsDevice)
        {
            Rectangle bounds = graphicsDevice.Viewport.Bounds;

            // anchor to the bottom of the viewport
            Vector2 location = new Vector2(this.origin.X, bounds.Bottom - (this.origin.Y + LineSpacing));
            foreach (var line in this.reporter.Status)
            {
                spriteBatch.DrawString(this.font, line, location, Color.White);
                location.Y -= LineSpacing;
            }
        }

        /// <inheritdoc />
        public bool HandleInput(Point input)
        {
            return false;
        }
    }
}
