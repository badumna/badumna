﻿// -----------------------------------------------------------------------
// <copyright file="SceneView.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace DemoFramework
{
    using BadumnaDemo;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;

    /// <summary>
    /// Displays multiple Badumna scene presentations on-screen.
    /// </summary>
    public class SceneView : IView
    {
        /// <summary>
        /// The scene presentation.
        /// </summary>
        private readonly ScenePresentation scenePresentation;

        /// <summary>
        /// The depth to draw this view at.
        /// </summary>
        private readonly float depth;

        /// <summary>
        /// A texture for drawing scenes.
        /// </summary>
        private Texture2D sceneTexture;

        /// <summary>
        /// Initialises a new instance of the SceneView class.
        /// </summary>
        /// <param name="scenePresentation">The scene presentation.</param>
        /// <param name="depth">The depth to draw this view at.</param>
        public SceneView(ScenePresentation scenePresentation, float depth)
        {
            this.scenePresentation = scenePresentation;
            this.depth = depth;
        }

        /// <inheritdoc />
        public bool IsVisible
        {
            get { return this.scenePresentation.IsVisible; }
        }

        /// <inheritdoc />
        public float Depth
        {
            get { return this.depth; }
        }

        /// <inheritdoc />
        public void LoadContent(Microsoft.Xna.Framework.Content.ContentManager content)
        {
            this.sceneTexture = content.Load<Texture2D>("Pixel");
        }

        /// <inheritdoc />
        public void Draw(SpriteBatch spriteBatch, GraphicsDevice graphicsDevice)
        {
            foreach (var scene in this.scenePresentation.Scenes)
            {
                this.DrawSceneBounds(spriteBatch, scene);
            }
        }

        /// <inheritdoc />
        public bool HandleInput(Point input)
        {
            return false;
        }

        /// <summary>
        /// Draws a scene on screen.
        /// </summary>
        /// <param name="spriteBatch">Sprite batch for drawing.</param>
        /// <param name="sceneBounds">Bounds of the scene.</param>
        private void DrawSceneBounds(SpriteBatch spriteBatch, SceneModule.Scene sceneBounds)
        {
            Rectangle bounds = new Rectangle(
                sceneBounds.OriginX,
                sceneBounds.OriginY,
                sceneBounds.Width,
                sceneBounds.Height);
            spriteBatch.Draw(this.sceneTexture, bounds, null, Color.LightBlue, 0, Vector2.Zero, SpriteEffects.None, this.Depth);
        }
    }
}
