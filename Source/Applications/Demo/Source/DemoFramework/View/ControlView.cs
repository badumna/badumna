﻿// -----------------------------------------------------------------------
// <copyright file="ControlView.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace DemoFramework
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;

    /// <summary>
    /// A view for buttons for command sets.
    /// </summary>
    public class ControlView : IView
    {
        /// <summary>
        /// Amount to offset the button draw depth to ensure that text appears over buttons.
        /// </summary>
        private const float TextDepthAdjustment = 0.01f;

        /// <summary>
        /// Width of UI buttons.
        /// </summary>
        private const int DefaultButtonWidth = 100;

        /// <summary>
        /// Height of UI buttons.
        /// </summary>
        private const int ButtonHeight = 30;

        /// <summary>
        /// Margin between UI buttons and window edge.
        /// </summary>
        private const int ButtonMargin = 10;

        /// <summary>
        /// Spacing between UI buttons.
        /// </summary>
        private const int ButtonSpacing = 5;

        /// <summary>
        /// Spacing between UI buttons.
        /// </summary>
        private const int ButtonSetSpacing = 10;

        /// <summary>
        /// Button widths that can be used (in increasing order).
        /// </summary>
        private readonly List<int> buttonWidths = new List<int>
        {
            (DefaultButtonWidth / 3),
            DefaultButtonWidth,
            (DefaultButtonWidth * 2) + ButtonSpacing
        };

        /// <summary>
        /// A predicate for determining whether this view is visible.
        /// </summary>
        private readonly Func<bool> visibilityPredicate;

        /// <summary>
        /// The depth at which to draw the view.
        /// </summary>
        private readonly float depth;

        /// <summary>
        /// Lock object for synchronising button updates.
        /// </summary>
        private object updateLock = new object();

        /// <summary>
        /// A value indicating whether buttons need to be updated before drawing.
        /// </summary>
        private bool updateRequired = false;

        /// <summary>
        /// A texture for drawing buttons.
        /// </summary>
        private Texture2D buttonTexture;

        /// <summary>
        /// A font for drawing button labels.
        /// </summary>
        private SpriteFont buttonFont;

        /// <summary>
        /// The command sets that have been registered with the view.
        /// </summary>
        private List<CommandList> commandSets = new List<CommandList>();

        /// <summary>
        /// All buttons to be rendered, accessible by command set.
        /// </summary>
        private Dictionary<CommandList, List<Button>> buttons =
            new Dictionary<CommandList, List<Button>>();

        /// <summary>
        /// Initialises a new instance of the ControlView class.
        /// </summary>
        /// <param name="visibilityPredicate">A predicate for determining whether the view should be visible.</param>
        /// <param name="depth">The depth to draw this view at.</param>
        public ControlView(Func<bool> visibilityPredicate, float depth)
        {
            this.visibilityPredicate = visibilityPredicate;
            this.depth = depth;
        }

        /// <summary>
        /// Gets a value indicating whether this view is visible.
        /// </summary>
        public bool IsVisible
        {
            get { return this.visibilityPredicate(); }
        }

        /// <summary>
        /// Gets the depth at which to draw the view.
        /// </summary>
        public float Depth
        {
            get { return this.depth; }
        }

        /// <summary>
        /// Adds a command set for a presentation object's commands.
        /// </summary>
        /// <param name="commandSet">The presentation object.</param>
        public void AddCommandSet(ICommandPresentation commandSet)
        {
            this.AddCommandSet(new CommandList(commandSet.Commands));
        }

        /// <summary>
        /// Adds a command set that requires buttons to be drawn for it.
        /// </summary>
        /// <param name="commandList">The command list.</param>
        public void AddCommandSet(CommandList commandList)
        {
            this.commandSets.Add(commandList);
            this.UpdateButtons();
            commandList.Commands.CollectionChanged += (sender, obj) => this.UpdateButtons();
        }

        /// <inheritdoc />
        public void LoadContent(ContentManager content)
        {
            this.buttonTexture = content.Load<Texture2D>("Pixel");
            this.buttonFont = content.Load<SpriteFont>("Font");
        }
        
        /// <inheritdoc />
        public void Draw(SpriteBatch spriteBatch, GraphicsDevice graphicsDevice)
        {
            lock (this.updateLock)
            {
                if (this.updateRequired)
                {
                    this.DoUpdateButtons();
                    this.updateRequired = false;
                }
            }

            foreach (var commands in this.commandSets)
            {
                foreach (var button in this.buttons[commands])
                {
                    this.DrawButton(spriteBatch, button);
                }
            }
        }

        /// <inheritdoc />
        public bool HandleInput(Point input)
        {
            foreach (Button button in this.buttons.Values.SelectMany(x => x))
            {
                if (button.HandleInput(input))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Triggers a button update on the next draw cycle.
        /// </summary>
        private void UpdateButtons()
        {
            lock (this.updateLock)
            {
                this.updateRequired = true;
            }
        }

        /// <summary>
        /// Update buttons for all command sets.
        /// </summary>
        private void DoUpdateButtons()
        {
            int rownum = 0;
            int locationY = ButtonMargin;
            int locationX = ButtonMargin;
            int colnum = 0;

            Action<bool> incrementRow = delegate(bool isWrapped)
            {
                locationY += (isWrapped ? 0 : ButtonSetSpacing) + ButtonSpacing + ButtonHeight;
                locationX = ButtonMargin;
                colnum = 0;
                rownum += 1;
            };

            foreach (CommandList commandList in this.commandSets)
            {
                List<Button> newButtons = new List<Button>();
                foreach (var command in commandList.Commands)
                {
                    if (colnum == commandList.Columns)
                    {
                        incrementRow(true);
                    }

                    int width = this.ButtonWidth(command);
                    newButtons.Add(new Button(
                        new Rectangle(locationX, locationY, width, ButtonHeight),
                        command));
                    locationX += width + ButtonSpacing;
                    colnum += 1;
                }

                this.buttons[commandList] = newButtons;
                incrementRow(false);
            }
        }

        /// <summary>
        /// Get the minimum button width to contain the command's text, where valid button widths are
        /// MinButtonWidth + (n * (MinButtonWidth + ButtonSpacing))
        /// </summary>
        /// <param name="command">The command.</param>
        /// <returns>The button width</returns>
        private int ButtonWidth(Command command)
        {
            if (command.Name.Length == 1)
            {
                // special case single-char buttons
                return this.buttonWidths[0];
            }

            float textWidth = this.buttonFont.MeasureString(command.Name).X;
            int width = 0;
            foreach (int w in this.buttonWidths.Skip(1))
            {
                width = w;
                if (textWidth < width + ButtonSpacing)
                {
                    break;
                }
            }

            return width;
        }

        /// <summary>
        /// Draw a button.
        /// </summary>
        /// <remarks>Must be called between inside sprite batch begin/end.</remarks>
        /// <param name="spriteBatch">Sprite batch for drawing.</param>
        /// <param name="button">The button to draw.</param>
        private void DrawButton(SpriteBatch spriteBatch, Button button)
        {
            spriteBatch.Draw(
                this.buttonTexture,
                button.Bounds,
                null,
                Color.LightGray,
                0,
                Vector2.Zero,
                SpriteEffects.None,
                this.Depth + TextDepthAdjustment);
            Color textColour = button.Command.IsEnabled ? Color.Black : Color.DarkGray;
            Vector2 textSize = this.buttonFont.MeasureString(button.Command.Name);
            Vector2 textPosition = new Vector2(button.Bounds.Center.X, button.Bounds.Center.Y) - (textSize / 2);
            spriteBatch.DrawString(
                this.buttonFont,
                button.Command.Name,
                textPosition,
                textColour,
                0,
                Vector2.Zero,
                1,
                SpriteEffects.None,
                this.Depth);
        }

        /// <summary>
        /// A command list contains a set of commands along with
        /// layout information (maximum number of button columns to present)
        /// </summary>
        public class CommandList
        {
            /// <summary>
            /// Default maximum number of columns to display.
            /// </summary>
            private const int DefaultColumns = 4;

            /// <summary>
            /// Initialises a new instance of the CommandList class.
            /// </summary>
            /// <param name="commands">Commands to include in the list.</param>
            /// <param name="columns">The maximum number of columns to use for displaying commands.</param>
            public CommandList(ObservableCollection<Command> commands, int columns)
            {
                this.Commands = commands;
                this.Columns = columns;
            }

            /// <summary>
            /// Initialises a new instance of the CommandList class.
            /// </summary>
            /// <param name="commands">Commands to include in the list.</param>
            public CommandList(ObservableCollection<Command> commands)
                : this(commands, DefaultColumns)
            {
            }

            /// <summary>
            /// Gets the maximum number of columns to use when displaying commands.
            /// </summary>
            public int Columns { get; private set; }

            /// <summary>
            /// Gets the commands in the list.
            /// </summary>
            public ObservableCollection<Command> Commands { get; private set; }
        }
    }
}
