﻿// -----------------------------------------------------------------------
// <copyright file="ReplicationView.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace DemoFramework
{
    using BadumnaDemo;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;

    /// <summary>
    /// Responsible for drawing replicated game state.
    /// </summary>
    public class ReplicationView : IView
    {
        /// <summary>
        /// The world presentation.
        /// </summary>
        private readonly ReplicationPresentation presentation;

        /// <summary>
        /// The depth at which to draw the view.
        /// </summary>
        private readonly float depth;

        /// <summary>
        /// Sprite for drawing avatars.
        /// </summary>
        private Texture2D avatarSprite;

        /// <summary>
        /// Avatar sprite origin for centring sprite.
        /// </summary>
        private Vector2 avatarSpriteOrigin;

        /// <summary>
        /// Initialises a new instance of the ReplicationView class.
        /// </summary>
        /// <param name="presentation">The world presentation.</param>
        /// <param name="depth">The depth at which to draw the view.</param>
        public ReplicationView(ReplicationPresentation presentation, float depth)
        {
            this.presentation = presentation;
            this.depth = depth;
        }

        /// <inheritdoc />
        public bool IsVisible
        {
            get { return this.presentation.IsVisible; }
        }

        /// <summary>
        /// Gets the depth at which to draw the view.
        /// </summary>
        public float Depth
        {
            get { return this.depth; }
        }

        /// <summary>
        /// Load graphics content.
        /// </summary>
        /// <param name="content">Content module for loading textures, fonts etc.</param>
        public void LoadContent(ContentManager content)
        {
            this.avatarSprite = content.Load<Texture2D>("Avatar");
            this.avatarSpriteOrigin = new Vector2(this.avatarSprite.Width / 2, this.avatarSprite.Height / 2);
        }

        /// <summary>
        /// Called each tick to draw the game state and UI.
        /// </summary>
        /// <param name="spriteBatch">Sprite batch for drawing.</param>
        /// <param name="graphicsDevice">Graphics device (not used).</param>
        public void Draw(SpriteBatch spriteBatch, GraphicsDevice graphicsDevice)
        {
            foreach (IAvatar avatar in this.presentation.Avatars)
            {
                this.DrawAvatar(spriteBatch, avatar);
            }
        }

        /// <inheritdoc />
        public bool HandleInput(Point input)
        {
            this.presentation.Move(input.X, input.Y);
            return true;
        }

        /// <summary>
        /// Draw an avatar.
        /// </summary>
        /// <remarks>Must be called between inside sprite batch begin/end.</remarks>
        /// <param name="spriteBatch">Sprite batch for drawing.</param>
        /// <param name="avatar">The avatar to render.</param>
        private void DrawAvatar(SpriteBatch spriteBatch, IAvatar avatar)
        {
            spriteBatch.Draw(
                this.avatarSprite,
                new Vector2(avatar.Position.X, avatar.Position.Y),
                null,
                new Color(avatar.Colour.R, avatar.Colour.G, avatar.Colour.B),
                0f,
                this.avatarSpriteOrigin,
                1f,
                SpriteEffects.None,
                this.Depth);
        }
    }
}
