﻿// -----------------------------------------------------------------------
// <copyright file="LogView.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace DemoFramework
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;

    /// <summary>
    /// Displays the most recent log events on-screen.
    /// </summary>
    public class LogView : IView
    {
        /// <summary>
        /// The log to view.
        /// </summary>
        private readonly LogPresentation eventLog;

        /// <summary>
        /// The depth at which to draw this view.
        /// </summary>
        private readonly float depth;

        /// <summary>
        /// Distance at which to space log entries.
        /// </summary>
        private float lineSpacing = 18f;

        /// <summary>
        /// Padding between log and the bottom of the viewport
        /// </summary>
        private float padding = 6f;

        /// <summary>
        /// A font for drawing log entries.
        /// </summary>
        private SpriteFont font;

        /// <summary>
        /// Initialises a new instance of the LogView class.
        /// </summary>
        /// <param name="eventLog">The log to view.</param>
        /// <param name="depth">The depth at which to draw this view.</param>
        public LogView(LogPresentation eventLog, float depth)
        {
            this.eventLog = eventLog;
            this.depth = depth;
        }

        /// <inheritdoc />
        /// <remarks>The log view is always visible.</remarks>
        public bool IsVisible
        {
            get { return true; }
        }

        /// <inheritdoc />
        public float Depth
        {
            get { return this.depth; }
        }

        /// <inheritdoc />
        public void LoadContent(ContentManager content)
        {
            this.font = content.Load<SpriteFont>("Font");
        }

        /// <inheritdoc />
        public void Draw(SpriteBatch spriteBatch, GraphicsDevice graphicsDevice)
        {
            Rectangle bounds = graphicsDevice.Viewport.Bounds;

            // anchor to the bottom left of the viewport
            Vector2 location = new Vector2(this.padding, bounds.Bottom - (this.lineSpacing + this.padding));
            foreach (var message in this.eventLog.Messages)
            {
                spriteBatch.DrawString(
                    this.font,
                    message,
                    location,
                    new Color(0, 0, 0, 200),
                    0,
                    Vector2.Zero,
                    0.8f,
                    SpriteEffects.None,
                    this.Depth);
                location.Y -= this.lineSpacing;
            }
        }

        /// <inheritdoc />
        public bool HandleInput(Point input)
        {
            return false;
        }
    }
}
