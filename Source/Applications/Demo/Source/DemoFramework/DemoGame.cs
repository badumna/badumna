﻿//-----------------------------------------------------------------------
// <copyright file="DemoGame.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace DemoFramework
{
    using System.Collections.Generic;
    using System.Linq;
    using BadumnaDemo;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;

    /// <summary>
    /// Responsible for starting the simulation, handling user input, and rendering the scene.
    /// </summary>
    public class DemoGame : Microsoft.Xna.Framework.Game
    {
        /// <summary>
        /// Presentations to be regularly updated.
        /// </summary>
        private readonly IEnumerable<ITickablePresentation> presentations;

        /// <summary>
        /// Views to be drawn.
        /// </summary>
        private readonly IEnumerable<IView> views;

        /// <summary>
        /// Module for touch and/or mouse input.
        /// </summary>
        private readonly InputManager inputModule;

        /// <summary>
        /// Sprite batch passed to views for drawing.
        /// </summary>
        private SpriteBatch spriteBatch;

        /// <summary>
        /// Initialises a new instance of the DemoGame class.
        /// </summary>
        /// <param name="presentations">Modules to be regularly updated.</param>
        /// <param name="views">The views to be drawn.</param>
        public DemoGame(IEnumerable<ITickablePresentation> presentations, IEnumerable<IView> views)
        {
            new GraphicsDeviceManager(this);
            this.Content.RootDirectory = "Content";

            this.presentations = presentations;
            this.views = views.OrderBy(v => v.Depth).ToList();
            this.inputModule = new InputManager();
        }

        /// <summary>
        /// Occurs when shutting down.
        /// </summary>
        /// <remarks>Used on mobile platforms to notify Badumna that it needs to shutdown.</remarks>
        public event ShutdownHandler ShuttingDown;

        /// <summary>
        /// Warn of shutdown.
        /// </summary>
        public void Shutdown()
        {
            ShutdownHandler handler = this.ShuttingDown;
            if (handler != null)
            {
                handler.Invoke();
            }
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            this.IsMouseVisible = true;
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            this.spriteBatch = new SpriteBatch(this.GraphicsDevice);

            foreach (var view in this.views)
            {
                view.LoadContent(this.Content);
            }
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Handle any input.
            if (this.IsActive)
            {
                this.inputModule.Update();
                int x, y;
                if (this.inputModule.IsNewlyTouched(out x, out y))
                {
                    var location = new Point(x, y);
                    foreach (var view in this.views)
                    {
                        if (view.IsVisible && view.HandleInput(location))
                        {
                            break;
                        }
                    }
                }
            }

            // Update all presentations.
            float elapsedMilliseconds = (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            foreach (var presentation in this.presentations)
            {
                presentation.Update(elapsedMilliseconds);
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            this.GraphicsDevice.Clear(Color.CornflowerBlue);
            this.spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);
            foreach (var view in this.views)
            {
                if (view.IsVisible)
                {
                    view.Draw(this.spriteBatch, this.GraphicsDevice);
                }
            }

            this.spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}