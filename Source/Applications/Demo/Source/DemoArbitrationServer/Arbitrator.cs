﻿//-----------------------------------------------------------------------
// <copyright file="Arbitrator.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace FriendListArbitrationServer
{
    using System;
    using System.Collections.ObjectModel;
    using System.Threading;
    using Badumna;
    using Badumna.Arbitration;
    using DemoArbitrationEvents;

    /// <summary>
    /// Arbitrator to make decisions on client requests.
    /// </summary>
    internal class Arbitrator
    {
        /// <summary>
        /// The Data Access Layer (DAL) through which the database is accessed.
        /// </summary>
        private readonly IArbitrationDAL arbitrationDAL;

        /// <summary>
        /// The network facade through which to access Badumna features.
        /// </summary>
        private INetworkFacade networkFacade;

        /// <summary>
        /// Initialises a new instance of the Arbitrator class.
        /// </summary>
        /// <param name="networkFacade">The network facade to use.</param>
        /// <param name="arbitrationDAL">The DAL through which to access the database.</param>
        /// <param name="dbProvider">The name of the database provider to use.</param>
        /// <param name="dbConnectionString">A string used to connect to the database.</param>
        internal Arbitrator(INetworkFacade networkFacade, IArbitrationDAL arbitrationDAL, string dbProvider, string dbConnectionString)
        {
            if (arbitrationDAL == null)
            {
                throw new ArgumentNullException("arbitrationDAL");
            }

            this.networkFacade = networkFacade;
            this.arbitrationDAL = arbitrationDAL;
            this.arbitrationDAL.Initialize(dbProvider, dbConnectionString);
        }

        /// <summary>
        /// Initialises a new instance of the Arbitrator class.
        /// </summary>
        /// <param name="networkFacade">The network facade to use.</param>
        /// <param name="dbProvider">The database provider.</param>
        /// <param name="dbConnectionString">The database connection string.</param>
        internal Arbitrator(INetworkFacade networkFacade, string dbProvider, string dbConnectionString) :
            this(networkFacade, new ArbitrationDAL(), dbProvider, dbConnectionString)
        {
        }

        /// <summary>
        /// Handle Arbitration requests from clients
        /// </summary>
        /// <param name="sessionId">ID of the client session this request came from.</param>
        /// <param name="message">The event details</param>
        internal void HandleClientEvent(int sessionId, byte[] message)
        {
            ArbitrationEvent arbitrationEvent = FriendListEventSet.Deserialize(message);
            FriendListRequest friendListRequest = arbitrationEvent as FriendListRequest;
            if (friendListRequest != null)
            {
                Console.WriteLine("Handling friend list request");
                ThreadPool.QueueUserWorkItem((_) => this.HandleFriendListRequest(sessionId, friendListRequest));
            }
            else
            {
                Console.WriteLine("Ignoring unknown request.");
            }
        }

        /// <summary>
        /// Handle client disconnections.
        /// This method is registered with the network facade, and will be called when clients disconnect.
        /// </summary>
        /// <param name="sessionId">ID of the client session that disconnected.</param>
        internal void HandleClientDisconnect(int sessionId)
        {
            // This server doesn't care about client disconnection.
        }

        /// <summary>
        /// Installation creates database tables and populates them with demo data.
        /// </summary>
        /// <param name="friendListsFilename">Name of file containing friend lists to use.</param>
        internal void Install(string friendListsFilename)
        {
            this.arbitrationDAL.Install(friendListsFilename);
        }

        /// <summary>
        /// Called to tidy up when finished with arbitrator.
        /// </summary>
        internal void OnShutdown()
        {
            this.arbitrationDAL.Shutdown();
        }

        /// <summary>
        /// Handle status requests received from the client. 
        /// </summary>
        /// <param name="sessionId">The ID of the session the event belongs to.</param>
        /// <param name="request">The event received.</param>
        private void HandleFriendListRequest(int sessionId, FriendListRequest request)
        {
            Console.WriteLine("Handling friend list request...");
            Collection<string> friendList = new Collection<string>(this.arbitrationDAL.GetFriends(request.UserName));
            Console.WriteLine("{0} friends found.", friendList.Count);
            FriendListReply reply = new FriendListReply(friendList);
            this.networkFacade.SendServerArbitrationEvent(sessionId, FriendListEventSet.Serialize(reply));
        }
    }
}
