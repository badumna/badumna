﻿//-----------------------------------------------------------------------
// <copyright file="FriendListFileReader.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace FriendListArbitrationServer
{
    using System.Collections.Generic;
    using System.IO;

    /// <summary>
    /// A helper class for reading friend lists from a text file.
    /// </summary>
    internal class FriendListFileReader
    {
        #region Fields
        
        /// <summary>
        /// Map of usernames to friend lists.
        /// </summary>
        private readonly Dictionary<string, List<string>> friendLists;
        
        #endregion // Fields

        #region Constructors

        /// <summary>
        /// Initialises a new instance of the FriendListFileReader class.
        /// </summary>
        /// <param name="filename">The name of the file holding the friend lists.</param>
        internal FriendListFileReader(string filename)
        {
            this.friendLists = new Dictionary<string, List<string>>();
            using (TextReader tr = new StreamReader(filename))
            {
                while (tr.Peek() >= 0)
                {
                    string line = tr.ReadLine();              
                    List<string> nameList = new List<string>(line.Split(' ', ','));
                    if (nameList.Count > 1)
                    {
                        this.friendLists.Add(
                            nameList[0],
                            nameList.GetRange(1, nameList.Count - 1));
                    }
                }

                tr.Close();
            }
        }

        #endregion // Constructors

        #region Properties

        /// <summary>
        /// Gets the usernames read from the file.
        /// </summary>
        internal IEnumerable<string> Usernames
        {
            get
            {
                foreach (string username in this.friendLists.Keys)
                {
                    yield return username;
                }
            }
        }

        #endregion // Properties

        #region Public methods

        /// <summary>
        /// Get the friends for a given username.
        /// </summary>
        /// <param name="userName">The username to retrieve friends for.</param>
        /// <returns>The name of the given user's friends.</returns>
        internal IEnumerable<string> GetFriends(string userName)
        {
            if (this.friendLists.ContainsKey(userName))
            {
                foreach (string friend in this.friendLists[userName])
                {
                    yield return friend;
                }
            }
        }
        
        #endregion // Public methods
    }
}
