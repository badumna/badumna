﻿//-----------------------------------------------------------------------
// <copyright file="ArbitrationProcess.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace FriendListArbitrationServer
{
    using System;
    using System.Configuration;
    using System.IO;
    using Badumna;
    using GermHarness;

    /// <summary>
    /// Hosted process to provide test arbitration service to a Badumna network.
    /// </summary>
    [Badumna.Core.DontPerformCoverage]
    internal class ArbitrationProcess : IHostedProcess
    {
        /// <summary>
        /// The peer harness hosting this process.
        /// </summary>
        private readonly IPeerHarness peerHarness;

        /// <summary>
        /// The arbitrator.
        /// </summary>
        private Arbitrator arbitrator;

        /// <summary>
        /// Initialises a new instance of the ArbitrationProcess class.
        /// </summary>
        /// <param name="peerHarness">The peer harness hosting this process.</param>
        public ArbitrationProcess(IPeerHarness peerHarness)
        {
            if (peerHarness == null)
            {
                throw new ArgumentNullException("peerHarness");
            }

            this.peerHarness = peerHarness;
        }

        #region IHostedProcess Members

        /// <summary>
        /// Called by process host to initialize the process.
        /// </summary>
        /// <param name="arguments">Command line arguments passed by process host.</param>
        public void OnInitialize(ref string[] arguments)
        {
        }

        /// <summary>
        /// Called periodically by the process host, to trigger regularly scheduled processing.
        /// </summary>
        /// <param name="delayMilliseconds">Milliseconds since last called.</param>
        /// <returns>Flag indicating success or errors.</returns>
        public bool OnPerformRegularTasks(int delayMilliseconds)
        {
            return true;
        }

        /// <summary>
        /// Called by the process host to pass a request.
        /// Does nothing, but is required by interface.
        /// </summary>
        /// <param name="requestType">Type of the request (usually cast from enumeration).</param>
        /// <param name="request">The request that the process should know how to handle.</param>
        /// <returns>Response for host.</returns>
        public byte[] OnProcessRequest(int requestType, byte[] request)
        {
            return null;
        }

        /// <summary>
        /// Called by the process host on shutdown.
        /// </summary>
        public void OnShutdown()
        {
            this.arbitrator.OnShutdown();
        }

        /// <summary>
        /// Called by process host on start.
        /// </summary>
        /// <returns>Success flag (always true at present).</returns>
        public bool OnStart()
        {
            string dbProvider = ConfigurationManager.AppSettings["dbProvider"];
            string connectionString = ConfigurationManager.ConnectionStrings[dbProvider].ConnectionString;
            this.arbitrator = new Arbitrator(
                this.peerHarness.NetworkFacadeInstance,
                dbProvider,
                connectionString);
            
            this.arbitrator.Install("FriendLists.txt");

            // registering the ArbitrationEventHandler
            this.peerHarness.NetworkFacadeInstance.RegisterArbitrationHandler(
                this.arbitrator.HandleClientEvent,
                TimeSpan.FromSeconds(60),
                this.arbitrator.HandleClientDisconnect);
            Console.WriteLine("The arbitrator is in session.");
            return true;
        }

        /// <summary>
        /// Write out supported command line options.
        /// </summary>
        /// <param name="tw">The text writer to use.</param>
        public void WriteOptionsDescription(TextWriter tw)
        {
            // No process-specific command line options.
        }

        #endregion
    }
}
