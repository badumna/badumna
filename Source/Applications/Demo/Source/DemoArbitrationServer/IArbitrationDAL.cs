﻿//-----------------------------------------------------------------------
// <copyright file="IArbitrationDAL.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace FriendListArbitrationServer
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    /// <summary>
    /// Specifies the interface for database managers handling arbitration requests.
    /// </summary>
    internal interface IArbitrationDAL
    {
        /// <summary>
        /// Prepare for arbitration.
        /// </summary>
        /// <param name="dbProvider">The name of the database provider to use.</param>
        /// <param name="dbConnectionString">A string used to connect to the database.</param>
        /// <remarks>Arbitrate() should not be called before this method is called.</remarks>
        void Initialize(string dbProvider, string dbConnectionString);

        /// <summary>
        /// Populate the database with tables and demo data from the specified file.
        /// </summary>
        /// <param name="friendListsFilename">The name of the file containing demo data.</param>
        void Install(string friendListsFilename);

        /// <summary>
        /// Perform any clean up after arbitration has finished.
        /// </summary>
        /// <remarks>GetFriends() should not be called after this method is called.</remarks>
        void Shutdown();

        /// <summary>
        /// Get a player's friend list.
        /// </summary>
        /// <param name="userName">The ID of the player.</param>
        /// <returns>A list of the IDs of the player's friends.</returns>
        ReadOnlyCollection<string> GetFriends(string userName);
    }
}
