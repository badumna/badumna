﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace FriendListArbitrationServer
{
    using System;
    using System.Collections.Generic;
    using Badumna;
    using Badumna.Security;
    using Badumna.ServiceDiscovery;
    using GermHarness;
    using Mono.Options;

    /// <summary>
    /// Program class to hold main routine.
    /// </summary>
    [Badumna.Core.DontPerformCoverage]
    internal class Program
    {
        /// <summary>
        /// The peer harness for hosting the process and communicating with Control Center.
        /// </summary>
        private static PeerHarness peerHarness;

        /// <summary>
        /// The option set for parsing command line arguments.
        /// </summary>
        private static OptionSet optionSet = new OptionSet()
            {
                { "h|help", "show this message and exit.", v =>
                    {
                        if (v != null)
                        {
                            Program.WriteUsageAndExit();
                        }
                    }
                }
            };

        /// <summary>
        /// Entry point to the program.
        /// Creates a PeerHarness and uses it to run the arbitration process.
        /// </summary>
        /// <remarks>
        /// If your network makes use of a secure Dei server, you'll need to configure this
        /// arbitration server to authenticate with Dei. An example configuration is provided,
        /// which can be activated using the following command line argument:
        /// '--dei-config-file=FriendListArbitrationServer.deiconfig'
        /// </remarks>
        /// <param name="arguments">Command line arguments.</param>
        internal static void Main(string[] arguments)
        {
            // Parse program-specific command line arguments.
            List<string> extras = null;
            try
            {
                Options options = new Options();
                
                // Set the default application name to match DemoGame.
                // This can be overridden with the --application-name command line argument
                options.Connectivity.ApplicationName = "api-example";

                options.Connectivity.ConfigureForLan();
                options.Connectivity.IsHostedService = true;
                options.Arbitration.Servers.Add(new ArbitrationServerDetails("friendserver"));
                peerHarness = new PeerHarness(options);

                extras = optionSet.Parse(arguments);

                string[] harnessArgs = extras.ToArray();
                if (peerHarness.Initialize(ref harnessArgs, new ArbitrationProcess(peerHarness)))
                {
                    if (harnessArgs.Length > 0)
                    {
                        string assemblyName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
                        Console.Write("{0}: Unknown argument(s):", assemblyName);
                        foreach (string arg in harnessArgs)
                        {
                            Console.Write(" " + arg);
                        }

                        Console.WriteLine(".");
                        Console.WriteLine("Try `{0} --help' for more information.", assemblyName);
                    }
                    else
                    {
                        // Start the arbitration process.
                        peerHarness.RegisterService(ServerType.Arbitration);
                        peerHarness.Start();
                    }
                }
                else
                {
                    Console.WriteLine("Could not initialize peer harness.");
                }
            }
            catch (OptionException ex)
            {
                string assemblyName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
                Console.Write("{0}: ", assemblyName);
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try `{0} --help' for more information.", assemblyName);
                Environment.Exit((int)ExitCode.InvalidArguments);
            }
            catch (DeiAuthenticationException ex)
            {
                Console.WriteLine(ex.Message);
                Environment.Exit((int)ExitCode.DeiAuthenticationFailed);
            }
            catch (SecurityException ex)
            {
                Console.WriteLine("Exception caught : " + ex.Message);
                Environment.Exit((int)ExitCode.AnnounceServiceFailed);
            }
        }

        /// <summary>
        /// Writes the usage.
        /// </summary>
        private static void WriteUsageAndExit()
        {
            string assemblyName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
            Console.WriteLine("Usage: {0} [Options]", assemblyName);
            Console.WriteLine("Start a Friend List Arbitration Server on the local machine.");
            Console.WriteLine(string.Empty);
            Console.WriteLine("Options:");
            peerHarness.WriteOptionDescriptions(Console.Out);
            Program.optionSet.WriteOptionDescriptions(Console.Out);
            Environment.Exit((int)ExitCode.DisplayHelp);
        }
    }
}
