﻿//-----------------------------------------------------------------------
// <copyright file="ArbitrationDAL.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace FriendListArbitrationServer
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.Common;

    /// <summary>
    /// Specifies the interface for database managers handling arbitration requests.
    /// </summary>
    internal class ArbitrationDAL : IArbitrationDAL
    {
        /// <summary>
        /// The name of the table holding friend data.
        /// </summary>
        private const string TableName = "Friends";

        /// <summary>
        /// The name of the table column holding friend names.
        /// </summary>
        private const string FriendNameColumn = "Friend";

        /// <summary>
        /// Parameter name for username in SQL queries.
        /// </summary>
        private const string UserNameParam = "userName";

        /// <summary>
        /// Parameter name for friend name in SQL queries.
        /// </summary>
        private const string FriendNameParam = "friendName";

        /// <summary>
        /// SQL command for selecting friends.
        /// </summary>
        private const string SelectFriendsQuery = "Select Friend From Friends Where Player = @userName";

        /// <summary>
        /// SQL command for creating the friends table.
        /// </summary>
        private const string CreateTableCommand = "CREATE TABLE Friends (Player VARCHAR(50) NOT NULL, Friend VARCHAR(50) NOT NULL)";

        /// <summary>
        /// SQL command for populating the table with data.
        /// </summary>
        private const string PopulateDataCommand = "Insert Into Friends (Player, Friend) Values (@userName, @friendName)";

        /// <summary>
        /// SQL command for removing the friends table.
        /// </summary>
        private const string DropTableCommand = "DROP TABLE Friends";

        /// <summary>
        /// Factory for creating instances of vendor-specific data source classes.
        /// </summary>
        private DbProviderFactory factory;

        /// <summary>
        /// Connection string for database connection.
        /// </summary>
        private string connectionString;

        /// <summary>
        /// Initialises a new instance of the ArbitrationDAL class.
        /// </summary>
        public ArbitrationDAL()
        {
        }

        /// <summary>
        /// Prepare for arbitration.
        /// </summary>
        /// <param name="dbProvider"> The name of the database provider to use.</param>
        /// <param name="connectionString">A string used to connect to the database.</param>
        /// <remarks>Arbitrate() should not be called before this method is called.</remarks>
        public void Initialize(string dbProvider, string connectionString)
        {
            // Error handling code for bad provider, or connection failure would go here in production code.
            this.factory = DbProviderFactories.GetFactory(dbProvider);
            this.connectionString = connectionString;
        }

        /// <summary>
        /// Check to see if the table exists.
        /// </summary>
        /// <param name="tableName">The name of the table to check for.</param>
        /// <returns>A value indicating whether the table exists.</returns>
        public bool TableExists(string tableName)
        {
            using (DbConnection connection = this.factory.CreateConnection())
            {
                connection.ConnectionString = this.connectionString;
                connection.Open();
                DataTable dataTable = connection.GetSchema(
                    "TABLES",
                    new string[] { null, null, tableName });
                return dataTable.Rows.Count > 0;
            }
        }

        /// <summary>
        /// Create tables in database and populate with demo data.
        /// </summary>
        /// <param name="friendListFilename">The name of the file containing demo data.</param>
        public void Install(string friendListFilename)
        {
            if (this.TableExists(ArbitrationDAL.TableName))
            {
                this.RemoveTable();
            }

            this.CreateTable();
            this.CreateData(friendListFilename);
        }

        /// <summary>
        /// Perform any clean up after arbitration has finished.
        /// </summary>
        /// <remarks>Arbitrate() should not be called after this method is called.</remarks>
        public void Shutdown()
        {
        }

        /// <summary>
        /// Get a player's friend list.
        /// </summary>
        /// <param name="userName">The name of the player.</param>
        /// <returns>A list of the names of the player's friends.</returns>
        public ReadOnlyCollection<string> GetFriends(string userName)
        {
            using (var command = this.factory.CreateCommand())
            {
                using (var connection = this.factory.CreateConnection())
                {
                    connection.ConnectionString = this.connectionString;
                    command.CommandText = ArbitrationDAL.SelectFriendsQuery;
                    var param = this.factory.CreateParameter();
                    param.ParameterName = ArbitrationDAL.UserNameParam;
                    param.Value = userName;
                    command.Parameters.Add(param);
                    command.Connection = connection;
                    connection.Open();
                    using (var dataReader = command.ExecuteReader())
                    {
                        List<string> friendList = new List<string>();
                        while (dataReader.Read())
                        {
                            friendList.Add((string)dataReader[ArbitrationDAL.FriendNameColumn]);
                        }

                        return new ReadOnlyCollection<string>(friendList);
                    }
                }
            }
        }

        /// <summary>
        /// Create the table in the database.
        /// </summary>
        private void CreateTable()
        {
            this.PerformNonQuery(this.SpecifyCreateTableCommand);
        }

        /// <summary>
        /// Populates a database command object with the command text for table creation.
        /// </summary>
        /// <param name="command">The command to populate.</param>
        private void SpecifyCreateTableCommand(DbCommand command)
        {
            command.CommandText = ArbitrationDAL.CreateTableCommand;
        }

        /// <summary>
        /// Create the data in the database table.
        /// </summary>
        /// <param name="friendListFile">The name of the file containing the friend lists.</param>
        private void CreateData(string friendListFile)
        {
            FriendListFileReader fileReader = new FriendListFileReader(friendListFile);
            foreach (string userName in fileReader.Usernames)
            {
                foreach (string friendName in fileReader.GetFriends(userName))
                {
                    // Avoid closing over loop variables until C# 5.
                    string currentUser = userName;
                    string currentFriend = friendName;
                    this.PerformNonQuery(c => this.SpecifyDataCreationCommand(c, currentUser, currentFriend));
                }
            }
        }

        /// <summary>
        /// Populates a database command object with the command text and parameters for populating the database.
        /// </summary>
        /// <param name="command">The command to populate.</param>
        /// <param name="userName">The user to add a friend for.</param>
        /// <param name="friendName">The friend to add.</param>
        private void SpecifyDataCreationCommand(DbCommand command, string userName, string friendName)
        {
            command.CommandText = ArbitrationDAL.PopulateDataCommand;
            var param = this.factory.CreateParameter();
            param.ParameterName = ArbitrationDAL.UserNameParam;
            param.Value = userName;
            command.Parameters.Add(param);
            param = this.factory.CreateParameter();
            param.ParameterName = ArbitrationDAL.FriendNameParam;
            param.Value = friendName;
            command.Parameters.Add(param);
        }

        /// <summary>
        /// Remove the table in the database.
        /// </summary>
        private void RemoveTable()
        {
            this.PerformNonQuery(this.SpecifyRemoveTableCommand);
        }

        /// <summary>
        /// Populates a database command object with the command text and parameters for table removal.
        /// </summary>
        /// <param name="command">The command to populate.</param>
        private void SpecifyRemoveTableCommand(DbCommand command)
        {
            command.CommandText = ArbitrationDAL.DropTableCommand;
        }

        /// <summary>
        /// Execute a non query.
        /// </summary>
        /// <param name="commandSpecifier">A function for supplying command text and parameters to a command object.</param>
        /// <returns>A value indicating whether the non query executed without error.</returns>
        private bool PerformNonQuery(Action<DbCommand> commandSpecifier)
        {
            try
            {
                using (var command = this.factory.CreateCommand())
                {
                    commandSpecifier.Invoke(command);
                    using (var connection = this.factory.CreateConnection())
                    {
                        connection.ConnectionString = this.connectionString;
                        command.Connection = connection;
                        connection.Open();
                        command.ExecuteNonQuery();
                        return true;
                    }
                }
            }
            catch (DbException e)
            {
                System.Diagnostics.Trace.TraceError(
                    "Exception while trying to perform query: " + e.Message);
            }

            return false;
        }
    }
}
