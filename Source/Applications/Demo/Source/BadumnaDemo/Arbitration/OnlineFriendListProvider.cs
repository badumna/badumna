﻿// -----------------------------------------------------------------------
// <copyright file="OnlineFriendListProvider.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace BadumnaDemo
{
    using System;
    using System.Collections.Generic;
    using Badumna;
    using Badumna.Arbitration;
    using DemoArbitrationEvents;

    /// <summary>
    /// Retrieves a list of known friends from an arbitration server for private chat.
    /// </summary>
    public class OnlineFriendListProvider : IFriendListProvider
    {
        /// <summary>
        /// Arbitration server to retrieve friend list from.
        /// </summary>
        private IArbitrator arbitrator;

        /// <summary>
        /// The name of the user whose friends should be retrieved.
        /// </summary>
        private string userName;

        /// <summary>
        /// Callback for returning the friend list.
        /// </summary>
        private Action<bool, string, IEnumerable<string>> friendListHandler;

        /// <summary>
        /// A value indicating whether a request to the arbitration server is awaiting a reply.
        /// </summary>
        private bool replyExpected;

        /// <inheritdoc />
        public void RequestFriendsList(
            INetworkFacade network,
            string userName,
            Action<bool, string, IEnumerable<string>> friendsListHandler)
        {
            this.userName = userName;
            this.friendListHandler = friendsListHandler;
            this.arbitrator = network.GetArbitrator("friendserver");
            this.arbitrator.Connect(
                this.HandleConnectionResult,
                this.HandleConnectionFailure,
                this.HandleServerMessage);
        }

        /// <summary>
        /// Handles the arbitration server connection result.
        /// </summary>
        /// <param name="result">The result of the attempt to connect to the arbitration server.</param>
        private void HandleConnectionResult(ServiceConnectionResultType result)
        {
            if (result == ServiceConnectionResultType.Success)
            {
                // Send a request to the arbitration server for a friend list.
                FriendListRequest request = new FriendListRequest(this.userName);
                this.arbitrator.SendEvent(
                    DemoArbitrationEvents.FriendListEventSet.Serialize(request));
                this.replyExpected = true;
            }
            else
            {
                this.friendListHandler.Invoke(false, "Could not connect to server.", null);
            }
        }

        /// <summary>
        /// Handles a arbitration server connection failure.
        /// </summary>
        private void HandleConnectionFailure()
        {
            if (this.replyExpected)
            {
                this.friendListHandler.Invoke(false, "Connection to server failed.", null);
                this.replyExpected = false;
            }
        }

        /// <summary>
        /// Handle messages from the arbitration server.
        /// </summary>
        /// <param name="message">A serialized arbitration event.</param>
        private void HandleServerMessage(byte[] message)
        {
            if (this.replyExpected)
            {
                ArbitrationEvent reply = DemoArbitrationEvents.FriendListEventSet.Deserialize(message);
                if (reply is FriendListReply)
                {
                    // Return the friend list received.
                    FriendListReply friendListReply = reply as FriendListReply;
                    this.friendListHandler.Invoke(true, null, friendListReply.FriendNames);
                }

                this.replyExpected = false;
            }
        }
    }
}
