﻿//-----------------------------------------------------------------------
// <copyright file="DeadReckonedReplicaAvatar.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaDemo
{
    using System.IO;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;

    /// <summary>
    /// Class for representing remote players' avatars.
    /// </summary>
    public class DeadReckonedReplicaAvatar : DeadReckonedAvatar, IReplicaAvatar
    {
        /// <summary>
        /// Update the avatar's state with data deserialized from a stream.
        /// </summary>
        /// <param name="includedParts">The parts of the avatar state that are included in the stream.</param>
        /// <param name="stream">The stream to deserialize state data from.</param>
        /// <param name="estimatedMillisecondsSinceDeparture">Estimated time since the original serialized this state update.</param>
        public void Deserialize(BooleanArray includedParts, Stream stream, int estimatedMillisecondsSinceDeparture)
        {
            BinaryReader reader = new BinaryReader(stream);

            if (includedParts[(int)Avatar.StateSegment.Color])
            {
                byte r = reader.ReadByte();
                byte g = reader.ReadByte();
                byte b = reader.ReadByte();
                this.Colour = new Colour(r, g, b);
            }

            if (includedParts[(int)Avatar.StateSegment.Orientation])
            {
                this.Orientation = reader.ReadSingle();
            }
        }
    }
}
