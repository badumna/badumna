﻿//-----------------------------------------------------------------------
// <copyright file="DeadReckonedAvatar.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaDemo
{
    using System;
    using System.IO;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;

    /// <summary>
    /// Extends Avatar to support dead reckoning.
    /// </summary>
    public class DeadReckonedAvatar : Avatar, IDeadReckonable
    {
        /// <summary>
        /// The velocity of the avatar.
        /// </summary>
        private Vector3 velocity;

        /// <summary>
        /// Gets or sets the velocity of the avatar.
        /// </summary>
        public Vector3 Velocity
        {
            get
            {
                return this.velocity;
            }

            set
            {
                this.velocity = value;
                this.OnVelocityUpdate();
            }
        }

        /// <summary>
        /// Called by Badumna on replicas to update the avatar's position as part of dead reckoning.
        /// </summary>
        /// <param name="reckonedPosition">The new avatar position as calculated by dead reckoning.</param>
        public void AttemptMovement(Vector3 reckonedPosition)
        {
            this.Position = reckonedPosition;
        }

        /// <summary>
        /// Override this method to trigger action on orientation updates.
        /// </summary>
        protected virtual void OnVelocityUpdate()
        {
            // Do nothing.
        }
    }
}
