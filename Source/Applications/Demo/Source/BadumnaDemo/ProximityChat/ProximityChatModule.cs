﻿// -----------------------------------------------------------------------
// <copyright file="ProximityChatModule.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace BadumnaDemo
{
    using Badumna;
    using Badumna.Chat;
    using Badumna.DataTypes;

    /// <summary>
    /// Demonstrates API for proximity chat.
    /// </summary>
    public class ProximityChatModule
    {
        /// <summary>
        /// A value indicating whether proximity chat is enabled.
        /// </summary>
        private bool isEnabled = false;

        /// <summary>
        /// The chat channel for proximity chat.
        /// Entities receive all proximity chat messages from entities within their 
        /// area of interest.
        /// </summary>
        private IChatChannel proximityChatChannel;

        /// <summary>
        /// Initialises a new instance of the ProximityChatModule class.
        /// </summary>
        /// <param name="world">The world the proximity chat is for.</param>
        public ProximityChatModule(ReplicationModule world)
        {
            world.OriginalEntityOnline += this.OnOriginalEntityOnline;
            world.OriginalEntityGoingOffline += this.OnOriginalEntityOffline;
        }

        /// <summary>
        /// An event raised when a proximity chat event occurs.
        /// </summary>
        public event MessageHandler ProximtyChatEventOccurred;

        /// <summary>
        /// Gets a value indicating whether proximity chat is enabled.
        /// </summary>
        public bool IsEnabled
        {
            get { return this.isEnabled; }
        }

        /// <summary>
        /// Sends a proximity chat message.
        /// </summary>
        /// <param name="message">The message to send.</param>
        public void SendMessage(string message)
        {
            this.proximityChatChannel.SendMessage(message);
            this.RaiseLogEvent("Shouted: \"" + message + "\"");
        }

        /// <summary>
        /// Set up chat channel once the original entity has been registered with a scene.
        /// </summary>
        /// <param name="network">The Badumna network facade.</param>
        /// <param name="originalEntityId">The ID of the original entity.</param>
        public void OnOriginalEntityOnline(INetworkFacade network, BadumnaId originalEntityId)
        {
            this.proximityChatChannel = network.ChatSession.SubscribeToProximityChannel(originalEntityId, this.ReceiveMessage);
            this.isEnabled = true;
        }

        /// <summary>
        /// Disable proximity chat on shutdown.
        /// </summary>
        public void OnOriginalEntityOffline()
        {
            this.proximityChatChannel = null;
            this.isEnabled = false;
        }

        /// <summary>
        /// Handler for incoming chat messages.
        /// </summary>
        /// <param name="channel">The channel the message arrived on.</param>
        /// <param name="userId">The id of the user that sent the message.</param>
        /// <param name="message">The message.</param>
        private void ReceiveMessage(IChatChannel channel, BadumnaId userId, string message)
        {
            this.RaiseLogEvent("Heard: \"" + message + "\"");
        }

        /// <summary>
        /// Raise an event indicating that something occurred which should be logged.
        /// </summary>
        /// <param name="message">The message for the log.</param>
        private void RaiseLogEvent(string message)
        {
            MessageHandler handler = this.ProximtyChatEventOccurred;
            if (handler != null)
            {
                handler.Invoke(message);
            }
        }
    }
}
