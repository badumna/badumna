﻿//-----------------------------------------------------------------------
// <copyright file="NetworkModule.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaDemo
{
    using System.Collections.Generic;
    using System.Threading;
    using Badumna;
    using Badumna.Security;

    /// <summary>
    /// Delegate for handling messages.
    /// </summary>
    /// <param name="message">The message.</param>
    public delegate void MessageHandler(string message);

    /// <summary>
    /// Delegate for handling log-in events.
    /// </summary>
    /// <param name="network">The Badumna network facade.</param>
    public delegate void LoginHandler(INetworkFacade network);

    /// <summary>
    /// Delegate for handling shutdown events.
    /// </summary>
    public delegate void ShutdownHandler();

    /// <summary>
    /// Demonstrates how to initialize and log in to Badumna.
    /// </summary>
    public class NetworkModule
    {
        /// <summary>
        /// A list of the states that indicate the network is busy.
        /// </summary>
        private static List<NetworkStatus> busyStates = new List<NetworkStatus>
        {
            NetworkStatus.Initializing,
            NetworkStatus.LoggingIn
        };

        /// <summary>
        /// The Badumna network facade.
        /// </summary>
        private INetworkFacade network;

        /// <summary>
        /// The current status of the network (e.g. initialized, logged in, etc.).
        /// </summary>
        private NetworkStatus status = NetworkStatus.Uninitialized;

        /// <summary>
        /// The network options.
        /// </summary>
        private Options options;

        /// <summary>
        /// A value indicating whether a login result has been received since last update.
        /// </summary>
        private bool loginResultReceived;

        /// <summary>
        /// The result of the last login attempt.
        /// </summary>
        private bool loginResult;

        /// <summary>
        /// Initialises a new instance of the NetworkModule class.
        /// </summary>
        /// <param name="options">The network options</param>
        public NetworkModule(Options options)
        {
            this.options = options;
            this.Initialize();
        }

        /// <summary>
        /// Event raised when an log-in has completed.
        /// </summary>
        public event LoginHandler LoggedIn;

        /// <summary>
        /// Event raised just before shutting down.
        /// </summary>
        public event ShutdownHandler ShuttingDown;

        /// <summary>
        /// The possible network statuses.
        /// </summary>
        private enum NetworkStatus
        {
            /// <summary>
            /// The network has not been initialized.
            /// </summary>
            Uninitialized,

            /// <summary>
            /// The network is currently initializing.
            /// </summary>
            Initializing,

            /// <summary>
            /// The network has been initialized, but not logged in to.
            /// </summary>
            Initialized,

            /// <summary>
            /// Log in to the network is in progress.
            /// </summary>
            LoggingIn,

            /// <summary>
            /// Logged in to the network.
            /// </summary>
            LoggedIn,

            /// <summary>
            /// Log in failed.
            /// </summary>
            LoginFailed,

            /// <summary>
            /// A network address change has been detected.
            /// </summary>
            AddressChanged
        }
        
        /// <summary>
        /// Gets the Badumna network facade.
        /// </summary>
        public INetworkFacade Network
        {
            get { return this.network; }
        }

        /// <summary>
        /// Gets the status of the network.
        /// </summary>
        public string StatusDescription
        {
            get
            {
                switch (this.status)
                {
                    case NetworkStatus.Uninitialized:
                        return "Uninitialized.";

                    case NetworkStatus.Initializing:
                        return "Initializing.";

                    case NetworkStatus.Initialized:
                        return "Ready for login.";

                    case NetworkStatus.LoggingIn:
                        return "Logging in...";

                    case NetworkStatus.LoggedIn:
                        return "Running.";

                    case NetworkStatus.LoginFailed:
                        return "Login failed.";

                    case NetworkStatus.AddressChanged:
                        return "Address change detected.";

                    default:
                        return "(unknown)";
                }
            }
         }

        /// <summary>
        /// Gets a value indicating whether currently logged in to Badumna network.
        /// </summary>
        public bool IsLoggedIn
        {
            get { return this.status == NetworkStatus.LoggedIn; }
        }

        /// <summary>
        /// Gets a value indicating whether currently logging in to Badumna network.
        /// </summary>
        public bool LoginInProgress
        {
            get
            {
                return NetworkModule.busyStates.Contains(this.status);
            }
        }

        /// <summary>
        /// Start to log in to the Badumna network asynchronously.
        /// </summary>
        /// <param name="identityProvider">The identity provider to log in with.</param>
        public void Login(IIdentityProvider identityProvider)
        {
            if (this.network == null)
            {
                this.Initialize();
            }

            // Create and start a thread to do the login, as it can take some time.
            ThreadPool.QueueUserWorkItem((_) => this.DoLogin(identityProvider));
        }

        /// <summary>
        /// Shuts down the Badumna network.
        /// </summary>
        public void Shutdown()
        {
            // Notify subscribers that we're about to shut down Badumna.
            ShutdownHandler handler = this.ShuttingDown;
            if (handler != null)
            {
                handler.Invoke();
            }

            this.network.Shutdown(false);
            this.network = null;

            // Update the status
            this.status = NetworkStatus.Uninitialized;
        }

        /// <summary>
        /// Called each tick to update the game state.
        /// </summary>
        /// <param name="milliseconds">Milliseconds elapsed since last update.</param>
        public virtual void Update(float milliseconds)
        {
            if (this.loginResultReceived)
            {
                if (this.loginResult)
                {
                    LoginHandler handler = this.LoggedIn;
                    if (handler != null)
                    {
                        handler.Invoke(this.network);
                    }

                    this.status = NetworkStatus.LoggedIn;
                }
                else
                {
                    // You do not have to shutdown Badumna when a log in has failed, that's just how this demo
                    // choses to handle it.
                    this.Shutdown();
                }

                this.loginResultReceived = false;
            }

            // Badumna must be allowed to update each tick.
            if (this.IsLoggedIn)
            {
                this.network.ProcessNetworkState();
            }
        }

        /// <summary>
        /// Initialise Badumna.
        /// </summary>
        private void Initialize()
        {
            this.network = NetworkFacade.Create(this.options);
            this.network.AddressChangedEvent += this.AddressChangedEventHandler;
            this.status = NetworkStatus.Initialized;
        }

        /// <summary>
        /// Log in to Badumna network and create an original avatar.
        /// This method is run in a separate thread as logging in can take some time.
        /// </summary>
        /// <param name="identityProvider">The identity provider to log in with.</param>
        private void DoLogin(IIdentityProvider identityProvider)
        {
            this.status = NetworkStatus.LoggingIn;

            // Log in to the network. This may take a little time.
            this.loginResult = this.network.Login(identityProvider);

            // Set a flag indicating a login result has been received, so any resulting actions can be
            // made on the Update thread in the next call to Update.
            this.loginResultReceived = true;
        }

        /// <summary>
        /// Handle the address changed event, by shutting down the network, 
        /// and reset the demo.
        /// </summary>
        private void AddressChangedEventHandler()
        {
            this.Shutdown();
        }
    }
}
