﻿// -----------------------------------------------------------------------
// <copyright file="DistributedNpcController.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace BadumnaDemo
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Badumna;
    using Badumna.Controllers;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;

    /// <summary>
    /// A distributed controller for running an NPC.
    /// </summary>
    public class DistributedNpcController : DistributedSceneController
    {
        /// <summary>
        /// Interval between checkpoints.
        /// </summary>
        private const double CheckPointIntervalSeconds = 10;

        /// <summary>
        /// Elapsed time since the last checkpoint was saved.
        /// </summary>
        private TimeSpan timeSinceLastCheckPoint = TimeSpan.Zero;

        /// <summary>
        /// A collection of the other entities in the scene.
        /// </summary>
        private List<IAvatar> replicaEntities = new List<IAvatar>();

        /// <summary>
        /// The original avatar for the NPC.
        /// </summary>
        private OriginalAvatar originalNPC;

        /// <summary>
        /// A controller for the NPC.
        /// </summary>
        private AvatarController avatarController;

        /// <summary>
        /// A value indicating whether this controller is currently running an NPC.
        /// </summary>
        private bool awake = false;

        /// <summary>
        /// Initialises a new instance of the DistributedNpcController class.
        /// </summary>
        /// <param name="uniqueName">A unique name for the distributed controller.</param>
        public DistributedNpcController(string uniqueName)
            : base(uniqueName)
        {
            this.ConstructControlledEntity((uint)EntityType.Avatar);
        }

        /// <summary>
        /// Called by Badumna to start running the NPC on this peer.
        /// </summary>
        /// <param name="entityId">The ID of the entity to be taken control of.</param>
        /// <param name="replicaEntity">A replica entity to copy.</param>
        /// <param name="entityType">An integer representing the type of the entity.</param>
        /// <returns>A new original entity.</returns>
        protected override ISpatialEntity TakeControlOfEntity(BadumnaId entityId, ISpatialReplica replicaEntity, uint entityType)
        {
            // This demo only knows about one type of entity.
            if (entityType != (uint)EntityType.Avatar)
            {
                return null;
            }

            // Create an original avatar for the entity.
            // One may have already been created if the NPC was previously running in this controller.
            if (this.originalNPC == null)
            {
                this.originalNPC = new OriginalAvatar(this.NetworkFacade);
                this.avatarController = new AvatarController(this.originalNPC, this.replicaEntities);
            }

            // Copy the entity state from the replica if there is one.
            IAvatar replicaToCopy = replicaEntity as IAvatar;
            if (replicaToCopy != null)
            {
                this.originalNPC.Position = replicaToCopy.Position;
                this.originalNPC.Colour = replicaToCopy.Colour;
            }
            else
            {
                // Initialize the entity state if there was none to copy.
                this.originalNPC.Colour = Colour.RandomColour();
                this.originalNPC.Position = new Vector3(300f, 150f, 0f);
            }

            return this.originalNPC;
        }

        /// <summary>
        /// Called by Badumna to create replicas for entities in the NPC's area of interest.
        /// </summary>
        /// <param name="entityId">The ID of the entity being replicated.</param>
        /// <param name="entityType">An integer representing the type of the entity being replicated.</param>
        /// <returns>A new replica entity.</returns>
        protected override ISpatialEntity InstantiateRemoteEntity(BadumnaId entityId, uint entityType)
        {
            ReplicaAvatar replicaAvatar = new ReplicaAvatar();
            this.replicaEntities.Add(replicaAvatar);
            return replicaAvatar;
        }

        /// <summary>
        /// Called by Badumna to remove a replica for an entity that is no longer in the NPC's area of interest.
        /// </summary>
        /// <param name="replica">The replica to remove.</param>
        protected override void RemoveEntity(IReplicableEntity replica)
        {
            ReplicaAvatar replicaAvatar = replica as ReplicaAvatar;
            this.replicaEntities.Remove(replicaAvatar);
        }

        /// <summary>
        /// Create a checkpoint of the NPC's state for use when the NPC needs to migrate to another peer.
        /// </summary>
        /// <param name="writer">A writer to write the checkpoint.</param>
        protected override void Checkpoint(BinaryWriter writer)
        {
            if (this.originalNPC != null)
            {
                // Write a value indicating that there is checkpoint data.
                writer.Write(true);

                // Write checkpoint data.
                writer.Write(this.originalNPC.Position.X);
                writer.Write(this.originalNPC.Position.Y);
                writer.Write(this.originalNPC.Position.Z);
                writer.Write(this.originalNPC.Colour.R);
                writer.Write(this.originalNPC.Colour.G);
                writer.Write(this.originalNPC.Colour.B);
            }
            else
            {
                // Write a value indicating that there is no checkpoint data.
                writer.Write(false);
            }
        }

        /// <summary>
        /// Called regularly by Badumna to update the NPC.
        /// </summary>
        /// <param name="duration">Elapsed time since the last update.</param>
        protected override void Process(TimeSpan duration)
        {
            // If the NPC is not running in this controller then no action is required.
            if (!this.awake)
            {
                return;
            }

            // Create checkpoints periodically.
            this.timeSinceLastCheckPoint += duration;
            if (this.timeSinceLastCheckPoint.TotalSeconds > CheckPointIntervalSeconds)
            {
                this.timeSinceLastCheckPoint -= TimeSpan.FromSeconds(CheckPointIntervalSeconds);
                this.Replicate();
            }

            // Update the avatar controller.
            this.avatarController.Update(duration);
        }

        /// <summary>
        /// Required to handle messages to the NPC.
        /// </summary>
        /// <param name="message">A message received.</param>
        /// <param name="source">The source of the message.</param>
        protected override void ReceiveMessage(Badumna.Core.MessageStream message, BadumnaId source)
        {
            // This demo does not include messages.
        }

        /// <summary>
        /// Called by Badumna to recover NPC state from a checkpoint when the NPC has migrated to this peer.
        /// </summary>
        /// <param name="reader">A reader to read the checkpoint from.</param>
        protected override void Recover(BinaryReader reader)
        {
            bool containsData = reader.ReadBoolean();

            if (containsData)
            {
                Vector3 position = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                Colour colour = new Colour(reader.ReadByte(), reader.ReadByte(), reader.ReadByte());

                if (this.originalNPC != null)
                {
                    this.originalNPC.Position = position;
                    this.originalNPC.Colour = colour;
                }
            }
        }

        /// <summary>
        /// Called by Badumna to restore a default NPC state when no checkpoint is available.
        /// </summary>
        protected override void Recover()
        {
            // this method is called when no checkpoint data can be found. 
            if (this.originalNPC != null)
            {
                // please be aware, the Vector3(0, 0, 0) position may not be always valid in some games.
                this.originalNPC.Position = new Vector3(0, 0, 0);
            }
        }

        /// <summary>
        /// Called by Badumna when an NPC begins running in this controller.
        /// </summary>
        protected override void Wake()
        {
            this.awake = true;
        }

        /// <summary>
        /// Called by Badumna when an NPC is non longer running in this controller.
        /// </summary>
        protected override void Sleep()
        {
            this.awake = false;
        }
    }
}
