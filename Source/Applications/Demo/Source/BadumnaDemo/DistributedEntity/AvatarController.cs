﻿// -----------------------------------------------------------------------
// <copyright file="AvatarController.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace BadumnaDemo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Badumna.DataTypes;

    /// <summary>
    /// Controls an NPC running on the local machine, making it follow its nearest neighbour.
    /// </summary>
    public class AvatarController
    {
        /// <summary>
        /// The average interval to wait between checks for a new target.
        /// </summary>
        private const double MeanTargetSeekingIntervalSeconds = 2;

        /// <summary>
        /// The size of the range of intervals that the target seeking interval should be picked from.
        /// </summary>
        private const double TargetSeekingIntervalRangeSeconds = 2;

        /// <summary>
        /// The closest that the NPC should approach to its target.
        /// </summary>
        private const float MinimumFollowingSeparation = 10f;

        /// <summary>
        /// The interval to wait between checks for a new target.
        /// </summary>
        private readonly TimeSpan targetSeekingInterval;

        /// <summary>
        /// The original avatar for the NPC.
        /// </summary>
        private readonly IOriginalAvatar originalAvatar;

        /// <summary>
        /// A collection of replica avatars 'visible' in the scene.
        /// </summary>
        private readonly IEnumerable<IAvatar> replicaAvatars;

        /// <summary>
        /// The avatar the NPC is currently following.
        /// </summary>
        private IAvatar target;

        /// <summary>
        /// The interval since a new target was last sought.
        /// </summary>
        private TimeSpan timeSinceTargetSought = TimeSpan.Zero;

        /// <summary>
        /// Initialises a new instance of the AvatarController class.
        /// </summary>
        /// <param name="originalAvatar">The original avatar being controlled.</param>
        /// <param name="replicaAvatars">A collection of replica avatars in the scene.</param>
        public AvatarController(IOriginalAvatar originalAvatar, IEnumerable<IAvatar> replicaAvatars)
        {
            // set the random find target interval to make sure multiple NPCs would have the chance to make 
            // decisions at different time otherwise they will all overlap with each other on the screen.
            double targetSeekingIntervalSeconds = MeanTargetSeekingIntervalSeconds -
                (TargetSeekingIntervalRangeSeconds / 2) +
                (new Random().NextDouble() * TargetSeekingIntervalRangeSeconds);
            this.targetSeekingInterval = TimeSpan.FromSeconds(targetSeekingIntervalSeconds);

            this.originalAvatar = originalAvatar;
            this.replicaAvatars = replicaAvatars;
        }

        /// <summary>
        /// The controller makes the NPC follow its nearest neighbour.
        /// </summary>
        /// <param name="interval">The time interval for this update.</param>
        public void Update(TimeSpan interval)
        {
            this.timeSinceTargetSought += interval;
            if (this.timeSinceTargetSought > this.targetSeekingInterval)
            {
                this.timeSinceTargetSought -= this.targetSeekingInterval;
                this.FindNearestTarget();
            }

            if (this.target != null)
            {
                Vector3 displacement = this.target.Position - this.originalAvatar.Position;
                float separation = displacement.Magnitude;
                float maxMovement = (float)interval.TotalSeconds * OriginalAvatar.MoveSpeed;

                // If it's possible to move too close to the target this step,
                if (separation - maxMovement < MinimumFollowingSeparation)
                {
                    // move as close to the target as permitted,
                    this.originalAvatar.Position = this.target.Position - (displacement.Normalize() * MinimumFollowingSeparation);
                }
                else
                {
                    // otherwise, move as close to the target as possible.
                    this.originalAvatar.Position += displacement.Normalize() * maxMovement;
                }
            }
        }

        /// <summary>
        /// Find the closest other avatar in the scene.
        /// </summary>
        private void FindNearestTarget()
        {
            double smallestColourDifference = double.MaxValue;
            foreach (var avatar in this.replicaAvatars)
            {
                double colourDifference = this.ColourDifference(this.originalAvatar.Colour, avatar.Colour);
                if (colourDifference <= smallestColourDifference)
                {
                    this.target = avatar;
                    smallestColourDifference = colourDifference;
                }
            }
        }

        /// <summary>
        /// Calculate a measure of the difference of two colours.
        /// </summary>
        /// <param name="a">The first colour to compare.</param>
        /// <param name="b">The second colour to compare.</param>
        /// <returns>Zero if the colours are identical, or an increasing number for increasing differences in the colour's component values.</returns>
        private double ColourDifference(Colour a, Colour b)
        {
            return Math.Pow(a.R - b.R, 2) +
                Math.Pow(a.G - b.G, 2) +
                Math.Pow(a.B - b.B, 2);
        }
    }
}
