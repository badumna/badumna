﻿// -----------------------------------------------------------------------
// <copyright file="DistributedEntityModule.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace BadumnaDemo
{
    using Badumna;

    /// <summary>
    /// Demonstrates how to start a distributed controller.
    /// </summary>
    public class DistributedEntityModule
    {
        /// <summary>
        /// Initialises a new instance of the DistributedEntityModule class.
        /// </summary>
        /// <param name="networkModule">The network module for subscribing to login events.</param>
        public DistributedEntityModule(NetworkModule networkModule)
        {
            networkModule.LoggedIn += this.OnLoggedIn;
        }

        /// <summary>
        /// Set up private chat once logged in.
        /// </summary>
        /// <param name="network">The Badumna network facade.</param>
        private void OnLoggedIn(INetworkFacade network)
        {
            // start a controller controlled NPC, let the NPC join the default scene. 
            // there should be no more than 16 such NPCs across the network. 
            network.StartController<DistributedNpcController>(ReplicationModule.DefaultSceneName, "npc_avatar", 16);
        }
    }
}
