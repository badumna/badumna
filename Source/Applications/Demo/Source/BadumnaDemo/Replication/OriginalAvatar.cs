﻿//-----------------------------------------------------------------------
// <copyright file="OriginalAvatar.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaDemo
{
    using System.IO;
    using Badumna;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;

    /// <summary>
    /// Class for representing the local player's avatar.
    /// </summary>
    public class OriginalAvatar : Avatar, IOriginalAvatar
    {
        /// <summary>
        /// Speed the avatar moves at in units per second.
        /// </summary>
        public const float MoveSpeed = 50f;

        /// <summary>
        /// The Badumna network facade.
        /// </summary>
        private INetworkFacade network;

        /// <summary>
        /// The location the avatar is moving towards.
        /// </summary>
        private Vector3 target;

        /// <summary>
        /// Initialises a new instance of the OriginalAvatar class.
        /// </summary>
        /// <param name="network">The Badumna network facade.</param>
        public OriginalAvatar(INetworkFacade network)
        {
            this.network = network;
        }

        /// <summary>
        /// Update the avatar by moving it towards its target if required.
        /// </summary>
        /// <param name="deltaSeconds">The time interval for the update.</param>
        public void Update(float deltaSeconds)
        {
            if (this.Position != this.target)
            {
                float maxMovement = OriginalAvatar.MoveSpeed * deltaSeconds;
                Vector3 displacement = this.target - this.Position;
                if (displacement.Magnitude <= maxMovement)
                {
                    this.Position = this.target;
                }
                else
                {
                    this.Position += displacement.Normalize() * maxMovement;
                }
            }
        }

        /// <summary>
        /// Move the avatar directly to a new target location.
        /// </summary>
        /// <param name="x">The x-coordinate of the new target location.</param>
        /// <param name="y">The y-coordinate of the new target location.</param>
        public void JumpToTarget(float x, float y)
        {
            this.SetTarget(x, y);
            this.Position = this.target;
        }

        /// <summary>
        /// Set a new target for the avatar to move towards.
        /// </summary>
        /// <param name="x">The x-coordinate of the new target location.</param>
        /// <param name="y">The y-coordinate of the new target location.</param>
        public void SetTarget(float x, float y)
        {
            this.target.X = x;
            this.target.Y = y;
        }

        /// <summary>
        /// Serialize some or all of the Avatar's state to a stream.
        /// </summary>
        /// <param name="requiredParts">The parts of the Avatar's state to serialize.</param>
        /// <param name="stream">The stream to serialize to.</param>
        public void Serialize(BooleanArray requiredParts, Stream stream)
        {
            BinaryWriter writer = new BinaryWriter(stream);

            if (requiredParts[(int)Avatar.StateSegment.Color])
            {
                writer.Write(this.Colour.R);
                writer.Write(this.Colour.G);
                writer.Write(this.Colour.B);
            }

            if (requiredParts[(int)Avatar.StateSegment.Orientation])
            {
                writer.Write(this.Orientation);
            }
        }

        /// <summary>
        /// Notify Badumna that the avatar's position has changed.
        /// </summary>
        protected override void OnPositionUpdate()
        {
            this.network.FlagForUpdate(this, (int)SpatialEntityStateSegment.Position);
        }

        /// <summary>
        /// Notify Badumna that the avatar's orientation has changed.
        /// </summary>
        protected override void OnOrientationUpdate()
        {
            this.network.FlagForUpdate(this, (int)Avatar.StateSegment.Orientation);
        }

        /// <summary>
        /// Notify Badumna that the avatar's colour has changed.
        /// </summary>
        protected override void OnColourUpdate()
        {
            this.network.FlagForUpdate(this, (int)Avatar.StateSegment.Color);
        }
    }
}
