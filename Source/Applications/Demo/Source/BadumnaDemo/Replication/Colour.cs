﻿//-----------------------------------------------------------------------
// <copyright file="Colour.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaDemo
{
    using System;

    /// <summary>
    /// Represents a colour using RGB value.
    /// </summary>
    public struct Colour
    {
        /// <summary>
        /// The RGB red channel of the current Colour structure.
        /// </summary>
        private readonly byte r;

        /// <summary>
        /// The RGB green channel of the current Colour structure.
        /// </summary>
        private readonly byte g;

        /// <summary>
        /// The RGB blue channel of the current Colour structure.
        /// </summary>
        private readonly byte b;

        /// <summary>
        /// Initialises a new instance of the Colour structure.
        /// </summary>
        /// <param name="r">The red component.</param>
        /// <param name="g">The green component.</param>
        /// <param name="b">The blue component.</param>
        public Colour(int r, int g, int b)
        {
            this.r = (byte)r;
            this.g = (byte)g;
            this.b = (byte)b;
        }

        /// <summary>
        /// Gets the red component.
        /// </summary>
        public byte R
        {
            get { return this.r; }
        }

        /// <summary>
        /// Gets the green component.
        /// </summary>
        public byte G
        {
            get { return this.g; }
        }

        /// <summary>
        /// Gets the blue component.
        /// </summary>
        public byte B
        {
            get { return this.b; }
        }

        /// <summary>
        /// Create a random colour.
        /// </summary>
        /// <returns>A new random colour.</returns>
        public static Colour RandomColour()
        {            
            byte[] bytes = new byte[3];
            new Random().NextBytes(bytes);
            return new Colour(bytes[0], bytes[1], bytes[2]);
        }
    }
}
