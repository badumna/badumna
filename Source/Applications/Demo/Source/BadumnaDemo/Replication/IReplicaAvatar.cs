﻿//-----------------------------------------------------------------------
// <copyright file="IReplicaAvatar.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaDemo
{
    using System.IO;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;

    /// <summary>
    /// Interface for replica avatars.
    /// </summary>
    public interface IReplicaAvatar : IAvatar, ISpatialReplica
    {
    }
}
