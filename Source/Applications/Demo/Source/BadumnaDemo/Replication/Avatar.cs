﻿//-----------------------------------------------------------------------
// <copyright file="Avatar.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaDemo
{
    using System;
    using System.IO;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;

    /// <summary>
    /// Base class for simulation avatars.
    /// </summary>
    public class Avatar : IAvatar
    {
        /// <summary>
        /// The position of the avatar.
        /// </summary>
        private Vector3 position = new Vector3(0f, 0f, 0f);
        
        /// <summary>
        /// The colour of the avatar.
        /// </summary>
        private Colour colour = new Colour(255, 0, 0);
        
        /// <summary>
        /// The orientation of the avatar.
        /// </summary>
        private float orientation = 0f;

        /// <summary>
        /// Initialises a new instance of the Avatar class.
        /// </summary>
        public Avatar()
        {
            // Initialize implicit properties 
            this.Radius = 15f;
            this.AreaOfInterestRadius = 150f;
        }

        /// <summary>
        /// Enumeration identifying parts of the entity state.
        /// </summary>
        protected enum StateSegment : int
        {
            /// <summary>
            /// Orientation of the avatar.
            /// </summary>
            Orientation = SpatialEntityStateSegment.FirstAvailableSegment,

            /// <summary>
            /// The avatar's colour.
            /// </summary>
            Color
        }

        /// <summary>
        /// Gets or sets a unique ID for the avatar, assigned by Badumna when the entity is registered with a scene.
        /// </summary>
        public BadumnaId Guid { get; set; }

        /// <summary>
        /// Gets or sets the radius of the avatar, used to determine whether it intersects other entities' areas of interest.
        /// </summary>
        public float Radius { get; set; }

        /// <summary>
        /// Gets or sets the radius of the avatar's area of interest.
        /// </summary>
        /// <remarks>A spatial original only receives updates for other entities within its own area of interest.</remarks>
        public float AreaOfInterestRadius { get; set; }

        /// <summary>
        /// Gets or sets the position of the avatar.
        /// </summary>
        public Vector3 Position
        {
            get
            {
                return this.position;
            }

            set
            {
                this.position = value;
                this.OnPositionUpdate();
            }
        }

        /// <summary>
        /// Gets or sets the colour of the avatar.
        /// </summary>
        public Colour Colour
        {
            get
            {
                return this.colour;
            }

            set
            {
                this.colour = value;
                this.OnColourUpdate();
            }
        }

        /// <summary>
        /// Gets or sets the orientation of the avatar.
        /// </summary>
        public float Orientation
        {
            get
            {
                return this.orientation;
            }

            set
            {
                // Limit the value to modulo 360 degrees.
                this.orientation  = value % 360f;
                if (this.orientation < 0)
                {
                    this.orientation += 360f;
                }

                this.OnOrientationUpdate();
            }
        }

        /// <summary>
        /// Used to receive custom events. Part of ISpatialEntity interface.
        /// </summary>
        /// <remarks>Not used in this demo.</remarks>
        /// <param name="stream">A stream to read event data from.</param>
        public void HandleEvent(Stream stream)
        {
            throw new NotImplementedException("Custom events are not used in this demo.");
        }

        /// <summary>
        /// Override this method to trigger action on position updates.
        /// </summary>
        protected virtual void OnPositionUpdate()
        {
            // Do nothing.
        }

        /// <summary>
        /// Override this method to trigger action on colour updates.
        /// </summary>
        protected virtual void OnColourUpdate()
        {
            // Do nothing.
        }

        /// <summary>
        /// Override this method to trigger action on orientation updates.
        /// </summary>
        protected virtual void OnOrientationUpdate()
        {
            // Do nothing.
        }
    }
}
