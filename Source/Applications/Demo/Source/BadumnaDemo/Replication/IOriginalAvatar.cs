﻿//-----------------------------------------------------------------------
// <copyright file="IOriginalAvatar.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaDemo
{
    using System.IO;
    using Badumna;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;

    /// <summary>
    /// Interface for original avatars.
    /// </summary>
    public interface IOriginalAvatar : IAvatar, ISpatialOriginal
    {
        /// <summary>
        /// Update the avatar by moving it towards its target if required.
        /// </summary>
        /// <param name="deltaSeconds">The time interval for the update.</param>
        void Update(float deltaSeconds);

        /// <summary>
        /// Move the avatar directly to a new target location.
        /// </summary>
        /// <param name="x">The x-coordinate of the new target location.</param>
        /// <param name="y">The y-coordinate of the new target location.</param>
        void JumpToTarget(float x, float y);

        /// <summary>
        /// Set a new target for the avatar to move towards.
        /// </summary>
        /// <param name="x">The x-coordinate of the new target location.</param>
        /// <param name="y">The y-coordinate of the new target location.</param>
        void SetTarget(float x, float y);
    }
}
