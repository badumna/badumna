﻿//-----------------------------------------------------------------------
// <copyright file="ReplicationModule.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaDemo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Threading;
    using Badumna;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;

    /// <summary>
    /// Delegate for handling original entity online events.
    /// </summary>
    /// <param name="network">The Badumna network facade.</param>
    /// <param name="originalEntityId">The ID of the original entity.</param>
    public delegate void OriginalEntityOnlineHandler(INetworkFacade network, BadumnaId originalEntityId);

    /// <summary>
    /// Delegate for handling original entity offline events.
    /// </summary>
    public delegate void OriginalEntityOfflineHandler();

    /// <summary>
    /// An enumeration defining the types of entities in the simulation.
    /// </summary>
    public enum EntityType : uint
    {
        /// <summary>
        /// Not used.
        /// </summary>
        None,

        /// <summary>
        /// This demo only has one type of entity.
        /// </summary>
        Avatar
    }

    /// <summary>
    /// Demonstrates how to use Badumna to create a minimal game world.
    /// </summary>
    public class ReplicationModule
    {
        /// <summary>
        /// A name for the scene to be used.
        /// </summary>
        public const string DefaultSceneName = "default-scene";

        /// <summary>
        /// Method for creating new original avatars.
        /// </summary>
        private readonly Func<INetworkFacade, IOriginalAvatar> originalAvatarFactory;

        /// <summary>
        /// Method for creating new replica avatars.
        /// </summary>
        private readonly Func<IReplicaAvatar> replicaAvatarFactory;

        /// <summary>
        /// The Badumna network facade.
        /// </summary>
        private INetworkFacade network;

        /// <summary>
        /// Badumna Network scene.
        /// </summary>
        private NetworkScene scene;

        /// <summary>
        /// The original avatar for the local player's character (implements ISpatialOriginal).
        /// </summary>
        private IOriginalAvatar originalAvatar;

        /// <summary>
        /// Avatars currently being replicated.
        /// </summary>
        private List<IReplicaAvatar> replicaAvatars = new List<IReplicaAvatar>();

        /// <summary>
        /// Initialises a new instance of the ReplicationModule class.
        /// </summary>
        /// <param name="networkModule">The network module.</param>
        public ReplicationModule(NetworkModule networkModule)
            : this(networkModule, (n) => new OriginalAvatar(n), () => new ReplicaAvatar())
        {
        }

        /// <summary>
        /// Initialises a new instance of the ReplicationModule class.
        /// </summary>
        /// <remarks>Supplying factory methods for creating avatars allows different demos to use different avatars.</remarks>
        /// <param name="networkModule">The network module.</param>
        /// <param name="originalAvatarFactory">A method for creating original avatars.</param>
        /// <param name="replicaAvatarFactory">A method for creating replica avatars.</param>
        public ReplicationModule(
            NetworkModule networkModule,
            Func<INetworkFacade, IOriginalAvatar> originalAvatarFactory,
            Func<IReplicaAvatar> replicaAvatarFactory)
        {
            this.originalAvatarFactory = originalAvatarFactory;
            this.replicaAvatarFactory = replicaAvatarFactory;

            networkModule.LoggedIn += this.OnLogin;
            networkModule.ShuttingDown += this.OnShutdown;
        }

        /// <summary>
        /// Event raised when the original entity is online.
        /// </summary>
        public event OriginalEntityOnlineHandler OriginalEntityOnline;

        /// <summary>
        /// Event raised when the original entity is online.
        /// </summary>
        public event OriginalEntityOfflineHandler OriginalEntityGoingOffline;

        /// <summary>
        /// Gets a value indicating whether replication is online.
        /// </summary>
        public bool IsOnline
        {
            get { return this.originalAvatar != null; }
        }

        /// <summary>
        /// Gets the avatar representing the local player.
        /// </summary>
        public IOriginalAvatar OriginalAvatar
        {
            get { return this.originalAvatar; }
        }

        /// <summary>
        /// Gets the replica avatars currently 'visible' in the scene.
        /// </summary>
        public IEnumerable<IAvatar> ReplicaAvatars
        {
            get
            {
                foreach (var replicaAvatar in this.replicaAvatars)
                {
                    yield return replicaAvatar;
                }
            }
        }

        /// <summary>
        /// Set up the original entity and replication, once logged in.
        /// </summary>
        /// <param name="network">The Badumna network facade.</param>
        public void OnLogin(INetworkFacade network)
        {
            // Store the badumna network facade.
            this.network = network;

            // Register entity details.
            // Avatar's area of interest is 150, its max velocity is 60 on both X and Y axis and 0 on the Z axis. 
            this.network.RegisterEntityDetails(150.0f, new Vector3(60, 60, 0).Magnitude);

            // Create the original.
            this.originalAvatar = this.originalAvatarFactory(this.network);
            if (this.originalAvatar == null)
            {
                throw new InvalidOperationException("Could not create original avatar");
            }

            // Join the Badumna scene.
            this.JoinScene(ReplicationModule.DefaultSceneName);

            // Initialize the avatar's position and color
            this.originalAvatar.JumpToTarget(300, 100);
            this.originalAvatar.Colour = new Colour(0, 255, 0);

            // Notify subscribers that the original entity is online.
            OriginalEntityOnlineHandler handler = this.OriginalEntityOnline;
            if (handler != null)
            {
                handler.Invoke(this.network, this.originalAvatar.Guid);
            }
        }

        /// <summary>
        /// Disable replication on shutdown.
        /// </summary>
        public void OnShutdown()
        {
            // Notify subscribers that we're about to shutdown.
            OriginalEntityOfflineHandler handler = this.OriginalEntityGoingOffline;
            if (handler != null)
            {
                handler.Invoke();
            }

            if (this.originalAvatar != null)
            {
                // Unregister the original avatar from the scene
                this.scene.UnregisterEntity(this.originalAvatar);
                this.originalAvatar = null;
            }

            if (this.scene != null)
            {
                // Leave the scene.
                this.scene.Leave();
                this.scene = null;
            }
        }

        /// <summary>
        /// Called each tick to update the game state.
        /// </summary>
        /// <param name="milliseconds">Milliseconds elapsed since last update.</param>
        public virtual void Update(float milliseconds)
        {
            if (this.originalAvatar != null)
            {
                this.originalAvatar.Update(milliseconds / 1000f);
            }
        }

        /// <summary>
        /// Join a given scene.
        /// </summary>
        /// <param name="sceneName">The name of the scene to join.</param>
        public void JoinScene(string sceneName)
        {
            if (this.scene != null)
            {
                if (this.scene.Name == sceneName)
                {
                    return;
                }
                else
                {
                    this.LeaveScene();
                }
            }

            Console.WriteLine("Joining scene: " + sceneName);

            // Join the network scene
            this.scene = this.network.JoinScene(sceneName, this.CreateSpatialReplica, this.RemoveSpatialReplica);

            // Register the original avatar with the scene
            this.scene.RegisterEntity(this.originalAvatar, (uint)EntityType.Avatar);
        }

        /// <summary>
        /// Create a spatial replica
        /// </summary>
        /// <remarks>
        /// This method (which is passed as an argument in the call to NetworkFacade.JoinScene
        /// is called by Badumna to create replicas when necessary.
        /// </remarks>
        /// <param name="scene">The scene the replica will belong to.</param>
        /// <param name="entityId">An ID for the entity being replicated.</param>
        /// <param name="entityType">An integer indicating the type of the entity being replicated.</param>
        /// <returns>A new spatial replica.</returns>
        private ISpatialReplica CreateSpatialReplica(NetworkScene scene, BadumnaId entityId, uint entityType)
        {
            if (entityType == (uint)EntityType.Avatar)
            {
                IReplicaAvatar replicaAvatar = this.replicaAvatarFactory();
                this.replicaAvatars.Add(replicaAvatar);
                return replicaAvatar;
            }

            return null;
        }

        /// <summary>
        /// Remove a spatial replica.
        /// </summary>
        /// <remarks>
        /// This method (which is passed as an argument in the call to NetworkFacade.JoinScene
        /// is called by Badumna to remove replicas when necessary.
        /// </remarks>
        /// <param name="scene">The scene the replica is being removed from.</param>
        /// <param name="replica">The replica to remove.</param>
        private void RemoveSpatialReplica(NetworkScene scene, IReplicableEntity replica)
        {
            IReplicaAvatar replicaAvatar = replica as IReplicaAvatar;
            if (replicaAvatar != null)
            {
                if (!this.replicaAvatars.Remove(replicaAvatar))
                {
                    throw new InvalidOperationException("Tried to remove missing replica.");
                }
            }
        }

        /// <summary>
        /// Leave the currently joined scene.
        /// </summary>
        private void LeaveScene()
        {
            if (this.scene != null)
            {
                Console.WriteLine("Leaving scene: " + this.scene.Name);

                this.scene.UnregisterEntity(this.OriginalAvatar);
                this.scene.Leave();
            }
        }
    }
}
