﻿//-----------------------------------------------------------------------
// <copyright file="IAvatar.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaDemo
{
    using System;
    using System.IO;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;

    /// <summary>
    /// Base interface for Avatar classes.
    /// </summary>
    public interface IAvatar : ISpatialEntity
    {
        /// <summary>
        /// Gets or sets the avatar's colour.
        /// </summary>
        Colour Colour { get; set; }
    }
}
