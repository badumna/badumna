﻿// -----------------------------------------------------------------------
// <copyright file="SceneModule.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace BadumnaDemo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Responsible for controlling which scene the original entity should be in.
    /// </summary>
    public class SceneModule
    {
        /// <summary>
        /// The name of the default scene.
        /// </summary>
        private readonly string defaultScene;

        /// <summary>
        /// The Badumna world.
        /// </summary>
        private readonly ReplicationModule replicationModule;

        /// <summary>
        /// The scenes being managed.
        /// </summary>
        private readonly List<Scene> scenes;

        /// <summary>
        /// Initialises a new instance of the SceneModule class.
        /// </summary>
        /// <param name="replicationModule">The Badumna world.</param>
        /// <param name="defaultScene">The name for the default scene.</param>
        /// <param name="scenes">Scenes to manage.</param>
        public SceneModule(ReplicationModule replicationModule, string defaultScene, IEnumerable<Scene> scenes)
        {
            this.replicationModule = replicationModule;
            this.defaultScene = defaultScene;
            this.scenes = new List<Scene>(scenes);
        }

        /// <summary>
        /// Gets the scenes.
        /// </summary>
        public IEnumerable<Scene> Scenes
        {
            get { return this.scenes; }
        }

        /// <summary>
        /// Called each tick to update the game state.
        /// </summary>
        /// <param name="milliseconds">Milliseconds elapsed since last update.</param>
        public void Update(double milliseconds)
        {
            if (!this.replicationModule.IsOnline)
            {
                return;
            }

            foreach (var scene in this.scenes)
            {
                if (scene.Contains(
                    this.replicationModule.OriginalAvatar.Position.X,
                    this.replicationModule.OriginalAvatar.Position.Y))
                {
                    this.replicationModule.JoinScene(scene.Name);
                    return;
                }
            }

            this.replicationModule.JoinScene(this.defaultScene);
        }

        /// <summary>
        /// The name, location and size of a scene.
        /// </summary>
        public struct Scene
        {
            /// <summary>
            /// Initialises a new instance of the Scene struct.
            /// </summary>
            /// <param name="name">The name of the scene.</param>
            /// <param name="originX">The x-coordinate of the scene's location.</param>
            /// <param name="originY">The y-coordinate of the scene's location.</param>
            /// <param name="width">The width of the scene.</param>
            /// <param name="height">The height of the scene.</param>
            public Scene(string name, int originX, int originY, int width, int height)
                : this()
            {
                this.Name = name;
                this.OriginX = originX;
                this.OriginY = originY;
                this.Width = width;
                this.Height = height;
            }

            /// <summary>
            /// Gets the name of the scene.
            /// </summary>
            public string Name { get; private set; }

            /// <summary>
            /// Gets the x-coordinate of the location of the scene.
            /// </summary>
            public int OriginX { get; private set; }

            /// <summary>
            /// Gets the y-coordinate of the location of the scene.
            /// </summary>
            public int OriginY { get; private set; }

            /// <summary>
            /// Gets the width of the scene.
            /// </summary>
            public int Width { get; private set; }

            /// <summary>
            /// Gets the height of the scene.
            /// </summary>
            public int Height { get; private set; }

            /// <summary>
            /// Tests whether a given location lies within this scene.
            /// </summary>
            /// <param name="x">The x-coordinate of the location to tests.</param>
            /// <param name="y">The y-coordinate of the location to tests.</param>
            /// <returns><c>true</c> if the location lies within the scene, otherwise <c>false</c>.</returns>
            public bool Contains(float x, float y)
            {
                return x >= this.OriginX &&
                    x <= this.OriginX + this.Width &&
                    y >= this.OriginY &&
                    y <= this.OriginY + this.Height;
            }
        }
    }
}
