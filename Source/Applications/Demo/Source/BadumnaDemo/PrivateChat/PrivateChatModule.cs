﻿//-----------------------------------------------------------------------
// <copyright file="PrivateChatModule.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaDemo
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Badumna;
    using Badumna.Chat;
    using Badumna.DataTypes;

    /// <summary>
    /// Demonstrates how to use Badumna to create a minimal game world.
    /// </summary>
    public class PrivateChatModule
    {
        /// <summary>
        /// Service for providing the list of friends for private chat.
        /// </summary>
        private readonly IFriendListProvider friendListProvider;

        /// <summary>
        /// The identity module which provides access to the chosen identity.
        /// </summary>
        private IIdentityModule identityModule;

        /// <summary>
        /// A value indicating whether private chat is enabled.
        /// </summary>
        private bool isEnabled = false;

        /// <summary>
        /// List of friends that can be chatted with.
        /// </summary>
        private ObservableCollection<Friend> friends = new ObservableCollection<Friend>();

        /// <summary>
        /// The network facade passed in the login notification.
        /// </summary>
        private INetworkFacade network;

        /// <summary>
        /// Initialises a new instance of the PrivateChatModule class.
        /// </summary>
        /// <param name="networkModule">The network module which will send notifications on log-in/shutdown.</param>
        /// <param name="identityModule">The identity module which provides access to chosen identity.</param>
        /// <param name="friendListProvider">Service that can supply a list of the user's friends on request.</param>
        public PrivateChatModule(
            NetworkModule networkModule,
            IIdentityModule identityModule,
            IFriendListProvider friendListProvider)
        {
            this.identityModule = identityModule;
            this.friendListProvider = friendListProvider;

            networkModule.LoggedIn += this.OnLoggedIn;
            networkModule.ShuttingDown += this.OnShuttingDown;
        }

        /// <summary>
        /// Event raised when events are to be logged.
        /// </summary>
        public event MessageHandler PrivateChatEventOccurred;

        /// <summary>
        /// Gets a value indicating whether private chat is enabled.
        /// </summary>
        public bool IsEnabled
        {
            get { return this.isEnabled; }
        }

        /// <summary>
        /// Gets the friends in the current user's friends list.
        /// </summary>
        public ObservableCollection<Friend> Friends
        {
            get { return this.friends; }
        }

        /// <summary>
        /// Update the local user's presence status.
        /// </summary>
        /// <param name="presenceStatus">The new presence status.</param>
        public void SetPresence(ChatStatus presenceStatus)
        {
            this.network.ChatSession.ChangePresence(presenceStatus);
        }

        /// <summary>
        /// Send a private chat message to another user.
        /// </summary>
        /// <param name="recipient">The user to send the message to.</param>
        /// <param name="message">The message to send.</param>
        public void SendPrivateMessage(string recipient, string message)
        {
            Friend friend = this.friends.FirstOrDefault(f => f.Name == recipient);
            if (friend != null && friend.Channel != null)
            {
                friend.Channel.SendMessage(message);
                this.RaiseEvent("Told " + friend.Name + ": \"" + message + "\"");
            }
        }

        /// <summary>
        /// Set up private chat once logged in.
        /// </summary>
        /// <param name="network">The Badumna network facade.</param>
        private void OnLoggedIn(INetworkFacade network)
        {
            // Store the Baduna network facade.
            this.network = network;

            this.RaiseEvent("Requesting friend list");

            // Request a list of friends to chat with.            
            this.friendListProvider.RequestFriendsList(network, this.identityModule.CharacterName, this.HandleFriendList);

            this.isEnabled = true;
        }

        /// <summary>
        /// Clear up on shutdown.
        /// </summary>
        private void OnShuttingDown()
        {
            this.isEnabled = false;
            this.friends.Clear();
        }

        /// <summary>
        /// Handler for receiving a list of friends from the friend-list provider.
        /// </summary>
        /// <param name="success">A value indicating whether the friend list request was successful.</param>
        /// <param name="error">A description of the error if the friend list was unsuccessful.</param>
        /// <param name="friends">A new friend list if the friend list request was successful.</param>
        private void HandleFriendList(bool success, string error, IEnumerable<string> friends)
        {
            if (success)
            {
                this.friends.Clear();
                foreach (string name in friends)
                {
                    // Keep track of all friends.
                    this.friends.Add(new Friend(name));
                }

                this.RaiseEvent(string.Format("Loaded friend list ({0} friends)", this.friends.Count));

                // Open private channels so other users can invite us to chat.
                this.network.ChatSession.OpenPrivateChannels(this.HandleChannelInvitation);

                // Update the local user's presence status so others can see we are online.
                this.network.ChatSession.ChangePresence(ChatStatus.Online);

                foreach (Friend friend in this.friends)
                {
                    // Invite each friend to private chat.
                    this.network.ChatSession.InviteUserToPrivateChannel(friend.Name);
                }
            }
            else
            {
                this.RaiseEvent("Friend list lookup failed: " + error);
            }
        }

        /// <summary>
        /// Handle an invitation to private chat from another user.
        /// </summary>
        /// <param name="channelId">The ID for the chat channel to that user.</param>
        /// <param name="username">The name of the user inviting us to chat.</param>
        private void HandleChannelInvitation(ChatChannelId channelId, string username)
        {
            Friend friend = this.friends.FirstOrDefault(f => f.Name == username);
            if (friend != null)
            {
                if (friend.Channel != null)
                {
                    if (friend.Channel.Id == channelId)
                    {
                        // We already have this channel
                        return;
                    }
                    else
                    {
                        // This friend's channel has changed - unsubscribe from the old one.
                        friend.Channel.Unsubscribe();
                    }
                }

                // Accept the invitation.
                friend.Channel = this.network.ChatSession.AcceptInvitation(channelId, this.HandlePrivateMessage, this.HandlePresence);
            }
        }

        /// <summary>
        /// Handler for receiving private chat messages.
        /// </summary>
        /// <param name="channel">The channel the message came on.</param>
        /// <param name="userId">The ID of the user who sent the message.</param>
        /// <param name="message">The message.</param>
        private void HandlePrivateMessage(IChatChannel channel, BadumnaId userId, string message)
        {
            Friend friend = this.friends.FirstOrDefault(f => f.Channel == channel);
            if (friend != null)
            {
                this.RaiseEvent(friend.Name + " said: \"" + message + "\"");
            }
        }

        /// <summary>
        /// Handler for receiving presence status updates from other users.
        /// </summary>
        /// <param name="channel">The channel the update relates to.</param>
        /// <param name="userId">The ID of the user that has changed their presence.</param>
        /// <param name="userName">The name of the user that has changed their presence.</param>
        /// <param name="status">The new presence status.</param>
        private void HandlePresence(IChatChannel channel, BadumnaId userId, string userName, ChatStatus status)
        {
            Friend friend = this.friends.FirstOrDefault(f => f.Name == userName);
            if (friend != null)
            {
                friend.Status = status;
                if (status == ChatStatus.Offline)
                {
                    friend.Channel = null;
                }
            }                 
        }

        /// <summary>
        /// Raise an event to notify that something has happened.
        /// </summary>
        /// <param name="description">A description of the event.</param>
        private void RaiseEvent(string description)
        {
            MessageHandler handler = this.PrivateChatEventOccurred;
            if (handler != null)
            {
                handler.Invoke(description);
            }
        }
    }
}
