﻿//-----------------------------------------------------------------------
// <copyright file="Friend.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaDemo
{
    using Badumna.Chat;

    /// <summary>
    /// The class used to represent a friend. 
    /// </summary>
    public class Friend
    {
        /// <summary>
        /// The unique name of the friend. 
        /// </summary>
        private string name;

        /// <summary>
        /// The presence status of the friend (e.g. online, busy, away and etc.). 
        /// </summary>
        private ChatStatus status;

        /// <summary>
        /// The chat channel with the friend.
        /// </summary>
        private IChatChannel channel;

        /// <summary>
        /// Initialises a new instance of the <see cref="Friend"/> class.
        /// </summary>
        /// <param name="name">The friend's name.</param>
        public Friend(string name)
        {
            this.name = name;
            this.status = ChatStatus.Offline;
            this.channel = null;
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The friend's name.</value>
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>The presence status.</value>
        public ChatStatus Status
        {
            get { return this.status; }
            set { this.status = value; }
        }

        /// <summary>
        /// Gets or sets the chat channel.
        /// </summary>
        /// <value>The chat channel.</value>
        public IChatChannel Channel
        {
            get { return this.channel; }
            set { this.channel = value; }
        }
    }
}