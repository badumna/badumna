﻿// -----------------------------------------------------------------------
// <copyright file="FriendListProvider.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace BadumnaDemo
{
    using System;
    using System.Collections.Generic;
    using Badumna;

    /// <summary>
    /// Provides a list of known friends for private chat.
    /// </summary>
    public class FriendListProvider : IFriendListProvider
    {
        /// <summary>
        /// This provider just stores a hard-coded dictionary of friend lists, to keep the demo simple.
        /// Other friend list providers show how to retrieve a friend list from an arbitration server.
        /// </summary>
        private static Dictionary<string, List<string>> friendsLists = new Dictionary<string, List<string>>()
            {
                { "alice", new List<string> { "bob", "carol" } },
                { "bob", new List<string> { "alice", "carol" } },
                { "carol", new List<string> { "alice", "bob", "dave" } },
                { "dave", new List<string> { "carol" } }
            };

        /// <summary>
        /// Request a friend list from this provider.
        /// </summary>
        /// <param name="network">The Badumna network facade.</param>
        /// <param name="userName">The name of the user whose friend list should be provided.</param>
        /// <param name="friendsListHandler">A callback for returning the friend list.</param>
        public void RequestFriendsList(INetworkFacade network, string userName, Action<bool, string, IEnumerable<string>> friendsListHandler)
        {
            if (friendsLists.ContainsKey(userName))
            {
                friendsListHandler(true, null, friendsLists[userName]);
            }
        }
    }
}
