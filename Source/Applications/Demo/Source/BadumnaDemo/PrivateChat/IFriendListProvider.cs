﻿// -----------------------------------------------------------------------
// <copyright file="IFriendListProvider.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace BadumnaDemo
{
    using System;
    using System.Collections.Generic;
    using Badumna;

    /// <summary>
    /// Friend list providers supply a list of friends for private chat.
    /// </summary>
    public interface IFriendListProvider
    {
        /// <summary>
        /// Request a friend list from the provider.
        /// </summary>
        /// <param name="network">The Badumna network facade.</param>
        /// <param name="userName">THe name of the user whose friend list should be provided.</param>
        /// <param name="friendsListHandler">A callback for returning the friend list.</param>
        void RequestFriendsList(INetworkFacade network, string userName, Action<bool, string, IEnumerable<string>> friendsListHandler);
    }
}
