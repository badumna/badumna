﻿// -----------------------------------------------------------------------
// <copyright file="IIdentityModule.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace BadumnaDemo
{
    using Badumna.Security;

    /// <summary>
    /// Delegate for handling identity selection events.
    /// </summary>
    /// <param name="identity">The identity that has been selected.</param>
    public delegate void IdentitySelectionHandler(IIdentityProvider identity);

    /// <summary>
    /// Identity modules are responsible for creating an identity for logging into a Badumna network.
    /// </summary>
    public interface IIdentityModule
    {
        /////// <summary>
        /////// Event raised when an identity is selected.
        /////// </summary>
        ////event IdentitySelectionHandler IdentitySelected;

        /////// <summary>
        /////// Gets a value indicating whether an identity has been created.
        /////// </summary>
        ////bool CanSelectCharacter { get; }
        
        /////// <summary>
        /////// Gets the identity.
        /////// </summary>
        ////IIdentityProvider Identity { get; }

        /// <summary>
        /// Gets the character name of the chosen identity.
        /// </summary>
        string CharacterName { get; }

        /////// <summary>
        /////// Clears the currently selected identity and re-enables identity selection.
        /////// </summary>
        ////void Clear();
    }
}
