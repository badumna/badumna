﻿// -----------------------------------------------------------------------
// <copyright file="VerifiedIdentityModule.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace BadumnaDemo
{
    using System;
    using System.Collections.ObjectModel;
    using System.Threading;
    using Badumna.Security;
    using Dei;

    /// <summary>
    /// Allows creation of verified identities.
    /// </summary>
    /// <remarks>
    /// To login to a secure Badumna network you need to use a verified Identity that has been authenticated using Dei.
    /// </remarks>
    public class VerifiedIdentityModule : IIdentityModule
    {
        /// <summary>
        /// Network module provides notifications of shutdown.
        /// </summary>
        private readonly NetworkModule networkModule;

        /// <summary>
        /// The IP address or domain name of the Dei Server host.
        /// </summary>
        private readonly string deiServerHost;

        /// <summary>
        /// The port the Dei server is listening on.
        /// </summary>
        private readonly ushort deiServerPort;

        /// <summary>
        /// The character name of the identity.
        /// </summary>
        private string characterName;

        /// <summary>
        /// A message describing the current status of identity selection.
        /// </summary>
        private string status;

        /// <summary>
        /// The key pair used to encrypt communications with the Badumna network.
        /// </summary>
        private string keyPairXml;

        /// <summary>
        /// A thread for long-running work performed in the background.
        /// </summary>
        private Thread backgroundThread;

        /// <summary>
        /// A lock object for this.backgroundThread
        /// </summary>
        private object backgroundThreadLock = new object();

        /// <summary>
        /// The session used to authenticate and obtain a Dei IdentityProvider
        /// </summary>
        private Session session;

        /// <summary>
        /// The identity provider - available after successfully authenticating and selecting an identity.
        /// </summary>
        private IIdentityProvider identity;

        /// <summary>
        /// The authenticated username, or null if not yet authenticated.
        /// </summary>
        private string username;

        /// <summary>
        /// A value indicating whether an identity was selected since the last update.
        /// </summary>
        private bool identitySelected;

        /// <summary>
        /// Initialises a new instance of the VerifiedIdentityModule class.
        /// </summary>
        /// <param name="networkModule">Network module provides notifications of shutdown.</param>
        /// <param name="deiServerHost">The IP address or domain name of the Dei Server host.</param>
        /// <param name="deiServerPort">The port the Dei Server is listening on.</param>
        /// <param name="keyPairXml">Optional pre-generated key pair used to encrypt communications.</param>
        public VerifiedIdentityModule(NetworkModule networkModule, string deiServerHost, ushort deiServerPort, string keyPairXml = null)
        {
            this.networkModule = networkModule;
            this.deiServerHost = deiServerHost;
            this.deiServerPort = deiServerPort;

            // If no key pair was pre-generated, generate one now.
            this.keyPairXml = keyPairXml ?? Dei.Session.GenerateKeyPair();

            this.networkModule.ShuttingDown += this.OnShutdown;

            this.Characters = new ObservableCollection<Character>();

            this.backgroundThread = null;
            this.Clear();
        }

        /// <summary>
        /// Event used to report login-related messages
        /// </summary>
        public event MessageHandler MessageHandler;

        /// <summary>
        /// Gets a value indicating whether authentication is currently permitted.
        /// </summary>
        /// <remarks>We shouldn't try and re-authenticate while already in a Dei session.</remarks>
        public bool CanAuthenticate
        {
            get { return this.session == null && this.identity == null; }
        }

        /// <inheritdoc />
        public bool CanSelectCharacter
        {
            get { return this.session != null && this.identity == null; }
        }

        /// <summary>
        /// Gets the username of the authenticated user, or `null` if no user is authenticated.
        /// </summary>
        public string Username
        {
            get { return this.username; }
        }

        /// <summary>
        /// Gets an observable collection of the user's available characters.
        /// </summary>
        public ObservableCollection<Character> Characters { get; private set; }

        /// <inheritdoc />
        public string CharacterName
        {
            get { return this.characterName; }
        }

        /// <summary>
        /// Gets a message describing the current status of identity selection.
        /// </summary>
        public string Status
        {
            get { return this.status; }
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="VerifiedIdentityModule"/>
        /// is currently performing work in the background thread.
        /// </summary>
        /// <value>
        ///   <c>true</c> if busy; otherwise, <c>false</c>.
        /// </value>
        public bool Busy
        {
            get
            {
                lock (this.backgroundThreadLock)
                {
                    return this.backgroundThread != null;
                }
            }
        }

        /// <summary>
        /// This module is updated to allow login to occur on main update thread.
        /// </summary>
        public void Update()
        {
            if (this.identitySelected)
            {
                this.networkModule.Login(this.identity);
                this.identitySelected = false;
            }
        }

        /// <inheritdoc />
        public void Clear()
        {
            this.Characters.Clear();
            this.characterName = null;
            this.identity = null;
            this.status = "Identity unselected.";
            this.CloseSession();
        }

        /// <summary>
        /// Begin the creation of a verified identity using a given user account.
        /// </summary>
        /// <remarks>Since verified identity creation involves contacting the Dei Server over the network,
        /// it is done asynchronously to avoid blocking the main game loop.</remarks>
        /// <param name="username">The username for the account to use.</param>
        /// <param name="password">The password for the account to use.</param>
        public void Authenticate(string username, string password)
        {
            this.Clear();
            this.RunInBackground(() => this.DoAuthenticate(username, password));
        }

        /// <summary>
        /// Selecting an identity to log in as.
        /// </summary>
        /// <remarks>Performed asynchronously to avoid blocking the main game loop.</remarks>
        /// <param name="character">The Character to identify as.</param>
        public void SelectIdentity(Character character)
        {
            this.RunInBackground(() => this.DoSelectIdentity(character));
        }

        /// <summary>
        /// Add a new character to the user's account.
        /// </summary>
        /// <remarks>Performed asynchronously to avoid blocking the main game loop.</remarks>
        /// <param name="characterName">The character name to create.</param>
        public void AddCharacter(string characterName)
        {
            this.RunInBackground(() => this.DoAddCharacter(characterName));
        }

        /// <summary>
        /// Deletes a character from the user's account.
        /// </summary>
        /// <remarks>Performed asynchronously to avoid blocking the main game loop.</remarks>
        /// <param name="character">The character to delete.</param>
        public void DeleteCharacter(Character character)
        {
            this.RunInBackground(() => this.DoDeleteCharacter(character));
        }

        /// <summary>
        /// Create a verified identity using a given user account.
        /// </summary>
        /// <param name="username">The username for the account to use.</param>
        /// <param name="password">The password for the account to use.</param>
        private void DoAuthenticate(string username, string password)
        {
            var initialStatus = this.status;
            this.status = "Identity: Authenticating user account.";

            // create a User session which we'll use to authenticate and obtain an IdentityProvider.
            // SSL is not used for this demo, but should be used in a real application
            this.session = new Session(this.deiServerHost, this.deiServerPort, this.keyPairXml, null);
            this.session.UseSslConnection = false;

            // Authentication connects to DeiServer and therefore can take some time.
            LoginResult result = this.session.Authenticate(username, password);

            if (result.WasSuccessful)
            {
                this.status = "Identity: authenticated.";
                this.Characters.Clear();
                foreach (var character in result.Characters)
                {
                    this.Characters.Add(character);
                }

                this.username = username;
                this.EmitMessage("Authentication succeeded");
            }
            else
            {
                this.CloseSession();
                this.status = initialStatus;
                this.EmitMessage("Authentication failed: " + result.ErrorDescription);
            }
        }

        /// <summary>
        /// Disposes the active session, if present.
        /// </summary>
        private void CloseSession()
        {
            if (this.session != null)
            {
                this.session.Dispose();
                this.session = null;
            }

            this.username = null;
        }

        /// <summary>
        /// Select an identity using the authenticated user account.
        /// </summary>
        /// <param name="character">The character to select.</param>
        private void DoSelectIdentity(Character character)
        {
            this.status = "Identity: Selecting identity.";
            LoginResult result = this.session.SelectIdentity(character, out this.identity);
            if (result.WasSuccessful)
            {
                // Store character name for future reference.
                this.characterName = character.Name;

                this.status = "Identity: " + this.characterName;

                // we no longer need the session after a successful SelectIdentity
                this.CloseSession();
                this.EmitMessage("Identity selection succeeded");

                // Set a flag to indicate that the selection has completed, so that logging in can be initiated on update.
                this.identitySelected = true;
            }
            else
            {
                this.status = "Identity unselected";
                this.EmitMessage("Identity selection failed: " + result.ErrorDescription);
            }
        }

        /// <summary>
        /// Add a new character to the user's account.
        /// </summary>
        /// <param name="characterName">The character name to create.</param>
        private void DoAddCharacter(string characterName)
        {
            Character character = this.session.CreateCharacter(characterName);
            if (character != null)
            {
                this.Characters.Add(character);
            }
            else
            {
                this.EmitFailure("Could not create character " + characterName);
            }
        }

        /// <summary>
        /// Deletes a character from the user's account.
        /// </summary>
        /// <param name="character">The character to delete.</param>
        private void DoDeleteCharacter(Character character)
        {
            if (this.session.DeleteCharacter(character))
            {
                this.Characters.Remove(character);
            }
            else
            {
                this.EmitFailure("Could not delete character " + character.Name);
            }
        }

        /// <summary>
        /// Emits a log message 
        /// </summary>
        /// <param name="message">The message contents.</param>
        private void EmitMessage(string message)
        {
            var handler = this.MessageHandler;
            if (handler != null)
            {
                handler(message);
            }
        }

        /// <summary>
        /// Emits an error log message, appending the current session.FailureReason to the message text.
        /// </summary>
        /// <param name="message">The message.</param>
        private void EmitFailure(string message)
        {
            this.EmitMessage(message + " (Reason: " + this.session.FailureReason + ")");
        }

        /// <summary>
        /// Runs an action on the background thread.
        /// </summary>
        /// <remarks>Requires that only one background action is run at any
        /// time, throws an InvalidOperationException if multiple actions
        /// are attempted.</remarks>
        /// <param name="action">The action to run.</param>
        private void RunInBackground(Action action)
        {
            lock (this.backgroundThreadLock)
            {
                if (this.Busy)
                {
                    throw new InvalidOperationException("identity module is busy!");
                }

                this.backgroundThread = new Thread(new ThreadStart(delegate()
                    {
                        try
                        {
                            action.Invoke();
                        }
                        finally
                        {
                            lock (this.backgroundThreadLock)
                            {
                                this.backgroundThread = null;
                            }
                        }
                    }));

                this.backgroundThread.Start();
            }
        }

        /// <summary>
        /// Reset the identity module on shutdown.
        /// </summary>
        private void OnShutdown()
        {
            this.Clear();
        }
    }
}
