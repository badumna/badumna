﻿// -----------------------------------------------------------------------
// <copyright file="UnverifiedIdentityModule.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace BadumnaDemo
{
    using Badumna.Security;

    /// <summary>
    /// Allows creation of unverified identities.
    /// </summary>
    /// <remarks>
    /// You can only login to an insecure Badumna network with an unverified identity.
    /// To login to a secure Badumna network you need to use a verified Identity that has been authenticated using Dei.
    /// </remarks>
    public class UnverifiedIdentityModule : IIdentityModule
    {
        /// <summary>
        /// The network module.
        /// </summary>
        private readonly NetworkModule networkModule;

        /// <summary>
        /// The character name of the identity.
        /// </summary>
        private string characterName;

        /// <summary>
        /// The key pair used to encrypt communications with the Badumna network.
        /// </summary>
        private string keyPairXml;

        /// <summary>
        /// The identity.
        /// </summary>
        private UnverifiedIdentityProvider identityProvider;

        /// <summary>
        /// Initialises a new instance of the UnverifiedIdentityModule class.
        /// </summary>
        /// <param name="networkModule">The network module.</param>
        /// <param name="keyPairXml">Optional pre-generated key pair used to encrypt communications.</param>
        public UnverifiedIdentityModule(NetworkModule networkModule, string keyPairXml = null)
        {
            this.networkModule = networkModule;

            // If no key pair was pre-generated, generate one now.
            this.keyPairXml = keyPairXml ?? UnverifiedIdentityProvider.GenerateKeyPair();

            this.networkModule.ShuttingDown += this.OnShutdown;

            this.Clear();
        }

        /// <summary>
        /// Gets a value indicating whether an identity is selected.
        /// </summary>
        public bool HasIdentity
        {
            get { return this.identityProvider != null; }
        }

        /// <inheritdoc />
        public string CharacterName
        {
            get { return this.characterName; }
        }

        /// <summary>
        /// Creates an identityProvider using a given character name.
        /// </summary>
        /// <param name="characterName">The character name for the identity.</param>
        public void SelectIdentity(string characterName)
        {
            // Create an unverified identity provider that can be used to log in to Badumna.
            this.identityProvider = new UnverifiedIdentityProvider(characterName, this.keyPairXml);
            
            // Store the character name for future reference.
            this.characterName = characterName;

            this.networkModule.Login(this.identityProvider);
        }

        /// <summary>
        /// Reset the identity module.
        /// </summary>
        private void Clear()
        {
            this.identityProvider = null;
            this.characterName = "Unselected";
        }

        /// <summary>
        /// Reset the identity module on shutdown.
        /// </summary>
        private void OnShutdown()
        {
            this.Clear();
        }
    }
}
