
namespace DemoGame
{
using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

using DemoFramework;

    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register ("AppDelegate")]
    public partial class AppDelegate : UIApplicationDelegate
    {
        // class-level declarations
        DemoGame game;

        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching (UIApplication app, NSDictionary options)
        {
            // Configure and create the demo.
            var builder = new DemoBuilder();
            builder.DeadReckoningEnabled = true;
            builder.MultipleScenesEnabled = true;
            builder.ProximityChatEnabled = true;
            builder.PrivateChatEnabled = true;
            builder.DistributedEntitiesEnabled = true;
            ////builder.SecureIdentitiesEnabled = true;
            ////builder.ArbitrationServerEnabled = true;            
            this.game = builder.Build();
            game.Run();

            return true;
        }

        /// <summary>
        /// This method is called when the application enters the background.
        /// </summary>
        /// <param name='application'>The application</param>
        public override void DidEnterBackground (UIApplication application)
        {
            // Badumna enabled games should gracefully shutdown on entering the background.
            game.Shutdown();
        }
    }
}

