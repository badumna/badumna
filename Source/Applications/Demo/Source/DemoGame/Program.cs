//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace DemoGame
{
    using DemoFramework;

    /// <summary>
    /// A program to demonstrate Badumna functionality.
    /// </summary>
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        private static void Main()
        {
            DemoBuilder builder = new DemoBuilder();

            builder.DeadReckoningEnabled = true;
            builder.MultipleScenesEnabled = true;
            builder.ProximityChatEnabled = true;
            builder.PrivateChatEnabled = true;
            builder.DistributedEntitiesEnabled = true;
            ////builder.VerifiedIdentitiesEnabled = true;
            ////builder.ArbitrationServerEnabled = true;
            
            DemoGame game = builder.Build();
            game.Run();
        }
    }
}
