﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Badumna;
using Microsoft.Win32;

namespace ConfigMaker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Apply(TextBox textbox, Action<string> setter)
        {
            var text = textbox.Text.Trim();
            if (text.Length > 0)
            {
                setter(text);
            }
        }

        private void Save_Click(object sender, RoutedEventArgs args)
        {
            var saveDialog = new SaveFileDialog
            {
                Filter = "XML documents|*.xml|All files|*.*",
                DefaultExt = ".xml"
            };

            if (!(saveDialog.ShowDialog() == true))
            {
                return;
            }

            try
            {
                var options = new Options();

                if (this.IsLanMode.IsChecked == true)
                {
                    options.Connectivity.ConfigureForLan();
                }

                this.Apply(this.SeedPeer, s => options.Connectivity.SeedPeers.Add(s));
                this.Apply(this.ApplicationName, s => options.Connectivity.ApplicationName = s);
                this.Apply(this.StartPort, s => options.Connectivity.StartPortRange = int.Parse(s));
                this.Apply(this.EndPort, s => options.Connectivity.EndPortRange = int.Parse(s));

                this.Apply(this.MatchServerAddress, s => options.Matchmaking.ServerAddress = s);
                this.Apply(this.ActiveMatchLimit, s => options.Matchmaking.ActiveMatchLimit = int.Parse(s));

                options.Save(saveDialog.FileName);

                MessageBox.Show("Saved.");
            }
            catch (Exception e)
            {
                MessageBox.Show("Save failed: " + e.Message);
            }
        }
    }
}
