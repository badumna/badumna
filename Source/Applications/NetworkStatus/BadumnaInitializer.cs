﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna;
using System.Xml;

namespace NetworkStatus
{
    delegate void SetNetworkStatus(string content, bool enableSaveButton);

    class BadumnaInitializer
    {
        private SetNetworkStatus statusSetter;
        private INetworkFacade networkFacade;

        public BadumnaInitializer(SetNetworkStatus setter)
        {
            this.statusSetter = setter;
        }

        public void Initialize()
        {
            Options options = new Options();
            options.Connectivity.ApplicationName = "Badumna_UPnP_Tester";
            this.networkFacade = NetworkFacade.Create(options);
            this.networkFacade.Login();
            this.GetNetworkStatus();

            this.networkFacade.Shutdown(true);
        }

        private void SetNetworkStatusQueryResult(string content, bool enable)
        {
            if (this.statusSetter != null)
            {
                this.statusSetter(content, enable);
            }
        }

        private void GetNetworkStatus()
        {
            try
            {
                Badumna.NetworkStatus status = this.networkFacade.GetNetworkStatus();
                XmlDocument xmlDoc = status.ToXml();

                string publicAddress = xmlDoc.SelectSingleNode("/Status/Connectivity/PublicAddress").InnerText;
                string privateAddress = xmlDoc.SelectSingleNode("/Status/Connectivity/PrivateAddress").InnerText;
                string routerModel = xmlDoc.SelectSingleNode("/Status/Connectivity/Router").InnerText;
                string upnpEnabled = xmlDoc.SelectSingleNode("/Status/Connectivity/UPnPEnabled").InnerText;
                string upnpSucceed = xmlDoc.SelectSingleNode("/Status/Connectivity/UPnPSucceeded").InnerText;

                string natType = "Unknown";
                string[] publicAddressParts = publicAddress.Split('|');
                if (publicAddressParts != null && publicAddressParts[0] != null)
                {
                    natType = publicAddressParts[0];
                }

                string publicPort = "Unknown";
                publicAddressParts = publicAddress.Split(':');
                if (publicAddressParts != null && publicAddressParts.Length >= 2 && publicAddressParts[1] != null)
                {
                    publicPort = publicAddressParts[1];
                }

                string privatePort = "Unknown";
                string[] privateAddressParts = privateAddress.Split(':');
                if (privateAddressParts != null && privateAddressParts.Length >= 2 && privateAddressParts[1] != null)
                {
                    privatePort = privateAddressParts[1];
                }

                string statusContent = string.Format(" Router Model: {0} " + Environment.NewLine + 
                                                     " Nat Type: {1}" + Environment.NewLine + 
                                                     " Public Port: {2}" + Environment.NewLine +
                                                     " Private Port: {3}" + Environment.NewLine + 
                                                     " UPnP Enabled: {4}" + Environment.NewLine +
                                                     " UPnP Succeed: {5}",
                                                     routerModel, natType, publicPort, privatePort, upnpEnabled, upnpSucceed);
                this.SetNetworkStatusQueryResult(statusContent, true);
            }
            catch
            {
                this.SetNetworkStatusQueryResult("Failed to get the network status.", false);
            }
        }
    }
}
