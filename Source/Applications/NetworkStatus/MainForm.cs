﻿using System;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using System.ComponentModel;


namespace NetworkStatus
{
    delegate void SetTextCallback(string text);
    delegate void SetBoolCallback(bool b);

    class MainForm : Form
    {
        private TextBox networkStatusTextBox;
        private Button saveButton;

        private Thread badumnaInitializereThread;

        public MainForm()
        {
            this.InitializeComponent();
            this.StartBadumnaInitializer();
        }

        public void SetNetworkStatusLabel(string content, bool enableSaveButton)
        {
            if (!this.networkStatusTextBox.InvokeRequired)
            {
                this.networkStatusTextBox.Text = content;
            }
            else
            {
                SetTextCallback d = new SetTextCallback(this.SetStatusLabel);
                this.Invoke(d, new object[] { content });

            }

            if (!this.saveButton.InvokeRequired)
            {
                this.saveButton.Enabled = enableSaveButton;
            }
            else
            {
                SetBoolCallback d = new SetBoolCallback(this.SetSaveAsButtonEnabled);
                this.Invoke(d, new object[] { enableSaveButton });
            }
        }

        private void SetStatusLabel(string text)
        {
            if (text != null)
            {
                this.networkStatusTextBox.Text = text;
                // this.networkStatusTextBox.Size = new Size(this.networkStatusTextBox.PreferredWidth, this.networkStatusTextBox.PreferredHeight + 2);
            }
        }

        private void SetSaveAsButtonEnabled(bool enabled)
        {
            this.saveButton.Enabled = enabled;
        }

        private void InitializeComponent()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "Badumna UPnP Client Tester";
            this.MaximizeBox = false;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.Size = new Size(300, 200);
            this.FormClosing += this.Form_Closing;

            // network status label
            this.networkStatusTextBox = new TextBox();
            this.networkStatusTextBox.Multiline = true;
            this.networkStatusTextBox.BorderStyle = BorderStyle.FixedSingle;
            this.networkStatusTextBox.Text = "Initializing the Badumna Network Suite.";
            this.networkStatusTextBox.ReadOnly = true;
            this.networkStatusTextBox.Size = new Size(275, 120);
            this.networkStatusTextBox.Location = new Point(10, 20);
            this.Controls.Add(this.networkStatusTextBox);

            // save as button
            this.saveButton = new Button();
            this.saveButton.Text = "Save As";
            this.saveButton.Size = this.saveButton.PreferredSize;
            this.saveButton.Location = new Point((this.Width - this.saveButton.Width) / 2, 140);
            this.saveButton.Click += this.SaveButton_Click;
            this.saveButton.Enabled = false;
            this.Controls.Add(this.saveButton);
        }

        public void Form_Closing(object sender, CancelEventArgs e)
        {
            if (this.badumnaInitializereThread != null)
            {
                this.badumnaInitializereThread.Join();
            }
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.SaveFileDialog sfd = new System.Windows.Forms.SaveFileDialog();
            sfd.Filter = "Text file (*.txt)|*.txt*|All Files|*.*";

            if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string[] filePath = sfd.FileNames;

                if (filePath != null && filePath[0] != null)
                {
                    string fullPath = filePath[0];

                    if (!fullPath.ToLower().EndsWith(".txt"))
                    {
                        fullPath = fullPath + ".txt";
                    }

                    this.SaveNetworkStatus(fullPath, this.networkStatusTextBox.Text);
                }
            }
        }

        private void SaveNetworkStatus(string path, string content)
        {
            using (TextWriter tw = new StreamWriter(path))
            {
                tw.Write(content);
                tw.Close();
            }
        }

        private void StartBadumnaInitializer()
        {
            BadumnaInitializer badumnaInitializer = new BadumnaInitializer(this.SetNetworkStatusLabel);
            badumnaInitializereThread = new Thread(new ThreadStart(badumnaInitializer.Initialize));
            badumnaInitializereThread.Start();
        }
    }
}
