﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

using Badumna;

namespace ConsoleChatApp
{
    public class ConsoleChatApp
    {
        private INetworkFacade networkFacade;
        private ChatManager chatManager;

        public void Run(string friendsListFilename)
        {
            bool success = this.InitializeBadumna();
            if (!success)
            {
                Console.WriteLine("Failed to initialize the badumna engine.");
                Environment.Exit(1);
            }

            this.chatManager = new ChatManager(this.networkFacade);
            success = this.chatManager.Initialize(friendsListFilename);
            if (!success)
            {
                Console.WriteLine("Failed to initialize the chat manager.");
                Environment.Exit(1);
            }

            this.networkFacade.AddressChangedEvent += this.AddressChangedHandler;
            
            // enter into the main loop.
            this.MainLoop();
        }

        private bool InitializeBadumna()
        {
            Options options = new Options();
            options.Connectivity.ApplicationName = "Badumna_Private_Chat_App";
            this.networkFacade = NetworkFacade.Create(options);
            bool success = this.networkFacade.Login();

            Console.WriteLine(this.networkFacade.GetNetworkStatus().ToString());

            return success;
        }

        private void MainLoop()
        {
            Console.WriteLine("Press c to send a chat message, x to exit.");

            while (true)
            {
                ConsoleKeyInfo keyinfo = Console.ReadKey();
                if (keyinfo.KeyChar == 'x')
                {
                    break;
                }
                else if (keyinfo.KeyChar == 'c')
                {
                    this.chatManager.SendChatMessage();
                }
            }

            this.Shutdown();
        }

        private void Shutdown()
        {
            // Badumna specific: log out from and shut down the network
            this.networkFacade.Logout();
            this.networkFacade.Shutdown();
        }

        private void AddressChangedHandler()
        {
            Console.Error.WriteLine("### Address has been changed, time to press x and exit.");
        }
    }
}