﻿//-----------------------------------------------------------------------
// <copyright file="Friend.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Chat;

namespace ConsoleChatApp
{
    /// <summary>
    /// The class used to represent a friend. 
    /// </summary>
    internal class Friend
    {
        /// <summary>
        /// The unique name of the friend. 
        /// </summary>
        private string name;

        /// <summary>
        /// The presence status of the friend (e.g. online, busy, away and etc.). 
        /// </summary>
        private ChatStatus status;

        /// <summary>
        /// The chat channel with the friend.
        /// </summary>
        private ChatChannelId channelId;

        /// <summary>
        /// Initializes a new instance of the <see cref="Friend"/> class.
        /// </summary>
        /// <param name="name">The friend's name.</param>
        public Friend(string name)
        {
            this.name = name;
            this.status = ChatStatus.Offline;
            this.channelId = null;
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The friend's name.</value>
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>The presence status.</value>
        public ChatStatus Status
        {
            get { return this.status; }
            set { this.status = value; }
        }

        /// <summary>
        /// Gets or sets the chat channel id.
        /// </summary>
        /// <value>The chat channel id.</value>
        public ChatChannelId ChannelId
        {
            get { return this.channelId; }
            set { this.channelId = value; }
        }
    }
}
