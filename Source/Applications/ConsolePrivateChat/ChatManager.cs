﻿//-----------------------------------------------------------------------
// <copyright file="ChatManager.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.IO;
using System.Threading;

using Badumna;
using Badumna.Chat;
using Badumna.DataTypes;

namespace ConsoleChatApp
{
    /// <summary>
    /// The ChatManager class implements fully distributed private chat by using Badumna's chat service. 
    /// </summary>
    internal class ChatManager
    {
        /// <summary>
        /// The friends list.
        /// </summary>
        private FriendsList friends;

        /// <summary>
        /// The chat service object returned by NetworkFacade.CreateChatService.
        /// </summary>
        private IChatService badumnaChatService;

        /// <summary>
        /// The user name of the local peer. 
        /// </summary>
        private string myName;

        /// <summary>
        /// The timer used to periodically call NetworkFacade.ProcessNetworkState. 
        /// </summary>
        private System.Threading.Timer processTimer;

        /// <summary>
        /// When the last chat message is sent. 
        /// </summary>
        private DateTime lastSend;

        /// <summary>
        /// message count.
        /// </summary>
        private int count;

        /// <summary>
        /// The network facade.
        /// </summary>
        private INetworkFacade networkFacade;

        /// <summary>
        /// Initializes a new instance of the <see cref="ChatManager"/> class.
        /// </summary>
        /// <param name="networkFacade">The network facade</param>
        public ChatManager(INetworkFacade networkFacade)
        {
            this.networkFacade = networkFacade;
            this.count = 0;
            this.lastSend = DateTime.UtcNow;
            this.friends = new FriendsList();
            TimerCallback callback = new TimerCallback(this.RegularProcessing);
            this.processTimer = new Timer(callback, null, 0, 50); 
        }

        /// <summary>
        /// Shutdowns this instance.
        /// </summary>
        public void Shutdown()
        {
        }

        /// <summary>
        /// Initializes this instance. Must be called after initializing the badumna suite.
        /// </summary>
        /// <param name="friendsListFile">The friends list file.</param>
        /// <returns>True if successful or false on failure.</returns>
        public bool Initialize(string friendsListFile)
        {
            this.badumnaChatService = this.networkFacade.CreateChatService();
            if (this.badumnaChatService == null)
            {
                return false;
            }

            string name = this.ReadUsername(friendsListFile);
            if (name == null)
            {
                return false;
            }
            this.myName = name;

            bool success = this.friends.LoadFriendsList(friendsListFile);
            if (!success)
            {
                return false;
            }

            this.badumnaChatService.OpenPrivateChannels(this.HandleChannelInvitation, this.myName);
            this.badumnaChatService.ChangePresence(ChatStatus.Online);

            // invite all users to private channel
            foreach (Friend friend in friends)
            {
                this.badumnaChatService.InviteUserToPrivateChannel(friend.Name);
            }

            return true;
        }

        /// <summary>
        /// Sets the presence status.
        /// </summary>
        /// <param name="status">The presence status.</param>
        public void SetPresenceStatus(ChatStatus status)
        {
            if (this.badumnaChatService != null)
            {
                this.badumnaChatService.ChangePresence(status);
            }
        }

        public void SendChatMessage()
        {
            string name = string.Empty;
            if (myName.ToLower() == "john")
            {
                name = "mary";
            }
            else if (myName.ToLower() == "mary")
            {
                name = "john";
            }
            else
            {
                Console.WriteLine("unknown name " + myName);
            }

            this.SendChatMessage(name, "private chat msg, c = " + this.count++);
        }

        /// <summary>
        /// Sends the chat message.
        /// </summary>
        /// <param name="username">The username of the receiver.</param>
        /// <param name="message">The message to be sent.</param>
        private void SendChatMessage(string username, string message)
        {
            Friend friend = this.friends.GetFriend(username);
            if (friend != null && friend.ChannelId != null && this.badumnaChatService != null)
            {
                this.badumnaChatService.SendChannelMessage(friend.ChannelId, message);
            }
        }

        /// <summary>
        /// Gets or sets my username.
        /// </summary>
        /// <value>The username.</value>
        public string MyUsername
        {
            get { return this.myName; }
            set { this.myName = value; }
        }

        /// <summary>
        /// Reads the username from the friends list file.
        /// </summary>
        /// <param name="filepath">The path of the friends list file.</param>
        /// <returns>The user name or null on failure.</returns>
        private string ReadUsername(string filepath)
        {
            using (TextReader tr = new StreamReader(filepath))
            {
                try
                {
                    string line = tr.ReadLine();
                    tr.Close();

                    return line;
                }
                catch
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Updates the friend presence status.
        /// </summary>
        /// <param name="name">The friend's name.</param>
        /// <param name="status">The presence status.</param>
        private void UpdateFriendPresenceStatus(string name, ChatStatus status)
        {
            this.friends.SetFriendChatStatus(name, status);
        }

        /// <summary>
        /// Badumna requires NetworkFacade.ProcessNetworkState to be called regularly.
        /// </summary>
        /// <param name="o">The o.</param>
        private void RegularProcessing(object o)
        {
            if (this.networkFacade.IsLoggedIn)
            {
                this.networkFacade.ProcessNetworkState();
            }
        }

        /// <summary>
        /// Handles the channel invitation.
        /// </summary>
        /// <param name="channel">The channel id.</param>
        /// <param name="username">The username.</param>
        private void HandleChannelInvitation(ChatChannelId channel, string username)
        {
            Friend friend = this.friends.GetFriend(username);
            if (friend != null)
            {
                if (friend.ChannelId != null)
                {
                    if (!friend.ChannelId.Equals(channel))
                    {
                        this.badumnaChatService.UnsubscribeFromChatChannel(friend.ChannelId);
                        friend.ChannelId = channel;
                        this.badumnaChatService.AcceptInvitation(channel, this.HandlePrivateMessage, this.HandlePresence);
                    }

                    return;
                }
                else
                {
                    friend.ChannelId = channel;
                    this.badumnaChatService.AcceptInvitation(channel, this.HandlePrivateMessage, this.HandlePresence);
                }
            }
        }

        /// <summary>
        /// Handles the incoming private message.
        /// </summary>
        /// <param name="channel">The channel id.</param>
        /// <param name="userId">The sender user id.</param>
        /// <param name="message">The chat message.</param>
        private void HandlePrivateMessage(ChatChannelId channel, BadumnaId userId, string message)
        {
            string name = this.friends.GetFriendNameByChannelId(channel);
            if (name != null)
            {
                Console.WriteLine("[Received - {0}] : {1}.", name, message);
            }
        }

        /// <summary>
        /// Handles the presence.
        /// </summary>
        /// <param name="channel">The channel id.</param>
        /// <param name="userId">The sender user id.</param>
        /// <param name="username">The username.</param>
        /// <param name="status">The current present status.</param>
        private void HandlePresence(ChatChannelId channel, BadumnaId userId, string username, ChatStatus status)
        {
            this.friends.SetFriendChatStatus(username, status);
            Console.WriteLine("[PC {0}] : now {1}.", username, status);

            if (status == ChatStatus.Offline)
            {
                Friend friend = this.friends.GetFriend(username);
                if (friend != null)
                {
                    friend.ChannelId = null;
                }
            }
        }
    }
}
