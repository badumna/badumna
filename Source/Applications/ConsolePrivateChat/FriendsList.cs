﻿//-----------------------------------------------------------------------
// <copyright file="FriendList.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

using Badumna.Chat;
using System;

namespace ConsoleChatApp
{
    /// <summary>
    /// The FriendsList class represent all friends of the local user.  
    /// </summary>
    internal class FriendsList : IEnumerable<Friend>
    {
        /// <summary>
        /// All friends, indexed by their unique name. 
        /// </summary>
        private Dictionary<string, Friend> friends;

        /// <summary>
        /// Initializes a new instance of the <see cref="FriendsList"/> class.
        /// </summary>
        public FriendsList()
        {
            this.friends = new Dictionary<string, Friend>();
        }

        /// <summary>
        /// Loads the friends list. In this example, friends list are stored in text files, see mary.list
        /// and john.list for the format of such friends list. 
        /// </summary>
        /// <param name="path">The full path of the friend list file.</param>
        /// <returns></returns>
        public bool LoadFriendsList(string path)
        {
            using (TextReader tr = new StreamReader(path))
            {
                try
                {
                    string line;
                    int lineNumber = 1;
                    while ((line = tr.ReadLine()) != null)
                    {
                        if (lineNumber != 1)
                        {
                            // skip the first line, this is my own name
                            Friend friend = new Friend(line);
                            this.AddFriend(friend);
                        }

                        lineNumber++;
                    }

                    tr.Close();

                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Sets the friend chat presence status.
        /// </summary>
        /// <param name="name">The name of the friend.</param>
        /// <param name="status">The presence status.</param>
        public void SetFriendChatStatus(string name, ChatStatus status)
        {
            Friend friend;
            if (this.friends.TryGetValue(name, out friend))
            {
                friend.Status = status;
            }
            else
            {
                Console.WriteLine("Unknown friend. name = {0}", name);
                Console.WriteLine("ST = {0}", Environment.StackTrace);
            }
        }

        /// <summary>
        /// Sets the friend chat channel id.
        /// </summary>
        /// <param name="name">The name of the friend.</param>
        /// <param name="id">The chat channel id.</param>
        public void SetFriendChatChannelId(string name, ChatChannelId id)
        {
            Friend friend;
            if (this.friends.TryGetValue(name, out friend))
            {
                friend.ChannelId = id;
            }
        }

        /// <summary>
        /// Gets the friend object according to the specified unique name.
        /// </summary>
        /// <param name="name">The friend name.</param>
        /// <returns>The friend object that matches the specified name.</returns>
        public Friend GetFriend(string name)
        {
            Friend friend;
            if (this.friends.TryGetValue(name, out friend))
            {
                return friend;
            }

            return null;
        }

        /// <summary>
        /// Gets the friend name according to the specified chat channel id.
        /// </summary>
        /// <param name="id">The chat channel id.</param>
        /// <returns>The friend name.</returns>
        public string GetFriendNameByChannelId(ChatChannelId id)
        {
            foreach (Friend friend in this.friends.Values)
            {
                if (friend.ChannelId != null && friend.ChannelId.Equals(id))
                {
                    return friend.Name;
                }
            }

            return null;
        }

        /// <summary>
        /// Adds the friend.
        /// </summary>
        /// <param name="friend">The friend.</param>
        /// <returns>true if successful, or false if the friend is already in the friend list.</returns>
        private bool AddFriend(Friend friend)
        {
            if (!this.friends.ContainsKey(friend.Name))
            {
                this.friends[friend.Name] = friend;
                return true;
            }

            return false;
        }

        #region IEnumerable<Friend> Members

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1"/> that can be used to iterate through the collection.
        /// </returns>
        public IEnumerator<Friend> GetEnumerator()
        {
            if (this.friends.Count == 0)
            {
                yield break;
            }

            foreach (KeyValuePair<string, Friend> kvp in this.friends)
            {
                yield return kvp.Value;
            }
        }

        #endregion

        #region IEnumerable Members

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator"/> object that can be used to iterate through the collection.
        /// </returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        #endregion
    }
}
