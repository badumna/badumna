﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace ConsoleChatApp
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("Usage: ConsolePrivateChat [list file name]");
                return;
            }

            if (!File.Exists(args[0]))
            {
                Console.WriteLine("Couldn't locate specified file " + args[0]);
                return;
            }

            ConsoleChatApp app = new ConsoleChatApp();
            app.Run(args[0]);
        }
    }
}
