﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MatchmakingServer
{
    using System;
    using System.Reflection;
    using System.Security;
    using Badumna;
    using Badumna.ServiceDiscovery;
    using GermHarness;
    using Mono.Options;

    /// <summary>
    /// The entry point of the matchmaking server application.
    /// </summary>
    internal class Program
    {       
        /// <summary>
        /// List of options that this class should handle. 
        /// </summary>
        private static readonly OptionSet OptionSet = new OptionSet 
            {
                { "h|help", "show this message and exit.", v =>
                    {
                        if (v != null)
                        {
                            WriteUsageAndExit();
                        }
                    }
                }
            };

        /// <summary>
        /// The peer harness for hosting the process and communicating with Control Center.
        /// </summary>
        private static PeerHarness peerHarness;

        /// <summary>
        /// Entry point of the application.
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        internal static void Main(string[] args)
        {
            // Parse program-specific command line arguments.
            try
            {
                var options = new Options();

                // Set the default application name to match DemoGame.
                // This can be overridden with the --application-name command line argument
                options.Connectivity.ApplicationName = "CloudDemo";
                options.Connectivity.IsHostedService = true;
                options.Arbitration.Servers.Add(new ArbitrationServerDetails("com.scalify.matchmaking"));
                peerHarness = new PeerHarness(options);

                var extras = OptionSet.Parse(args);

                var harnessArgs = extras.ToArray();
                if (peerHarness.Initialize(ref harnessArgs, new ArbitrationProcess(peerHarness)))
                {
                    if (harnessArgs.Length > 0)
                    {
                        string assemblyName = Assembly.GetExecutingAssembly().GetName().Name;
                        Console.Write("{0}: Unknown argument(s):", assemblyName);
                        foreach (var arg in harnessArgs)
                        {
                            Console.Write(" " + arg);
                        }

                        Console.WriteLine(".");
                        Console.WriteLine("Try `{0} --help' for more information.", assemblyName);
                    }
                    else
                    {
                        // install the cancel key handler
                        Console.CancelKeyPress += delegate(object sender, ConsoleCancelEventArgs e)
                        {
                            e.Cancel = true;
                            peerHarness.Stop();
                        };

                        // Start the arbitration process.
                        peerHarness.Start();
                        Console.WriteLine("Matchmaking server has been stopped.");
                    }
                }
                else
                {
                    Console.WriteLine("Could not initialize peer harness.");
                }
            }
            catch (OptionException ex)
            {
                string assemblyName = Assembly.GetExecutingAssembly().GetName().Name;
                Console.Write("{0}: ", assemblyName);
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try `{0} --help' for more information.", assemblyName);
                Environment.Exit((int)ExitCode.InvalidArguments);
            }
            catch (DeiAuthenticationException ex)
            {
                Console.WriteLine("Exception caught : " + ex.Message);
                Environment.Exit((int)ExitCode.DeiAuthenticationFailed);
            }
            catch (SecurityException ex)
            {
                Console.WriteLine("Exception caught : " + ex.Message);
                Environment.Exit((int)ExitCode.AnnounceServiceFailed);
            }
        }

        /// <summary>
        /// Writes the usage.
        /// </summary>
        private static void WriteUsageAndExit()
        {
            string assemblyName = Assembly.GetExecutingAssembly().GetName().Name;
            Console.WriteLine("Usage: {0} [Options]", assemblyName);
            Console.WriteLine("Start a Matchmaking Server on the local machine.");
            Console.WriteLine(string.Empty);
            Console.WriteLine("Options:");
            peerHarness.WriteOptionDescriptions(Console.Out);
            (new ArbitrationProcess(peerHarness)).WriteOptionsDescription(Console.Out);
            OptionSet.WriteOptionDescriptions(Console.Out);
            Environment.Exit((int)ExitCode.DisplayHelp);
        }
    }
}
