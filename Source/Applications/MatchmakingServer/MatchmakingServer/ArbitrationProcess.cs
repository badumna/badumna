﻿//-----------------------------------------------------------------------
// <copyright file="ArbitrationProcess.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MatchmakingServer
{
    using System;
    
    using System.IO;
    using System.Threading;

    using GermHarness;

    using Mono.Options;
    using System.Diagnostics;

    using Utilities;
    using System.Text;

    /// <summary>
    /// ArbitrationProcess class implements IHostedProcess and responsible to 
    /// start the matchmaker server.
    /// </summary>
    internal class ArbitrationProcess : IHostedProcess
    {
        /// <summary>
        /// Peer harness instance.
        /// </summary>
        private readonly IPeerHarness peerHarness;

        /// <summary>
        /// List of options that this class should handle.
        /// </summary>
        private readonly OptionSet optionSet;

        /// <summary>
        /// Record interval when stats recording is enabled.
        /// </summary>
        private readonly TimeSpan recordInterval = TimeSpan.FromSeconds(5);

        /// <summary>
        /// Matchmaker server.
        /// </summary>
        private Matchmaker matchmaker;

        /// <summary>
        /// Record the network status to a file.
        /// </summary>
        private bool recordStats;

        /// <summary>
        /// File stream for record stats to a file.
        /// </summary>
        private FileStream statsFileStream;

        /// <summary>
        /// Record stats timer.
        /// </summary>
        private Timer statsTimer;

        /// <summary>
        /// Performance monitor.
        /// </summary>
        private PerformanceMonitor performanceMonitor;

        /// <summary>
        /// Initializes a new instance of the <see cref="ArbitrationProcess"/> class.
        /// </summary>
        /// <param name="peerHarness">Peer harness instance.</param>
        public ArbitrationProcess(IPeerHarness peerHarness)
        {
            if (peerHarness == null)
            {
                throw new ArgumentNullException("peerHarness");
            }

            this.peerHarness = peerHarness;

            this.optionSet = new OptionSet
                {
                    { "r|record-stats", "Records the network stats into a file.", var => this.recordStats = true }
                };
        }

        /// <summary>
        /// OnInitialize do nothing.
        /// </summary>
        /// <param name="arguments">Arguments passed from the upper class.</param>
        public void OnInitialize(ref string[] arguments)
        {
            this.performanceMonitor = new PerformanceMonitor(Environment.OSVersion);

            var extras = this.optionSet.Parse(arguments);
            arguments = extras.ToArray();
        }

        /// <summary>
        /// OnPerformRegularTasks call the matchmaker server regular task method.
        /// </summary>
        /// <param name="delayMilliseconds">Delay since last call in milliseconds.</param>
        /// <returns>Return true.</returns>
        public bool OnPerformRegularTasks(int delayMilliseconds)
        {
            if (this.matchmaker != null)
            {
                this.matchmaker.OnPerformRegularTasks();
            }
            
            return true;
        }

        /// <summary>
        /// OnProcessRequest do nothing, enable this when custom request from CC need to be implemented.
        /// </summary>
        /// <param name="requestType">Request type.</param>
        /// <param name="request">Request arguments in bytes</param>
        /// <returns>Return the request reply in this case it will just return null.</returns>
        public byte[] OnProcessRequest(int requestType, byte[] request)
        {
            return null;
        }

        /// <summary>
        /// Shutdown the matchmaker server.
        /// </summary>
        public void OnShutdown()
        {
            this.matchmaker.OnShutdown();

            if (this.statsFileStream != null)
            {
                this.statsFileStream.Close();
            }

            if (this.statsTimer != null)
            {
                this.statsTimer.Dispose();
                this.statsTimer = null;
            }
        }

        /// <summary>
        /// Initialize the matchmaker server and register its service to the network.
        /// </summary>
        /// <returns>Return true.</returns>
        public bool OnStart()
        {
            this.matchmaker = new Matchmaker(this.peerHarness.NetworkFacadeInstance);
            this.peerHarness.NetworkFacadeInstance.RegisterArbitrationHandler(
                this.matchmaker.HandleClientEvent,
                TimeSpan.FromMinutes(16),
                this.matchmaker.HandleClientDisconnect);

            if (this.recordStats)
            {
                this.statsTimer = new Timer((object state) => this.RecordStats(), null, 0, (int)this.recordInterval.TotalMilliseconds);
            }

            return true;
        }

        /// <summary>
        /// WriteOptionsDescription if there is any.
        /// </summary>
        /// <param name="tw">Text writer.</param>
        public void WriteOptionsDescription(TextWriter tw)
        {
            this.optionSet.WriteOptionDescriptions(tw);
        }

        /// <summary>
        /// Records the network status to a file.
        /// </summary>
        private void RecordStats()
        {
            if (this.statsFileStream == null)
            {
                this.statsFileStream = new FileStream("allstats.csv", FileMode.Create);
            }

            // Gets the network status.
            var machineStatus = this.performanceMonitor.GetMachineStatus();
            var networkStatus = this.peerHarness.NetworkFacadeInstance.GetNetworkStatus();
            var statsRecord = string.Format(
                "{0},{1},{2},{3},{4},{5},{6},0,{7},{8}\r\n",
                DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffffff"),
                Process.GetCurrentProcess().Id.ToString(),
                networkStatus.ActiveConnectionCount,
                networkStatus.TotalBytesSentPerSecond,
                networkStatus.TotalBytesReceivedPerSecond,
                networkStatus.RemoteObjectCount,
                networkStatus.InitializingConnectionCount,
                machineStatus[0],
                machineStatus[1]);

            var inBytes = new UTF8Encoding(true).GetBytes(statsRecord);
            
            lock (this.statsFileStream)
            {
                this.statsFileStream.Write(inBytes, 0, inBytes.Length);
                this.statsFileStream.Flush();
            }
        }
    }
}
