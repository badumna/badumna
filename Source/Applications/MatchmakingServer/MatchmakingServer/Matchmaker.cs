﻿//-----------------------------------------------------------------------
// <copyright file="Matchmaker.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MatchmakingServer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Badumna;
    using Badumna.Arbitration;
    using Badumna.Matchmaking;

    /// <summary>
    /// Matchmaker class has responsibility for handling the incoming matchmaking request
    /// and allocate the player into suitable game room and notify them when the match 
    /// is ready.
    /// </summary>
    internal class Matchmaker
    {
        /// <summary>
        /// Room lock object.
        /// </summary>
        private readonly object roomLock = new object();

        /// <summary>
        /// Badumna network.
        /// </summary>
        private readonly INetworkFacade network;

        /// <summary>
        /// Queue of matchmaking request.
        /// </summary>
        private readonly Queue<Player> queue = new Queue<Player>();

        /// <summary>
        /// List of open rooms.
        /// </summary>
        private readonly List<GameRoom> rooms = new List<GameRoom>();

        /// <summary>
        /// Collection of Session Id of user that currently requesting a match.
        /// </summary>
        private readonly Dictionary<int, Player> requests = new Dictionary<int, Player>();

        /// <summary>
        /// Initializes a new instance of the <see cref="Matchmaker"/> class.
        /// </summary>
        /// <param name="network">Badumna network.</param>
        internal Matchmaker(INetworkFacade network)
        {
            this.network = network;
        }

        /// <summary>
        /// Handle incoming client event.
        /// </summary>
        /// <param name="sessionId">Client session id.</param>
        /// <param name="message">Incoming message in byte array.</param>
        internal void HandleClientEvent(int sessionId, byte[] message)
        {
            ArbitrationEvent arbitrationEvent = MatchmakingEventSet.Deserialize(message);
            if (arbitrationEvent is MatchmakingRequestEvent)
            {
                var request = arbitrationEvent as MatchmakingRequestEvent;
                Console.WriteLine("Handling matchmaking request from session Id {0}", sessionId);
                
                var user = new Player
                {   
                    Id = sessionId, 
                    MinimumPlayers = request.MinPlayers, 
                    MaximumPlayers = request.MaxPlayers, 
                    PlayerAttributes = request.PlayerAttributes, 
                    PlayerGroup = request.PlayerGroup, 
                    ApplicationId = request.ApplicationId,
                    Timeout = DateTime.Now + request.Timeout
                };

                lock (this.queue)
                {
                    this.queue.Enqueue(user);
                }
            }                        
            else
            {
                Console.WriteLine("Ignoring unknown request.");
            }
        }

        /// <summary>
        /// Handle client disconnection.
        /// </summary>
        /// <param name="sessionId">Disconnected client session id.</param>
        internal void HandleClientDisconnect(int sessionId)
        {
            Console.WriteLine("Client with session Id {0} is disconnected.", sessionId);
        }

        /// <summary>
        /// Shutdown matchmaker.
        /// </summary>
        internal void OnShutdown()
        {
        }

        /// <summary>
        /// Matchmaker regular task, this method is called periodically.
        /// </summary>
        internal void OnPerformRegularTasks()
        {
            // check any pending request.
            while (this.queue.Count > 0)
            {
                Player player;

                lock (this.queue)
                {
                    player = this.queue.Dequeue();
                }
                
                // if previous request is currently being processed, 
                // than ignored the new one unless the request is different
                Player p;
                if (this.requests.TryGetValue(player.Id, out p))
                {
                    if (p.Equals(player))
                    {
                        continue;
                    }
                    else
                    {
                        // Remove the player from room.
                        var r = this.rooms.Find((GameRoom i) => i.Players.Contains(p));

                        if (r != null)
                        {
                            this.RemoveUser(r, p);                            
                        }
                    }
                }
            
                // add user to an open room.
                this.requests.Add(player.Id, player);
                var room = this.GetFirstOpenRoom(player)
                           ?? this.AddRoom(player);

                lock (this.roomLock)
                {
                    room.AddPlayer(player);
                    this.SendMatchmakingProgress(MatchmakingOperation.PlayerAdded, room, player);
                }
            }

            var openRooms = this.rooms.FindAll(r => r.Open);

            // Update open rooms
            foreach (var openRoom in openRooms)
            {
                var now = DateTime.Now;
                if (openRoom.IsFull)
                {
                    // room is full.
                    this.CloseRoom(openRoom);
                }
                else if (openRoom.Players.Count >= openRoom.MinimumPlayers)
                {
                    if ((openRoom.CombinedPlayerAttributes == 0xFFFFFFFF)
                        || openRoom.Players.Any(player => (player.Timeout - now).Milliseconds < 0))
                    {
                        // check if the player attributes is completed.
                        // check if any player has reached the timeout, if yes then start the game.
                        this.CloseRoom(openRoom);                        
                    }                    
                }
                else
                {
                    // room is still haven't got minimum players. 
                    var timeoutPlayer = openRoom.Players.Where(player => (player.Timeout - now).Milliseconds < 0).ToList();

                    foreach (var player in timeoutPlayer)
                    {
                        this.MatchmakingTimeout(openRoom, player);
                    }

                    timeoutPlayer.Clear();
                }
            }
        }

        /// <summary>
        /// Close a room and notify all the players that the match is ready.
        /// </summary>
        /// <param name="room">Game room that need to be closed.</param>
        private void CloseRoom(GameRoom room)
        {
            Console.WriteLine("Closing room ({0}) and start match", room.Id);

            lock (this.roomLock)
            {
                this.rooms.Remove(room);
            }

            var localId = 0;

            foreach (var player in room.Players)
            {
                var reply = new MatchmakingReplyEvent(room.Id, (short)room.Players.Count, (short)localId);
                this.network.SendServerArbitrationEvent(player.Id, MatchmakingEventSet.Serialize(reply));
                this.requests.Remove(player.Id);
                localId++;
            }
        }

        /// <summary>
        /// Handle the matchmaking timeout event.
        /// </summary>
        /// <param name="room">Game room where one of the player request is time out.</param>
        /// <param name="player">Player which match's request is timeout.</param>
        private void MatchmakingTimeout(GameRoom room, Player player)
        {
            var reply = new MatchmakingReplyEvent(string.Empty, -1, -1);
            this.network.SendServerArbitrationEvent(player.Id, MatchmakingEventSet.Serialize(reply));
            this.RemoveUser(room, player);
        }

        /// <summary>
        /// Get the first open room that have the same properties as the given min and max players.
        /// </summary>
        /// <param name="player">Player details.</param>
        /// <returns>Return an open game room.</returns>
        private GameRoom GetFirstOpenRoom(Player player)
        {
            foreach (var room in this.rooms)
            {
                if (!room.Open || room.IsFull)
                {
                    continue;
                }

                if (room.IsEmpty)
                {
                    // reused an empty room
                    room.MinimumPlayers = player.MinimumPlayers;
                    room.MaximumPlayers = player.MaximumPlayers;
                    room.PlayerGroup = player.PlayerGroup;
                    room.ApplicationId = player.ApplicationId;
                    return room;
                }

                if (room.ApplicationId != player.ApplicationId
                    || room.PlayerGroup != player.PlayerGroup
                    || room.MinimumPlayers != player.MinimumPlayers 
                    || room.MaximumPlayers != player.MaximumPlayers)
                {
                    continue;
                }

                if ((player.PlayerAttributes == 0 && room.CombinedPlayerAttributes == 0)
                    || (player.PlayerAttributes == uint.MaxValue && (room.CombinedPlayerAttributes != 0 || room.IsEmpty)))
                {
                    return room;
                }

                // case where the player attributes is not #FFFFFFFF
                var suitable = room.Players.Aggregate(true, (current, p) => current & (p.PlayerAttributes != player.PlayerAttributes));

                if (suitable)
                {
                    return room;
                }
            }

            return null;
        }

        /// <summary>
        /// Add a new game room with given properties.
        /// </summary>
        /// <param name="player">The player.</param>
        /// <returns>
        /// Return a new game room that just been created.
        /// </returns>
        private GameRoom AddRoom(Player player)
        {
            var newRoom = new GameRoom
                              {
                                  Id = Guid.NewGuid().ToString(),
                                  MinimumPlayers = player.MinimumPlayers,
                                  MaximumPlayers = player.MaximumPlayers,
                                  PlayerGroup = player.PlayerGroup,
                                  ApplicationId = player.ApplicationId
                              };
            this.rooms.Add(newRoom);

            Console.WriteLine("New room ({0}) added, current open room count: {1}", newRoom.Id, this.rooms.Count);

            return newRoom;
        }

        /// <summary>
        /// Send matchmaking progress info back to the client.
        /// </summary>
        /// <param name="progressInfo">Progress info.</param>
        /// <param name="room">Game room.</param>
        /// <param name="player">Player just added or removed to/from the game room.</param>
        private void SendMatchmakingProgress(MatchmakingOperation operation, GameRoom room, Player player)
        {
            var character = this.network.GetCharacterForArbitrationSession(player.Id);
            var playerName = character != null ? character.Name : "unknown";
            var progress = new MatchmakingProgressEvent(operation, playerName);

            // Notify all players in the room
            foreach (var p in room.Players)
            {
                this.network.SendServerArbitrationEvent(p.Id, MatchmakingEventSet.Serialize(progress));
            }
        }

        /// <summary>
        /// Remove a specific user from a specific room.
        /// </summary>
        /// <param name="room">Game room.</param>
        /// <param name="player">A given player.</param>
        private void RemoveUser(GameRoom room, Player player)
        {
            lock (this.roomLock)
            {
                Console.WriteLine("Remove user from room ({0})", room.Id);
                room.RemovePlayer(player);
                this.requests.Remove(player.Id);
                this.SendMatchmakingProgress(MatchmakingOperation.PlayerRemoved, room, player);
            }
        }
    }
}
