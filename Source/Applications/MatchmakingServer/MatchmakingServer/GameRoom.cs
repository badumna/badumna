﻿//-----------------------------------------------------------------------
// <copyright file="GameRoom.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MatchmakingServer
{
    using System.Collections.Generic;

    /// <summary>
    /// A game room representation used for allocating match, and will be soon removed as the match started.
    /// </summary>
    internal class GameRoom
    {
        /// <summary>
        /// List of players.
        /// </summary>
        private readonly List<Player> players = new List<Player>();

        /// <summary>
        /// Initializes a new instance of the <see cref="GameRoom"/> class.
        /// </summary>
        public GameRoom()
        {
            this.Open = true;
        }

        /// <summary>
        /// Gets or sets the room unique id.
        /// </summary>
        public string Id { get; internal set; }

        /// <summary>
        /// Gets or sets the minimum number of players on the room.
        /// </summary>
        public int MinimumPlayers { get; internal set; }

        /// <summary>
        /// Gets or sets the maximum number of players on the room.
        /// </summary>
        public int MaximumPlayers { get; internal set; }

        /// <summary>
        /// Gets or sets the bit mask ORed together of all players in the room.
        /// </summary>
        public uint CombinedPlayerAttributes { get; internal set; }

        /// <summary>
        /// Gets or sets the room player group information.
        /// </summary>
        public ushort PlayerGroup { get; internal set; }

        /// <summary>
        /// Gets the application name.
        /// </summary>
        public string ApplicationId { get; internal set; }

        /// <summary>
        /// Gets or sets a value indicating whether the room is open.
        /// </summary>
        public bool Open { get; internal set; }

        /// <summary>
        /// Gets the list of players.
        /// </summary>
        public List<Player> Players
        {
            get
            {
                return this.players;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the room is empty.
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                return this.players.Count == 0;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the room is full.
        /// </summary>
        public bool IsFull
        {
            get
            {
                return this.players.Count > 0 && this.players.Count == this.MaximumPlayers;
            }
        }

        /// <summary>
        /// Add new player to the room.
        /// </summary>
        /// <param name="player">New player to be added.</param>
        public void AddPlayer(Player player)
        {
            this.players.Add(player);
            this.CombinedPlayerAttributes |= player.PlayerAttributes;
        }

        /// <summary>
        /// Remove the player from the room.
        /// </summary>
        /// <param name="player">Player need to be removed.</param>
        public void RemovePlayer(Player player)
        {
            this.players.Remove(player);

            // recalculate player attribute.
            this.CombinedPlayerAttributes = 0;
            foreach (var p in this.Players)
            {
                this.CombinedPlayerAttributes |= p.PlayerAttributes;
            }

            // check if the room is empty,
            // if yes then reset all the properties.
            if (this.IsEmpty)
            {
                this.MinimumPlayers = 0;
                this.MaximumPlayers = 0;
                this.PlayerGroup = 0;
                this.ApplicationId = null;
            }
        }
    }
}
