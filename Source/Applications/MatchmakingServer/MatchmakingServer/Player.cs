﻿//-----------------------------------------------------------------------
// <copyright file="Player.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MatchmakingServer
{
    using System;
    
    /// <summary>
    /// Players store the information from the matchmaking request.
    /// </summary>
    internal class Player
    {
        /// <summary>
        /// Gets or sets the player id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the number of minimum players needed.
        /// </summary>
        public int MinimumPlayers { get; set; }        

        /// <summary>
        /// Gets or sets the maximum number of players on the room.
        /// </summary>
        public int MaximumPlayers { get; set; }

        /// <summary>
        /// Gets or sets the player attributes.
        /// </summary>
        public uint PlayerAttributes { get; set; }
        
        /// <summary>
        /// Gets or sets the player group.
        /// </summary>
        public ushort PlayerGroup { get; set; }

        /// <summary>
        /// Gets or sets the application id.
        /// </summary>
        public string ApplicationId { get; set; }
        
        /// <summary>
        /// Gets or sets the request timeout.
        /// </summary>
        public DateTime Timeout { get; set; }

        /// <summary>
        /// Check whether the other player is equal.
        /// </summary>
        /// <param name="other">Other player.</param>
        /// <returns>Return true if other player is equal.</returns>
        public bool Equals(Player other)
        {
            if (this.Id == other.Id && this.MinimumPlayers == other.MinimumPlayers
                && this.MaximumPlayers == other.MaximumPlayers && this.PlayerAttributes == other.PlayerAttributes
                && this.PlayerGroup == other.PlayerGroup && this.ApplicationId == other.ApplicationId
                && this.Timeout == other.Timeout)
            {
                return true;
            }

            return false;
        }
    }
}
