﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MatchmakingClient
{
    using System;

    /// <summary>
    /// Entry point for matchmaking client application.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Entry point.
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        internal static void Main(string[] args)
        {
            if (args.Length > 0 && args[0].ToLower() == "test")
            {
                var testDriver = new TestDriver();
                testDriver.Run();
            }
            else
            {
                var client = new MatchmakingClient(new Logger());

                while (true)
                {
                    var line = Console.In.ReadLine();

                    if (line != null && line.Equals("exit"))
                    {
                        client.Stop();
                        break;
                    }

                    client.RunCommand(line);
                }
            }
        }
    }
}
