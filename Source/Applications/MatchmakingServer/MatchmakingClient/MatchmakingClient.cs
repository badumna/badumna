﻿//-----------------------------------------------------------------------
// <copyright file="MatchmakingClient.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MatchmakingClient
{
    using System;
    using System.Threading;

    using Badumna;
    using Badumna.Matchmaking;

    /// <summary>
    /// Matchmaking client has a main functionality to connect to matchmaking server and requesting matchmaking.
    /// </summary>
    internal class MatchmakingClient
    {
        /// <summary>
        /// An instance of logger class.
        /// </summary>
        private readonly Logger logger;

        /// <summary>
        /// The default app id.
        /// </summary>
        private const string defaultAppId = "matchmaking_test";

        /// <summary>
        /// Badumna network.
        /// </summary>
        private INetworkFacade network;

        /// <summary>
        /// A value indicating whether the client is running.
        /// </summary>
        private bool isRunning;

        /// <summary>
        /// Badumna matchmaker.
        /// </summary>
        private MatchmakingRequest request;

        /// <summary>
        /// Application id.
        /// </summary>
        private string appId;

        /// <summary>
        /// Initializes a new instance of the <see cref="MatchmakingClient"/> class.
        /// </summary>
        /// <param name="logger">Event logger.</param>
        public MatchmakingClient(Logger logger)
            : this(logger, defaultAppId)
        {            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MatchmakingClient"/> class.
        /// </summary>
        /// <param name="logger">Event logger.</param>
        /// <param name="applicationId">Application id.</param>
        public MatchmakingClient(Logger logger, string applicationId)
        {
            this.logger = logger;
            this.appId = applicationId;
            this.SetupBadumna();

            var thread = new Thread(this.RunLoop);
            thread.Start();
        }

        /// <summary>
        /// Gets the event logger.
        /// </summary>
        internal Logger Logger
        {
            get
            {
                return this.logger;
            }
        }

        /// <summary>
        /// Run the incoming command either from the user or from the test case.
        /// </summary>
        /// <param name="command">Client command.</param>
        public void RunCommand(string command)
        {
            var split = command.Split(';');
            string[] args = null;
            if (split.Length > 1)
            {
                args = new string[split.Length - 1];
                for (var i = 1; i < split.Length; i++)
                {
                    args[i - 1] = split[i];
                }
            }

            command = split[0];

            switch (command)
            {
                case "shutdown":
                    this.Stop();
                    break;
                case "findmatch":
                    uint playerAttributes = 0;                    
                    if (args != null)
                    {
                        switch (args.Length)
                        {
                            case 4:
                                playerAttributes = Convert.ToUInt32(args[3], 16);
                                break;
                        }
                        
                        this.RequestMatch(short.Parse(args[0]), short.Parse(args[1]), ushort.Parse(args[2]), playerAttributes);
                    }

                    break;
            }
        }

        /// <summary>
        /// Stopping the matchmaking client and shutdown the network.
        /// </summary>
        internal void Stop()
        {
            this.isRunning = false;

            if (this.request != null)
            {
                this.request.OnSuccess -= this.OnSuccess;
                this.request.OnError -= this.OnError;
                this.request.Progress -= this.Progress;
            }

            this.network.Shutdown();
        }

        /// <summary>
        /// Setup/initialize Badumna network.
        /// </summary>
        private void SetupBadumna()
        {
            this.network = NetworkFacade.Create(this.appId);

            // Login to Badumna
            this.network.Login();
            this.logger.Trace("Logged in...");
        }

        /// <summary>
        /// Run loop, calling Process network state with rate of 30fps.
        /// </summary>
        private void RunLoop()
        {
            this.isRunning = true;

            while (this.isRunning)
            {
                this.network.ProcessNetworkState();
                Thread.Sleep(33);
            }
        }

        /// <summary>
        /// OnSuccess will be called when the matchmaking is succeeded.
        /// </summary>
        /// <param name="roomId">Match room id.</param>
        /// <param name="expectedPlayers">The number of expected players on the match.</param>
        /// <param name="playerId">Local player id on the match.</param>
        private void OnSuccess(string roomId, int expectedPlayers, int playerId)
        {
            this.logger.Trace("Join Room number {0}, expected players : {1}, player id : {2}", roomId, expectedPlayers, playerId);            
        }

        /// <summary>
        /// OnError will be called when the error is occurred when requesting a match.
        /// </summary>
        /// <param name="errorMessage">Given error message.</param>
        private void OnError(string errorMessage)
        {
            this.logger.Trace(errorMessage);
        }

        /// <summary>
        /// Progress will be called when there is a new progress status from the matchmaker.
        /// </summary>
        /// <param name="progressInfo">Progress information.</param>
        private void Progress(string progressInfo)
        {
            this.logger.Verbose(progressInfo);
        }

        /// <summary>
        /// Sending a matchmaking request.
        /// </summary>
        /// <param name="minPlayers">Number of minimum players.</param>
        /// <param name="maxPlayers">Number of maximum players.</param>
        /// <param name="playerGroup">Player group.</param>
        /// <param name="playerAttributes">Player attributes.</param>
        private void RequestMatch(short minPlayers, short maxPlayers, ushort playerGroup, uint playerAttributes)
        {
            if (this.request == null)
            {
                this.request = this.network.GetMatchmakingRequest();
                this.request.OnSuccess += this.OnSuccess;
                this.request.OnError += this.OnError;
                this.request.Progress += this.Progress;
            }

            this.request.MinimumPlayers = minPlayers;
            this.request.MaximumPlayers = maxPlayers;
            this.request.PlayerGroup = playerGroup;
            this.request.PlayerAttributes = playerAttributes;
            this.request.RequestMatch();
        }        
    }
}
