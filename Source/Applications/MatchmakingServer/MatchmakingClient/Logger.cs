﻿//-----------------------------------------------------------------------
// <copyright file="Logger.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MatchmakingClient
{
    using System;

    /// <summary>
    /// New log message delegate.
    /// </summary>
    /// <param name="message">New log message.</param>
    internal delegate void NewLlogMessage(string message);

    /// <summary>
    /// Logger class is used to print out the log into console and notify all the NewLogEvent subscriber 
    /// when there is a new log message.
    /// </summary>
    internal class Logger
    {
        /// <summary>
        /// New log event.
        /// </summary>
        public event NewLlogMessage NewLogEvent;

        /// <summary>
        /// Trace log to console and notify the NewLogEvent subscriber.
        /// </summary>
        /// <param name="format">Log message format.</param>
        /// <param name="args">Log argument parameters.</param>
        public void Trace(string format, params object[] args)
        {
            var message = string.Format(format, args);

            if (this.NewLogEvent != null)
            {
                this.NewLogEvent.Invoke(message);
            }

            Console.Out.WriteLine(message);
        }

        /// <summary>
        /// Trace log to console but do not notify the NewLogEvent subscriber.
        /// </summary>
        /// <param name="format">Log message format.</param>
        /// <param name="args">Log argument parameters.</param>
        public void Verbose(string format, params object[] args)
        {
            var message = string.Format(format, args);
            Console.Out.WriteLine(message);
        }
    }
}
