﻿//-----------------------------------------------------------------------
// <copyright file="TestDriver.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MatchmakingClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    /// <summary>
    /// Test driver has responsibility to load all the test cases and run them.
    /// </summary>
    internal class TestDriver
    {
        /// <summary>
        /// List of test cases, all types derived from TestCase.
        /// </summary>
        private readonly List<TestCase> testcases = new List<TestCase>();

        /// <summary>
        /// Number of test that succeeded.
        /// </summary>
        private int testSucceed;

        /// <summary>
        /// Number of failed tests.
        /// </summary>
        private int testFailed;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestDriver"/> class.
        /// </summary>
        public TestDriver()
        {
            // load the test cases
            this.testcases = (from type in Assembly.GetExecutingAssembly().GetTypes()
                              where type.BaseType == typeof(TestCase) && type.GetConstructor(Type.EmptyTypes) != null
                              select (TestCase)Activator.CreateInstance(type)).ToList();
        }

        /// <summary>
        /// Run the test cases.
        /// </summary>
        public void Run()
        {
            foreach (var testcase in this.testcases)
            {
                Console.Out.WriteLine("=============================================================================");
                testcase.Run();
                Console.Out.WriteLine("=============================================================================");

                if (testcase.Succeed)
                {
                    this.testSucceed++;
                }
                else
                {
                    this.testFailed++;
                }
            }

            // Summary
            Console.Out.WriteLine("Test succeed : {0}, test failed : {1}, total test : {2}", this.testSucceed, this.testFailed, this.testcases.Count);
        }
    }
}
