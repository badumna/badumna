﻿//-----------------------------------------------------------------------
// <copyright file="Test.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MatchmakingClient
{
    using System;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading;
    
    /// <summary>
    /// The actual test.
    /// </summary>
    internal class Test
    {
        /// <summary>
        /// Result lock object.
        /// </summary>
        private readonly object resultLock = new object();

        /// <summary>
        /// Matchmaking client instance.
        /// </summary>
        private readonly MatchmakingClient client;

        /// <summary>
        /// Test commands.
        /// </summary>
        private readonly string[] commands;

        /// <summary>
        /// Expected responses.
        /// </summary>
        private readonly string[] responses;

        /// <summary>
        /// Test results for each command.
        /// </summary>
        private readonly bool[] results;

        /// <summary>
        /// The command counter.
        /// </summary>
        private int counter;

        /// <summary>
        /// A value indicating whether the test should proceed to the next command.
        /// </summary>
        private bool nextCommand;

        /// <summary>
        /// Initializes a new instance of the <see cref="Test"/> class.
        /// </summary>
        /// <param name="client">Matchmaking client.</param>
        /// <param name="commands">Test commands.</param>
        /// <param name="responses">Expected responses.</param>
        public Test(MatchmakingClient client, string[] commands, string[] responses)
        {
            this.client = client;
            this.commands = commands;
            this.responses = responses;

            this.client.Logger.NewLogEvent += this.NewLogEventHandler;
            this.results = new bool[this.commands.Length];
        }

        /// <summary>
        /// Gets a value indicating whether this test is succeed.
        /// </summary>
        public bool TestSucceed
        {
            get
            {
                return this.results.All(r => r);
            }
        }

        /// <summary>
        /// Run the test.
        /// </summary>
        public void Run()
        {
            for (int i = 0; i < this.commands.Length; i++)
            {
                this.counter = i;
                this.client.RunCommand(this.commands[i]);

                var wait = true;

                if (string.IsNullOrEmpty(this.responses[i]))
                {
                    // if no response is expected means do not wait,
                    // do the next command immediately.
                    wait = false;
                    this.results[i] = true;
                }

                while (wait)
                {
                    Thread.Sleep(50);
                    if (this.nextCommand)
                    {
                        wait = false;
                        this.nextCommand = false;

                        if (!this.results[i])
                        {
                            // Immediately stop the test when it failed.
                            this.UnsubscribeLogEvent();
                            return;
                        }
                    }
                }
            }

            this.UnsubscribeLogEvent();
        }

        /// <summary>
        /// NewLogEvent handler will be called when there is a new log message.
        /// </summary>
        /// <param name="message">New log message.</param>
        private void NewLogEventHandler(string message)
        {
            var regex = new Regex(this.responses[this.counter]);
            bool result = regex.IsMatch(message);

            lock (this.resultLock)
            {
                this.results[this.counter] = result;
            }

            if (!result)
            {
                Console.Out.WriteLine("Expected : {0}, Actual : {1}", this.responses[this.counter], message);
            }

            this.nextCommand = true;
        }

        /// <summary>
        /// Unsubscribe from the client logger when the test is finished.
        /// </summary>
        private void UnsubscribeLogEvent()
        {
            this.client.Logger.NewLogEvent -= this.NewLogEventHandler;
        }
    }
}
