﻿// -----------------------------------------------------------------------
// <copyright file="MultipleRequestShouldOverridePreviousRequestTest.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace MatchmakingClient.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;

    /// <summary>
    /// Multiple request should override the previous one if the new request different.
    /// </summary>
    internal class MultipleRequestShouldOverridePreviousRequestTest : TestCase
    {
        /// <summary>
        /// Test threads.
        /// </summary>
        private readonly List<Thread> threads = new List<Thread>();

        /// <summary>
        /// First client.
        /// </summary>
        private MatchmakingClient client1;

        /// <summary>
        /// Second client.
        /// </summary>
        private MatchmakingClient client2;

        /// <summary>
        /// Run the test case.
        /// </summary>
        public override void Run()
        {
            Console.Out.WriteLine("MultipleRequestShouldOverridePreviousRequestTest");

            var currentTests = new List<Test>();

            // Reused room when room is empty and the new request have different properties
            // (e.g. min and max player property are set to different value). 
            this.client1 = new MatchmakingClient(new Logger());
            this.client2 = new MatchmakingClient(new Logger());

            // Timeout tests.
            var commands = new[] { Resources.findmatch + ";3;3;0", Resources.findmatch + ";2;3;0" };
            var responses = new[] { string.Empty, string.Format(Resources.joinRoom, 2) };
            var test1 = new Test(this.client1, commands, responses);

            commands = new[] { Resources.findmatch + ";2;3;0" };
            responses = new[] { string.Format(Resources.joinRoom, 2) };
            var test2 = new Test(this.client2, commands, responses);

            this.tests.Add(test1);
            this.tests.Add(test2);

            currentTests.Add(test1);
            currentTests.Add(test2);

            foreach (var thread in currentTests.Select(test => new Thread(test.Run)))
            {
                thread.Start();

                this.threads.Add(thread);
            }

            while (true)
            {
                var isRunning = this.threads.Aggregate(false, (current, t) => current | t.IsAlive);

                if (!isRunning)
                {
                    break;
                }

                Thread.Sleep(100);
            }

            this.client1.Stop();
            this.client2.Stop();
        }
    }
}
