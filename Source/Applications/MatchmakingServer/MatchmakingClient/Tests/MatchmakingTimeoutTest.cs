﻿//-----------------------------------------------------------------------
// <copyright file="MatchmakingTimeoutTest.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MatchmakingClient.Tests
{
    using System;

    /// <summary>
    /// Matchmaking timeout when there is only one peer requesting match.
    /// </summary>
    internal class MatchmakingTimeoutTest : TestCase
    {
        /// <summary>
        /// Matchmaking client.
        /// </summary>
        private MatchmakingClient client;

        /// <summary>
        /// Run test case.
        /// </summary>
        public override void Run()
        {
            Console.Out.WriteLine("MatchmakingTimeoutTest");

            this.client = new MatchmakingClient(new Logger());
            this.tests.Add(new Test(this.client, new[] { Resources.findmatch + ";2;2;0" }, new[] { Resources.matchmakingTimeout }));

            foreach (var test in this.tests)
            {
                test.Run();
            }

            this.client.Stop();            
        }
    }
}
