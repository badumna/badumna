﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StartMatchWhenPlayerAttributesIsCompleteTest.cs" company="Scalify">
//   Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace MatchmakingClient.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;

    /// <summary>
    /// Should start match when the player attribute masking is complete.
    /// </summary>
    internal class StartMatchWhenPlayerAttributesIsCompleteTest : TestCase
    {
        /// <summary>
        /// Test threads.
        /// </summary>
        private readonly List<Thread> threads = new List<Thread>();

        /// <summary>
        /// First client.
        /// </summary>
        private MatchmakingClient client1;

        /// <summary>
        /// Second client.
        /// </summary>
        private MatchmakingClient client2;

        /// <summary>
        /// Run the test case.
        /// </summary>
        public override void Run()
        {
            Console.Out.WriteLine("StartMatchWhenPlayerAttributesIsCompleteTest");

            this.client1 = new MatchmakingClient(new Logger());
            this.client2 = new MatchmakingClient(new Logger());

            var commands = new[] { Resources.findmatch + ";2;3;0;0xFFFF0000" };
            var responses = new[] { string.Format(Resources.joinRoom, 2) };
            this.tests.Add(new Test(this.client1, commands, responses));

            commands = new[] { Resources.findmatch + ";2;3;0;0x0000FFFF" };
            this.tests.Add(new Test(this.client2, commands, responses));

            foreach (var thread in this.tests.Select(test => new Thread(test.Run)))
            {
                thread.Start();
                this.threads.Add(thread);
            }

            while (true)
            {
                var isRunning = this.threads.Aggregate(false, (current, t) => current | t.IsAlive);

                if (!isRunning)
                {
                    break;
                }

                Thread.Sleep(100);
            }

            this.client1.Stop();
            this.client2.Stop();
        }
    }
}
