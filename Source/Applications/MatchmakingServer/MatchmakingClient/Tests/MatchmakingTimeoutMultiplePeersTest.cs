﻿//-----------------------------------------------------------------------
// <copyright file="MatchmakingTimeoutMultiplePeersTest.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MatchmakingClient.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;

    /// <summary>
    /// Matchmaking request should timeout for multiple peers when the room capacity is still less 
    /// then the given min players.
    /// </summary>
    internal class MatchmakingTimeoutMultiplePeersTest : TestCase
    {
        /// <summary>
        /// Test threads.
        /// </summary>
        private readonly List<Thread> threads = new List<Thread>();

        /// <summary>
        /// First client.
        /// </summary>
        private MatchmakingClient client1;

        /// <summary>
        /// Second client.
        /// </summary>
        private MatchmakingClient client2;

        /// <summary>
        /// Run the test case.
        /// </summary>
        public override void Run()
        {
            Console.Out.WriteLine("MatchmakingTimeoutMultiplePeersTest");

            // In case room is still less then the min players, when timeout, notify the user.
            this.client1 = new MatchmakingClient(new Logger());
            this.client2 = new MatchmakingClient(new Logger());

            var commands = new[] { Resources.findmatch + ";3;4;0" };
            var responses = new[] { Resources.matchmakingTimeout };
            this.tests.Add(new Test(this.client1, commands, responses));
            this.tests.Add(new Test(this.client2, commands, responses));

            foreach (var thread in this.tests.Select(test => new Thread(test.Run)))
            {
                thread.Start();

                this.threads.Add(thread);
            }

            while (true)
            {
                var isRunning = this.threads.Aggregate(false, (current, t) => current | t.IsAlive);

                if (!isRunning)
                {
                    break;
                }

                Thread.Sleep(100);
            }

            this.client1.Stop();
            this.client2.Stop();
        }
    }
}
