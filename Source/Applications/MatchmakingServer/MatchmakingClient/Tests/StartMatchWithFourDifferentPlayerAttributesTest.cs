﻿// -----------------------------------------------------------------------
// <copyright file="StartMatchWithFourDifferentPlayerAttributesTest.cs" company="Scalify">
//   Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MatchmakingClient.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;

    /// <summary>
    /// Start match only when four different player with four different roles are found. 
    /// </summary>
    internal class StartMatchWithFourDifferentPlayerAttributesTest : TestCase
    {
        /// <summary>
        /// Test threads.
        /// </summary>
        private readonly List<Thread> threads = new List<Thread>();

        /// <summary>
        /// Clients collection.
        /// </summary>
        private readonly List<MatchmakingClient> clients = new List<MatchmakingClient>(4);

        /// <summary>
        /// Four different player attributes.
        /// </summary>
        private readonly string[] playersAttributes = new[] { "0xFF000000", "0x00FF0000", "0x0000FF00", "0x000000FF" };
        
        /// <summary>
        /// Run the test case.
        /// </summary>
        public override void Run()
        {
            Console.Out.WriteLine("StartMatchWithFourDifferentPlayerAttributesTest");

            var responses = new[] { string.Format(Resources.joinRoom, 4) };

            for (var i = 0; i < this.clients.Capacity; i++)
            {
                var client = new MatchmakingClient(new Logger());                
                this.clients.Add(client);

                var commands = new[] { Resources.findmatch + ";4;4;0;" + this.playersAttributes[i] };
                this.tests.Add(new Test(client, commands, responses));
            }            

            foreach (var thread in this.tests.Select(test => new Thread(test.Run)))
            {
                thread.Start();
                this.threads.Add(thread);
            }

            while (true)
            {
                var isRunning = this.threads.Aggregate(false, (current, t) => current | t.IsAlive);

                if (!isRunning)
                {
                    break;
                }

                Thread.Sleep(100);
            }

            foreach (var client in this.clients)
            {
                client.Stop();
            }
        }
    }
}
