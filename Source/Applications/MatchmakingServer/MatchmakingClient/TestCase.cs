﻿//-----------------------------------------------------------------------
// <copyright file="TestCase.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MatchmakingClient
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Base test case class, all the test case should be derived from this class.
    /// The test driver using reflection to load all the test cases.
    /// </summary>
    internal class TestCase
    {
        /// <summary>
        /// List of test instances.
        /// </summary>
        protected List<Test> tests = new List<Test>();

        /// <summary>
        /// Gets a value indicating whether the test case is succeed.
        /// </summary>
        public bool Succeed
        {
            get
            {
                return this.tests.Aggregate(true, (current, test) => current & test.TestSucceed);
            }
        }

        /// <summary>
        /// Run the test case.
        /// </summary>
        public virtual void Run()
        {
        }
    }
}
