﻿//-----------------------------------------------------------------------
// <copyright file="UserInput.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace ValidationDemo
{
    using System.Diagnostics;
    using System.Text;
    using Badumna.Validation;

    /// <summary>
    /// Represents user input for the player.
    /// </summary>
    public class UserInput : IUserInput
    {
        /// <summary>
        /// A flag indicating whether the up button is currently pressed.
        /// </summary>
        private bool up;

        /// <summary>
        /// A flag indicating whether the down button is currently pressed.
        /// </summary>
        private bool down;

        /// <summary>
        /// A flag indicating whether the left button is currently pressed.
        /// </summary>
        private bool left;

        /// <summary>
        /// A flag indicating whether the right button is currently pressed.
        /// </summary>
        private bool right;

        /// <summary>
        /// A flag indicating whether the action button has just been pressed.
        /// </summary>
        private bool action;

        /// <summary>
        /// A flag indicating whether the colour change button has just been pressed.
        /// </summary>
        private bool colour;

        /// <summary>
        /// Initializes a new instance of the UserInput class.
        /// </summary>
        /// <param name="up">A flag indicating whether the up button is pressed.</param>
        /// <param name="down">A flag indicating whether the down button is pressed.</param>
        /// <param name="left">A flag indicating whether the left button is pressed.</param>
        /// <param name="right">A flag indicating whether the right button is pressed.</param>
        /// <param name="action">A flag indicating whether the action button has just been pressed.</param>
        /// <param name="colour">A flag indicating whether the colour change button has just been pressed.</param>
        public UserInput(bool up, bool down, bool left, bool right, bool action, bool colour)
        {
            this.up = up;
            this.down = down;
            this.left = left;
            this.right = right;
            this.action = action;
            this.colour = colour;
        }

        /// <summary>
        /// Initializes a new instance of the UserInput class.
        /// </summary>
        /// <param name="bytes">A byte array containing serialized input data.</param>
        public UserInput(byte[] bytes)
        {
            this.FromBytes(bytes);
        }

        /// <summary>
        /// Gets a value indicating whether the up button is pressed.
        /// </summary>
        public bool Up
        {
            get { return this.up; }
        }

        /// <summary>
        /// Gets a value indicating whether the down button is pressed.
        /// </summary>
        public bool Down
        {
            get { return this.down; }
        }

        /// <summary>
        /// Gets a value indicating whether the left button is pressed.
        /// </summary>
        public bool Left
        {
            get { return this.left; }
        }

        /// <summary>
        /// Gets a value indicating whether the right button is pressed.
        /// </summary>
        public bool Right
        {
            get { return this.right; }
        }

        /// <summary>
        /// Gets a value indicating whether the action button has just been pressed.
        /// </summary>
        public bool Action
        {
            get { return this.action; }
        }

        /// <summary>
        /// Gets a value indicating whether the colour change button has just been pressed.
        /// </summary>
        public bool Colour
        {
            get { return this.colour; }
        }

        /// <inheritdoc/>
        public byte[] ToBytes()
        {
            byte data = 0;
            if (this.up)
            {
                data |= 1;
            }

            if (this.down)
            {
                data |= 2;
            }

            if (this.left)
            {
                data |= 4;
            }

            if (this.right)
            {
                data |= 8;
            }

            if (this.action)
            {
                data |= 16;
            }

            if (this.colour)
            {
                data |= 32;
            }

            return new byte[1] { data };
        }

        /// <summary>
        /// Initializes this instance with data deserialized from a byte array.
        /// </summary>
        /// <param name="bytes">The serialized data.</param>
        public void FromBytes(byte[] bytes)
        {
            Debug.Assert(bytes.Length == 1, "Serialized user input should be one byte long.");
            byte data = bytes[0];
            this.up = (data & 1) > 0;
            this.down = (data & 2) > 0;
            this.left = (data & 4) > 0;
            this.right = (data & 8) > 0;
            this.action = (data & 16) > 0;
            this.colour = (data & 32) > 0;
        }
    }
}
