﻿//-----------------------------------------------------------------------
// <copyright file="Player.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using Badumna;
using Badumna.Validation;
using Microsoft.Xna.Framework;
using BVector3 = Badumna.DataTypes.Vector3;

namespace ValidationDemo
{
    /// <summary>
    /// Delegate type for action event handlers. 
    /// </summary>
    public delegate void ActionHandler();

    /// <summary>
    /// Represents the player avatar.
    /// </summary>
    public class Player : ValidatedEntity
    {
        /// <summary>
        /// Speed the player will move at.
        /// </summary>
        public const float MoveSpeed = 50f;

        /// <summary>
        /// The value to use as the interest radius.
        /// </summary>
        public const float InterestRadius = 100f;

        /// <summary>
        /// A dictionary mapping integer colour codes to XNA colours.
        /// </summary>
        private static readonly Dictionary<int, Color> colourTable = new Dictionary<int, Color>()
        {
            { 0, Color.Red },
            { 1, Color.Orange },
            { 2, Color.Yellow },
            { 3, Color.Green },
            { 4, Color.Cyan },
            { 5, Color.Blue },
            { 6, Color.BlueViolet }
        };

        /// <summary>
        /// A value indicating whether the action key was pressed during the last update.
        /// </summary>
        private bool actionPressed = false;

        /// <summary>
        /// A value indicating whether the colour key was pressed during the last update.
        /// </summary>
        private bool colourPressed = false;

        /// <summary>
        /// Initializes a new instance of the Player class.
        /// </summary>
        /// <param name="networkFacade">The network facade.</param>
        public Player(INetworkFacade networkFacade)
            : this(networkFacade, 100f, 100f)
        {
        }

        /// <summary>
        /// Initializes a new instance of the Player class.
        /// </summary>
        /// <param name="networkFacade">The network facade.</param>
        /// <param name="x">The initial x coordinate.</param>
        /// <param name="y">The initial y coordinate.</param>
        public Player(INetworkFacade networkFacade, float x, float y)
            : base(networkFacade)
        {
            this.Position = new BVector3(x, y, 0);
            this.Radius = 10f;
            this.AreaOfInterestRadius = Player.InterestRadius;
        }

        /// <summary>
        /// An event triggered when the action button is pressed.
        /// </summary>
        public event ActionHandler ActionTriggered;

        /// <summary>
        /// Gets a dictionary mapping integer colour codes to XNA colours.
        /// </summary>
        public static IDictionary<int, Color> ColourTable
        {
            get { return Player.colourTable; }
        }

        /// <summary>
        /// Gets the player position as an XNA Vector2 for convenience.
        /// </summary>
        public Vector2 XnaPosition
        {
            get
            {
                return new Vector2(this.Position.X, this.Position.Y);
            }
        }

        /// <summary>
        /// An integer representing the colour to render the player in.
        /// </summary>
        /// <remarks>
        /// The colour can be looked up using this value in the colour table.</remarks>
        [Replicable]
        public int Colour
        {
            get;
            set;
        }

        /// <summary>
        /// Handle events received.
        /// </summary>
        /// <param name="stream">The strean to read the event details from.</param>
        public override void HandleEvent(Stream stream)
        {
            // There is only one event in this demo so nothing needs to be read from the stream
            // to work out which action to trigger.
            this.TriggerAction();
        }

        /// <summary>
        /// Update the player.
        /// </summary>
        /// <remarks>
        /// The player updates should depend upon current replicated state, user input, and time interval only.
        /// </remarks>
        /// <param name="interval">The time interval for this update.</param>
        /// <param name="userInput">Serialized user input.</param>
        /// <param name="mode">The update mode.</param>
        public override void Update(TimeSpan interval, byte[] userInput, UpdateMode mode)
        {
            UserInput input = new UserInput(userInput);
            if (userInput == null)
            {
                throw new ArgumentException("input must be non-null instance of UserInput.");
            }

            float x = this.Position.X;
            float y = this.Position.Y;
            if (input.Up && !input.Down)
            {
                y -= (float)interval.TotalSeconds * Player.MoveSpeed;
            }
            else if (input.Down && !input.Up)
            {
                y += (float)interval.TotalSeconds * Player.MoveSpeed;
            }

            if (input.Left && !input.Right)
            {
                x -= (float)interval.TotalSeconds * Player.MoveSpeed;
            }
            else if (input.Right && !input.Left)
            {
                x += (float)interval.TotalSeconds * Player.MoveSpeed;
            }

            this.Position = new BVector3(x, y, 0);

            if (input.Colour && !this.colourPressed)
            {
                this.Colour++;
                this.Colour = this.Colour % ColourTable.Count;
            }

            this.colourPressed = input.Colour;

            if (input.Action && !this.actionPressed)
            {
                if (mode == UpdateMode.Prediction)
                {
                    this.TriggerAction();
                }
                else if (mode == UpdateMode.Authoritative)
                {
                    this.NetworkFacade.SendCustomMessageToRemoteCopies(this, new MemoryStream());
                }
            }

            this.actionPressed = input.Action;
        }

        /// <summary>
        /// Trigger the action.
        /// </summary>
        private void TriggerAction()
        {
            ActionHandler handler = this.ActionTriggered;
            if (handler != null)
            {
                handler.Invoke();
            }
        }
    }
}
