//-----------------------------------------------------------------------
// <copyright file="Game1.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using Badumna;
using Badumna.DataTypes;
using Badumna.SpatialEntities;
using Badumna.Validation;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace ValidationDemo
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        /// <summary>
        /// Integer assigned to identify the player entity type.
        /// </summary>
        private readonly uint playerEntityType = 7;

        /// <summary>
        /// Graphics device.
        /// </summary>
        private GraphicsDeviceManager graphics;

        /// <summary>
        /// Sprite batch for rendering sprites.
        /// </summary>
        private SpriteBatch spriteBatch;
        
        /// <summary>
        /// A font for writing status info on screen.
        /// </summary>
        private SpriteFont font;

        /// <summary>
        /// String for displaying status messages on screen.
        /// </summary>
        private string status = "Uninitialized";

        /// <summary>
        /// The network facade.
        /// </summary>
        private INetworkFacade networkFacade;

        /// <summary>
        /// A value indicating whether Badumna startup, log in, and player creation have completed.
        /// </summary>
        private bool started = false;

        /// <summary>
        /// A value indicating whether exiting has been queued.
        /// </summary>
        private bool exitQueued = false;

        /// <summary>
        /// A sound effect to indicate actions by the local player.
        /// </summary>
        private SoundEffect localBleep;

        /// <summary>
        /// A sound effect to indicate actions by remote players.
        /// </summary>
        private SoundEffect remoteBleep;

        /// <summary>
        /// The network scene.
        /// </summary>
        private NetworkScene scene;
        
        /// <summary>
        /// The local player.
        /// </summary>
        private Player player;

        /// <summary>
        /// The remote players currently being replicated to this peer.
        /// </summary>
        private List<Player> remotePlayers = new List<Player>();

        /// <summary>
        /// A texture for drawing players.
        /// </summary>
        private Texture2D playerTexture;

        /// <summary>
        /// Origin to use for the player sprite when drawing.
        /// </summary>
        private Vector2 spriteOrigin;

        /// <summary>
        /// Flag indicating whether colour key is pressed (used to detect keydown events).
        /// </summary>
        private KeyboardState oldKeyboardState;

        /// <summary>
        /// Initializes a new instance of the Game1 class.
        /// </summary>
        public Game1()
        {
            this.graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            this.IsMouseVisible = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            this.StartBadumnaAsynchronously();
            this.oldKeyboardState = Keyboard.GetState();
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            this.spriteBatch = new SpriteBatch(GraphicsDevice);
            this.font = Content.Load<SpriteFont>("font");

            // Load game assets.
            this.playerTexture = Content.Load<Texture2D>("avatar");
            this.localBleep = Content.Load<SoundEffect>("Bleep1");
            this.remoteBleep = Content.Load<SoundEffect>("Bleep2");

            this.spriteOrigin = new Vector2(this.playerTexture.Width, 0) / 2;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // No non ContentManager content to unload.
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Shut down and exit if exit is queued.
            if (this.exitQueued)
            {
                this.started = false;
                this.networkFacade.Shutdown(false);
                this.Exit();
            }

            // Allows the game to exit
            KeyboardState newKeyboardState = Keyboard.GetState();
            if (newKeyboardState.IsKeyDown(Keys.Q))
            {
                // Queue exit for next loop, to allow status update to be drawn first.
                this.status = "Quitting.";
                this.exitQueued = true;
            }

            if (this.started)
            {
                // Don't do anything if the player is not registered.
                if (this.player.Status != EntityStatus.Paused)
                {
                    // Get user input.
                    UserInput input = new UserInput(
                        newKeyboardState.IsKeyDown(Keys.Up),
                        newKeyboardState.IsKeyDown(Keys.Down),
                        newKeyboardState.IsKeyDown(Keys.Left),
                        newKeyboardState.IsKeyDown(Keys.Right),
                        newKeyboardState.IsKeyDown(Keys.Enter) && !this.oldKeyboardState.IsKeyDown(Keys.Enter),
                        newKeyboardState.IsKeyDown(Keys.C) && !this.oldKeyboardState.IsKeyDown(Keys.C));

                    // Update the player.
                    this.networkFacade.ValidationFacade.UpdateEntity(
                        this.player,
                        gameTime.ElapsedGameTime,
                        input.ToBytes());
                }

                // Trigger Badumna regular processing
                this.networkFacade.ProcessNetworkState();
            }

            this.oldKeyboardState = newKeyboardState;

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            this.spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);

            if (this.player != null)
            {
                // Draw the player.
                Color colour = Color.Gray;
                if (this.player.Status != EntityStatus.Paused)
                {
                    colour = Player.ColourTable[this.player.Colour];
                }

                this.spriteBatch.Draw(this.playerTexture, this.player.XnaPosition, null, colour, 0, this.spriteOrigin, 1f, SpriteEffects.None, 1f);

                // Draw the player label.
                string label = this.player.Status.ToString();
                Vector2 fontOrigin = new Vector2(this.font.MeasureString(label).X / 2, this.playerTexture.Height);
                this.spriteBatch.DrawString(this.font, label, this.player.XnaPosition, Color.White, 0, fontOrigin, 1f, SpriteEffects.None, 0f);
            }

            // Draw any remote players.
            foreach (Player remotePlayer in this.remotePlayers)
            {
                Color colour = Player.ColourTable[remotePlayer.Colour];
                this.spriteBatch.Draw(this.playerTexture, remotePlayer.XnaPosition, null, colour, 0, this.spriteOrigin, 1f, SpriteEffects.None, 1f);
            }

            // Draw status information.
            this.spriteBatch.DrawString(this.font, this.status, new Vector2(20, 440), Color.White);

            this.spriteBatch.End();
            base.Draw(gameTime);
        }

        /// <summary>
        /// Begin to initalize Badumna and login on a separate thread.
        /// </summary>
        private void StartBadumnaAsynchronously()
        {
            this.status = "Initializing Badumna";
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += delegate { this.StartBadumna(); };
            worker.RunWorkerAsync();
        }

        /// <summary>
        /// Initialize Badumna and log in to the network.
        /// </summary>
        private void StartBadumna()
        {
            // Set up the Badumna options
            Options options = new Options();

            // Use LAN mode for testing
            options.Connectivity.ConfigureForLan();

            // To use on the Internet, comment out the ConfigureForLan() line above and
            // add a known seed peer. e.g.:
            ////options.Connectivity.SeedPeers.Add("seedpeer.example.com:21251");

            // Set the application name
            options.Connectivity.ApplicationName = "validation-demo";

            // Set the master validation server address.
            options.Validation.Servers.Add(new ValidationServerDetails("master", "example.com:21270"));

            // Initialize Badumna and log in.
            this.networkFacade = NetworkFacade.Create(options);
            this.status = "Logging in.";
            this.networkFacade.Login();
            this.status = "Logged in.";

            // Register entity types to be replicated.
            this.networkFacade.RegisterEntityDetails(
                Player.InterestRadius,
                new Badumna.DataTypes.Vector3(Player.MoveSpeed, Player.MoveSpeed, 0f).Magnitude);

            // Configure validation to accept validation requests.
            this.networkFacade.ValidationFacade.ConfigureValidator(
                () => true,
                this.CreateValidatedEntity,
                this.OnValidatedEntityRemoval);

            // Join a scene.
            this.scene = this.networkFacade.JoinScene("foo", this.CreateSpatialReplica, this.RemoveSpatialReplica);

            // Subscribe to validation events.
            this.networkFacade.ValidationFacade.ValidatorOnline += this.OnValidatorOnline;
            this.networkFacade.ValidationFacade.ValidationFailed += this.OnValidationFailure;

            // Create the player.
            this.player = new Player(this.networkFacade);
            this.player.ActionTriggered += () => { this.localBleep.Play(); };
            this.status = "Registering player.";

            // Set player's initial state
            this.player.Position = new Badumna.DataTypes.Vector3(200f, 200f, 0);
            this.player.Colour = 3;
            ISerializedEntityState initialState = this.player.SaveState();
            
            // Register the player for validation.
            this.networkFacade.ValidationFacade.RegisterValidatedEntity(this.player, this.playerEntityType, initialState);

            // Join the scene.
            this.networkFacade.ValidationFacade.RegisterEntityWithScene(this.player, this.scene);

            this.started = true;
        }

        /// <summary>
        /// Handle validator online notifications from Badumna.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void OnValidatorOnline(object sender, ValidatorOnlineEventArgs e)
        {
            this.status = "Live validating.";
        }

        /// <summary>
        /// Handle validation failure notifications from Badumna.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void OnValidationFailure(object sender, ValidationFailureEventArgs e)
        {
            if (e.Reason == ValidationFailureReason.ServerUnreachable)
            {
                this.status = "Failure: server unreachable. Retrying...";
            }
            else
            {
                this.status = "Failure: no validator available. Retrying...";
            }
        }

        /// <summary>
        /// Method for creating spatial replicas that is passed to Badumna when joining a scene.
        /// </summary>
        /// <param name="scene">The scene that the replica is from.</param>
        /// <param name="entityId">The replica's ID.</param>
        /// <param name="entityType">An integer indicating the type of entity.</param>
        /// <returns></returns>
        private ISpatialReplica CreateSpatialReplica(NetworkScene scene, BadumnaId entityId, uint entityType)
        {
            if (entityType == this.playerEntityType)
            {
                Player remotePlayer = new Player(this.networkFacade);
                remotePlayer.ActionTriggered += () => { this.remoteBleep.Play(); };
                this.remotePlayers.Add(remotePlayer);
                return remotePlayer;
            }

            return null;
        }

        /// <summary>
        /// Method for removing spatial replicas that is passed to Badumna when joining a scene.
        /// </summary>
        /// <param name="scene">The scene the entity has been removed from.</param>
        /// <param name="replica">The replica to remove.</param>
        private void RemoveSpatialReplica(NetworkScene scene, ISpatialReplica replica)
        {
            Player remotePlayer = replica as Player;
            if (remotePlayer != null)
            {
                this.remotePlayers.Remove(remotePlayer);
            }
        }

        /// <summary>
        /// Factory method for creating validated entities on validator.
        /// </summary>
        /// <param name="entityType">An integer representing the type of the entity.</param>
        /// <returns>A new validated entity.</returns>
        private IValidatedEntity CreateValidatedEntity(uint entityType)
        {
            if (entityType == this.playerEntityType)
            {
                return new Player(this.networkFacade);
            }

            return null;
        }

        /// <summary>
        /// Delegate for handling validated entity removal on validators.
        /// </summary>
        /// <param name="entity">The entity to remove.</param>
        private void OnValidatedEntityRemoval(IValidatedEntity entity)
        {
            // No clean up required.
        }
    }
}
