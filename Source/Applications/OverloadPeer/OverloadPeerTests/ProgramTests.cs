﻿//-----------------------------------------------------------------------
// <copyright file="ProgramTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace OverloadPeerTests
{
    using Badumna;
    using Badumna.Utilities;
    using GermHarness;
    using Mono.Options;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    public class ProgramTests
    {
        private IOptionSet optionSet;
        private PeerHarnessFactory peerHarnessFactory;
        private IConsoleWriter consoleWriter;

        [SetUp]
        public void SetUp()
        {
            this.optionSet = MockRepository.GenerateMock<IOptionSet>();
            this.peerHarnessFactory = MockRepository.GenerateMock<PeerHarnessFactory>();
            this.consoleWriter = MockRepository.GenerateMock<IConsoleWriter>();
        }

        [Test]
        public void ConstructorCretesPeerHarnessWithDefaultOptions()
        {
            // Expect
            this.peerHarnessFactory.Expect(f => f.Invoke(null))
                .IgnoreArguments()
                .WhenCalled(c =>
                    {
                        Options options = (Options)c.Arguments[0];
                        Assert.AreEqual(21252, options.Connectivity.StartPortRange);
                        Assert.AreEqual(21252, options.Connectivity.EndPortRange);
                        Assert.AreEqual(1, options.Connectivity.MaxPortsToTry);
                        Assert.IsTrue(options.Connectivity.IsBroadcastEnabled);
                        Assert.AreEqual(21250, options.Connectivity.BroadcastPort);
                        Assert.IsTrue(options.Connectivity.IsPortForwardingEnabled);
                        Assert.IsTrue(options.Connectivity.IsHostedService);
                        Assert.IsTrue(options.Connectivity.StunServers.Contains("stun1.noc.ams-ix.net"));
                        Assert.IsTrue(options.Connectivity.StunServers.Contains("stun.voipbuster.com"));
                        Assert.IsTrue(options.Connectivity.StunServers.Contains("stun.voxgratia.org"));
                        Assert.AreEqual(4, options.Connectivity.StunServers.Count);
                    });

            // Act
            OverloadPeer.Program program = new OverloadPeer.Program(
                this.optionSet,
                this.peerHarnessFactory,
                this.consoleWriter);

            // Verify
            this.peerHarnessFactory.VerifyAllExpectations();
        }
    }
}
