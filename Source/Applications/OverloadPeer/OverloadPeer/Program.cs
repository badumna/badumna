﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace OverloadPeer
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Badumna;
    using Badumna.Core;
    using Badumna.Security;
    using Badumna.ServiceDiscovery;
    using Badumna.Utilities;
    using GermHarness;
    using Mono.Options;

    /// <summary>
    /// TODO: Overload Peer description.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Option set for parsing command line arguments.
        /// </summary>
        private readonly IOptionSet optionSet;

        /// <summary>
        /// The peer harness for hosting the process and communicating with the Control Center.
        /// </summary>
        private readonly IPeerHarness peerHarness;

        /// <summary>
        /// A writer for writing messages to the console.
        /// </summary>
        private readonly IConsoleWriter consoleWriter;

        /// <summary>
        /// Initializes a new instance of the Program class.
        /// </summary>
        public Program()
            : this(
            new OptionSet(),
            o => new PeerHarness(o),
            new ConsoleWriter())
        {
        }

        /// <summary>
        /// Initializes a new instance of the Program class.
        /// </summary>
        /// <param name="optionSet">Option set for parsing command line options.</param>
        /// <param name="peerHarnessFactory">Delegate for creating peer harness.</param>
        /// <param name="consoleWriter">A writer for outputing messages to the console.</param>
        public Program(
            IOptionSet optionSet,
            PeerHarnessFactory peerHarnessFactory,
            IConsoleWriter consoleWriter)
        {
            if (optionSet == null)
            {
                throw new ArgumentNullException("optionSet");
            }

            if (peerHarnessFactory == null)
            {
                throw new ArgumentNullException("peerHarnessFactory");
            }

            if (consoleWriter == null)
            {
                throw new ArgumentNullException("consoleWriter");
            }

            this.optionSet = optionSet;
            this.peerHarness = peerHarnessFactory(Program.GetDefaultOptions());
            this.consoleWriter = consoleWriter;

            this.optionSet.Add<string>(
                "h|help",
                "show this message and exit.",
                v =>
                {
                    if (v != null)
                    {
                        this.WriteUsageAndExit();
                    }
                });
        }

        /// <summary>
        /// Launch the hosted process.
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        internal void Run(string[] args)
        {
            // Parse program-specific command line arguments.
            try
            {
                List<string> extras = this.optionSet.Parse(args);
                string[] harnessArgs = extras.ToArray();
                this.peerHarness.Initialize(ref harnessArgs);
                if (harnessArgs.Length > 0)
                {
                    StringBuilder errorMessage = new StringBuilder("Unknown argument(s):");
                    foreach (string arg in harnessArgs)
                    {
                        errorMessage.AppendFormat(" {0}", arg);
                    }

                    this.HandleCommandLineArgumentErrors(errorMessage.ToString());
                }
                else
                {
                    // install the cancel key handler
                    this.consoleWriter.CancelKeyPress += delegate(object sender, ConsoleCancelEventArgs e)
                    {
                        e.Cancel = true;
                        this.peerHarness.Stop();
                    };

                    // start the harness to launch the seed peer, this will not return until the seed peer process is terminated.
                    this.peerHarness.RegisterService(ServerType.Overload);
                    this.peerHarness.Start();
                    this.consoleWriter.WriteLine("Overload peer has been stopped.");
                }
            }
            catch (OptionException ex)
            {
                this.HandleCommandLineArgumentErrors(
                    string.Format("Error parsing option {0}: {1}", ex.OptionName, ex.Message));
            }
            catch (DeiConfigException ex)
            {
                this.HandleCommandLineArgumentErrors(
                    string.Format("Error in Dei config: {0}", ex.Message));
            }
            catch (DeiAuthenticationException ex)
            {
                this.consoleWriter.WriteLine(ex.Message);
                Environment.Exit((int)ExitCode.DeiAuthenticationFailed);
            }
            catch (SecurityException ex)
            {
                this.consoleWriter.WriteLine("Exception caught : " + ex.Message);
                Environment.Exit((int)ExitCode.AnnounceServiceFailed);
            }
            catch (ConfigurationException ex)
            {
                this.consoleWriter.WriteLine(ex.Message);
                Environment.Exit((int)ExitCode.ConfigurationError);
            }
        }

        /// <summary>
        /// Entry point for program.
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        private static void Main(string[] args)
        {
            new Program().Run(args);
        }

        /// <summary>
        /// Gets an options object with default SeedPeer configuration.
        /// </summary>
        /// <returns>Default options.</returns>
        private static Options GetDefaultOptions()
        {
            Options options = new Options();

            options.Connectivity.ConfigureForSpecificPort(21252);
            options.Connectivity.IsBroadcastEnabled = true;
            options.Connectivity.BroadcastPort = 21250;
            options.Connectivity.IsPortForwardingEnabled = true;
            options.Connectivity.IsHostedService = true;

            options.Overload.IsServer = true;

            return options;
        }

        /// <summary>
        /// Add options for processing command line arguments.`
        /// </summary>
        private void AddCommandLineOptions()
        {
            this.optionSet.Add<string>(
                "h|help",
                "show this message and exit.",
                v =>
                {
                    if (v != null)
                    {
                        this.WriteUsageAndExit();
                    }
                });
        }

        /// <summary>
        /// Handle errors encountered during processing of command line options.
        /// </summary>
        /// <param name="message">A message describing the error.</param>
        private void HandleCommandLineArgumentErrors(string message)
        {
            this.consoleWriter.WriteLine("OverloadPeer: {0}", message);
            this.consoleWriter.WriteLine("Try `SeedPeer --help' for more information.");
            Environment.Exit((int)ExitCode.InvalidArguments);
        }

        /// <summary>
        /// Write usage instructions and exit.
        /// </summary>
        private void WriteUsageAndExit()
        {
            this.consoleWriter.WriteLine("Usage: OverloadPeer [Options]");
            this.consoleWriter.WriteLine("Start an Overload Peer on the local machine.");
            this.consoleWriter.WriteLine(string.Empty);
            this.consoleWriter.WriteLine("Options:");
            this.peerHarness.WriteOptionDescriptions(this.consoleWriter.Out);
            this.optionSet.WriteOptionDescriptions(this.consoleWriter.Out);
            Environment.Exit((int)ExitCode.DisplayHelp);
        }
    }
}
