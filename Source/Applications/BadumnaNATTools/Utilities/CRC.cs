//---------------------------------------------------------------------------------
// <copyright file="CRC.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;

namespace NatTools.Utilities
{
    /// <summary>
    /// Cyclic redundancy check implementation. 
    /// </summary>
    internal class CRC
    {
        private static ushort[] LookupTable16;
        private const ushort Polynomial16 = 0x8005;

        /// <summary>
        /// Initializes the <see cref="CRC"/> class.
        /// </summary>
        static CRC()
        {
            CRC.LookupTable16 = new ushort[256];
            for (ushort i = 0; i < 256; i++)
            {
                ushort remainder = (ushort)(i << 8);

                for (int j = 0; j < 8; j++)
                {
                    if ((remainder & 0x8000) > 0)
                    {
                        remainder = (ushort)((remainder << 1) ^ CRC.Polynomial16);
                    }
                    else
                    {
                        remainder <<= 1;
                    }
                }

                CRC.LookupTable16[i] = remainder;
            }        
        }

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>This does not reverse the bits of the input bytes or the result, so doesn't match the standard CRC-16.</remarks>
        /// <param name="data">Input data.</param>
        /// <returns>16bits CRC value.</returns>
        public static ushort CRC16(byte[] data)
        {
            return CRC.CRC16(data, 0, data.Length);
        }

        /// <summary>
        /// CRs the C16.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="offset">The offset.</param>
        /// <param name="length">The length.</param>
        /// <returns></returns>
        public static ushort CRC16(byte[] data, int offset, int length)
        {
            ushort result = 0;

            if (offset > data.Length || offset + length > data.Length)
            {
                throw new IndexOutOfRangeException("Offset and offset+length must be less than the length of the data.");
            }

            for (int i = offset; i < offset + length; i++)
            {
                result = (ushort)((result << 8) ^ CRC.LookupTable16[data[i] ^ (result >> 8)]);
            }

            return result;
        }
    }
}
