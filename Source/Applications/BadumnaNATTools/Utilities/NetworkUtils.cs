﻿//---------------------------------------------------------------------------------
// <copyright file="UPnPPortMappingEntry.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace NatTools
{
    /// <summary>
    /// Some utility methods.
    /// </summary>
    public class NetworkUtils
    {
        /// <summary>
        /// Get the list of address for this machine.
        /// </summary>
        /// <param name="port">The port being used.</param>
        /// <returns>A list local IPEndPoint.</returns>
        public static List<IPEndPoint> GetLocalAddresses(int port)
        {
            List<IPEndPoint> addresses = new List<IPEndPoint>();

            IPHostEntry ipHostInfo = null;
            try
            {
                ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            }
            catch
            {
                return addresses;
            }

            // Add the addresses that are resolved by the DNS query first.
            foreach (IPAddress address in ipHostInfo.AddressList)
            {
                if (address.AddressFamily == AddressFamily.InterNetwork && !IPAddress.IsLoopback(address))
                {
                    IPEndPoint localAddress = new IPEndPoint(address, port);
                    addresses.Add(localAddress);
                }
            }

            // Add any additional address that are for other interfaces.
            foreach (NetworkInterface networkInterface in NetworkInterface.GetAllNetworkInterfaces())
            {
                // on Ubuntu 9.04 + Mono 2.0, network interface's operational status is aways unknown.  
                if (networkInterface.NetworkInterfaceType != NetworkInterfaceType.Loopback &&
                    (networkInterface.OperationalStatus == OperationalStatus.Up ||
                     networkInterface.OperationalStatus == OperationalStatus.Unknown ||
                     networkInterface.OperationalStatus == OperationalStatus.Testing))
                {
                    UnicastIPAddressInformationCollection collection =
                        networkInterface.GetIPProperties().UnicastAddresses;
                    foreach (UnicastIPAddressInformation addressInformation in collection)
                    {
                        IPEndPoint address = new IPEndPoint(addressInformation.Address, port);
                        if (!addresses.Contains(address))
                        {
                            addresses.Add(address);
                        }
                    }
                }
            }

            return addresses;
        }
    }
}