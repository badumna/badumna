﻿//---------------------------------------------------------------------------------
// <copyright file="IPortForwarder.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Net;
using System.Net.Sockets;

namespace NatTools.UPnPClient
{
    /// <summary>
    /// The Port Forwarder interface.
    /// </summary>
    public interface IPortForwarder : IDisposable
    {
        /// <summary>
        /// Initializes the port forwarder.
        /// </summary>
        /// <param name="localAddress">The local peer address.</param>
        /// <returns>Whether the initialization succeeded or not.</returns>
        bool Initialize(IPAddress localAddress);

        /// <summary>
        /// Gets the external address.
        /// </summary>
        /// <returns>The external address or null on error.</returns>
        IPAddress GetExternalAddress();

        /// <summary>
        /// Tries to map the ports.
        /// </summary>
        /// <param name="localAddress">The local address.</param>
        /// <param name="mappingLease">The mapping lease.</param>
        /// <returns>Port forwarding result object.</returns>
        PortForwardingResult TryMapPort(IPEndPoint localAddress, TimeSpan mappingLease);
    }
}
