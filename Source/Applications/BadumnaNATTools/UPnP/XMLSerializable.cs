﻿//---------------------------------------------------------------------------------
// <copyright file="XMLSerializable.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace NatTools.UPnPClient
{
    /// <summary>
    /// XML Serializable class.
    /// </summary>
    internal class XMLSerializable
    {
        /// <summary>
        /// The content.
        /// </summary>
        private string content;

        /// <summary>
        /// Initializes a new instance of the <see cref="XMLSerializable"/> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public XMLSerializable(string content)
        {
            this.content = content;
        }

        /// <summary>
        /// Gets the content.
        /// </summary>
        /// <value>The content.</value>
        protected string Content
        {
            get { return this.content; }
        }

        /// <summary>
        /// Gets the byte value.
        /// </summary>
        /// <param name="name">The node name.</param>
        /// <returns>The byte value.</returns>
        public byte GetByte(string name)
        {
            string value = this.GetNode(name);
            return byte.Parse(value);
        }

        /// <summary>
        /// Gets the int value.
        /// </summary>
        /// <param name="name">The node name.</param>
        /// <returns>The int value.</returns>
        public int GetInt(string name)
        {
            string value = this.GetNode(name);
            return int.Parse(value);
        }

        /// <summary>
        /// Gets the string value.
        /// </summary>
        /// <param name="name">The node name.</param>
        /// <returns>The string value.</returns>
        public string GetString(string name)
        {
            return this.GetNode(name);
        }

        /// <summary>
        /// Gets the node.
        /// </summary>
        /// <param name="name">The node name.</param>
        /// <returns>The node content.</returns>
        public string GetNode(string name)
        {
            string startTag = "<" + name + ">";
            string endTag = "</" + name + ">";

            return this.GetNodeImpl(startTag, endTag);
        }

        /// <summary>
        /// Gets the node.
        /// </summary>
        /// <exception cref="SimpleXMLException">Thrown when the xml document is invalid.</exception>
        /// <param name="startTag">The start tag.</param>
        /// <param name="endTag">The end tag.</param>
        /// <returns>The node content.</returns>
        private string GetNodeImpl(string startTag, string endTag)
        {
            if (!this.content.Contains(startTag))
            {
                throw new SimpleXMLException("node " + startTag + " can not be located");
            }

            int start = this.content.IndexOf(startTag, 0);
            int startIndex = start + startTag.Length;
            int end = this.content.IndexOf(endTag, startIndex);

            if (end == 0)
            {
                throw new SimpleXMLException("node " + startTag + " can not be located");
            }

            int length = end - (start + startTag.Length);
            return this.content.Substring(start + startTag.Length, length).Trim();
        }
    }
}