﻿//---------------------------------------------------------------------------------
// <copyright file="UPnPPortMappingId.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace NatTools.UPnPClient
{
    /// <summary>
    /// The id used to identify port mapping entries on the router. This is normally used when removing mapped port. 
    /// </summary>
    internal class UPnPPortMappingId : IXMLSerializable
    {
        /// <summary>
        /// Remote host.
        /// </summary>
        private string remoteHost;

        /// <summary>
        /// External port.
        /// </summary>
        private int externalPort;

        /// <summary>
        /// Protocol, udp or tcp.
        /// </summary>
        private string protocol;

        /// <summary>
        /// Initializes a new instance of the <see cref="UPnPPortMappingId"/> class.
        /// </summary>
        public UPnPPortMappingId()
        {
            this.remoteHost = string.Empty;
            this.externalPort = 0;
            this.protocol = "UDP";
        }

        /// <summary>
        /// Gets or sets the remote host.
        /// </summary>
        /// <value>The remote host.</value>
        public string RemoteHost 
        {
            get { return this.remoteHost; }
            set { this.remoteHost = value; }
        }

        /// <summary>
        /// Gets or sets the external port.
        /// </summary>
        /// <value>The external port.</value>
        public int ExternalPort 
        {
            get { return this.externalPort; }
            set { this.externalPort = value; }
        }

        /// <summary>
        /// Gets or sets the protocol.
        /// </summary>
        /// <value>The protocol.</value>
        public string Protocol 
        {
            get { return this.protocol; }
            set { this.protocol = value; }
        }

        /// <summary>
        /// Serializes the object.
        /// </summary>
        /// <param name="nameSpace">The name space.</param>
        /// <param name="methodName">Name of the method.</param>
        /// <returns>Serlaized content.</returns>
        public string Serialize(string nameSpace, string methodName)
        {
            string formatString = "<s:Envelope s:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" "
                                + "xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                                + "<s:Body>"
                                + "<u:{0} xmlns:u=\"{1}\">"
                                + "<NewRemoteHost>{2}</NewRemoteHost>"
                                + "<NewExternalPort>{3}</NewExternalPort>"
                                + "<NewProtocol>{4}</NewProtocol>"
                                + "</u:{0}>"
                                + "</s:Body>"
                                + "</s:Envelope>";

            return string.Format(
                formatString,
                methodName,
                nameSpace,
                this.remoteHost,
                this.externalPort,
                this.protocol);
        }
    }
}