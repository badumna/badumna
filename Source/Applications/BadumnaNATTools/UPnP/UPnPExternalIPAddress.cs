﻿//---------------------------------------------------------------------------------
// <copyright file="UPnPExternalIPAddress.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace NatTools.UPnPClient
{
    /// <summary>
    /// UPnP external IP Address class.
    /// </summary>
    internal class UPnPExternalIPAddress : XMLSerializable, IXMLSerializable
    {
        /// <summary>
        /// The external IP address reported by the UPnP device.
        /// </summary>
        private string newExternalIPAddress;

        /// <summary>
        /// Initializes a new instance of the <see cref="UPnPExternalIPAddress"/> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public UPnPExternalIPAddress(string content)
            : base(content)
        {
            this.newExternalIPAddress = this.GetString("NewExternalIPAddress");
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UPnPExternalIPAddress"/> class.
        /// </summary>
        public UPnPExternalIPAddress()
            : base(string.Empty)
        {
            this.newExternalIPAddress = string.Empty;
        }

        /// <summary>
        /// Gets the external IP address.
        /// </summary>
        /// <value>The external IP address.</value>
        public string ExternalIPAddress
        {
            get { return this.newExternalIPAddress; }
        }

        /// <summary>
        /// Serializes the specified name space.
        /// </summary>
        /// <param name="nameSpace">The name space.</param>
        /// <param name="methodName">Name of the method.</param>
        /// <returns>The serialized data.</returns>
        public string Serialize(string nameSpace, string methodName)
        {
            string formatString = "<s:Envelope s:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" "
                          + "xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                          + "<s:Body>" 
                          + "<u:{0} xmlns:u=\"{1}\">"
                          + "<NewExternalIPAddress></NewExternalIPAddress>"
                          + "</u:{0}>"
                          + "</s:Body>"
                          + "</s:Envelope>";

            return string.Format(formatString, methodName, nameSpace);
        }
    }
}