﻿//---------------------------------------------------------------------------------
// <copyright file="IGDDescParser.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace NatTools.UPnPClient
{
    /// <summary>
    /// The parser for the Internet Gateway Device Description. 
    /// This simple parser is translated (C->C#) from MiniUPnP (http://miniupnp.free.fr/). MiniUPnP is released 
    /// under BSD license. 
    /// </summary>
    internal class IGDDescParser
    {
        /// <summary>
        /// The index of the current char in xml doc.  
        /// </summary>
        private int index;

        /// <summary>
        /// The char array of the desc. 
        /// </summary>
        private char[] descCharArray;

        /// <summary>
        /// The IGD data.
        /// </summary>
        private IGDData data;

        /// <summary>
        /// Initializes a new instance of the <see cref="IGDDescParser"/> class.
        /// </summary>
        public IGDDescParser()
        {
            this.data = new IGDData();
        }

        /// <summary>
        /// Gets the result.
        /// </summary>
        /// <value>The result of parse.</value>
        public IGDData Result
        {
            get { return this.data; }
        }
        
        /// <summary>
        /// Gets the current char.
        /// </summary>
        /// <value>The current char.</value>
        private char CurrentChar
        {
            get { return this.descCharArray[this.index]; }
        }

        /// <summary>
        /// Parses the specified content.
        /// </summary>
        /// <param name="content">The content.</param>
        public void Parse(string content)
        {
            this.descCharArray = content.ToCharArray();
            try
            {
                this.ParseElement();
            }
            catch
            {
            }
        }

        /// <summary>
        /// Parses the elements.
        /// </summary>
        private void ParseElement()
        {
            int i;
            int elementNameIndex = 0;

            while (this.index < (this.descCharArray.Length - 1))
            {
                if (this.CurrentChar == '<' && this.CharAt(this.index + 1) != '?')
                {
                    i = 0;
                    elementNameIndex = ++this.index;

                    while (!this.IsWhiteSpace(this.CurrentChar) && this.CurrentChar != '>' && this.CurrentChar != '/')
                    {
                        i++;
                        this.index++;

                        if (this.index >= this.descCharArray.Length)
                        {
                            return;
                        }

                        // to ignore namespace
                        if (this.CurrentChar == ':')
                        {
                            i = 0;
                            elementNameIndex = ++this.index;
                        }
                    }

                    if (i > 0)
                    {
                        // handle element start
                        this.HandleElementStart(elementNameIndex, i);

                        if (this.CurrentChar != '/')
                        {
                            int dataIndex = ++this.index;
                            i = 0;
                            if (this.index >= this.descCharArray.Length)
                            {
                                return;
                            }

                            while (this.IsWhiteSpace(this.CurrentChar))
                            {
                                this.index++;
                                if (this.index >= this.descCharArray.Length)
                                {
                                    return;
                                }
                            }

                            while (this.CurrentChar != '<')
                            {
                                i++;
                                this.index++;
                                if (this.index >= this.descCharArray.Length)
                                {
                                    return;
                                }
                            }

                            if (i > 0)
                            {
                                this.HandleData(dataIndex, i);
                            }
                        }
                    }
                    else if (this.CurrentChar == '/')
                    {
                        i = 0;
                        elementNameIndex = ++this.index;
                        if (this.index >= this.descCharArray.Length)
                        {
                            return;
                        }

                        while (this.CurrentChar != '>')
                        {
                            i++;
                            this.index++;

                            if (this.index >= this.descCharArray.Length)
                            {
                                return;
                            }
                        }

                        this.HandleElementEnd(elementNameIndex, i);
                        this.index++;
                    }
                }
                else
                {
                    this.index++;
                }
            }
        }

        /// <summary>
        /// Get the char at.
        /// </summary>
        /// <param name="idx">The index of char.</param>
        /// <returns>The specified char.</returns>
        private char CharAt(int idx)
        {
            return this.descCharArray[idx];
        }

        /// <summary>
        /// Determines whether the specified char is a white space.
        /// </summary>
        /// <param name="ch">The specified char.</param>
        /// <returns>
        /// <c>true</c> if is white space; otherwise, <c>false</c>.
        /// </returns>
        private bool IsWhiteSpace(char ch)
        {
            if (ch == ' ' || ch == '\t' || ch == '\n' || ch == '\r')
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Handles the element start.
        /// </summary>
        /// <param name="elementNameIndex">Index of the element name.</param>
        /// <param name="length">The length of the element name.</param>
        private void HandleElementStart(int elementNameIndex, int length)
        {
            char[] name = new char[length];

            for (int i = 0; i < length; i++)
            {
                name[i] = this.descCharArray[elementNameIndex + i];
            }

            this.data.CurElementName = new string(name);
            this.data.Level++;

            if (length == 7 && this.data.CurElementName == "service")
            {
                this.data.Tmp.ControlURL = string.Empty;
                this.data.Tmp.EventSubURL = string.Empty;
                this.data.Tmp.SCPDURL = string.Empty;
                this.data.Tmp.ServiceType = string.Empty;
            }
        }

        /// <summary>
        /// Handles the element end.
        /// </summary>
        /// <param name="elementNameIndex">Index of the element name.</param>
        /// <param name="length">The length of the element name.</param>
        private void HandleElementEnd(int elementNameIndex, int length)
        {
            this.data.Level--;
            char[] name = new char[length];
            for (int i = 0; i < length; i++)
            {
                name[i] = this.descCharArray[elementNameIndex + i];
            }

            string eltName = new string(name);

            if (length == 7 && eltName == "service")
            {
                if (this.data.Tmp.ServiceType == "urn:schemas-upnp-org:service:WANCommonInterfaceConfig:1")
                {
                    this.data.CIF.CopyFrom(this.data.Tmp);
                }
                else if (this.data.Tmp.ServiceType == "urn:schemas-upnp-org:service:WANIPConnection:1" ||
                         this.data.Tmp.ServiceType == "urn:schemas-upnp-org:service:WANPPPConnection:1")
                {
                    if (this.data.First.ServiceType == string.Empty)
                    {
                        this.data.First.CopyFrom(this.data.Tmp);
                    }
                    else
                    {
                        this.data.Second.CopyFrom(this.data.Tmp);
                    }
                }
            }
        }

        /// <summary>
        /// Handles the data.
        /// </summary>
        /// <param name="idx">The index.</param>
        /// <param name="length">The data length.</param>
        private void HandleData(int idx, int length)
        {
            if (this.data.CurElementName == "URLBase")
            {
                this.data.URLBase = this.GetStringData(idx, length);
            }
            else if (this.data.CurElementName == "serviceType")
            {
                this.data.Tmp.ServiceType = this.GetStringData(idx, length);
            }
            else if (this.data.CurElementName == "controlURL")
            {
                this.data.Tmp.ControlURL = this.GetStringData(idx, length);
            }
            else if (this.data.CurElementName == "eventSubURL")
            {
                this.data.Tmp.EventSubURL = this.GetStringData(idx, length);
            }
            else if (this.data.CurElementName == "SCPDURL")
            {
                this.data.Tmp.SCPDURL = this.GetStringData(idx, length);
            }
        }

        /// <summary>
        /// Gets the string data.
        /// </summary>
        /// <param name="idx">The index.</param>
        /// <param name="length">The string length.</param>
        /// <returns>The result data.</returns>
        private string GetStringData(int idx, int length)
        {
            char[] strdata = new char[length];

            for (int i = 0; i < length; i++)
            {
                strdata[i] = this.descCharArray[idx + i];
            }

            return new string(strdata);
        }

        /// <summary>
        /// The internal IGD data class.
        /// </summary>
        internal class IGDData
        {
            /// <summary>
            /// Current element name.
            /// </summary>
            private string curElementName;

            /// <summary>
            /// The URL base;
            /// </summary>
            private string urlBase;

            /// <summary>
            /// The current level.
            /// </summary>
            private int level;

            /// <summary>
            /// The cif service.
            /// </summary>
            private UPnPService cif;

            /// <summary>
            /// The first service.
            /// </summary>
            private UPnPService first;

            /// <summary>
            /// The second service.
            /// </summary>
            private UPnPService second;

            /// <summary>
            /// The tmp service.
            /// </summary>
            private UPnPService tmp;

            /// <summary>
            /// Initializes a new instance of the IGDDescParser.IGDData class.
            /// </summary>
            public IGDData()
            {
                this.curElementName = string.Empty;
                this.urlBase = string.Empty;
                this.level = 0;

                this.cif = new UPnPService();
                this.first = new UPnPService();
                this.second = new UPnPService();
                this.tmp = new UPnPService();
            }

            /// <summary>
            /// Gets or sets the name of the cur element.
            /// </summary>
            /// <value>The name of the cur element.</value>
            public string CurElementName
            {
                get { return this.curElementName; }
                set { this.curElementName = value; }
            }

            /// <summary>
            /// Gets or sets the URL base.
            /// </summary>
            /// <value>The URL base.</value>
            public string URLBase
            {
                get { return this.urlBase; }
                set { this.urlBase = value; }
            }

            /// <summary>
            /// Gets or sets the level.
            /// </summary>
            /// <value>The level.</value>
            public int Level
            {
                get { return this.level; }
                set { this.level = value; }
            }

            /// <summary>
            /// Gets or sets the CIF service.
            /// </summary>
            /// <value>The CIF service.</value>
            public UPnPService CIF
            {
                get { return this.cif; }
                set { this.cif = value; }
            }

            /// <summary>
            /// Gets or sets the first service.
            /// </summary>
            /// <value>The first service.</value>
            public UPnPService First
            {
                get { return this.first; }
                set { this.first = value; }
            }

            /// <summary>
            /// Gets or sets the second.
            /// </summary>
            /// <value>The second.</value>
            public UPnPService Second
            {
                get { return this.second; }
                set { this.second = value; }
            }

            /// <summary>
            /// Gets or sets the TMP service.
            /// </summary>
            /// <value>The TMP service.</value>
            public UPnPService Tmp
            {
                get { return this.tmp; }
                set { this.tmp = value; }
            }
        }
    }
}