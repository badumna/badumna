﻿//---------------------------------------------------------------------------------
// <copyright file="IXMLSerializable.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace NatTools.UPnPClient
{
    /// <summary>
    /// IXMLSerializable interface.
    /// </summary>
    internal interface IXMLSerializable
    {
        /// <summary>
        /// Serializes the specified name space.
        /// </summary>
        /// <param name="nameSpace">The name space.</param>
        /// <param name="methodName">Name of the method.</param>
        /// <returns>Serialized content.</returns>
        string Serialize(string nameSpace, string methodName);
    }
}
