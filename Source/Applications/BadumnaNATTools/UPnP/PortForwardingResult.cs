﻿//---------------------------------------------------------------------------------
// <copyright file="PortForwardingResult.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace NatTools.UPnPClient
{
    /// <summary>
    /// The result of the port forwarding operation. 
    /// </summary>
    public class PortForwardingResult
    {
        /// <summary>
        /// Whether the port forwarding succeeded. 
        /// </summary>
        private bool succeed;

        /// <summary>
        /// The mapped internal port.
        /// </summary>
        private int internalPort;

        /// <summary>
        /// The mapped external port.
        /// </summary>
        private int externalPort;

        /// <summary>
        /// The lease duration in seconds. 
        /// </summary>
        private uint leaseInSeconds;

        /// <summary>
        /// Gets or sets a value indicating whether succeed flag.
        /// </summary>
        /// <value>Whether succeed.</value>
        public bool Succeed
        {
            get { return this.succeed; }
            internal set { this.succeed = value; } 
        }

        /// <summary>
        /// Gets or sets the mapped internal port.
        /// </summary>
        /// <value>The internal port.</value>
        public int InternalPort
        {
            get { return this.internalPort; }
            internal set { this.internalPort = value; }
        }

        /// <summary>
        /// Gets or sets the mapped external port.
        /// </summary>
        /// <value>The external port.</value>
        public int ExternalPort
        {
            get { return this.externalPort; }
            internal set { this.externalPort = value; }
        }

        /// <summary>
        /// Gets or sets the lease duration in seconds.
        /// </summary>
        /// <value>The lease duration in seconds.</value>
        public uint LeaseInSeconds
        {
            get { return this.leaseInSeconds; }
            internal set { this.leaseInSeconds = value; }
        }
    }
}