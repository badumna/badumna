﻿//---------------------------------------------------------------------------------
// <copyright file="SimpleHttpWebRequest.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace NatTools.UPnPClient
{
    /// <summary>
    /// A very simple http client class.
    /// The .net WebRequest is not allowed in unity 2.6's webplayer mode, this class is implemented to replace the 
    /// WebRequest class. 
    /// </summary>
    internal class SimpleHttpWebRequest
    {
        /// <summary>
        /// The stream used to hold the request data.
        /// </summary>
        private MemoryStream requestStream;

        /// <summary>
        /// The http method.
        /// </summary>
        private string method;

        /// <summary>
        /// The user agent string.
        /// </summary>
        private string userAgent;

        /// <summary>
        /// Whether to request the http connection to keep alive.  
        /// </summary>
        private bool keepAlive;

        /// <summary>
        /// The length of the content.
        /// </summary>
        private int contentLength;

        /// <summary>
        /// The content type of the request.
        /// </summary>
        private string contentType;

        /// <summary>
        /// Timeout value. 
        /// </summary>
        private int timeoutInMilliseconds = 30000;

        /// <summary>
        /// The requested uri.
        /// </summary>
        private Uri requestUri;

        /// <summary>
        /// The web header collection.
        /// </summary>
        private WebHeaderCollection headerCollection;

        /// <summary>
        /// Indicates that a pending GetResponse call should be aborted.
        /// </summary>
        private ManualResetEvent shouldAbort = new ManualResetEvent(false);

        /// <summary>
        /// Backing field for AdvertiseAsHttp11.
        /// </summary>
        private bool advertiseAsHttp11 = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleHttpWebRequest"/> class.
        /// </summary>
        public SimpleHttpWebRequest()
        {
            // default method is get
            this.method = "GET";
            this.headerCollection = new WebHeaderCollection();
        }

        /// <summary>
        /// Gets or sets the method.
        /// </summary>
        /// <value>The method.</value>
        public string Method
        {
            get { return this.method; }
            set { this.method = value; }
        }

        /// <summary>
        /// Gets or sets the user agent.
        /// </summary>
        /// <value>The user agent.</value>
        public string UserAgent
        {
            get { return this.userAgent; }
            set { this.userAgent = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [keep alive].
        /// </summary>
        /// <value><c>true</c> if [keep alive]; otherwise, <c>false</c>.</value>
        public bool KeepAlive
        {
            get { return this.keepAlive; }
            set { this.keepAlive = value; }
        }

        /// <summary>
        /// Gets or sets the length of the content.
        /// </summary>
        /// <value>The length of the content.</value>
        public int ContentLength
        {
            get
            {
                return this.contentLength;
            }

            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("Http request ContentLength must be set to be positive.");
                }

                this.contentLength = value;
            }
        }

        /// <summary>
        /// Gets or sets the type of the content.
        /// </summary>
        /// <value>The type of the content.</value>
        public string ContentType
        {
            get { return this.contentType; }
            set { this.contentType = value; }
        }

        /// <summary>
        /// Gets or sets the timeout.
        /// </summary>
        /// <value>The timeout.</value>
        public int Timeout
        {
            get
            {
                return this.timeoutInMilliseconds;
            }

            set
            {
                if (value <= 0)
                {
                    throw new ArgumentException("Http request timeout must be set to be positive.");
                }

                this.timeoutInMilliseconds = value;
            }
        }

        /// <summary>
        /// Gets the request URI.
        /// </summary>
        /// <value>The request URI.</value>
        public Uri RequestUri
        {
            get { return this.requestUri; }
            private set { this.requestUri = value; }
        }

        /// <summary>
        /// Gets or sets the web header collection.
        /// </summary>
        /// <value>The web header collection.</value>
        public WebHeaderCollection Headers
        {
            get { return this.headerCollection; }
            set { this.headerCollection = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the request should be made as if we were an HTTP/1.1 client.
        /// </summary>
        /// <remarks>
        /// The client doesn't support all required features of HTTP/1.1 (e.g. the chunked transfer encoding) so
        /// this should only be used when the destination server is known to work OK with this implementations.
        /// Intervening proxies may cause issues...
        /// </remarks>
        public bool AdvertiseAsHttp11
        {
            get { return this.advertiseAsHttp11; }
            set { this.advertiseAsHttp11 = value; }
        }

        /// <summary>
        /// Creates the specified request URI string.
        /// </summary>
        /// <param name="requestUriString">The request URI string.</param>
        /// <returns>The http web request object.</returns>
        public static SimpleHttpWebRequest Create(string requestUriString)
        {
            return SimpleHttpWebRequest.Create(new Uri(requestUriString));
        }

        /// <summary>
        /// Creates a request to the specified URI.
        /// </summary>
        /// <param name="requestUri">The URI for the request</param>
        /// <returns>A new SimpleHttpWebRequest that can be populated with the request details</returns>
        public static SimpleHttpWebRequest Create(Uri requestUri)
        {
            SimpleHttpWebRequest request = new SimpleHttpWebRequest();
            request.requestUri = requestUri;
            return request;
        }

        /// <summary>
        /// Gets the request stream.
        /// </summary>
        /// <returns>The request stream.</returns>
        public Stream GetRequestStream()
        {
            if (this.requestStream == null)
            {
                // 16k bytes stream
                this.requestStream = new MemoryStream(16 * 1024);
            }

            return this.requestStream;
        }

        /// <summary>
        /// Gets the response.
        /// </summary>
        /// <returns>A simple web response object that contains the response for the request.</returns>
        public SimpleWebResponse GetResponse()
        {
            using (TcpClient tcpClient = new TcpClient())
            {
                string host = this.requestUri.Host;
                int port = this.requestUri.Port;

                try
                {
                    tcpClient.Connect(host, port);
                }
                catch (Exception e)
                {
                    throw new WebException("Failed to connect to server", e, WebExceptionStatus.ConnectFailure, null);
                }

                bool mustClose;  // Ignored, because we always close
                return this.GetResponse(tcpClient, out mustClose);
            }
        }

        /// <summary>
        /// Gets the response using the specified TCP client.
        /// </summary>
        /// <param name="tcpClient">The TCP client to make the request on.</param>
        /// <param name="mustClose">Output parameter indicates that the HTTP server will not accept more requests on 
        /// this connection</param>
        /// <returns>The response to the request</returns>
        public SimpleWebResponse GetResponse(TcpClient tcpClient, out bool mustClose)
        {
            try
            {
                NetworkStream stream = tcpClient.GetStream();
                this.SendRequest(stream);
                SimpleWebResponse webResponse = this.ReadResponse(tcpClient, out mustClose);

                if (webResponse.HasFailureStatus)
                {
                    throw new WebException(
                        "Request failed: " + webResponse.StatusDescription, 
                        null, 
                        WebExceptionStatus.RequestCanceled, 
                        webResponse);
                }

                // release all resource.
                this.Close();

                return webResponse;
            }
            catch (WebException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new WebException("GetResponse failed", e, WebExceptionStatus.UnknownError, null);
            }
        }

        /// <summary>
        /// Aborts a pending GetResponse call.
        /// </summary>
        public void Abort()
        {
            this.shouldAbort.Set();
        }

        /// <summary>
        /// Closes this instance and cleanup.
        /// </summary>
        public void Close()
        {
            if (this.requestStream != null)
            {
                this.requestStream.Close();
                this.requestStream = null;
            }

            if (this.headerCollection != null)
            {
                this.headerCollection.Clear();
                this.headerCollection = null;
            }
        }

        /// <summary>
        /// Reads all the bytes remaining in the stream.
        /// </summary>
        /// <param name="stream">The stream to read from.</param>
        /// <returns>A buffer containing the bytes remaining in the stream.</returns>
        private static byte[] ReadToEnd(Stream stream)
        {
            int offset = 0;
            int freeSpace = 1024;
            byte[] buffer = new byte[freeSpace];

            int bytesRead;
            do
            {
                if (freeSpace == 0)
                {
                    freeSpace = buffer.Length;
                    Array.Resize(ref buffer, buffer.Length + freeSpace);
                }

                bytesRead = stream.Read(buffer, offset, freeSpace);
                freeSpace -= bytesRead;
                offset += bytesRead;
            }
            while (bytesRead > 0);

            Array.Resize(ref buffer, offset);

            return buffer;
        }

        /// <summary>
        /// Reads up to 'count' bytes from the stream.  Will block if bytes aren't yet available and
        /// may return fewer than 'count' bytes if the end of the stream is reached first.
        /// </summary>
        /// <param name="stream">The stream to read from</param>
        /// <param name="count">The number of bytes to read</param>
        /// <returns>The bytes read</returns>
        private static byte[] ReadBytes(Stream stream, int count)
        {
            byte[] result = new byte[count];
            int offset = 0;

            while (count > 0)
            {
                int bytesRead = stream.Read(result, offset, count);
                if (bytesRead == 0)
                {
                    break;  // End of stream
                }

                count -= bytesRead;
                offset += bytesRead;
            }

            // Normally won't do anything but required if connection is closed early
            Array.Resize(ref result, offset);

            return result;
        }

        /// <summary>
        /// Reads a block of data in chunked encoding from the stream.
        /// </summary>
        /// <param name="stream">The stream to read from</param>
        /// <returns>The bytes read</returns>
        private static byte[] ReadChunked(Stream stream)
        {
            byte[] result = new byte[0];
            int offset = 0;

            while (true)
            {
                string chunkHeader = SimpleHttpWebRequest.ReadLine(stream);
                string[] parts = chunkHeader.Split();
                int chunkLength;

                if (parts.Length < 1 ||
                    !int.TryParse(parts[0], NumberStyles.HexNumber, CultureInfo.InvariantCulture, out chunkLength))
                {
                    goto exit;
                }

                if (chunkLength == 0)
                {
                    break;
                }

                Array.Resize(ref result, result.Length + chunkLength);
                while (chunkLength > 0)
                {
                    int bytesRead = stream.Read(result, offset, chunkLength);
                    if (bytesRead == 0)
                    {
                        goto exit;
                    }

                    offset += bytesRead;
                    chunkLength -= bytesRead;
                }

                SimpleHttpWebRequest.ReadLine(stream);
            }

            // Trailer
            while (SimpleHttpWebRequest.ReadLine(stream).Length > 0)
            {
            }

        exit:
            Array.Resize(ref result, offset);
            return result;
        }

        /// <summary>
        /// Reads a single ASCII line from a stream up to an LF character.  CR and LF characters at the
        /// end of the line are trimmed.
        /// </summary>
        /// <remarks>
        /// This is required because StreamReader buffers bytes from the stream making it
        /// impossible to switch from a StreamReader back to the underlying Stream halfway
        /// through (can't seek on the stream either because it's a NetworkStream).
        /// </remarks>
        /// <param name="stream">The stream to read from</param>
        /// <returns>The line read</returns>
        private static string ReadLine(Stream stream)
        {
            int offset = 0;
            byte[] buffer = new byte[1024];

            while (true)
            {
                int b = stream.ReadByte();
                if (b < 0)
                {
                    break;
                }

                if (offset >= buffer.Length)
                {
                    Array.Resize(ref buffer, buffer.Length * 2);
                }

                buffer[offset] = (byte)b;
                offset++;

                // ASCII LF
                if (b == 10)
                {
                    break;
                }
            }

            return Encoding.ASCII.GetString(buffer, 0, offset).TrimEnd('\n', '\r');
        }

        /// <summary>
        /// Sends the request.
        /// </summary>
        /// <param name="stream">The stream to send the request on</param>
        private void SendRequest(NetworkStream stream)
        {
            this.FinalizeHeaders();

            byte[] headers = this.GetRequest(this.requestUri.PathAndQuery);
            stream.Write(headers, 0, headers.Length);

            if (this.requestStream != null)
            {
                if (this.contentLength == 0)
                {
                    throw new WebException("Content length is set to zero while the request stream contains data.");
                }

                byte[] request = this.requestStream.GetBuffer();

                if (request.Length < this.contentLength)
                {
                    /* This won't necessarily be thrown because MemoryStream can over-allocate the buffer.
                     * In this case we might send some random data but it's really the caller's bug in not
                     * setting content length correctly.  Using MemoryStream.GetBuffer saves a buffer copy
                     * compared to using GetArray. */
                    throw new WebException("Content length does not match size of data written to request stream.");
                }

                stream.Write(request, 0, this.contentLength);
            }

            stream.Flush();
        }

        /// <summary>
        /// Gets the request.
        /// </summary>
        /// <param name="path">The path of the resource that is being requested.</param>
        /// <returns>The request buffer.</returns>
        private byte[] GetRequest(string path)
        {
            string methodString = string.Format(
                "{0} {1} HTTP/1.{2}\r\n", 
                this.method, 
                path, 
                this.AdvertiseAsHttp11 ? "1" : "0");
            int headerByteArrayLength = this.Headers.ToByteArray().Length;
            int totalLength = methodString.Length + headerByteArrayLength;
            byte[] request = new byte[totalLength];
            System.Text.Encoding enc = System.Text.Encoding.ASCII;

            Buffer.BlockCopy(enc.GetBytes(methodString), 0, request, 0, methodString.Length);
            Buffer.BlockCopy(this.Headers.ToByteArray(), 0, request, methodString.Length, headerByteArrayLength);

            return request;
        }

        /// <summary>
        /// Finalizes the headers.
        /// </summary>
        private void FinalizeHeaders()
        {
            if (this.userAgent != null)
            {
                this.Headers.Add("User-Agent", this.userAgent);
            }

            if (this.contentType != null)
            {
                this.Headers.Add("Content-Type", this.contentType);
            }

            if (this.keepAlive)
            {
                this.Headers.Add("Connection", "Keep-Alive");
            }

            if (this.contentLength != 0)
            {
                this.Headers.Add("Content-Length", string.Empty + this.contentLength);
            }
            else
            {
                this.Headers.Add("Content-Length", "0");
            }

            this.Headers.Add("Host", this.requestUri.Host);
            this.Headers.Add("Cache-Control", "no-cache");
        }

        /// <summary>
        /// Reads the response.
        /// </summary>
        /// <param name="tcpClient">The TCPClient to read the response from</param>
        /// <param name="mustClose">Output parameter indicates that the HTTP server will not accept more requests on 
        /// this connection</param>
        /// <returns>The response for the request.</returns>
        private SimpleWebResponse ReadResponse(TcpClient tcpClient, out bool mustClose)
        {
            NetworkStream stream = tcpClient.GetStream();

            /* Wait until the response starts coming through, checking all the while for an Abort().
             * Once the response starts coming we won't process aborts any more.  This should cover
             * most cases (in particular, Abort() is required for shutting down the HTTP tunnel's
             * get-changelist loop. */
            int timeout = this.Timeout;
            while (tcpClient.Connected && !stream.DataAvailable && (this.Timeout == 0 || timeout > 0))
            {
                if (this.shouldAbort.WaitOne(TimeSpan.FromMilliseconds(10), false))
                {
                    throw new WebException("Request canceled", WebExceptionStatus.RequestCanceled);
                }

                timeout -= 10;
            }

            if (!tcpClient.Connected)
            {
                throw new WebException("Connection closed", WebExceptionStatus.ConnectionClosed);
            }

            if (timeout < 0)
            {
                throw new WebException("Request timed out", WebExceptionStatus.Timeout);
            }

            // Read status line "HTTP/xx.yy sss Human readable reason"
            string statusLine = SimpleHttpWebRequest.ReadLine(stream);
            string[] statusLineParts = statusLine.Split(new char[] { ' ' }, 3, StringSplitOptions.RemoveEmptyEntries);

            if (statusLineParts.Length < 2)
            {
                throw new WebException("Bad status line", WebExceptionStatus.ServerProtocolViolation);
            }

            if (!statusLineParts[0].StartsWith("HTTP/"))
            {
                throw new WebException("Bad HTTP-Version", WebExceptionStatus.ServerProtocolViolation);
            }

            if (statusLineParts[1].Length != 3 ||
                !char.IsDigit(statusLineParts[1], 0) ||
                !char.IsDigit(statusLineParts[1], 1) ||
                !char.IsDigit(statusLineParts[1], 2))
            {
                throw new WebException("Bad status code", WebExceptionStatus.ServerProtocolViolation);
            }

            int statusCode = int.Parse(statusLineParts[1]);
            string statusReason = statusLineParts.Length == 3 ? statusLineParts[2] : string.Empty;

            // Read headers
            mustClose = false;
            bool isChunked = false;
            int lengthToExpect = -1;
            bool done = false;
            do
            {
                string currentLine = SimpleHttpWebRequest.ReadLine(stream);

                // HTTP headers are case-insensitive
                currentLine = currentLine.ToLower().Trim();

                if (currentLine.StartsWith("content-length:"))
                {
                    string[] parts = currentLine.Split(':');
                    if (parts != null && parts.Length == 2)
                    {
                        lengthToExpect = int.Parse(parts[1]);
                    }
                }

                if (currentLine.StartsWith("connection:"))
                {
                    string[] parts = currentLine.Substring("connection:".Length).Split(',');
                    foreach (string part in parts)
                    {
                        if (part.Trim() == "close")
                        {
                            mustClose = true;
                            break;
                        }
                    }
                }

                if (currentLine.StartsWith("transfer-encoding:"))
                {
                    isChunked = currentLine.EndsWith("chunked");  // Chunked must be the last transfer encoding applied
                }

                if (string.IsNullOrEmpty(currentLine))
                {
                    done = true;
                }
            }
            while (!done);

            // Read body.
            byte[] result;

            // Order of precendence of detemining response length for HTTP/1.1 is defined in RFC2616 Section 4.4
            if (isChunked)
            {
                result = SimpleHttpWebRequest.ReadChunked(stream);
            }
            else
            {
                if (lengthToExpect >= 0)
                {
                    result = SimpleHttpWebRequest.ReadBytes(stream, lengthToExpect);
                }
                else
                {
                    result = SimpleHttpWebRequest.ReadToEnd(stream);
                }
            }

            return new SimpleWebResponse(new MemoryStream(result), statusCode, statusReason);
        }
    }
}
