﻿//---------------------------------------------------------------------------------
// <copyright file="SimpleWebResponse.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System.IO;
using System.Net;

namespace NatTools.UPnPClient
{
    /// <summary>
    /// Represents an HTTP response.  This class is intended to roughly track the .NET Framework's HttpWebResponse.
    /// It exists so that HTTP responses can be retrieved in, e.g., Unity, where HttpWebResponse is not usable.
    /// </summary>
    /// <remarks>
    /// Derived from WebResponse so that SimpleWebResponse can be attached to WebException.
    /// </remarks>
    internal class SimpleWebResponse : WebResponse
    {
        /// <summary>
        /// The response stream.
        /// </summary>
        private Stream responseStream;

        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleWebResponse"/> class.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <param name="statusCode">The status code of the response.</param>
        /// <param name="statusDescription">The description of the status.</param>
        public SimpleWebResponse(Stream stream, int statusCode, string statusDescription)
        {
            this.responseStream = stream;
            this.StatusCode = (HttpStatusCode)statusCode;
            this.StatusDescription = statusDescription;
        }

        /// <summary>
        /// Gets the HTTP status code of the response.
        /// </summary>
        public HttpStatusCode StatusCode { get; private set; }

        /// <summary>
        /// Gets a human-readable description of the status.
        /// </summary>
        public string StatusDescription { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the response has a failure status
        /// (i.e. An HTTP status code of 4xx or 5xx).
        /// </summary>
        public bool HasFailureStatus
        {
            get
            {
                return (int)this.StatusCode >= 400 && (int)this.StatusCode <= 599;
            }
        }

        /// <summary>
        /// Gets the response stream.
        /// </summary>
        /// <returns>The web response stream.</returns>
        public override Stream GetResponseStream()
        {
            return this.responseStream;
        }

        /// <summary>
        /// Closes this instance.
        /// </summary>
        public override void Close()
        {
            this.responseStream.Close();
        }
    }
}
