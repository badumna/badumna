﻿//---------------------------------------------------------------------------------
// <copyright file="UPnPPortMappingEntry.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Net.Sockets;

namespace NatTools.UPnPClient
{
    /// <summary>
    /// The UPnP port mapping entry.
    /// </summary>
    internal class UPnPPortMappingEntry : XMLSerializable, IXMLSerializable
    {
        /// <summary>
        /// Remote host.
        /// </summary>
        private string remoteHost;

        /// <summary>
        /// External port.
        /// </summary>
        private int externalPort;

        /// <summary>
        /// Protocol, udp or tcp.
        /// </summary>
        private ProtocolType protocol;

        /// <summary>
        /// Internal port.
        /// </summary>
        private int internalPort;

        /// <summary>
        /// Internal client.
        /// </summary>
        private string internalClient;

        /// <summary>
        /// Whether enabled.
        /// </summary>
        private bool isEnabled;

        /// <summary>
        /// The description.
        /// </summary>
        private string description;

        /// <summary>
        /// Lease duration.
        /// </summary>
        private uint leaseDuration;

        /// <summary>
        /// Initializes a new instance of the <see cref="UPnPPortMappingEntry"/> class.
        /// </summary>
        /// <exception cref="SimpleXMLException">
        /// Thrown when the xml document contains unsupported protocol type.
        /// </exception>
        /// <param name="content">The content.</param>
        public UPnPPortMappingEntry(string content)
            : base(content)
        {
            string protocolString;

            this.remoteHost = this.GetString("NewRemoteHost");
            this.internalClient = this.GetString("NewInternalClient");
            this.description = this.GetString("NewPortMappingDescription");
            this.externalPort = this.GetInt("NewExternalPort");
            protocolString = this.GetString("NewProtocol");
            this.internalPort = this.GetInt("NewInternalPort");

            if (protocolString.ToLower() == "udp")
            {
                this.protocol = ProtocolType.Udp;
            }
            else if (protocolString.ToLower() == "tcp")
            {
                this.protocol = ProtocolType.Tcp;
            }
            else
            {
                throw new SimpleXMLException("Unknown protocol type.");
            }

            string enabledString = this.GetString("NewEnabled");
            if (enabledString == "1")
            {
                this.isEnabled = true;
            }

            this.leaseDuration = (uint)this.GetInt("NewLeaseDuration");
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UPnPPortMappingEntry"/> class.
        /// </summary>
        public UPnPPortMappingEntry()
            : base(string.Empty)
        {
            this.remoteHost = string.Empty;
            this.externalPort = 0;
            this.protocol = ProtocolType.Udp;
            this.internalPort = 0;
            this.isEnabled = false;
            this.leaseDuration = 0;
        }

        /// <summary>
        /// Gets or sets the remote host.
        /// </summary>
        /// <value>The remote host.</value>
        public string RemoteHost 
        {
            get { return this.remoteHost; }
            set { this.remoteHost = value; }
        }

        /// <summary>
        /// Gets or sets the external port.
        /// </summary>
        /// <value>The external port.</value>
        public int ExternalPort 
        {
            get { return this.externalPort; }
            set { this.externalPort = value; }
        }

        /// <summary>
        /// Gets or sets the protocol type.
        /// </summary>
        /// <exception cref="System.ArgumentException">Thrown when the protocol type is not TCP or UDP.</exception>
        /// <value>The protocol type.</value>
        public ProtocolType Protocol 
        {
            get { return this.protocol; }
            set 
            {
                if (value == ProtocolType.Udp || value == ProtocolType.Tcp)
                {
                    this.protocol = value;
                }
                else
                {
                    throw new ArgumentException("Only TCP and UDP are supported.");
                }
            }
        }

        /// <summary>
        /// Gets or sets the internal port.
        /// </summary>
        /// <value>The internal port.</value>
        public int InternalPort 
        {
            get { return this.internalPort; }
            set { this.internalPort = value; }
        }

        /// <summary>
        /// Gets or sets the internal client.
        /// </summary>
        /// <value>The internal client.</value>
        public string InternalClient 
        {
            get { return this.internalClient; }
            set { this.internalClient = value; }
        }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        public string Description 
        {
            get { return this.description; }
            set { this.description = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is enabled.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is enabled; otherwise, <c>false</c>.
        /// </value>
        public bool IsEnabled
        {
            get { return this.isEnabled; }
            set { this.isEnabled = value; }
        }

        /// <summary>
        /// Gets or sets the duration of the lease.
        /// </summary>
        /// <value>The duration of the lease.</value>
        public uint LeaseDuration 
        {
            get { return this.leaseDuration; }
            set { this.leaseDuration = value; }
        }

        /// <summary>
        /// Serializes the object.
        /// </summary>
        /// <param name="nameSpace">The name space.</param>
        /// <param name="methodName">Name of the method.</param>
        /// <returns>Serlaized content.</returns>
        public string Serialize(string nameSpace, string methodName)
        {
            string protocolString = string.Empty;
            if (this.protocol == ProtocolType.Tcp)
            {
                protocolString = "TCP";
            }
            else if (this.protocol == ProtocolType.Udp)
            {
                protocolString = "UDP";
            }

            string formatString = "<s:Envelope s:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" "
                                    + "xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                                    + "<s:Body>"
                                    + "<u:{0} xmlns:u=\"{1}\">"
                                    + "<NewRemoteHost>{2}</NewRemoteHost>"
                                    + "<NewExternalPort>{3}</NewExternalPort>"
                                    + "<NewProtocol>{4}</NewProtocol>"
                                    + "<NewInternalPort>{5}</NewInternalPort>"
                                    + "<NewInternalClient>{6}</NewInternalClient>"
                                    + "<NewEnabled>{7}</NewEnabled>"
                                    + "<NewPortMappingDescription>{8}</NewPortMappingDescription>"
                                    + "<NewLeaseDuration>{9}</NewLeaseDuration>"
                                    + "</u:{0}>"
                                    + "</s:Body>"
                                    + "</s:Envelope>";

            string enabledString = "0";
            if (this.isEnabled)
            {
                enabledString = "1";
            }

            return string.Format(
                formatString,
                methodName,
                nameSpace,
                this.remoteHost,
                this.externalPort,
                protocolString,
                this.internalPort,
                this.internalClient,
                enabledString,
                this.description,
                this.leaseDuration);
        }
    }
}