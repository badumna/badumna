﻿//---------------------------------------------------------------------------------
// <copyright file="StunClient.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;

namespace NatTools.STUNClient
{
    /// <summary>
    /// The stun client can be used to determine peer's public IP address, port and the NAT type.
    /// </summary>
    public class StunClient
    {
        /// <summary>
        /// The no-where address indicating an invalid IPEndPoint.
        /// </summary>
        private readonly IPEndPoint AddressNoWhere = new IPEndPoint(0, 0);

        /// <summary>
        /// Number of attempts to try.
        /// </summary>
        private readonly static int STUNNumberOfAttempts = 3;

        /// <summary>
        /// Timeout value.
        /// </summary>
        private readonly static int STUNRequestTimeoutMilliseonds = 3000;

        /// <summary>
        /// A list of known private addresses assigned to the local peer. 
        /// </summary>
        private List<IPEndPoint> privateAddresses;

        /// <summary>
        /// The random number generator.
        /// </summary>
        private Random randomNumberGenerator;

        // stun query commands
        private const Int16 BindAddressRequest = 0x0001;
        private const Int16 BindAddressResponse = 0x0101;

        private const Int16 MappedAddress = 0x0001;
        private const Int16 ResponseAddress = 0x0002;
        
        private const Int16 ChangeRequest = 0x0003;
        private const Int16 SourceAddress = 0x0004;
        private const Int16 ChangedAddress = 0x0005;
        
        private const Int32 NoRequest = 0x0000000;
        
        // For some reason some servers respond to 3rd bit and some respond to the 1st - so both are used.
        private const Int32 ChangeAddress = 0x0000005; 
        private const Int32 ChangePort = 0x0000002;

        /// <summary>
        /// The random id used to idnetify request/reply pairs.  
        /// </summary>
        private byte[] identifier = new byte[16];

        private IPEndPoint mappedEndPointA;
        private IPEndPoint mappedEndPointB;

        private IPEndPoint firstServerEndPoint;
        private IPEndPoint secondServerEndPoint;

        /// <summary>
        /// The socket used for sending and receiving stun queries. 
        /// </summary>
        private Socket socket;

        /// <summary>
        /// The external port that we believe has been mapped to our internal socket.
        /// </summary>
        private int expectedExternalPort = -1;

        /// <summary>
        /// Local IPEndpoints.
        /// </summary>
        private List<IPEndPoint> localAddresses;

        /// <summary>
        /// Initializes a new instance of the <see cref="StunClient"/> class.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">Thrown when socket is null.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">Thrown when the port is invalid.</exception>
        /// <param name="socket">The socket to use.</param>
        /// <param name="localAddresses">The local addresses.</param>
        /// <param name="expectedExternalPort">The expected external port, or -1 when the external port is 
        /// unknown.</param>
        public StunClient(Socket socket, int expectedExternalPort)
        {
            if (socket == null)
            {
                throw new ArgumentNullException("socket can not be null.");
            }

            if (expectedExternalPort != -1 &&
                (expectedExternalPort > IPEndPoint.MaxPort || expectedExternalPort < IPEndPoint.MinPort))
            {
                throw new ArgumentOutOfRangeException("invalid port.");
            }

            int port = ((IPEndPoint)socket.LocalEndPoint).Port;
            this.localAddresses = NetworkUtils.GetLocalAddresses(port);
            this.firstServerEndPoint = this.AddressNoWhere;
            this.secondServerEndPoint = this.AddressNoWhere;

            this.socket = socket;
            this.privateAddresses = localAddresses;
            this.expectedExternalPort = expectedExternalPort;

            this.randomNumberGenerator = new Random(Environment.TickCount);
        }

        /// <summary>
        /// Initiates the request to the specified server and port.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">Thrown when the hostname or address is null.</exception>
        /// <exception cref="System.ArgumentOutOfRange">Thrown when port parameter is invalid.</exception>
        /// <param name="hostNameOrAddress">The host name or address of the stun server.</param>
        /// <param name="port">The port of stun server.</param>
        /// <returns>A stun query result object that details the query result.</returns>
        public StunResult InitiateRequest(string hostNameOrAddress, int port)
        {
            if (hostNameOrAddress == null)
            {
                throw new ArgumentNullException("hostname can not be null.");
            }

            IPAddress[] addressList = null;
            try
            {
                addressList = Dns.GetHostAddresses(hostNameOrAddress);
            }
            catch
            {
            }

            if (addressList == null || addressList.Length == 0)
            {
                return new StunResult("DNS query failed.");
            }

            return this.InitiateRequest(new IPEndPoint(addressList[0], port));
        }

        /// <summary>
        /// Initiates the request to the specified server.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">Thrown when the hostname or address is null.</exception>
        /// <param name="hostNameOrAddress">The host name or address of the stun server.</param>
        /// <returns>A stun query result object that details the query result.</returns>
        public StunResult InitiateRequest(string hostNameOrAddress)
        {
            return this.InitiateRequest(hostNameOrAddress, 3478);
        }

        /// <summary>
        /// Initiates the request with specified STUN servers. Requests will be made sequencially with all specified 
        /// servers. The returned result is the result from first successful STUN query or it returns failed if all 
        /// queries fail. 
        /// </summary>
        /// <exception cref="System.ArgumentNullException">Thrown when the addresses list is null.</exception>
        /// <param name="addresses">The STUN server addresses.</param>
        /// <returns>The result of STUN query.</returns>
        public StunResult InitiateRequest(List<string> hostNamesOrAddresses)
        {
            if (hostNamesOrAddresses == null)
            {
                throw new ArgumentNullException("addresses can not be null.");
            }

            foreach (string address in hostNamesOrAddresses)
            {
                StunResult result = this.InitiateRequest(address);
                if (result.Succeeded && result.NatType != NatType.Blocked)
                {
                    return result;
                }
            }

            return new StunResult("Stun query failed.");
        }

        /// <summary>
        /// Initiates the request using STUN's defaut port.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">Thrown when serverAddress is null.</exception>
        /// <param name="serverAddress">The server address.</param>
        /// <returns>A stun query result object that details the query result.</returns>
        private StunResult InitiateRequest(IPAddress serverAddress)
        {
            if (serverAddress == null)
            {
                throw new ArgumentNullException("server address can not be null.");
            }

            return this.InitiateRequest(new IPEndPoint(serverAddress, 3478));
        }

        /// <summary>
        /// Initiates the request.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">Thrown when serverEndPoint is null.</exception>
        /// <param name="serverEndPoint">The server end point.</param>
        /// <returns>A stun query result object that details the query result.</returns>
        private StunResult InitiateRequest(IPEndPoint serverEndPoint)
        {
            if (serverEndPoint == null)
            {
                throw new ArgumentNullException("server end point can not be null.");
            }

            try
            {
                this.mappedEndPointA = null;
                this.mappedEndPointB = null; ;
                this.firstServerEndPoint = serverEndPoint;

                return this.EchoAddressRequest();
            }
            catch (SocketException se)
            {
                return new StunResult("Socket exception : " + se.Message);
            }
            catch
            {
                return new StunResult("Exception caught during stun query.");
            }
        }

        private StunResult EchoAddressRequest()
        {
            this.SendRequest(this.firstServerEndPoint, StunClient.NoRequest, STUNNumberOfAttempts, -1);
            bool received = this.ReceiveResponse(STUNRequestTimeoutMilliseonds);

            if (!received)
            {
                return new StunResult(this.AddressNoWhere, NatType.Blocked);
            }

            if (null == this.mappedEndPointA)
            {
                return new StunResult("Failed to initiate stun request.");
            }

            // After a port has been mapped the outbound packet may still not get its source port re-written by the NAT 
            // device so we need to check that the external port that we have mapped provides a reachable route to this 
            // peer. We do this by sending another STUN query, this time asking the server to reply using the external 
            // port. If we get a response then all subsequent STUN queries will ask to use this port. We only need to do 
            // this if the mapped external port is differs from the last STUN query.
            if (this.expectedExternalPort > 0 && this.mappedEndPointA.Port != this.expectedExternalPort)
            {
                this.SendRequest(
                    this.firstServerEndPoint, 
                    StunClient.NoRequest, 
                    STUNNumberOfAttempts, 
                    this.expectedExternalPort);
                received = this.ReceiveResponse(STUNRequestTimeoutMilliseonds);
                if (!received)
                {
                    // The port is not working - so don't use it in subsequent requests.
                    this.expectedExternalPort = -1;
                }
            }
            else
            {
                this.expectedExternalPort = -1;
            }

            if (this.privateAddresses.Contains(this.mappedEndPointA))
            {
                return this.ChangeAddressRequest1();
            }

            return ChangeAddressRequest2();
        }

        private StunResult ChangeAddressRequest1()
        {
            this.SendRequest(
                this.firstServerEndPoint, 
                StunClient.ChangeAddress | StunClient.ChangePort, 
                STUNNumberOfAttempts, 
                this.expectedExternalPort);

            bool received = this.ReceiveResponse(STUNRequestTimeoutMilliseonds);

            if (!received)
            {
                return new StunResult(this.mappedEndPointA, NatType.SymmetricFirewall);
            }

            if (this.privateAddresses.Contains(this.mappedEndPointB))
            {
                return new StunResult(this.mappedEndPointA, NatType.Open);
            }

            return new StunResult("Stun query failed.");
        }

        private StunResult ChangeAddressRequest2()
        {
            this.SendRequest(
                this.firstServerEndPoint, 
                StunClient.ChangeAddress | StunClient.ChangePort, 
                STUNNumberOfAttempts, 
                this.expectedExternalPort);
            bool received = this.ReceiveResponse(STUNRequestTimeoutMilliseonds);

            if (!received)
            {
                return this.EchoAddressRequest2();
            }

            // Try to detect port weirdness!
            this.mappedEndPointB = null;

            this.SendRequest(
                this.secondServerEndPoint, 
                StunClient.NoRequest, 
                STUNNumberOfAttempts, 
                this.expectedExternalPort);
            received = this.ReceiveResponse(STUNRequestTimeoutMilliseonds);

            // Check if the ports are preserved
            if (!received || !this.mappedEndPointA.Equals(this.mappedEndPointB))
            {
                if (this.expectedExternalPort > 0)
                {
                    return new StunResult(
                        new IPEndPoint(this.mappedEndPointA.Address, this.expectedExternalPort), 
                        NatType.MangledFullCone);
                }
                else
                {
                    return new StunResult(this.mappedEndPointA, NatType.MangledFullCone);
                }
            }

            return new StunResult(this.mappedEndPointA, NatType.FullCone);
        }

        private StunResult EchoAddressRequest2()
        {
            this.mappedEndPointB = null;
            this.SendRequest(
                this.secondServerEndPoint, 
                StunClient.NoRequest, 
                STUNNumberOfAttempts, 
                this.expectedExternalPort);
            bool received = this.ReceiveResponse(STUNRequestTimeoutMilliseonds);
         
            if (!received || null == this.mappedEndPointB)
            {
                return new StunResult("Failed to receive echo request 2.");
            }

            if (!this.mappedEndPointA.Equals(this.mappedEndPointB))
            {
                return new StunResult(this.mappedEndPointA, NatType.SymmetricNat);
            }

            return ChangePortRequest();
        }

        private StunResult ChangePortRequest()
        {
            this.SendRequest(
                this.firstServerEndPoint, 
                StunClient.ChangePort, 
                STUNNumberOfAttempts, 
                this.expectedExternalPort);
            bool received = this.ReceiveResponse(STUNRequestTimeoutMilliseonds);

            if (!received)
            {
                return new StunResult(this.mappedEndPointA, NatType.RestrictedPort);
            }

            return new StunResult(this.mappedEndPointA, NatType.RestrictedCone);
        }

        /// <summary>
        /// Generates the random id.
        /// </summary>
        private void GenerateRandomId()
        {
            this.randomNumberGenerator.NextBytes(this.identifier);
        }

        /// <summary>
        /// Sends the request.
        /// </summary>
        /// <param name="destination">The destination.</param>
        /// <param name="request">The request.</param>
        /// <param name="numberOfAttempts">The number of attempts.</param>
        /// <param name="replyPort">The reply port.</param>
        private void SendRequest(IPEndPoint destination, Int32 request, int numberOfAttempts, int replyPort)
        {
            // Logger.TraceVerbose("Sending STUN request to {0}", destination);
            StunClientMessageBuffer message = new StunClientMessageBuffer();
            IPEndPoint serverAddr = destination;

            this.GenerateRandomId();
            this.WriteTypeToMessage(message, StunClient.BindAddressRequest);
            this.WriteLengthToMessage(message, 0x0000);
            this.WriteIDToMessage(message, this.identifier);

            if (request != StunClient.NoRequest)
            {
                this.WriteRequestToMessage(message, request);
            }

            if (this.mappedEndPointA != null && replyPort > 0)
            {
                this.WriteResponseAddress(message, new IPEndPoint(this.mappedEndPointA.Address, replyPort));
            }

            try
            {
                for (int i = 0; i < numberOfAttempts; i++)
                {
                    this.socket.SendTo(message.GetBuffer(), destination);
                }
            }
            catch (SocketException)
            {
                throw;
            }
        }

        /// <summary>
        /// Receives the response.
        /// </summary>
        /// <param name="milliseconds">The timeout value in milliseconds.</param>
        /// <returns>Whether any response is received.</returns>
        private bool ReceiveResponse(int milliseconds)
        {
            if (milliseconds <= 0)
            {
                return false;
            }

            EndPoint endPoint = new IPEndPoint(IPAddress.Any, 0);
            StunClientMessageBuffer message = new StunClientMessageBuffer();

            byte[] response = new byte[1000];
            int len = 0;

            DateTime start = DateTime.Now;
            try
            {
                this.socket.ReceiveTimeout = milliseconds;
                len = this.socket.ReceiveFrom(response, 0, response.Length, SocketFlags.None, ref endPoint);
                milliseconds -= (int)(DateTime.Now - start).TotalMilliseconds;
            }
            catch (SocketException)
            {
                return false;
            }

            if (0 == len)
            {
                return false;
            }

            message.Write(response, len);
            if (!this.HandleReply(message))
            {
                // The packet was not a valid STUN reply - keep listening.
                return this.ReceiveResponse(milliseconds);
            }

            return true;
        }

        /// <summary>
        /// Writes the type to message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="value">The type value.</param>
        private void WriteTypeToMessage(StunClientMessageBuffer message, Int16 value)
        {
            byte[] bytes = BitConverter.GetBytes(value);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            message.WriteOffset = 0;
            message.Write(bytes, bytes.Length);
        }

        /// <summary>
        /// Writes the length to message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="value">The length value.</param>
        private void WriteLengthToMessage(StunClientMessageBuffer message, Int16 value)
        {
            byte[] bytes = BitConverter.GetBytes(value);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            message.WriteOffset = 2;
            message.Write(bytes, bytes.Length);
        }

        /// <summary>
        /// Writes the ID to message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="id">The id.</param>
        private void WriteIDToMessage(StunClientMessageBuffer message, byte[] id)
        {
            Debug.Assert(id.Length == 16, "Invalid length for STUN identifier.");
            message.WriteOffset = 4;
            message.Write(id, id.Length);
        }


        /// <summary>
        /// Writes the request to message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="request">The request.</param>
        private void WriteRequestToMessage(StunClientMessageBuffer message, Int32 request)
        {
            byte[] type = BitConverter.GetBytes(StunClient.ChangeRequest);
            byte[] value = BitConverter.GetBytes(request);
            byte[] len = BitConverter.GetBytes((Int16)value.Length);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(type);
                Array.Reverse(len);
                Array.Reverse(value);
            }

            message.WriteOffset = 20;
            message.Write(type, type.Length);
            message.Write(len, len.Length);
            message.Write(value, value.Length);

            this.WriteLengthToMessage(message, (Int16)(type.Length + len.Length + value.Length));
        }

        /// <summary>
        /// Reads int16 from message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns>The int16 from the message.</returns>
        private Int16 ReadInt16FromMessage(StunClientMessageBuffer message)
        {
            this.CheckLength(message, 2);
            byte[] bytes = message.ReadBytes(2);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            return BitConverter.ToInt16(bytes, 0);
        }

        /// <summary>
        /// Reads the UInt16 from message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns>The Uint16 read from the message.</returns>
        private UInt16 ReadUInt16FromMessage(StunClientMessageBuffer message)
        {
            this.CheckLength(message, 2);
            byte[] bytes = message.ReadBytes(2);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            return BitConverter.ToUInt16(bytes, 0);
        }

        /// <summary>
        /// Reads the specified number of bytes from message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="length">The number of bytes to read.</param>
        /// <returns></returns>
        private byte[] ReadBytesFromMessage(StunClientMessageBuffer message, int length)
        {
            this.CheckLength(message, length);
            byte[] bytes = message.ReadBytes(length);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            return bytes;
        }

        /// <summary>
        /// Handles the reply.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns>Whether the message is successfully handled.</returns>
        private bool HandleReply(StunClientMessageBuffer message)
        {
            try
            {
                // header length
                this.CheckLength(message, 20); 
            }
            catch
            {
                return false;
            }

            Int16 responseType = this.ReadInt16FromMessage(message);
            Int16 responseLength = this.ReadInt16FromMessage(message);
            // Order independant.
            byte[] responseIdentifier = message.ReadBytes(this.identifier.Length);

            if (responseType == StunClient.BindAddressResponse)
            {
                // Check identifier
                if (this.IsCorrectIdentifier(responseIdentifier))
                {
                    while (message.ReadOffset < message.Length)
                    {
                        try
                        {
                            this.ReadAttribute(message);
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }

                    return true;
                }
            }
           
            return false;
        }

        /// <summary>
        /// Checks the length.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="length">The length.</param>
        private void CheckLength(StunClientMessageBuffer message, int length)
        {
            if ((message.Length - message.ReadOffset) < length)
            {
                throw new Exception("Not enough bytes.");
            }
        }

        /// <summary>
        /// Determines whether the specified identifier is correct.
        /// </summary>
        /// <param name="identifier">The identifier.</param>
        /// <returns>
        /// <c>true</c> if is correct identifier; otherwise, <c>false</c>.
        /// </returns>
        private bool IsCorrectIdentifier(byte[] identifier)
        {
            if (this.identifier.Length != identifier.Length)
            {
                return false;
            }

            for (int i = 0; i < this.identifier.Length; i++)
            {
                if (this.identifier[i] != identifier[i])
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Reads the attribute.
        /// </summary>
        /// <param name="message">The message.</param>
        private void ReadAttribute(StunClientMessageBuffer message)
        {
            Int16 type = this.ReadInt16FromMessage(message);
            Int16 length = this.ReadInt16FromMessage(message);

            switch (type)
            {
                case StunClient.MappedAddress:
                    IPEndPoint endPoint = this.ReadAddress(message);

                    if (null == this.mappedEndPointA)
                    {
                        this.mappedEndPointA = endPoint;
                    }
                    else if (null == this.mappedEndPointB)
                    {
                        this.mappedEndPointB = endPoint;
                    }

                    return;

                case StunClient.ChangedAddress:
                    this.secondServerEndPoint = this.ReadAddress(message);

                    return;

                case StunClient.SourceAddress:
                    IPEndPoint source = this.ReadAddress(message);

                    return;
                default:
                    break;
            }

            byte[] value = this.ReadBytesFromMessage(message, length);
        }

        /// <summary>
        /// Reads the address from the message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns>The address.</returns>
        private IPEndPoint ReadAddress(StunClientMessageBuffer message)
        {
            Int16 addressFamily = this.ReadInt16FromMessage(message);
            UInt16 port = this.ReadUInt16FromMessage(message);
            byte[] address = this.ReadBytesFromMessage(message, 4);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(address);
            }

            return new IPEndPoint(new IPAddress(address), port);
        }

        /// <summary>
        /// Writes an attribute to request the reply be sent to a spcecific address.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="address">The address.</param>
        private void WriteResponseAddress(StunClientMessageBuffer message, IPEndPoint address)
        {
            byte[] familyBytes = BitConverter.GetBytes((UInt16)1);
            byte[] portBytes = BitConverter.GetBytes((UInt16)address.Port);
            byte[] addressBytes = address.Address.GetAddressBytes();
            byte[] type = BitConverter.GetBytes(StunClient.ResponseAddress);
            byte[] len = BitConverter.GetBytes((Int16)(familyBytes.Length + portBytes.Length + addressBytes.Length));

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(type);
                Array.Reverse(len);
                Array.Reverse(familyBytes);
                Array.Reverse(portBytes);
            }

            message.WriteOffset = message.Length;
            message.Write(type, type.Length);
            message.Write(len, len.Length);
            message.Write(familyBytes, familyBytes.Length);
            message.Write(portBytes, portBytes.Length);
            message.Write(addressBytes, addressBytes.Length);

            this.WriteLengthToMessage(message, (Int16)(message.Length - 20));
        }
    }
}
