﻿//---------------------------------------------------------------------------------
// <copyright file="StunResult.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System.Net;

namespace NatTools.STUNClient
{
    /// <summary>
    /// The result of STUN query.
    /// </summary>
    public class StunResult
    {
        /// <summary>
        /// The public IP address and port obtained as the result of STUN query. 
        /// </summary>
        private IPEndPoint publicAddress;

        /// <summary>
        /// The NAT type obtained as the result of STUN query. 
        /// </summary>
        private NatType natType;

        /// <summary>
        /// The reason of the STUN query failure. 
        /// </summary>
        private string failureReason;

        /// <summary>
        /// Whether the STUN query succeeded. 
        /// </summary>
        private bool succeeded;

        /// <summary>
        /// Initializes a new instance of the <see cref="STUNResult"/> class.
        /// </summary>
        internal StunResult()
            : this(string.Empty)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="STUNResult"/> class.
        /// </summary>
        /// <param name="publicAddress">The public address.</param>
        /// <param name="natType">Type of the nat.</param>
        internal StunResult(IPEndPoint publicAddress, NatType natType)
        {
            this.succeeded = true;
            this.publicAddress = publicAddress;
            this.natType = natType;
            this.failureReason = string.Empty;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="STUNResult"/> class.
        /// </summary>
        /// <param name="reason">The failure reason.</param>
        internal StunResult(string reason)
        {
            this.succeeded = false;
            this.publicAddress = null;
            this.natType = NatType.Unknown;
            this.failureReason = reason;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this STUN query is succeeded.
        /// </summary>
        /// <value><c>true</c> if succeeded; otherwise, <c>false</c>.</value>
        public bool Succeeded
        {
            get { return this.succeeded; }
            internal set { this.succeeded = value; }
        }

        /// <summary>
        /// Gets or sets the public address.
        /// </summary>
        /// <value>The public address.</value>
        public IPEndPoint PublicAddress
        {
            get { return this.publicAddress; }
            internal set { this.publicAddress = value; }
        }

        /// <summary>
        /// Gets or sets the type of the NAT.
        /// </summary>
        /// <value>The type of the nat.</value>
        public NatType NatType
        {
            get { return this.natType; }
            internal set { this.natType = value; }
        }
    }
}
