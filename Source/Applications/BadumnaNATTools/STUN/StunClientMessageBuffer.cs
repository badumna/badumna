﻿//---------------------------------------------------------------------------------
// <copyright file="StunClientMessageBuffer.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.IO;

namespace NatTools.STUNClient
{
    /// <summary>
    /// The message buffer used by the stun client. 
    /// </summary>
    internal class StunClientMessageBuffer : IDisposable
    {
        /// <summary>
        /// The message buffer.
        /// </summary>
        private MemoryStream buffer;

        /// <summary>
        /// An offset from the start of the buffer indicating the location of read operations.
        /// </summary>
        private int readOffset;
        
        /// <summary>
        /// An offset from the start of the buffer indicating the location of write operations.
        /// </summary>
        private int writeOffset;

        /// <summary>
        /// The entire length of the messsage.
        /// </summary>
        private int length;
        
        /// <summary>
        /// Default constructor.
        /// </summary>
        public StunClientMessageBuffer()
        {
            this.buffer = new MemoryStream();
        }

        /// <summary>
        /// Gets or sets the read offset.
        /// </summary>
        /// <value>The read offset.</value>
        public int ReadOffset
        {
            get
            {
                return this.readOffset;
            }

            set
            {
                if (0 > value || value > this.Length)
                {
                    throw new ArgumentOutOfRangeException("ReadOffset");
                }

                this.readOffset = value;
            }
        }

        /// <summary>
        /// Gets the length.
        /// </summary>
        /// <value>The length.</value>
        public int Length
        {
            get { return this.length; }
        }

        /// <summary>
        /// The number of bytes that have yet to be read before the end of the message.
        /// </summary>
        /// <value>The number of available bytes.</value>
        public int Available
        {
            get { return this.length - this.readOffset; }
        }

        /// <summary>
        /// Gets or sets the write offset.
        /// </summary>
        /// <value>The write offset.</value>
        public int WriteOffset
        {
            get
            {
                return this.writeOffset;
            }

            set
            {
                if (0 > value || value > this.Length)
                {
                    throw new ArgumentOutOfRangeException("WriteOffset");
                }

                this.writeOffset = value;
            }
        }

        /// <summary>
        /// Gets the buffer.
        /// </summary>
        /// <returns>The buffer in byte array.</returns>
        public byte[] GetBuffer()
        {
            byte[] buf = this.buffer.ToArray();
            return buf;
        }

        /// <summary>
        /// Reads the byte.
        /// </summary>
        /// <returns>The byte.</returns>
        public byte ReadByte()
        {
            if (0 == this.Available)
            {
                throw new Exception("No more byte.");
            }

            byte result;

            this.buffer.Seek(this.ReadOffset, SeekOrigin.Begin);
            result = (byte)this.buffer.ReadByte();
            this.ReadOffset++;

            return result;
        }

        /// <summary>
        /// Reads the specified number of bytes.
        /// </summary>
        /// <param name="readLength">Length of the read.</param>
        /// <returns>The read bytes.</returns>
        public byte[] ReadBytes(int readLength)
        {
            if (readLength > this.Available || readLength < 0)
            {
                throw new ArgumentOutOfRangeException("readLength");
            }

            byte[] result = new byte[readLength];

            this.buffer.Seek(this.ReadOffset, SeekOrigin.Begin);
            int length = this.buffer.Read(result, 0, readLength);

            if (length != readLength)
            {
                throw new Exception("Failed to read enough bytes");
            }

            this.ReadOffset += readLength;

            return result;
        }

        /// <summary>
        /// Reads the bytes.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <param name="len">The len.</param>
        /// <returns>The number of read bytes.</returns>
        public int ReadBytes(byte[] result, int len)
        {
            return this.ReadBytes(result, 0, len);
        }

        /// <summary>
        /// Reads the bytes.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <param name="offset">The offset.</param>
        /// <param name="len">The len.</param>
        /// <returns>The number of read bytes.</returns>
        public int ReadBytes(byte[] result, int offset, int len)
        {
            int remainingBytes = this.Length - this.ReadOffset;
            int readLength = len > remainingBytes ? remainingBytes : len;

            if (0 == readLength)
            {
                return 0;
            }

            this.buffer.Seek(this.ReadOffset, SeekOrigin.Begin);
            this.buffer.Read(result, offset, readLength);
            this.ReadOffset += readLength;

            return readLength;
        }

        /// <summary>
        /// Reads the short.
        /// </summary>
        /// <returns>The read short number.</returns>
        public short ReadShort()
        {
            return BitConverter.ToInt16(this.ReadBytes(2), 0);
        }

        /// <summary>
        /// Reads the UShort.
        /// </summary>
        /// <returns>The read Ushort.</returns>
        public ushort ReadUShort()
        {
            return BitConverter.ToUInt16(this.ReadBytes(2), 0);
        }

        /// <summary>
        /// Reads the long.
        /// </summary>
        /// <returns>The read long.</returns>
        public long ReadLong()
        {
            return BitConverter.ToInt64(this.ReadBytes(8), 0);
        }

        /// <summary>
        /// Reads the Ulong.
        /// </summary>
        /// <returns>The read ulong.</returns>
        public ulong ReadULong()
        {
            return BitConverter.ToUInt64(this.ReadBytes(8), 0);
        }

        /// <summary>
        /// Reads the float.
        /// </summary>
        /// <returns>The read float.</returns>
        public float ReadFloat()
        {
            return BitConverter.ToSingle(this.ReadBytes(4), 0);
        }

        /// <summary>
        /// Reads the double.
        /// </summary>
        /// <returns>The read double.</returns>
        public double ReadDouble()
        {
            return BitConverter.ToDouble(this.ReadBytes(8), 0);
        }

        /// <summary>
        /// Reads the int.
        /// </summary>
        /// <returns></returns>
        public int ReadInt()
        {
            return BitConverter.ToInt32(this.ReadBytes(4), 0);
        }

        /// <summary>
        /// Reads the UInt.
        /// </summary>
        /// <returns>The read UInt.</returns>
        public uint ReadUInt()
        {
            return BitConverter.ToUInt32(this.ReadBytes(4), 0);
        }

        /// <summary>
        /// Reads the bool.
        /// </summary>
        /// <returns>The read bool.</returns>
        public bool ReadBool()
        {
            return this.ReadByte() != 0;
        }

        /// <summary>
        /// Writes the specified bool data.
        /// </summary>
        /// <param name="data">The data to be written.</param>
        public void Write(bool data)
        {
            if (data)
            {
                this.Write((byte)1);
            }
            else
            {
                this.Write((byte)0);
            }
        }

        /// <summary>
        /// Writes the specified data.
        /// </summary>
        /// <param name="data">The data.</param>
        public void Write(short data)
        {
            this.Write(BitConverter.GetBytes(data), 0, 2);
        }

        /// <summary>
        /// Writes the specified data.
        /// </summary>
        /// <param name="data">The data.</param>
        public void Write(ushort data)
        {
            this.Write(BitConverter.GetBytes(data), 0, 2);
        }

        /// <summary>
        /// Writes the specified data.
        /// </summary>
        /// <param name="data">The data.</param>
        public void Write(long data)
        {
            this.Write(BitConverter.GetBytes(data), 0, 8);
        }

        /// <summary>
        /// Writes the specified data.
        /// </summary>
        /// <param name="data">The data.</param>
        public void Write(ulong data)
        {
            this.Write(BitConverter.GetBytes(data), 0, 8);
        }

        /// <summary>
        /// Writes the specified data.
        /// </summary>
        /// <param name="data">The data.</param>
        public void Write(int data)
        {
            this.Write(BitConverter.GetBytes(data), 0, 4);
        }

        /// <summary>
        /// Writes the specified data.
        /// </summary>
        /// <param name="data">The data.</param>
        public void Write(uint data)
        {
            this.Write(BitConverter.GetBytes(data), 0, 4);
        }

        /// <summary>
        /// Writes the specified data.
        /// </summary>
        /// <param name="data">The data.</param>
        public void Write(float data)
        {
            this.Write(BitConverter.GetBytes(data), 0, 4);
        }

        /// <summary>
        /// Writes the specified data.
        /// </summary>
        /// <param name="data">The data.</param>
        public void Write(double data)
        {
            this.Write(BitConverter.GetBytes(data), 0, 8);
        }

        /// <summary>
        /// Writes the specified data.
        /// </summary>
        /// <param name="data">The data.</param>
        public void Write(byte data)
        {
            this.buffer.Seek(this.WriteOffset, SeekOrigin.Begin);
            this.buffer.WriteByte(data);

            if (this.length < this.WriteOffset + 1)
            {
                this.length = this.WriteOffset + 1;
            }

            this.WriteOffset += 1;
        }

        /// <summary>
        /// Writes the specified data.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="length">The length.</param>
        public void Write(byte[] data, int length)
        {
            this.Write(data, 0, length);
        }

        /// <summary>
        /// Writes the specified data.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="offset">The offset.</param>
        /// <param name="length">The length.</param>
        public void Write(byte[] data, int offset, int length)
        {
            if (null == data || length == 0)
            {
                return;
            }

            this.buffer.Seek(this.WriteOffset, SeekOrigin.Begin);
            this.buffer.Write(data, offset, length);

            if (this.length < this.WriteOffset + length)
            {
                this.length = this.WriteOffset + length;
            }

            this.WriteOffset += length;
        }

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.buffer.Dispose();
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
