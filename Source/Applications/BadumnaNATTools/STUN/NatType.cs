﻿//---------------------------------------------------------------------------------
// <copyright file="NatType.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace NatTools.STUNClient
{
    /// <summary>
    /// The NAT types defined in RFC 3489. The behaviors are also explained in:
    /// http://en.wikipedia.org/wiki/Network_address_translator
    /// </summary>
    public enum NatType
    {
        /// <summary>
        /// Unknown type. This is set when the NAT type is unknown or on error. 
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// The UDP communication is blocked by firewall. 
        /// </summary>
        Blocked = 1,

        /// <summary>
        /// On the open internet, not behind NAT.  
        /// </summary>
        Open = 2,
        
        /// <summary>
        /// Symmetric Firewall, defined in RFC 3489.
        /// </summary>
        SymmetricFirewall = 3,
        
        /// <summary>
        /// Full Cone, defined in RFC 3489.
        /// </summary>
        FullCone = 4,

        /// <summary>
        /// Symmetric NAT, defined in RFC 3489.
        /// </summary>
        SymmetricNat = 5,

        /// <summary>
        /// Restricted Port, defined in RFC 3489. 
        /// </summary>
        RestrictedPort = 6,
        
        /// <summary>
        /// Restricted Cone, defined in RFC 3489.
        /// </summary>
        RestrictedCone = 7,
        
        /// <summary>
        /// This is a Badumna extension. 
        /// MangledFullCone is usually caused by router's buggy UPnP implementation. When other peers send UDP packets 
        /// to the public port (as reported by STUN), they will be accepted as the UPnP port mapping is there. However, 
        /// when sending packets out to different destinations, the source port of the packet will be keep changing. 
        /// From other peers' point of view, it is like from different senders. This type of NAT is usually identified
        /// and reported by the STUN client when UPnP mapped the local port to a different external port (e.g. local 
        /// UDP port 1000 is mapped to public port 1005 by UPnP).  
        /// </summary>
        MangledFullCone = 8
    }
}
