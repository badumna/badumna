Badumna NAT Tools 
-----------------
The BadumnaNATTools solution contains the STUN and UPnP clients used by the Badumna suite, it also contains the data
scrambling scheme proposed for the Badumna/Unity Web Player 3 integration. 

STUN
----
STUN client can be used to report the type of NAT (e.g. FullCone, SymmetricNAT etc.), the public IP and port observed
by the STUN server. The client is implemented in StunClient.cs.

UPnP
----
The UPnP port forwarding client supports programmatically setup port forwardings on routers. 
The client is implemented in UpnpPortForwarder.cs. BadumnaUPnPPortForwarder.cs shows how it is used by Badumna itself, 
it is just an example. 

Please note, the IGDDescParser.cs contains some simple XML parser code directly translated (C->C#) from the MiniUPnP 
project. MiniUPnP is released under the BSD license.
 
Data Scrambling
---------------
The proposed scrambling scheme is implemented in DataScrambleScheme.cs. 

NATToolsExample
---------------
This is an example showing how to use the above components. It will:

1. create a UDP socket on a specified port, bind to it.
2. depends on whether port forwarding is disabled by command line option, it may use UPnP to do port forwarding for the
   used UDP port.
3. use the STUN client to figure out whether the process is running behind a NAT, if so what is the NAT type. 
4. perform data scrambling/unscrambling on a small random data set. 
5. dispose the port forwarder object, which will in turn remove the forwarded port from the router. 