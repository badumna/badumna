﻿//---------------------------------------------------------------------------------
// <copyright file="Program.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

using NatTools.STUNClient;
using NatTools.UPnPClient;
using NatTools.Utilities;

namespace NatTools
{
    /// <summary>
    /// The example program for the Badumna STUN and UPnP clients. 
    /// </summary>
    internal class NATToolsExample
    {
        /// <summary>
        /// The UDP port to use. 
        /// </summary>
        private static int UDPPort = 21334;

        public void Run(bool performPortForwarding)
        {
            using (Socket socket = this.CreateSocket(UDPPort))
            {
                using (IPortForwarder forwarder = new BadumnaUPnPPortForwarder())
                {
                    // get local addresses
                    List<IPEndPoint> addresses = NetworkUtils.GetLocalAddresses(UDPPort);
                    if (addresses.Count == 0)
                    {
                        Console.Error.WriteLine("Failed to determine the local address.");
                        return;
                    }

                    // port forwarding if not disabled.
                    int expectedPort = -1;
                    if (performPortForwarding)
                    {
                        expectedPort = this.PerformUPnPPortForwarding(addresses[0], forwarder);
                    }

                    // stun query
                    this.PerformSTUNQuery(socket, expectedPort);

                    // data scrambling
                    this.PerformDataScramble();

                    Console.WriteLine("Press any key to exit.");
                    ConsoleKeyInfo keyinfo = Console.ReadKey();
                    // now both the socket and the forwarder will be disposed. 
                    // the forwarder will remove the created port mapping from the router.
                }
            }
        }

        /// <summary>
        /// Creates a UDP socket.
        /// </summary>
        /// <param name="port">The port to use.</param>
        /// <returns>The socket created.</returns>
        private Socket CreateSocket(int port)
        {
            IPEndPoint address = new IPEndPoint(IPAddress.Any, port);
            Socket socket = new Socket(address.AddressFamily, SocketType.Dgram, ProtocolType.Udp);
            socket.Bind(address);

            return socket;
        }

        /// <summary>
        /// Performs the UPnP port forwarding.
        /// </summary>
        /// <param name="localAddress">The local address, e.g. the private IP address.</param>
        /// <returns>The mapped public port.</returns>
        private int PerformUPnPPortForwarding(IPEndPoint localAddress, IPortForwarder forwarder)
        {
            Console.WriteLine("UPnP port forwarding requested.");

            if (forwarder.Initialize(localAddress.Address))
            {
                PortForwardingResult result = forwarder.TryMapPort(
                        localAddress,
                        TimeSpan.FromSeconds(600));
                if (result != null && result.Succeed)
                {
                    Console.WriteLine("UPnP port forwarding succeeded.");
                    return result.ExternalPort;
                }
                else
                {
                    Console.Error.WriteLine("UPnP port forwarding failed.");
                }
            }
            else
            {
                // most likely the forwarder cannot locate the UPnP device. 
                Console.WriteLine("Unable to do UPnP port forwarding, probably not behind a UPnP enabled NAT?");
            }

            return -1;
        }

        /// <summary>
        /// Performs the STUN query.
        /// </summary>
        /// <param name="socket">The socket to use.</param>
        /// <param name="expectedPort">The expected port.</param>
        private void PerformSTUNQuery(Socket socket, int expectedPort)
        {
            // perform stun query
            /// A list of public STUN servers and their fair use policies can be found at:
            /// http://www.voip-info.org/wiki/view/STUN
            List<string> stunServers = new List<string>();
            stunServers.Add("stun1.noc.ams-ix.net");
            stunServers.Add("stun.voipbuster.com");
            stunServers.Add("stun.voxgratia.org");

            StunClient stun = new StunClient(socket, expectedPort);
            StunResult stunResult = stun.InitiateRequest(stunServers);

            if (stunResult.Succeeded)
            {
                Console.WriteLine("Public ip is {0}, port is {1}",
                    stunResult.PublicAddress.Address,
                    stunResult.PublicAddress.Port);
                Console.WriteLine("NAT type is {0}", stunResult.NatType);
            }
            else
            {
                Console.Error.WriteLine("STUN query failed.");
            }
        }

        /// <summary>
        /// Performs the data scramble test.
        /// </summary>
        private void PerformDataScramble()
        {
            byte[] data = new byte[128];
            for (int i = 0; i < 128; i++)
            {
                data[i] = (byte)i;
            }

            bool success = true;
            byte[] scrambledData = DataScrambleScheme.ScrambleUDPData(data);
            byte[] unscrabled = DataScrambleScheme.UnscrambleUDPData(scrambledData);
            if (unscrabled != null && unscrabled.Length == data.Length)
            {
                for (int i = 0; i < 128; i++)
                {
                    if (data[i] != unscrabled[i])
                    {
                        success = false;
                    }
                }
            }
            else
            {
                success = false;
            }

            if (!success)
            {
                Console.Error.WriteLine("Scramble/Unscramble failed.");
            }
            else
            {
                Console.WriteLine("Scramble/Unscramble succeeded.");
            }
        }

        /// <summary>
        /// The main entry of the program.
        /// </summary>
        /// <param name="args">The arguments list.</param>
        static void Main(string[] args)
        {
            // check the program command line option to see whether UPnP should be disabled.
            bool performUPnPPortForwarding = true;
            if (args != null && args.Length == 1 && args[0].ToLower() == "-u")
            {
                performUPnPPortForwarding = false;
            }

            try
            {
                NATToolsExample example = new NATToolsExample();
                example.Run(performUPnPPortForwarding);
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Exception caught - {0}", e.Message);
            }
        }
    }
}