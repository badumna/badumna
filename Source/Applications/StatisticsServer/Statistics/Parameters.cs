﻿//---------------------------------------------------------------------------------
// <copyright file="Parameters.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace Statistics
{
    using System;

    /// <summary>
    /// Commonly used parameters.
    /// </summary>
    public class Parameters
    {
        /// <summary>
        /// The hostname of the statistics server.
        /// </summary>
        public static readonly string StatisticsServerHostname = "localhost";

        /// <summary>
        /// The port of the statistics server. 
        /// </summary>
        public static readonly int StatisticsServerPort = 21256;

        /// <summary>
        /// The statistics message interval.
        /// </summary>
        public static readonly TimeSpan StatisticsMessageInterval = TimeSpan.FromMinutes(5.0f);

        /// <summary>
        /// The process interval.
        /// </summary>
        public static readonly TimeSpan ProcessInterval = TimeSpan.FromSeconds(1.0f / 30.0f);

        /// <summary>
        /// Range type of the Session starts and Concurrent Users line graphs.
        /// </summary>
        public enum RangeType : int
        {
            /// <summary>
            /// Range of one day.
            /// </summary>
            Day = 0,

            /// <summary>
            /// Range of one week.
            /// </summary>
            Week,

            /// <summary>
            /// Range of one month.
            /// </summary>
            Month
        }
    }
}