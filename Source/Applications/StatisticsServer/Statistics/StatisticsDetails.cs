﻿//---------------------------------------------------------------------------------
// <copyright file="StatisticsDetails.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace Statistics
{
    using System.IO;

    /// <summary>
    /// User details to be sent in statistics message.
    /// </summary>
    public class StatisticsDetails : TrackingPacket
    {
        /// <summary>
        /// The magic number.
        /// </summary>
        private static readonly ushort MagicNumber = 0xab13;

        /// <summary>
        /// Backing field for <see cref="Payload"/>.  Marked as
        /// <c>volatile</c> so that updates from a different
        /// thread are seen.
        /// </summary>
        private volatile string payload = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="StatisticsDetails"/> class.
        /// </summary>
        public StatisticsDetails()
            : base(StatisticsDetails.MagicNumber)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StatisticsDetails"/> class.
        /// </summary>
        /// <param name="payload">The initial payload.</param>
        /// <param name="uniqueishId">The uniqueish identifier (an ID that with high probability distinguishes 
        /// this session within the local machine).</param>
        public StatisticsDetails(string payload, long userId, uint uniqueishId)
            : this()
        {
            this.Payload = payload;
            this.Session = new SessionIdentity(userId, uniqueishId);
        }

        /// <summary>
        /// Gets the minimum size.
        /// </summary>
        public static int MinimumSize
        {
            get { return 14; }
        }

        /// <summary>
        /// Gets or sets the payload.
        /// </summary>
        public string Payload
        {
            get { return this.payload; }
            set { this.payload = value; }
        }

        public SessionIdentity Session { get; internal set; }

        /// <summary>
        /// Writes the payload data to the packet.
        /// </summary>
        /// <param name="writer">The binary writer to write the payload data to.</param>
        protected override void WritePayload(BinaryWriter writer)
        {
            writer.Write(this.Session.UserId);
            writer.Write(this.Payload);
            writer.Write(this.Session.SessionId);
        }

        /// <summary>
        /// Reads the payload data from the packet.
        /// </summary>
        /// <param name="reader">The binary reader to read the payload data from.</param>
        /// <returns><c>true</c> if the packet contained a valid payload, <c>false</c> otherwise.</returns>
        protected override bool ReadPayload(BinaryReader reader)
        {
            long userId = reader.ReadInt64();
            this.Payload = reader.ReadString();
            uint sessionId = reader.ReadUInt32();
            this.Session = new SessionIdentity(userId, sessionId);
            return true;
        }
    }
}