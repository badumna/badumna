﻿//---------------------------------------------------------------------------------
// <copyright file="ProcessBase.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace Statistics
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading;

    using Mono.Options;

    /// <summary>
    /// Base class for auto testing processes.
    /// </summary>
    public class ProcessBase
    {
        /// <summary>
        /// Option set for parsing command line options.
        /// </summary>
        private readonly OptionSet optionSet;

        /// <summary>
        /// A value indicating whether the peer is running.
        /// </summary>
        private bool isRunning = false;

        /// <summary>
        /// Initializes a new instance of the ProcessBase class.
        /// </summary>
        public ProcessBase()
        {
            this.optionSet = new OptionSet();
        }

        /// <summary>
        /// Initialize the process with command line arguments.
        /// </summary>
        /// <param name="args">Initialization arguments.</param>
        /// <returns>True if successful, otherwise false.</returns>
        public virtual bool Initialize(ref string[] args)
        {
            List<string> extras = this.optionSet.Parse(args);
            args = extras.ToArray();

            return true;
        }

        /// <summary>
        /// Write a list of supported command line options.
        /// </summary>
        /// <param name="tw">The text writer to use.</param>
        public virtual void WriteOptionDescriptions(TextWriter tw)
        {
            this.optionSet.WriteOptionDescriptions(tw);
        }

        /// <summary>
        /// Start the process.
        /// </summary>
        public void Start()
        {
            this.OnStartUp();
            this.RunLoop();
        }

        /// <summary>
        /// Stop the process.
        /// </summary>
        public void Stop()
        {
            this.isRunning = false;
        }

        /// <summary>
        /// Called regularly to perform updates.
        /// </summary>
        /// <param name="timeStep">Time since last update.</param>
        protected virtual void Update(double timeStep)
        {
        }

        /// <summary>
        /// Called when the process is starting up.
        /// </summary>
        protected virtual void OnStartUp()
        {
        }

        /// <summary>
        /// Called when the process is shutting down.
        /// </summary>
        protected virtual void OnShutDown()
        {
        }

        /// <summary>
        /// Run the process.
        /// </summary>
        private void RunLoop()
        {
            DateTime lastUpdateTime = DateTime.Now;
            this.isRunning = true;

            do
            {
                DateTime currentTime = DateTime.Now;
                this.Update((currentTime - lastUpdateTime).TotalSeconds);
                Thread.Sleep(Parameters.ProcessInterval.Milliseconds);
                lastUpdateTime = currentTime;
            }
            while (this.isRunning);

            this.OnShutDown();
        }
    }
}