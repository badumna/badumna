﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Statistics
{
    public class SessionIdentity : IEquatable<SessionIdentity>
    {
        public long UserId { get; private set; }
        public uint SessionId {get; private set;}

        public SessionIdentity(long userId, uint sessionId)
        {
            this.UserId = userId;
            this.SessionId = sessionId;
        }

        public override string ToString()
        {
            return String.Format("{0}:{1}", this.UserId, this.SessionId);
        }

        public static bool operator ==(SessionIdentity a, SessionIdentity b)
        {
            bool result;
            if (object.Equals(null, a))
            {
                result = object.Equals(null, b);
            }
            else
            {
                result = a.Equals(b);
            }

            return result;
        }

        /// <inheritdoc/>
        public static bool operator !=(SessionIdentity a, SessionIdentity b)
        {
            return !(a == b);
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            return this.Equals(obj as SessionIdentity);
        }

        public bool Equals(SessionIdentity other)
        {
            if (other == null)
            {
                return false;
            }

            return this.SessionId == other.SessionId && this.UserId == other.UserId;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.SessionId.GetHashCode();
        }
    }
}
