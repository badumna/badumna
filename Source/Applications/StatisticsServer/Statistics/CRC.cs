﻿//---------------------------------------------------------------------------------
// <copyright file="CRC.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace Statistics
{
    using System;

    /// <summary>
    /// CRC implementation borrowed from the badumna's utils module. 
    /// </summary>
    internal class CRC
    {
        /// <summary>
        /// Polynomial 16.
        /// </summary>
        private const ushort Polynomial16 = 0x8005;

        /// <summary>
        /// Polynomial 32.
        /// </summary>
        private const uint Polynomial32 = 0x04C11DB7;

        /// <summary>
        /// Lookup table for polynomial 16.
        /// </summary>
        private static ushort[] lookupTable16;

        /// <summary>
        /// Lookup table for polynomial 32.
        /// </summary>
        private static uint[] lookupTable32;

        /// <summary>
        /// Initializes static members of the <see cref="CRC"/> class.
        /// </summary>
        static CRC()
        {
            CRC.lookupTable32 = new uint[256];
            for (uint i = 0; i < 256; i++)
            {
                uint remainder = i << 24;

                for (int j = 0; j < 8; j++)
                {
                    if ((remainder & 0x80000000) > 0)
                    {
                        remainder = (remainder << 1) ^ CRC.Polynomial32;
                    }
                    else
                    {
                        remainder <<= 1;
                    }
                }

                CRC.lookupTable32[i] = remainder;
            }

            CRC.lookupTable16 = new ushort[256];
            for (ushort i = 0; i < 256; i++)
            {
                ushort remainder = (ushort)(i << 8);

                for (int j = 0; j < 8; j++)
                {
                    if ((remainder & 0x8000) > 0)
                    {
                        remainder = (ushort)((remainder << 1) ^ CRC.Polynomial16);
                    }
                    else
                    {
                        remainder <<= 1;
                    }
                }

                CRC.lookupTable16[i] = remainder;
            }
        }

        /// <summary>
        /// CRC using polynomial 32.
        /// </summary>
        /// <remarks>This only matches the standard CRC-32 in the respect that it uses the same polynomial.</remarks>
        /// <param name="data">Data to check.</param>
        /// <returns>Check value.</returns>
        public static uint CRC32(byte[] data)
        {
            return CRC.CRC32(0, data);
        }

        /// <summary>
        /// This function allows a CRC to be calculated in multiple passes.  The result from the previous pass should be
        /// passed in as the initialValue for the following pass.  The result will be the same as if the CRC was calculated
        /// in a single pass on all the data concatenated together (in the same order).
        /// </summary>
        /// <remarks>This only matches the standard CRC-32 in the respect that it uses the same polynomial.</remarks>
        /// <param name="initialValue">Initial value.</param>
        /// <param name="data">Data to check.</param>
        /// <returns>Check value.</returns>
        public static uint CRC32(uint initialValue, byte[] data)
        {
            uint result = initialValue;

            for (int i = 0; i < data.Length; i++)
            {
                result = (result << 8) ^ CRC.lookupTable32[data[i] ^ (result >> 24)];
            }

            return result;
        }

        /// <summary>
        /// CRC using polynomial 16.
        /// </summary>
        /// <remarks>This does not reverse the bits of the input bytes or the result, so doesn't match the standard CRC-16.</remarks>
        /// <param name="data">Data to check.</param>
        /// <returns>Check value.</returns>
        public static ushort CRC16(byte[] data)
        {
            return CRC.CRC16(data, 0, data.Length);
        }

        /// <summary>
        /// CRC using polynomial 16.
        /// </summary>
        /// <param name="data">Data to check.</param>
        /// <param name="offset">Offset amount.</param>
        /// <param name="length">Length of data.</param>
        /// <returns>Check value.</returns>
        public static ushort CRC16(byte[] data, int offset, int length)
        {
            ushort result = 0;

            if (offset > data.Length || offset + length > data.Length)
            {
                throw new IndexOutOfRangeException("Offset and offset+length must be less than the length of the data.");
            }

            for (int i = offset; i < offset + length; i++)
            {
                result = (ushort)((result << 8) ^ CRC.lookupTable16[data[i] ^ (result >> 8)]);
            }

            return result;
        }
    }
}