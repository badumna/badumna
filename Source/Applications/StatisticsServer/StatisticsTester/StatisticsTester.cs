﻿//---------------------------------------------------------------------------------
// <copyright file="StatisticsTester.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace StatisticsTester
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;

    using Mono.Options;

    using Statistics;
    using log4net;

    /// <summary>
    /// Application used to send dummy packets to Statistics Server.
    /// </summary>
    public class StatisticsTester : ProcessBase
    {
        /// <summary>
        /// Random number generator.
        /// </summary>
        private readonly Random random = new Random(Process.GetCurrentProcess().Id);

        /// <summary>
        /// Option set for parsing command line options.
        /// </summary>
        private readonly OptionSet optionSet;

        /// <summary>
        /// Messager used to send statistics messages.
        /// </summary>
        private StatisticsMessager messager = new StatisticsMessager();

        /// <summary>
        /// List to store references to peers.
        /// </summary>
        private List<StatisticsDetails> peerList = new List<StatisticsDetails>();

        /// <summary>
        /// Flag indicating whether the manager is running in mono.
        /// </summary>
        private bool isMono = false;

        /// <summary>
        /// IP address of the Statistics Server.
        /// </summary>
        private string serverHost = Parameters.StatisticsServerHostname;

        /// <summary>
        /// Port number of the Statistics Server.
        /// </summary>
        private int serverPort = Parameters.StatisticsServerPort;

        /// <summary>
        /// Time interval in between statistics messages.
        /// </summary>
        private double messageInterval = Parameters.StatisticsMessageInterval.TotalSeconds;

        /// <summary>
        /// Duration of simulated sessions.
        /// </summary>
        private double? sessionDuration;

        /// <summary>
        /// Whether to generate uniqu user IDs for each peer.
        /// </summary>
        private bool uniqueUserIds = true;

        /// <summary>
        /// The number of peers to deploy.
        /// </summary>
        private int numPeers = 1;

        /// <summary>
        /// Total number of scenes.
        /// </summary>
        private int? numScenes = null;

        /// <summary>
        /// Flag indicating whether peers should be assigned to scenes randomly using the command line option.
        /// </summary>
        private bool randomScenesIsSet = false;

        /// <summary>
        /// Explicit scene list (null if none given)
        /// </summary>
        private List<string> sceneList;

        /// <summary>
        /// Accumulated time since last statistics message.
        /// </summary>
        private double lastMessageTime = 0.0f;

        /// <summary>
        /// Accumulated time since last session ID refresh.
        /// </summary>
        private double currentSessionTime = 0.0f;

        private ILog logger;

        /// <summary>
        /// Initializes a new instance of the StatisticsTester class.
        /// </summary>
        public StatisticsTester()
        {
            this.logger = LogManager.GetLogger(this.GetType());

            this.optionSet = new OptionSet()
            {
                { "server-address=", "IP address and port number of statistics server.", var =>
                    {
                        string[] parts = var.Split(':');

                        if (parts.Length == 2)
                        {
                            this.serverHost = parts[0];
                            this.serverPort = int.Parse(parts[1]);
                        }
                    }
                },
                { "message-interval=", "Time interval of statistics messages, in seconds.", (float var) =>
                    {
                        this.messageInterval = var;
                    }
                },
                { "session-duration=", "Duration of simulated sessions, in seconds.", (double var) =>
                    {
                        this.sessionDuration = var;
                    }
                },
                { "infinite-sessions", "Never change session ID.", var =>
                    {
                        this.sessionDuration = null;
                    }
                },
                { "num-peers=", "Number of peers to deploy.", (int var) =>
                    {
                        this.numPeers = var;
                    }
                },
                { "single-user-id", "All peers share the same user ID (simulating a non-Dei network).", var =>
                    {
                        this.uniqueUserIds = false;
                    }
                },
                { "num-scenes=", "Total number of scenes.", (int var) =>
                    {
                        this.numScenes = var;
                    }
                },
                { "scene-names=", "List of scene names (whitespace separated).", (string var) =>
                    {
                        this.sceneList = new List<string>(var.Split(null));
                    }
                },
                { "random-scenes", "Assign peers to scenes randomly.", var =>
                    {
                        this.randomScenesIsSet = true;
                    }
                }
            };
        }

        /// <summary>
        /// Initialize the peer with command line arguments.
        /// </summary>
        /// <param name="args">Initialization arguments.</param>
        /// <returns>True if successful, otherwise false.</returns>
        public override bool Initialize(ref string[] args)
        {
            List<string> extras = this.optionSet.Parse(args);
            args = extras.ToArray();

            if (this.sceneList != null && this.numScenes != null)
            {
                throw new InvalidOperationException("You can't specify both --num-scenes and --scene-names");
            }

            return base.Initialize(ref args);
        }

        /// <summary>
        /// Write a list of supported command line options.
        /// </summary>
        /// <param name="tw">The text writer to use.</param>
        public override void WriteOptionDescriptions(TextWriter tw)
        {
            this.optionSet.WriteOptionDescriptions(tw);
            base.WriteOptionDescriptions(tw);
        }

        /// <summary>
        /// Called regularly to perform updates.
        /// </summary>
        /// <param name="timeStep">Time since last update.</param>
        protected override void Update(double timeStep)
        {
            this.UpdatePeers(timeStep);

            base.Update(timeStep);
        }

        /// <summary>
        /// Called when the statistics tester is starting up.
        /// </summary>
        protected override void OnStartUp()
        {
            this.isMono = Type.GetType("Mono.Runtime") != null;
            this.StartUpPeers();

            base.OnStartUp();
        }

        /// <summary>
        /// Called when the statistics tester is shutting down.
        /// </summary>
        protected override void OnShutDown()
        {
            this.ShutDownPeers();

            base.OnShutDown();
        }

        /// <summary>
        /// Updates the peers.
        /// </summary>
        /// <param name="timeStep">Time since last update.</param>
        private void UpdatePeers(double timeStep)
        {
            this.lastMessageTime += timeStep;
            this.currentSessionTime += timeStep;

            if (this.sessionDuration.HasValue && this.currentSessionTime > this.sessionDuration)
            {
                System.Console.WriteLine(String.Format("{0}: Generating new Session IDs for each peer...", DateTime.Now));
                this.ChangeSessionIds();
                this.currentSessionTime = this.currentSessionTime % this.sessionDuration.Value;
            }

            while (this.lastMessageTime >= this.messageInterval)
            {
                foreach (StatisticsDetails details in this.peerList)
                {
                    this.messager.SendMessage(details, this.serverHost, this.serverPort);
                }

                this.lastMessageTime -= this.messageInterval;
            }
        }

        /// <summary>
        /// Start up peers using the command line settings.
        /// </summary>
        private void StartUpPeers()
        {
            List<string> sceneList = this.GetSceneList();
            int numScenes = sceneList.Count;

            this.logger.DebugFormat("Starting up {0} peers across {1} scenes:\n{2}", this.numPeers, numScenes, string.Join(", ",sceneList.ToArray()));

            for (int i = 0; i < this.numPeers; ++i)
            {
                int randomNumber = this.random.Next();
                string payload = this.randomScenesIsSet ? sceneList[randomNumber % numScenes] : sceneList[i % numScenes];
                long userId = this.uniqueUserIds ? (long)randomNumber : -1;
                StatisticsDetails details = new StatisticsDetails(payload, userId, (uint)randomNumber);
                this.peerList.Add(details);
            }

            this.ChangeSessionIds();
        }

        /// <summary>
        /// Gives each StatisticsDetail a new Session ID with the same User, simulating the user quitting and rejoining.
        /// </summary>
        private void ChangeSessionIds()
        {
            foreach(StatisticsDetails details in this.peerList)
            {
                SessionIdentity oldSession = details.Session;
                details.Session = new SessionIdentity(oldSession.UserId, (uint)this.random.Next());
            }
        }

        /// <summary>
        /// Gets the scene list, which will either come from an explicit list or be randomly generated.
        /// </summary>
        /// <returns></returns>
        private List<string> GetSceneList()
        {
            if (this.sceneList != null)
            {
                return this.sceneList;
            }

            // if no scene list given, generate some randomly:
            List<string> sceneList = new List<string>();
            int numScenes = this.numScenes == null ? this.numPeers : this.numScenes.Value;

            for (int i = 0; i < numScenes; ++i)
            {
                int randomNumber = this.random.Next();
                sceneList.Add("scene" + randomNumber.ToString());
            }
            return sceneList;
        }

        /// <summary>
        /// Shut down the peers.
        /// </summary>
        private void ShutDownPeers()
        {
            this.peerList.Clear();
        }
    }
}
