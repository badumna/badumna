﻿//---------------------------------------------------------------------------------
// <copyright file="StatisticsMessager.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace StatisticsTester
{
    using System;
    using System.Net;
    using System.Net.Sockets;

    using Statistics;
    using log4net;

    /// <summary>
    /// The statistics messager class is used to send statistics messages. 
    /// </summary>
    internal class StatisticsMessager
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StatisticsMessager"/> class.
        /// </summary>
        public StatisticsMessager()
        {
        }

        /// <summary>
        /// Sends the statistics message and schedule the next task.
        /// </summary>
        /// <param name="details">The statistics details.</param>
        /// <param name="address">The server address.</param>
        /// <param name="port">The server port.</param>
        public void SendMessage(StatisticsDetails details, string address, int port)
        {
            using (Socket socket = this.GetSocket())
            {
                byte[] message = details.ToArray();
                IPEndPoint endpoint = this.GetServerEndPoint(address, port);
                if (endpoint != null && message != null && message.Length > 0)
                {
                    int sent = socket.SendTo(message, endpoint);
                    if (sent != message.Length)
                    {
                        Console.WriteLine("Failed to send the whole message.");
                    }
                }
            }
        }

        /// <summary>
        /// Gets the socket used for sending the statistics message.
        /// </summary>
        /// <returns>The udp socket.</returns>
        private Socket GetSocket()
        {
            Socket socket = null;

            try
            {
                socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to create socket for statistics message.");
                throw e;
            }

            return socket;
        }

        /// <summary>
        /// Gets the server end point.
        /// </summary>
        /// <returns>The server end point.</returns>
        /// <param name="address">The server address.</param>
        /// <param name="port">The server port.</param>
        private IPEndPoint GetServerEndPoint(string address, int port)
        {
            IPAddress[] addressList = Dns.GetHostAddresses(address);

            foreach (var ip in addressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    // server only operates on IPv4 currently.
                    IPEndPoint serverEndPoint = new IPEndPoint(ip, port);
                    return serverEndPoint;
                }
            }

            return null;
        }
    }
}