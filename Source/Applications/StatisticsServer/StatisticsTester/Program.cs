﻿//---------------------------------------------------------------------------------
// <copyright file="Program.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace StatisticsTester
{
    using System;
    using System.Collections.Generic;

    using Mono.Options;
    using log4net;
    using log4net.Appender;
    using log4net.Layout;
    using log4net.Repository.Hierarchy;
    using log4net.Core;

    /// <summary>
    /// The Statistics Tester sends dummy messages to the Statistics Server.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// The Statistics Tester used for testing the Statistics Server.
        /// </summary>
        private static StatisticsTester statisticsTester = new StatisticsTester();

        private static bool Verbose = false;

        /// <summary>
        /// Option set for parsing command line options.
        /// </summary>
        private static OptionSet optionSet = new OptionSet()
        {
            { "h|help", "Display this message and exit.", var =>
                {
                    Program.WriteUsageAndExit();
                }
            },
            { "v|verbose", "Log additional information.", var =>
                {
                    Program.Verbose = true;
                }
            },
        };

        /// <summary>
        /// Entry point for program.
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        private static void Main(string[] args)
        {
            try
            {
                List<string> extras = Program.optionSet.Parse(args);
                string[] testerArgs = extras.ToArray();

                Program.ConfigureLogging(Program.Verbose ? Level.Debug : Level.Warn);

                if (Program.statisticsTester.Initialize(ref testerArgs))
                {
                    if (testerArgs.Length > 0)
                    {
                        Program.HandleUnknownArguments(testerArgs);
                    }
                    else
                    {
                        Console.CancelKeyPress += delegate(object sender, ConsoleCancelEventArgs e)
                        {
                            e.Cancel = true;
                            Program.statisticsTester.Stop();
                        };

                        Console.WriteLine("StatisticsTester has been started.");
                        Program.statisticsTester.Start();
                        Console.WriteLine("StatisticsTester has been stopped.");
                    }
                }
                else
                {
                    Console.WriteLine("StatisticsTester: Could not initialize tester.");
                }
            }
            catch (OptionException ex)
            {
                Console.WriteLine("StatisticsTester: {0}", ex.Message);
                Console.WriteLine("Try 'StatisticsTester --help' for more information.");
            }
        }

        private static void ConfigureLogging(Level logLevel)
        {
            Hierarchy hierarchy = (Hierarchy)LogManager.GetRepository();
            hierarchy.Root.RemoveAllAppenders(); /*Remove any other appenders*/

            ConsoleAppender appender = new ConsoleAppender();
            PatternLayout pl = new PatternLayout();
            pl.ConversionPattern = "%-5p [%-10c]: %m%n";
            pl.ActivateOptions();
            appender.Layout = pl;
            appender.ActivateOptions();

            log4net.Config.BasicConfigurator.Configure(appender);

            hierarchy.Root.Level = logLevel;
            ILog log = LogManager.GetLogger(typeof(Program));
            log.Debug("Verbosity set to: " + logLevel);
        }

        /// <summary>
        /// Write error message listing unknown arguments.
        /// </summary>
        /// <param name="args">Unknown arguments.</param>
        private static void HandleUnknownArguments(string[] args)
        {
            Console.Write("StatisticsTester: Unknown argument(s):");

            foreach (string arg in args)
            {
                Console.Write(" " + arg);
            }

            Console.WriteLine(".");
            Console.WriteLine("Try 'StatisticsTester --help' for more information.");
        }

        /// <summary>
        /// Write usage instructions and exit.
        /// </summary>
        private static void WriteUsageAndExit()
        {
            Console.WriteLine("Usage: StatisticsTester [Options]");
            Console.WriteLine("Start a StatisticsTester on the local machine.");
            Console.WriteLine(string.Empty);
            Console.WriteLine("Options:");
            Program.statisticsTester.WriteOptionDescriptions(Console.Out);
            Program.optionSet.WriteOptionDescriptions(Console.Out);
            Environment.Exit(0);
        }
    }
}