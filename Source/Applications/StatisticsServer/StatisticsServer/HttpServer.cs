﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Xml;
using Statistics;
using System.Globalization;
using System.Diagnostics;

namespace StatisticsServer
{
    class HttpServer
    {
        private HttpListener listener;
        private StatisticsServerHostedProcess process;
        private string httpPrefix;

        public HttpServer(string httpPrefix, StatisticsServerHostedProcess process)
        {
            this.httpPrefix = httpPrefix;
            this.process = process;
        }

        /// <summary>
        /// Starts the HTTP server to run asynchronously in the background.
        /// </summary>
        public void Run()
        {
            if (!HttpListener.IsSupported)
            {
                throw new Exception("HttpListener is not supported.");
            }

            // Create a listener.
            this.listener = new HttpListener();
            this.listener.Prefixes.Add(this.httpPrefix);
            try
            {
                this.listener.Start();
            }
            catch (System.Net.HttpListenerException)
            {
                Console.WriteLine("Listener could not start! Try running (as admin):\n" +
                    "  netsh http add urlacl \"" + httpPrefix + "\" \"user=Everyone\"" +
                    "\n(Or use \"DOMAIN\\USER\" instead of \"Everyone\")\n");
                throw;
            }

            Console.WriteLine("HTTP server listening on " + this.httpPrefix);
            this.ContinueProcessing();
        }

        /// <summary>
        /// Stops the HTTP server.
        /// </summary>
        public void Stop()
        {
            if (this.listener != null)
            {
                Console.WriteLine("Stopping HTTP server");
                this.listener.Stop();
                this.listener = null;
            }
        }

        /// <summary>
        /// set up a listener for the next (async) request.
        /// </summary>
        /// <para>
        /// Performed once at the start of the server process,
        /// and then once after each async request is received.
        /// </para>
        private void ContinueProcessing()
        {
            this.listener.BeginGetContext(HandleRequest, null);
        }

        /// <summary>
        /// Handles an incoming HTTP request.
        /// </summary>
        /// <param name="ar">The async request placeholder.</param>
        private void HandleRequest(IAsyncResult ar)
        {
            var context = this.listener.EndGetContext(ar);
            this.ContinueProcessing();

            HttpListenerRequest request = context.Request;
            HttpListenerResponse response = context.Response;

            string startTimeParam = request.QueryString.Get("startTime");
            string rangeParam = request.QueryString.Get("rangeType");

            try
            {
                if (startTimeParam != null)
                {
                    if (rangeParam == null)
                    {
                        throw new ArgumentException("missing rangeType parameter");
                    }

                    long startTimeSeconds;
                    if (!long.TryParse(startTimeParam, NumberStyles.None, CultureInfo.InvariantCulture, out startTimeSeconds))
                    {
                        throw new ArgumentException("invalid startTime");
                    }

                    if (!Enum.IsDefined(typeof(Parameters.RangeType), rangeParam))
                    {
                        throw new ArgumentException("invalid rangeType");
                    }

                    Parameters.RangeType rangeType = (Parameters.RangeType) Enum.Parse(typeof(Parameters.RangeType), rangeParam);

                    DateTime startTime = UnixTimeStampToDateTime(startTimeSeconds);
                    this.process.WriteSessionActivity(startTime, rangeType, response.OutputStream);
                }
                else
                {
                    this.process.WriteStatistics(response.OutputStream);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception occurred in HTTP handler: " + e + "\n" + e.StackTrace);
                response.StatusCode = 500;
                ResponseUtil.WriteString(e.ToString(), response.OutputStream);
            }

            response.OutputStream.Close();
        }

        /// <summary>
        /// Convert a unix timestamp (an integer number of seconds
        /// since midnight, 1/1/1970) into a DateTime.
        /// </summary>
        /// <param name="unixTimeStamp">The unix time stamp.</param>
        /// <returns>A DateTime</returns>
        private DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }
    }
}
