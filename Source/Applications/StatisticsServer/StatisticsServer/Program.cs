﻿//---------------------------------------------------------------------------------
// <copyright file="Program.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace StatisticsServer
{
    using System;
    using System.Collections.Generic;

    using GermHarness;

    using Mono.Options;

    /// <summary>
    /// The Statistics Server listens for statistics messages and sends status updates to Control Center.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// The harness for hosting the process and communicating with the Control Center.
        /// </summary>
        private static ProcessHarness harness = new ProcessHarness();

        /// <summary>
        /// The Statistics Server hosted process.
        /// </summary>
        private static StatisticsServerHostedProcess process = new StatisticsServerHostedProcess();

        /// <summary>
        /// Option set for parsing command line arguments.
        /// </summary>
        private static OptionSet optionSet = new OptionSet()
            {
                { "h|help", "Display this message and exit.", var =>
                    {
                        Program.WriteUsageAndExit();
                    }
                },
            };

        /// <summary>
        /// Entry point for program.
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        private static void Main(string[] args)
        {
            try
            {
                List<string> extras = Program.optionSet.Parse(args);
                string[] harnessArgs = extras.ToArray();

                if (Program.harness.Initialize(ref harnessArgs, Program.process))
                {
                    if (harnessArgs.Length > 0)
                    {
                        Program.HandleUnknownArguments(harnessArgs);
                    }
                    else
                    {
                        Console.CancelKeyPress += delegate(object sender, ConsoleCancelEventArgs e)
                        {
                            e.Cancel = true;
                            Program.harness.Stop();
                        };

                        Program.harness.Start();
                        Console.WriteLine("Statistics Server has been stopped.");
                    }
                }
                else
                {
                    Console.WriteLine("StatisticsServer: Could not initialize harness.");
                }
            }
            catch (OptionException ex)
            {
                Console.WriteLine("StatisticsServer: {0}", ex.Message);
                Console.WriteLine("Try `StatisticsServer --help' for more information.");
            }
        }

        /// <summary>
        /// Write error message listing unknown arguments.
        /// </summary>
        /// <param name="arguments">Unknown arguments.</param>
        private static void HandleUnknownArguments(string[] arguments)
        {
            Console.Write("StatisticsServer: Unknown argument(s):");
            foreach (string arg in arguments)
            {
                Console.Write(" " + arg);
            }

            Console.WriteLine(".");
            Console.WriteLine("Try `StatisticsServer --help' for more information.");
        }

        /// <summary>
        /// Write usage instructions and exit.
        /// </summary>
        private static void WriteUsageAndExit()
        {
            Console.WriteLine("Usage: StatisticsServer [Options]");
            Console.WriteLine("Start a Statistics Server on the local machine.");
            Console.WriteLine(string.Empty);
            Console.WriteLine("Options:");
            Program.harness.WriteOptionDescriptions(Console.Out);
            Program.process.WriteOptionsDescription(Console.Out);
            Program.optionSet.WriteOptionDescriptions(Console.Out);
            Environment.Exit(0);
        }
    }
}