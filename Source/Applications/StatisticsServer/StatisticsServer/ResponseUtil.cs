﻿using System;
using System.Xml;
using System.IO;

namespace StatisticsServer
{
    /// <summary>
    /// Common functionality used by both HttpServer and StatisticsServerHostedProcess for formatting responses.
    /// </summary>
    class ResponseUtil
    {
        /// <summary>
        /// Writes the UTF-8 encoding of an XML document to a stream.
        /// </summary>
        /// <param name="content">The content.</param>
        /// <param name="dest">The stream to write to.</param>
        public static void WriteXml(XmlDocument content, Stream stream)
        {
            ResponseUtil.WriteString(content.OuterXml, stream);
        }

        /// <summary>
        /// Writes the UTF-8 encoding of a string to a stream.
        /// </summary>
        /// <param name="content">The content.</param>
        /// <param name="dest">The stream to write to.</param>
        public static void WriteString(string content, Stream stream)
        {
            using (StreamWriter writer = new StreamWriter(stream, System.Text.Encoding.UTF8))
            {
                writer.Write(content);
            }
        }
    }
}
