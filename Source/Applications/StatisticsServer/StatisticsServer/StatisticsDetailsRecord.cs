﻿//---------------------------------------------------------------------------------
// <copyright file="StatisticsDetailsRecord.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace StatisticsServer
{
    using System;

    using Statistics;

    /// <summary>
    /// Record of the received statistics details.
    /// </summary>
    internal class StatisticsDetailsRecord
    {
        /// <summary>
        /// The user details.
        /// </summary>
        private StatisticsDetails details;

        /// <summary>
        /// When the information is received in UTC time.
        /// </summary>
        private DateTime receiveTime;

        /// <summary>
        /// Whether it is a valid record. 
        /// </summary>
        private bool valid;

        /// <summary>
        /// Initializes a new instance of the <see cref="StatisticsDetailsRecord"/> class.
        /// </summary>
        /// <param name="buffer">The buffer.</param>
        public StatisticsDetailsRecord(byte[] buffer)
        {
            this.details = new StatisticsDetails();

            if (this.details.Deserialize(buffer))
            {
                this.valid = true;
                this.receiveTime = DateTime.Now;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is valid.
        /// </summary>
        /// <value><c>true</c> if this instance is valid; otherwise, <c>false</c>.</value>
        public bool IsValid
        {
            get { return this.valid; }
        }

        /// <summary>
        /// Gets the statistics details.
        /// </summary>
        public StatisticsDetails Details
        {
            get { return this.details; }
        }

        /// <summary>
        /// Gets the receive time.
        /// </summary>
        public DateTime ReceiveTime
        {
            get { return this.receiveTime; }
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return string.Format("{0}, {1}, {2}", this.receiveTime.ToString("yyyy-MM-dd HH:mm:ss"), this.details.Session, this.details.Payload);
        }
    }
}