﻿//---------------------------------------------------------------------------------
// <copyright file="Listener.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace StatisticsServer
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Sockets;
    using System.Threading;

    using Statistics;

    /// <summary>
    /// Listens in to a port for incoming Statistics update messages.
    /// </summary>
    internal class Listener
    {
        /// <summary>
        /// The size of the socket send and receive buffer.
        /// </summary>
        private static readonly int bufferSize = 64 * 1024;

        /// <summary>
        /// The udp socket used to receive incoming records.
        /// </summary>
        private Socket socket;

        /// <summary>
        /// Whether is running.
        /// </summary>
        private volatile bool isRunning;

        /// <summary>
        /// The receive thread.
        /// </summary>
        private Thread receiveThread;

        /// <summary>
        /// Initializes a new instance of the <see cref="Listener"/> class.
        /// </summary>
        /// <param name="port">The port to listen to.</param>
        public Listener(int port)
        {
            try
            {
                this.socket = this.CreateSocket(port);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Occurs when record received.
        /// </summary>
        public event RecordReceivedHandler RecordReceived;

        /// <summary>
        /// Gets a value indicating whether this instance is windows platform.
        /// </summary>
        /// <value>
        /// true if this instance is windows platform; otherwise, false.
        /// </value>
        private bool IsWindowsPlatform
        {
            get
            {
                return System.Environment.OSVersion.Platform == PlatformID.Win32NT || System.Environment.OSVersion.Platform == PlatformID.Win32S
                    || System.Environment.OSVersion.Platform == PlatformID.Win32Windows || System.Environment.OSVersion.Platform == PlatformID.WinCE;
            }
        }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        public void Start()
        {
            this.receiveThread = new Thread(this.ReceiveMessages);
            this.receiveThread.IsBackground = true;
            this.receiveThread.Start();
        }

        /// <summary>
        /// Shutdowns this instance.
        /// </summary>
        public void Shutdown()
        {
            this.isRunning = false;
        }

        /// <summary>
        /// Creates the udp socket and bind to the specified port.
        /// </summary>
        /// <param name="port">The port to listen to.</param>
        /// <returns>The created udp socket.</returns>
        private Socket CreateSocket(int port)
        {
            Socket socket = null;

            if (port < 1024 || port > 65535)
            {
                throw new ArgumentOutOfRangeException("port");
            }

            try
            {
                socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                socket.SendBufferSize = bufferSize;
                socket.ReceiveBufferSize = bufferSize;
                socket.EnableBroadcast = false;

                if (this.IsWindowsPlatform)
                {
                    // Set option to stop invalid exceptions for udp socket.
                    const int SIO_UDP_CONNRESET = -1744830452;
                    byte[] inValue = new byte[] { 0, 0, 0, 0 };     // == false
                    byte[] outValue = new byte[] { 0, 0, 0, 0 };    // initialize to 0
                    socket.IOControl(SIO_UDP_CONNRESET, inValue, outValue);
                }

                EndPoint ep = new IPEndPoint(IPAddress.Any, port);
                socket.Bind(ep);
                Console.WriteLine("Successfully bind to port {0}", port);
            }
            catch (SocketException)
            {
                Console.WriteLine("Failed to bind to port {0}", port);
                throw;
            }
            catch
            {
                Console.WriteLine("Failed to initialize the socket on port {0}", port);
                throw;
            }

            return socket;
        }

        /// <summary>
        /// Receives the messages.
        /// </summary>
        private void ReceiveMessages()
        {
            try
            {
                this.isRunning = true;

                List<Socket> readSockets = new List<Socket>();
                int numBytesRead = 0;
                byte[] buffer = new byte[bufferSize];
                EndPoint receiveEndPoint;

                while ((this.socket != null) && this.isRunning)
                {
                    numBytesRead = 0;
                    readSockets.Clear();
                    readSockets.Add(this.socket);

                    try
                    {
                        Socket.Select(readSockets, null, null, 300000);
                    }
                    catch
                    {
                        Console.WriteLine("Exception caught when determining socket status.");
                    }

                    if (readSockets.Count > 0)
                    {
                        try
                        {
                            if (this.socket != null)
                            {
                                receiveEndPoint = new IPEndPoint(IPAddress.Any, 0);
                                this.socket.ReceiveTimeout = 300;
                                numBytesRead = this.socket.ReceiveFrom(buffer, 0, bufferSize, SocketFlags.None, ref receiveEndPoint);
                            }
                        }
                        catch (NullReferenceException)
                        {
                            // This can be thrown on Mono/Linux when the socket is closed.
                            this.isRunning = false;
                        }
                        catch
                        {
                            Console.WriteLine("Exception caught when receiving messages.");
                        }

                        if ((numBytesRead >= StatisticsDetails.MinimumSize) && (this.RecordReceived != null))
                        {
                            try
                            {
                                byte[] array = new byte[numBytesRead];
                                Buffer.BlockCopy(buffer, 0, array, 0, numBytesRead);
                                this.RecordReceived(new StatisticsDetailsRecord(array));
                            }
                            catch
                            {
                                Console.WriteLine("Exception caught when handling packet");
                            }
                        }
                    }
                }
            }
            catch
            {
                Console.WriteLine("Exception caught when receiving messages.");
            }

            try
            {
                if ((this.socket != null) && !this.isRunning)
                {
                    this.socket.Blocking = false;
                    this.socket.Shutdown(SocketShutdown.Both);
                    this.socket.Close();
                    this.socket = null;
                }
            }
            catch
            {
                Console.WriteLine("Exception caught when closing socket.");
            }
        }
    }
}