﻿//---------------------------------------------------------------------------------
// <copyright file="SessionDetails.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace StatisticsServer
{
    using System;

    /// <summary>
    /// Details of a running session.
    /// </summary>
    internal class SessionDetails
    {
        /// <summary>
        /// The most current statistics details record of the session.
        /// </summary>
        private StatisticsDetailsRecord record;

        /// <summary>
        /// Start time of the session.
        /// </summary>
        private DateTime startTime;

        /// <summary>
        /// Initializes a new instance of the <see cref="SessionDetails"/> class.
        /// </summary>
        /// <param name="record">Statistics details record.</param>
        public SessionDetails(StatisticsDetailsRecord record)
        {
            this.record = record;
            this.startTime = record.ReceiveTime;
        }

        /// <summary>
        /// Gets or sets the statistics details record.
        /// </summary>
        public StatisticsDetailsRecord Record
        {
            get { return this.record; }
            set { this.record = value; }
        }

        /// <summary>
        /// Gets the start time.
        /// </summary>
        public DateTime StartTime
        {
            get { return this.startTime; }
        }
    }
}