﻿//---------------------------------------------------------------------------------
// <copyright file="StatisticsManager.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace StatisticsServer
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Xml;

    using Statistics;
    using System.Linq;

    /// <summary>
    /// The handler called on receiving a record.
    /// </summary>
    /// <param name="record">Statistics details record.</param>
    internal delegate void RecordReceivedHandler(StatisticsDetailsRecord record);

    /// <summary>
    /// Manager in charge of keeping statistics data.
    /// </summary>
    internal class StatisticsManager
    {
        /// <summary>
        /// The data access layer through which the database is accessed.
        /// </summary>
        private readonly StatisticsDAL statisticsDAL;

        /// <summary>
        /// Dictionary of session details.
        /// </summary>
        private Dictionary<SessionIdentity, SessionDetails> sessionDetails = new Dictionary<SessionIdentity, SessionDetails>();

        /// <summary>
        /// Object to lock access to sessionList to make it threadsafe.
        /// </summary>
        private object sessionLock = new object();

        /// <summary>
        /// The session processing thread.
        /// </summary>
        private Thread sessionThread;

        /// <summary>
        /// Buffer for received statistics details records.
        /// </summary>
        private Queue<StatisticsDetailsRecord> recordQueue = new Queue<StatisticsDetailsRecord>();

        /// <summary>
        /// Object used to lock access to recordQueue to make it threadsafe.
        /// </summary>
        private object recordLock = new object();

        /// <summary>
        /// The record processing thread.
        /// </summary>
        private Thread recordThread;

        /// <summary>
        /// Flag indicating whether the recording thread is running.
        /// </summary>
        private bool isRunning = false;

        /// <summary>
        /// Time interval in between statistics messages.
        /// </summary>
        private double messageInterval = Parameters.StatisticsMessageInterval.TotalSeconds;

        /// <summary>
        /// Initializes a new instance of the <see cref="StatisticsManager"/> class.
        /// </summary>
        /// <param name="statisticsDAL">The DAL through which to access the database.</param>
        /// <param name="databaseProvider">The data privider to use for database access.</param>
        /// <param name="databaseConnectionString">A string used to connect to the database.</param>
        /// <param name="messageInterval">Message interval.</param>
        public StatisticsManager(StatisticsDAL statisticsDAL, string databaseProvider, string databaseConnectionString, double messageInterval)
        {
            this.statisticsDAL = statisticsDAL;
            this.statisticsDAL.Initialise(databaseProvider, databaseConnectionString);

            this.messageInterval = messageInterval;
            this.isRunning = true;

            this.sessionThread = new Thread(this.ProcessSessionList);
            this.sessionThread.IsBackground = true;
            this.sessionThread.Start();

            this.recordThread = new Thread(this.ProcessRecordQueue);
            this.recordThread.IsBackground = true;
            this.recordThread.Start();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StatisticsManager"/> class.
        /// </summary>
        /// <param name="databaseProvider">The data privider to use for database access.</param>
        /// <param name="databaseConnectionString">A string used to connect to the database.</param>
        /// <param name="messageInterval">Message interval.</param>
        public StatisticsManager(string databaseProvider, string databaseConnectionString, double messageInterval)
            : this(new StatisticsDAL(), databaseProvider, databaseConnectionString, messageInterval)
        {
        }

        /// <summary>
        /// Perform any needed clean-up.
        /// </summary>
        public void Shutdown()
        {
            this.isRunning = false;

            this.statisticsDAL.Shutdown();
        }

        /// <summary>
        /// Called when a record is received.
        /// </summary>
        /// <param name="record">Statistics details record.</param>
        public void OnRecordReceived(StatisticsDetailsRecord record)
        {
            if (!record.IsValid)
            {
                return;
            }

            //// Records are put onto the record queue when they come in. They will later be processed by the session
            //// thread.
            lock (this.recordLock)
            {
                this.recordQueue.Enqueue(record);
            }
        }

        /// <summary>
        /// Gets a summary of statistics data.
        /// </summary>
        /// <returns>Stats summary in XML format.</returns>
        public XmlDocument GetStatistics()
        {
            XmlDocument statistics = new XmlDocument();
            statistics.AppendChild(statistics.CreateXmlDeclaration("1.0", "UTF-8", null));

            XmlNode rootNode = statistics.AppendChild(statistics.CreateElement("Statistics"));
            XmlNode statsNode = rootNode.AppendChild(statistics.CreateElement("Statistics"));
            statsNode.AppendChild(statistics.CreateElement("NumberOfSessions")).InnerText = this.sessionDetails.Count.ToString();

            IEnumerable<IGrouping<string, SessionIdentity>> sessionsPerScene;

            lock (this.sessionLock)
            {
                sessionsPerScene = Enumerable.GroupBy(this.sessionDetails,
                    (item) => item.Value.Record.Details.Payload,
                    (item) => item.Key,
                    EqualityComparer<string>.Default);
            }

            XmlNode scenesNode = rootNode.AppendChild(statistics.CreateElement("Scenes"));
            foreach (var group in sessionsPerScene)
            {
                List<SessionIdentity> sessions = new List<SessionIdentity>(group);
                var uniqueUsers = new HashSet<long>(System.Linq.Enumerable.Select(sessions, (item) => item.UserId));

                XmlNode sceneNode = scenesNode.AppendChild(statistics.CreateElement("Scene"));
                sceneNode.AppendChild(statistics.CreateElement("Name")).InnerText = group.Key;
                sceneNode.AppendChild(statistics.CreateElement("Users")).InnerText = uniqueUsers.Count.ToString();
                sceneNode.AppendChild(statistics.CreateElement("Sessions")).InnerText = sessions.Count.ToString();
            }

            return statistics;
        }

        /// <summary>
        /// Get a summary of session activity data.
        /// </summary>
        /// <param name="startTime">The start time.</param>
        /// <param name="rangeType">The range type.</param>
        /// <returns>Session activity summary in XML format.</returns>
        public XmlDocument GetSessionActivity(DateTime startTime, Parameters.RangeType rangeType)
        {
            XmlDocument statistics = new XmlDocument();
            statistics.AppendChild(statistics.CreateXmlDeclaration("1.0", "UTF-8", null));

            long[] sessionStarts = this.statisticsDAL.GetSessionStarts(startTime, rangeType);
            long[] concurrentUsers = this.statisticsDAL.GetConcurrentUsers(startTime, rangeType);
            long uniqueUsers = this.statisticsDAL.GetUniqueUsers(startTime, rangeType);
            double averageSessionTime = this.statisticsDAL.GetAverageSessionTime(startTime, rangeType);
            XmlNode rootNode = statistics.AppendChild(statistics.CreateElement("Statistics"));
            XmlNode statsNode = rootNode.AppendChild(statistics.CreateElement("Statistics"));
            statsNode.AppendChild(statistics.CreateElement("UniqueUsers")).InnerText = uniqueUsers.ToString();
            statsNode.AppendChild(statistics.CreateElement("AverageSessionTime")).InnerText = averageSessionTime.ToString();

            for (int i = 0; i < sessionStarts.Length; ++i)
            {
                statsNode.AppendChild(statistics.CreateElement("SessionStarts")).InnerText = sessionStarts[i].ToString();
            }

            for (int i = 0; i < concurrentUsers.Length; ++i)
            {
                statsNode.AppendChild(statistics.CreateElement("ConcurrentUsers")).InnerText = concurrentUsers[i].ToString();
            }

            return statistics;
        }

        /// <summary>
        /// Go through the session list and remove old items.
        /// </summary>
        private void UpdateSessionList()
        {
            lock (this.sessionLock)
            {
                List<SessionIdentity> removeList = new List<SessionIdentity>();
                DateTime currentTime = DateTime.Now;

                foreach(var details in this.sessionDetails.Values)
                {
                    if ((currentTime - details.Record.ReceiveTime).TotalSeconds > (this.messageInterval * 2.0f))
                    {
                        DateTime endTime = details.Record.ReceiveTime.AddSeconds(this.messageInterval);
                        this.RecordUserSession(details, endTime);
                        removeList.Add(details.Record.Details.Session);
                    }
                }

                foreach (var details in removeList)
                {
                    this.sessionDetails.Remove(details);
                }
            }
        }

        /// <summary>
        /// Record a user session entry into the database.
        /// </summary>
        /// <param name="session">The session details.</param>
        /// <param name="endTime">The end time.</param>
        private void RecordUserSession(SessionDetails session, DateTime endTime)
        {
            this.statisticsDAL.RecordUserSession(
                session.Record.Details.Session.UserId,
                (endTime - session.StartTime).Minutes,
                session.StartTime);
        }

        /// <summary>
        /// Record a scene snapshot entry into the database.
        /// </summary>
        private void RecordSceneSnapshot()
        {
            XmlDocument statistics = this.GetStatistics();
            XmlNodeList sceneNodes = statistics.SelectNodes("/Statistics/Scenes/Scene");

            if (sceneNodes.Count > 0)
            {
                string[] categories = new string[sceneNodes.Count];
                long[] counts = new long[sceneNodes.Count];

                for (int i = 0; i < sceneNodes.Count; ++i)
                {
                    categories.SetValue(sceneNodes[i].SelectSingleNode("Name").InnerText, i);
                    counts.SetValue(Int64.Parse(sceneNodes[i].SelectSingleNode("Users").InnerText), i);
                }

                this.statisticsDAL.RecordSceneSnapshot(
                    DateTime.Now,
                    categories,
                    counts);
            }
        }

        /// <summary>
        /// Process the session list.
        /// </summary>
        private void ProcessSessionList()
        {
            DateTime previousTime = DateTime.Now;

            while (this.isRunning)
            {
                DateTime currentTime = DateTime.Now;
                double timeDelta = (currentTime - previousTime).TotalSeconds;

                if (timeDelta >= this.messageInterval)
                {
                    this.UpdateSessionList();
                    this.RecordSceneSnapshot();
                    previousTime = currentTime.AddSeconds(this.messageInterval - timeDelta);
                }

                Thread.Sleep(Parameters.ProcessInterval);
            }
        }

        /// <summary>
        /// Process the record queue.
        /// </summary>
        private void ProcessRecordQueue()
        {
            while (this.isRunning)
            {
                if (this.recordQueue.Count > 0)
                {
                    Queue<StatisticsDetailsRecord> records;

                    lock (this.recordLock)
                    {
                        records = this.recordQueue;
                        this.recordQueue = new Queue<StatisticsDetailsRecord>();
                    }

                    DateTime currentTime = DateTime.Now;
                    foreach (StatisticsDetailsRecord record in records)
                    {
                        SessionIdentity sessionId = record.Details.Session;

                        lock (this.sessionLock)
                        {
                            SessionDetails session;
                            if (this.sessionDetails.TryGetValue(sessionId, out session))
                            {
                                session.Record = record;
                            }
                            else
                            {
                                this.sessionDetails.Add(sessionId, new SessionDetails(record));
                            }
                        }
                    }
                }

                Thread.Sleep(Parameters.ProcessInterval);
            }
        }
    }
}
