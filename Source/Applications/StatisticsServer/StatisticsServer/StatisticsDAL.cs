﻿//---------------------------------------------------------------------------------
// <copyright file="StatisticsDAL.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace StatisticsServer
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Threading;

    using Statistics;

    /// <summary>
    /// The Data Access Layer for interacting with the database.
    /// </summary>
    internal class StatisticsDAL
    {
        /// <summary>
        /// Preferred format for DateTime string representation.
        /// </summary>
        private readonly string dateTimeFormat = "yyyy-MM-dd HH:mm:ss";

        /// <summary>
        /// List to queue up recording requests.
        /// </summary>
        private Queue<DbCommand> recordQueue = new Queue<DbCommand>();

        delegate DbConnection ConnectionFactory();

        /// <summary>
        /// Creates and returns a new connection object.
        /// </summary>
        private ConnectionFactory CreateConnection;

        /// <summary>
        /// Object used to lock access to recordQueue to make it threadsafe.
        /// </summary>
        private object recordLock = new object();

        /// <summary>
        /// The recording thread.
        /// </summary>
        private Thread recordThread;

        /// <summary>
        /// Flag indicating whether the recording thread is running.
        /// </summary>
        private bool isRunning = false;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="StatisticsDAL"/> class.
        /// </summary>
        public StatisticsDAL()
        {
        }

        /// <summary>
        /// Prepare the database.
        /// </summary>
        /// <param name="databaseProvider">The data provider to use for database access.</param>
        /// <param name="databaseConnectionString">A string used to connect to the database.</param>
        public void Initialise(string databaseProvider, string databaseConnectionString)
        {
            this.isRunning = true;

            var factory = DbProviderFactories.GetFactory(databaseProvider);
            this.CreateConnection = () =>
            {
                var conn = factory.CreateConnection();
                conn.ConnectionString = databaseConnectionString;
                conn.Open();
                return conn;
            };

            this.InitialiseDatabase();

            this.recordThread = new Thread(this.ProcessRecordQueue);
            this.recordThread.IsBackground = true;
            this.recordThread.Start();
        }

        /// <summary>
        /// Perform any needed clean-up.
        /// </summary>
        public void Shutdown()
        {
            this.isRunning = false;
        }

        /// <summary>
        /// Record a user session entry into the database.
        /// </summary>
        /// <param name="userId">The unique ID of the user.</param>
        /// <param name="totalMins">Total minutes of the session.</param>
        /// <param name="startTime">The start time of the session.</param>
        public void RecordUserSession(long userId, long totalMins, DateTime startTime)
        {
            this.Enqueue(this.Sql(
                @"INSERT INTO Sessions (UserID, TotalMins, StartTime) VALUES (@id, @mins, @startTime)",
                "id", userId.ToString(),
                "mins", totalMins.ToString(),
                "startTime", startTime.ToString(this.dateTimeFormat)
            ));
        }

        /// <summary>
        /// Record a scene snapshot entry into the database.
        /// </summary>
        /// <param name="time">The time stamp.</param>
        /// <param name="categories">List of current categories.</param>
        /// <param name="counts">List of current user counts.</param>
        public void RecordSceneSnapshot(DateTime time, string[] categories, long[] counts)
        {
            if ((categories.Length > 0) && (counts.Length > 0) && (categories.Length == counts.Length))
            {
                for (int i = 0; i < categories.Length; ++i)
                {
                    this.Enqueue(this.Sql(
                        "INSERT INTO Snapshots (Time, Category, Count) VALUES (@time, @category, @count)",
                        "time", time.ToString(this.dateTimeFormat),
                        "category", categories[i],
                        "count", counts[i].ToString()
                    ));
                }
            }
        }

        /// <summary>
        /// Get the number of session starts for a selected period.
        /// </summary>
        /// <param name="startTime">The start time.</param>
        /// <param name="rangeType">The range type.</param>
        /// <returns>List of session starts.</returns>
        public long[] GetSessionStarts(DateTime startTime, Parameters.RangeType rangeType)
        {
            int dataLength = 24;

            switch (rangeType)
            {
                default:
                case Parameters.RangeType.Day:
                    break;

                case Parameters.RangeType.Week:
                    dataLength = 28;
                    break;

                case Parameters.RangeType.Month:
                    dataLength = startTime.AddMonths(1).AddDays(-1).Day;
                    break;
            }

            long[] data = new long[dataLength];

            using (var connection = this.CreateConnection())
            using (IDbTransaction transaction = connection.BeginTransaction(IsolationLevel.Serializable))
            {
                for (int i = 0; i < dataLength; ++i)
                {
                    DateTime start = startTime.AddHours(i);
                    DateTime end = startTime.AddHours(i + 1);

                    switch (rangeType)
                    {
                        default:
                        case Parameters.RangeType.Day:
                            break;

                        case Parameters.RangeType.Week:
                            start = startTime.AddHours(i * 6);
                            end = startTime.AddHours((i * 6) + 6);
                            break;

                        case Parameters.RangeType.Month:
                            start = startTime.AddDays(i);
                            end = startTime.AddDays(i + 1);
                            break;
                    }

                    using (IDbCommand command = this.Sql("SELECT * FROM Sessions WHERE StartTime > @start AND StartTime <= @end",
                        "start", start.ToString(this.dateTimeFormat),
                        "end", end.ToString(this.dateTimeFormat)).Bind(transaction))
                    {
                        using (IDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                data[i]++;
                            }
                        }
                    }
                }

                transaction.Commit();
            }

            return data;
        }

        /// <summary>
        /// Get the maximum number of concurrent users for a selected period.
        /// </summary>
        /// <param name="startTime">The start time.</param>
        /// <param name="rangeType">The range type.</param>
        /// <returns>List of concurrent users.</returns>
        public long[] GetConcurrentUsers(DateTime startTime, Parameters.RangeType rangeType)
        {

            int dataLength = 24;
            switch (rangeType)
            {
                default:
                case Parameters.RangeType.Day:
                    break;

                case Parameters.RangeType.Week:
                    dataLength = 28;
                    break;

                case Parameters.RangeType.Month:
                    dataLength = startTime.AddMonths(1).AddDays(-1).Day;
                    break;
            }

            long[] data = new long[dataLength];

            using (var connection = this.CreateConnection())
            using (IDbTransaction transaction = connection.BeginTransaction(IsolationLevel.Serializable))
            {
                for (int i = 0; i < dataLength; ++i)
                {
                    DateTime start = startTime.AddHours(i);
                    DateTime end = startTime.AddHours(i + 1);

                    switch (rangeType)
                    {
                        default:
                        case Parameters.RangeType.Day:
                            break;

                        case Parameters.RangeType.Week:
                            start = startTime.AddHours(i * 6);
                            end = startTime.AddHours((i * 6) + 6);
                            break;

                        case Parameters.RangeType.Month:
                            start = startTime.AddDays(i);
                            end = startTime.AddDays(i + 1);
                            break;
                    }

                    using (var command = this.Sql("SELECT * FROM Snapshots WHERE Time > @start AND Time <= @end",
                        "start", start.ToString(this.dateTimeFormat),
                        "end", end.ToString(this.dateTimeFormat)).Bind(transaction))
                    {
                        using (IDataReader reader = command.ExecuteReader())
                        {
                            Dictionary<string, long> snapshotList = new Dictionary<string, long>();

                            while (reader.Read())
                            {
                                DateTime time = (DateTime)reader["Time"];
                                long count = (long)reader["Count"];
                                string timeString = time.ToString(this.dateTimeFormat);

                                if (snapshotList.ContainsKey(timeString))
                                {
                                    snapshotList[timeString] += count;
                                }
                                else
                                {
                                    snapshotList.Add(timeString, count);
                                }
                            }

                            Dictionary<string, long>.ValueCollection.Enumerator snapshot = snapshotList.Values.GetEnumerator();

                            while (snapshot.MoveNext())
                            {
                                data[i] = (snapshot.Current > data[i]) ? snapshot.Current : data[i];
                            }
                        }
                    }
                }

                transaction.Commit();
            }

            return data;
        }

        /// <summary>
        /// Get the number of unique users for a selected period.
        /// </summary>
        /// <param name="startTime">The start time.</param>
        /// <param name="rangeType">The range type.</param>
        /// <returns>Number of unique users.</returns>
        public long GetUniqueUsers(DateTime startTime, Parameters.RangeType rangeType)
        {
            long data = 0;
            DateTime endTime = startTime.AddDays(1.0f);

            switch (rangeType)
            {
                default:
                case Parameters.RangeType.Day:
                    break;

                case Parameters.RangeType.Week:
                    endTime = startTime.AddDays(7);
                    break;

                case Parameters.RangeType.Month:
                    endTime = startTime.AddMonths(1);
                    break;
            }

            using (var connection = this.CreateConnection())
            using (IDbTransaction transaction = connection.BeginTransaction(IsolationLevel.Serializable))
            {
                using (var command = this.Sql("SELECT * FROM Sessions WHERE StartTime > @start AND StartTime <= @end",
                    "start", startTime.ToString(this.dateTimeFormat),
                    "end", endTime.ToString(this.dateTimeFormat)).Bind(transaction))
                {
                    using (IDataReader reader = command.ExecuteReader())
                    {
                        List<long> uniqueUsers = new List<long>();

                        while (reader.Read())
                        {
                            long userId = (long)reader["UserID"];

                            if (!uniqueUsers.Contains(userId))
                            {
                                uniqueUsers.Add(userId);
                            }
                        }

                        data = uniqueUsers.Count;
                    }
                }

                transaction.Commit();
            }

            return data;
        }

        /// <summary>
        /// Get the average session time for a selected period.
        /// </summary>
        /// <param name="startTime">The start time.</param>
        /// <param name="rangeType">The range type.</param>
        /// <returns>Average session time.</returns>
        public double GetAverageSessionTime(DateTime startTime, Parameters.RangeType rangeType)
        {
            double data = 0.0f;

            DateTime endTime = startTime.AddDays(1);

            switch (rangeType)
            {
                default:
                case Parameters.RangeType.Day:
                    break;

                case Parameters.RangeType.Week:
                    endTime = startTime.AddDays(7);
                    break;

                case Parameters.RangeType.Month:
                    endTime = startTime.AddMonths(1);
                    break;
            }

            using (var connection = this.CreateConnection())
            using (IDbTransaction transaction = connection.BeginTransaction(IsolationLevel.Serializable))
            {
                using (var command = this.Sql("SELECT * FROM Sessions WHERE StartTime > @start AND StartTime <= @end",
                    "start", startTime.ToString(this.dateTimeFormat),
                    "end", endTime.ToString(this.dateTimeFormat)).Bind(transaction))
                {
                    using (IDataReader reader = command.ExecuteReader())
                    {
                        long sum = 0;
                        long sessionCount = 0;

                        while (reader.Read())
                        {
                            long totalMins = (long)reader["TotalMins"];
                            sum += totalMins;
                            sessionCount++;
                        }

                        if (sessionCount > 0)
                        {
                            data = (double)sum / sessionCount;
                        }
                    }
                }
            }

            return data;
        }

        /// <summary>
        /// Prepare the database.
        /// </summary>
        private void InitialiseDatabase()
        {
            using (var conn = this.CreateConnection())
            {
                if (!this.TableExists(conn, "Sessions"))
                {
                    this.Execute(this.Sql("CREATE TABLE Sessions (UserID INTEGER NOT NULL, TotalMins INTEGER NOT NULL, StartTime DATETIME NOT NULL)").Bind(conn));
                }

                if (!this.TableExists(conn, "Snapshots"))
                {
                    this.Execute(this.Sql("CREATE TABLE Snapshots (Time DATETIME NOT NULL, Category TEXT NOT NULL, Count INTEGER NOT NULL)").Bind(conn));
                }
            }
        }

        /// <summary>
        /// Check that a table exists in the database.
        /// </summary>
        /// <param name="tableName">Table name.</param>
        /// <returns>True, if table exists, otherwise false.</returns>
        private bool TableExists(DbConnection conn, string tableName)
        {
            DataTable dataTable = conn.GetSchema(
                "TABLES",
                new string[] { null, null, tableName });

            return dataTable.Rows.Count > 0;
        }

        /// <summary>
        /// Process the record queue.
        /// </summary>
        private void ProcessRecordQueue()
        {
            while (this.isRunning)
            {
                if (this.recordQueue.Count > 0)
                {
                    using (var connection = this.CreateConnection())
                    using (IDbTransaction transaction = connection.BeginTransaction(IsolationLevel.Serializable))
                    {
                        Queue<DbCommand> commands;

                        lock (this.recordLock)
                        {
                            commands =this.recordQueue;
                            this.recordQueue = new Queue<DbCommand>();
                        }

                        foreach (DbCommand command in commands)
                        {
                            this.Execute(command.Bind(transaction));
                        }

                        transaction.Commit();
                    }
                }

                Thread.Sleep(Parameters.ProcessInterval);
            }
        }

        /// <summary>
        /// Enqueues the specified command.
        /// </summary>
        /// <param name="command">The command.</param>
        protected void Enqueue(DbCommand command)
        {
            lock (this.recordLock)
            {
                this.recordQueue.Enqueue(command);
            }
        }

        /// <summary>
        /// Executes the specified command.
        /// Calls command.Dispose() once executed.
        /// </summary>
        /// <param name="command">The command.</param>
        protected void Execute(IDbCommand command)
        {
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                command.Dispose();
            }
        }

        /// <summary>
        /// Create a DBCommand from the given query &amp; params.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="args">The params.</param>
        /// <returns></returns>
        protected DbCommand Sql(string query, params object[] args)
        {
            return new DbCommand(query, args);
        }

        /// <summary>
        /// Like IDbCommand, but delays the requirement for an active connection until
        /// execution time
        /// </summary>
        protected class DbCommand
        {
            private readonly object[] args;
            private readonly string query;
            public DbCommand(string query, params object[] args)
            {
                this.query = query;
                this.args = args;
            }

            /// <summary>
            /// Returns a Command bound to the given connection.
            /// </summary>
            /// <param name="conn">The connection.</param>
            /// <returns>A Command</returns>
            public IDbCommand Bind(IDbConnection conn)
            {
                var command = conn.CreateCommand();
                this.PopulateCommand(command);
                return command;
            }

            /// <summary>
            /// Returns a Command bound to the given transaction.
            /// </summary>
            /// <param name="conn">The transaction.</param>
            /// <returns>A Command</returns>
            public IDbCommand Bind(IDbTransaction transaction)
            {
                var command = this.Bind(transaction.Connection);
                command.Transaction = transaction;
                return command;
            }

            /// <summary>
            /// Populates a DB command.
            /// </summary>
            /// <param name="command">The command.</param>
            /// <param name="query">The query.</param>
            /// <param name="args">The params.</param>
            private void PopulateCommand(IDbCommand command)
            {
                if (this.args.Length % 2 != 0)
                {
                    throw new ArgumentException("DbCommand has odd number of parameter arguments (key-value pairs are required)");
                }

                command.CommandText = query;
                for (int i = 0; i < this.args.Length; i += 2)
                {
                    string name = this.args[i] as string;
                    if (string.IsNullOrEmpty(name))
                    {
                        throw new ArgumentException("parameter name must be a non-empty string");
                    }

                    var param = command.CreateParameter();
                    param.ParameterName = name;
                    param.Value = this.args[i + 1];
                    command.Parameters.Add(param);
                }
            }
        }
    }
}