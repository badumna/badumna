﻿//---------------------------------------------------------------------------------
// <copyright file="StatisticsServerHostedProcess.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace StatisticsServer
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Xml;

    using GermHarness;

    using Mono.Options;

    using Statistics;
    using System.Diagnostics;

    /// <summary>
    /// The regular task processed by the Statistics Server.
    /// </summary>
    internal class StatisticsServerHostedProcess : IHostedProcess
    {
        /// <summary>
        /// Option set for parsing command line options.
        /// </summary>
        private readonly OptionSet optionSet;

        /// <summary>
        /// The tracker object. 
        /// </summary>
        private StatisticsTracker tracker;

        /// <summary>
        /// Port number of the Statistics Server.
        /// </summary>
        private int portNumber = Parameters.StatisticsServerPort;

        /// <summary>
        /// HTTP prefix to serve statistics over (null if HTTP server is not enabled).
        /// </summary>
        private string httpPrefix;

        /// <summary>
        /// HTTP server instance
        /// </summary>
        private HttpServer httpServer;

        /// <summary>
        /// Time interval in between statistics messages.
        /// </summary>
        private double messageInterval = Parameters.StatisticsMessageInterval.TotalSeconds;

        /// <summary>
        /// Initializes a new instance of the <see cref="StatisticsServerHostedProcess"/> class.
        /// </summary>
        public StatisticsServerHostedProcess()
        {
            this.optionSet = new OptionSet()
            {
                { "port-number=", "Port number for Statistics Server to listen for messages.", (int var) =>
                    {
                        this.portNumber = var;
                    }
                },
                { "message-interval=", "Time interval of statistics messages, in seconds.", (float var) =>
                    {
                        this.messageInterval = var;
                    }
                },
                { "http-serve=", "HTTP prefix to serve statistics on (e.g \"http://+:12345/\". Default: no statistics over HTTP).", (string var) =>
                    {
                        this.httpPrefix = var;
                    }
                }
            };
        }

        /// <summary>
        /// Statistics message types.
        /// </summary>
        public enum StatisticsMessageType : int
        {
            /// <summary>
            /// Get the statistics and scene data.
            /// </summary>
            Status = 0,

            /// <summary>
            /// Get the session activity data.
            /// </summary>
            Session,
        }

        /// <summary>
        /// Called when the peer harness has started, before any other calls are made.
        /// </summary>
        /// <param name="arguments">An array of arguments that were passed to the process.</param>
        public void OnInitialize(ref string[] arguments)
        {
            List<string> extras = this.optionSet.Parse(arguments);
            arguments = extras.ToArray();
        }

        /// <summary>
        /// The peer is shutting down. Called when either the process is about to end
        /// or a remote user has requested the process shutdown.
        /// </summary>
        public void OnShutdown()
        {
            if (this.httpServer != null)
            {
                this.httpServer.Stop();
                this.httpServer = null;
            }

            if (this.tracker != null)
            {
                this.tracker.Shutdown();
                this.tracker = null;
            }
        }

        /// <summary>
        /// Called when the process has received a request to start.
        /// Is called after OnInitialize() and before any other methods in the IPeer interface.
        /// </summary>
        /// <returns>Whether success.</returns>
        public bool OnStart()
        {
            this.tracker = new StatisticsTracker();

            this.tracker.Start(this.portNumber, this.messageInterval);
            if (this.httpPrefix != null)
            {
                this.httpServer = new HttpServer(this.httpPrefix, this);
                this.httpServer.Run();
            }

            return true;
        }

        /// <summary>
        /// Called at regular intervals (<see cref="Harness.ProcessNetworkStateIntervalMs">ProcessNetworkStateIntervalMs</see>).
        /// </summary>
        /// <param name="delayMilliseconds">The delay since the last call to this method</param>
        /// <returns>
        /// True if the wishes to continue in the run loop. If false the process will shutdown and exit.
        /// </returns>
        public bool OnPerformRegularTasks(int delayMilliseconds)
        {
            return true;
        }

        /// <summary>
        /// Process any remote requests.
        /// </summary>
        /// <param name="requestType">An id for the type of request</param>
        /// <param name="request">Any data associated with the request</param>
        /// <returns>The reply to the request</returns>
        public byte[] OnProcessRequest(int requestType, byte[] request)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                switch ((StatisticsMessageType)requestType)
                {
                    case StatisticsMessageType.Status:
                        this.WriteStatistics(stream);
                        break;

                    case StatisticsMessageType.Session:
                        DateTime startTime = DateTime.Today;
                        Parameters.RangeType rangeType = Parameters.RangeType.Day;

                        using (MemoryStream inputStream = new MemoryStream(request))
                        {
                            using (BinaryReader reader = new BinaryReader(inputStream))
                            {
                                long dateData = reader.ReadInt64();
                                startTime = DateTime.FromBinary(dateData);
                                rangeType = (Parameters.RangeType)reader.ReadInt32();
                            }
                        }

                        this.WriteSessionActivity(startTime, rangeType, stream);
                        break;

                    default:
                        break;
                }

                return stream.ToArray();
            }
        }

        /// <summary>
        /// Write list of supported command line arguments.
        /// </summary>
        /// <param name="tw">The text writer to use.</param>
        public void WriteOptionsDescription(TextWriter tw)
        {
            this.optionSet.WriteOptionDescriptions(tw);
        }

        /// <summary>
        /// Returns statistics.
        /// </summary>
        /// <returns>The stats summary.</returns>
        internal void WriteStatistics(Stream stream)
        {
            XmlDocument statistics = this.tracker.StatisticsManager.GetStatistics();
            ResponseUtil.WriteXml(statistics, stream);
        }
        
        /// <summary>
        /// Returns session activity.
        /// </summary>
        /// <param name="startTime">The start time.</param>
        /// <param name="rangeType">The range type.</param>
        /// <returns>The session activity.</returns>
        internal void WriteSessionActivity(DateTime startTime, Parameters.RangeType rangeType, Stream stream)
        {
            XmlDocument sessionActivity = this.tracker.StatisticsManager.GetSessionActivity(startTime, rangeType);
            ResponseUtil.WriteXml(sessionActivity, stream);
        }
    }
}