﻿//---------------------------------------------------------------------------------
// <copyright file="StatisticsTracker.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2011 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace StatisticsServer
{
    using System.Configuration;

    /// <summary>
    /// The statistics tracker is used to collect statistics messages and store them into permanent storage. 
    /// </summary>
    internal class StatisticsTracker
    {
        /// <summary>
        /// The manager used to keep statistics data.
        /// </summary>
        private StatisticsManager manager;

        /// <summary>
        /// The listener used to listen for incoming statistics messages.
        /// </summary>
        private Listener listener;

        /// <summary>
        /// Initializes a new instance of the <see cref="StatisticsTracker"/> class.
        /// </summary>
        public StatisticsTracker()
        {
        }

        /// <summary>
        /// Gets the stats manager.
        /// </summary>
        public StatisticsManager StatisticsManager
        {
            get { return this.manager; }
        }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        /// <param name="portNumber">Port number.</param>
        /// <param name="messageInterval">Message interval</param>
        public void Start(int portNumber, double messageInterval)
        {
            string databaseProvider = ConfigurationManager.AppSettings["dbProvider"];
            string databaseConnectionString = ConfigurationManager.ConnectionStrings[databaseProvider].ConnectionString;
            this.manager = new StatisticsManager(databaseProvider, databaseConnectionString, messageInterval);
            this.listener = new Listener(portNumber);
            this.listener.RecordReceived += this.manager.OnRecordReceived;
            this.listener.Start();
        }

        /// <summary>
        /// Shutdowns this instance.
        /// </summary>
        public void Shutdown()
        {
            this.manager.Shutdown();
            this.listener.Shutdown();
        }
    }
}