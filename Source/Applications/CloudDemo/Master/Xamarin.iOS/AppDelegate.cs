using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace BadumnaCloudDemo
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register ("AppDelegate")]
    public partial class AppDelegate : UIApplicationDelegate
    {
        public static string KeyPair;
        private const string KeyFile = "KeyPair.xml";
        private Game1 game;

        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching (UIApplication app, NSDictionary options)
        {
            var docs = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var path = Path.Combine(docs, "..", "Library", KeyFile);
            if (File.Exists (path))
            {
                KeyPair = File.ReadAllText(path);
            }
            else
            {
                KeyPair = Badumna.Security.UnverifiedIdentityProvider.GenerateKeyPair();
                File.WriteAllText(path, KeyPair);
            }
            this.game = new Game1 ();
            this.game.Run ();
            return true;
        }

        public override void WillTerminate (UIApplication application)
        {
            // Shut down gracefully on exit.
            if (this.game != null)
            {
                this.game.Shutdown();
            }
        }
    }
}

