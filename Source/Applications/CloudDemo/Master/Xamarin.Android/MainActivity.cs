using System;
using System.IO;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace BadumnaCloudDemo
{
    [Activity (Label = "BadumnaCloudDemo", MainLauncher = true)]
    public class Activity1 : Microsoft.Xna.Framework.AndroidGameActivity
    {
        public static string KeyPair;
        private const string KeyFile = "KeyPair.xml";
        private Game1 game;
        
        protected override void OnCreate (Bundle bundle)
        {
            base.OnCreate(bundle);
            
            var path = Path.Combine(
                System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal),
                KeyFile);
            if (File.Exists (path))
            {
                KeyPair = File.ReadAllText(path);
            }
            else
            {
                KeyPair = Badumna.Security.UnverifiedIdentityProvider.GenerateKeyPair();
                File.WriteAllText(path, KeyPair);
            }
        }
        
        protected override void OnResume()
        {
            Game1.Activity = this;
            this.game = new Game1();
            SetContentView(this.game.Window);
            base.OnResume();
            this.game.Run();
        }
        
        protected override void OnPause()
        {
            base.OnPause();
            this.game.Shutdown();
            this.game = null;
        }
    }
}


