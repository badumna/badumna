﻿using System;
using System.Collections.Generic;
using System.Linq;
using Badumna;
using Badumna.Core;
#if CHAT
using Badumna.Chat;
#endif
using Badumna.DataTypes;
#if MATCHMAKING
using Badumna.Matchmaking;
#endif
using Badumna.SpatialEntities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#if TOUCH
using Microsoft.Xna.Framework.Input.Touch;
#else
using Microsoft.Xna.Framework.Input;
#endif

namespace BadumnaCloudDemo
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
#if MATCHMAKING
        #region Instruction texts
        private const string MatchMakingInstructions = "Click to request match.";
        private const string GameInstructions = "Click to move. Race to the finish.";
        #endregion // Instruction texts

#endif
        #region Screen dimension fields
        private int screenWidth = 320;
        private int screenHeight = 480;
        #endregion // Screen dimensions

        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;

        #region Badumna fields
        private INetworkFacade network;
        private NetworkScene scene;
        private OriginalAvatar localAvatar;
        private List<Avatar> replicas = new List<Avatar>();
        private string characterName;
        #endregion

        #region Rendering and logic fields
        private Texture2D sprite;
        private Texture2D blankTexture;
        private SpriteFont font;
        private Vector2 spriteOrigin;
        private bool loggedIn;
#if TOUCH
#else
        private MouseState lastMouseState;
        private KeyboardState lastKeyboardState;
#endif
        private Queue<string> log = new Queue<string>();
        #endregion // Message log fields
#if CHAT

        #region Proximity chat channel
        private IChatChannel proximityChatChannel;
        #endregion // Proximity chat channel
#endif
#if MATCHMAKING

        #region Matchmaking fields
        private MatchmakingOptions matchmakingOptions;
        private bool canRequestMatch;
        private string instructions = String.Empty;
        private List<Color> colours = new List<Color> { Color.Red, Color.Yellow, Color.Green, Color.Blue };
        private List<Badumna.DataTypes.Vector3> startPoints = new List<Badumna.DataTypes.Vector3>
        {
            new Badumna.DataTypes.Vector3(50, 100, 0),
            new Badumna.DataTypes.Vector3(50, 150, 0),
            new Badumna.DataTypes.Vector3(50, 200, 0),
            new Badumna.DataTypes.Vector3(50, 250, 0)
        };
        #endregion // Matchmaking fields
#endif

        #region Constructor
        public Game1()
        {
            this.graphics = new GraphicsDeviceManager(this);
            this.Content.RootDirectory = "Content";
            #region // highlight
            this.IsMouseVisible = true;            
#if IOS
            // Deliberate height/width switcheroo.
            this.screenWidth = this.graphics.PreferredBackBufferHeight;
            this.screenHeight = this.graphics.PreferredBackBufferWidth;
#endif
#if WINDOWS
            this.graphics.PreferredBackBufferWidth = screenWidth;
            this.graphics.PreferredBackBufferHeight = screenHeight;
#endif
            #endregion // highlight
#if IOS || ANDROID
            this.graphics.SupportedOrientations = DisplayOrientation.Portrait;
#endif
#if TOUCH
            TouchPanel.EnabledGestures = GestureType.Tap |
#if CHAT
            GestureType.Hold |
#endif
            GestureType.Flick;
#endif
        }
        #endregion // Constructor

        #region Shutdown
        public void Shutdown()
        {
            this.loggedIn = false;

            if (this.scene != null)
            {
                this.scene.Leave();
                this.scene = null;
            }

            if (this.network != null)
            {
                this.network.Shutdown();
                this.network = null;
            }
        }
        #endregion // Shutdown

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        #region Initialize method (Replication)
        #region Initialize method (Chat)
        #region Initialize method (Matchmaking)
        protected override void Initialize()
        {
            #region highlight Initialize method (Replication)
            this.WriteToLog("Initializing Badumna...");
#if PRO
            var options = new Options();
            options.Connectivity.ApplicationName = "dotnet-example";

            // The type of discovery mechanism to use.
            // Badumna requires the knowledge of one existing peer to connect with in
            // order to join the network. The address of any other peer can be 'discovered' through
            // SeedPeers(peers that are 'always' running).

            // Use LAN mode for testing
            options.Connectivity.ConfigureForLan();

            // To use on the Internet, comment out the ConfigureForLan() line above,
            // uncomment the code below and add a known seed peer.  e.g.:
            // options.Connectivity.SeedPeers.Add("seedpeer.example.com:21251");

#endif
            NetworkFacade.BeginCreate(
#if PRO
                options,
#else
                "Put your ID here!",
#endif
                result =>
                {
                    try
                    {
                        this.network = NetworkFacade.EndCreate(result);
                        this.WriteToLog("Badumna initialization succeeded.");
                    }
                    catch (BadumnaException ex)
                    {
                        this.WriteToLog("Badumna initialization failed.");
                        this.WriteToLog(ex.Message);
                        this.WriteToLog("Please check:");
#if PRO
                        this.WriteToLog(" - your network configuration");
#else
                        this.WriteToLog(" - your application ID");
#endif
                        this.WriteToLog(" - your internet connection");
                        return;
                    }

                    this.network.RegisterEntityDetails(150f, 50f);
                    this.network.TypeRegistry.RegisterValueType<Color>(
                        (c, w) =>
                        {
                            w.Write(c.R);
                            w.Write(c.G);
                            w.Write(c.B);
                            w.Write(c.A);
                        },
                        r => new Color(r.ReadByte(), r.ReadByte(), r.ReadByte(), r.ReadByte()));
                    this.characterName = "Player " + new Random().Next(1000).ToString("000");
#if ANDROID
                    if (!this.network.Login(this.characterName, Activity1.KeyPair))
#else
#if IOS
                    if (!this.network.Login(this.characterName, AppDelegate.KeyPair))
#else
                    if (!this.network.Login(this.characterName))
#endif
#endif
                    {
                        this.WriteToLog("Failed to log into Badumna.");
                    }

#if MATCHMAKING
                    #region highlight Initialize method (Matchmaking)
                    this.matchmakingOptions = new MatchmakingOptions
                    {
                        MinimumPlayers = 2,
                        MaximumPlayers = 4,
                        Timeout = TimeSpan.FromSeconds(10)
                    };

                    this.canRequestMatch = true;
                    this.instructions = MatchMakingInstructions;
                    #endregion // highlight Initialize method (Matchmaking)
#else
                    this.localAvatar = new OriginalAvatar(new Badumna.DataTypes.Vector3(50, 50, 0));
                    this.scene = this.network.JoinScene("scene1", this.CreateReplica, this.RemoveReplica);
                    this.scene.RegisterEntity(this.localAvatar, 0, 15f, 150f);
#if CHAT

#region highlight Initialize method (Chat)
                    this.proximityChatChannel = this.network.ChatSession.SubscribeToProximityChannel(
                        this.localAvatar,
                        this.HandleChatMessage);
#endregion // highlight Initialize method (Chat)
#endif
#endif
                    this.loggedIn = true;
                });
            #endregion // highlight Initialize method (Replication)

            base.Initialize();
        }
        #endregion // Initialize method (Matchmaking)
        #endregion // Initialize method (Chat)
        #endregion // Initialize method (Replication)

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        #region LoadContent method
        protected override void LoadContent()
        {
#if ANDROID
            this.screenWidth = AndroidGameActivity.Game.Window.Size.Width;
            this.screenHeight = AndroidGameActivity.Game.Window.Size.Height;
#endif
            // Create a new SpriteBatch, which can be used to draw textures.
            this.spriteBatch = new SpriteBatch(GraphicsDevice);

            #region highlight
            this.sprite = Content.Load<Texture2D>("Avatar");
            this.spriteOrigin = new Vector2(this.sprite.Width / 2f, this.sprite.Height / 2f);
            this.font = Content.Load<SpriteFont>("Font");
            this.blankTexture = Content.Load<Texture2D>("Pixel");
            #endregion // highlight
        }
        #endregion

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        #region Update method (Replication)
        #region Update method (Chat)
        #region Update method (Matchmaking)
        protected override void Update(GameTime gameTime)
        {
            #region highlight Update method (Replication)
            if (this.loggedIn)
            {
                this.network.ProcessNetworkState();
#if TOUCH
                GestureSample gesture = default(GestureSample);
                if (TouchPanel.IsGestureAvailable)
                {
                    gesture = TouchPanel.ReadGesture();
                }

#else
                MouseState mouseState = Mouse.GetState();
                KeyboardState keyboardState = Keyboard.GetState();
#endif
#if MATCHMAKING
#region highlight Update method (Matchmaking)
                if (this.canRequestMatch)
                {
#if TOUCH
                    if (gesture.GestureType == GestureType.Tap)
#else
                    if (this.IsActive && mouseState.LeftButton == ButtonState.Pressed &&
                            this.lastMouseState.LeftButton == ButtonState.Released)
#endif
                    {
                        this.canRequestMatch = false;
                        this.instructions = "Await matchmaking result.";
                        this.network.BeginMatchmaking(
                            this.OnMatchmakingCompletion,
                            this.OnMatchmakingProgress,
                            this.matchmakingOptions);
                    }
                }
                else if (this.localAvatar != null)
#endregion highlight Update method (Matchmaking)
#else
                if (this.localAvatar != null)
#endif
                {
#if TOUCH
                    if (gesture.GestureType == GestureType.Tap)
                    {
                        this.localAvatar.SetTarget(gesture.Position.X, gesture.Position.Y);
                    }
#else
                    if (this.IsActive && mouseState.LeftButton == ButtonState.Pressed &&
                        this.lastMouseState.LeftButton == ButtonState.Released)
                    {
                        this.localAvatar.SetTarget(mouseState.X, mouseState.Y);
                    }

#endif
#if MATCHMAKING
#else
#if TOUCH
                    if (gesture.GestureType == GestureType.Flick)
#else
                    if (this.IsActive && keyboardState.IsKeyDown(Keys.C) &&
                        this.lastKeyboardState.IsKeyUp(Keys.C))
#endif
                    {
                        var random = new Random();
                        this.localAvatar.Colour = new Color(
                            random.Next(255),
                            random.Next(255),
                            random.Next(255));
                    }
#endif
#if CHAT

                    #region highlight Update method (Chat)
#if TOUCH
                    if (gesture.GestureType == GestureType.Hold)
#else
                    if (this.IsActive && keyboardState.IsKeyDown(Keys.Space) &&
                        this.lastKeyboardState.IsKeyUp(Keys.Space))
#endif
                    {
                        var message = "Hello!";
                        var chatMessage = this.characterName + ": " + message;
                        this.proximityChatChannel.SendMessage(chatMessage);
                        this.WriteToLog("You: " + message);
                    }
#endregion // highlight Update method (Chat)
#endif

                    this.localAvatar.Update(gameTime.ElapsedGameTime.TotalSeconds);
#if MATCHMAKING

#region highlight Update method (Matchmaking)
                    if (this.localAvatar.Position.X > screenWidth - 60)
                    {
                        this.scene.Leave();
                        this.scene = null;
                        this.localAvatar = null;
                        this.canRequestMatch = true;
                        this.instructions = MatchMakingInstructions;
                    }
#endregion // highlight Update method (Matchmaking)
#endif
                }

#if TOUCH
#else
                this.lastMouseState = mouseState;
                this.lastKeyboardState = keyboardState;
#endif
            }
            #endregion // highlight Update method (Replication)

            base.Update(gameTime);
        }
        #endregion // Update method (Matchmaking)
        #endregion // Update method (Chat)
        #endregion // Update method (Replication)
#if WINDOWS

        #region OnExiting method
        protected override void  OnExiting(object sender, EventArgs args)
        {
            base.OnExiting(sender, args);
            this.Shutdown();
        }
        #endregion // OnExiting method
#endif

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        #region Draw method (Replication)
        #region Draw method (Matchmaking)
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.DarkOrange);

            this.spriteBatch.Begin();
#if MATCHMAKING

            #region highlight Draw method (Matchmaking)
            // Draw racetrack
            this.spriteBatch.Draw(this.blankTexture, new Rectangle(60, 50, 2, screenHeight - 225), Color.White);
            this.spriteBatch.Draw(this.blankTexture, new Rectangle(screenWidth - 60, 50, 2, screenHeight - 225), Color.White);
            this.spriteBatch.DrawString(this.font, "Start", new Vector2(85, 50), Color.White, (float)Math.PI / 2, Vector2.Zero, 1, 0, 0);
            this.spriteBatch.DrawString(this.font, "Finish", new Vector2(screenWidth - 60, 50), Color.White, (float)Math.PI / 2, Vector2.Zero, 1, 0, 0);
            #endregion // highlight Draw method (Matchmaking)
#endif

            #region highlight Draw method (Replication)
            // Draw layout
            this.spriteBatch.Draw(this.blankTexture, new Rectangle(0, screenHeight - 125, screenWidth, 25), Color.Black);
            this.spriteBatch.Draw(this.blankTexture, new Rectangle(0, screenHeight - 100, screenWidth, 100), Color.White);

            // Draw log messages
            Vector2 textLocation = new Vector2(0, screenHeight - 100);
            foreach (var message in this.log)
            {
                this.spriteBatch.DrawString(this.font, message, textLocation, Color.Black);
                textLocation.Y += 20;
            }
            #endregion highlight Draw method (Replication)

#if MATCHMAKING
#region highlight Draw method (Matchmaking)
            // Draw instructions
            this.spriteBatch.DrawString(this.font, this.instructions, new Vector2(0, screenHeight - 125), Color.White);
#endregion // highlight Draw method (Matchmaking)

#endif
            #region highlight Draw method (Replication)
            // Draw avatars
            if (this.localAvatar != null)
            {
                this.DrawAvatar(this.localAvatar);
            }

            foreach (var replica in this.replicas)
            {
                this.DrawAvatar(replica);
            }
            #endregion highlight Draw method (Replication)

            this.spriteBatch.End();
            base.Draw(gameTime);
        }
        #endregion // Draw method (Matchmaking)
        #endregion // Draw method (Replication)

        /// <summary>
        /// Draw an avatar (can be original or replica).
        /// </summary>
        /// <param name="avatar">The avatar to draw.</param>
        #region DrawAvatar method
        private void DrawAvatar(Avatar avatar)
        {
            var pos = new Vector2(avatar.Position.X, avatar.Position.Y);
            this.spriteBatch.Draw(
                this.sprite, pos, null, avatar.Colour, 0f, this.spriteOrigin, 1f, 0, 0f);
        }
        #endregion // DrawAvatar method

        #region Replica delegates
        /// <summary>
        /// Called by Badumna to create an object representing a replica entity.
        /// </summary>
        /// <param name="scene">The scene the replica belongs to.</param>
        /// <param name="entityid">The ID assigned to the entity.</param>
        /// <param name="entitytype">An integer indicating the type of the entity.</param>
        /// <returns>A new replica.</returns>
        private IReplicableEntity CreateReplica(
            NetworkScene scene,
            BadumnaId entityid,
            uint entitytype)
        {
            var replica = new Avatar();
            this.replicas.Add(replica);
            return replica;
        }

        /// <summary>
        /// Called by Badumna to remove a replica from a scene.
        /// </summary>
        /// <param name="scene">The scene the replica belongs to.</param>
        /// <param name="replica">The replica to remove.</param>
        private void RemoveReplica(NetworkScene scene, IReplicableEntity replica)
        {
            this.replicas.Remove(replica as Avatar);
        }
        #endregion
#if MATCHMAKING

        #region Matchmaking delegates
        private void OnMatchmakingCompletion(MatchmakingAsyncResult result)
        {
            if (result.Succeeded)
            {
                var match = result.Match;
                this.instructions = GameInstructions;
                this.localAvatar = new OriginalAvatar(this.startPoints[match.PlayerId]);
                this.localAvatar.Colour = this.colours[match.PlayerId];
                this.scene = this.network.JoinMiniScene(match.RoomId, this.CreateReplica, this.RemoveReplica);
                this.scene.RegisterEntity(this.localAvatar, 0, 15f, 150f);
#if CHAT
                this.proximityChatChannel = this.network.ChatSession.SubscribeToProximityChannel(
                    this.localAvatar,
                    this.HandleChatMessage);
#endif
                this.WriteToLog("Match made.");
            }
            else
            {
                this.canRequestMatch = true;
                this.instructions = MatchMakingInstructions;
                string errorMessage;
                switch (result.Error)
                {
                    case MatchmakingError.ConnectionFailed:
                        errorMessage = "Connection failed!";
                        break;
                    case MatchmakingError.Timeout:
                        errorMessage = "Timed out finding a match.";
                        break;
                    default:
                        errorMessage = "Unknown error";
                        break;
                }
                this.WriteToLog(errorMessage);
            }
        }

        private void OnMatchmakingProgress(MatchmakingProgressEvent update)
        {
            this.WriteToLog(update.ToString());
        }
        #endregion // Matchmaking delegates
#endif
#if CHAT

        #region Chat handler
        private void HandleChatMessage(IChatChannel channel, BadumnaId userId, string message)
        {
            this.WriteToLog(message);
        }
        #endregion // Chat handler
#endif

        #region Write log message
        private void WriteToLog(string message)
        {
            this.log.Enqueue(DateTime.Now.ToString("HH:mm:ss") + " " + message);
            
            // Cull old log messages.
            while (this.log.Count > 5)
            {
                this.log.Dequeue();
            }
        }
        #endregion // Write log message
    }
}
