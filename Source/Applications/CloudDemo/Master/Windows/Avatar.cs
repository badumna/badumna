﻿//-----------------------------------------------------------------------
// <copyright file="Avatar.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaCloudDemo
{
    using System;
    using System.IO;
    using Badumna;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;
    using Color = Microsoft.Xna.Framework.Color;

    // Base class for simulation avatars.
    #region Avatar class
    public class Avatar : IReplicableEntity
    {
        // All entities must have a position which is automatically replicated.
        [Smoothing(Interpolation=200, Extrapolation=0)]
        public Vector3 Position { get; set; }

        // The colour of the avatar is marked as a replicable property.
        [Replicable]
        public Color Colour { get; set; }
    }
    #endregion
}