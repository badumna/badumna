
import os
import sys
import logging
import shutil
import errno
import subprocess
import functools
import argparse
import copy
import time
import re
import contextlib
import itertools
from fnmatch import fnmatch
from zipfile import ZipFile
from glob import iglob

logger = logging.getLogger()

def RP(*path):
    return os.path.normpath(os.path.join(*path))

def P(*path):
    return os.path.abspath(RP(*path))

def mkdir(path):
    """Make a directory, no worries if it already exists.

    Keep trying up to 5 times if windows gives us a bogus EACCES when it shouldn't, see:
    http://stackoverflow.com/questions/4609572/what-is-a-good-solution-to-a-bogus-oserror-13-eacces-using-python-on-windows
    """
    tries = 5
    while True:
        try:
            os.makedirs(path)
        except OSError, e:
            if e.errno != errno.EEXIST:
                if tries > 0 and e.errno == errno.EACCES:
                    logger.debug("Error making directory {}: {} ({} tries left)".format(path, e, tries))
                    tries -= 1
                    time.sleep(0.3)
                    continue
                else:
                    raise
        break

def is_windows():
    return sys.platform in ('win32','cygwin')

@contextlib.contextmanager
def working_dir(path):
    initial = os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(initial)

def copy_tree(src, dest):
    logger.info("Copying {} -> {}".format(src, dest))
    shutil.copytree(src, dest)

def copy_file(src, dest):
    logger.info("Copying {} -> {}".format(src, dest))
    shutil.copy(src, dest)

def to_posix_path(p):
    if not is_windows(): return p
    assert os.path.isabs(p), "to_posix_path() assumes an absolute path!"
    drive, path = os.path.splitdrive(p)
    path = path.lstrip(os.path.sep)
    drive = drive.rstrip(':')
    path_parts = [drive] + (path.split("\\"))
    return "/" + "/".join(path_parts)

def glob_dir(base, glob='*'):
    '''like glob("<base>/*"), but doesn't fail when `base` contains special characters'''
    return [os.path.join(base, f) for f in os.listdir(base) if fnmatch(f, glob)]

def erase(path):
    """Delete a file, no worries if the file doesn't exist."""
    if not os.path.isabs(path):
        raise ValueError('erase only accepts absolute paths')

    if os.path.exists(path):
        logger.debug("erasing: %s", path)
        if os.path.isdir(path):
            shutil.rmtree(path)
        else:
            os.remove(path)
    else:
        logger.info("not erasing nonexistant path: %s", path)

def cleanup(path, patterns, recurse=False):
    """Delete the files matching the given patterns in path.
       Be careful to ensure that the patterns are safe.
    """
    if not os.path.isabs(path):
        raise ValueError('cleanup only accepts absolute paths')

    if isinstance(patterns, basestring):
        patterns = [patterns]

    if any('..' in x for x in patterns):
        raise ValueError('parent directory references are not allowed')

    if any(len(x) == 0 for x in patterns):
        raise ValueError('empty patterns are not allowed')

    if recurse:
        paths = [x[0] for x in os.walk(path)]
    else:
        paths = [path]

    for p in paths:
        for pattern in patterns:
            for filename in iglob(P(p, pattern)):
                erase(filename)

def make_clean_dir(path):
    """Ensure the path is empty and exists."""
    if not os.path.isabs(path):
        raise ValueError('make_clean_dir only accepts absolute paths')

    logger.debug("Making clean dir at: {}".format(path))
    if os.path.exists(path):
        shutil.rmtree(path)
    mkdir(path)

def add_path(p, warn=True):
    """Add an item to $PATH, skipping if it's already present.
    Warns (and does nothing) if the path doesn't point to an existing directory"""
    paths = os.environ.get('PATH','').split(os.pathsep)
    if p not in paths:
        if not os.path.isdir(p):
            if warn:
                logger.warning("Warning: attempted to add a nonexistent PATH: " + p)
            return

        logger.debug("Adding to $PATH: " + p)
        paths.append(p)
        os.environ['PATH'] = os.pathsep.join(paths)
        # logger.debug("PATH=" + os.environ['PATH'])

def tree(root, desc="Contents of %s:", level=logging.DEBUG):
    logger.log(level, desc, root)
    for path, file in walk_files(root):
        logger.log(level, " - %s", path[len(root) + 1:])

def walk_files(root):
    '''yields all files (recursively) in a given root'''
    assert os.path.exists(root)
    for (path, dirs, files) in os.walk(root):
        for file in files:
            yield (os.path.join(path, file), file)


@functools.total_ordering
class TaskName(object):
    def __init__(self, task_name):
        object.__init__(self)
        
        name, part, config = task_name.partition('[')
        config = part + config

        # For showing back to the user
        self.orig_name = name
        self.orig_config = config

        self.name = set(n for n in name.split(':') if len(n) > 0)

        if len(config) == 0:
            self.config = None    # Unspecified, for task definitions
        else:
            assert config[0] == '[' and config[-1] == ']'
            self.config = set(c for c in config[1:-1].split(',') if len(c) > 0)

    def __str__(self):
        return self.orig_name + self.orig_config

    def __repr__(self):
        return 'TaskName({})'.format(str(self))

    def __eq__(self, other):
        return self.name == other.name and self.config == other.config

    def __ne__(self, other):
        return not (self == other)

    def __lt__(self, other):
        return self.orig_name < other.orig_name

    def __hash__(self):
        return hash(str(sorted(self.name)))

class TaskSelector(TaskName):
    def __init__(self, selector):
        TaskName.__init__(self, selector)
        if self.config is None:
            # selectors must always have a config because if matched to a name that has no config we will pass on this config
            self.config = set()
            self.orig_config = '[]'

    def match(self, task_name):
        if self.name.issubset(task_name.name):
            if task_name.config == self.config:
                return task_name
            elif task_name.config is None:
                return TaskName(task_name.orig_name + self.orig_config)
        return None

    def matches(self, task_names):
        """Return the task names which match this selector.  All returned names will have a config specified."""
        for task_name in task_names:
            m = self.match(task_name)
            if m is not None:
                yield m

def to_taskname(name):
    return name if isinstance(name, TaskName) else TaskName(name)

class Task(object):
    _args = []

    def __init__(self, name, function, deps=[], **kwargs):
        object.__init__(self)
        self.name = to_taskname(name)
        assert self.name.config is None, "Tasks must not specify a config"   # config is assigned by gen_task, which doesn't use this constructor.
        self.function = function
        self.deps = [to_taskname(d) for d in deps]
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def run(self):
        if self._args: # if args have been given, make sure the function is expecting them:
            logger.debug("Running task with args: %s" % (self._args,))
            self.function(self, copy.copy(self._args))
        else:
            self.function(self)

    def __repr__(self):
        return 'Task({}, {}, {}, ...)'.format(repr(self.name), self.function.func_name, repr(self.deps))


class SelectionScope(object):
    '''A scope, used to group task invocations (SelectionNode)s that can be interleaved'''
    _next_id = 0
    def __init__(self):
        self._scope = {}
        self._id = SelectionScope._next_id
        SelectionScope._next_id += 1

    def get_or_create(self, name, defined_tasks):
        '''
        Returns an existing node if present, otherwise creates (and adds) a new one
        '''
        if name not in self._scope:
            self.add(name, defined_tasks)
        return self._scope[name]
    
    def add(self, name, defined_tasks):
        assert name not in self._scope
        created = SelectionNode(gen_task(name, defined_tasks), scope=self)
        self._scope[name] = created
        return created

    def keys(self):
        return self._scope.keys()

    def __getitem__(self, name):
        return self._scope[name]

    def __contains__(self, name):
        return name in self._scope

    def tasks(self):
        return self._scope.values()
    
    @property
    def id(self):
        return self._id

    def __str__(self):
        return "Scope#%s" % (self._id,)

class SelectionNode(object):
    '''
    A SelectionNode is a particular appearance of a given task in the selection (dependency) graph.
    A node has a task and a scope - a task will never appear twice in the same scope, but may
    appear multiple times in different scopes.

    If a task has a `strict_ordering` attribute of `True`, dependencies will be run
    in the order specified (otherwise, they will be run in an arbitrary order)'.

    For a strict_ordering task, each direct dependency will have its own scope, which means
    that all transitive dependencies will be run right before the dependency itself,
    and not interleaved with tasks from other scopes. This is to protect against
    tasks which clobber each other's outputs, or even themselves (e.g when run in a different config)
    '''
    def __init__(self, task, scope):
        self.task = task
        self.scope = scope
        self.deps = None
        self.has_run = False
        self.strict_ordering = getattr(self.task, 'strict_ordering', False)

    def expand_dependencies(self, defined_tasks):
        logger.debug("Expanding deps for %s", self)
        if self.deps is not None:
            logger.debug("Skipping expand_dependencies() on %s - already done", self)

        def make_dependency(depname, scope):
            return scope.get_or_create(depname, defined_tasks)

        if self.strict_ordering:
            # dependencies is an ordered list, with a new scope per task
            self.deps = list([make_dependency(name, scope=SelectionScope()) for name in self.task.deps])
        else:
            # dependencies is a set, sharing the current task's scope
            self.deps = set([make_dependency(name, scope=self.scope) for name in self.task.deps])

        for dep in self.deps:
            dep.expand_dependencies(defined_tasks)

    def traverse(self):
        '''
        Traverse this node's dependencies (and the node itself)
        in an order which satisfies ordering constraints with respect to dependencies
        and the `strict_ordering` property of a selection.
        '''

        assert self.deps is not None, "expand_dependencies not called on %r" % (self,)
        for child in self.deps:
            for task in child.traverse():
                yield task
        if not self.has_run: yield self

    def __repr__(self):
        return "<#SelectionNode %s (%s)>" % (self.name, self.scope)

    def set_args(self, args):
        self.task._args = args

    @property
    def name(self):
        return self.task.name

    @property
    def runnable(self):
        return all(dep.has_run for dep in self.deps)

    def run(self):
        assert self.runnable, "Task is not runnable!"
        try:
            self.task.run()
        finally:
            self.mark_as_run()

    def mark_as_run(self):
        self.has_run = True

def no_op(*a):
    pass

def check_output(cmd, *a, **k):
    '''Wrapper around subprocess.check_output with error reporting & logging'''
    logger.debug("running: " + repr(cmd))
    try:
        return subprocess.check_output(cmd, *a, **k)
    except OSError as e:
        logger.warning("Failed to run command %r" % (cmd))
        raise

def recursively_unzip(base):
    for path, file in walk_files(base):
        if ext(path) == 'zip':
            dest = os.path.splitext(path)[0]
            unzip(path, dest = dest)
            recursively_unzip(dest)

def unzip(path, dest):
    logger.info("Unzipping %s -> %s", path, dest)
    assert not os.path.exists(dest), "destination {} already exists!".format(dest)
    os.makedirs(dest)
    ZipFile(path).extractall(dest)
    erase(path)

def ext(filename):
    '''get just the extension from a filename'''
    return os.path.splitext(filename)[1][1:]

def check_call(cmd, *a, **k):
    '''Wrapper around subprocess.check_call with error reporting & logging'''
    logger.debug("running: %r\n  in directory: %s", cmd, k.get('cwd', os.getcwd()))
    try:
        return subprocess.check_call(cmd, *a, **k)
    except OSError as e:
        logger.warning("Failed to run command %r" % (cmd))
        raise

def check_bash(cmd, *a, **k):
    '''Run a shell script with bash (uses Cygwin if on Windows).'''
    # Not just running bash from path, because I think the C++ Makefile expects to find MinGW in the path?
    if sys.platform == 'win32':
        shell = ['/cygwin/bin/bash', '--login']
    else:
        shell = ['bash']
    check_call(shell + cmd, *a, **k)

def task_exec(task):
    check_call(task.command_line, cwd=task.working_dir)

def msbuild(project, target=None, properties={}, args=[], keep_output=False):
    command = ['msbuild', project]
    if target is not None:
        command.append('/t:' + target)
    if not logger.isEnabledFor(logging.DEBUG):
        command.append('/verbosity:minimal')
    command.append('/nologo')
    command.extend(['/p:{}={}'.format(*x) for x in properties.items()])
    command.extend(args)

    if keep_output:
        return check_output(command)
    else:
        check_call(command)

def extract_subproject_paths(sln_path):
    project_paths = {}
    # Ack... we want to use the .sln for building demos etc (to make sure it works as-distributed), but
    # then we have no good way to extract the assembly paths for component projects,
    # because they are not individual tasks.
    #
    # (so here's a bad way!)
    base = os.path.dirname(sln_path)
    with open(sln_path) as sln:
        for line in sln:
            match = re.match(r'Project.*"(?P<name>[^"]+)", *"(?P<path>[^"]+\.[a-z]*proj)"', line)
            if match is not None:
                groups = match.groupdict()
                project_paths[groups['name']] = P(base, groups['path'])
    logger.debug("Extracted projects from %s:\n%r", sln_path, project_paths)
    return project_paths

def run_target(target, opts):
    global CURRENT_GROUP
    failed = False

    for task in target.traverse():

        logger.info('Performing %s (%s)' % (task.name, task.scope))
        start_time = time.time()

        assert not task.has_run, "Task is running twice!"
        
        if opts.is_check_run or (opts.nodeps and task is not target):
            task.mark_as_run()
            continue

        try_again = True
        while try_again:
            try_again = False
            try:
                #TODO: eliminate hacky global
                CURRENT_GROUP = task.scope
                task.run()
            except KeyboardInterrupt:
                raise
            except Exception as e:
                if opts.retry:
                    logging.error("Caught %s (but --retry is set): %s", type(e).__name__, e, exc_info=True)
                    print "Press RETURN to re-run task %s (or ctrl-c to cancel)" % (task,)
                    sys.stdout.flush()
                    sys.stderr.flush()
                    raw_input()
                    try_again = True
                    continue

                if opts.keep_going:
                    logging.error("Caught %s (but --keep-going is set): %s", type(e).__name__, e, exc_info=True)
                    failed = True
                else:
                    raise

        logger.info('Completed %s (%s) in %i seconds' % (task.name, task.scope, time.time() - start_time))

    assert target.has_run, "No remaining task is runnable (bad dependencies?)"
    assert not failed, "One or more tasks failed"

def go(targets, opts):
    failed = False
    for target in targets:
        logger.info("Beginning new top-level target: %s", target.name)
        try:
            run_target(target, opts)
        except AssertionError as e:
            if opts.keep_going:
                failed = True
            else:
                raise
    assert not failed, "One or more tasks failed"


def dump_dependencies(filename, tasks):
    scopes = set()

    for task in tasks:
        scopes.add(task.scope)

    def fmt(node):
        return "%s; %s" % (node.name, node.scope)

    with open(filename, 'w') as out:
        out.write('digraph {\n')
        for scope in scopes:
            for task in scope.tasks():
                for dep in task.deps:
                    out.write('    "{}" -> "{}";\n'.format(fmt(dep), fmt(task)))

            out.write('    subgraph cluster_%s {\n' % (scope.id))
            for task in scope.tasks():
                out.write('        "{}";\n'.format(fmt(task)))
            out.write('        color=blue;\n    }\n')
        out.write('}\n')

def list_targets(tasks):
    for task in sorted(tasks, key=lambda x: x.name):
        print task.name

def make_concrete_task_name(task_name, inherited_config):
    task_name = to_taskname(task_name)

    config = task_name.config if task_name.config is not None else set()
    # Currently the final config is the union of the parent config and any directly specified config.
    # However, if the directly specified config includes '~', then the parent config is not inherited.
    # Additionally, if the directly specified config includes '~keyword', then keyword will be removed
    # from the inherited parent config if it's present.

    if '~' in config:
        config.remove('~')
        inherited = set()
    else:
        inherited = set(inherited_config)

    to_remove = [c for c in config if c.startswith('~')]
    config.difference_update(to_remove)
    inherited.difference_update(c[1:] for c in to_remove)

    config.update(inherited)

    return TaskName('{}[{}]'.format(task_name.orig_name, ','.join(config)))


def gen_task(task_name, defined_tasks):
    assert task_name.config is not None, "Can't generate a runnable task without a config"
    task = copy.copy(defined_tasks[TaskName(task_name.orig_name)])
    task.name = task_name
    task.deps = [make_concrete_task_name(d, task_name.config) for d in task.deps]
    return task

def get_current_task(task_name):
    global CURRENT_GROUP
    if not task_name in CURRENT_GROUP:
        logging.warn("get_current_task() called for unknown task: %s" % (task_name,))
        logging.debug("current tasks:\n" + "\n".join([" - %s" % (name,) for name in CURRENT_GROUP.keys()]))
        return None
    return CURRENT_GROUP[task_name].task

def parse_args():
    parser = argparse.ArgumentParser(description='Build Badumna.\nAll arguments after a lone "--" will be given to the named task')
    parser.add_argument('-c', '--check', dest='is_check_run', action='store_true', help='list tasks to be executed only, don''t execute them')
    parser.add_argument('--dump', help='dumps the dependency structure to a dot file which can be visualized with GraphViz (e.g. "dot -Tsvg -O dump.dot")')
    parser.add_argument('-l', '--list', action='store_true', help='list all targets')
    parser.add_argument('-n', '--nodeps', action='store_true', help='run only the specified tasks, ignore all dependency information')
    parser.add_argument('-v', '--verbose', action='store_true', help='print more stuff')
    parser.add_argument('--ansi', action='store_true', help='always (ansi) colourize output')
    parser.add_argument('--nodocs', dest='skip_docs', action='store_true', default=False, help='skip building API docs')
    parser.add_argument('-k', '--keep-going', action='store_true', help='keep going after a task fails')
    parser.add_argument('--retry', action='store_true', help='retry failed tasks (after prompting)')
    parser.add_argument('--badumna-log', choices=['Info','Verbose','Warning','Error'], help='set $BADUMNA_LOG_OVERRIDE appropriate for the given verbosity level (only has an effect in trace builds)')
    parser.add_argument('--badumna-log-tags', help='Comma-separated list of LogTag values to include')
    parser.add_argument('--badumna-log-label', help='Label to use for badumna log file name')
    parser.add_argument('tasks', nargs='*', help='tasks to run (defaults to "build")')

    argv = sys.argv[1:]
    task_args = []

    SEPARATOR = '--'
    if SEPARATOR in argv:
        idx = argv.index(SEPARATOR)
        argv, task_args = argv[:idx], argv[idx+1:]

    opts = parser.parse_args(argv)

    if opts.verbose:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    return opts, task_args


def select_tasks(defined_tasks, selectors, default=False):
    if not selectors:
        if default is True:
            selectors = [""]
        elif default:
            return default
        else:
            raise AssertionError('No task selectors given')

    tasks = []
    for task_selector in selectors:
        # each selector gets its own scope,
        # to prevent dependencies between named targets from
        # being interspersed
        scope = SelectionScope()
        task_selector = TaskSelector(task_selector)
        matches = set(task_selector.matches(defined_tasks.iterkeys()))
        if len(matches) == 0:
            raise AssertionError('No matching tasks for selector {}'.format(task_selector))
        for m in matches:
            tasks.append(scope.add(m, defined_tasks))
    return tasks

def main(args, defined_tasks):
    opts, task_args = args

    if opts.list:
        targets = select_tasks(defined_tasks, opts.tasks, default=True)
        list_targets(targets)
        return

    targets = select_tasks(defined_tasks, opts.tasks)

    for target in targets:
        if task_args:
            target.set_args(copy.copy(task_args))
        target.expand_dependencies(defined_tasks)

    if opts.dump is not None:
        all_targets = itertools.chain(*[target.traverse() for target in targets])
        dump_dependencies(opts.dump, all_targets)
        return

    go(targets, opts)

