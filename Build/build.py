
import os
import sys
import re
import logging
import shutil
from glob import iglob
import subprocess
from itertools import chain, ifilterfalse, product
from functools import partial
import collections
from zipfile import ZipFile
import tarfile
from tempfile import mkdtemp, TemporaryFile
import colorlog
import argparse
from xml.etree import ElementTree
from preprocessor import PreProcessor
from fnmatch import fnmatch

import assemble
import runner
import jenkins
from assemble import (
    cleanup,
    copy_file,
    copy_tree,
    glob_dir,
    erase,
    ext,
    get_current_task,
    make_clean_dir,
    make_concrete_task_name,
    mkdir,
    walk_files,
    P,
    RP,
    Task,
    TaskName,
)

logger = logging.getLogger()
logger.addHandler(colorlog.ColorizingStreamHandler())
SKIP_DOCS=True

# NOTE: this python env only works properly on windows hosts, which is why
# we can't run the whole build process through it:
BUNDLED_PYTHON =  P("Third Party/Python27-env/Scripts/python")

# our custom checks only run on certain types of files
CODE_EXTENSIONS = frozenset(['cpp','hpp','h','c','cs','rst','csproj','as3proj', 'py', 'rst'])
# and we want to exclude a few files that are outside of our control
CODE_EXCLUSIONS = frozenset(map(re.compile, [
    r'Mono-2\.\d+\.\d+.*include',
    r'Resource\.[Dd]esigner\.cs$',
    r'include\\log4cplus',
    r'Utils\\(Linked|Scoped)Ptr.h$',
    r'python-env\\',
    r'cpplint\.py',
]))
COPYRIGHT_COMPANY="Scalify"
COPYRIGHT_TEXT = "Copyright (c) 2012 " + COPYRIGHT_COMPANY + ". All rights reserved."
INTERNAL_MARKER = '.scalify-internal.'

# A whitelist of filenames it's OK to release without obfuscation
UNOBFUSCATED_FILES = frozenset([
    'Dei.AdminClient.dll',
    'DeiAccountManager.dll',
    'Badumna.Package.dll',
    'Badumna.Package.Builder.dll',
    'BadumnaNetworkController.dll',
    'BadumnaDemo.dll',
    'BadumnaDemo (Windows).exe',
])

UNBUILDABLE_SOLUTIONS = frozenset([
    'BadumnaDemo.Android.sln',
    'BadumnaDemo.MonoTouch.sln',
])

def get_msb_properties(task_name, additional_constants=[]):
    args = dict(
        TreatWarningsAsErrors=not any(x in task_name.name for x in ['jsc', 'ScriptCoreLib', 'ScriptCoreLibA', 'ScriptCoreLib.Ultra.Library']),
        PythonBuildConfiguration='-'.join(sorted(task_name.config))
    )

    if 'unity-ios' in task_name.config:
        args['AssemblyNameSuffix'] = '.Unity.iOS'

    if 'android' in task_name.config:
        args['AssemblyNameSuffix'] = '.Android'

    constants = additional_constants[:]
    if 'release' in task_name.config:
        args['Configuration'] = 'Release'
        args['BuildType'] = 'Production'
    else:
        args['Configuration'] = 'Debug'
        constants.append('DEBUG')

    defines = [
        ('trial', 'DEV_TRIAL'),
        ('trace', 'TRACE'),
        ('android', 'ANDROID'),
        ('unity-ios', 'IOS'),
    ]

    for conf, constant in defines:
        if conf in task_name.config:
            constants.append(constant)

    if 'unity-ios' in task_name.config:
        assert 'TRACE' not in constants, "Unity iOS build does not work when TRACE is defined"

    if 'ScriptCoreLib.Ultra.VisualBasic' in task_name.name:
        args['DefineConstants'] = '=1,'.join(constants) + '=1'
    else:
        args['DefineConstants'] = ' '.join(constants)

    return args

def msbuild_build(task):
    additional_constants = getattr(task, 'constants', [])
    build_properties = get_msb_properties(task.name, additional_constants=additional_constants)
    build_properties['BuildProjectReferences'] = task.path.endswith('.sln')
    assemble.msbuild(task.path, getattr(task, 'target', 'Build'), build_properties, getattr(task, 'msbuild_args', []))

def msbuild_test(arch, coverage, gui=False):
    def test(task, args=[]):
        assembly = get_task_assembly_path(task)
        command = [assembly]
        if gui:
            fixture_prop = 'fixture'
            executable = 'nunit'
        else:
            fixture_prop = 'run'
            executable = 'nunit-console'
            command.append('/labels')
            result_dir = P('TestResults')
            command.append('/xml={}'.format(P(result_dir, '-'.join(sorted(task.name.name - set(['test'])) + sorted(task.name.config)) + '.xml')))
            mkdir(result_dir)

        if arch:
            executable += "-" + arch
        command.insert(0, executable)

        is_flag  = lambda x: x.startswith("/")
        flags    = list(filter(      is_flag, args))
        fixtures = list(ifilterfalse(is_flag, args))

        if(fixtures):
            assert len(fixtures) == 1, "Only 1 fixture allowed"
            command.append("/{}:{}".format(fixture_prop, fixtures[0]))

        result_dir = P('TestResults')
        mkdir(result_dir)

        if coverage:
            # NCover
            command.insert(0, 'NCover.Console.exe')
            coverage_result_dir = P('CoverageTestResults')
            coverage_result_xml = '{}'.format(P(coverage_result_dir, '-'.join(sorted(task.name.name - set(['test'])) + sorted(task.name.config)) + '-coverage.xml'))
            command.extend(['//x', coverage_result_xml])
            mkdir(coverage_result_dir)

        command.extend(flags)
        assemble.check_call(command)

        if coverage:
            # NCover Explorer
            command = ['NCoverExplorer.Console.exe', coverage_result_xml, '/r:ModuleClassFunctionSummary']
            command.append('/html:{0}'.format(P(coverage_result_dir, '-'.join(sorted(task.name.name - set(['test'])) + sorted(task.name.config)) + '-coverage.html')))
            assemble.check_call(command)

    return test

def msbuild_clean(task):
    assemble.msbuild(task.path, 'Clean', get_msb_properties(task.name))

def monodev_task(target, task):
    if 'release' in task.name.config:
        config = 'Release'
    else:
        config = 'Debug'

    if hasattr(task, 'platform'):
        # mdtool doesn't seem to use the solution configurations properly.
        # Sometimes need to specify the platform to get the correct config for the project
        # (just specifying Release might result in Debug being build, but Release|x86 will build Release|x86)
        config += '|' + task.platform

    mdtool = P('/Applications/MonoDevelop.app/Contents/MacOS/mdtool')

    assemble.check_call([mdtool, 'build', '-t:' + target, '-c:' + config, task.path])

def monodev_build(task):
    monodev_task('Build', task)

def monodev_clean(task):
    monodev_task('Clean', task)

def get_task_assembly_path(task):
    return get_assembly_path(task.name, task.path)

def get_assembly_path(name, path):
    return assemble.msbuild(P('Build/GetTargetPath.msbuild'), properties=dict(get_msb_properties(name), Project=path), args=['/v:m', '/nologo'], keep_output=True).strip()

def copy_msbuildproj_output(task, dest):
    if dest[-1] != os.path.sep:
        dest += os.path.sep
    assemble.msbuild(task.path, 'ResolveReferences;CopyFilesToOutputDirectory', dict(get_msb_properties(task.name), OutDir=dest, BuildingProject='true'))

def get_obfuscation_output_dir(task_name):
    return P('Build/obfuscate/output', 'out-' + '-'.join(sorted(task_name.config)))

def jenkins_obfuscate(inputs, output_dir):
    make_clean_dir(output_dir)
    for input_file in inputs:
        copy_file(input_file, output_dir)

    #return jenkins.obfuscate(inputs, output_dir)

def obfuscate(task):
    input_dir = P('Build/obfuscate/input')  # TODO: For parallelization, this task needs to have separate input directories for different configs
    output_dir = get_obfuscation_output_dir(task.name)

    make_clean_dir(input_dir)  # Important, because Dotfuscator will obfuscate everything in this directory that's listed in the config file

    # Copy the input assemblies to where the submit script expects to find them
    for dep in task.deps:
        copy_msbuildproj_output(get_current_task(dep), input_dir)

    # HACK: For MonoDroid builds, we need the MonoDroid mscorlib.dll.  It's not explicitly referenced so it doesn't get copied automatically by copy_msbuildproj_output.
    if 'android' in task.name.config:
        copy_file(P('Third Party/MonoDroid/mscorlib/mscorlib.dll'), input_dir)

    # Run the obfuscation
    jenkins_obfuscate(glob_dir(input_dir), output_dir)


def read_code_files(root, code_extensions, exclusions, linewise=True):
    '''uses code_extensions and exclusions (a list of regexes) to
    yield the contents of only relevant (code) files'''
    for path, filename in walk_files(root):
        if ext(filename).lower() in code_extensions:

            if any([exclusion.search(path) for exclusion in exclusions]):
                logger.debug("skipping file (exclusion): %s", path)
                continue

            logger.debug("processing file: %s", path)
            with open(path) as file:
                if linewise:
                    for lineno, line in enumerate(file,1):
                        yield path, lineno, line.rstrip()
                else:
                    yield path, file
        else:
            logger.debug("skipping file: %s", path)

def check_whitespace(task):
    ok = True
    for path, lineno, line in read_code_files(task.path, CODE_EXTENSIONS, CODE_EXCLUSIONS):
        if '\t' in line:
            logger.error("TAB character detected in %s:%s", path, lineno)
            ok = False
    assert ok, "whitespace errors found"

def check_todos(task):
    ok = True
    for path, lineno, line in read_code_files(task.path, CODE_EXTENSIONS, CODE_EXCLUSIONS):
        if any([mark in line for mark in ('TODO',)]):
            logger.error("TODO detected in %s:%s\n    %s", path, lineno, line)
            ok = False
    assert ok, "TODOs found"

def check_copyright(task):
    ok = True
    for path, file in read_code_files(task.path, CODE_EXTENSIONS, CODE_EXCLUSIONS, linewise=False):
        has_copyright = False
        copyright_company = None
        copyright_filename = None
        copyright_text = []
        for lineno, line in enumerate(file):
            if "</copyright>" in line or lineno > 10: break
            if "<copyright" in line: has_copyright = True
            if has_copyright:
                file_match = re.search('file="(?P<file>[^"]*)', line)
                if file_match:
                    copyright_filename = file_match.groupdict()['file']

                company_match = re.search('company="(?P<company>[^"]*)', line)
                if company_match:
                    copyright_company = company_match.groupdict()['company']

                if not (file_match or company_match):
                    copyright_text.append(line.lstrip("/").strip())

        if has_copyright:
            if copyright_filename != os.path.basename(path):
                ok = False
                logger.error("Copyright in %s has incorrect filename: %s" , path, copyright_filename)


            if copyright_company is None:
                ok = False
                logger.error("Copyright in %s has no company name" , path)

            if copyright_text == []:
                ok = False
                logger.error("Copyright in %s has no text" , path)
            #TODO: if we want consistency, enable these:
            # if copyright_company != COPYRIGHT_COMPANY:
            #     ok = False
            #     logger.error("Copyright in %s has incorrect company: %s" , path, copyright_company)

            # if COPYRIGHT_TEXT not in copyright_text:
            #     ok = False
            #     logger.error("Copyright in %s is missing text (%s). It has:\n%s" , path, COPYRIGHT_TEXT, "\n".join(copyright_text))
        else:
            logger.info("No copyright header in %s", path)
            #TODO: should this be an error?
            # ok = False

    assert ok, "Copyright errors found"

def build_api_docs(task):
    if SKIP_DOCS:
        logger.error("Skipping documentation!")
        return
    proj_file = 'Badumna_Copy.shfbproj'
    cover_page_file = 'Badumna API Documentation - v%s.aml' % VERSION[:VERSION.rfind('.')]

    sandcastle_dir = P('Third Party/Sandcastle')
    sandcastle_help_file_builder_dir = P('Third Party/Sandcastle Help File Builder')
    api_documentation_dir = P('Documentation/API')
    api_documentation_proj = P(api_documentation_dir, proj_file)

    # copy and replace text in file
    copy_file(P(api_documentation_dir, 'Badumna.shfbproj'), api_documentation_proj)
    copy_file(P(api_documentation_dir, 'Badumna API Documentation.aml'), P(api_documentation_dir, cover_page_file))

    package_replace_text(RP('Documentation/API'), proj_file, 'VERSION', VERSION[:VERSION.rfind('.')])(task, P('.'))
    package_replace_text(RP('Documentation/API'), proj_file, 'Badumna API Documentation.aml', cover_page_file)(task, P('.'))
    package_replace_text(RP('Documentation/API'), cover_page_file, 'VERSION', VERSION[:VERSION.rfind('.')])(task, P('.'))

    # These directories are deleted prior to building help.  Ensure that they're not changed
    # to point to an important location!
    api_documentation_input_dir = P(api_documentation_dir, 'Input')
    api_documentation_output_dir = P(api_documentation_dir, 'Output')
    api_documentation_xml_output_dir = P(api_documentation_dir, 'XmlOutput')

    env = dict(os.environ)
    env['DXROOT'] = sandcastle_dir

    # Build reflection data for Sandcastle if required
    if not os.path.exists(P(sandcastle_dir, 'Data')):
        # DXROOT property needs to be passed in addition to environment variable.
        subprocess.check_call(['msbuild', P(sandcastle_dir, 'fxReflection.proj'), '/p:DXROOT=' + env['DXROOT']], env=env)

    # Clean up
    make_clean_dir(api_documentation_input_dir)
    make_clean_dir(api_documentation_output_dir)
    make_clean_dir(api_documentation_xml_output_dir)
    erase(P(api_documentation_dir, 'Web Documentation.zip'))

    # Copy in the input files
    for dep in task.deps:
        assembly = get_task_assembly_path(get_current_task(dep))
        copy_file(assembly, api_documentation_input_dir)
        copy_file(os.path.splitext(assembly)[0] + '.xml', api_documentation_input_dir)

    os.environ['SHFBROOT'] = sandcastle_help_file_builder_dir
    subprocess.check_call(['msbuild', api_documentation_proj, '/p:SandcastlePath={}'.format(sandcastle_dir)])

    erase(api_documentation_proj)
    erase(P(api_documentation_dir, cover_page_file))

def package_assembly(dest):
    def f(task, build_dir):
        dest_dir = P(build_dir, dest)
        copy_msbuildproj_output(task, dest_dir)
        cleanup(dest_dir, ['*.xml', '*.pdb'])  # TODO: This shouldn't be done! We at least need to commit the .pdb before blowing it away.
    return f

def build_web_application(task, dest=None):
    cmd = [
        'aspnet_compiler',
        '-v', '/',
        '-p', os.path.dirname(task.path),
    ]
    if dest is not None:
        cmd.append(dest)
    subprocess.check_call(cmd)

def package_web_application(dest):
    def f(task, build_dir):
        build_web_application(task, P(build_dir, dest))

        # For some reason the ASP.NET compiler copies in all the files from the project folder, not just the files specified in the csproj file...
        cleanup(P(build_dir, dest), ['*.csproj', '*.csproj.user', '*.FileListAbsolute.txt', '*.StyleCop', '.gitignore', '*.pdb'], recurse=True)
        shutil.rmtree(P(build_dir, dest, 'obj'))
    return f

def package_copyoutput(dest):
    def f(task, build_dir):
        copy_tree(task.output_dir, P(build_dir, dest))
    return f

def expect_content_in_packaged_file(dest, contents):
    def f(task, build_dir):
        path = RP(build_dir, dest)
        with open(path) as f:
            for line in f:
                if contents in line:
                    break
            else:
                raise AssertionError("Couldn't find expected text %r in packaged file %s" % (contents, path))
    return f

def package_copytree(source, dest):
    def f(task, build_dir):
        copy_tree(source, P(build_dir, dest))
        post_export_package_cleanup(task, build_dir, dest)
    return f

def post_export_package_cleanup(task, build_dir, dest):
    cleanup(P(build_dir, dest), '.gitignore', recurse=True)
    replace_internal_properties(task, build_dir)

def package_copy(source, dest):
    def f(task, build_dir):
        copy_file(source, P(build_dir, dest))
    return f

def git_snapshot_hash():
    if git_snapshot_hash._ is None:
        commit = subprocess.check_output(['git','stash', 'create']).strip() or 'HEAD'
        print "Temporary commit: {}".format(commit)
        git_snapshot_hash._ = commit
    return git_snapshot_hash._
git_snapshot_hash._ = None

def vcs_export(source, dest):
    assert os.path.exists(source) and not os.path.isabs(source), 'vcs_export, src=%s dest=%s' % (source, dest)
    mkdir(dest)
    commit = git_snapshot_hash()
    archive_cmd = ['git','archive', '--format=tar', commit, source]
    # tarfile contents are all under `source`/, but we want them at the top-level
    # so we'll strip the number of components in `source`:
    leading_components = len(source.split(os.path.sep))
    archive_proc = subprocess.Popen(archive_cmd, stdout = subprocess.PIPE)
    tar = tarfile.open(fileobj = archive_proc.stdout, mode='r|')
    for entry in tar:
        entry.name = os.path.sep.join(entry.name.split('/')[leading_components:])
        tar.extract(entry, dest)

    archive_proc.communicate()
    tar.close()
    
    archive_status = archive_proc.wait()
    assert archive_status == 0, "git-archive failed"

def vcs_clean(task):
    if sys.stdin.isatty() and os.environ.get('BUILD_NUMBER', None) is None:
        while True:
            print ("\nWARNING: this will DELETE all uncommitted changes (including ignored\n"
                    + "files) and revert your entire workspace. Are you SURE? "),
            response = raw_input().strip()
            if response not in ('yes', 'no'):
                print '\nPlease type "yes" or "no"' % (response,)
                continue
            assert response == 'yes', "Cancelled."
            break

    assemble.check_call(['git','reset','--hard'])
    assemble.check_call(['git','clean','-fdx','--exclude=/Third Party/Sandcastle/Data/'])


def package_export(source, dest):
    def f(task, build_dir):
        mkdir(os.path.dirname(P(build_dir, dest)))  # Make sure the parent of the destination exists
        vcs_export(source, P(build_dir, dest))
        post_export_package_cleanup(task, build_dir, dest)
    return f

def export_git_head(dest):
    def f(task, build_dir):
        with open(RP(build_dir, dest), 'w') as f:
            subprocess.check_call(["git", "rev-parse", "HEAD"], stdout = f)
    return f

def replace_internal_properties(task, build_dir):
    '''
    Finds any *.scalify-internal.* files in the build path. When found, it:
     - removes the file
     - searches all files in the same directory as the internal file
       and replaces any instances of the filename with the packaged version
       of the filename (which is just the filename with ".scalify-internal" removed).
    '''
    to_remove = []
    for path, internal_filename in walk_files(build_dir):
        if INTERNAL_MARKER in internal_filename:
            to_remove.append(path)
            packaged_filename = internal_filename.replace(INTERNAL_MARKER, '.')
            parent_dir = os.path.dirname(path)
            logger.info("replacing references to %s with %s in %s", internal_filename, packaged_filename, parent_dir)
            assert os.path.exists(os.path.join(parent_dir, packaged_filename)), "Replacement file does not exist!"

            replacement_found = False
            for other_file in os.listdir(parent_dir):
                other_path = os.path.join(parent_dir, other_file)
                if os.path.isdir(other_path): continue
                with open(other_path, 'rb') as f:
                    contents = f.read()

                if internal_filename in contents:
                    replacement_found = True
                    contents = contents.replace(internal_filename, packaged_filename)
                    with open(other_path, 'wb') as f:
                        f.write(contents)

            assert replacement_found, "Could not find any instances of {} to replace in {}".format(internal_filename, parent_dir)

    for junk in to_remove:
        erase(junk)


def package_replace_text(path, filename_pattern, original_text, replacement_text, expected_count = None, regexp=False):
    if regexp:
        def subn(contents):
            re_pattern = re.compile(original_text, re.MULTILINE)
            return re.subn(re_pattern, replacement_text, contents)
    else:
        def subn(contents):
            num_replacements = len(re.findall(re.escape(original_text), contents))
            contents = contents.replace(original_text, replacement_text)
            return (contents, num_replacements)

    def f(task, build_dir):
        for p, _, __ in os.walk(P(build_dir, path)):
            pattern = P(p, filename_pattern)
            assert os.path.commonprefix([pattern, P(build_dir)]) == P(build_dir)
            for filename in iglob(pattern):
                with open(filename, 'rb') as in_file:
                    contents = in_file.read()
                (contents, num_replacements) = subn(contents)
                log_args = ("replacing %d instances of [%s] with [%s] in %s", num_replacements, original_text, replacement_text, filename)
                if expected_count is not None and expected_count != num_replacements:
                    logger.error(*log_args)
                    assert False, "{} replacements expected!".format(expected_count)

                with open(filename, 'wb') as out_file:
                    out_file.write(contents)
    return f

def package_delete_lines(path, filename_pattern, search_pattern):
    def f(task, build_dir):
        for p, _, __ in os.walk(P(build_dir, path)):
            pattern = P(p, filename_pattern)
            assert os.path.commonprefix([pattern, P(build_dir)]) == P(build_dir)
            re_pattern = re.compile(original_text)
            for filename in iglob(pattern):
                with open(filename, 'rb') as in_file:
                    contents = in_file.read()
                with open(filename, 'wb') as out_file:                  
                    for line in contents:
                        if re_pattern.match(line) is None:
                             out_file.writeline(line)

    return f

def package_apidoc(task, build_dir):
    copy_tree(P('Documentation/API/Output'), P(build_dir, 'Documentation/API'))
    mkdir(P(build_dir, 'Assemblies/Desktop'))
    for filename in glob_dir('Documentation/API/XmlOutput', '*.xml'):
        copy_file(filename, P(build_dir, 'Assemblies/Desktop'))

def package_platform_xmldocs(xml_file, *dest_paths):
    """Create XML documentation for the platform-specific assemblies.
    
    xml_file -- The documentation file to use as the template for the other assemblies.
    dest_paths -- The paths where the documentation file should be copied.  These should
                  be the path with the assembly name, but without the '.dll' extension.

    The documentation is created by copying and modifying the documentation generated for the desktop asssemblies.
    This is a bit of a hack, but is the easiest way to do it and the public API for all the .NET assemblies
    should be the same across platforms.
    Doing this properly requires generating xml docs for each platform and running them through Sandcastle,
    which for MonoTouch also involves sending the interim xml docs from OS X to Windows.
    """
    def f(task, build_dir):
        docs = ElementTree.parse(P(build_dir, xml_file))
        for dest in dest_paths:
            docs.getroot().find('assembly/name').text = os.path.basename(dest)
            mkdir(P(build_dir, os.path.dirname(dest)))
            docs.write(P(build_dir, dest + '.xml'), xml_declaration=True, encoding='utf-8')
    return f

def package_control_center_service(name, description, dest, include_dependencies=[]):
    """Collects a control center service into a temporary directory in the build path so that obfuscated DLLs can be replaced."""
    def f(task, build_dir):
        temp_dir = P(build_dir, '-'.join(['temp-cc', name] + sorted(task.name.config)))
        make_clean_dir(temp_dir)
        package_dir = P(temp_dir, name)
        copy_msbuildproj_output(task, package_dir)
        for dep in include_dependencies:
            dep_task = get_current_task(make_concrete_task_name(dep, task.name.config))
            copy_msbuildproj_output(dep_task, package_dir)
        cleanup(package_dir, ['*.xml', '*.pdb'])
        assemble.tree(package_dir)
    return f

def finish_packaging_control_center_service(name, description, dest):
    """Finalizes the packaging of a control center service by running the packager and removing the temporary directory."""
    def f(task, build_dir):
        packager_task = get_current_task(make_concrete_task_name('build:SimplexPrestoManifesto', task.name.config))
        packager = get_task_assembly_path(packager_task)

        temp_dir = P(build_dir, '-'.join(['temp-cc', name] + sorted(task.name.config)))
        package_dir = P(temp_dir, name)
        shutil.make_archive(P(temp_dir, name), 'zip', package_dir)

        final_description = description + ' v' + VERSION
        if 'trial' in task.name.config:
            final_description += ' (Trial)'

        executable = os.path.basename(get_task_assembly_path(task))

        assemble.check_call([
            packager,
            'applicationName:' + name,
            'projectFile:' + P(temp_dir, name + '.zip'),
            'updateType:complete',
            'includeExtractionTools',
            'executableFile:' + executable,
            'description:' + final_description,
            'output:' + P(build_dir, dest, name)
        ])

        shutil.rmtree(temp_dir)
    return f


def package_unitypackages(unity_files, dest_dir, replacement_map):
    def f(task, build_dir):
        replace_map = dict()
        for key in replacement_map:
            replace_map[key.lower()] = replacement_map[key]
        logger.info("package_unitypackages: replace_map keys are: %r", sorted(replacement_map.keys()))

        mkdir(P(build_dir, dest_dir))

        for unity_file in unity_files:
            with tarfile.open(unity_file, 'r') as in_file:
                temp_dir = mkdtemp()
                in_file.extractall(temp_dir)

                out_filename = os.path.join(temp_dir, 'archtemp.tar')

                with tarfile.open(out_filename, 'w:gz') as out_file:
                    for path, files, directories  in os.walk(temp_dir):
                        if path == temp_dir:
                            continue

                        with open(os.path.join(path, 'pathname'), 'r') as path_file:
                            filename = os.path.basename(path_file.readline().strip()).lower()
                            if filename in replace_map:
                                replacement = P(build_dir, replace_map[filename])
                                asset_path = os.path.join(path, 'asset')
                                logging.debug("unitypackage: replacing %s with %s", P(asset_path, filename), replacement)
                                copy_file(replacement, asset_path)
                            else:
                                logging.debug("unitypackage: not replacing %s", filename)

                        out_file.add(path, os.path.basename(path))

                copy_file(out_filename, P(build_dir, dest_dir, os.path.basename(unity_file)))

                shutil.rmtree(temp_dir)
    return f

def package_cpp_examples(dest_dir, source_dir='pro', demos=['*Demo*']):
    def f(task, build_dir):
        final_dest_dir = P(build_dir, dest_dir)
        mkdir(final_dest_dir)

        patterns = [
            P(build_dir, 'Assemblies/Desktop/Badumna.dll'),
            P(build_dir, 'Assemblies/Desktop/Dei.dll'),
            P(build_dir, 'Assemblies/Desktop/DeiAccountManager.dll'),
            P('Source/CppWrapper/BadumnaCpp/libbadumnadl.dll'),
            P('/QtSDK/Desktop/Qt/4.7.3/mingw/lib/QtCore4.dll'),
            P('/QtSDK/Desktop/Qt/4.7.3/mingw/lib/QtGui4.dll'),
            P('/QtSDK/mingw/bin/mingwm10.dll'),
            P('/MinGW/bin/libgcc_s_dw2-1.dll'),
            P('/MinGW/bin/libstdc++-6.dll')
        ]

        patterns.extend(P('Source/CppWrapper/ApiExamples/{}/{}.exe'.format(source_dir, x)) for x in demos)

        if any(fnmatch('Scene-Demo5-PrivateChat', x) for x in demos):
            patterns.extend([
                P('Source/CppWrapper/ApiExamples/Scene-Demo5-PrivateChat/john.list'),
                P('Source/CppWrapper/ApiExamples/Scene-Demo5-PrivateChat/mary.list')
            ])

        for pattern in patterns:
            for filename in iglob(pattern):
                copy_file(filename, final_dest_dir)

        copy_tree(P('Source/CppWrapper/BadumnaCpp/libs/Mono-2.8.1'), P(final_dest_dir, 'mono'))
        shutil.move(P(final_dest_dir, 'mono/mono-2.0.dll'), P(final_dest_dir))
    return f

def package_cleanup(pattern, filter):
    def f(task, build_dir):
        for elem in iglob(P(build_dir, pattern)):
            if filter(elem):
                erase(elem)
    return f

def package_replace_all_obfuscated(task, build_dir):
    # Copy obfuscated assemblies over the normal assemblies.  This should happen at the very end.
    output_dir = get_obfuscation_output_dir(task.name)
    obfuscated = set(os.path.basename(f) for f in glob_dir(output_dir))
    for path, _, files in os.walk(build_dir):
        to_copy = obfuscated.intersection(files)
        for f in to_copy:
            copy_file(P(output_dir, f), path)

def package_obfuscated(dest):
    def f(task, build_dir):
        output_dir = get_obfuscation_output_dir(task.name)
        build_dest = P(build_dir, dest)
        mkdir(build_dest)
        for asm in glob_dir(output_dir, '*.dll'):
            copy_file(asm, build_dest)
    return f

def check_built_package(task, args):
    '''
    Runs ALL checks on a built package (zipfile or pre-extracted directory)
    
    Note that in checking, it will modify a folder's contents by expanding
    all zip files to their parent directories. Zip files are left as-is,
    since they get copied into a temp dir first.
    '''
    parser = argparse.ArgumentParser()
    parser.add_argument('--keep', action='store_true', help="don't delete temporary (unpacked) dir - only used if path is a .zip")
    parser.add_argument('paths', nargs='+')
    opts = parser.parse_args(args)
    keep = opts.keep

    for path in opts.paths:
        if assemble.ext(path) == 'zip':
            orig_path = path
            path = mkdtemp()
            logger.info("Created temp dir %s", path)
            shutil.copy(orig_path, path)
        else:
            keep = True

        assemble.recursively_unzip(P(path))
        try:
            check_package_contents(path, full=True)
        finally:
            if not keep:
                shutil.rmtree(path)

def check_package_contents(p, full=True):
    logger.info("running check_package_contents(%r, full=%r)", p, full)
    errors = []
    def _try(func, *a, **k):
        try:
            func(*a, **k)
        except AssertionError as e:
            errors.append(e)
    _try(check_unwanted_files, p)
    _try(check_badumna_dll_version, p, enforce=full)
    _try(check_third_party_references, p)
    _try(check_copyright_information, p)
    _try(check_package_license, p)
    if full:
        _try(check_obfuscated_assemblies, p)
        _try(check_solutions_are_buildable, p)

    if errors:
        if len(errors) == 1:
            raise errors[0]
        else:
            raise AssertionError("Multiple errors: " + ", ".join(map(str, errors)))

def check_unwanted_files(p):
    logger.info("Checking for unwanted files in %s", p)
    ok = True
    unwanted_file_types = ('crt', 'snk', 'pdb')
    def is_unwanted(filename):
        ext = assemble.ext(filename).lower()
        return ext in unwanted_file_types or INTERNAL_MARKER.lower() in filename.lower()

    for path, filename in walk_files(p):
        ext = assemble.ext(filename).lower()
        #TODO: add exclusions for any .pdb files we actually want to ship
        if is_unwanted(filename):
            logger.error("unwanted file found in package: %s", path)
            ok = False
        if ext == 'zip':
            zip_files = ZipFile(path).namelist()
            for zip_file in zip_files:
                ext = assemble.ext(zip_file).lower()
                if is_unwanted(zip_file):
                    logger.error("unwanted file found in package: %s#%s", path, zip_file)
                    ok = False

            absolute_paths = list(filter(os.path.isabs, zip_files))
            if absolute_paths:
                logger.warn("%d absolute paths found in packaged zip (%s):", len(absolute_paths), path)
                for path in absolute_paths:
                    logger.warn(" - %s", path)
                #TODO: make this an error?
    assert ok, "unwanted files check failed"

def check_badumna_dll_version(p, enforce=True):
    # we only use `enforce=True` in a final build, because temporary OSX packages
    # fail the check due to their inconsistent path names
    logger.info("Checking trial / release versions files in %s", p)
    ok = True
    for path, filename in walk_files(p):
        if re.match("Badumna\.(.*\.)dll$", filename) and not ".Package." in filename:
            proc = subprocess.Popen(["strings", path], stdout=subprocess.PIPE)
            is_trial = False
            for line in proc.stdout:
                line = line.strip()
                if re.match('^Badumna( .*)Trial$', line):
                    is_trial = True
            assert proc.wait() == 0, "`strings` failed"
            type_desc = "TRIAL" if is_trial else "RELEASE"
            logger.info("%10s DLL: %s", type_desc, path)
            file_should_be_trial = ('-Trial' in path)
            if enforce and (file_should_be_trial != is_trial):
                ok = False
                logger.error("unexpected {} DLL in {}".format(type_desc, path))
    assert ok, "badumna DLL trial/release check failed"

def check_copyright_information(p):
    logger.info("Checking copyright information in %s", p)
    ok = True

    for path, filename in walk_files(p):
        if assemble.ext(path) in ('dll', 'exe') and any(s in filename for s in ['Badumna', 'Dei', 'GermHarness', 'ControlCenter', 'Tunnel', 'OverloadPeer', 'SeedPeer', 'StatisticsServer']) and not any(s in filename for s in ['Demo', 'DeiAccountManager']):
            output = ''
            if assemble.is_windows():                
                try:
                    output = assemble.check_output(["AssemblyInfo.exe", "company", path])
                except:
                    output = assemble.check_output(["AssemblyInfo_x86.exe", "company", path])
            else:
                # try:
                #     output = assemble.check_output(["mono", "AssemblyInfo.exe", "company", path])
                # except:
                #     output = assemble.check_output(["mono", "AssemblyInfo_x86.exe", "company", path])
                logger.info("Skipping check copyright information for %s" % filename)
                continue

            if output.strip() != 'National ICT Australia':
                ok = False
                logger.error("Company name is not found  in %s" % filename)
            
    assert ok, 'company information check failed'

def check_obfuscated_assemblies(p):
    logger.info("Checking obfuscated assemblies in %s", p)
    ok = True
    for path, filename in walk_files(p):
        if assemble.ext(path) in ('dll', 'exe') and any(s in filename for s in ['Badumna', 'Dei']):
            should_be_obfuscated = filename not in UNOBFUSCATED_FILES
            is_obfuscated = False
            output = assemble.check_output(['monodis', path, '--typedef']).splitlines()
            if len(output) == 0 and assemble.ext(path) == 'exe':
                # not a .NET assembly; ignore it
                continue
            for line in output:
                if 'DotfuscatorAttribute' in line:
                    logger.info("%s is Obfuscated", path)
                    is_obfuscated = True
                    break
            else:
                logger.info("%s is not Obfuscated", path)
            if should_be_obfuscated != is_obfuscated:
                ok = False
                logger.error("Assembly %s should %sbe obfuscated!", path, ("" if should_be_obfuscated else "NOT "))
    assert ok, "Obfuscation check failed"

def check_solutions_are_buildable(p):
    ok = True
    build_actions = []
    for path, filename in walk_files(p):
        if assemble.ext(filename) == 'sln':
            if filename in UNBUILDABLE_SOLUTIONS or 'Xamarin' in path:
                logger.info("Skipping test build of %s", filename)
                continue
            build_actions.append((path, lambda: assemble.check_call(['msbuild', path, '/verbosity:quiet'])))
        elif filename.lower() == 'makefile':
            parent_dir = os.path.dirname(path)
            grandparent_dir = os.path.dirname(parent_dir)
            parent_dir_name = os.path.basename(parent_dir)
            grandparent_dir_name = os.path.basename(grandparent_dir)
            logger.debug("Parent: %s, grandparent: %s", parent_dir_name, grandparent_dir_name)

            MAKE_DEFAULT = ['make']
            MAKE_RELEASE = ['make', 'release=1']

            def action(cmd):
                def _action(parent_dir=parent_dir):
                    # the default arguments above are used to refer to the value of the variables at
                    # the time of function definition, rather than closing over the variables themselves
                    # (which are redefined in later loop iterations)
                    logger.info("Running `%s` in %s", " ".join(cmd), parent_dir)
                    with assemble.working_dir(parent_dir):
                        assemble.check_call(cmd)
                return _action

            if parent_dir_name == 'Source' and 'C++' in grandparent_dir_name:
                # Bit of a hack for C++ project: the `Examples/Source`
                # project can *only* be built once the `Source` project has succeeded,
                # so we make sure this comes first.
                build_actions.insert(0, (path, action(MAKE_DEFAULT)))
                build_actions.insert(0, (path, action(MAKE_RELEASE)))
            else:
                build_actions.append((path, action(MAKE_DEFAULT)))
                build_actions.append((path, action(MAKE_RELEASE)))

    logger.debug("Projects to build: %r", build_actions)
    for path, action in build_actions:
        try:
            logger.info("Checking that solution builds: %s", path)
            action()
        except subprocess.CalledProcessError, e:
            logger.error("Error building solution %s: %s", path, e)
            ok = False
    assert ok, "Unbuildable solutions found"

def check_third_party_references(p):
    logger.info("Checking for third-party references in %s", p)
    ok = True
    for path, lineno, line in read_code_files(p, CODE_EXTENSIONS, CODE_EXCLUSIONS):
        if re.search(r'Third Party[\\/]', line):
            logger.error("\"Third Party/\" reference found in %s:%s", path, lineno)
            ok = False
    assert ok, "\"Third Party/\" references found"

def check_package_license(p):
    logger.info("Checking license files in %s", p)
    license_found = False
    third_party_license_found = False
    for path, filename in walk_files(p):
        if filename == "License.rtf":
            license_found = True
        elif filename == "Third Party Licenses.rtf":
            third_party_license_found = True

        if license_found and third_party_license_found:
            break

    assert license_found, "License.rtf is not found"
    assert third_party_license_found, "Third Party License.rtf is not found"

def package(task):
    package_name = task.get_package_name(task.name.config)

    build_dir = P('Installers', package_name)
    make_clean_dir(build_dir)
    erase(P('Installers', package_name + '.zip'))

    copy_file('License.rtf', build_dir)
    copy_file('ReadMe.html', build_dir)
    copy_file('Third Party Licenses.rtf', build_dir)

    for step in task.steps:
        package_task = None
        if step[0] is not None:
            dep_name = make_concrete_task_name(step[0], task.name.config)
            deps = [x for x in task.deps if x == dep_name]
            assert len(deps) == 1, 'dep specified in step should match exactly one dep in the task produced by gen_task ({} -> {} / {})'.format(step[0], deps, task.deps)
            package_task = get_current_task(deps[0])

        if step[1] is not None:
            step[1](package_task, build_dir)

    check_package_contents(build_dir, full=False)
    assemble.tree(build_dir, desc="Package contents (%s)")

    zip_format = 'gztar' if sys.platform == 'darwin' else 'zip'  # Using tar.gz on OS X so that symlinks stay intact
    shutil.make_archive(P('Installers', package_name), zip_format, 'Installers', package_name)

def seq(*func):
    funcs = [x for f in func for x in (f if isinstance(f, collections.Iterable) else [f])]  # Flatten one layer, and store as a list in case our inputs are generators (so we can call the final func multiple times)
    def g(*args, **kwargs):
        for f in funcs:
            f(*args, **kwargs)
    return g

def badumna_log_env(*a, **k):
    '''Returns a copy of os.environ with a custom badumna log config'''
    env = os.environ.copy()
    for key, val in generate_badumna_log_config(*a, **k):
        env[key] = val
    return env

def generate_badumna_log_config(level=None, tags=None, label=None):
    level = level or BADUMNA_LOG_LEVEL
    if level is None:
        return []
    if tags is None:
        tags = 'All'

    label_xml = ""
    if label is not None:
        label_xml = " Label=\"%s\"" % (label,)

    xml = (
        '''<Module Name="Logger" Enabled="true" EnableDefaultTrace="true" AssertUI="false" IncludeTags="''' + tags + '''" ExcludeTags="None">''' +
            '''<Logger Type="DotNetTraceLogger">''' +
                '''<TraceFile Enabled="true"''' + label_xml + '''/>''' +
                '''<Verbosity>'''+level+'''</Verbosity>''' +
            '''</Logger>''' +
        '''</Module>''')
    return [('BADUMNA_LOG_OVERRIDE', xml)]

def define_tasks():
    def msb(path, deps=[], name=None, nunit=False, coverage=False, **kwargs):
        if name is None:
            name = os.path.splitext(os.path.basename(path))[0]
        build_deps = ['build:{}'.format(x) for x in deps]
        clean_deps = ['clean:{}'.format(x) for x in deps]
        try:
            # extra build deps which aren't prefixed by "build"
            # (and don't necessarily have a corresponding "clean" task)
            build_deps.extend(kwargs.pop('build_deps'))
        except KeyError: pass

        test_deps = build_deps + ['build:{}'.format(name)]

        yield Task('build:{}'.format(name), msbuild_build, build_deps, path=path, **kwargs)
        yield Task('clean:{}'.format(name), msbuild_clean, clean_deps, path=path, **kwargs)
        if nunit:
            arch = None if nunit is True else nunit
            testname = name[:-5] if name.endswith("Tests") else name
            yield Task('test:{}'.format(testname), msbuild_test(arch, coverage, gui=False), test_deps, path=path, **kwargs)
            yield Task('test-gui:{}'.format(testname), msbuild_test(arch, coverage, gui=True), test_deps, path=path, **kwargs)

    msbuild_tasks = chain(
        msb(P('Source/Versioning/Versioning.csproj')),

        msb(P('Source/Badumna/Badumna.csproj'),
            ['Versioning'],
            commit_results=True,
            cpp_auto_test_dir='.'),

        msb(P('Source/Extensions/TrustedComponent/TrustedComponent.csproj'),
            ['Versioning', 'Badumna']),

        msb(P('Source/DebuggingTools/Diagnostics/Diagnostics.csproj'),
            ['Versioning', 'Badumna']),

        msb(P('Source/ManagementTools/Dei/Dei/Dei.csproj'),
            ['Versioning', 'Badumna'],
            commit_results=True,
            cpp_auto_test_dir='.'),

        msb(P('Source/ManagementTools/ControlCenter/GermHarness/GermHarness.csproj'),
            ['Versioning', 'Badumna', 'Dei'],
            commit_results=True),

        msb(P('Source/ManagementTools/ControlCenter/Germinator/Germ/Germ.csproj'),
            ['Versioning', 'PerformanceMonitor']),

        msb(P('Source/ManagementTools/Packaging/Badumna.Package/Badumna.Package.csproj'),
            ['Versioning']),

        msb(P('Source/ManagementTools/Packaging/ApplicationStarter/ApplicationStarter.csproj'),
            ['Versioning']),

        msb(P('Source/ManagementTools/Dei/Dei.AdminClient/Dei.AdminClient.csproj'),
            ['Versioning', 'Dei']),

        msb(P('Source/ManagementTools/Utilities/X509CertificateCreator/X509CertificateCreator.csproj'),
            ['Versioning']),

        msb(P('Source/ManagementTools/Utilities/PerformanceMonitor/PerformanceMonitor.csproj'),
            ['Versioning']),

        msb(P('Source/Simulation/Agency/Agency.csproj'),
            ['Badumna']),

        msb(P('Source/Simulation/NetworkSimulator/NetworkSimulator.csproj'),
            ['Badumna', 'Diagnostics', 'Agency']),

        msb(P('Source/ManagementTools/ControlCenter/SqliteProvider/SqliteProvider/SqliteProvider.csproj'),
            ['Versioning']),

        msb(P('Source/ManagementTools/AspNetHost/AspNetHost/AspNetHost.csproj'),
            ['Versioning']),

        msb(P('Source/ManagementTools/Packaging/Badumna.Package.Builder/Badumna.Package.Builder.csproj'),
            ['Versioning', 'Badumna.Package']),

        msb(P('Source/ManagementTools/Packaging/ProcessMonitor/ProcessMonitor.csproj'),
            ['Badumna.Package']),

        msb(P('Source/Tunnel/Tunnel.csproj'),
            ['Versioning', 'Badumna', 'GermHarness']),

        msb(P('Source/Applications/SeedPeer/SeedPeer/SeedPeer.csproj'),
            ['Versioning', 'Badumna', 'Dei', 'GermHarness']),

        msb(P('Source/Applications/MatchmakingServer/MatchmakingServer/MatchmakingServer.csproj'),
            ['PerformanceMonitor'],
            build_deps=['applibs:dotnet']),

        msb(P('Source/ManagementTools/Dei/DeiServer/DeiServer.csproj'),
            ['Versioning', 'Badumna', 'Dei', 'DeiAccountManager', 'GermHarness', 'X509CertificateCreator'],
            cpp_auto_test_dir='servers'),

        msb(P('Source/Applications/OverloadPeer/OverloadPeer/OverloadPeer.csproj'),
            ['Versioning', 'Badumna', 'Dei', 'GermHarness']),

        msb(P('Source/Applications/StatisticsServer/Statistics/Statistics.csproj'),
            build_deps=['applibs:dotnet']),

        msb(P('Source/Applications/StatisticsServer/StatisticsServer/StatisticsServer.csproj'),
            ['Statistics'],
            build_deps = ['applibs:dotnet']),

        msb(P('Source/Applications/StatisticsServer/StatisticsTester/StatisticsTester.csproj'),
            ['Versioning', 'Statistics'],
            build_deps = ['applibs:dotnet']),

        msb(P('Source/ManagementTools/ControlCenter/LaunchGerm/LaunchGerm.csproj'),
            ['Versioning', 'Germ']),

        msb(P('Source/ManagementTools/AspNetHost/Launcher/Launcher.csproj'),
            ['Versioning', 'AspNetHost']),

        msb(P('Source/ManagementTools/ControlCenter/Germinator/Germinator/Germinator.csproj'),
            ['Versioning', 'Badumna', 'Badumna.Package', 'Badumna.Package.Builder', 'Germ', 'GermHarness']),

        msb(P('Source/ManagementTools/ControlCenter/WebSignUpDemo/WebSignUpDemoPrebuild.csproj'),
            ['Badumna', 'Dei', 'Dei.AdminClient', 'Launcher'],
            skip_fxcop=True),

        msb(P('Source/ManagementTools/Dei/DeiAccountManager/DeiAccountManager.csproj'),
            ['Badumna', 'Dei'],
            cpp_auto_test_dir='servers'),

        msb(P('Source/TestUtilities/TestUtilities/TestUtilities.csproj')),

        msb(P('Source/Badumna/Tests/BadumnaTests.csproj'),
            ['Badumna', 'TestUtilities'],
            nunit='x86', coverage=True),

        msb(P('Source/Tunnel/Tests/TunnelTests.csproj'),
            ['Badumna', 'Tunnel'],
            nunit='x86', coverage=True),

        msb(P('Source/ManagementTools/Dei/DeiTests/DeiTests.csproj'),
            ['Badumna', 'Dei'],
            nunit='x86', coverage=True),

        msb(P('Source/ManagementTools/Dei/DeiServerTests/DeiServerTests.csproj'),
            ['Badumna', 'Dei', 'DeiAccountManager', 'DeiServer', 'GermHarness', 'X509CertificateCreator'],
            nunit='x86', coverage=True),

        msb(P('Source/ManagementTools/ControlCenter/BadumnaNetworkController/BadumnaNetworkController.csproj'),
            ['Versioning', 'Badumna', 'Badumna.Package', 'Dei', 'Dei.AdminClient', 'ApplicationStarter', 'Germ', 'Germinator', 'Launcher', 'LaunchGerm', 'ProcessMonitor', 'SqliteProvider', 'X509CertificateCreator']),

        msb(P('Source/ManagementTools/Packaging/SimplexPrestoManifesto/SimplexPrestoManifesto/SimplexPrestoManifesto.csproj'),
            ['Germinator', 'ApplicationStarter', 'ProcessMonitor']),

        msb(P('Source/ManagementTools/ControlCenter/WebSignUpDemo/WebSignUpDemo/WebSignUpDemo.csproj'),
            ['WebSignUpDemoPrebuild']),

        msb(P('Source/Applications/Demo/Source/BadumnaDemo.Windows.sln'),
            build_deps=['applibs:dotnet']),

        msb(P('Source/Applications/CloudDemo/Master/Windows/BadumnaCloudDemo.sln'),
            build_deps=['applibs:dotnet'],
            # enable ALL the things - individual projects below deal with individual functions, this
            # target is for quickly running / testing the demo so we want all features enabled.
            constants = ['WINDOWS', 'CHAT', 'MATCHMAKING']),

        msb(P('Source/Applications/CloudDemo/Windows-Generated/Demo1-Replication/Demo1-Replication.sln'),
            build_deps=['build:demosource:dotnet', 'applibs:dotnet'],
            constants=['WINDOWS']),

        msb(P('Source/Applications/CloudDemo/Windows-Generated/Demo2-Chat/Demo2-Chat.sln'),
            build_deps=['build:demosource:dotnet', 'applibs:dotnet'],
            constants=['WINDOWS']),        

        msb(P('Source/Testing/AutoTesting/AutoTesting.sln'),
            build_deps=['applibs:dotnet[trace]']),
    )

    def make_cpp(task):
        args = ['release=1'] if 'release' in task.name.config else []

        default_target = 'lib'
        
        for name, dep_task_name in [('BADUMNA_DLL', 'build:Badumna'), ('DEI_DLL', 'build:Dei')]:
            dep_task = get_current_task(make_concrete_task_name(dep_task_name, task.name.config))
            if dep_task is not None:
                args.append('{}={}'.format(name, get_task_assembly_path(dep_task)))

        command_line = ['make']
        if hasattr(task, 'target'):
            targets = task.target.split()
            targets = map(lambda t: default_target if t == '__DEFAULT__' else t, targets)
        else:
            targets = [default_target]

        scratch_install_dir = None
        if 'install' in task.name.config:
            if not any([t.startswith('INSTALL_DIR=') for t in targets]):
                scratch_install_dir = mkdtemp(prefix="badumna-install-")
                args.append('INSTALL_DIR=' + assemble.to_posix_path(scratch_install_dir))
            if not 'install' in targets:
                targets.append('install')

        command_line.extend(targets)
        command_line.extend(args)

        if hasattr(task, 'user_args'):
            command_line.extend(task.user_args.split())

        try:
            assemble.check_call(command_line, cwd=task.path)
        finally:
            if scratch_install_dir:
                assemble.tree(scratch_install_dir, desc="Installed files (in %s)", level=logging.INFO)
                try:
                    shutil.rmtree(scratch_install_dir)
                except OSError as e:
                    logger.error("Unable to clean up scratch_install_dir %s: %s", scratch_install_dir, e)


    def make_or_vs_build(task):
        if not 'vs' in task.name.config:
            return make_cpp(task)
        vs_path = task.get_vs_path(task)
        assemble.msbuild(vs_path, 'Build', get_msb_properties(task.name))

    cpp_auto_test_bin = P("Source/CppWrapper/BadumnaAutoTest/bin")

    def copy_cpp_artifacts(task):
        for dep in map(get_current_task, task.deps):
            dest = getattr(dep, 'cpp_auto_test_dir', None)
            if not dest:
                logger.info("Not copying auto test artifacts from {} - no cpp_auto_test_dir property".format(dep.name))
                continue
            copy_msbuildproj_output(dep, P(cpp_auto_test_bin, dest))

    def run_integration_tests(task, args=[]):
        cmd = [sys.executable, "start_tests.py"]
        cmd.extend(args)
        assemble.check_call(cmd, cwd=task.path)

    cpp_permutations = [
        'install',
    ]

    if assemble.is_windows():
        cpp_permutations.extend([
            'vs,vs2008',
            'vs',
        ])

    cpp_tasks = [
        Task('clean:BadumnaCpp',
            make_cpp,
            target='clean',
            path=P('Source/CppWrapper')),

        Task('build:BadumnaCppTest',
            make_cpp,
            ['build:BadumnaCpp'],
            path=P('Source/CppWrapper/BadumnaCppTest'),
            target='all'),

        Task('build:BadumnaCpp',
            make_or_vs_build,
            ['build:Badumna', 'build:Dei'],
            path=P('Source/CppWrapper/BadumnaCpp'),
            get_vs_path=lambda t: P('Source/CppWrapper/MSVC/{year}/BadumnaCpp.sln'.format(
                year='2008' if 'vs2008' in t.name.config else '2010')),
            ),

        Task('build:BadumnaCppApiExamples',
            make_cpp,
            ['build:BadumnaCpp'],
            path=P('Source/CppWrapper/ApiExamples'),
            target='all install',
            user_args='INSTALL_DIR=pro'),

        Task('build:BadumnaCppCloudApiExamples',
            make_cpp,
            ['build:BadumnaCpp'],
            path=P('Source/CppWrapper/ApiExamples'),
            target='all install',
            user_args='CLOUD_OPTIONS=-DCLOUD=1 INSTALL_DIR=cloud'),

        Task('test:BadumnaCpp',
            make_cpp, # TODO: run compiled tests when using vs config
            ['build:BadumnaCppTest', 'build:Badumna[~vs,~install]', 'build:Dei'],
            path=P('Source/CppWrapper/BadumnaCppTest'),
            target='test verbose=1'), #NOCOMMIT (verbose=1)

        Task('test:BadumnaCppPermutations',
            assemble.no_op,
            list(chain(*[
                [
                    'clean:BadumnaCpp',
                    # 'test:BadumnaCpp[' + config + ']', #TODO (tests only work when Badumna is built in Mingw) ...
                    'build:BadumnaCppApiExamples[' + config + ']'
                ] for config in cpp_permutations])),
            strict_ordering=True
        ),

        Task('compile:BadumnaCppIntegration',
            make_cpp,
            path=P('Source/CppWrapper'),
            target='badumna_auto_test',
            deps=['build:BadumnaCpp']),

        Task('build:BadumnaCppIntegration',
            seq(copy_cpp_artifacts, make_cpp),
            path=P('Source/CppWrapper'),
            target='install_auto_test_deps',
            deps=['build:Badumna', 'build:Dei', 'build:DeiServer', 'compile:BadumnaCppIntegration']),

        Task('test:BadumnaCppIntegration',
            run_integration_tests,
            path=cpp_auto_test_bin,
            deps=['build:BadumnaCppIntegration']),

        Task('build:BadumnaCppAll',
            assemble.no_op,
            [
                'build:BadumnaCpp',
                'build:BadumnaCppTest',
                'build:BadumnaCppIntegration',
                'build:BadumnaCppApiExamples'
            ]),

        Task('build:cpp-apidocs',
            make_cpp,
            path=P('Source/CppWrapper'),
            target='apidoc',
            output_dir=P('Source/CppWrapper/BadumnaCpp/Build/Docs/html')),
    ]

    badumna_cloud_root = P("Source/Web/BadumnaCloud/")
    def run_django(task, args=[]):
        assemble.check_call([sys.executable, P(badumna_cloud_root, "manage.py")] + task.args + args, cwd=badumna_cloud_root)

    def clean_badumna_cloud(task):
        assemble.cleanup(badumna_cloud_root, ("*.s3db", "*.html"), recurse=False)

    def run_badumna_cloud(task, args=[]):
        runner.run_badumna_cloud(
            args,
            basedir=badumna_cloud_root,
            stats_server_path=dep_executable("build:StatisticsServer", task))

    def run_statistics_server(task, args=[]):
        with runner.Supervisor() as supervisor:
            runner.spawn_statistics_server(
                supervisor,
                server_path=dep_executable("build:StatisticsServer", task),
                args=args)

    def run_dependency(depname):
        def run(task, args=[]):
            with runner.Supervisor() as supervisor:
                supervisor.start_process([dep_executable(depname, task)] + args, intercept_output=False)
        return run

    def run_statistics_tester(task, args=[]):
        with runner.Supervisor() as supervisor:
            supervisor.start_process(
                [dep_executable("build:StatisticsTester", task)] + args,
                intercept_output=False)

    def dep_executable(depname, task):
        task = get_current_task(make_concrete_task_name(depname, task.name.config))
        return get_task_assembly_path(task)

    def export_cloud_depencency(task, args):
        assert len(args) == 2, "Usage: export-app -- app_name destination_path"
        project, output_dir = args
        make_clean_dir(output_dir)  # Important, because Dotfuscator will obfuscate everything in this directory that's listed in the config file
        task_name = make_concrete_task_name("build:" + project, task.name.config)
        task = get_current_task(task_name)
        assert task is not None, "So such dependency: %s" % (task_name,)
        copy_msbuildproj_output(task, output_dir)

    badumna_cloud_tasks = [
            Task('clean:BadumnaCloud',
                clean_badumna_cloud,
            ),

            Task('build:BadumnaCloud:db:init',
                run_django,
                args=['syncdb', '--noinput', '--verbosity=0'],
            ),

            Task('build:BadumnaCloud:db:migrate',
                run_django,
                args=['migrate'],
                deps=['build:BadumnaCloud:db:init'],
            ),

            Task('package:BadumnaCloud:export',
                export_cloud_depencency,
                deps=['build:StatisticsServer', 'build:SeedPeer','build:MatchmakingServer']),

            Task('build:BadumnaCloud',
                assemble.no_op,
                deps=['build:BadumnaCloud:db:migrate'],
            ),

            Task('test:BadumnaCloud:unit',
                run_django,
                args=['test', 'unit', '--noinput', '--traceback'],
                deps=['build:BadumnaCloud'],
            ),

            Task('test:BadumnaCloud:integration',
                run_django,
                args=['test', 'integration', '--noinput', '--traceback'],
                deps=['build:BadumnaCloud'],
            ),
    ]

    def generate_monotouch_projects(task):
        build_type = 'Release' if 'release' in task.name.config else 'Debug'
        constants = 'DEV_TRIAL' if 'trial' in task.name.config else ''
        command = P('Source/iOS/Build/UpdateMonoProject/bin', build_type, 'UpdateMonoProject.exe')
        assemble.check_call(['mono', command, P('Source/iOS/Badumna/Badumna.csproj.tmpl'), P('Source/Badumna/Badumna.csproj'), constants])
        assemble.check_call(['mono', command, P('Source/iOS/Dei/Dei.csproj.tmpl'), P('Source/ManagementTools/Dei/Dei/Dei.csproj'), constants])

    def osx_build(task):
        output_dir = RP(task.base_output_dir, '-'.join(sorted(task.name.config)))
        mkdir(output_dir)
        jenkins.osx_build(task.target + task.name.orig_config, task.archive, output_dir)

    mono_touch_tasks = [
        Task('build:versioning-mt',  # HACK: Separate versioning task for MonoTouch, because mdtool doesn't build Versioning.csproj correctly
            assemble.task_exec,
            command_line=['python', 'update_template.py'],
            working_dir=P('Source/Versioning')),
        Task('build:update-mono-project',
            monodev_build,
            path=P('Source/iOS/Build/UpdateMonoProject/UpdateMonoProject.sln'),
            platform='x86'),
        Task('build:generate-monotouch-projects',
            generate_monotouch_projects,
            ['build:update-mono-project']),
        Task('build:badumna-mt',
            monodev_build,
            ['build:generate-monotouch-projects', 'build:versioning-mt'],
            path=P('Source/iOS/Badumna.sln')),
        Task('clean:badumna-mt',
            monodev_clean,
            ['build:generate-monotouch-projects', 'build:versioning-mt'],
            path=P('Source/iOS/Badumna.sln')),
        Task('build:badumna-mt-win',
            osx_build,
            target='package:badumna-mt',
            archive='Installers/Badumna-Xamarin.iOS/Assemblies/*.dll',
            base_output_dir=RP('build/osx'))
    ]

    # Not using copy_msbuildproj_output because when copying into Assemblies, it will clean stuff out of Assemblies/Android
    def copy_app_lib(dest, source_task_name, task):
        mkdir(dest)
        copy_file(get_task_assembly_path(get_current_task(make_concrete_task_name(source_task_name, task.name.config))), dest)

    def copy_monotouch_libs(task):
        dest = P('Source/Applications/Assemblies/iOS')
        mkdir(dest)
        path = P('Source/iOS/Dei/bin')
        if 'release' in task.name.config:
            path = P(path, 'Release')
        else:
            path = P(path, 'Debug')

        for f in glob_dir(path, '*.dll'):
            copy_file(f, dest)

    app_lib_dotnet = ['build:' + x for x in ['Badumna', 'Dei', 'GermHarness']]
    app_lib_android = ['build:' + x for x in ['Badumna[android]', 'Dei[android]', 'Badumna', 'Dei', 'GermHarness']]  # GermHarness relies on the normal .NET Badumna and Dei assemblies

    applib_tasks = [
        Task('applibs:dotnet',
            seq([partial(copy_app_lib, P('Source/Applications/Assemblies/Desktop'), name) for name in app_lib_dotnet],
                lambda task: copy_file(P('Third Party/Mono.Options/bin/Mono.Options.dll'), P('Source/Applications/Assemblies/Desktop'))),
            app_lib_dotnet),

        Task('applibs:android',
            seq([partial(copy_app_lib, P('Source/Applications/Assemblies/Android'), name) for name in app_lib_android],
                lambda task: copy_file(P('Third Party/Mono.Options/bin/Mono.Options.dll'), P('Source/Applications/Assemblies/Android'))),
            app_lib_android),

        Task('applibs:monotouch',
             copy_monotouch_libs,
            ['build:badumna-mt'])
    ]

    def generate_unity_bootcamp_demo(task):
        preprocessor = PreProcessor()        

        root = P('Source/Unity/Unity/Scripts')
        master_dir = P(root, 'Bootcamp')
        scripts = glob_dir(P(master_dir, 'Scripts/network'), '*.cs')        
        scripts += glob_dir(P(master_dir, 'Scripts/misc'), '*.js')        
        scripts += glob_dir(P(master_dir, 'Scripts/soldier'), '*.js')        
        scripts += glob_dir(P(master_dir, 'Scripts/weapons'), '*.js')        

        demo = [("Bootcamp", [])]

        for demo_name, demo_constants in demo:
            dest = P(root, 'Generated', demo_name)
            doc_dest = P(root, 'Generated-Docs', demo_name)
            logger.info("Generating: %s -> %s", master_dir, dest)
            assemble.make_clean_dir(dest)
            assemble.make_clean_dir(doc_dest)
            for script in scripts:
                filename = os.path.split(script)[1]
                preprocessor.preprocess(
                    script,
                    P(dest, filename),
                    demo_constants,
                    docDest=doc_dest)

    def generate_unity_angrybots_demo(task):
        preprocessor = PreProcessor()        

        root = P('Source/Unity/Unity/Scripts')
        master_dir = P(root, 'AngryBots')
        scripts = glob_dir(P(master_dir, 'Badumna/Resources'), '*.cs')        
        scripts += glob_dir(P(master_dir, 'Plugins'), '*.cs')        
        scripts += glob_dir(P(master_dir, 'Plugins/Network'), '*.cs')        
        scripts += glob_dir(P(master_dir, 'Scripts/AI'), '*.js')        
        scripts += glob_dir(P(master_dir, 'Scripts/Managers'), '*.js')        
        scripts += glob_dir(P(master_dir, 'Scripts/Modules'), '*.js')        
        scripts += glob_dir(P(master_dir, 'Scripts/Weapon'), '*.js')        

        demo = [("AngryBots", [])]

        for demo_name, demo_constants in demo:
            dest = P(root, 'Generated', demo_name)
            doc_dest = P(root, 'Generated-Docs', demo_name)
            logger.info("Generating: %s -> %s", master_dir, dest)
            assemble.make_clean_dir(dest)
            assemble.make_clean_dir(doc_dest)
            for script in scripts:
                filename = os.path.split(script)[1]
                preprocessor.preprocess(
                    script,
                    P(dest, filename),
                    demo_constants,
                    docDest=doc_dest)

    def generate_unity_cloud_demos(task):
        preprocessor = PreProcessor()

        root = P('Source/Unity/Unity/Scripts')
        master_dir = P(root, 'Master')
        scripts = glob_dir(master_dir, '*.cs')

        demos = [
            ("BadumnaCloud-Scene", []),
            ("Scene-Demo1-Replication", ["REPLICATION"]),
            ("Scene-Demo2-RPC", ["REPLICATION", "RPC"]),
            ("Scene-Demo3-Chat", ["REPLICATION", "RPC", "CHAT"]),
            ("BadumnaCloud-Match", ["MATCH"]),
            ("Match-Demo1-Matchmaking", ["MATCH", "MATCH_BASIC"]),
            ("Match-Demo2-Controller", ["MATCH", "MATCH_BASIC", "MATCH_CONTROLLER"]),
            ("Match-Demo3-Replication", ["MATCH", "MATCH_BASIC", "MATCH_CONTROLLER", "MATCH_REPLICATION"]),
            ("Match-Demo4-RPC", ["MATCH", "MATCH_BASIC", "MATCH_CONTROLLER", "MATCH_REPLICATION", "MATCH_RPC"]),
            ("Match-Demo5-Chat", ["MATCH", "MATCH_BASIC", "MATCH_CONTROLLER", "MATCH_REPLICATION", "MATCH_RPC", "MATCH_CHAT"]),
            ("Match-Demo6-HostedEntity", ["MATCH", "MATCH_BASIC", "MATCH_CONTROLLER", "MATCH_REPLICATION", "MATCH_RPC", "MATCH_CHAT", "MATCH_HOSTED_ENTITY"]),
            ("Match-Demo7-Advanced", ["MATCH", "MATCH_BASIC", "MATCH_CONTROLLER", "MATCH_REPLICATION", "MATCH_RPC", "MATCH_CHAT", "MATCH_HOSTED_ENTITY", "MATCH_ADVANCE"])]
            

        for demo_name, demo_constants in demos:
            dest = P(root, 'Generated', demo_name)
            doc_dest = P(root, 'Generated-Docs', demo_name)
            logger.info("Generating: %s -> %s", master_dir, dest)
            assemble.make_clean_dir(dest)
            assemble.make_clean_dir(doc_dest)
            for script in scripts:
                filename = os.path.split(script)[1]
                preprocessor.preprocess(
                    script,
                    P(dest, filename),
                    demo_constants,
                    docDest=doc_dest)

    def generate_unity_demos(task):
        preprocessor = PreProcessor()

        root = P('Source/Unity/Unity/Scripts')
        master_dir = P(root, 'Master')
        scripts = glob_dir(master_dir, '*.cs')

        demos = [
            ("BadumnaPro-Scene", ["PRO"]),
            ("Scene-Demo1-Replication(Pro)", ["PRO", "REPLICATION"]),
            ("Scene-Demo2-RPC(Pro)", ["PRO", "REPLICATION", "RPC"]),
            ("Scene-Demo3-Chat(Pro)", ["PRO", "REPLICATION", "RPC", "CHAT"]),
            ("Scene-Demo4-VerifiedIdentity(Pro)", ["PRO", "REPLICATION", "RPC", "CHAT", "DEI"]),
            ("Scene-Demo5-ArbitrationServer(Pro)", ["PRO", "REPLICATION", "RPC", "CHAT", "DEI", "ARBITRATION_SERVER"]),
            ("Scene-Demo6-PrivateChat(Pro)", ["PRO", "REPLICATION", "RPC", "CHAT", "DEI", "ARBITRATION_SERVER", "PRIVATE_CHAT"]),
            ("BadumnaPro-Match", ["PRO", "MATCH"]),
            ("Match-Demo1-Matchmaking(Pro)", ["PRO", "MATCH", "MATCH_BASIC"]),
            ("Match-Demo2-Controller(Pro)", ["PRO", "MATCH", "MATCH_BASIC", "MATCH_CONTROLLER"]),
            ("Match-Demo3-Replication(Pro)", ["PRO", "MATCH", "MATCH_BASIC", "MATCH_CONTROLLER", "MATCH_REPLICATION"]),
            ("Match-Demo4-RPC(Pro)", ["PRO", "MATCH", "MATCH_BASIC", "MATCH_CONTROLLER", "MATCH_REPLICATION", "MATCH_RPC"]),
            ("Match-Demo5-Chat(Pro)", ["PRO", "MATCH", "MATCH_BASIC", "MATCH_CONTROLLER", "MATCH_REPLICATION", "MATCH_RPC", "MATCH_CHAT"]),
            ("Match-Demo6-HostedEntity(Pro)", ["PRO", "MATCH", "MATCH_BASIC", "MATCH_CONTROLLER", "MATCH_REPLICATION", "MATCH_RPC", "MATCH_CHAT", "MATCH_HOSTED_ENTITY"]),
            ("Match-Demo7-Advanced(Pro)", ["PRO", "MATCH", "MATCH_BASIC", "MATCH_CONTROLLER", "MATCH_REPLICATION", "MATCH_RPC", "MATCH_CHAT", "MATCH_HOSTED_ENTITY", "MATCH_ADVANCE"])
        ]

        for demo_name, demo_constants in demos:
            dest = P(root, 'Generated', demo_name)
            doc_dest = P(root, 'Generated-Docs', demo_name)
            logger.info("Generating: %s -> %s", master_dir, dest)
            assemble.make_clean_dir(dest)
            assemble.make_clean_dir(doc_dest)
            for script in scripts:
                filename = os.path.split(script)[1]
                preprocessor.preprocess(
                    script,
                    P(dest, filename),
                    demo_constants,
                    docDest=doc_dest)

    def generate_dotnet_cloud_demos(task):       
        demos = [
            ("Demo1-Replication", []),
            ("Demo2-Chat", ["CHAT"]),]
        generate_dotnet_demos(demos)

    def generate_dotnet_pro_demos(task):
        demos = [
            ("Demo1-Replication(Pro)", ["PRO"]),
            ("Demo2-Chat(Pro)", ["PRO", "CHAT"]),]
        generate_dotnet_demos(demos, pro=True)        
        

    def generate_dotnet_demos(demos, pro=False):
        builder = PreProcessor()
        root = RP('Source/Applications/CloudDemo')
        platforms = ("Windows", "Xamarin.iOS", "Xamarin.Android")
        platform_constants = {"Windows" : ["WINDOWS"], "Xamarin.iOS" : ["IOS", "TOUCH"], "Xamarin.Android" : ["ANDROID", "TOUCH"]}

        shared_source = ("Game1.cs", "Avatar.cs", "OriginalAvatar.cs")
        temp_dir = mkdtemp()
        try:
            for platform in platforms:
                master_dir = RP(root, 'Master', platform)
                # we make a fresh export to eliminate any guff
                # in the working copy (e.g compiler output)
                template_dir = P(temp_dir, platform, "Template")
                vcs_export(master_dir, template_dir)
                if platform != "Windows":
                    for filename in shared_source:
                        shutil.copy(P(temp_dir, "Windows", "Template", filename), template_dir)
                    lib_dir = P(root, platform + ('-Pro-Generated' if pro else '-Generated'), "Libraries")
                    mkdir(lib_dir)
                    for assembly in iglob(P("Third Party/MonoGame/Binaries", platform, "*.dll")):
                        shutil.copy(assembly, lib_dir)
                for demo_name, demo_constants in demos:
                    demo_dir = P(root, platform + ('-Pro-Generated' if pro else '-Generated'), demo_name)
                    doc_dir = P(root, platform + ('-Pro-Generated-Docs' if pro else '-Generated-Docs'), demo_name)
                    assemble.make_clean_dir(doc_dir)
                    logger.info("Generating: %s", demo_dir)
                    builder.copy_folder(template_dir, demo_dir)
                    demo_sln = P(demo_dir, "BadumnaCloudDemo.sln")
                    symbols = demo_constants + platform_constants[platform]
                    builder.preprocess(demo_sln, P(demo_dir, demo_name + ".sln"), symbols)
                    assemble.erase(demo_sln)
                    game_file = RP('Game1.cs')
                    builder.preprocess(P(template_dir, game_file), P(demo_dir, game_file), symbols, docDest=doc_dir)
        finally:
            assemble.erase(temp_dir)

    sphinx_build_cmd = [BUNDLED_PYTHON, P('Documentation/Sphinx/build.py')]

    # for the check build, we do a clean build, with all warnings enabled and no caching of results / environment
    sphinx_clean_check_args = ['-W','-n', '-a', '-q']

    sphinx_root = P('Documentation/Sphinx')
    sphinx_build_dir = P(sphinx_root, '_build')
    tasks = chain(
        msbuild_tasks,
        cpp_tasks,
        badumna_cloud_tasks,
        mono_touch_tasks,
        applib_tasks,

        [
        Task('build:obfuscated',
            obfuscate,
            ['build:{}'.format(x) for x in ['Badumna', 'TrustedComponent', 'Diagnostics', 'Dei', 'GermHarness', 'X509CertificateCreator', 'NetworkSimulator', 'Tunnel', 'DeiServer']]),

        Task('build:Badumna-obfuscated',
            obfuscate,
            ['build:Badumna']),

        Task('build:Badumna-Dei-obfuscated',
            obfuscate,
            ['build:Badumna', 'build:Dei']),

        Task('build:docs:apidocs',
            build_api_docs,
            ['build:{}[release]'.format(x) for x in ['Badumna', 'Dei', 'GermHarness', 'Dei.AdminClient']]),

        Task('build:docs:website',
            assemble.task_exec,
            command_line=sphinx_build_cmd,
            working_dir=sphinx_root),

        Task('build:demosource:unity-bootcamp',
            generate_unity_bootcamp_demo),

        Task('build:demosource:unity-angrybots',
            generate_unity_angrybots_demo),

        Task('build:demosource:unity:badumna-pro',
            generate_unity_demos),

        Task('build:demosource:unity',
            generate_unity_cloud_demos),

        Task('build:CloudDemo:dotnet',
            assemble.no_op,
            deps = ['build:Demo1-Replication', 'build:Demo2-Chat']),

        Task('build:demosource:dotnet',
            generate_dotnet_cloud_demos),

        Task('build:demosource:dotnet:badumna-pro',
            generate_dotnet_pro_demos),

        Task('clean:docs',
            lambda task: assemble.erase(P("Documentation/Sphinx/_build"))),

        Task('build:docs:sdk',
            assemble.task_exec,
            ['build:demosource:dotnet', 'build:demosource:dotnet:badumna-pro', 'build:demosource:unity', 'build:demosource:unity-bootcamp', 'build:demosource:unity-angrybots', 'build:demosource:unity:badumna-pro'],
            command_line=sphinx_build_cmd + ['sdk'],
            working_dir=sphinx_root,
            output_dir=P(sphinx_build_dir, 'sdk')),

        Task('build:docs:cloud-web',
            assemble.task_exec,
            ['build:demosource:dotnet', 'build:demosource:dotnet:badumna-pro', 'build:demosource:unity', 'build:demosource:unity-bootcamp', 'build:demosource:unity-angrybots', 'build:demosource:unity:badumna-pro'],
            command_line=sphinx_build_cmd + ['cloud-web'],
            working_dir=sphinx_root,
            output_dir=P(sphinx_build_dir, 'cloud-web')),

        Task('check:slow:docs:sdk',
            assemble.task_exec,
            ['build:demosource:dotnet', 'build:demosource:dotnet:badumna-pro', 'build:demosource:unity', 'build:demosource:unity-bootcamp', 'build:demosource:unity-angrybots', 'build:demosource:unity:badumna-pro'],
            command_line=sphinx_build_cmd + ['sdk'] + sphinx_clean_check_args,
            working_dir=sphinx_root,
            output_dir=P(sphinx_build_dir, 'sdk')),

        Task('check:fragile:docs:external-links',
            assemble.task_exec,
            command_line = sphinx_build_cmd + ['linkcheck'],
            working_dir = sphinx_root),

        Task('check:slow:docs:spelling',
            assemble.task_exec,
            ['build:demosource:dotnet', 'build:demosource:dotnet:badumna-pro', 'build:demosource:unity', 'build:demosource:unity-bootcamp', 'build:demosource:unity-angrybots', 'build:demosource:unity:badumna-pro'],
            command_line = sphinx_build_cmd + ['spelling'],
            working_dir = sphinx_root),

        Task('check:quick:docs:apilinks',
            assemble.task_exec,
            ['build:docs:apidocs'],
            command_line = [sys.executable, P(sphinx_root, 'check-apilink-directives.py')],
            working_dir = sphinx_root),
        ]
    )


    def get_package_name(base_name):
        return lambda config: base_name + ('-Trial' if 'trial' in config else '')

    management_tools_steps = [
        ('build:BadumnaNetworkController', package_web_application(RP('ManagementTools/ControlCenter'))),
        ('build:SimplexPrestoManifesto', None),  # Required for package_control_center_service
        ('build:Tunnel', seq(
            package_assembly(RP('ManagementTools/HttpTunnel')),
            package_control_center_service('HttpTunneling', 'HTTP Tunnel Server', RP('ManagementTools/ControlCenter/Services')))),
        ('build:SeedPeer', seq(
            package_assembly(RP('ManagementTools/SeedPeer')),
            package_control_center_service('SeedPeer', 'Seed Peer', RP('ManagementTools/ControlCenter/Services')))),
        ('build:DeiServer', seq(
            package_assembly(RP('ManagementTools/Dei/DeiServer')),
            package_export(RP('Source/ManagementTools/Dei/DeiAccountManager'), RP('ManagementTools/Dei/DeiAccountManager')),
            package_control_center_service('Dei', 'Dei Server', RP('ManagementTools/ControlCenter/Services'))
        )),
        ('build:OverloadPeer', seq(
            package_assembly(RP('ManagementTools/OverloadPeer')),
            package_control_center_service('OverloadPeer', 'Overload Peer', RP('ManagementTools/ControlCenter/Services')))),
        ('build:StatisticsServer', seq(
            package_assembly(RP('ManagementTools/StatisticsServer')),
            package_control_center_service('StatisticsServer', 'Statistics Server', RP('ManagementTools/ControlCenter/Services')))),
        ('build:WebSignUpDemo', package_web_application(RP('ManagementTools/Dei/WebSignUpDemo'))),
    ]

    management_tools_post_obfuscation_steps = [
        ('build:Tunnel', finish_packaging_control_center_service('HttpTunneling', 'HTTP Tunnel Server', RP('ManagementTools/ControlCenter/Services'))),
        ('build:SeedPeer', finish_packaging_control_center_service('SeedPeer', 'Seed Peer', RP('ManagementTools/ControlCenter/Services'))),
        ('build:DeiServer', finish_packaging_control_center_service('Dei', 'Dei Server', RP('ManagementTools/ControlCenter/Services'))),
        ('build:OverloadPeer', finish_packaging_control_center_service('OverloadPeer', 'Overload Peer', RP('ManagementTools/ControlCenter/Services'))),
        ('build:StatisticsServer', finish_packaging_control_center_service('StatisticsServer', 'Statistics Server', RP('ManagementTools/ControlCenter/Services')))
    ]

    manual_steps = [
        ('build:docs:sdk', package_copyoutput(RP('Documentation/Manual'))),
    ]

    assemblies_steps = [
        ('build:Badumna', package_assembly(RP('Assemblies/Desktop'))),
        ('build:Dei', package_assembly(RP('Assemblies/Desktop'))),
        ('build:Dei.AdminClient', package_assembly(RP('Assemblies/Desktop'))),
        ('build:GermHarness', package_assembly(RP('Assemblies/Desktop')))
    ]

    obfuscation_steps = [
        ('build:obfuscated', package_replace_all_obfuscated),   # Obfuscation must be at end, and must have same config as the assembly steps (otherwise the obfuscation step will produce assemblies with the wrong config)
        ('build:Badumna-Dei-obfuscated[android]', package_obfuscated(RP('Assemblies/Android'))) # Must be after package_replace_all_obfuscated, so it doesn't get replaced
    ]

    cpp_cloud_demos = [
        'Demo0-Initialization',
        'Match-Demo',
        'Scene-Demo1-Replication',
        'Scene-Demo2-ProximityChat',
        'Scene-Demo3-DeadReckoning',
        'Scene-Demo4-MultipleScenes'
    ]

    # Packaging steps are executed in order.
    # Steps are tuples of (dependency, packaging_function(task, build_dir))
    packaging = {
        'ControlCenter': dict(
            get_package_name=get_package_name("ControlCenter"),
            relative_path=RP("Installers/ControlCenter/ManagementTools/ControlCenter"),
            steps = list(chain(
                management_tools_steps,
                management_tools_post_obfuscation_steps,
            )),
        ),

        'dotnet': dict(
            get_package_name=get_package_name('BadumnaPro-dotNET-v' + VERSION),
            steps=list(chain(
                [(None, package_export(RP('Source/Applications/Assemblies'), RP('Assemblies')))], # copy all the checked in dlls under Assemblies folder.
                assemblies_steps,
                [('build:badumna-mt-win', lambda task, build_dir: [mkdir(P(build_dir, 'Assemblies/iOS'))] + [copy_file(x, P(build_dir, 'Assemblies/iOS')) for x in iglob(P(task.base_output_dir, '-'.join(sorted(task.name.config)), '*'))])],
                management_tools_steps,
                manual_steps, [
                ('build:docs:apidocs', seq(
                    package_apidoc,   # must be after assemblies_steps, because package_assembly deletes the xml files
                    package_platform_xmldocs(RP('Assemblies/Desktop/Badumna.xml'), RP('Assemblies/Android/Badumna.Android'), RP('Assemblies/iOS/Badumna.Xamarin.iOS')),
                    package_platform_xmldocs(RP('Assemblies/Desktop/Dei.xml'), RP('Assemblies/Android/Dei.Android'), RP('Assemblies/iOS/Dei.Xamarin.iOS')))),
                ('build:demosource:dotnet:badumna-pro', seq(
                    package_copytree(P('Source/Applications/CloudDemo/Windows-Pro-Generated'), RP('Demo/Windows')),
                    package_copytree(P('Source/Applications/CloudDemo/Xamarin.iOS-Pro-Generated'), RP('Demo/Xamarin.iOS')),
                    package_copytree(P('Source/Applications/CloudDemo/Xamarin.Android-Pro-Generated'), RP('Demo/Xamarin.Android')),
                ))],
                obfuscation_steps,  # must be at the end, because it does package_replace_all_obfuscated
                management_tools_post_obfuscation_steps
            ))
        ),

        'dotnet-cloud': dict(
            get_package_name=get_package_name('BadumnaCloud-dotNET-v' + VERSION),
            steps=list(chain(
                [('build:Badumna', package_assembly(RP('Assemblies/Desktop'))),
                ('build:demosource:dotnet', seq(
                    package_copytree(P('Source/Applications/CloudDemo/Windows-Generated'), RP('Demo/Windows')),
                    package_copytree(P('Source/Applications/CloudDemo/Xamarin.iOS-Generated'), RP('Demo/Xamarin.iOS')),
                    package_copytree(P('Source/Applications/CloudDemo/Xamarin.Android-Generated'), RP('Demo/Xamarin.Android')),
                    expect_content_in_packaged_file('Demo/Windows/Demo1-Replication/Game1.cs', 'Put your ID here!'),
                )),
                ('build:docs:sdk', package_copyoutput(RP('Documentation/Manual'))),
                ('build:docs:apidocs', seq(
                    package_apidoc,   # must be after assemblies_steps, because package_assembly deletes the xml files
                    lambda _, build_dir: cleanup(P(build_dir, 'Assemblies/Desktop'), ['Dei.xml', 'Dei.AdminClient.xml', 'GermHarness.xml']),
                    package_platform_xmldocs(RP('Assemblies/Desktop/Badumna.xml'), RP('Assemblies/Android/Badumna.Android'), RP('Assemblies/iOS/Badumna.Xamarin.iOS')))) ],
                obfuscation_steps,  # must be at the end, because it does package_replace_all_obfuscated
                [(None, lambda _, build_dir: jenkins.osx_build('package:badumna-mt[release]', 'Installers/Badumna-Xamarin.iOS/Assemblies/Badumna.Xamarin.iOS.dll', P(build_dir, 'Assemblies/iOS'))),
                (None, lambda _, build_dir: cleanup(P(build_dir, 'Assemblies/Android'), ['Dei.Android.*']))]
            ))
        ),

        'server-cloud': dict(
            get_package_name=get_package_name('BadumnaCloud-Server-v' + VERSION),
            steps=[
                    # used by deployment tasks to correlate artifacts with git commits
                    (None, export_git_head('GIT_COMMIT')),

                    ('build:SeedPeer', package_assembly(RP('SeedPeer'))),
                    ('build:StatisticsServer', package_assembly(RP('StatisticsServer'))),
                    ('build:MatchmakingServer', package_assembly(RP('MatchmakingServer'))),

                    ('build:docs:cloud-web', package_copyoutput(RP('Documentation/Sphinx'))),

                    # These directories get copied to the servers, so we can use the files
                    # to check server component versions
                    (None, export_git_head('SeedPeer/GIT_COMMIT')),
                    (None, export_git_head('StatisticsServer/GIT_COMMIT')),
                    (None, export_git_head('MatchmakingServer/GIT_COMMIT')),

                    # use this instead of `obfuscation_steps`, as that generates unnecessary android cruft.
                    ('build:obfuscated', package_replace_all_obfuscated),
            ],
        ),


        'unity': dict(
            get_package_name=get_package_name('BadumnaPro-Unity-v' + VERSION),
            steps=list(chain(
                assemblies_steps,
                management_tools_steps,
                manual_steps, [
                ('build:docs:apidocs', seq(
                    package_apidoc,   # must be after assemblies_steps, because package_assembly deletes the xml files
                    package_platform_xmldocs(RP('Assemblies/Desktop/Badumna.xml'), RP('Assemblies/Android/Badumna.Android'), RP('Assemblies/iOS/Badumna.Unity.iOS')),
                    package_platform_xmldocs(RP('Assemblies/Desktop/Dei.xml'), RP('Assemblies/Android/Dei.Android'), RP('Assemblies/iOS/Dei.Unity.iOS')))),
                (None, package_export(RP('Source/Applications/Demo/Binaries'), RP('Demo/Binaries'))),
                (None, package_export(RP('Source/Applications/Demo/Source/DemoArbitrationServer'), RP('Demo/Source/DemoArbitrationServer'))),
                (None, package_export(RP('Source/Applications/Demo/Source/DemoArbitrationEvents'), RP('Demo/Source/DemoArbitrationEvents'))),
                (None, lambda _, build_dir: copy_file(RP('Source/Applications/Demo/Source/DemoArbitrationServer.sln'), P(build_dir, 'Demo/Source'))),
                (None, lambda _, build_dir: assemble.msbuild(P(build_dir, 'Demo/Source/DemoArbitrationServer.sln'), properties=dict(OutDir=P(build_dir, 'Demo/Binaries') + '\\', BaseIntermediateOutputPath=RP('../../../../temp-demo-obj') + '\\', Configuration='Release')) ), 
                (None, lambda _, build_dir: cleanup(P(build_dir, 'Demo/Source'), ['Settings.StyleCop', '*.cachefile'], recurse=True)),
                ('build:DeiServer', package_assembly(RP('Demo/Binaries'))),
                (None, lambda _, build_dir: cleanup(P(build_dir, 'Demo/Binaries'), ['*.pdb', '*.xml'])),
                ],
                obfuscation_steps, [ # must be toward the end, because it does package_replace_all_obfuscated
                ('build:Badumna-Dei-obfuscated[~trace,unity-ios]', package_obfuscated(RP('Assemblies/iOS'))),
                (None, package_copy(P('Source/Unity/Unity/DemoAssets.unitypackage'), RP('Demo'))),                
                ('build:demosource:unity:badumna-pro', seq( # post-obfuscation, so that we get the obfuscated dll
                    package_unitypackages(
                        [P('Source/Unity/Unity', package + '.unitypackage')],
                        RP('Demo'),
                        dict([
                            ('Badumna.dll', RP('Assemblies/Desktop/Badumna.dll')),    
                            ('Badumna.xml', RP('Assemblies/Desktop/Badumna.xml')),
                            ('Dei.dll', RP('Assemblies/Desktop/Dei.dll')),
                            ('Dei.xml', RP('Assemblies/Desktop/Dei.xml'))] +
                            # note that we get the file list from `Master`, not `Generated`, because `Generated`
                            # does NOT exist at task definition time (it is created by build:demosource:unity)
                            [(fname, P('Source/Unity/Unity/Scripts/Generated', package, fname))
                                for fname in os.listdir('Source/Unity/Unity/Scripts/Master')] +
                            [(fname, P('Source/Unity/Unity/Assets', fname)) for fname in os.listdir('Source/Unity/Unity/Assets')])
                    ) for package in ['BadumnaPro-Scene', 'BadumnaPro-Match', 'Scene-Demo1-Replication(Pro)', 'Scene-Demo2-RPC(Pro)', 'Scene-Demo3-Chat(Pro)', 'Scene-Demo4-VerifiedIdentity(Pro)', 'Scene-Demo5-ArbitrationServer(Pro)', 'Scene-Demo6-PrivateChat(Pro)', 
                        'Match-Demo1-Matchmaking(Pro)', 'Match-Demo2-Controller(Pro)', 'Match-Demo3-Replication(Pro)', 'Match-Demo4-RPC(Pro)', 'Match-Demo5-Chat(Pro)', 'Match-Demo6-HostedEntity(Pro)', 'Match-Demo7-Advanced(Pro)']
                ))],
                management_tools_post_obfuscation_steps
            ))
        ),

        'unity-cloud': dict(
            get_package_name=get_package_name('BadumnaCloud-Unity-v' + VERSION),
            steps=list(chain(
                [('build:Badumna', package_assembly(RP('Assemblies/Desktop')))], [
                ('build:docs:sdk', package_copyoutput(RP('Documentation/Manual'))),
                ('build:docs:apidocs', seq(
                    package_apidoc,   # must be after assemblies_steps, because package_assembly deletes the xml files
                    lambda _, build_dir: cleanup(P(build_dir, 'Assemblies/Desktop'), ['Dei.xml', 'Dei.AdminClient.xml', 'GermHarness.xml']),
                    package_platform_xmldocs(RP('Assemblies/Desktop/Badumna.xml'), RP('Assemblies/Android/Badumna.Android'), RP('Assemblies/iOS/Badumna.Unity.iOS')))),
                ],
                obfuscation_steps, [ # must be toward the end, because it does package_replace_all_obfuscated
                ('build:Badumna-Dei-obfuscated[~trace,unity-ios]', package_obfuscated(RP('Assemblies/iOS'))),
                (None, lambda _, build_dir: cleanup(P(build_dir, 'Assemblies'), [RP('Android/Dei.Android.dll'), RP('iOS/Dei.Unity.iOS.dll')])),
                (None, lambda _, build_dir: mkdir(P(build_dir, 'Unity Packages'))),
                (None, package_copy(P('Source/Unity/Unity/DemoAssets.unitypackage'), RP('Unity Packages'))),
                ('build:demosource:unity', seq(  # post-obfuscation, so that we get the obfuscated dll
                    package_unitypackages(
                        [P('Source/Unity/Unity', package + '.unitypackage')],
                        RP('Unity Packages'),
                        dict([
                            ('Badumna.dll', RP('Assemblies/Desktop/Badumna.dll')),
                            ('Badumna.xml', RP('Assemblies/Desktop/Badumna.xml'))] +
                            # note that we get the file list from `Master`, not `Generated`, because `Generated`
                            # does NOT exist at task definition time (it is created by build:demosource:unity)
                            [(fname, P('Source/Unity/Unity/Scripts/Generated', package, fname))
                                for fname in os.listdir('Source/Unity/Unity/Scripts/Master')] +
                            [(fname, P('Source/Unity/Unity/Assets', fname)) for fname in os.listdir('Source/Unity/Unity/Assets')])
                    ) for package in ['BadumnaCloud-Scene', 'BadumnaCloud-Match', 'Scene-Demo1-Replication', 'Scene-Demo2-RPC', 'Scene-Demo3-Chat', 
                        'Match-Demo1-Matchmaking', 'Match-Demo2-Controller', 'Match-Demo3-Replication', 'Match-Demo4-RPC', 'Match-Demo5-Chat', 'Match-Demo6-HostedEntity', 'Match-Demo7-Advanced']
                ))]
            ))
        ),

        'cpp': dict(
            get_package_name=get_package_name('BadumnaPro-C++-v' + VERSION),
            steps=list(chain([
                (None, package_export(RP('Source/CppWrapper/BadumnaCpp'), RP('Source'))),
                (None, lambda _, build_dir: cleanup(P(build_dir, 'Source'), ['cpp_lint_check.sh', 'cpplint.py', '0001-Changes-to-build-the-runtime-for-iOS.patch', 'build_ios.sh', 'copy_ios_resources.sh', 'README_iOS'])),
                (None, package_replace_text(RP('Source'), 'Makefile', '^VERSION=.*', 'VERSION='+VERSION, expected_count=1, regexp=True)),
                (None, package_export(RP('Source/CppWrapper/ApiExamples'), RP('Examples/Source'))),
                (None, package_replace_text(RP('Examples/Source'), '*/*.sln', '..\\BadumnaCpp', '..\\..\\Source', expected_count=1)),
                (None, package_replace_text(RP('Examples/Source'), '*/*.vcxproj', '..\\BadumnaCpp', '..\\..\\Source', expected_count=2)),
                #(None, package_export(RP('Source/iOS/Demo/BadumnaDemo'), RP('iOS/Demo'))),
                ('build:cpp-apidocs', package_copyoutput(RP('Documentation/API'))),
                (None, package_export(RP('Source/Applications/Demo/Binaries'), RP('Examples/Binaries'))),
                ('build:Badumna', seq(package_assembly(RP('Source/libs')), package_assembly(RP('Examples/Binaries')))),
                ('build:Dei', seq(package_assembly(RP('Source/libs')), package_assembly(RP('Examples/Binaries')))),
                ('build:DeiServer', package_assembly(RP('Examples/Binaries'))),
                ('build:BadumnaCppApiExamples', package_cpp_examples(RP('Examples/Binaries'))) ],
                assemblies_steps,
                management_tools_steps,
                manual_steps, [
                ('build:obfuscated', package_replace_all_obfuscated) ],  # Obfuscation must be at end, and must have same config as the assembly steps (otherwise the obfuscation step will produce assemblies with the wrong config)
                management_tools_post_obfuscation_steps
            ))
        ),

        'dgc-test': dict(
            get_package_name=get_package_name('dgc-test'),
            steps=list(chain([
                ('build:BadumnaCppApiExamples', package_cpp_examples(RP('Examples/Binaries'))) ],
            ))
        ),

        'cpp-cloud': dict(
            get_package_name=get_package_name('BadumnaCloud-C++-v' + VERSION),
            steps=list(chain([
                (None, package_export(RP('Source/CppWrapper/BadumnaCpp'), RP('Source'))),
                (None, lambda _, build_dir: cleanup(P(build_dir, 'Source'), ['cpp_lint_check.sh', 'cpplint.py', '0001-Changes-to-build-the-runtime-for-iOS.patch', 'build_ios.sh', 'copy_ios_resources.sh', 'README_iOS'])),
                (None, package_replace_text(RP('Source'), 'Makefile', '^VERSION=.*', 'VERSION='+VERSION, expected_count=1, regexp=True)),
                (None, package_export(RP('Source/CppWrapper/ApiExamples'), RP('Examples/Source'))),
                (None, package_cleanup(RP('Examples/Source/*'), lambda x: os.path.isdir(x) and not os.path.basename(x) in cpp_cloud_demos)),
                (None, package_replace_text(RP('Examples/Source'), '*/*.sln', '..\\BadumnaCpp', '..\\..\\Source', expected_count=1)),
                (None, package_replace_text(RP('Examples/Source'), '*/*.vcxproj', '..\\BadumnaCpp', '..\\..\\Source', expected_count=2)),
                (None, package_replace_text(RP('Examples/Source'), '*/*.vcxproj', ';%(PreprocessorDefinitions)', ';CLOUD;%(PreprocessorDefinitions)', expected_count=1)),
                (None, package_replace_text(RP('Examples/Source'), 'Makefile', 'CLOUD_OPTIONS=', 'CLOUD_OPTIONS=-DCLOUD=1', expected_count=1)),
                #(None, package_export(RP('Source/iOS/Demo/BadumnaDemo'), RP('iOS/Demo'))),
                ('build:cpp-apidocs', package_copyoutput(RP('Documentation/API'))),
                ('build:Badumna', seq(package_assembly(RP('Source/libs')), package_assembly(RP('Examples/Binaries')))),
                ('build:BadumnaCppCloudApiExamples', package_cpp_examples(RP('Examples/Binaries'), 'cloud', cpp_cloud_demos)),
                ('build:Badumna', package_assembly(RP('Assemblies/Desktop'))),
                ('build:docs:sdk', package_copyoutput(RP('Documentation/Manual'))),
                ('build:obfuscated', package_replace_all_obfuscated) ],  # Obfuscation must be at end, and must have same config as the assembly steps (otherwise the obfuscation step will produce assemblies with the wrong config)
            ))
        ),

        'sdk-tester': dict(
            get_package_name=lambda x:'sdk-tester',
            steps=list(chain([
                (None, package_export(RP('Build/testing'), RP('testing'))),
                (None, package_export(RP('Third Party\Python27-env'), RP('Python27-env'))),
                (None, package_export(RP('Third Party\Python27'), RP('Python27'))),
                (None, package_copy(RP('Build/colorlog.py'), RP(''))),
                (None, package_copy(RP('Build/controlcenter_tester.py'), RP(''))),
                (None, package_copy(RP('Build/sdk_tester.py'), RP(''))),
                (None, package_copy(RP('Build/sdk_tester.cmd'), RP(''))),
            ]))
        ),
    }

    monotouch_obfuscate_assemblies = [
        P('Source/iOS/Dei/bin/Release/Badumna.Xamarin.iOS.dll'),
        P('Source/iOS/Dei/bin/Release/Dei.Xamarin.iOS.dll'),
        P('/Developer/MonoTouch/usr/lib/mono/2.1/mscorlib.dll'),
        P('/Developer/MonoTouch/usr/lib/mono/2.1/monotouch.dll'),
        P('/Developer/MonoTouch/usr/lib/mono/2.1/System.dll'),
        P('/Developer/MonoTouch/usr/lib/mono/2.1/System.Xml.dll')
    ]

    def obfuscate_monotouch(output_dir):
        def f(task, build_dir):
            jenkins_obfuscate(monotouch_obfuscate_assemblies, P(build_dir, output_dir))
        return f

    def build_and_package_native_ios(task, build_dir):
        def build_native(mode):
            assemble.check_call([P('Source/CppWrapper/BadumnaCpp/build_ios.sh'), P(build_dir, mode)])
            output_dir = P(build_dir, 'native', mode)
            mkdir(output_dir)
            ios_build_dir = P('Source/CppWrapper/BadumnaCpp/Build/iOS')
            shutil.copytree(P(ios_build_dir, 'Badumna.framework'), P(output_dir, 'Badumna.framework'), symlinks=True)
            copy_tree(P(ios_build_dir, 'BadumnaResources'), P(output_dir, 'BadumnaResources'))

        build_native('release')
        build_native('trial')

    packaging['osx-built-components'] = dict(
            get_package_name=lambda _: 'OSX-Built-Components-v' + VERSION,
            strict_ordering=True,
            steps=[
                ('clean:badumna-mt[release]', None),
                ('build:badumna-mt[release]', obfuscate_monotouch('release')),
                ('clean:badumna-mt[release,trial]', None),
                ('build:badumna-mt[release,trial]', obfuscate_monotouch('trial')),
#               DGC: Temporarily disabled building of native iOS products because they now need the Unity.iOS assemblies
#                    instead of the MonoTouch ones.  Bit of work to fix correctly, and we're not yet releasing the native
#                    iOS library, so I'm punting this for now.
#                (None, build_and_package_native_ios) # native package is built off the obfuscated release and trial versions, so they have to be done first.
            ]
        )

    packaging['badumna-mt'] = dict(
            get_package_name=lambda _: 'Badumna-Xamarin.iOS',
            strict_ordering=True,
            steps=[
                ('clean:badumna-mt[release]', None),
                ('build:badumna-mt[release]', obfuscate_monotouch('Assemblies'))
            ]
        )

    packaging_tasks = [
        Task('package:badumna-pro',
            assemble.no_op, [
            'package:dotnet[release]',
            'package:cpp[release]',
            'package:unity[release]' ]
        ),

        Task('windows-built-components',
            assemble.no_op,
            [
                'all-public-packages[trial]',
                'all-public-packages',
            ],
            strict_ordering=True,
        ),
        Task('package:cloud',
            assemble.no_op,
            [
                'package:unity-cloud',
                'package:dotnet-cloud',
                'package:cpp-cloud',
                'package:server-cloud',
            ],
            strict_ordering=True,
        ),
        Task('package:sdk-tester',
            assemble.no_op
        ),
    ]

    for taskname, pack in packaging.iteritems():
        deps = list(s[0] for s in pack['steps'] if s[0] is not None)
        if pack.get('strict_ordering', False) is False:
            deps = set(deps)
        packaging_tasks.append(Task('package:' + taskname, package, deps=deps, **pack))

    tasks = chain(tasks, packaging_tasks)

    dotnet_project_tasks = ['{}[{}]'.format(name, config) for name, config in product(
        [
            'Badumna',
            'Dei',
            'DeiServer',
            'Tunnel',
        ],
        ['debug','release']
    )]

    # high-level tasks for the continuous integration server (and devs) to run
    ci_tasks = [

            Task('ci:dotnet:build',
                assemble.no_op,
                #TODO: add 'CloudDemo:dotnet'
                ['build:{}'.format(task) for task in dotnet_project_tasks + ['BadumnaDemo.Windows']],
                desc="Compile only (in Debug and Release)"),

            Task('ci:dotnet:unit',
                assemble.no_op,
                ['test:{}'.format(task) for task in dotnet_project_tasks],
                desc="dotnet unit tests (Debug and Release)"),

            Task('ci:dotnet:integration',
                assemble.no_op,
                [], #TODO...
                desc="dotnet Integration tests"),

            Task('ci:cpp:unit',
                assemble.no_op,
                ['test:BadumnaCpp'],
                desc="C++ unit tests"),

            Task('ci:cpp:integration',
                assemble.no_op,
                ['test:BadumnaCppIntegration[release]'],
                desc="C++ (BadumnaAutoTest) integration tests"),

            Task('ci:cloud:unit',
                assemble.no_op,
                ['test:BadumnaCloud:unit'],
                desc='Badumna Cloud unit tests',
            ),

            Task('ci:cloud:integration',
                assemble.no_op,
                ['test:BadumnaCloud:integration'],
                desc='Badumna Cloud integration tests',
            ),

            Task('ci:all',
                assemble.no_op,
                [
                    'ci:dotnet:unit',
                    'ci:dotnet:integration',
                    'ci:cpp:unit',
                    'ci:cpp:integration',
                    'ci:cloud:unit',
                    'ci:cloud:integration',
                ],
                desc="test ALL of the things"),
            
            Task('ci:reset-workspace',
                vcs_clean,
                desc="Clean EVERYTHING (this task WILL delete un-committed changes)"),

            Task('ci:check',
                assemble.no_op,
                [
                    'check:quick',
                    'check:slow',
                ],
                desc="Run all automated checks (except `fragile` ones)"),

            Task('ci:build-all-win',
                assemble.no_op,
                [
                    'build:Agency',
                    'build:ApplicationStarter',
                    'build:AspNetHost',
                    #'build:AutoTesting',
                    'build:Badumna',
                    'build:Badumna.Package',
                    'build:Badumna.Package.Builder',
                    #'build:BadumnaCloud',
                    'build:BadumnaCpp',
                    'build:BadumnaCppAll',
                    'build:BadumnaCppApiExamples',
                    'build:BadumnaCppIntegration',
                    'build:BadumnaCppTest',
                    'build:BadumnaDemo.Windows',
                    'build:BadumnaNetworkController',
                    'build:BadumnaTests',
                    'build:CloudDemo',
                    'build:CloudDemo:dotnet',
                    'build:Dei',
                    'build:Dei.AdminClient',
                    'build:DeiAccountManager',
                    'build:DeiServer',
                    'build:DeiServerTests',
                    'build:DeiTests',
                    'build:Demo1-Replication',
                    'build:Demo2-Chat',
                    'build:Diagnostics',
                    'build:Germ',
                    'build:GermHarness',
                    'build:Germinator',
                    'build:LaunchGerm',
                    'build:Launcher',
                    'build:MatchmakingServer',
                    'build:NetworkSimulator',
                    'build:OverloadPeer',
                    'build:PerformanceMonitor',
                    'build:ProcessMonitor',
                    'build:ScriptCoreLib',
                    'build:ScriptCoreLib.Ultra.Library',
                    'build:ScriptCoreLib.Ultra.VisualBasic',
                    'build:ScriptCoreLibA',
                    'build:SeedPeer',
                    'build:SimplexPrestoManifesto',
                    'build:SqliteProvider',
                    'build:Statistics',
                    'build:StatisticsServer',
                    'build:StatisticsTester',
                    'build:TestUtilities',
                    'build:TrustedComponent',
                    'build:Tunnel',
                    'build:TunnelTests',
                    'build:Versioning',
                    'build:WebSignUpDemo',
                    'build:WebSignUpDemoPrebuild',
                    'build:X509CertificateCreator',
                    'build:cpp-apidocs',
                    'build:demosource:dotnet',
                    'build:demosource:dotnet:badumna-pro',
                    'build:demosource:unity',
                    'build:docs:apidocs',
                    'build:docs:sdk',
                    'build:docs:website',
                    'build:jsc',
                ],
                desc="Builds everything that can be built on Windows"),
        ]

    tasks = chain(tasks, ci_tasks)


    def run_windows_demo(task, args=[]):
        dei_server_task = get_current_task(make_concrete_task_name('build:DeiServer', task.name.config))
        build_task = [dep for dep in task.deps if dep.orig_name == 'build:BadumnaDemo.Windows']
        assert len(build_task) == 1
        build_task = build_task[0]
        project_paths = assemble.extract_subproject_paths(get_current_task(build_task).path)

        def assembly_path(name):
            '''get the assembly path for one of the solution's component projects'''
            # create a new name for the task using this task's config:
            task_name = make_concrete_task_name(name, task.name.config)
            task_path = project_paths[name]
            return get_assembly_path(task_name, task_path)

        runner.run_demo(args,
                dei_path = get_task_assembly_path(dei_server_task),
                arbitration_path = assembly_path("DemoArbitrationServer"),
                demo_path = assembly_path("DemoGame (Windows)")
        )

    def run_cloud_demo(task, args=[]):
        seed_peer_exe = dep_executable('build:SeedPeer', task)
        matchmaking_exe = dep_executable('build:MatchmakingServer', task)
        build_task = [dep for dep in task.deps if dep.orig_name == 'build:CloudDemo']
        assert len(build_task) == 1
        build_task = build_task[0]
        project_paths = assemble.extract_subproject_paths(get_current_task(build_task).path)

        p = argparse.ArgumentParser()
        p.add_argument('--matchmaking', '-m', action='store_true', help='run matchmaking server')
        p.add_argument('--server', action='store_true', help='run cloud server')
        p.add_argument('--stats', action='store_true', help='run stats server')
        p.add_argument('--all', '-a', action='store_true', help='run all optional services')
        p.add_argument('num_peers', nargs='?', type=int, help='number of peers (games) to run', default=2)
        opts = p.parse_args(args)

        if opts.all:
            opts.matchmaking = opts.server = opts.stats = True

        def assembly_path(name):
            '''get the assembly path for one of the solution's component projects'''
            # create a new name for the task using this task's config:
            task_name = make_concrete_task_name(name, task.name.config)
            task_path = project_paths[name]
            return get_assembly_path(task_name, task_path)

        with runner.Supervisor() as supervisor:
            if opts.server:
                os.environ['BADUMNA_CLOUD_URI'] = 'http://localhost:8000' # this is used by the CloudDemo executable
                runner.spawn_badumna_cloud(supervisor, badumna_cloud_root)
            if opts.stats:
                runner.spawn_statistics_server(
                    supervisor,
                    server_path=dep_executable("build:StatisticsServer", task))
            supervisor.start_process([seed_peer_exe, '--application-name','com.badumna.cloud0'], env = badumna_log_env(label='SeedPeer'))
            if opts.matchmaking:
                supervisor.start_process([matchmaking_exe, '--application-name','com.badumna.cloud0'], env = badumna_log_env(label='MatchmakingServer'))

            import time
            time.sleep(2)
            for i in range(0, opts.num_peers):
                supervisor.start_process([assembly_path('BadumnaCloudDemo')])

    def run_control_center(task, args=[]):
        main_task = get_current_task(make_concrete_task_name('package:ControlCenter', task.name.config))
        basedir = main_task.relative_path
        with runner.Supervisor() as supervisor:
            supervisor.start_process([RP(basedir, "ControlCenter.exe")] + args, intercept_output=False)

    def run_germ(task, args=[]):
        main_task = get_current_task(make_concrete_task_name('build:LaunchGerm', task.name.config))
        assembly_path = get_task_assembly_path(main_task)
        with runner.Supervisor() as supervisor:
            supervisor.start_process(
                    [assembly_path] + args,
                    cwd=P("Installers/ControlCenter/ManagementTools/ControlCenter/Germ"),
                    intercept_output=False)

    def run_matchmaking_server(task, args=[]):
        main_task = get_current_task(make_concrete_task_name('build:MatchmakingServer', task.name.config))
        assembly_path = get_task_assembly_path(main_task)
        with runner.Supervisor() as supervisor:
            supervisor.start_process(
                    [assembly_path] + args,
                    intercept_output=False)

    run_tasks = [
            Task('run:BadumnaDemo.Windows',
                run_windows_demo,
                ['build:BadumnaDemo.Windows','build:DeiServer']
            ),

            Task('run:CloudDemo',
                run_cloud_demo,
                ['build:CloudDemo','build:DeiServer', 'build:MatchmakingServer', 'build:SeedPeer', 'build:BadumnaCloud', 'build:StatisticsServer']
            ),

            Task('run:ControlCenter',
                run_control_center,
                deps=['package:ControlCenter'],
            ),

            Task('run:Germ',
                run_germ,
                deps=['build:LaunchGerm'],
            ),

            Task('run:MatchmakingServer',
                run_matchmaking_server,
                deps=['build:MatchmakingServer'],
            ),

            Task('run:BadumnaCloud',
                run_badumna_cloud,
                deps=['build:StatisticsServer', 'build:BadumnaCloud'],
            ),

            Task('run:StatisticsServer',
                run_statistics_server,
                deps=['build:StatisticsServer'],
            ),

            Task('run:StatisticsTester',
                run_statistics_tester,
                deps=['build:StatisticsTester'],
            ),

            Task('run:SeedPeer',
                run_dependency("build:SeedPeer"),
                deps=['build:SeedPeer']
            ),
    ]
    tasks = chain(tasks, run_tasks)



    badumna_cpp_path = RP("Source/CppWrapper/BadumnaCpp")
    badumna_demo_path = RP("Source/Applications/Demo")
    check_tasks = [
        Task('check:quick:buildscripts:whitespace', check_whitespace, path=RP("Build")),

        Task('check:quick:BadumnaCpp:whitespace', check_whitespace, path=badumna_cpp_path),
        Task('check:quick:BadumnaCpp:todos', check_todos, path=badumna_cpp_path),

        Task('check:quick:BadumnaCloud:todos', check_todos, path="Source/Web/BadumnaCloud"),
        Task('check:quick:BadumnaCloud:whitespace', check_whitespace, path="Source/Web/BadumnaCloud"),

        Task('check:quick:BadumnaDemo:whitespace', check_whitespace, path=badumna_demo_path),
        Task('check:quick:BadumnaDemo:todos', check_todos, path=badumna_demo_path),

        Task('check:quick:CloudDemo:whitespace', check_whitespace, path=RP("Source/Applications/CloudDemo")),
        Task('check:quick:CloudDemo:todos', check_todos, path=RP("Source/Applications/CloudDemo")),

        Task('check:quick:manual:whitespace', check_whitespace, path="Documentation/Sphinx"),
        Task('check:quick:manual:todos', check_todos, path="Documentation/Sphinx"),

        Task('check:quick:copyright:Badumna', check_copyright, path=RP("Source/Badumna")),
        Task('check:quick:copyright:BadumnaCpp', check_copyright, path=badumna_cpp_path),
        Task('check:quick:copyright:Demo', check_copyright, path=badumna_demo_path),
        #TODO: check the rest of our (non-released) source code - it's too noisy for now

        Task('pkgcheck', check_built_package),
    ]
    tasks = chain(tasks, check_tasks)

    return dict((p.name, p) for p in tasks)


def main():
    os.chdir(P(os.path.dirname(os.path.abspath(__file__)), '..'))

    global VERSION
    with open('VERSION', 'r') as version_file:
        VERSION = version_file.readline()

    args = assemble.parse_args()

    opts = args[0]
    if opts.ansi:
        # useful when piping into `less` and you want to retain colours
        import colorlog
        colorlog.ColorizingStreamHandler.is_tty = True
        colorlog.ColorizingStreamHandler.output_colorized = lambda self, message: self.stream.write(message)

    global SKIP_DOCS
    SKIP_DOCS = opts.skip_docs

    global BADUMNA_LOG_LEVEL
    BADUMNA_LOG_LEVEL = opts.badumna_log
    for envvar, val in generate_badumna_log_config(tags=opts.badumna_log_tags, label=opts.badumna_log_label):
        os.environ[envvar] = val;
        logger.info("export %s='%s'", envvar, val);

    assemble.add_path(P('Third Party/NUnit/NUnit 2.5.5/bin/net-2.0'))
    assemble.add_path(P('Third Party/NCover/NCover 1.5.8'))
    assemble.add_path(P('Third Party/NCover/NCoverExplorer-1.4.0.7'))
    assemble.add_path(P('Integration Tests/Tools'))
    
    # Convenient stuff outside our repo (don't warn if the path is wrong - people may have different install paths
    assemble.add_path('C:/Program Files (x86)/Mono-2.10.8/bin', warn=False)
    assemble.add_path('C:/Program Files (x86)/Mono-2.10.9/bin', warn=False)

    if os.environ.get('HOMEPATH', None) == '\\':
        # $HOMEPATH is not set reliably under mingw / cygwin / jenkins,
        # This'll do for now:
        import getpass
        homepath = r'\Users\{}'.format(getpass.getuser())
        os.environ['HOMEPATH'] = homepath
        logger.debug("[fixed] $HOMEPATH: " + homepath)

    try:
        assemble.main(args, define_tasks())
    except (AssertionError, subprocess.CalledProcessError) as e:
        import traceback
        exc = traceback.format_exc()
        logger.debug(exc)
        logger.error(str(e) or exc)
        sys.exit(1)
    except KeyboardInterrupt:
        logger.info("Interrupted")
        sys.exit(2)

if __name__ == '__main__':
    main()
