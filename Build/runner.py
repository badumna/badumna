"""
Utilities and functions for running complex tasks / scenarios (typically
involving multiple concurrent processes).

Using the Supervisor class ensures that child processes don't get left running,
and all child processed are stopped if something fails.
"""
import os
import sys
import subprocess
import time
import signal
import threading
import argparse
import contextlib
import tempfile
from Queue import Queue, Empty

import logging
import colorlog
handler = colorlog.ColorizingStreamHandler()
handler.setFormatter(logging.Formatter(fmt="%(levelname)8s|%(name)-25s: %(message)s"))

logger = logging.getLogger(__name__)
logger.addHandler(handler)
logger.propagate = False

DEFAULT_PATHS = {
    # these should never be used in build targets - they're just defaults for use when
    # we use this script outside the build
    'arbitration': 'Source/Applications/Demo/Source/DemoArbitrationServer/bin/x86/Debug/FriendListArbitrationServer.exe',
    'demo': 'Source/Applications/Demo/Source/DemoGame/bin/x86/Debug/BadumnaDemo (Windows).exe',
    'dei': 'Source/ManagementTools/Dei/DeiServer/bin/x86/Debug/DeiServer.exe'
}

def run_demo(args,
        arbitration_path=None,
        demo_path=None,
        dei_path=None):

    '''Run the windows demo with optional dei & arbitration servers'''
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--dei', action='store_true', help="run dei server")
    parser.add_argument('-a', '--arbitration', action='store_true', help="run arbitrator")
    parser.add_argument('num_peers', nargs='?', default=2, type=int, help="number of peers to run")
    opts = parser.parse_args(args)

    with Supervisor() as supervisor:
        arbitration_args = []

        if opts.dei:
            if dei_path is None:
                logger.warn("Using default dei path")
                dei_path = DEFAULT_PATHS['dei']

            arbitration_args.append("--dei-config-file=FriendListArbitrationServer.deiconfig")
            cwd="Source/Applications/Demo/Binaries"
            supervisor.start_process([dei_path, "-v"], cwd=cwd)
            time.sleep(2)

        if opts.arbitration:
            if arbitration_path is None:
                logger.warn("Using default arbitration path")
                arbitration_path = DEFAULT_PATHS['arbitration']

            cwd, exe = os.path.split(arbitration_path)
            supervisor.start_process([exe] + arbitration_args, cwd=cwd)
            time.sleep(2)

        if demo_path is None:
            demo_path = DEFAULT_PATHS['demo']
            logger.warn("Using default demo path")

        for i in range(0, opts.num_peers):
            cwd, exe = os.path.split(demo_path)
            supervisor.start_process([exe], cwd=cwd, name="Client #{}".format(i+1))

def run_badumna_cloud(args, basedir, stats_server_path):
    '''Run the Badumna Cloud server with optional stats server'''
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--stats', action='store_true', help="run stats server")
    opts = parser.parse_args(args)

    with Supervisor() as supervisor:
        spawn_badumna_cloud(supervisor, basedir)
        if opts.stats:
            spawn_statistics_server(supervisor, stats_server_path)

def spawn_badumna_cloud(supervisor, basedir):
    supervisor.start_process([sys.executable, os.path.join(basedir, 'manage.py'), 'runserver', '--noreload','--nothreading'], cwd=basedir, intercept_output=False)

def spawn_statistics_server(supervisor, server_path, args=[]):
    supervisor.start_process([server_path, '--http-serve', 'http://+:21257/'] + args, cwd=os.path.dirname(server_path), intercept_output=False)

@contextlib.contextmanager
def server_config(opts, name='unknown'):
    '''Generate a badumna XML config appropriate for the given options'''
    with open(os.path.join(os.path.dirname(__file__), 'server-config.tmpl.xml')) as f:
        template = make_template(f.read())

    with tempfile.NamedTemporaryFile(prefix='badumna-config-' + name + '_') as output:
        contents = template.render(opts).encode('UTF-8')
        output.write(contents)
        output.flush()
        logger.debug("Wrote %s, with config: %r", output.name, opts)
        # logger.debug(contents)
        yield output.name

def make_template(contents):
    import jinja2
    template = jinja2.Template(contents)
    template.environment.undefined = jinja2.StrictUndefined
    return template

def print_demo_config(opts):
    '''
    Prints out demonstration (WindowsDemo) config appropriate for the
    current remote options.
    '''
    template = make_template('''
        var options = new Options();
        {% if lan_mode %}
        options.Connectivity.ConfigureForLan();
        {% endif %}
        options.Connectivity.SeedPeers.Add("{{host}}:{{seed_peer_port}}");
        options.Connectivity.IsBroadcastEnabled = false;
        {% if tunnel_enabled %}
        options.Connectivity.TunnelMode = TunnelMode.On;
        options.Connectivity.TunnelUris.Add("http://{{host}}:21247");
        {% endif %}
        {% if overload_enabled %}
        options.Overload.EnableForcedOverload("i_understand_this_is_for_testing_only");
        options.Overload.IsClientEnabled = true;
        {% endif %}
        options.Connectivity.ApplicationName = "{{application_name}}";
    ''')

    print '-'*30
    print "Use the following demo config:"
    print '-'*30
    print(template.render(opts).encode('UTF-8'))
    print '-'*30

def run_remote_demo(args):
    '''Run the windows demo with seed peer, arbitration and optional overload / tunnel servers running remotely'''
    parser = argparse.ArgumentParser()
    parser.add_argument('--overload', action='store_true', help="enable overload")
    parser.add_argument('--tunnel', action='store_true', help="enable tunnel")
    parser.add_argument('--appname', help="Application name", default='test')
    parser.add_argument('--lan', action='store_true', help="run seed peer in LAN mode")
    parser.add_argument('--dei', action='store_true', help="use Dei authentication (default %(default)s)")
    parser.add_argument('--no-dei', action='store_false', dest='dei')
    parser.add_argument('--user', help="username on remote host", default='badumna')
    parser.add_argument('host', help="remote host")
    parser.add_argument('path', help="remote (unpacked) dotNET package path")
    opts = parser.parse_args(args)

    host = opts.host
    base = opts.path

    def scp(src, dest):
        if src[1] == ':':
            src = src[2:]
        subprocess.check_call(['scp',src, opts.user + '@' + host + ':' + dest])

    configuration = {
        'application_name': opts.appname,
        'start_port': 21300,
        'end_port': 21399,
        'lan_mode': False,
        'host': opts.host,
        'seed_peer_port': 21251,
        'overload_enabled': opts.overload,
        'tunnel_enabled': opts.tunnel,
    }

    def server_config_for(**additional_opts):
        opts = configuration.copy()
        opts.update(additional_opts)
        return server_config(opts, name=opts.get('server_mode', 'unknown'))
    
    def dest_config_file(name):
        return '/tmp/badumna-config-{}.xml'.format(name)

    with Supervisor() as supervisor:
        SSH_CMD = ['ssh',opts.user +'@'+ opts.host,'--']
        def start_remotely(cmd, **k):
            supervisor.start_process(SSH_CMD + cmd, log='remote-%(name)s-out.log' % (k), **k)

        with server_config_for(server_mode='seed', lan_mode = opts.lan) as path:
            scp(path, dest_config_file('seed'))

        with server_config_for(server_mode='arbitration') as path:
            scp(path, dest_config_file('arbitration'))

        # Dei and Arbitration servers need their sqlite DB in the right place, and windows `ssh` / `popen` is crippled
        # in some way that disallows CDing into a different directory remotely :/
        # So here's a script:
        with tempfile.NamedTemporaryFile() as f:
            f.write(
                '''
                #!/bin/sh
                set -eux
                cd "$1"
                shift 1
                exec "$@"
                '''.strip())
            f.flush()
            scp(f.name, 'with-cwd')
            subprocess.check_call(SSH_CMD + ['chmod','+x','with-cwd'])

        # HACK, probably a DANGEROUS ONE:
        subprocess.call(SSH_CMD + ['pkill', 'mono'])
        assert subprocess.Popen(SSH_CMD + ['pgrep', '-l', 'mono']).wait() == 1, "Mono processes still running on remote host!"

        if opts.dei:
            dei_config = ['--dei-config-file='+base +'/Demo/Binaries/FriendListArbitrationServer.deiconfig']
            dei_relative_config = ['--dei-config-file=FriendListArbitrationServer.deiconfig']
            start_remotely(['./with-cwd', base + '/Demo/Binaries', 'mono', './DeiServer.exe', '-v'], name='dei')
            time.sleep(1)
        else:
            dei_config = dei_relative_config = []

        start_remotely(['mono', base + '/ManagementTools/SeedPeer/SeedPeer.exe', '-v']
                + dei_config + ['--badumna-config-file=' + dest_config_file('seed')], name='seedpeer')
        time.sleep(1)

        start_remotely(['./with-cwd', base + '/Demo/Binaries', 'mono', './FriendListArbitrationServer.exe']
                + dei_relative_config
                + [ '-v', '--badumna-config-file=' + dest_config_file('arbitration')], name='arbitration')

        if opts.overload:
            with server_config_for(server_mode='overload') as path:
                scp(path, dest_config_file('overload'))
            start_remotely(['mono', base + '/ManagementTools/OverloadPeer/OverloadPeer.exe']
                + dei_config + ['-v', '--badumna-config-file=' + dest_config_file('overload')], name='overload')

        if opts.tunnel:
            with server_config_for(server_mode='tunnel') as path:
                scp(path, dest_config_file('tunnel'))
            start_remotely(['mono', base + '/ManagementTools/HttpTunnel/Tunnel.exe',
                '-v', '--badumna-config-file=' + dest_config_file('tunnel')], name='tunnel')

        print_demo_config(configuration)
        print "** Also, make sure VerifiedIdentityModule's constructor has the arguments: \"%s\", %r" % (opts.host, 21248,)


class Supervisor(object):
    '''A supervisor can be used to launch many threads / processes,
    and make sure they all get cleaned up and errors reported appropriately
    '''
    def __init__(self):
        self.threads = Queue()
        self.exiting = threading.Event()
        self._error = threading.Event()
        logger.warn("Supervisor created... (press return to cancel)")
        self.spawn_thread(self.wait_for_input, daemon=True)

    def spawn_thread(self, target, **k):
        try:
            daemon = k.pop('daemon')
        except KeyError:
            daemon = False
        thread = threading.Thread(target=target,**k)
        thread.daemon = daemon
        self.threads.put(thread)
        thread.start()

    def wait_for_input(self):
        sys.stdin.readline()
        logger.warn("Exiting...")
        self.exiting.set()

    def error(self):
        self._error.set()
        self.exiting.set()

    def start_process(self, *a, **k):
        if self.exiting.isSet():
            self.error()
            return
        try:
            name = k.pop('name')
        except KeyError:
            name = a[0][0]
        logger.info("Launching {}".format(name))

        try:
            log_file = k.pop('log')
            logger.info("Logging to %s", log_file)
            log_file = open(log_file, 'w')
        except KeyError:
            log_file = None

        cwd = k.get('cwd', None)
        if cwd is not None:
            cwd = os.path.join(*cwd.split("/"))
            a[0][0] = os.path.join(cwd, a[0][0])

        try:
            intercept_output = k.pop('intercept_output')
        except KeyError:
            intercept_output = True

        if intercept_output:
            k['stdout'] = subprocess.PIPE
            k['stderr'] = subprocess.STDOUT

        logger.debug(" args: {!r}".format(a[0]))
        child = subprocess.Popen(*a, **k)
        logger.debug(" launched PID {}".format(child.pid))
        child_logger = logging.getLogger(".".join([__name__, name, str(child.pid)]))

        def reaper():
            # wait for `exiting` condition, and kill process
            self.exiting.wait()
            if child.poll() is None:
                logger.warn("Killing process")
                child.kill()

        def poller():
            ret = child.wait()
            child_logger.warn("exited with return code {}".format(ret))
            if ret != 0:
                self.error()
            self.exiting.set()

        def printer():
            for line in child.stdout:
                if(log_file is not None):
                    log_file.write(line)
                    log_file.flush()
                else:
                    child_logger.info(line.rstrip())
                if self.exiting.isSet(): return

        if intercept_output:
            self.spawn_thread(printer, daemon=True)
        self.spawn_thread(reaper)
        self.spawn_thread(poller)

    def cleanup(self):
        try:
            while True:
                sys.stdout.flush()
                thread = self.threads.get(False)
                if thread.is_alive():
                    if not thread.daemon:
                        thread.join()
        except Empty: pass
        logger.info("all children terminated")

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_trace):
        logger.info("All processes running...")
        if exc_type is not None:
            logger.error("Caught exception: %s" % (exc_val,))
            self.exiting.set()
        while True:
            try:
                logger.info("waiting for threads to exit...")
                self.cleanup()
                break
            except KeyboardInterrupt:
                print "Interrupted ..."
                pass
        if self._error.isSet():
            raise AssertionError("Error occurred in child process (or cancelled)")
        return False



if __name__ == '__main__':
    assert len(sys.argv) > 1
    action = 'run_' + sys.argv[1]
    actions = globals()
    assert action in actions, "Unknown action: {}".format(action)

    logger.setLevel(logging.INFO)
    try:
        sys.argv.remove('-v')
    except ValueError: pass
    else:
        logger.setLevel(logging.DEBUG)

    logger.info("running {}".format(action))
    actions[action](sys.argv[2:])
