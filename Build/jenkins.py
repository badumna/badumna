# To set up the cacerts file so that java accepts our self-signed certificate:
#openssl s_client -connect dev.badumna.com:443 < /dev/null | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > dev_badumna.cer
#cp /System/Library/Frameworks/JavaVM.framework/Home/lib/security/cacerts .

#keytool -import -file dev_badumna.cer -keystore cacerts  # password is "changeit"

import os
import urllib
import subprocess
import shutil
import zipfile
import logging

from utils import url_to_tempfile, make_temp_zip

logger = logging.getLogger()

JENKINS_URL = "https://dev.badumna.com/jenkins"
SCRIPT_DIR = os.path.abspath(os.path.dirname(__file__))


def cli(command):
    """Run the given command via the Jenkins CLI."""
    cli = url_to_tempfile(JENKINS_URL + '/jnlpJars/jenkins-cli.jar')

    try:
        output = subprocess.check_output([
            'java',
            '-Djavax.net.ssl.trustStore=' + os.path.join(SCRIPT_DIR, 'cacerts'),
            '-jar', cli,
            '-i', os.path.normpath(os.path.expanduser('~/.ssh/id_rsa_jenkins')),
            '-s', JENKINS_URL] + command)
    except subprocess.CalledProcessError as ex:
        if ex.returncode == -1:
            logger.warn("Make sure you have created a keypair for Jenkins, the private key is in ~/.ssh/id_rsa_jenkins, and the public key has been added to your Jenkins user account.")
        raise

    os.remove(cli)

    return output


def build(job_name, parameters):
    """Build the specified Jenkins job and returns its build number if successful."""
    command = ['build', job_name, '-s']
    command += [x for p in parameters.iteritems() for x in ('-p', '{}={}'.format(p[0], p[1]))]
    output = cli(command)

    build_number = int(output.splitlines()[0].rsplit('#')[1])
    return build_number


def get_artifacts(job_name, build_number, output_directory):
    """Save the artifacts from the specified build."""
    archive_url = JENKINS_URL + '/job/{}/{}/artifact/*zip*/archive.zip'.format(urllib.quote(job_name), build_number)
    archive_zip_file = url_to_tempfile(archive_url)
    print "fetching artifacts from jenkins job %r build number %r from URL: %s" % (job_name, build_number, archive_url)

    with zipfile.ZipFile(archive_zip_file) as archive_zip:
        for member in archive_zip.infolist():
            if member.file_size == 0:   # intended to skip directories, but will also skip empty files...
                continue

            member_file = archive_zip.open(member)
            with open(os.path.join(output_directory, os.path.basename(member.filename)), 'wb') as output_file:
                print "Copying %s -> %s" % (member.filename, output_directory)
                shutil.copyfileobj(member_file, output_file)
            member_file.close()
            member_file = None   # Workaround for the fact the ZipFileExt doesn't actually close the file handle.  Dropping the reference should hopefully cause the file to be closed in most Python implementations.

    os.remove(archive_zip_file)


def obfuscate(input_files, output_directory):
    """Run the Obfuscate job."""
    assert os.path.isdir(output_directory), "Output directory does not exist: " + output_directory
    job_name = "Obfuscate"

    input_zip = make_temp_zip(input_files)

    build_number = build(
        job_name, {
            'files.zip': input_zip,
            'dotfuscator-config.xml': os.path.join(SCRIPT_DIR, 'obfuscate', 'badumna-dotfuscator.xml')
        }
    )

    os.remove(input_zip)
    get_artifacts(job_name, build_number, output_directory)


def osx_build(target, files_to_archive, output_directory, commit=None):
    assert os.path.exists(output_directory), "Output directory does not exist: " + output_directory

    if commit is None:
        commit = subprocess.check_output(['git', 'rev-parse', 'HEAD']).strip()

    job_name = '(generic) Build and archive (osx)'

    build_number = build(
        job_name, {
            'COMMIT': commit,
            'TARGET': target,
            'ARCHIVE': files_to_archive
        })

    get_artifacts(job_name, build_number, output_directory)
