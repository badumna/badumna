from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import NoSuchElementException, MoveTargetOutOfBoundsException, TimeoutException
from selenium.webdriver.common.keys import Keys
import time

CERT_PASSWORD = 'password'
USER = 'admin'
USER_PASSWORD = 'admin_pass'
GERM_ADDRESS = 'localhost:21253'

DEI_SERVER_ADDRESS = 'localhost:21248'
DEI_ADMIN = 'admin'
DEI_ADMIN_PASSWORD = 'admin_password'
DEI_USER = 'arbitrator'
DEI_PASSWORD = 'arbitrator_password'

CC_URL = 'http://127.0.0.1:21254'
CERT_URL = '%s/Home/CreateCertificate' % CC_URL
CERT_AUTH_URL = '%s/Home/CertificateAuthentication' % CC_URL
LOGIN_URL = '%s/Account/LogOn' % CC_URL
CREATE_URL = '%s/Network/Create' % CC_URL
HOME_URL = '%s/Network/Details?name=%s'
ADD_GERM_URL = '%s/Germ/Create' % CC_URL

SERVICE_CONFIGURATION = {
    'SeedPeer' : {
        'UseDei' : True,
        'DeiUsername' : DEI_USER,
        'DeiPassword' : DEI_PASSWORD,
    },
    'OverloadPeer' : {
        'UseDei' : True,
        'DeiUsername' : DEI_USER,
        'DeiPassword' : DEI_PASSWORD,
    },
}
_sentinel = object()

class Object(object):pass
class ControlCenter():
    def __init__(self, friendly_name, application_name):
        self.friendly_name = friendly_name
        self.application_name = application_name
        self.selenium = webdriver.Firefox()

    def get(self, url='/'):
        return self.selenium.get(url)

    def css(self, selector, elem=_sentinel, wait=True):
        if elem is _sentinel:
            elem = self.selenium

        state = Object()
        def find():
            state.found = elem.find_element_by_css_selector(selector)
            return state.found
        try:
            self.wait.until_success(lambda *a: find())
        except TimeoutException:
            raise NoSuchElementException("Could not find by css selector: %s" %(selector,))
        return state.found

    def all_css(self, selector, elem=_sentinel):
        if elem is _sentinel:
            elem = self.selenium
        return elem.find_elements_by_css_selector(selector)

    def by_name(self, name, elem=_sentinel, wait=True):
        if elem is _sentinel:
            elem = self.selenium

        state = Object()
        def find():
            state.found = elem.find_element_by_name(name)
            return state.found
        try:
            self.wait.until_success(lambda *a: find())
        except TimeoutException:
            raise NoSuchElementException("Could not find by name: %s" %(name,))
        return state.found

    def by_id(self, id, elem=_sentinel, wait=True):
        if elem is _sentinel:
            elem = self.selenium

        state = Object()
        def find():
            state.found = elem.find_element_by_id(id)
            return state.found
        try:
            self.wait.until_success(lambda *a: find())
        except TimeoutException:
            raise NoSuchElementException("Could not find by id: %s" %(id,))
        return state.found

    def a(self, name):
        elements = self.all_css('a')
        for elem in elements:
            if elem.text.strip() == name:
                return elem
        return None

    def get_wait(self, timeout=5, ignored_exceptions=None):
        if ignored_exceptions is None:
            ignored_exceptions = (NoSuchElementException, MoveTargetOutOfBoundsException, AssertionError)
        return WaitExtensions(self.selenium, timeout=5, ignored_exceptions=ignored_exceptions)
    wait = property(get_wait)

    def enter_text(self, elem, text):
        for attempt in range(0,5):
            elem.clear()
            elem.send_keys(text)
            if elem.get_attribute("value") == text:
                return

    def run(self):
        self.get(CC_URL)

        if self.selenium.current_url == CERT_URL or self.selenium.current_url == CERT_AUTH_URL:
            self.enter_certificate_passphrase()
            assert self.selenium.current_url == LOGIN_URL
        
        if self.selenium.current_url == LOGIN_URL:
            self.login()

        if self.selenium.current_url == CREATE_URL:
            self.create_configuration()

        if self.selenium.current_url == HOME_URL % (CC_URL, self.friendly_name):
            self.add_germ();
            assert self.selenium.current_url == HOME_URL % (CC_URL, self.friendly_name)
            self.start_service('Dei')            
            self.create_dei_account(DEI_USER, DEI_PASSWORD)
            self.start_service('SeedPeer')
            self.start_service('OverloadPeer')
            self.start_service('StatisticsServer')

    def enter_certificate_passphrase(self):
        password_form = self.css("form")
        self.enter_text(self.by_name('Passphrase', password_form), CERT_PASSWORD)

        try:
            self.enter_text(self.by_name('PassphraseConfirmation', password_form), CERT_PASSWORD)
        except:
            pass

        password_form.submit()

    def login(self):
        login_form = self.css("form")
        
        self.enter_text(self.by_name('username', login_form), USER)
        self.enter_text(self.by_name('password', login_form), USER_PASSWORD)

        login_form.submit()

    def create_configuration(self):
        create_form = self.css("form")

        self.enter_text(self.by_name('Name', create_form), self.friendly_name)
        self.enter_text(self.by_name('ProtocolHash', create_form), self.application_name)

        create_form.submit()

    def add_germ(self):
        a = self.a('Add Germ')        
        a.click()
        assert self.selenium.current_url == ADD_GERM_URL

        germ_form = self.css("form")
        self.enter_text(self.by_name('Host', germ_form), GERM_ADDRESS.split(':')[0])
        self.enter_text(self.by_name('Port', germ_form), GERM_ADDRESS.split(':')[1])
        germ_form.submit()
        self.go_home(5)

    def go_home(self, delay=0):
        time.sleep(delay)
        try:
            self.wait.until_success(lambda *a: self.get('%s/Network' % CC_URL))
        except TimeoutException:
            raise
        time.sleep(5) #extra 5 seconds to make sure the ajax request in the home page doesn't cause any trouble.                     

    def start_service(self, service_name):
        self.get('%s/Component/Edit?index=%s' % (CC_URL, service_name))

        ## Configure services
        ## Please note due to auto saving in the configuration page, you cannot
        ## change the settings to quick.
        if service_name in SERVICE_CONFIGURATION:
            configs = SERVICE_CONFIGURATION[service_name]
            if 'UseDei' in configs and configs['UseDei']:
                checkbox = self.by_name('UseDei')
                if not checkbox.is_selected():
                    checkbox.click()
            time.sleep(2) 
            for key in [x for x in configs if x != 'UseDei']:
                elem = self.by_name(key)
                elem.send_keys(configs[key])
                elem.send_keys(Keys.TAB)
                time.sleep(2)                
                self.selenium.refresh()
                time.sleep(2)                

        ## Starting services
        a = self.a('Start')
        elements = self.all_css('a')
        a.click()
        button = self.by_id('jqi_state0_buttonSubmit')
        button.click()
        self.go_home(20)

    def create_dei_account(self, user, password):
        a = self.a('Show status')
        a.click()

        ## TODO: assert is in the Dei status page
        
        ## Get dei login form
        dei_login_form = self.css('form')
        self.enter_text(self.by_name('Username', dei_login_form), DEI_ADMIN)
        self.enter_text(self.by_name('Password', dei_login_form), DEI_ADMIN_PASSWORD)
        dei_login_form.submit()
        
        time.sleep(15)
        dei_account_form = self.css('form')
        self.enter_text(self.by_name('Username', dei_account_form), user)
        self.enter_text(self.by_name('Password', dei_account_form), password)
        self.enter_text(self.by_name('PasswordConfirmation', dei_account_form), password)
        for e in dei_account_form.find_elements_by_id('accountType'):
            if e.get_attribute('value').strip() == "HostedService":
                e.click()
        dei_account_form.submit()
        self.go_home(10)

class WaitExtensions(WebDriverWait):
    def until_success(self, fn, *a):
        def action(*inner_a):
            try:
                fn(*inner_a)
                return True
            except self._ignored_exceptions as e:
                return False
        return self.until(action, *a)

# if __name__ == '__main__':
#     cc = ControlCenter('MyApp', 'ControlCenter_Test')
#     cc.run()