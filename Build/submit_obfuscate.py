#!/usr/bin/env python

import sys
from jenkins import obfuscate

if len(sys.argv) < 3:
    print "usage: {} <out_dir> file1 [file2 ...]".format(sys.argv[0])
    sys.exit(1)

obfuscate(sys.argv[2:], sys.argv[1])
