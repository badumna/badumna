import os, sys
import logging
import colorlog
import argparse
import subprocess
from zipfile import ZipFile
import time
import shutil
from controlcenter_tester import ControlCenter

logger = logging.getLogger()
logger.addHandler(colorlog.ColorizingStreamHandler())

DEI_SERVER_PATH = 'ManagementTools/Dei/DeiServer/DeiServer.exe'
SEED_PEER_PATH = 'ManagementTools/SeedPeer/SeedPeer.exe'
OVERLOAD_PEER_PATH = 'ManagementTools/OverloadPeer/OverloadPeer.exe'
STATISTICS_SERVER_PATH = 'ManagementTools/StatisticsServer/StatisticsServer.exe'

CONTROL_CENTER_PATH = 'ManagementTools/ControlCenter/ControlCenter.exe'
GERM_PATH = 'ManagementTools/ControlCenter/Germ/LaunchGerm.exe'

#---------------------------------------------------------------------------------------
# Test Utils
#---------------------------------------------------------------------------------------
def is_windows():
    return sys.platform.startswith('win')

def RP(*path):
    return os.path.normpath(os.path.join(*path))

def P(*path):
    return os.path.abspath(RP(*path))

def ext(filename):
    return os.path.splitext(filename)[1][1:]

def copy_file(src, dest):
    logger.info("Copying %s -> %s" %(src, dest))
    shutil.copy(src, dest)

def erase(path):
    if os.path.exists(path):
        logger.debug("erasing: %s" % path)
        if os.path.isdir(path):
            shutil.rmtree(path)
        else:
            os.remove(path)
    else:
        logger.info("not erasing nonexistant path: %s", path)

def prompt_to_continue(message, expected='yes'):
    logging.info(message)
    
    while True:
        logging.info('Enter %s to continue, or ^C to abort' % expected)
        if raw_input('--> ') == expected:
            break

#---------------------------------------------------------------------------------------
# Testing Codes
#---------------------------------------------------------------------------------------

TEST_FILES = {
    'deiConfig' : P('testing/deiConfig'),    
    'deiDatabase' : P('testing/DeiAccounts.s3db')   
}

def unzip_sdk(sdk_zip_path):
    logger.info('Unziping sdk: %s' % sdk_zip_path)
    assert ext(sdk_zip_path) == 'zip', 'Invalid installer zip path'
    dest = os.path.splitext(sdk_zip_path)[0]

    if os.path.exists(dest):
        erase(dest)

    ZipFile(sdk_zip_path).extractall(os.path.dirname(sdk_zip_path))
    return P(dest)

def check_paths(sdk_path):
    # make sure all the .exe files to test are exist.
    check_path(P(sdk_path, DEI_SERVER_PATH))
    check_path(P(sdk_path, SEED_PEER_PATH))
    check_path(P(sdk_path, OVERLOAD_PEER_PATH))
    check_path(P(sdk_path, STATISTICS_SERVER_PATH))
    check_path(P(sdk_path, CONTROL_CENTER_PATH))
    check_path(P(sdk_path, GERM_PATH))

def check_path(path):
    assert os.path.exists(path), '%s is not exist' % path
    logger.debug('success: %s exists' % path)

def running_console_services(sdk_path):
    copy_file(TEST_FILES['deiDatabase'], os.path.dirname(P(sdk_path, DEI_SERVER_PATH)))

    running_service(['"%s"' %P(sdk_path, DEI_SERVER_PATH)], os.path.dirname(P(sdk_path, DEI_SERVER_PATH)))
    
    time.sleep(5) # sleep for 5 seconds before running the seed peer
    running_service(['"%s"' %P(sdk_path, SEED_PEER_PATH), '--application-name=sdk_test', '--dei-config-file=%s' %TEST_FILES['deiConfig'], '-v'])

    running_service(['"%s"' %P(sdk_path, OVERLOAD_PEER_PATH), '--application-name=sdk_test', '--dei-config-file=%s' %TEST_FILES['deiConfig'], '-v'])

    running_service(['"%s"' %P(sdk_path, STATISTICS_SERVER_PATH)])

def running_service(cmd, wd='', *a, **k):
    logger.debug('running: %s' % repr(cmd))
    cwd = os.getcwd()
    if wd != '':
        os.chdir(wd)
    
    if is_windows():
        os.system("start cmd /K %s" % ' '.join(cmd))
    else:
        full_cmd = "screen mono %s" % ' '.join(cmd)
        logger.debug("Running: " + full_cmd)
        os.system(full_cmd)
    os.chdir(cwd)

def running_control_center(sdk_path):
    running_service(['"%s"' %P(sdk_path, CONTROL_CENTER_PATH)], os.path.dirname(P(sdk_path, CONTROL_CENTER_PATH)))
    time.sleep(25) # sleep for 25 seconds before launching the germ
    running_service(['"%s"' %P(sdk_path, GERM_PATH)], os.path.dirname(P(sdk_path, GERM_PATH)))

def parse_args():
    parser = argparse.ArgumentParser(description='Automatic installer tester')
    parser.add_argument('-f', '--sdk-path', default=[], nargs=1, help='full path to the SDK zip file, can be specified multiple times')
    parser.add_argument('-v', '--verbose', action='store_true', help='print more stuff')

    return parser.parse_args(sys.argv[1:])


def main():
    opts = parse_args()

    if opts.verbose:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    if not is_windows() and not 'STY' in os.environ:
        logger.error('Must be run inside a "screen" session')
        sys.exit(1)

    for path in opts.sdk_path:
        logger.info('SDK path: %s' % path)
        output_sdk = unzip_sdk(path)
        logger.info('SDK output: %s' % output_sdk)
        
        logger.info('Checking files in SDK output')
        check_paths(output_sdk)

        logger.info('Running console services')
        running_console_services(output_sdk)
        logger.info('Finish running console services')
        prompt_to_continue('When you ready, kill all the running services to continue testing the SDK.')

        logger.info('Starting control center')
        running_control_center(output_sdk)
        logger.info('Testing control center')        
        cc = ControlCenter('MyApp', 'ControlCenter_Test')
        cc.run()
        logger.info('Finish testing control center')                

if __name__ == '__main__':
    main()