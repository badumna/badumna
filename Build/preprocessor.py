import os
import re
import shutil
import logging
logger = logging.getLogger(__name__)

class PreProcessor(object):
    
    def __init__(self):
        # stack of statuses of nested inclusion clauses:
        #   True: currently including code
        #   False: currently excluding code
        #   None: current #if is being entirely excluded due to nesting inside excluded code
        self.includeStack = [True]
        self.regionstack = []
        self._if_re = re.compile(r'#if\s+(\w+(?:\s*\|\|\s*\w+)*)\s*(?://.*)?$')
        self._leading_docstring = re.compile(r'\s*/// ')
        self._xml_re = re.compile(r'<\s*(?P<tag>[-a-zA-Z]+)\s*(?P<attrs>[^>]*)/?\s*>')
        self._xml_endtag_re = re.compile(r'</\s*[-a-zA-Z]+\s*>')
        self._xml_attr_re = re.compile(r'(?P<key>[-a-zA-Z]+)="(?P<val>[^"]*)"')
        self._xml_entity_re = re.compile(r'&[a-zA-Z]+;')
        self._region_prefix = re.compile(r'#(end)?region +(// *)?')
        
    def preprocess(self, inputFilename, outputFilename, symbols, docDest=None):
        # Does not test for well formed grammar of preprocessor directives,
        # but C# compiler should already have checked that.
        outputPath, filename = os.path.split(outputFilename)
        if not os.path.exists(outputPath):
            os.makedirs(outputPath)
        regionFactory = None if docDest is None else Region.Factory(outputPath, docDest, filename)
        with open(inputFilename,"r") as inputFile:
            with open(outputFilename,"w") as outputFile:
                self.definedSymbols = symbols
                lineno = 1
                for input_lineno, line in enumerate(inputFile.readlines(), 1):
                    try:
                        if line.startswith("#if"):
                            self._process_if(line)
                        elif line.startswith("#else"):
                            self._process_else()
                        elif line.startswith("#endif"):
                            self._process_endif()
                        elif self.includeStack[-1]:
                            line = line.rstrip()
                            if regionFactory and line.lstrip().startswith("#region"):
                                name = self._region_prefix.sub("", line).strip()
                                self._begin_region(regionFactory, name, lineno)
                            elif regionFactory and line.lstrip().startswith("#endregion"):
                                name = self._region_prefix.sub("", line).strip()
                                self._end_region(name, lineno)
                            elif line.startswith("##"):
                                print >> outputFile, line[1:]
                                lineno += 1
                            elif self._leading_docstring.match(line):
                                for doc_line in self._process_doc_line(line):
                                    print >> outputFile, doc_line.rstrip()
                                    lineno += 1
                            else:
                                print >> outputFile, line
                                lineno += 1
                    except Exception as e:
                        logging.error("Error processing %(filename)s:%(input_lineno)s: %(line)s\nError message: %(e)s" % locals())
                        raise

    def replace_in_file(self, search_text, replace_text, infile, outfile):
        with open(inputFilename,"r") as input_file:
            with open(outputFilename,"w") as output_file:
                output_file.write(input_file.read().replace(search_text, replace_text))
        
    def copy_folder(self, folder, destination):
        if os.path.exists(destination):
            shutil.rmtree(destination)
        shutil.copytree(folder, destination)
    
    def _process_doc_line(self, line):
        # logger.debug("Processing doc line: %s", line)
        prefix = self._leading_docstring.match(line)
        assert prefix is not None, "Failed to match documentation line"
        # logger.debug("prefix = %r", prefix.group(0))
        prefix = prefix.group(0)
        content = line[len(prefix):]
        prefix = prefix.replace('///','//')
        logger.debug("content after stripping prefix is: %s", content)

        def xml_tag_replace(match):
            logger.debug("processing XML: %r", match.group(0))
            match = match.groupdict()
            if match['tag'] == 'param':
                attrs = dict(self._xml_attr_re.findall(match['attrs']))
                logger.debug("found attributes: %r", attrs)
                assert attrs.keys() == ['name'], "I only know about `name` attributes on <param> elements!"
                return (" - %(name)s: " % attrs)

            elif match['tag'] == 'summary':
                # just strip it
                return ""
            elif match['tag'] == 'returns':
                return "Returns: "
            elif match['tag'] in ('remarks', 'para'):
                return "\n"
            else:
                raise AssertionError("Unknown documentation tag: %s" % match['tag'])

        # leave pure-whitespace lines alone
        if not content.strip():
            output = [line]
        else:
            content = self._xml_re.sub(xml_tag_replace, content)
            content = self._xml_endtag_re.sub("", content)
            if self._xml_entity_re.search(content):
                raise AssertionError(
                        "ERROR: unreplaced XML entity in line: %s\n(add some entity-replace code to %s to fix this!): "
                        % (content, __file__))
            # logger.debug("Content after XML close tag replacement: %s", content)
            if not content.strip(" "):
                output = []
            else:
                output = [prefix + l for l in content.splitlines()]
        logger.debug("Output: %r", output)
        return output

    def _process_if(self, line):
        # Cannot negate head of stack as not None -> True
        if self.includeStack[-1] != True:
            self.includeStack.append(None)
            return
        match = self._if_re.search(line)
        if match is None:
            raise Exception("#if clause not supported (only simple '||' constructs are allowed; line was '{}').".format(line))
        symbols = re.findall(r'\w+', match.group(1))
        for symbol in symbols:
            if symbol in self.definedSymbols:
                self.includeStack.append(True)
                return
        self.includeStack.append(False)

    def _process_else(self):
        # Cannot just negate head of stack as: not None -> True
        self.includeStack.append(self.includeStack.pop() == False)

    def _process_endif(self):
        self.includeStack.pop()

    def _find_active_region(self, highlight_name):
        name = highlight_name[len("highlight"):].strip()
        if name:
            for region in self.regionstack:
                if region.name == name:
                    return region
            else:
                raise AssertionError("Couldn't find containing region for: " + highlight_name)
        else:
            # assume it applies to the innermost region
            return self.regionstack[-1]

    def _begin_region(self, factory, name, lineno):
        if name.lower().startswith("highlight"):
            # logger.info("begin_highlight[%s]: %s", name, lineno)
            region = self._find_active_region(name)
            region.begin_emphasis(lineno)
        else:
            # logger.info("begin_region[%s]: %s", name, lineno)
            self.regionstack.append(factory(name, lineno))

    def _end_region(self, name, lineno):
        if name.lower().startswith("highlight"):
            # logger.info("end_highlight[%s]: %s, region_stack len is currently %s", lineno, name, len(self.regionstack))
            region = self._find_active_region(name)
            region.end_emphasis(lineno-1)
        else:
            # logger.info("end_region: %s, region_stack len is currently %s: %s", lineno, len(self.regionstack), name)
            region = self.regionstack.pop()
            region.end(lineno-1)
            region.export()

class Lines(object):
    '''represents a sequence of lines in a file,
    used for including / emphasizing lines in RST documentation'''
    def __init__(self, lineno):
        self.startpos = lineno
        self.endpos = None

    def end(self, lineno):
        assert self.endpos is None
        self.endpos = lineno

    def as_rst(self):
        assert self.endpos >= self.startpos
        if self.endpos == self.startpos:
            return str(self.startpos)
        return "-".join(map(str, (self.startpos, self.endpos)))
    
    def __repr__(self):
        return "Lines%r" % ((self.startpos, self.endpos),)

class Region(object):

    @classmethod
    def Factory(cls, source_base, output_base, filename):
        return lambda name, lineno: cls(source_base, output_base, filename, name, lineno)

    def __init__(self, source_base, output_base, filename, name, lineno):
        self.lines = Lines(lineno)
        self.source_base = source_base
        self.output_base = output_base
        self.filename = filename
        self.name = name
        self.emphasis_list = []
        self.emphasis_stack = []

    def begin_emphasis(self, lineno):
        em = Lines(lineno - self.lines.startpos + 1)
        self.emphasis_stack.append(em)

    def end_emphasis(self, lineno):
        em = self.emphasis_stack.pop()
        em.end(lineno - self.lines.startpos + 1)
        self.emphasis_list.append(em)

    def end(self, lineno):
        self.lines.end(lineno)

    def export(self):
        assert self.emphasis_stack == [], "Unclosed emphasis (highlight) for region: %s - %r" % (self.name, self.emphasis_stack)
        if not os.path.isdir(self.output_base):
            os.makedirs(self.output_base)
        filename = self.filename + "." + self.name.replace(" ","_") + ".rst"
        with open(os.path.join(self.output_base, filename), 'w') as f:
            code_path = os.path.join(self.source_base, self.filename)
            output = [
                ".. literalinclude:: %s" % (code_path,),
                "    :language: c#",
                "    :lines: %s" % (self.lines.as_rst())
            ]
            if self.emphasis_list:
                output.append("    :emphasize-lines: %s" % ",".join([em.as_rst() for em in self.emphasis_list]))
            for line in output:
                print >> f, line
        logging.debug("Wrote: %s", os.path.join(self.output_base, filename))
