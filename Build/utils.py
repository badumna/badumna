
import tempfile
import zipfile
import shutil
import urllib2

def make_temp_zip(filenames):
    """Zip the given files into a temporary file and return its name."""
    with tempfile.NamedTemporaryFile(delete=False) as input_zip_file:
        with zipfile.ZipFile(input_zip_file, 'w') as input_zip:
            for input_file in filenames:
                input_zip.write(input_file)

    return input_zip_file.name

def url_to_tempfile(url):
    """Save the contents of the given URL to a temporary file and return its name."""
    source = urllib2.urlopen(url)
    with tempfile.NamedTemporaryFile(delete=False) as temp_file:
        shutil.copyfileobj(source, temp_file)
        source.close()
    return temp_file.name
